/******************************************************************
Project: Release 11.9 Date: 26.APR.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_DemandForecastDaysDisplay')
DROP FUNCTION fn_DemandForecastDaysDisplay
GO
CREATE FUNCTION [dbo].[fn_DemandForecastDaysDisplay]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numItemCode NUMERIC(18,0)
	,@numLeadDays NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays NUMERIC(18,0)
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @fltDemandBasedOnReleaseDate FLOAT = 0
	DECLARE @fltDemandBasedOnHistoricalSales FLOAT = 0
	DECLARE @fltDemandBasedOnOpportunity FLOAT = 0
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @numAnalysisDays INT = 0

	SET @fltDemandBasedOnReleaseDate = ISNULL((SELECT
												SUM(numUnitHour - ISNULL(numQtyShipped,0))
											FROM
												OpportunityMaster OM
											INNER JOIN
												OpportunityItems OI
											ON
												OM.numOppId=OI.numOppId
											WHERE
												OM.numDomainId=@numDomainID
												AND OM.tintOppType=1
												AND OM.tintOppStatus=1
												AND ISNULL(OM.tintshipped,0)=0
												AND (ISNULL(OI.numUnitHour,0) - ISNULL(numQtyShipped,0)) > 0
												AND OI.numItemCode=@numItemCode
												AND OI.numWarehouseItmsID=@numWarehouseItemID
												AND ISNULL(bitDropship,0) = 0
												AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
												AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)
	

	-- KIT CHILD ITEMS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId = OI.numOppId
												INNER JOIN
													OpportunityKitItems OKI
												ON
													OI.numoppitemtCode=OKI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKI.numChildItemID=@numItemCode
													AND OKI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	-- KIT CHILD ITEMS OF CHILD KITS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId=OI.numOppId
												INNER JOIN
													OpportunityKitChildItems OKCI
												ON
													OI.numoppitemtCode=OKCI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKCI.numItemID=@numItemCode
													AND OKCI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	--ITEMS USED IN WORK ORDER
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(WOD.numQtyItemsReq)
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrder WO
												ON
													WOD.numWOId=WO.numWOId
												LEFT JOIN
													OpportunityItems OI
												ON
													WO.numOppItemID = OI.numoppitemtCode
												LEFT JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													WO.numDomainID=@numDomainID
													AND WO.numWOStatus <> 23184 -- NOT COMPLETED
													AND WOD.numChildItemID = @numItemCode
													AND WOD.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(WOD.numQtyItemsReq,0) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
													AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
													AND 1 = (CASE 
															WHEN OI.numoppitemtCode IS NOT NULL 
															THEN (CASE 
																	WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																	THEN 1 
																	ELSE 0 
																END)
															ELSE 
																(CASE 
																WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																THEN 1 
																ELSE 0 
																END)
															END)),0)


	IF ISNULL(@bitShowHistoricSales,0) = 1
	BEGIN
		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numHistoricalAnalysisPattern

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		SET @fltDemandBasedOnHistoricalSales = ISNULL((SELECT
															SUM(numUnitHour)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI	
														ON
															OM.numOppId=OI.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND numItemCode=@numItemCode
															AND numWarehouseItmsID=@numWarehouseItemID
															AND ISNULL(OI.numUnitHour,0) > 0
															AND ISNULL(bitDropship,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitItems OKI
														ON
															OI.numoppitemtCode=OKI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKI.numChildItemID=@numItemCode
															AND OKI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKCI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitChildItems OKCI
														ON
															OI.numoppitemtCode=OKCI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKCI.numItemID=@numItemCode
															AND OKCI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKCI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(WOD.numQtyItemsReq)
														FROM
															WorkOrderDetails WOD
														INNER JOIN
															WorkOrder WO
														ON
															WOD.numWOId=WO.numWOId
														LEFT JOIN
															OpportunityItems OI
														ON
															WO.numOppItemID = OI.numoppitemtCode
														LEFT JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															WO.numDomainID=@numDomainID
															AND WOD.numChildItemID = @numItemCode
															AND WOD.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(WOD.numQtyItemsReq,0) > 0
															AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		SET @fltDemandBasedOnHistoricalSales = (@fltDemandBasedOnHistoricalSales/@numAnalysisDays) * @numForecastDays
	END

	IF ISNULL(@bitIncludeOpportunity,0) = 1
	BEGIN
		SET @fltDemandBasedOnOpportunity = ISNULL((SELECT
														SUM(OI.numUnitHour * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND numItemCode=@numItemCode
														AND numWarehouseItmsID=@numWarehouseItemID
														AND ISNULL(bitDropship,0) = 0
														AND (ISNULL(OI.numUnitHour,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >=  @numOpportunityPercentComplete
												),0)

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitItems OKI
													ON
														OI.numoppitemtCode=OKI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKI.numChildItemID=@numItemCode
														AND OKI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKCI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitChildItems OKCI
													ON
														OI.numoppitemtCode=OKCI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKCI.numItemID=@numItemCode
														AND OKCI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKCI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(WOD.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														WorkOrder WO
													ON
														OI.numoppitemtCode = WO.numOppItemID
													INNER JOIN
														WorkOrderDetails WOD
													ON
														WO.numWOId=WOD.numWOId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND WO.numDomainID=@numDomainID
														AND WO.numWOStatus <> 23184 -- NOT COMPLETED
														AND WOD.numChildItemID = @numItemCode
														AND WOD.numWareHouseItemId=@numWarehouseItemID
														AND (ISNULL(WOD.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
	END


	RETURN CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge ',CASE WHEN @numLeadDays > @numForecastDays THEN 'bg-red'  ELSE 'bg-light-blue' END,'">',CASE WHEN @numLeadDays > @numForecastDays THEN @fltDemandBasedOnReleaseDate * -1 ELSE @fltDemandBasedOnReleaseDate END,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetContactName]    Script Date: 07/26/2008 18:12:31 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactname')
DROP FUNCTION fn_getcontactname
GO
CREATE FUNCTION [dbo].[fn_GetContactName](@numContactID numeric)
Returns varchar(100) 
As
BEGIN
	DECLARE @RetName Varchar(100)
	set @RetName=''
	select @RetName=isnull(vcFirstname,'')+' '+isnull(vcLastName,'') from AdditionalContactsInformation  where numContactID=@numContactID
	RETURN isnull(@RetName,'-')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
		DECLARE @bitIncludeRequisitions BIT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
			,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDate,(CASE
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
			THEN 
				(ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
												THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
												ELSE 0 
												END)) - ISNULL(numBackOrder,0) +
				(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																														SUM(numUnitHour) 
																													FROM 
																														OpportunityItems 
																													INNER JOIN 
																														OpportunityMaster 
																													ON 
																														OpportunityItems.numOppID=OpportunityMaster.numOppId 
																													WHERE 
																														OpportunityMaster.numDomainId=',@numDomainID,'
																														AND OpportunityMaster.tintOppType=2 
																														AND OpportunityMaster.tintOppStatus=0 
																														AND OpportunityItems.numItemCode=Item.numItemCode 
																														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN 
														T1.numUnitHour > 0 
													THEN 1 
													ELSE 0 
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN   


  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,        
		A.vcFirstname + ' ' + A.vcLastName AS vcGivenName, 
		A.numPhoneExtension As ContactPhoneExt, 
		A.numPhone AS ContactPhone, 
		A.numContactId AS ContactID,
		A.vcEmail AS Email,                                                        
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 D.tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 A.vcLinkedinId,
  A.vcLinkedinUrl,
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
A.vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 A.vcTitle,            
 A.vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,


 ---- Edited by Priya for Follow-up Changes (10 Jan 2018)
 --ISNULL((SELECT CONCAT(vcECampName, ' (' + 
	--								 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
	--									WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
	--								 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
	--								     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID),0) AS VARCHAR) + ') ', 
	--	 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
	--	 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
	--	 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,

	(CASE WHEN 
			ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1),0) 
			= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 ),0)
	THEN

	ISNULL((SELECT CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ', 
		 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')
	ELSE

	ISNULL((SELECT CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/GreenFlag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ', 
		 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')

	END
	) vcECampName,

		----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
---- Edited by Priya for Follow-up Changes (10 Jan 2018)


 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact,D.vcPartnerCode AS vcPartnerCode,
 ISNULL(D.numPartenerSource,0) AS numPartenerSourceId,
 (D3.vcPartnerCode+'-'+C3.vcCompanyName) as numPartenerSource,
 ISNULL(D.numPartenerContact,0) AS numPartenerContactId,
 A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=A.numContactId),'') AS vcPassword,ISNULL(A.vcTaxID,'') vcTaxID
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId  
  LEFT JOIN divisionMaster D3 ON D.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON D.numPartenerContact = A1.numContactId                                                               
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDiscountItems')
DROP PROCEDURE USP_GetItemPromotionDiscountItems
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDiscountItems]
	@numDomainID NUMERIC(18,0), 
	@numPromotionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT
AS
BEGIN
	DECLARE @tintDiscountBasedOn TINYINT
	DECLARE @tintDiscountType TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @vcCurrency VARCHAR(10)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @monDiscountedItemPrice DECIMAL(20,5)
	
	SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=@numDomainID

	SELECT @numWarehouseID=ISNULL(numWareHouseID,0) FROM WarehouseItems WHERE numWarehouseItemID = @numWarehouseItemID
	
	SELECT 
		@tintDiscountBasedOn=tintDiscoutBaseOn
		,@tintDiscountType=tintDiscountType 
		,@fltDiscountValue=ISNULL(fltDiscountValue,0)
		,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,0)
		,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
	FROM 
		PromotionOffer 
	WHERE 
		numProId = @numPromotionID 

	DECLARE @TempItem TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	PRINT @tintDiscountBasedOn

	IF @tintDiscountBasedOn = 1 -- Based on individual item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 1
	END
	ELSE IF @tintDiscountBasedOn = 2 -- Based on individual item classification(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemClassification IN (SELECT
												numValue
											FROM
												PromotionOfferItems
											WHERE
												numProId=@numPromotionID
												AND tintRecordType = 6
												AND tintType = 2)
	END
	ELSE IF @tintDiscountBasedOn = 3  -- Based on related item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			SimilarItems
		WHERE
			numParentItemCode =@numItemCode
	END
	ELSE IF @tintDiscountBasedOn = 6 
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 4
	END

	SELECT
		COUNT(Item.numItemCode) OVER() AS TotalRecords
		,Item.numItemCode
		,ISNULL(Item.vcItemName,'') vcItemName
		,ISNULL(Item.txtItemDesc,'') txtItemDesc
		,(CASE 
			WHEN @tintDiscountBasedOn = 6
			THEN @monDiscountedItemPrice
			ELSE
				(CASE 
					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
					THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,Item.numItemCode,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
					ELSE ISNULL(monListPrice,0)
				END)
		END) monListPrice
		,(CASE @tintDiscountType
			WHEN 1 THEN 0
			WHEN 2 THEN 1
			WHEN 3 THEN 0
		END) tintDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN CONCAT(@fltDiscountValue,'%')
			WHEN 2 THEN CONCAT(@vcCurrency,@fltDiscountValue)
			WHEN 3 THEN '100%'
		END) vcDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN @fltDiscountValue
			WHEN 2 THEN @fltDiscountValue
			WHEN 3 THEN 100
		END) fltDiscount
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN ISNULL(monListPrice,0) - (ISNULL(monListPrice,0) * (@fltDiscountValue/100))  ELSE ISNULL(monListPrice,0) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN ISNULL(monListPrice,0) > @fltDiscountValue THEN (ISNULL(monListPrice,0) - @fltDiscountValue) ELSE 0 END)  ELSE ISNULL(monListPrice,0) END)
			WHEN 3 THEN 0
		END) monSalePrice
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN ISNULL(monListPrice,0) - (ISNULL(monListPrice,0) * (@fltDiscountValue/100))  ELSE ISNULL(monListPrice,0) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN ISNULL(monListPrice,0) > @fltDiscountValue THEN (ISNULL(monListPrice,0) - @fltDiscountValue) ELSE 0 END)  ELSE ISNULL(monListPrice,0) END)
			WHEN 3 THEN 0
		END) monTotalAmount
		,numWareHouseItemID
		,ISNULL(Item.bitMatrix,0) bitMatrix
		,ISNULL(numItemGroup,0) numItemGroup
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID=IInner.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1),0) > 0 THEN 1 ELSE 0 END) ELSE 0 END) bitHasChildKits
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0) = 1 THEN ISNULL(bitCalAmtBasedonDepItems,0) ELSE 0 END) bitCalAmtBasedonDepItems
	FROM
		Item
	INNER JOIN
		@TempItem T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numItemID=Item.numItemCode
			AND (numWareHouseID=@numWarehouseID OR @numWarehouseItemID=0)
	) T2
	WHERE
		numDomainID=@numDomainID
	ORDER BY
		vcItemName
	OFFSET
		((@numPageIndex - 1) * @numPageSize) ROWS
	FETCH NEXT
		@numPageSize ROWS ONLY
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0,
	@ClientTimeZoneOffset AS INT
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT
			[numProId]
			,[vcProName]
			,[numDomainId]
			,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
			,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
			,[bitNeverExpires]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
			,[vcShortDesc]
			,[vcLongDesc]
			,[tintItemCalDiscount]
			,ISNULL(bitUseOrderPromotion,0) bitUseOrderPromotion
			,ISNULL(numOrderPromotionID,0) numOrderPromotionID
			,ISNULL(monDiscountedItemPrice,0) monDiscountedItemPrice
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId AND ISNULL(IsOrderBasedPromotion,0) = 0
    END  
	ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			PO.numProId,
            PO.vcProName,
			(CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN 'Order based' ELSE 'Item based' END) vcPromotionType,
			(CASE 
				WHEN ISNULL(PO.numOrderPromotionID,0) > 0
				THEN
					(CASE WHEN ISNULL(POOrder.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidFrom) ELSE POOrder.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidTo) ELSE POOrder.dtValidTo END),@numDomainID)) END)
				ELSE
					(CASE WHEN ISNULL(PO.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidFrom) ELSE PO.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidTo) ELSE PO.dtValidTo END),@numDomainID)) END)
			END) AS vcDateValidation
			,(CASE 
				WHEN ISNULL(PO.bitRequireCouponCode,0) = 1 OR ISNULL(PO.numOrderPromotionID,0) > 0 THEN
			       STUFF((SELECT CONCAT (', ' , vcDiscountCode , ' (', ISNULL(SUM(DCU.intCodeUsed),0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = (CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN PO.numOrderPromotionID ELSE PO.numProId END) GROUP BY D.vcDiscountCode, DCU.numDiscountId
									   FOR XML PATH ('')), 1, 1, '')
				ELSE '' 
			END) AS vcCouponCode
			,(CASE WHEN ISNULL(PO.bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(PO.bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN
					(CASE WHEN ISNULL(PO.bitUseForCouponManagement,0) = 0 THEN CONCAT('Buy ', 
															CASE PO.tintDiscountType WHEN 1 THEN
																		STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
																		   FROM PromotionOfferOrder POO					   
																		   WHERE POO.numPromotionID =  PO.numProId
																		   FOR XML PATH ('')), 1, 1, '')  
																	WHEN 2 THEN 
																		STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
																				   FROM PromotionOfferOrder POO					   
																				   WHERE POO.numPromotionID =  PO.numProId
																				   FOR XML PATH ('')), 1, 1, '')
															ELSE '' END) ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN PO.tintOfferTriggerValueType=1 THEN CAST(PO.fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',PO.fltOfferTriggerValue) END
					,CASE PO.tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE  
						WHEN (PO.tintDiscountType = 1 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT(PO.fltDiscountValue,'% off on') 
						WHEN (PO.tintDiscountType = 2 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT('$',PO.fltDiscountValue, ' off on')
						WHEN (PO.tintDiscountType = 3 AND PO.tintDiscoutBaseOn <> 4) THEN 
						CONCAT(PO.fltDiscountValue,' of') END
					, CASE 
						WHEN PO.tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 3 THEN ' related items.'
						WHEN PO.tintDiscoutBaseOn = 4 THEN CONCAT(PO.fltDiscountValue,' quantity free for any item of equal or lesser value')
						WHEN PO.tintDiscoutBaseOn = 5 THEN CONCAT(PO.fltDiscountValue,' quantity for any item of equal or lesser value at $',ISNULL(PO.monDiscountedItemPrice,0))
						WHEN PO.tintDiscoutBaseOn = 6 THEN CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=4 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN CONCAT(' at $',ISNULL(PO.monDiscountedItemPrice,0)) ELSE '' END)
								,'.'
							)
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,PO.bitEnabled
				, PO.tintCustomersBasedOn
				, PO.tintOfferTriggerValueTypeRange
				, PO.fltOfferTriggerValueRange
				, PO.[IsOrderBasedPromotion]
				, ISNULL(PO.monDiscountedItemPrice,0) monDiscountedItemPrice
        FROM  
			PromotionOffer PO
		LEFT JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		WHERE 
			PO.numDomainID = @numDomainID
		ORDER BY
			PO.dtCreated DESC
    END
	  
   	
END      
  
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
			,UserMaster.vcDashboardTemplateIDs
			,UserMaster.vcEmailAlias
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			tintGroupType=1 -- Application Users                    
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
			,'' AS vcEmailAlias
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,monHourlyRate
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPAuth,0) ELSE ISNULL(@bitSMTPAuth,0) END) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,(CASE WHEN ISNULL([vcSMTPServer],'') = '' THEN ISNULL(@vcSMTPServer,'') ELSE [vcSMTPServer] END) vcSMTPServer
			,(CASE WHEN ISNULL(numSMTPPort,0) = 0 THEN ISNULL(@numSMTPPort,0) ELSE numSMTPPort END) numSMTPPort
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPSSL,0) ELSE ISNULL(@bitSMTPSSL,0) END) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
			,U.vcDashboardTemplateIDs
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOffer')
DROP PROCEDURE USP_ManagePromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitUseOrderPromotion BIT,
	@numOrderPromotionID NUMERIC(18,0),
	@tintOfferTriggerValueType TINYINT,
	@fltOfferTriggerValue INT,
	@tintOfferBasedOn TINYINT,
	@tintDiscountType TINYINT,
	@fltDiscountValue FLOAT,
	@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@tintOfferTriggerValueTypeRange TINYINT,
	@fltOfferTriggerValueRange FLOAT,
	@vcShortDesc VARCHAR(100),
	@vcLongDesc VARCHAR(500),
	@tintItemCalDiscount TINYINT,
	@monDiscountedItemPrice DECIMAL(20,5)
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,0
		   ,0
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			IF @tintOfferBasedOn <> 4
			BEGIN
				IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
				BEGIN
					RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 					RETURN;
				END
			END

			IF ISNULL(@tintDiscoutBaseOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=1 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END
			ELSE IF ISNULL(@tintDiscoutBaseOn,0) = 2  AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=2 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END
			ELSE IF ISNULL(@tintDiscoutBaseOn,0) = 6  AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=4 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END

			IF ISNULL(@bitUseOrderPromotion,0) = 0
			BEGIN
				/*Check For Duplicate Customer-Item relationship 
					1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
				IF @tintCustomersBasedOn = 1
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
					BEGIN
						RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 						RETURN;
					END
				END
			
					/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
				ELSE IF @tintCustomersBasedOn = 2
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
					BEGIN
						RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 						RETURN;
					END
				END

				ELSE IF @tintCustomersBasedOn = 0
				BEGIN
					RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 					RETURN;
				END

				IF @tintCustomersBasedOn = 3
				BEGIN
					DELETE FROM PromotionOfferorganizations 
						WHERE tintType IN (1, 2) 
							AND numProId = @numProId
				END

				IF (
					SELECT
						COUNT(*)
					FROM
						PromotionOffer
					LEFT JOIN
						PromotionOfferOrganizations
					ON
						PromotionOffer.numProId = PromotionOfferOrganizations.numProId
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
					LEFT JOIN
						PromotionOfferItems
					ON
						PromotionOffer.numProId = PromotionOfferItems.numProId
						AND 1 = (CASE 
									WHEN @tintOfferBasedOn=1 THEN (CASE WHEN PromotionOfferItems.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintOfferBasedOn=2 THEN (CASE WHEN PromotionOfferItems.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItems.tintRecordType = 5
					LEFT JOIN
						PromotionOfferItems PromotionOfferItemsDiscount
					ON
						PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn=1 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintDiscoutBaseOn=2 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItemsDiscount.tintRecordType = 6
					WHERE
						PromotionOffer.numDomainId=@numDomainID
						AND PromotionOffer.numProId <> @numProId
						AND tintCustomersBasedOn = @tintCustomersBasedOn
						AND tintDiscoutBaseOn = @tintDiscoutBaseOn
						AND tintOfferBasedOn = @tintOfferBasedOn
						AND tintOfferTriggerValueType = @tintOfferTriggerValueType
						AND 1 = (CASE 
									WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN fltOfferTriggerValue = @fltOfferTriggerValue THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE WHEN fltOfferTriggerValue BETWEEN @fltOfferTriggerValue AND @fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN @fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE 
													WHEN (@fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) OR (@fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									ELSE 0
								END)
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN 3
								END)
					) > 0
				BEGIN
					DECLARE @ERRORMESSAGEVALIDATION AS VARCHAR(50)
					SET @ERRORMESSAGEVALIDATION = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 THEN 'ORGANIZATION-ITEM'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 THEN 'ORGANIZATION-ALLITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 THEN 'RELATIONSHIPPROFILE-ITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 THEN 'ALLORGANIZATIONS-ITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 THEN 'ALLORGANIZATIONS-ALLITEMS'
							END)
					RAISERROR(@ERRORMESSAGEVALIDATION,16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				DELETE FROM PromotionOfferorganizations WHERE numProId=@numProId
			END

			UPDATE 
				PromotionOffer
			SET 
				vcProName = @vcProName
				,numDomainId = @numDomainId
				,dtValidFrom = @dtValidFrom
				,dtValidTo = @dtValidTo
				,bitNeverExpires = @bitNeverExpires
				,bitUseOrderPromotion=@bitUseOrderPromotion
				,numOrderPromotionID=(CASE WHEN ISNULL(@bitUseOrderPromotion,0)=1 THEN @numOrderPromotionID ELSE 0 END)
				,tintOfferTriggerValueType = @tintOfferTriggerValueType
				,fltOfferTriggerValue = @fltOfferTriggerValue
				,tintOfferBasedOn = @tintOfferBasedOn
				,tintDiscountType = @tintDiscountType
				,fltDiscountValue = @fltDiscountValue
				,tintDiscoutBaseOn = @tintDiscoutBaseOn
				,numModifiedBy = @numUserCntID
				,dtModified = GETUTCDATE()
				,tintCustomersBasedOn = @tintCustomersBasedOn
				,tintOfferTriggerValueTypeRange = @tintOfferTriggerValueTypeRange
				,fltOfferTriggerValueRange = @fltOfferTriggerValueRange
				,vcShortDesc = @vcShortDesc
				,vcLongDesc = @vcLongDesc
				,tintItemCalDiscount = @tintItemCalDiscount
				,bitError=0
				,monDiscountedItemPrice=@monDiscountedItemPrice
			WHERE 
				numProId=@numProId
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			UPDATE PromotionOffer SET bitEnabled=0,bitError=1 WHERE numProId=@numProId 

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
        
		SELECT  @numProId
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOfferDTL')
DROP PROCEDURE USP_ManagePromotionOfferDTL
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOfferDTL]    
@numProID as NUMERIC(9)=0,    
@tintRuleAppType as TINYINT,  
@numValue as NUMERIC(9)=0,  
@numProfile as NUMERIC(9)=0,  
@numPODTLID as NUMERIC(9)=0,  
@byteMode as TINYINT,
@tintRecordType TINYINT --5=Promotion Offer Items, 6 = Promotion Disocunt Items,2=ShippingRule
AS    
	IF @byteMode=0  
	BEGIN  
		IF @tintRuleAppType=1 
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
		ELSE IF @tintRuleAppType=2 
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
		ELSE IF @tintRuleAppType=4
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
        
		INSERT INTO [PromotionOfferItems] (numProId,numValue,tintType,tintRecordType) VALUES (@numProId,@numValue,@tintRuleAppType,@tintRecordType)
	END  
	ELSE IF @byteMode=1  
	BEGIN  
		DELETE FROM PromotionOfferItems WHERE numProItemId = @numPODTLID AND tintRecordType=@tintRecordType
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_PromotionOffer_ApplyItemPromotionToECommerce') 
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToECommerce
GO
CREATE  PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToECommerce]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)	
)
AS
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

	IF @numDivisionID > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numCartID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
		,numItemClassification NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
		,monInsertUnitPrice DECIMAL(20,5)
        ,fltInsertDiscount DECIMAL(20,5)
        ,bitInsertDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	INSERT INTO @TEMP
	(
		numCartID
        ,numItemCode
		,numItemClassification
        ,numUnits
		,numWarehouseItemID
        ,monUnitPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertUnitPrice
        ,fltInsertDiscount
        ,bitInsertDiscountType
        ,monTotalAmount
        ,numPromotionID
        ,bitPromotionTriggered
		,bitPromotionDiscount
		,vcKitChildItems
		,numSelectedPromotionID
		,bitChanged
	)
	SELECT
		numCartID
        ,CI.numItemCode
		,ISNULL(I.numItemClassification,0)
        ,numUnitHour * ISNULL(decUOMConversionFactor,1)
		,numWarehouseItmsID
        ,monPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
        ,monTotAmount
        ,PromotionID
        ,bitPromotionTrigerred
		,bitPromotionDiscount
		,vcChildKitItemSelection
		,numPreferredPromotionID
		,0
	FROM
		CartItems CI
	INNER JOIN
		Item I
	ON
		CI.numItemCode = I.numItemCode
	WHERE
		CI.numDomainId=@numDomainId
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @l INT = 1
	DECLARE @lCount INT
	SET @lCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @numTempPromotionID NUMERIC(18,0)
	DECLARE @tintOfferTriggerValueType TINYINT
	DECLARE @tintOfferBasedOn TINYINT
	DECLARE @tintOfferTriggerValueTypeRange TINYINT
	DECLARE @fltOfferTriggerValue FLOAT
	DECLARE @fltOfferTriggerValueRange FLOAT
	DECLARE @vcShortDesc VARCHAR(500)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @monDiscountedItemPrice DECIMAL(20,5)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @monTotalAmount DECIMAL(20,5)

	DECLARE @bitRemainingCheckRquired AS BIT
	DECLARE @numRemainingPromotion FLOAT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT

	DECLARE @TEMPUsedPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPUsedPromotion
	(
		ID
		,numPromotionID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
		,numPromotionID
		,SUM(numUnits)
		,SUM(monTotalAmount)
	FROM
		@TEMP T1
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
	GROUP BY
		numPromotionID


	DECLARE @j INT = 1
	DECLARE @jCount INT
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

	-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
	UPDATE 
		T1
	SET 
		numPromotionID=0
		,bitPromotionTriggered=0
		,bitPromotionDiscount=0
		,vcPromotionDescription=''
		,bitChanged=1
		,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
		,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
		,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
	FROM
		@TEMP T1
	WHERE 
		numPromotionID > 0
		AND ISNULL(bitPromotionTriggered,0) = 0
		AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(T2.bitPromotionTriggered,0) = 1) = 0

	-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPUsedPromotion 
		WHERE 
			ID = @j

		IF NOT EXISTS (SELECT 
						PO.numProId 
					FROM 
						PromotionOffer PO
					LEFT JOIN	
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					LEFT JOIN	
						DiscountCodes DC
					ON
						PO.numOrderPromotionID = DC.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND PO.numProId=@numTempPromotionID
						AND ISNULL(PO.bitEnabled,0)=1 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN 
										(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
									ELSE 1 
								END)
				)
		BEGIN
			-- IF Promotion is not valid than revert line item price to based on default pricing
			UPDATE 
				T1
			SET 
				numPromotionID=0
				,bitPromotionTriggered=0
				,bitPromotionDiscount=0
				,vcPromotionDescription=''
				,bitChanged=1
				,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
				,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
				,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
			FROM
				@TEMP T1
			WHERE 
				numPromotionID=@numTempPromotionID
		END

		SET @j = @j + 1
	END

	UPDATE
		T1
	SET 
		T1.bitPromotionDiscount = 0
		,T1.bitChanged=1
		,T1.monUnitPrice=ISNULL(T2.monPrice,0)
		,T1.fltDiscount=ISNULL(T2.decDiscount,0)
		,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID=PO.numProId
	CROSS APPLY
		dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionDiscount,0) = 1
		AND ISNULL(PO.tintDiscountType,0) = 3

	IF ISNULL(@bitBasedOnDiscountCode,0) = 1
	BEGIN
		DECLARE @TableItemCouponPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,tintOfferTriggerValueType TINYINT
			,tintOfferTriggerValueTypeRange TINYINT
			,fltOfferTriggerValue FLOAT
			,fltOfferTriggerValueRange FLOAT
			,tintOfferBasedOn TINYINT
			,vcShortDesc VARCHAR(300)
		)

		INSERT INTO @TableItemCouponPromotion
		(
			numPromotionID
			,tintOfferTriggerValueType
			,tintOfferTriggerValueTypeRange
			,fltOfferTriggerValue
			,fltOfferTriggerValueRange
			,tintOfferBasedOn
			,vcShortDesc
		)
		SELECT
			PO.numProId
			,PO.tintOfferTriggerValueType
			,PO.tintOfferTriggerValueTypeRange
			,PO.fltOfferTriggerValue
			,PO.fltOfferTriggerValueRange
			,PO.tintOfferBasedOn
			,ISNULL(PO.vcShortDesc,'-')
		FROM
			PromotionOffer PO
		INNER JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		INNER JOIN 
			DiscountCodes DC
		ON 
			POOrder.numProId = DC.numPromotionID
		WHERE
			PO.numDomainId = @numDomainID
			AND POOrder.numDomainId=@numDomainID 
			AND ISNULL(PO.bitEnabled,0)=1 
			AND ISNULL(POOrder.bitEnabled,0)=1 
			AND ISNULL(PO.bitUseOrderPromotion,0)=1 
			AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
			AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
			AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
			AND DC.vcDiscountCode = @vcDiscountCode
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC
			
		SET @l = 1
		DECLARE @k INT = 1
		DECLARE @kCount INT 
		SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

		WHILE @k <= @kCount
		BEGIN
			SELECT
				@numTempPromotionID=numPromotionID
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TableItemCouponPromotion
			WHERE
				ID=@k

			IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @i = 1

					WHILE @i <= @iCount
					BEGIN

						IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @i
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @i = @i + 1
					END

				END
			END

			SET @k = @k + 1
		END
	END

	-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
	SET @i = 1
	WHILE @i <= @iCount
	BEGIN
		SET @numTempPromotionID = NULL

		SELECT
			@numTempPromotionID=numProId
			,@tintOfferTriggerValueType=tintOfferTriggerValueType
			,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
			,@fltOfferTriggerValue=fltOfferTriggerValue
			,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
			,@tintOfferBasedOn=tintOfferBasedOn
			,@vcShortDesc=vcShortDesc
		FROM
			@TEMP T1
		INNER JOIN
			Item I
		ON
			T1.numItemCode=I.numItemCode
		CROSS APPLY
		(
			SELECT TOP 1
				PO.numProId
				,PO.tintOfferBasedOn
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,ISNULL(PO.vcShortDesc,'') vcShortDesc
			FROM 
				PromotionOffer PO
			WHERE
				PO.numDomainId=@numDomainID
				AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
				AND ISNULL(PO.bitUseOrderPromotion,0) = 0
				AND ISNULL(PO.bitRequireCouponCode,0) = 0
				AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE 
							WHEN ISNULL(numOrderPromotionID,0) > 0
							THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							ELSE
								(CASE tintCustomersBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									WHEN 3 THEN 1
									ELSE 0
								END)
						END)
				AND 1 = (CASE 
							WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 4 THEN 1 
							ELSE 0
						END)
			ORDER BY
				(CASE 
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
				END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

		) T2
		WHERE
			ISNULL(T1.numPromotionID,0)  = 0
			AND T1.ID = @i
			AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

		IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
		BEGIN
			IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
			BEGIN
				SET @l = 1

				WHILE @l <= @lCount
				BEGIN
						
					IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						UPDATE
							T1
						SET
							bitChanged = 1
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=1
							,vcPromotionDescription=ISNULL(@vcShortDesc,'')
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							T1.ID = @l
							AND ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
									END)
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
					END

					SET @l = @l + 1
				END

			END
		END

		SET @i = @i + 1
	END
		

	DECLARE @TEMPPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,vcShortDesc VARCHAR(500)
		,numTriggerItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPPromotion
	(
		ID
		,numPromotionID
		,numTriggerItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numCartID ASC)
		,numPromotionID
		,numItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID = PO.numProId
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
		AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

	SET @j = 1
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numItemCode=numTriggerItemCode
			,@numTempWarehouseItemID=numWarehouseItemID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPPromotion 
		WHERE 
			ID = @j

			
		SELECT
			@fltDiscountValue=ISNULL(fltDiscountValue,0)
			,@tintDiscountType=ISNULL(tintDiscountType,0)
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@vcShortDesc=ISNULL(vcShortDesc,'')
			,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
		FROM
			PromotionOffer
		WHERE
			numProId=@numTempPromotionID

		-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
		IF @tintDiscountType = 2 OR @tintDiscountType = 3
		BEGIN
			SET @bitRemainingCheckRquired = 1
		END
		ELSE
		BEGIN
			SET @bitRemainingCheckRquired = 0
			SET @numRemainingPromotion = 0
		END

		-- If promotion is valid than check whether any item left to apply promotion
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			IF @bitRemainingCheckRquired=1
			BEGIN
				IF @tintDiscountType = 2 -- Discount by amount
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
				END
				ELSE IF @tintDiscountType = 3 -- Discount by quantity
				BEGIN
					IF @tintDiscoutBaseOn IN (5,6)
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
				END
			END

			IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
			BEGIN
				UPDATE
					T1
				SET
					monUnitPrice = (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
					,fltDiscount = (CASE 
										WHEN @bitRemainingCheckRquired=0 
										THEN 
											@fltDiscountValue
										ELSE 
											(CASE 
												WHEN @tintDiscountType = 2 -- Discount by amount
												THEN
													(CASE 
														WHEN T1.numUnits >= @numRemainingPromotion 
														THEN @numRemainingPromotion 
														ELSE T1.numUnits 
													END) * (CASE 
																WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																ELSE 
																	(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
															END)
												WHEN @tintDiscountType = 3 -- Discount by quantity
												THEN
													(CASE 
														WHEN @tintDiscoutBaseOn IN (5,6)
														THEN
															(T1.numUnits * (CASE 
																				WHEN @tintItemCalDiscount = 1 
																				THEN ISNULL(T1.monInsertUnitPrice,0) 
																				ELSE ISNULL(I.monListPrice,0) 
																			END)) - (CASE 
																						WHEN T1.numUnits >= @numRemainingPromotion 
																						THEN @numRemainingPromotion 
																						ELSE T1.numUnits 
																					END) * (CASE 
																								WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																								THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																								ELSE 
																									(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																							END)
														ELSE
															(CASE 
																WHEN T1.numUnits >= @numRemainingPromotion 
																THEN (@numRemainingPromotion * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
																ELSE (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
															END)
													END)
												ELSE 0
											END)
										END
									)
					,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
					,numPromotionID=@numTempPromotionID
					,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
					,vcPromotionDescription=@vcShortDesc
					,bitChanged=1
					,bitPromotionDiscount=1
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				WHERE
					ID=@i
					AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
					AND 1 = (CASE 
								WHEN @tintDiscoutBaseOn = 1 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
								THEN 
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 3 -- Related Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
								THEN
									(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 6 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END)
			END

			SET @i = @i + 1
		END

		SET @j = @j + 1
	END

	-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
	IF (SELECT 
			COUNT(*) 
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
	BEGIN
		UPDATE
			T1
		SET
			T1.numPromotionID = 0
			,T1.bitPromotionTriggered=0
			,T1.bitPromotionDiscount=0
			,T1.vcPromotionDescription=''
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
			,T1.fltDiscount=ISNULL(T1.fltInsertDiscount,0)
			,T1.bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			
			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1
			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE T1.numUnits 
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
															ELSE
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END
	END


	UPDATE
		CI
	SET
		monPrice=T1.monUnitPrice
		,fltDiscount=T1.fltDiscount
		,bitDiscountType=T1.bitDiscountType
		,monTotAmtBefDiscount = ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(T1.fltDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(T1.bitDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount >= 0
											THEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount
											ELSE 0
										END)
									ELSE ISNULL(CI.numUnitHour,0) * (ISNULL(T1.monUnitPrice,0) - (ISNULL(T1.monUnitPrice,0) * (T1.fltDiscount/100)))
								END)
							ELSE ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
						END)
		,PromotionID=T1.numPromotionID
		,bitPromotionTrigerred=T1.bitPromotionTriggered
		,PromotionDesc=T1.vcPromotionDescription
		,bitPromotionDiscount=T1.bitPromotionDiscount
	FROM
		CartItems CI
	INNER JOIN
		@Temp T1
	ON
		CI.numCartID = T1.numCartID
	WHERE
		T1.bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @l INT = 1
		DECLARE @lCount INT
		SET @lCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @tintOfferTriggerValueTypeRange TINYINT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @fltOfferTriggerValueRange FLOAT
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @monDiscountedItemPrice DECIMAL(20,5)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPUsedPromotion
		(
			ID
			,numPromotionID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
		FROM
			@TEMP T1
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
		GROUP BY
			numPromotionID

		DECLARE @j INT = 1
		DECLARE @jCount INT
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitPromotionDiscount=0
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPUsedPromotion 
			WHERE 
				ID = @j

			IF NOT EXISTS (SELECT 
								PO.numProId 
							FROM 
								PromotionOffer PO
							LEFT JOIN	
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							LEFT JOIN	
								DiscountCodes DC
							ON
								PO.numOrderPromotionID = DC.numPromotionID
							WHERE 
								PO.numDomainId=@numDomainID 
								AND PO.numProId=@numTempPromotionID
								AND ISNULL(PO.bitEnabled,0)=1 
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
											ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN
												(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											ELSE
												(CASE PO.tintCustomersBasedOn 
													WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
													WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
													WHEN 3 THEN 1
													ELSE 0
												END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN 
												(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
											ELSE 1 
										END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitPromotionDiscount=0
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		UPDATE
			T1
		SET 
			T1.bitPromotionDiscount = 0
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T2.monPrice,0)
			,T1.fltDiscount=ISNULL(T2.decDiscount,0)
			,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID=PO.numProId
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionDiscount,0) = 1
			AND ISNULL(PO.tintDiscountType,0) = 3

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			DECLARE @TableItemCouponPromotion TABLE
			(
				ID INT IDENTITY(1,1)
				,numPromotionID NUMERIC(18,0)
				,tintOfferTriggerValueType TINYINT
				,tintOfferTriggerValueTypeRange TINYINT
				,fltOfferTriggerValue FLOAT
				,fltOfferTriggerValueRange FLOAT
				,tintOfferBasedOn TINYINT
				,vcShortDesc VARCHAR(300)
			)

			INSERT INTO @TableItemCouponPromotion
			(
				numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc
			)
			SELECT
				PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,ISNULL(PO.vcShortDesc,'-')
			FROM
				PromotionOffer PO
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				PO.numDomainId = @numDomainID
				AND POOrder.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(POOrder.bitEnabled,0)=1 
				AND ISNULL(PO.bitUseOrderPromotion,0)=1 
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
			
			SET @l = 1
			DECLARE @k INT = 1
			DECLARE @kCount INT 
			SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

			WHILE @k <= @kCount
			BEGIN
				SELECT
					@numTempPromotionID=numPromotionID
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
					,@tintOfferBasedOn=tintOfferBasedOn
					,@vcShortDesc=vcShortDesc
				FROM
					@TableItemCouponPromotion
				WHERE
					ID=@k

				IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
				BEGIN
					IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						SET @i = 1

						WHILE @i <= @iCount
						BEGIN

							IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
							BEGIN
								UPDATE
									T1
								SET
									bitChanged = 1
									,numPromotionID=@numTempPromotionID
									,bitPromotionTriggered=1
									,vcPromotionDescription=ISNULL(@vcShortDesc,'')
								FROM
									@TEMP T1
								INNER JOIN
									Item I
								ON
									T1.numItemCode = I.numItemCode
								WHERE
									T1.ID = @i
									AND ISNULL(numPromotionID,0) = 0
									AND 1 = (CASE 
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
											END)
									AND 1 = (CASE 
												WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 4 THEN 1 
												ELSE 0
											END)
							END

							SET @i = @i + 1
						END

					END
				END

				SET @k = @k + 1
			END
		END

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			SET @numTempPromotionID = NULL

			SELECT
				@numTempPromotionID=numProId
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				WHERE
					PO.numDomainId=@numDomainID
					AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitUseOrderPromotion,0) = 0
					AND ISNULL(PO.bitRequireCouponCode,0) = 0
					AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

			IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @l = 1

					WHILE @l <= @lCount
					BEGIN
						
						IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @l
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @l = @l + 1
					END

				END
			END

			SET @i = @i + 1
		END
		

		DECLARE @TEMPPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN @numRemainingPromotion 
																	ELSE T1.numUnits 
																END) * (CASE 
																			WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																			THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																			ELSE 
																				(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																		END)
															ELSE
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
		IF (SELECT 
				COUNT(*) 
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
		BEGIN
			UPDATE
				T1
			SET
				T1.numPromotionID = 0
				,T1.bitPromotionTriggered=0
				,T1.bitPromotionDiscount=0
				,T1.vcPromotionDescription=''
				,T1.bitChanged=1
				,T1.monUnitPrice=ISNULL(T2.monPrice,0)
				,T1.fltDiscount=ISNULL(T2.decDiscount,0)
				,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			CROSS APPLY
				dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

			SET @j = 1
			SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numTempPromotionID=numPromotionID
					,@numItemCode=numTriggerItemCode
					,@numTempWarehouseItemID=numWarehouseItemID
					,@numUnits = numUnits
					,@monTotalAmount=monTotalAmount
				FROM 
					@TEMPPromotion 
				WHERE 
					ID = @j

				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							IF @tintDiscoutBaseOn IN (5,6)
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
							ELSE
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN @tintDiscoutBaseOn IN (5,6)
																THEN
																	(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE T1.numUnits 
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
																ELSE
																	(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN (@numRemainingPromotion * (CASE 
																											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																										END)) 
																		ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
																	END)
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
							,bitPromotionDiscount=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn IN (4,5) -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 6 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END

				SET @j = @j + 1
			END
		END
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskDataForGanttChart')
DROP PROCEDURE USP_GetTaskDataForGanttChart
GO
CREATE PROCEDURE [dbo].[USP_GetTaskDataForGanttChart]
@numDomainID as numeric(9)=0,    
@numOppId AS NUMERIC(18,0)=0,
@numProjectID AS NUMERIC(18,0)=0
as    
BEGIN   
SELECT 
	S.vcMileStoneName AS MileStoneName,
	S.vcStageName,
	T.vcTaskName,
	S.numStageDetailsId,
	T.numTaskId,
	CAST(S.tintPercentage As INT) AS tinProgressPercentage,
	T.bitSavedTask,
	T.bitTaskClosed,
	S.bitIsDueDaysUsed,
	T.numHours,
	CONVERT(VARCHAR, T.numHours / 8) AS TaskDays,
	CASE WHEN T.numAssignTo>0 THEN (SELECT TOP 1 vcFirstName+' '+vcLastName FROM AdditionalContactsInformation WHERE numContactId=T.numAssignTo) ELSE '-' END AS vcContactName,
	S.dtStartDate As StageStartDate,
	CAST(CASE WHEN S.bitIsDueDaysUsed=1 THEN DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE))
		ELSE DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),'_')),CAST(S.dtStartDate AS DATE)) END AS DATETIME) AS StageEndDate
FROM 
	StagePercentageDetails AS S,
	StagePercentageDetailsTask AS T
	LEFT JOIN ProjectProcessStageDetails AS PPD
	ON T.numStageDetailsId=PPD.numStageDetailsId AND (PPD.numOppId=@numOppId OR PPD.numProjectId=@numProjectID) AND (T.numOppId=@numOppId OR T.numProjectId=@numProjectID)
WHERE
	T.numStageDetailsId=S.numStageDetailsId AND
	1 =(CASE WHEN @numOppId>0 AND S.numOppID=@numOppId AND T.numOppID=@numOppId THEN 1 WHEN @numProjectID>0 AND S.numProjectID=@numProjectID AND T.numProjectID=@numProjectID THEN 1 ELSE 0 END) AND
	T.bitSavedTask=1 AND
	S.numDomainId=@numDomainID AND
	T.numDomainID=@numDomainID
ORDER BY
	S.vcMileStoneName,
	S.numStageOrder,
	T.numOrder
END
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserEmailTemplate_ComposeEmail')
DROP PROCEDURE USP_GetUserEmailTemplate_ComposeEmail
GO

--exec [USP_GetUserEmailTemplate_ComposeEmail] 189640,72,0,''
CREATE PROCEDURE [dbo].[USP_GetUserEmailTemplate_ComposeEmail] ( @numUserID NUMERIC(9) = 0,
                                                    @numDomainID NUMERIC(9) = 0,
                                                    @isMarketing AS BIT =0,
													@Screen As VARCHAR(20),
													@numGroupId AS NUMERIC(18)=0,
													@numCategoryId AS NUMERIC(18)=0)
AS 
DECLARE @dynamicQuery AS VARCHAR(MAX)=''
IF @isMarketing = 1 
    BEGIN
        SET @dynamicQuery='SELECT  vcDocName,
                numGenericDocID
        FROM    GenericDocuments
        WHERE   numDocCategory = 369
                --AND vcDocumentSection = ''M''
                AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''	
				AND numModuleID <> 8  --Exclude BizDoc Templates'
    END
    
IF @isMarketing=0 
BEGIN
	
	IF @Screen = 'Opp/Order'		 
		SET @dynamicQuery='SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType =1 --1 =generic,2=specific
						--AND ISNULL(vcDocumentSection,'''') <> ''M''
						AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
						AND numModuleID = 2 -- Opp/Order Templates'
	ELSE IF @Screen = 'BizDoc'
		SET @dynamicQuery='SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType =1 
						--AND ISNULL(vcDocumentSection,'''') <> ''M''
						AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
						AND  (numModuleID = 1 OR numModuleID = 45 OR numModuleID = 2 OR numModuleID = 8) '
	ELSE
		--AND  (numModuleID = 1 OR numModuleID = 45 or numModuleID = 0 or isnull(numModuleID, 0) =0)
		SET @dynamicQuery='SELECT  vcDocName,
					numGenericDocID
			FROM    GenericDocuments
			WHERE   numDocCategory = 369
					AND tintDocumentType =1 
					--AND ISNULL(vcDocumentSection,'''') <> ''M''
					AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
					 '
		
END
IF(@numCategoryId>0)
BEGIN
	SET @dynamicQuery=@dynamicQuery+' AND numCategoryId='+CAST(@numCategoryId AS VARCHAR)+' '
END
IF(@numGroupId>0)
BEGIN
	SET @dynamicQuery=@dynamicQuery+'  AND ('+CAST(@numGroupId AS VARCHAR)+' IN (SELECT Items FROM Split(ISNULL(vcGroupsPermission,''''),'','')) OR ISNULL(vcGroupsPermission,''0'')=''0'') '
END
PRINT @dynamicQuery
EXEC(@dynamicQuery)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetItemSelections')
DROP PROCEDURE USP_PromotionOffer_GetItemSelections
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetItemSelections]    
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@tintRecordType AS TINYINT,
	@tintItemSelectionType AS TINYINT
AS
BEGIN
	IF @tintItemSelectionType = 1
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID 
		FROM 
			Item   
		JOIN 
			PromotionOfferItems
		ON 
			numValue=numItemCode  
		WHERE 
			numProId=@numProId
			AND tintType = 1
			AND tintRecordType = @tintRecordType
	END
	ELSE IF @tintItemSelectionType = 4
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID 
		FROM 
			Item   
		JOIN 
			PromotionOfferItems
		ON 
			numValue=numItemCode  
		WHERE 
			numProId=@numProId
			AND tintType = 4
			AND tintRecordType = @tintRecordType
	END
	ELSE IF @tintItemSelectionType = 2
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,dbo.[GetListIemName](numValue) ItemClassification,
			(SELECT COUNT(*) FROM Item I1 WHERE I1.numItemClassification=numValue) AS ItemsCount 
		FROM 
			PromotionOfferItems   
		WHERE 
			numProId=@numProId
			AND tintType = 2
			AND tintRecordType = @tintRecordType
	END
END 
GO
