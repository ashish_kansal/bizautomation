/******************************************************************
Project: Release 14.8 Date: 26.JAN.2021
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

ALTER TABLE Domain ADD bitInventoryInvoicing BIT DEFAULT 0
ALTER TABLE ShippingServiceTypes ADD vcItemClassification VARCHAR(1000)
ALTER TABLE ShippingRules ADD bitItemClassification BIT

/******************************************** SANDEEP *********************************************/

ALTER TABLE Category ADD numItemGroup NUMERIC(18,0)

--------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[CategoryMatrixGroup]    Script Date: 26-Jan-21 1:50:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CategoryMatrixGroup](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCategoryID] [numeric](18, 0) NOT NULL,
	[numItemGroup] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_CategoryMatrixGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CategoryMatrixGroup]  WITH CHECK ADD  CONSTRAINT [FK_Category_CategoryMatrixGroup] FOREIGN KEY([numCategoryID])
REFERENCES [dbo].[Category] ([numCategoryID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CategoryMatrixGroup] CHECK CONSTRAINT [FK_Category_CategoryMatrixGroup]
GO


