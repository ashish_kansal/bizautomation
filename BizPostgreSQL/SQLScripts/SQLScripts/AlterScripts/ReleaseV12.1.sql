/******************************************************************
Project: Release 12.1 Date: 13.JUL.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),10,122,'Sales Fulfillment 2.0','../opportunity/frmMassSalesFulfillment.aspx',1,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		1,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


-------------------------------------------

ALTER TABLE OpportunityMaster ADD dtExpectedDate DATE
ALTER TABLE OpportunityItems ADD numQtyPicked FLOAT
ALTER TABLE OpportunityItems ADD vcInclusionDetail VARCHAR(MAX)
ALTER TABLE ShippingReport ADD bitDelivered BIT DEFAULT 0
ALTER TABLE ShippingReport ADD vcTrackingDetail VARCHAR(300)
ALTER TABLE ShippingReport ADD dtLastTrackingUpdate DATETIME
--------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitAllowEdit,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
)
VALUES
(
	3,'Tracking Detail','vcTrackingDetail','vcTrackingDetail','TrackingDetail','ShippingReport','V','R','Label','',0,1,1,1,1,0,0,0,1,1,0,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,39,0,0,'Tracking Detail','Label','TrackingDetail',1,1,1,1,0,0,1,0,1,0,0,0
)

--------------------------

UPDATE 
	OI
SET
	OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
FROM 
	OpportunityItems OI
INNER JOIN
	Item I
ON
	OI.numItemCode = I.numItemCode
WHERE
	ISNULL(I.bitKitParent,0) = 1

--------------------------

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	141,'Mass Sales Fulfillment Left Configuration','Y','N',0,0
),
(
	142,'Mass Sales Fulfillment Right Configuration','Y','N',0,0
)

--------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Age','numAge','numAge','OrderAge','OpportunityMaster','N','R','Label',0,1,0,0,0,1,0,1,1,1
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Age','Label',1,1,1,1,0,0,1,1,0
)

--------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Anticipated Delivery','dtAnticipatedDelivery','dtAnticipatedDelivery','AnticipatedDelivery','OpportunityBizDocs','V','R','Label',0,1,0,0,0,1,0,1,1,1
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Anticipated Delivery','Label',2,1,2,1,0,0,1,0,0
)

--------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,251,141,0,0,'Item Name','TextBox',3,1,3,1,0,1,1,1,1
),
(
	4,273,141,0,0,'Classification','SelectBox',4,1,4,1,0,0,1,1,1
),
(
	4,253,141,0,0,'Description','TextBox',5,1,5,1,0,0,1,0,0
),
(
	4,350,141,0,0,'Attributes','Label',6,1,6,1,0,0,1,0,0
),
(
	4,281,141,0,0,'SKU','TextBox',7,1,7,1,0,1,1,1,1
)
,
(
	4,203,141,0,0,'UPC','TextBox',8,1,8,1,0,0,1,1,1
)

-------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcNotes')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Item Notes','TextBox',9,1,9,1,0,0,1,0,0
)


------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInclusionDetails')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Inclusion Details','Label',10,1,10,1,0,0,1,0,0
)

-------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,294,141,0,0,'Item Type','Label',11,1,11,1,0,0,1,1,0
)


--------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Ship Status','vcShipStatus','vcShipStatus','ShipStatus','OpportunityBizDocs','V','R','Label',0,1,0,0,0,1,0,1,1,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Ship Status','Label',12,1,12,1,0,0,1,0,0
)

-----------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,233,141,0,0,'Location','TextBox',13,1,13,1,0,0,1,1,1
)

--------------------------
-- DON'T EXECUTE ON SERVER
--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Left-to','numLeftTo','numLeftTo','LeftTo','OpportunityItems','N','R','Label',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Left-to','Label',14,1,14,1,0,0,1,0,0
--)

-------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,3,141,0,0,'Organization','TextBox',15,1,15,1,0,0,1,1,1
),
(
	3,96,141,0,0,'Order Name','TextBox',16,1,16,1,0,1,1,1,1
),
(
	3,101,141,0,0,'Order status','SelectBox',17,1,17,1,0,0,1,1,0
)
,
(
	3,122,141,0,0,'Comments','TextBox',18,1,18,1,0,0,1,0,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcItemReleaseDate')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Release','DateField',19,1,19,1,0,0,1,1,0
)

-------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Expected Date','dtExpectedDate','dtExpectedDate','ExpectedDate','OpportunityMaster','V','R','DateField',0,1,0,0,0,1,0,1,1,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,1,1,'Expected Date','DateField',20,1,20,1,0,1,1,1,1
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitDetailField
)
VALUES
(
	3,@numFieldID,39,1,1,'Expected Date','DateField',20,1,20,1,0,1,1,1,1,1
)



------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,269,141,0,0,'Amt Paid','Label',21,1,21,1,0,0,1,1,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Remaining','Label',22,1,22,1,0,0,1,1,0
)

-------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Picked','numQtyPicked','numQtyPicked','QtyPicked','OpportunityItems','V','R','Label',0,1,0,0,0,1,0,1,1,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Picked','Label',23,1,23,1,0,0,1,1,0
)

-------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Packed','numQtyPacked','numQtyPacked','QtyPacked','OpportunityItems','V','R','Label',0,1,0,0,0,1,0,1,1,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Packed','Label',24,1,24,1,0,0,1,1,0
)


----------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,255,141,0,0,'Shipped','Label',25,1,25,1,0,0,1,1,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInvoiced')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Invoiced','Label',26,1,26,1,0,0,1,1,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bitPaidInFull')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Paid','Label',27,1,27,1,0,0,1,1,0
)

------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,258,141,0,0,'Ordered','TextBox',28,1,28,1,0,0,1,1,0
)

--------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Payment Status','vcPaymentStatus','vcPaymentStatus','PaymentStatus','OpportunityMaster','V','R','Label',0,1,0,0,0,1,0,1,1,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Payment Status','Label',29,1,29,1,0,0,1,0,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Ship Via, Service','Label',30,1,30,1,0,0,1,0,0
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Prefered Ship Via, Service','Label',31,1,31,1,0,0,1,0,0
)


----------------------------------------


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,119,141,0,0,'Created','DateField',32,1,32,1,0,0,1,1,1
)


-------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Ship Rate','numShipRate','numShipRate','ShipRate','OpportunityMaster','V','R','Label',0,1,0,0,0,1,0,0,0,0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Ship Rate','Label',33,1,33,1,0,0,1,0,0
)

------------------------------


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,248,141,0,0,'Invoice','Label',34,1,34,1,0,0,1,1,1
)


------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,351,142,0,0,'Image','TextBox',1,1,1,1,0,1,1,0,0
)
-------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInclusionDetails')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,142,0,0,'Inclusion Details','Label',2,1,2,1,0,0,1,0,0
)

----------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,251,142,0,0,'Item Name','TextBox',3,1,3,1,0,1,1,1,1
),
(
	3,96,142,0,0,'Order Name','TextBox',4,1,4,1,0,0,1,1,1
),
(
	3,253,142,0,0,'Description','TextBox',5,1,5,1,0,0,1,0,0
),
(
	3,350,142,0,0,'Attributes','Label',6,1,6,1,0,0,1,0,0
),
(
	3,281,142,0,0,'SKU','Label',7,1,7,1,0,1,1,1,1
),
(
	3,203,142,0,0,'UPC','Label',8,1,8,1,0,0,1,1,1
),
(
	3,233,142,0,0,'Location','Label',9,1,9,1,0,0,1,1,1
)


------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
)
VALUES
(
	3,@numFieldID,142,0,0,'Remaining','Label',10,1,10,1,0,1,1,1,0,1
)

------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numQtyPicked')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
)
VALUES
(
	3,@numFieldID,142,0,0,'Picked','Label',11,1,11,1,0,1,1,1,0,1
)


-------------------------------------------------------------

UPDATE DycFormField_Mapping SET bitAllowFiltering=0 WHERE numFormID=141 AND numFieldID=273

------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentConfiguration]    Script Date: 03-Jun-19 9:10:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentConfiguration](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[bitGroupByOrderForPick] [bit] NOT NULL,
	[bitGroupByOrderForShip] [bit] NOT NULL,
	[bitGroupByOrderForInvoice] [bit] NOT NULL,
	[bitGroupByOrderForPay] [bit] NOT NULL,
	[bitGroupByOrderForClose] [bit] NOT NULL,
	[tintInvoicingType] [tinyint] NOT NULL,
	[tintScanValue] [tinyint] NOT NULL,
	[tintPackingMode] [tinyint] NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

----------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentBatch]    Script Date: 13-Jul-19 5:31:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentBatch](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[vcName] [varchar](100) NOT NULL,
	[bitEnabled] [bit] NOT NULL CONSTRAINT [DF_MassSalesFulfillmentBatch_bitEnabled]  DEFAULT ((1)),
 CONSTRAINT [PK_MassSalesFulfillmentBatch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


-----------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentBatchOrders]    Script Date: 13-Jul-19 5:32:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentBatchOrders](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBatchID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[numOppItemID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentBatchOrders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




-----------------------------------
-- Below is commission changes
------------------------------------
UPDATE UserMaster SET bitPayroll = 1
UPDATE PageMaster SET vcPageDesc='Payroll Expenses' WHERE vcPageDesc = 'Payroll Expences'

ALTER TABLE Domain ADD bitCommissionBasedOn BIT DEFAULT 0
ALTER TABLE Domain ADD tintCommissionBasedOn TINYINT DEFAULT 1

ALTER TABLE BizDocComission ADD numOppItemID NUMERIC(18,0)
ALTER TABLE BizDocComission ADD monInvoiceSubTotal DECIMAL(20,5)
ALTER TABLE BizDocComission ADD monOrderSubTotal DECIMAL(20,5)

ALTER TABLE PayrollHeader ADD numComPayPeriodID NUMERIC(18,0)

ALTER TABLE PayrollDetail ADD monHourlyAmt DECIMAL(20,5)
ALTER TABLE PayrollDetail ADD monAdditionalAmt DECIMAL(20,5)
ALTER TABLE PayrollDetail ADD monSalesReturn DECIMAL(20,5)
ALTER TABLE PayrollDetail ADD monOverPayment DECIMAL(20,5)
ALTER TABLE PayrollDetail ADD numDivisionID NUMERIC(18,0)

ALTER TABLE BizDocComission ADD monCommissionPaid DECIMAL(20,5) DEFAULT 0
ALTER TABLE BizDocComission ADD bitNewImplementation BIT DEFAULT 1

ALTER TABLE BizDocComission ADD bitDomainCommissionBasedOn BIT
ALTER TABLE BizDocComission ADD tintDomainCommissionBasedOn TINYINT

UPDATE
	BDC
SET
	BDC.numOppItemID = OpportunityBizDocItems.numOppItemID
FROM
	BizDocComission BDC
INNER JOIN
	OpportunityBizDocItems 
ON
	BDC.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID

---------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[TimeAndExpenseCommission]    Script Date: 20-May-19 12:19:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TimeAndExpenseCommission](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
	[numTimeAndExpenseID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[dtFromDate] [datetime] NOT NULL,
	[dtToDate] [datetime] NOT NULL,
	[numType] [tinyint] NOT NULL,
	[numCategory] [tinyint] NOT NULL,
	[bitReimburse] [bit] NOT NULL,
	[numHours] [float] NOT NULL,
	[monHourlyRate] [decimal](20, 5) NOT NULL,
	[monTotalAmount] [decimal](20, 5) NOT NULL,
	[bitCommissionPaid] [bit] NOT NULL,
	[monAmountPaid] [decimal](20, 5) NULL CONSTRAINT [DF__TimeAndEx__monAm__391F47BF]  DEFAULT ((0)),
 CONSTRAINT [PK_TimeAndExpenseCommission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[StagePercentageDetailsTaskCommission]    Script Date: 20-May-19 12:19:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagePercentageDetailsTaskCommission](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
	[numTaskID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numHours] [float] NOT NULL,
	[monHourlyRate] [decimal](20, 5) NOT NULL,
	[monTotalAmount] [decimal](20, 5) NOT NULL,
	[bitCommissionPaid] [bit] NOT NULL,
	[monAmountPaid] [decimal](20, 5) NOT NULL,
 CONSTRAINT [PK_StagePercentageDetailsTaskCommission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[SalesReturnCommission]    Script Date: 20-May-19 12:19:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SalesReturnCommission](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[tintAssignTo] [tinyint] NOT NULL,
	[numReturnHeaderID] [numeric](18, 0) NOT NULL,
	[numReturnItemID] [numeric](18, 0) NOT NULL,
	[tintComType] [tinyint] NOT NULL,
	[tintComBasedOn] [tinyint] NOT NULL,
	[decCommission] [decimal](18, 2) NOT NULL,
	[monCommissionReversed] [decimal](20, 5) NOT NULL,
	[bitCommissionReversed] [bit] NOT NULL,
 CONSTRAINT [PK_SalesReturnCommission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[BizDocComissionPaymentDifference]    Script Date: 13-Jul-19 12:48:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BizDocComissionPaymentDifference](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numComissionID] [numeric](18, 0) NOT NULL,
	[tintReason] [tinyint] NOT NULL,
	[vcReason] [varchar](100) NOT NULL,
	[monDifference] [decimal](20, 5) NOT NULL,
	[bitDifferencePaid] [bit] NOT NULL,
	[numComPayPeriodID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BizDocComissionPaymentDifference] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO





/******************************************** PRASANT *********************************************/


ALTER TABLE DivisionMaster ADD bitAutoCheckCustomerPart BIT DEFAULT 0
INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
149,37,'','Save Inventory',0,0,1,0,0,'',0
)

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	4,'Counted','StockQtyCount','StockQtyCount','Item','V','R','Label','',0,1,0,0,0,1,0,0,0
)
SELECT @numFieldID = SCOPE_IDENTITY()

SELECT @numFormFieldID = NULL

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,21,0,0,'Counted','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

-----------------------------------------
BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	4,'Qty to Adjust','StockQtyAdjust','StockQtyAdjust','Item','V','R','Label','',0,1,0,0,0,1,0,0,0
)
SELECT @numFieldID = SCOPE_IDENTITY()

SELECT @numFormFieldID = NULL

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,21,0,0,'Qty to Adjust','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

-----------------------------------------

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	4,'Internal Location','vcLocation','vcLocation','WareHouseItems','V','R','Label','',0,1,0,0,0,1,0,0,0
)
SELECT @numFieldID = SCOPE_IDENTITY()

SELECT @numFormFieldID = NULL

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,21,0,0,'Internal Location','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
-----------------------------------------------------------01/07/2019---------------------
GO
CREATE NONCLUSTERED INDEX NONCLUSTER_DIVISONID_CONTACTID
ON [dbo].[OpportunityMaster] ([numDomainId])
INCLUDE ([numContactId],[numDivisionId])
GO


BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	6,'To','vcTo','vcTo','EmailHistory','V','R','TextArea','',0,1,0,0,0,1,0,0,1
)
SELECT @numFieldID = SCOPE_IDENTITY()
--Lead Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(44,'To','R','TextArea','vcTo',0,0,'V','vcTo',0,'EmailHistory',0,7,1,'vcTo',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(6,@numFieldID,44,0,0,'To','TextArea',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
ALTER TABLE InboxTree ADD numLastUid NUMERIC(18,0) DEFAULT 0
ALTER TABLE InboxTree ADD numOldEmailLastUid NUMERIC(18,0) DEFAULT 0
ALTER TABLE InboxTreeSort ADD numLastUid NUMERIC(18,0) DEFAULT 0
ALTER TABLE InboxTreeSort ADD numOldEmailLastUid NUMERIC(18,0) DEFAULT 0


UPDATE DycFormField_Mapping SET bitAllowEdit=1 WHERE vcFieldName='Follow-up Status' AND numFormID=43
UPDATE DycFieldMaster SET vcPropertyName='vcCompactContactDetails' WHERE vcDbColumnName='vcCompactContactDetails'