/******************************************************************
Project: Release 13.0 Date: 02.DEC.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_GetAvailableFields' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_GetAvailableFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 July 2014
-- Description:	Gets list of available fields of form
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_GetAvailableFields
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@pageID NUMERIC(18,0),
	@bitGridConfiguration BIT = 0,
	@numFormFieldGroupId NUMERIC(18,0)=0
AS
BEGIN
	SET NOCOUNT ON;

	IF @bitGridConfiguration = 1 -- Retrive Available Fields For Grids Columns Configuration
	BEGIN           
		IF @PageId = 1 OR @PageId = 4
		BEGIN          
			SELECT  
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 boolRequired,
				0 numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
				0 boolAOIField,
				numFormID,
				vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    View_DynamicDefaultColumns
			WHERE   numFormID = @numFormID
					AND ISNULL(bitSettingField, 0) = 1
					AND ISNULL(bitDeleted, 0) = 0
					AND numDomainID = @numDomainID
					AND numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									bitCustom = 0 AND
									bitGridConfiguration = 1 )
			UNION
			SELECT  
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFW_Fld_Master.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    CFW_Fld_Master
					LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
												AND numRelation = CASE
																WHEN @numRelCntType IN (
																1, 2, 3 )
																THEN numRelation
																ELSE @numRelCntType
																END
					LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
			WHERE   CFW_Fld_Master.grp_id = @PageId
					AND CFW_Fld_Master.numDomainID = @numDomainID
					AND Fld_type <> 'Link'
					AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										bitCustom = 1 AND
										bitGridConfiguration = 1)
			ORDER BY 
					vcFieldName                                       
		END  
		ELSE
		BEGIN
			SELECT  
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 boolRequired,
				0 numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
				0 boolAOIField,
				numFormID,
				vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    View_DynamicDefaultColumns
			WHERE   numFormID = @numFormID
					AND ISNULL(bitSettingField, 0) = 1
					AND ISNULL(bitDeleted, 0) = 0
					AND numDomainID = @numDomainID
					AND numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									bitCustom = 0 AND
									bitGridConfiguration = 1 )
			UNION
			SELECT  
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFW_Fld_Master.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    CFW_Fld_Master
					LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
			WHERE   CFW_Fld_Master.grp_id = @PageId
					AND CFW_Fld_Master.numDomainID = @numDomainID
					AND Fld_type <> 'Link'
					AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										bitCustom = 1 AND
										bitGridConfiguration = 1)
			ORDER BY 
					vcFieldName                   
		END
	END
	ELSE --Retrive Available Fields For Detail(Layout) Form
	BEGIN
		SELECT 
			vcFieldName AS vcNewFormFieldName,
			vcFieldName AS vcFormFieldName,
			CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
			'R' as vcFieldType,
			vcAssociatedControlType,
			numListID,
			vcDbColumnName,
			vcListItemType,
			0 intColumnNum,
			0 intRowNum,
			0 boolRequired,
			0 numAuthGroupID,
			CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
			0 boolAOIField,
			numFormID,
			vcLookBackTableName,
			CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
			0 AS bitDefaultMandatory,
			ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@numFormID AND 
			numDomainID=@numDomainID  AND
			1=(CASE 
					WHEN @tintPageType=2 THEN 
					CASE 
						WHEN ISNULL(bitAddField,0)= 1 THEN 1 
						ELSE 0 
					END 
					WHEN @tintPageType=3 THEN 
					CASE 
						WHEN ISNULL(bitDetailField,0)=1 THEN 1 
						ELSE 0 
					END
					ELSE 
						0 
				END) AND 
			numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									tintPageType = @tintPageType AND
									bitCustom = 0 AND
									bitGridConfiguration = 0 )
		ORDER BY
			vcNewFormFieldName

		IF @pageID=4 OR @pageID=1  OR @pageID =12 OR @pageID =13  OR @pageID =14                
		BEGIN           
			SELECT 
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFM.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFM.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFM.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM 
				CFW_Fld_Master CFM 
			JOIN 
				CFW_Fld_Dtl CFD 
			ON 
				Fld_id=numFieldId 
			LEFT JOIN 
				CFw_Grp_Master 
			ON 
				subgrp=CFM.Grp_id                              
			WHERE 
				CFM.grp_id=@pageID AND 
				CFD.numRelation=@numRelCntType AND 
				CFM.numDomainID=@numDomainID AND 
				subgrp=0 
				AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										tintPageType = @tintPageType AND
										bitCustom = 1 AND
										bitGridConfiguration = 0)
			ORDER BY 
				vcNewFormFieldName,
				subgrp,
				numOrder
		END        
      
		IF @pageID= 2 or  @pageID= 3 or @pageID= 5 or @pageID= 6 or @pageID= 7 or @pageID= 8   or @pageID= 11                
		BEGIN 
			SELECT 
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFM.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFM.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFM.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM 
				CFW_Fld_Master CFM 
			LEFT JOIN 
				CFw_Grp_Master 
			ON 
				subgrp=CFM.Grp_id                              
			WHERE 
				CFM.grp_id=@pageID AND 
				CFM.numDomainID=@numDomainID AND 
				subgrp=0 AND 
				fld_id NOT IN (
								SELECT 
										numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									tintPageType = @tintPageType AND
									bitCustom = 1 AND
									bitGridConfiguration = 0)
			ORDER BY 
				vcNewFormFieldName,
				subgrp                   
		END       
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_GetSelectedFields' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_GetSelectedFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 July 2014
-- Description:	Gets list of available fields of form
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_GetSelectedFields
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@tintColumn INT,
	@bitGridConfiguration BIT = 0,
	@numFormFieldGroupId NUMERIC(18,0)=0
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		(
			SELECT 
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcNewFormFieldName,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcFormFieldName,
				'R' as vcFieldType,
                vcAssociatedControlType,
                numListID,
                vcDbColumnName,
                vcListItemType,
                intColumnNum intColumnNum,
				intRowNum intRowNum,
                0 as boolRequired,
                ISNULL(numAuthGroupID, 0) numAuthGroupID,
                CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                0 boolAOIField,
                numFormID,
                vcLookBackTableName,
                CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + '0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(numFormFieldGroupId,0) AS numFormGroupId 
			FROM
				View_DynamicColumnsMasterConfig
			WHERE
				numFormId = @numFormID AND
				numDomainID = @numDomainID AND
				numAuthGroupID = @numGroupID AND
				numRelCntType = @numRelCntType AND
				tintPageType = @tintPageType AND
				intColumnNum = @tintColumn AND
				bitCustom = 0 AND
				bitGridConfiguration = @bitGridConfiguration AND 
				ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)
			UNION
			SELECT 
				CONVERT(VARCHAR(15),numFieldId) + 'C' AS numFormFieldId,
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				'C' as vcFieldType,
				vcFieldType as vcAssociatedControlType,
				ISNULL(numListID, 0) numListID,
				vcDbColumnName,
				CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				intColumnNum intColumnNum,
				intRowNum intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(numFormFieldGroupId,0) AS numFormGroupId
			FROM
				View_DynamicCustomColumnsMasterConfig 
			WHERE
				numFormId = @numFormID AND
				numDomainID = @numDomainID AND
				numAuthGroupID = @numGroupID AND
				numRelCntType = @numRelCntType AND
				tintPageType = @tintPageType AND
				intColumnNum = @tintColumn AND
				bitCustom = 1 AND
				bitGridConfiguration = @bitGridConfiguration AND 
				ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)
				
		) AS TEMPFINAL
	ORDER BY
		TEMPFINAL.intRowNum
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_Save' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 25 July 2014
-- Description: Saves form configuration
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_Save
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@pageID NUMERIC(18,0),
	@bitGridConfiguration BIT = 0,
	@strFomFld as TEXT,
	@numFormFieldGroupId AS NUMERIC(18,0)=0
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM 
		BizFormWizardMasterConfiguration 
	WHERE 
		numDomainID = @numDomainID AND
		numFormId = @numFormId AND 
		numGroupID = @numGroupID AND
		numRelCntType = @numRelCntType AND
		tintPageType = @tintPageType AND
		bitGridConfiguration = @bitGridConfiguration AND
		ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)

	DELETE FROM 
		DycFormConfigurationDetails 
	WHERE 
		numFormId=@numFormId 
		AND numDomainID=@numDomainID 
		AND numUserCntID IN (SELECT numUserDetailID FROM UserMaster WHERE numDomainID =  @numDomainID AND numGroupID = @numGroupID)
		AND ISNULL(numRelCntType,0) = ISNULL(@numRelCntType,0)
		AND ISNULL(tintPageType,0) = ISNULL(@tintPageType,0)

    DECLARE @hDoc1 int                                                        
	EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strFomFld                                                        
			
	INSERT INTO BizFormWizardMasterConfiguration
		(
			numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId
		)
	SELECT
        @numFormId,
		replace(replace(X.numFormFieldId,'R',''),'C',''),                                                        
		X.intColumnNum,                                
		X.intRowNum,
		@numDomainId,
		@numGroupID,
		@numRelCntType,
		@tintPageType,
		(
		CASE 
			WHEN CHARINDEX('C',X.numFormFieldId) > 0 THEN 1 
			ELSE 0 
		END
		),
		@bitGridConfiguration,
		@numFormFieldGroupId
	FROM
		(
			SELECT 
				* 
			FROM 
				OPENXML (@hDoc1,'/FormFields/FormField',2)                                                        
			WITH 
				(numFormFieldId varchar(20),                                                        
				vcFieldType char(1),                                                        
				intColumnNum int,                                
				intRowNum int,
				boolAOIField bit
				)
		)X      
	
	EXEC sp_xml_removedocument @hDoc1
	
END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardModule_GetAll' ) 
    DROP PROCEDURE USP_BizFormWizardModule_GetAll
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 July 2014
-- Description:	Gets list of bizform wizard modules
-- =============================================
CREATE PROCEDURE USP_BizFormWizardModule_GetAll
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM ListDetails WHERE numListID = 5 AND numDomainID = @numDomainID AND constFlag <> 1) > 0
		SELECT * FROM BizFormWizardModule ORDER BY tintSortOrder
	ELSE
		SELECT * FROM BizFormWizardModule ORDER BY tintSortOrder	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Cases_ChangeOrganization')
DROP PROCEDURE USP_Cases_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_Cases_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numCaseId NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM Cases WHERE numDomainId=@numDomainID AND numCaseId = @numCaseId

	BEGIN TRY
	BEGIN TRANSACTION
		UPDATE 
			Cases 
		SET 
			numDivisionId=@numNewDivisionID
			,numContactId=@numNewContactID
		WHERE 
			numDomainId=@numDomainID 
			AND numCaseId = @numCaseId
		
		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numCaseID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numCaseId,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Cases_GetFieldValue')
DROP PROCEDURE USP_Cases_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_Cases_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numCaseId NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM Cases WHERE numDomainId=@numDomainID AND numCaseId=@numCaseId
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChartOfAccounts_SaveAccountTypeOrder')
DROP PROCEDURE USP_ChartOfAccounts_SaveAccountTypeOrder
GO
CREATE PROCEDURE [dbo].[USP_ChartOfAccounts_SaveAccountTypeOrder]    
(
	@numDomainID NUMERIC(18,0)
	,@vcAccountTypes VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @hDocItem int

	IF LEN(ISNULL(@vcAccountTypes,'')) > 0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcAccountTypes

		UPDATE
			ATD
		SET
			ATD.tintSortOrder = T1.tintSortOrder
		FROM
			AccountTypeDetail ATD
		INNER JOIN
		(
			SELECT
				numAccountTypeID,
				tintSortOrder
			FROM
				OPENXML (@hDocItem,'/NewDataSet/AccountType',2)
			WITH
			(
				numAccountTypeID NUMERIC(18,0),
				tintSortOrder INT
			)
		) AS T1
		ON 
			ATD.numAccountTypeID = T1.numAccountTypeID
		WHERE
			ATD.numDomainID = @numDomainID

		EXEC sp_xml_removedocument @hDocItem 
	END

	
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartofChildAccounts' ) 
    DROP PROCEDURE USP_ChartofChildAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE PROCEDURE [dbo].[USP_ChartofChildAccounts]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0,
    @strSortOn AS VARCHAR(11) = NULL,
    @strSortDirection AS VARCHAR(10) = NULL
AS 
    BEGIN    
		
		-- GETTING P&L VALUE
		SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId ;

		DECLARE @dtFinYearFrom DATETIME ;
		DECLARE @dtFinYearTo DATETIME ;
		SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= GETDATE()
                                AND dtPeriodTo >= GETDATE()
                                AND numDomainId = @numDomainId
                        ) ;
		PRINT @dtFinYearFrom
		SELECT @dtFinYearTo = MAX(datEntry_Date) FROM #view_journal WHERE numDomainID=@numDomainId
		PRINT @dtFinYearTo

		CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

		INSERT INTO  #PLSummary
		SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
		0 AS OPENING,
		ISNULL((SELECT sum(Debit) FROM #view_journal VJ
		WHERE VJ.numDomainId=@numDomainId AND
			VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
			datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo ),0) as DEBIT,

		ISNULL((SELECT sum(Credit) FROM #view_journal VJ
		WHERE VJ.numDomainId=@numDomainId AND
			VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
			datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo ),0) as CREDIT,
			ISNULL(COA.bitIsSubAccount,0)
		FROM Chart_of_Accounts COA
		WHERE COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND 
			  (COA.vcAccountCode LIKE '0103%' OR
			   COA.vcAccountCode LIKE '0104%' OR
			   COA.vcAccountCode LIKE '0106%')  ;


		CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));

		INSERT INTO #PLOutPut
		SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
		 ISNULL(SUM(Opening),0) as Opening,
		ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
		FROM 
		 AccountTypeDetail ATD RIGHT OUTER JOIN 
		#PLSummary PL ON
		PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
		AND ATD.numDomainId=@numDomainId AND
		(ATD.vcAccountCode LIKE '0103%' OR
			   ATD.vcAccountCode LIKE '0104%' OR
			   ATD.vcAccountCode LIKE '0106%')
		WHERE 
		PL.bitIsSubAccount=0
		GROUP BY 
		ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


		DECLARE @CURRENTPL DECIMAL(20,5) ;
		DECLARE @PLOPENING DECIMAL(20,5);
		DECLARE @PLCHARTID NUMERIC(8)
		DECLARE @TotalIncome DECIMAL(20,5);
		DECLARE @TotalExpense DECIMAL(20,5);
		DECLARE @TotalCOGS DECIMAL(20,5);
		DECLARE  @CurrentPL_COA DECIMAL(20,5)
		SET @CURRENTPL =0;	
		SET @PLOPENING=0;

		SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
		bitProfitLoss=1;

		SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFinYearFrom AND @dtFinYearTo;

		SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0103')

		SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0104')

		SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0106')

		PRINT 'CurrentPL_COA = ' + CAST(@CurrentPL_COA AS VARCHAR(20))
		PRINT 'TotalIncome = ' + CAST(@TotalIncome AS VARCHAR(20))
		PRINT 'TotalExpense = ' + CAST(@TotalExpense AS VARCHAR(20))
		PRINT 'TotalCOGS = ' + CAST(@TotalCOGS AS VARCHAR(20))

		SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
		PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

		SET @CURRENTPL=@CURRENTPL * (-1)

		SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
		AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
		AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SET @CURRENTPL=@CURRENTPL * (-1)

		PRINT '@PLOPENING = ' + CAST(@PLOPENING AS VARCHAR(20))
		PRINT '@CURRENTPL = ' + CAST(@CURRENTPL AS VARCHAR(20))
		PRINT '-(@PLOPENING + @CURRENTPL) = ' + CAST((@PLOPENING + @CURRENTPL) AS VARCHAR(20))

        IF ( @strSortOn = 'ID'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  

                SELECT  c.numAccountId,
                        [numAccountTypeID],
                        c.[numParntAcntTypeId],
                        c.[vcAccountCode],
                        ISNULL(c.[vcAccountCode], '') + ' ~ '
                        + c.vcAccountName AS vcAccountName,
                        c.vcAccountName AS vcAccountName1,
                        AT.[vcAccountType],
                        CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
							 WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
							 ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
								   WHERE COA.numDomainId=@numDomainId 
								   AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0)))
						END AS [numOpeningBal],	 
                        ISNULL(c.numParentAccId,0) AS [numParentAccId],
                        COA.vcAccountName AS [vcParentAccountName],
                        (SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
                        ISNULL(c.intLevel,1) AS [intLevel]
						,ISNULL(c.bitActive,0) bitActive
                FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainId
                --AND c.bitActive = 0
                ORDER BY numAccountID ASC  
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)	
									   ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                        
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                --AND c.bitActive = 0
                ORDER BY c.numAccountID ASC
            END  
  
        IF ( @strSortOn = 'DESCRIPTION'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  
                
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								       ELSE (SELECT SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                      
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1

                WHERE   c.numDomainId = @numDomainId
                    AND c.[numParntAcntTypeId] IS NULL
                    --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE  CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									    WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
										ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA WHERE COA.numDomainId=@numDomainId AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                        AND c.[numParntAcntTypeId] IS NOT NULL
                        --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
                
            END  
    END
	
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteDomain')
DROP PROCEDURE USP_DeleteDomain
GO
-- exec USP_DeleteDomain 82
-- exec USP_DeleteDomain 153
-- 72,97,103,104,107,110
CREATE PROCEDURE USP_DeleteDomain
@numDomainID NUMERIC(9)
AS 
BEGIN
IF @numDomainID=1
BEGIN
	raiserror('Verry funny.. can''t delete yourself',16,1);
	RETURN ;
END

/*Allow deletion only for suspended subscription*/
IF exists (SELECT * FROM [Subscribers] WHERE bitActive = 1 AND numTargetDomainID = @numDomainID)
BEGIN
	raiserror('Can not DELETE active subscription',16,1);
	RETURN ;
END

BEGIN TRY 
	BEGIN TRANSACTION

			--DELETE FROM [OpportunityAddress] WHERE [numDomainId]=@numDomainID;

			DELETE FROM [General_Journal_Details] WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId = @numDomainID)
			DELETE FROM [General_Journal_Header] WHERE [numDomainId]=@numDomainID;


			--Project Management
			---------------------------------------------------------
			DELETE FROM ProjectProgress WHERE numDomainId=@numDomainID
			 DELETE FROM [ProjectsOpportunities] WHERE [numDomainId]=@numDomainID
			 delete RECENTITEMS where numRecordID IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID) and chrRecordType='P'       
			 delete from ProjectsStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsSubStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)              
			 delete from TimeAndExpense where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsDependency where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsContacts where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)         
			 delete from ProjectsMaster WHERE numDomainID = @numDomainID  


			--Case Management
			---------------------------------------------------------
			 DELETE FROM [CaseOpportunities] WHERE [numDomainId] =@numDomainID
			 delete recentItems where numRecordID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and chrRecordType = 'S'
			 delete from TimeAndExpense where  numCaseId IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and numDomainID=   @numDomainID  
			 delete from CaseContacts where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 delete from CaseSolutions where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 DELETE from Cases where numDomainID=@numDomainID

			--Contract 
			---------------------------------------------------------

			DELETE FROM [ContractsContact] WHERE [numDomainId]=@numDomainID
			delete contractManagement where numDomainID=@numDomainID

			--Solution/KnowledgeBase
			---------------------------------------------------------
			delete from CaseSolutions where [numSolnID] IN ( SELECT numSolnID FROM [SolutionMaster] WHERE numDomainID=@numDomainID)      
			DELETE FROM [SolutionMaster] WHERE [numDomainID]=@numDomainID


			--Document Management
			---------------------------------------------------------
			DELETE FROM 	DocumentWorkflow	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [GenericDocuments] WHERE [numDomainID]=@numDomainID
			delete from SpecificDocuments where [numDomainID]=@numDomainID

			--Outlook
			---------------------------------------------------------
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numEmailGroupID] IN (SELECT [numEmailGroupID] FROM  ProfileEmailGroup	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	ProfileEmailGroup	 WHERE numDomainID = @numDomainID
			DELETE FROM [Correspondence] WHERE [numDomainID]=@numDomainID /***/
			DELETE FROM [Correspondence] WHERE [numEmailHistoryID] IN(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			DELETE FROM [EmailMaster] WHERE [numEmailId] IN (select [numEmailId] from EmailHStrToBCCAndCC where [numEmailHstrID] in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID) )
			DELETE FROM [EmailHstrAttchDtls] WHERE [numEmailHstrID] IN (select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete EmailHStrToBCCAndCC where numEmailHSTRID in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete [EmailHistory] where [numDomainID] =@numDomainID

			--Marketing/Campaign Mgmt
			---------------------------------------------------------
			DELETE FROM [CampaignDetails] WHERE [numCampaignId] IN (SELECT [numCampaignId] FROM [CampaignMaster] WHERE [numDomainID]=@numDomainID)
			delete from CampaignMaster where numDomainID= @numDomainID

			delete from  ConECampaignDTL where numECampDTLID in (select numECampDTLId from ECampaignDTLs where numECampID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID) )
			delete from  ConECampaign where numECampaignID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaignAssignee] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ECampaignDTLs] WHERE [numECampID] IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaign] WHERE [numDomainID]=@numDomainID
			
			DELETE FROM 	BroadCastDtls	 WHERE [numBroadCastID] IN (SELECT [numBroadCastId] FROM [Broadcast] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	Broadcast	 WHERE numDomainID = @numDomainID

			--Survey
			---------------------------------------------------------
			 --First Delete All Survey Responses related to this Survey.      
			 DELETE FROM dbo.SurveyTemplate WHERE numDomainId=@numDomainID
			 
			 DELETE FROM SurveyResponse       
			  WHERE numRespondantID IN       
			   (SELECT numRespondantID       
				FROM SurveyRespondentsMaster       
				 WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID) )      
			    
			 --Delete all Survey Resposen for the Current Survey   
			 DELETE FROM  SurveyResponse  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			  
			 --Then Delete All Survey Respondants related to this Survey.      
			 DELETE FROM  SurveyRespondentsChild  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 DELETE FROM  SurveyRespondentsMaster  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)    
			  
			 --Deletes the Rules for the Survey Answers    
			 DELETE FROM SurveyWorkflowRules    
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Ans Master related to this Survey.      
			 DELETE FROM SurveyAnsMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Question Master related to this Survey.      
			 DELETE FROM SurveyQuestionMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete The Survey from the Survey Master.      
			 DELETE FROM SurveyMaster       
			  WHERE  numDomainId = @numDomainID  


			--E-Commerce
			---------------------------------------------------------
			DELETE FROM dbo.ReviewDetail WHERE numReviewId IN (SELECT numReviewId FROM dbo.Review WHERE numDomainId = @numDomainID)
			DELETE FROM dbo.Review WHERE numDomainId = @numDomainID
			
			DELETE FROM Ratings WHERE numDomainId = @numDomainID
			
			DELETE FROM 	WebAPIDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	WebService	 WHERE numDomainID = @numDomainID
			DELETE FROM 	SiteWiseCategories	 WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	MetaTags	 WHERE [tintMetatagFor]=1 AND [numReferenceID] IN (SELECT numitemcode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM SiteSubscriberDetails WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [SiteBreadCrumb] WHERE [numDomainID] = @numDomainID
			DELETE FROM [StyleSheetDetails] WHERE [numCssID] IN (SELECT numCssID FROM   [StyleSheets] WHERE  [numDomainID] = @numDomainID)
			DELETE FROM [StyleSheets] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SiteTemplates] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [PageElementDetail] WHERE   [numDomainID] = @numDomainID
			DELETE FROM [SiteMenu] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SitePages] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [SiteCategories] WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)

			delete from ShippingRules WHERE numDomainID = @numDomainID
			DELETE FROM Sites WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.PromotionOfferItems WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM dbo.PromotionOfferContacts WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID


			--Module-Opportunity,Shipping 
			---------------------------------------------------------
			
			delete from CreditBalanceHistory WHERE numDomainId = @numDomainID
--			where numReturnID IN (SELECT numReturnID FROM dbo.[Returns] WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			AND numOppBizDocsId in (select numOppBizDocsId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
			DELETE FROM TransactionHistory WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OrderAutoRuleDetails	 WHERE [numRuleID] IN (SELECT [numRuleID] FROM [OrderAutoRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	OrderAutoRule	 WHERE numDomainID = @numDomainID

			DELETE FROM 	InventroyReportDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocStatusApprove	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocApprovalRuleEmployee	 WHERE [numBizDocAppId] IN (SELECT [numBizDocAppId] FROM [BizDocApprovalRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	BizDocApprovalRule	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizActionDetails	 WHERE [numBizActionId] IN (SELECT [numBizActionId] FROM  BizDocAction	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	BizDocAction	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocComission	 WHERE numDomainID = @numDomainID

			delete from Comments where numStageID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)
			delete from StageDependency where numDependantOnID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)

			  DELETE FROM 	StagePercentageDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	[SubStageDetails]	 WHERE [numSubStageHdrID] IN (SELECT [numSubStageHdrID] FROM [SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID))
			  DELETE FROM 	[SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID)


			
			DELETE FROM dbo.EmbeddedCostItems WHERE numDomainID=@numDomainID
			  DELETE FROM dbo.EmbeddedCostDefaults WHERE numDomainID=@numDomainID	
			  DELETE FROM EmbeddedCostConfig WHERE numDomainID=@numDomainID
				DELETE FROM EmbeddedCost WHERE numDomainID=@numDomainID
			  DELETE FROM  ShippingProviderForEComerce WHERE [numDomainID]=@numDomainID 
			  DELETE FROM [ShippingFieldValues] WHERE [numDomainID]=@numDomainID 
			 
			  DELETE FROM [ShippingReportItems] WHERE [numItemCode] IN (SELECT numItemCode FROM Item WHERE numDomainID =@numDomainID)/***/
			  DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))

			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (SELECT numShippingReportId FROM [ShippingReport] WHERE [numDomainId] =@numDomainID)
			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))
			
			  DELETE FROM [ShippingReport] WHERE [numDomainId] =@numDomainID	/***/
			  DELETE FROM [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

			  DELETE FROM SalesTemplateItems WHERE [numDomainID] =@numDomainID /***/
			  DELETE FROM [OpportunitySalesTemplate] WHERE [numDomainID]=@numDomainID
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityMaster] INNER JOIN [OpportunityBizDocs] ON [OpportunityMaster].[numOppId] = [OpportunityBizDocs].[numOppId] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numChildOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentProjectID] IN (SELECT [numProId] FROM [ProjectsMaster] WHERE [numDomainId]=@numDomainID)
			  
			  DELETE FROM dbo.WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM dbo.WorkOrder WHERE numDomainID=@numDomainID)
			  DELETE FROM WorkOrder WHERE numDomainID=@numDomainID
			  
			  DELETE FROM RECENTITEMS where numRecordID in  (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) and chrRecordType='O'
			      
			 
			  DELETE FROM 	PortalBizDocs	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	PortalWorkflow	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	BizDocAttachments	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	WareHouseForSalesFulfillment	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	OpportunityBizDocsPaymentDetails	 WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	OpportunityBizDocsDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM OppWarehouseSerializedItem where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItemLinking where numNewOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityAddress  where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			                      
			  DELETE FROM OpportunityBizDocItems where numOppBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

--			  DELETE FROM OpportunityBizDocTaxItems where numOppBizDocID in                        
--			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			
			  DELETE FROM OpportunityItemsTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    
			  DELETE FROM OpportunityMasterTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    

			  DELETE FROM OpportunityBizDocDtl where numBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))                        
			  
			  DELETE FROM 	OpportunityRecurring	 WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranSeedId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranOppID] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  --DELETE FROM OpportunityKitItems WHERE [numChildItem] IN (SELECT [numItemCode] FROM item WHERE [numDomainID]=@numDomainID)/***/
			  --DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityBizDocs] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) ) /***/
			  
			  --'OpportunityOrderStatus','OpportunityMasterTaxItems','OpportunityItemsTaxItems'
			  DELETE FROM [OpportunityOrderStatus] WHERE numDomainID = @numDomainID
			  DELETE FROM [OpportunityMasterTaxItems] WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM [OpportunityItemsTaxItems]  WHERE [numOppItemID] IN (SELECT [numOppItemID] FROM [dbo].[OpportunityItems] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) )
			  
			  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) ) /***/
			  DELETE FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityContact where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityDependency where numOpportunityId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	TimeExpAddAmount	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM TimeAndExpense where numDomainID= @numDomainID
			  --delete from CreditBalanceHistory where numDomainID= @numDomainID 
			   delete from OpportunityKitChildItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  delete from OpportunityKitItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                        
 
			  DELETE FROM [Returns] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			  
			  DELETE FROM OpportunityStageDetails WHERE numDomainId=@numDomainID
			  DELETE FROM OpportunityStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunitySubStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityRecurring] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID;
			  DELETE FROM [OpportunityMaster] WHERE [numDivisionId] IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM 	Sales_process_List_Master	 WHERE numDomainID = @numDomainID

			--Item Management,Pricebook,Category,Group,Assets
			---------------------------------------------------------
			DELETE FROM WebApiOppItemDetails WHERE numDomainId = @numDomainID
			
			DELETE FROM [PricingTable] WHERE [numPriceRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleDTL] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleItems] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID         

			DELETE FROM [CompanyAssetSerial] WHERE [numAssetItemID] IN (SELECT [numAItemCode] FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID)
			DELETE FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID

			DELETE FROM [ItemCategory] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [Category] WHERE [numDomainID] =@numDomainID

			DELETE FROM [ItemGroupsDTL] WHERE [numItemGroupID] IN (SELECT [numItemGroupID] FROM [ItemGroups] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemGroups] WHERE [numDomainID]=@numDomainID

			
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM [Vendor] WHERE numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDiscountProfile] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemExtendedDetails] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemDetails] WHERE [numItemKitID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDetails] WHERE [numChildItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [ItemOppAccAttrDTL] WHERE [numOptAccAttrID] IN(SELECT [numItemAttrOptAccID] FROM  [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ItemTax] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [TaxItems] WHERE [numDomainID]=@numDomainID

			DELETE FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID

			DELETE FROM warehouseItmsDTL WHERE numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems WHERE [numWareHouseID] IN (SELECT [numWareHouseID] FROM [Warehouses] WHERE [numDomainID]=@numDomainID)) /***/
			DELETE FROM [WareHouseDetails] WHERE [numDomainId]=@numDomainID
			delete from WareHouseItems WHERE [numDomainID]=@numDomainID
			DELETE FROM [Warehouses] WHERE [numDomainID]=@numDomainID

			Delete from ItemImages where [numDomainID]=@numDomainID
			delete from item where [numDomainID] = @numDomainID
			DELETE FROM [ItemAPI] WHERE  [numDomainId] = @numDomainID
			DELETE FROM LuceneItemsIndex WHERE [numDomainId] = @numDomainID

			--Administration (custom fiels,master lists,alerts)
			---------------------------------------------------------
			DELETE FROM 	FieldRelationshipDTL	 WHERE [numFieldRelID] IN (SELECT [numFieldRelID] FROM [FieldRelationship] WHERE [numDomainID] = @numDomainID)
			DELETE FROM 	FieldRelationship	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Currency	 WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END 
			delete from VendorShipmentMethod where numDomainID = @numDomainID
			DELETE FROM 	ListMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TaxDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ExceptionLog	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PaymentGatewayDTLID	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	DomainWiseSort	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ShortCutBar	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabDefault	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AuthoritativeBizDocs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListOrder	 WHERE numDomainID = @numDomainID
			DELETE FROM 	InboxTree	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ImapUserDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM		NameTemplate WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END

			DELETE FROM 	ShortCutGrpConf	 WHERE numDomainID = @numDomainID			
			DELETE FROM 	ShortCutUsrCnf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DefaultTabs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	GroupTabDetails	 WHERE [numGroupId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			
		
			
			DELETE FROM 	RecImportConfg	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PageLayoutDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RoutinLeadsValues	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeadDetails	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeads	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	SavedSearch WHERE numDomainID=@numDomainID
			DELETE FROM 	AdvSearchCriteria WHERE numDomainID =@numDomainID
			DELETE FROM 	AdvSerViewConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserRoles	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	UserTeams	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserTerritory	 WHERE [numDomainID]= @numDomainID
			DELETE FROM 	State	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	HomePage	 WHERE numDomainID = @numDomainID
			DELETE FROM [Favorites] WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM Favorites	WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM HTMLFormURL WHERE numDomainId=@numDomainID
			
			DELETE FROM 	CFW_FLD_Values	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Attributes	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Case	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Cont	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Item	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Opp	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_OppItems WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Pro	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Product	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Serialized_Items	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			--DELETE FROM 	InitialListColumnConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RelationsDefaultFilter	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFrmConfigBizDocsSumm	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DynamicFormMasterParam	 WHERE numDomainID = @numDomainID
			
			DELETE FROM     DycField_Globalization WHERE numDomainID = @numDomainID
			DELETE FROM     DycField_Validation WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormConfigurationDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormField_Mapping	 WHERE numDomainID = @numDomainID -- deletes custom field
			DELETE FROM		DycFieldMaster WHERE numDomainID = @numDomainID 

			DELETE FROM 	CFw_Grp_Master	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CFW_Fld_Dtl	 WHERE [numFieldId] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Master	 WHERE numDomainID = @numDomainID

			DELETE TreeNavigationAuthorization WHERE numDomainID = @numDomainID
			
			DELETE FROM 	GroupAuthorization	 WHERE [numGroupID] IN (SELECT [numGroupID] from AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	AlertEmailDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AlertDomainDtl	 WHERE [numDomainId] = @numDomainID
			DELETE FROM 	AlertContactsAndCompany	 WHERE [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			-- Wil need FK to DOmainID 
			--DELETE FROM 	AlertDTL	 WHERE numDomainID = @numDomainID 
			-- table need column domainID
			DELETE FROM 	BizDocAlerts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocFilter	 WHERE numDomainID = @numDomainID

			DELETE FROM UOMConversion WHERE numDomainID=@numDomainID
			DELETE FROM UOM WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END
			
			/*
			SELECT * FROM [AlertHDR]
			SELECT * FROM AlertDTL
			SELECT * FROM [AlertEmailDTL]
			SELECT * FROM [AlertDomainDtl]
			SELECT * FROM [AlertContactsAndCompany]
			SELECT * FROM [AlertEmailMergeFlds]
			SELECT * FROM [BizDocAlerts] 
			*/
			-- Web Analytics,Tracking
			---------------------------------------------------------
			DELETE FROM 	TrackingCampaignData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TrackingVisitorsDTL	 WHERE [numTracVisitorsHDRID] IN (SELECT [numTrackingID] FROM [TrackingVisitorsHDR] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	TrackingVisitorsHDR	 WHERE numDomainID = @numDomainID
			

			
			--Action Item, Time & expense,TIckler
			---------------------------------------------------------
			DELETE FROM 	Recurrence	 WHERE  [RecurrenceID] IN (SELECT [RecurrenceID] FROM [Activity] WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)) ) 
			--DELETE FROM 	tmpCalandarTbl	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ActivityResource	 WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	ResourcePreference	 WHERE [ResourceID] IN (SELECT [ResourceID] FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	Resource	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Communication	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Activity	 WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID))
		

			DELETE FROM 	TimeAndExpense	 WHERE numDomainID = @numDomainID
			DELETE FROM 	tblActionItemData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FollowUpHistory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Forecast	 WHERE numDomainID = @numDomainID


			--Reports
			---------------------------------------------------------
			DELETE FROM     DashboardAllowedReports WHERE [numGrpId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			DELETE FROM 	MyReportList	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByTeam	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByTerritory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByCampaign	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByAsItsType	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CustRptSchContacts	 WHERE [numScheduleId] IN (SELECT numScheduleId FROM CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID))
			DELETE FROM 	CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportSummationlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportOrderlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportFilterlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM CustReportFields WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [CustomReport] WHERE [numDomainID]=@numDomainID;
			--Accounting
			---------------------------------------------------------
			DELETE FROM dbo.ReturnItems WHERE [numReturnHeaderID] IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM ReturnPaymentHistory WHERE numReturnHeaderID IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM	dbo.ReturnHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM BankReconcileMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillDetails WHERE numBillID IN (SELECT numBillID FROM dbo.BillHeader WHERE numDomainID = @numDomainID)
			DELETE FROM dbo.BillHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillPaymentDetails WHERE numBillID IN (SELECT numBillPaymentID FROM BillPaymentHeader WHERE numDomainID = @numDomainID)
			DELETE FROM BillPaymentHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM OnlineBillPayeeDetails WHERE numBankDetailId IN (SELECT numBankDetailID FROM dbo.BankDetails WHERE numDomainID = @numDomainID)
			DELETE FROM BankTransactionMapping WHERE numTransactionID IN (SELECT numTransactionID FROM BankStatementTransactions WHERE numDomainID = @numDomainID)
			DELETE FROM BankStatementTransactions WHERE numDomainID = @numDomainID
			DELETE FROM BankStatementHeader WHERE numDomainID = numDomainID
			DELETE FROM dbo.BankDetails WHERE numDomainID = @numDomainID
								
			DELETE FROM 	CashCreditCardRecurringDetails	 WHERE  [numCashCreditCardId] IN ( SELECT [numCashCreditId] FROM CashCreditCardDetails	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CashCreditCardDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetDetails	 WHERE [numProcurementId] IN (SELECT [numProcurementBudgetId] FROM ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	FixedAssetDetails	 WHERE numDomainID = @numDomainID
			
--			DELETE FROM 	ChecksRecurringDetails	 WHERE [numCheckId] IN (SELECT [numCheckId] FROM [CheckDetails] WHERE [numDomainId]=@numDomainID)
--			DELETE FROM 	CheckDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckDetails WHERE numCheckHeaderID IN (SELECT numCheckHeaderID FROM dbo.CheckHeader WHERE numDomainID = @numDomainID)
			--DELETE FROM dbo.CheckCompanyDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckHeader WHERE numDomainID = @numDomainID
						
			DELETE FROM 	MarketBudgetDetails	 WHERE [numMarketId] IN (SELECT [numMarketBudgetId] FROM  MarketBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	OperationBudgetDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	OperationBudgetDetails	 WHERE [numBudgetId] IN (SELECT [numBudgetId] FROM [OperationBudgetMaster] WHERE [numDomainId]=@numDomainID)
			
			DELETE FROM 	DepositeDetails	 WHERE numDepositID IN (SELECT numDepositID FROM dbo.DepositMaster WHERE numDomainID = @numDomainID)
			DELETE FROM 	dbo.DepositMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OperationBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	MarketBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountingCharges	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ChartAccountOpening	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RecurringTemplate	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FinancialYear	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountTypeDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COARelationships	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COAShippingMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COACreditCardCharge	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ContactTypeMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Chart_Of_Accounts	 WHERE numDomainID = @numDomainID

			--Commission
			---------------------------------------------------------
			DELETE FROM 	CommissionContacts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionReports	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionRuleContacts WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleItems WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleDtl WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRules	 WHERE numDomainID = @numDomainID
			

			--Relationship
			---------------------------------------------------------
			 
			DELETE FROM 	DynamicFormAOIConf	 WHERE numDomainID = @numDomainID
			delete  RECENTITEMS  where numRecordID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID) and chrRecordType='C'
			DELETE FROM CommunicationAttendees WHERE numCommId IN (SELECT numCommId FROM Communication WHERE [numDomainID]=@numDomainID)
			DELETE FROM Communication WHERE [numDomainID]=@numDomainID
			delete from ExtranetAccountsDtl WHERE [numDomainID] =@numDomainID
			delete from ExtranetAccountsDtl WHERE [numExtranetID] IN (SELECT [numExtranetID] FROM  ExtarnetAccounts where [numDomainID]=@numDomainID)
			delete from ExtarnetAccounts where [numDomainID]=@numDomainID
			DELETE FROM [ExtarnetAccounts] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			delete from CaseContacts WHERE numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete from ProjectsContacts where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                   
			delete from OpportunityContact where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete AOIContactLink where [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			delete UserTeams WHERE [numDomainID]=@numDomainID
			delete UserTerritory where  [numDomainID]=@numDomainID
			DELETE FROM CustomerCreditCardInfo	 WHERE numContactID IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			
			DELETE FROM AddressDetails	 WHERE numDomainID=@numDomainID 
			DELETE FROM AdditionalContactsInformation WHERE [numDomainID]=@numDomainID
			DELETE FROM [CompanyAssociations] WHERE [numDomainID]=@numDomainID/***/
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM 	DivisionTaxTypes	 WHERE numDivisionID IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID)
			
			DELETE FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID
			DELETE FROM [CompanyInfo] WHERE [numDomainID]=@numDomainID


			
	
			
			
			DELETE FROM UserAccessedDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM [UserMaster] WHERE [numDomainID] =@numDomainID; 
			DELETE FROM [AccountTypeDetail] WHERE [numDomainID]=@numDomainID;
			DELETE FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID;
			DELETE FROM TaxCountryConfi WHERE [numDomainID]=@numDomainID
			Delete from AlertConfig where [numDomainID]=@numDomainID
			
			DELETE FROM [Domain] WHERE [numDomainId]=@numDomainID;
			DELETE FROM 	SubscriberHstr	 WHERE [numTargetDomainID] = @numDomainID
			DELETE FROM 	Subscribers	 WHERE [numTargetDomainID] = @numDomainID


			PRINT 'Domain Deleted successfully.'
/*---------------------------------------------------------------------------------*/

/* Not sure about following table 

DELETE FROM 	EformConfiguration	 WHERE numDomainID = @numDomainID
*/

/*---------------------------------------------------------------------------------*/
	--ROLLBACK
	COMMIT
END TRY 
BEGIN CATCH		
PRINT @@ERROR
PRINT ERROR_MESSAGE() 
    IF (@@TRANCOUNT > 0) BEGIN
        PRINT 'Unexpected error occurred!'
        ROLLBACK TRAN
        RETURN 1
    END
END CATCH	
END
/*
UPDATE [Subscribers] SET [bitActive]=0 WHERE [numTargetDomainID]=70
*/


GO
/****** Object:  StoredProcedure [dbo].[Usp_DeleteSalesReturn]    Script Date: 01/22/2009 01:36:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_DeleteSalesReturn' ) 
    DROP PROCEDURE Usp_DeleteSalesReturn
GO
CREATE PROCEDURE [dbo].[Usp_DeleteSalesReturn]
    @numReturnHeaderID NUMERIC(9),
    @numDomainId NUMERIC(9)
AS 
    BEGIN
	
        IF EXISTS ( SELECT  numReturnHeaderID
                    FROM    ReturnHeader
                    WHERE   numReturnHeaderID = @numReturnHeaderID
                            AND ISNULL(tintReceiveType, 0) <> 0 ) 
            BEGIN
                RAISERROR ( 'CreditMemo_Refund', 16, 1 ) ;
                RETURN ;
            END

		IF EXISTS (SELECT numDepositID FROM dbo.DepositMaster WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
						 AND numDomainId=@numDomainId
						 And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'Deposit_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END
            
        IF EXISTS (SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
					AND numDomainId=@numDomainId
					And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'BillPayment_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END

		DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID
		
        DELETE  FROM ReturnItems
        WHERE   numReturnHeaderID = @numReturnHeaderID
        
        /*Start : If Refund against Deposit or Bill Payment*/
        DECLARE @numBillPaymentIDRef AS NUMERIC(18);SET @numBillPaymentIDRef=0
        DECLARE @numDepositIDRef AS NUMERIC(18);SET @numDepositIDRef=0
        DECLARE @monAmount AS DECIMAL(20,5);SET @monAmount=0
        
        SELECT @numBillPaymentIDRef=ISNULL(numBillPaymentIDRef,0),
			   @numDepositIDRef=ISNULL(numDepositIDRef,0),
			   @monAmount=ISNULL(monAmount,0)
			 FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID
        
        IF @numBillPaymentIDRef > 0
        BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount = ISNULL(monRefundAmount,0) - @monAmount
				WHERE numDomainID=@numDomainId AND numBillPaymentID=@numBillPaymentIDRef
        END

		IF @numDepositIDRef > 0
        BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount = ISNULL(monRefundAmount,0) - @monAmount
				WHERE numDomainID=@numDomainId AND numDepositId=@numDepositIDRef
        END

        DELETE  FROM [dbo].[ReturnItems]
        WHERE   [ReturnItems].[numReturnHeaderID] = @numReturnHeaderID 
        DELETE  FROM ReturnHeader
        WHERE   numReturnHeaderID = @numReturnHeaderID
                AND [numDomainID] = @numDomainId 

		DELETE FROM OpportunityMasterTaxItems WHERE numReturnHeaderID = @numReturnHeaderID
		DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID = @numReturnHeaderID
  
    END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteSalesReturnDetails' ) 
    DROP PROCEDURE USP_DeleteSalesReturnDetails
GO

CREATE PROCEDURE [dbo].[USP_DeleteSalesReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9),
      @numReturnStatus	NUMERIC(18,0),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

    DECLARE @tintReturnType TINYINT,@tintReceiveType TINYINT
    
    SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
    IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numDepositID FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
     
    IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
 
    IF (@tintReturnType=1 or @tintReturnType=2) 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS INT 
		DECLARE @numTempWarehouseItemID AS INT
		DECLARE @numTempWareHouseItmsDTLID AS INT
			
		DECLARE @TempOppSerial TABLE
		(
			ID INT IDENTITY(1,1),
			numOppId NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numQty INT,
			bitLotNo NUMERIC(18,0)
		)

		INSERT INTO @TempOppSerial
		(
			numOppId,
			numOppItemID,
			numWarehouseItemID,
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo
		)
		SELECT	
			RH.numOppId,
			RI.numOppItemID,
			OWSIReturn.numWarehouseItmsID,
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo
		FROM
			ReturnHeader RH
		INNER JOIN
			ReturnItems RI
		ON
			RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN
			OppWarehouseSerializedItem OWSIReturn
		ON
			RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
			AND RI.numReturnItemID = OWSIReturn.numReturnItemID
		INNER JOIN
			OpportunityItems OI
		ON
			RI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			RH.numReturnHeaderID = @numReturnHeaderID

		IF @tintReturnType = 1 --SALES RETURN
		BEGIN
			--CHECK IF SERIAL/LOT# ARE NOT USED IN SALES ORDER
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('Serial_LotNo_Used', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- MAKE SERIAL QTY 0 OR DECREASE LOT QUANTITY BECAUSE IT IS USED IN ORDER 
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					OWSI.numReturnHeaderID=@numReturnHeaderID
			END
		END
		ELSE IF @tintReturnType = 2 --PURCHASE RETURN
		BEGIN
			-- MAKE SERIAL QTY 1 OR INCREASE LOT QUANTITY BECAUSE IT IS RETURNED TO WAREHOUSE
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID=@numReturnHeaderID
		END

		--RE INSERT ENTERIES SERIAL/LOT# ENTERIES WITH ORDER IN OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		--SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		--WHILE @i <= @COUNT
		--BEGIN
		--	SELECT
		--		@bitTempLotNo = bitLotNo,
		--		@numTempQty = numQty,
		--		@numTempOppID = numOppId,
		--		@numTempOppItemID = numOppItemID,
		--		@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID,
		--		@numTempWarehouseItemID = numWarehouseItemID
		--	FROM
		--		@TempOppSerial
		--	WHERE
		--		ID = @i


		--	IF EXISTS (SELECT * FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsID=@numTempWarehouseItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID)
		--	BEGIN
		--		UPDATE 
		--			OppWarehouseSerializedItem
		--		SET 
		--			numQty = ISNULL(numQty,0) + ISNULL(@numTempQty,0)
		--		WHERE
		--			numOppID=@numTempOppID 
		--			AND numOppItemID=@numTempOppItemID 
		--			AND numWarehouseItmsID=@numTempWarehouseItemID
		--			AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
		--	END
		--	ELSE
		--	BEGIN
		--		INSERT INTO OppWarehouseSerializedItem
		--		(
		--			numWarehouseItmsDTLID,
		--			numOppID,
		--			numOppItemID,
		--			numWarehouseItmsID,
		--			numQty
		--		)
		--		VALUES
		--		(
		--			@numTempWareHouseItmsDTLID,
		--			@numTempOppID,
		--			@numTempOppItemID,
		--			@numTempWarehouseItemID,
		--			@numTempQty
		--		)
		--	END

		--	SET @i = @i + 1
		--END
    END
	
	IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,2
    END
         	              
    UPDATE  
		dbo.ReturnHeader
    SET     
		numReturnStatus = @numReturnStatus,
        numBizdocTempID = CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN NULL ELSE numBizdocTempID END,
        vcBizDocName =CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN '' ELSE vcBizDocName END ,
        IsCreateRefundReceipt = 0,
        tintReceiveType=0,monBizDocAmount=0, numItemCode = 0
    WHERE   
		numReturnHeaderID = @numReturnHeaderID
        AND	numDomainId = @numDomainId
    
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID
	 END	
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID
     END
    
	PRINT @tintReceiveType 
	PRINT @tintReturnType

	 IF @tintReceiveType = 1 AND @tintReturnType = 4
	 BEGIN
		DECLARE @monAppliedAmount AS DECIMAL(20,5)
		DECLARE @numDepositIDRef AS NUMERIC(18,0)
		DECLARE @numParentID NUMERIC(18,0)

		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0),@numParentID=ISNULL(numParentID,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID

		SELECT 
			@monAppliedAmount = ISNULL([RH].[monAmount],0)
		FROM 
			[dbo].[ReturnHeader] AS RH 
		WHERE 
			[RH].[numReturnHeaderID] = @numReturnHeaderID 
			AND [RH].[numDomainId] = @numDomainId

		IF @numDepositIDRef > 0			
		BEGIN
			UPDATE 
				[dbo].[DepositMaster] 
			SET 
				[monAppliedAmount] = ISNULL([monAppliedAmount],0) - @monAppliedAmount
				,[monRefundAmount] = ISNULL(monRefundAmount,0) + ISNULL([monDepositAmount],0)
				,[numReturnHeaderID] = (CASE WHEN ISNULL(@numParentID,0) > 0 THEN @numParentID ELSE NULL END)
			WHERE 
				[numDepositId] = @numDepositIDRef 
				AND [numDomainId] = @numDomainId
		END
	END
	
	DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
    
	DELETE FROM 
		dbo.General_Journal_Details 
	WHERE 
		numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [General_Journal_Header].[numDomainId] = @numDomainId)
		AND [General_Journal_Details].[numDomainId] = @numDomainId
    
	DELETE FROM dbo.General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [numDomainId] = @numDomainId
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
--Created by chintan
--EXEC [dbo].USP_GetAccountTypes 72,0,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountTypes')
DROP PROCEDURE USP_GetAccountTypes
GO
CREATE PROCEDURE [dbo].[USP_GetAccountTypes]
               @numDomainId      AS NUMERIC(9)  = 0,
               @numAccountTypeID AS NUMERIC(9)  = 0,
               @tintMode         TINYINT  = 0
AS
  BEGIN
    IF @tintMode = 1 -- select all account type with len(accountcode) = 4
      BEGIN
        SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                 [vcAccountType] AS [vcAccountType1],
               [numParentID],
               [numDomainID],
               [vcAccountCode] + '~' + CAST ([numAccountTypeID] AS VARCHAR(20)) AS CandidateKey
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) = 4
      END
    ELSE IF @tintMode = 2
		BEGIN
		DECLARE @vcParentAccountCode VARCHAR(50)
			SELECT @vcParentAccountCode=vcAccountCode FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID
			SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                  [vcAccountType] AS [vcAccountType1],
               [numParentID],
               [numDomainID]
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) > 4
               AND [vcAccountCode] LIKE  @vcParentAccountCode +'%' 
--               AND [numAccountTypeID]  NOT IN ( SELECT [numParentID]
--        FROM   [AccountTypeDetail]
--        WHERE  [numDomainID] = @numDomainId
--               AND LEN([vcAccountCode]) > 4
--               AND [vcAccountCode] LIKE  @vcParentAccountCode +'%' )
		END
    ELSE IF @tintMode = 3 --Select all account type which has atleast one account 
	  SELECT    [numAccountTypeID],
				[vcAccountCode],
				[vcAccountType] /*+ '-' + [vcAccountCode]*/ AS [vcAccountType],
				[numParentID],
				[numDomainID]
	  FROM      [AccountTypeDetail]
	  WHERE     [numDomainID] = @numDomainId
				AND [numAccountTypeID] IN (
				SELECT  ISNULL([numParntAcntTypeId], 0)
				FROM    [Chart_Of_Accounts]
				WHERE   [numDomainId] = @numDomainId )
    ELSE 
      BEGIN
        IF @numAccountTypeID = 0
          BEGIN
            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                   [vcAccountType] AS [vcAccountType1],
                   [numParentID],
                   [numDomainID],
				   ISNULL(bitActive,0) bitActive
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NULL

            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                     [vcAccountType] AS [vcAccountType1],
                   [numParentID],
                   [numDomainID],
				   ISNULL(bitActive,0) bitActive
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NOT NULL
                   ORDER BY (CASE vcAccountType WHEN 'Assets' THEN 1 WHEN 'Liabilities' THEN 2 WHEN 'Equity' THEN 3 WHEN 'Income' THEN 4 WHEN 'Cost Of Goods' THEN 5 WHEN 'Expense' THEN 6 ELSE 7 END),numParentID,ISNULL(tintSortOrder,0),vcAccountCode
                   
          END
        ELSE
          IF @numAccountTypeID > 0
            BEGIN
              SELECT [numAccountTypeID],
                     [vcAccountCode],
                     [vcAccountType],
                     [numParentID],
                     [numDomainID],
					 [bitActive]
              FROM   [AccountTypeDetail]
              WHERE  [numDomainID] = @numDomainId
                     AND [numAccountTypeID] = @numAccountTypeID
            END
      END
  END


GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssociations]    Script Date: 03/05/2010 20:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Modified By: Debasish              
--Modified Date: 30th Dec 2005              
--Purpose for Modification: To get the associations for the company              
-- EXEC usp_GetAssociations 91300 ,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getassociations')
DROP PROCEDURE usp_getassociations
GO
CREATE PROCEDURE [dbo].[usp_GetAssociations]
    @numDivID AS NUMERIC(9),
    @bitGetAssociatedTo BIT = 1,
	@numAssociationId AS NUMERIC(9)=0,
	@numDominId AS NUMERIC(9)=0
AS 
    BEGIN
		IF @numAssociationID > 0
		BEGIN
		     SELECT CA.*, CMP.vcCompanyName FROM [dbo].[CompanyAssociations] AS CA 
			 LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
			 WHERE [CA].[numAssociationID]=@numAssociationID AND [CA].[numDomainID]=@numDominId
		END			                
        ELSE IF @bitGetAssociatedTo = 1 
            BEGIN                    
				
                SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                          WHERE     DivisionMaster.numDivisionID = CA.numDivisionID
                        ) AS TargetCompany,
                        ISNULL(ACI.numContactID,ACIDiv.numContactId) numContactID,
                        ISNULL(ACI.vcFirstname, ISNULL(ACIDiv.vcFirstname,'')) + ' '
                        + ISNULL(ACI.vcLastName, ISNULL(ACIDiv.vcLastName,'')) AS ContactName,
                        ISNULL(ACI.numPhone, ISNULL(ACIDiv.numPhone,'')) + ', '
                        + ISNULL(ACI.numPhoneExtension, ISNULL(ACIDiv.numPhoneExtension,'')) AS numPhone,
                        ISNULL(ACI.vcEmail,ISNULL(ACIDiv.numPhoneExtension,'')) vcEmail,
                        LD.vcData,
                        LD1.vcData AS Relationship,
                        LD2.vcData AS OrgProfile,
                        DIV.tintCRMType,
                        CASE WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             --WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						CAST(1 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numAssociateFromDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType
						LEFT OUTER JOIN [DivisionMaster] DIV1 ON DIV1.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId AND Div1.numCompanyID=CMP1.numCompanyId
						LEFT OUTER JOIN [AdditionalContactsInformation] ACIDiv ON ACIDiv.numDivisionId = DIV1.numDivisionID AND ISNULL(ACIDiv.bitPrimaryContact,0) = 1
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numAssociateFromDivisionID = @numDivID
                        AND CA.bitDeleted = 0

              
                
				union
				
						SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numAssociateFromDivisionID as numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                          WHERE     DivisionMaster.numDivisionID = CA.numAssociateFromDivisionID
                        ) AS TargetCompany,
                        ACI.numContactID,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        LD.vcData,
                        LD1.vcData AS OrgProfile,
                        LD2.vcData AS Relationship ,
                        DIV.tintCRMType,
                        CASE --WHEN CA.bitParentOrg = 1 THEN 'Child'
                             WHEN CA.bitChildOrg = 1 THEN '(Parent)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						--CAST((CASE WHEN CA.bitChildOrg = 1 THEN 0 ELSE 1 END)AS BIT) 
						CAST(0 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numAssociateFromDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numDivisionID = @numDivID
                        AND CA.bitDeleted = 0 --AND isnull(CA.numAssoTypeLabel,0)<>0
                ORDER BY TargetCompany


                SELECT  CompanyInfo.vcCompanyName,
						DIV.[numDivisionID],
                        LD.vcData AS OrgProfile,
                        LD1.vcData AS Relationship,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        ACI.[numContactId],isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo
                FROM    DivisionMaster AS DIV
                        LEFT OUTER JOIN CompanyInfo ON DIV.numCompanyID = CompanyInfo.numCompanyId
                        LEFT OUTER JOIN ListDetails AS LD ON [CompanyInfo].[vcProfile] = LD.[numListItemID]
                        LEFT OUTER JOIN ListDetails AS LD1 ON CompanyInfo.numCompanyType = LD1.numListItemID
                        LEFT OUTER JOIN AdditionalContactsInformation AS ACI ON DIV.numDivisionID = ACI.[numDivisionId]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= DIV.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   DIV.[numDivisionID] = @numDivID
                        AND ACI.bitPrimaryContact = 1
           
            END       
        ELSE 
            BEGIN      
                SELECT  numAssociationID,
                        CMP.vcCompanyName AS Company,
                        LD.vcData
                FROM    
					CompanyAssociations CA
				LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
				LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
                WHERE   CA.bitDeleted = 0
                        AND CA.numDivisionID = @numDivID
                        AND DIV.numDivisionID = CA.numAssociateFromDivisionID
                        AND CMP.numCompanyID = DIV.numCompanyID
                        AND LD.numListItemID = CA.numReferralType
                GROUP BY numAssociationID,
                        CMP.vcCompanyName,
                        DIV.vcDivisionName,
                        LD.vcData
                ORDER BY vcCompanyName,
                        vcDivisionName       
            END           
    END

GO
SET ANSI_NULLS ON

GO
SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCampaignReport')
DROP PROCEDURE USP_GetCampaignReport
GO
CREATE PROCEDURE [dbo].[USP_GetCampaignReport]                                 
	@numDomainID NUMERIC(18,0)
	,@ClientTimezoneOffset INT
	,@numLeadSource NUMERIC(18,0)
	,@tintReportView TINYINT
	,@dtFromDate DATETIME
	,@dtToDate DATETIME                                            
AS                                                                
BEGIN  
	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @tintReportView IN (1,2)
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			numConversionPercent NUMERIC(18,2),
			monGrossProfit DECIMAL(20,5)
		)

		IF @tintReportView = 2
		BEGIN
			;WITH CTE AS 
			(
				SELECT
					YEAR(@dtFromDate) AS 'yr',
					MONTH(@dtFromDate) AS 'mm',
					DATENAME(mm, @dtFromDate) AS 'mon',
					(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
					CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
					DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
					@dtFromDate 'new_date'
				UNION ALL
				SELECT
					YEAR(DATEADD(d,1,new_date)) AS 'yr',
					MONTH(DATEADD(d,1,new_date)) AS 'mm',
					DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
					(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
					CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
					DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
					DATEADD(d,1,new_date) 'new_date'
				FROM CTE
				WHERE DATEADD(d,1,new_date) < @dtToDate
			)
	
			INSERT INTO #tempYearMonth
			(
				intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate
			)
			SELECT 
				yr,mm,MonthYear,dtStartDate,dtEndDate
			FROM 
				CTE
			GROUP BY 
				yr, mm, MonthYear, dtStartDate, dtEndDate
			ORDER BY 
				yr, mm
			OPTION (MAXRECURSION 5000)

			UPDATE 
				#tempYearMonth
			SET
				EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)
			END
		ELSE
		BEGIN
			;WITH CTE AS 
			(
				SELECT
					dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
					dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
					('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
					CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
					WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
					WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
					WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
					WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
					END AS dtStartDate,
					CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
					WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					END AS dtEndDate,
					@dtFromDate 'new_date'
				UNION ALL
				SELECT
					dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
					dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
					('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
					CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
					WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
					WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
					WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
					WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
					END AS dtStartDate,
					CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
					WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					END AS dtEndDate,
					DATEADD(d,1,new_date) 'new_date'
				FROM CTE
				WHERE DATEADD(d,1,new_date) < @dtToDate
			)
	
			INSERT INTO #tempYearMonth
			(
				intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate
			)
			SELECT 
				yr,qq,MonthYear,dtStartDate,dtEndDate
			FROM 
				CTE
			GROUP BY 
				yr, qq, MonthYear, dtStartDate, dtEndDate
			ORDER BY 
				yr, qq
			OPTION
				(MAXRECURSION 5000)

			UPDATE 
				#tempYearMonth
			SET
				EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)
		END
		
		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			ISNULL(vcData,'') AS Campaign
			,ISNULL((SELECT
						SUM(monDealAmount)
					FROM
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID
					INNER JOIN
						OpportunityMaster OM
					ON
						DM.numDivisionID = OM.numDivisionId
					WHERE 
						CI.numDomainID=@numDomainID 
						AND DM.numDomainID = @numDomainID
						AND OM.numDomainId=@numDomainID
						AND CI.vcHow=LD.numListItemID
						AND OM.tintOppType=1
						AND OM.tintOppStatus = 1
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,OM.bintCreatedDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) monTotalIncome
			,ISNULL((SELECT 
						COUNT(DM.numDivisionID) 
					FROM 
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID 
					WHERE 
						CI.numDomainID=@numDomainID 
						AND CI.vcHow=LD.numListItemID 
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,DM.bintCreatedDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) numLeadCount
			,ISNULL((SELECT 
						COUNT(numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) numCustomerCount
			,Stuff((SELECT 
						CONCAT(N', ',numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate 
				FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') AS DivisionIDs
			,ISNULL((SELECT 
						AVG(DATEDIFF(DAY,dtOrgCreatedDate,dtFirstSalesOrderDate))
					FROM
					(
						SELECT
							DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
							,DM.bintCreatedDate
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) intAverageDaysToConvert
			,ISNULL((SELECT 
						SUM(monDealAmount)
					FROM
						OpportunityMaster 
					WHERE
						numDomainId=@numDomainID
						AND tintOppType=1
						AND tintOppStatus=1
						AND numDivisionID IN (SELECT
													numDivisionID
												FROM
												(
													SELECT
														DM.numDivisionID
														,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
													FROM
														CompanyInfo CI 
													INNER JOIN 
														DivisionMaster DM 
													ON 
														CI.numCompanyId = DM.numCompanyID
													INNER JOIN
														OpportunityMaster OM
													ON
														DM.numDivisionID = OM.numDivisionId
													WHERE 
														CI.numDomainID=@numDomainID 
														AND DM.numDomainID = @numDomainID
														AND OM.numDomainId=@numDomainID
														AND CI.vcHow=LD.numListItemID
														AND OM.tintOppType=1
														AND OM.tintOppStatus = 1
													GROUP BY
														DM.numDivisionID
												) TEMP
												WHERE
													DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate)),0) monCampaignIncome
			,ISNULL((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID=BD.numBillID WHERE BH.numDomainID=@numDomainID AND ISNULL(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN TYM.StartDate AND TYM.EndDate),0) AS monCampaignExpense
			,TYM.*
		INTO
			#TEMPYearFinal
		FROM 
			ListDetails LD
		OUTER APPLY
			#TempYearMonth TYM
		WHERE
			LD.numDomainID = @numDomainID
			AND LD.numListID = 18
			AND (ISNULL(@numLeadSource,0)=0 OR LD.numListItemID=@numLeadSource)
	
		UPDATE
			#TEMPYearFinal
		SET
			numConversionPercent=(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END)
			,monGrossProfit = ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0) 


		SET @sql = 'SELECT
						Campaign,' + @columns + '
					FROM
					(
						SELECT
							Campaign
							,MonthYear
							,CONCAT(''{''
							,''TotalIncome:'',ISNULL(monTotalIncome,0),'',''
							,''LeadCount:'',ISNULL(numLeadCount,0),'',''
							,''CostPerLead:'',(CASE WHEN ISNULL(numLeadCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numLeadCount,0) ELSE 0 END),'',''
							,''CustomerCount:'',ISNULL(numCustomerCount,0),'',''
							,''CostPerCustomer:'',(CASE WHEN ISNULL(numCustomerCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numCustomerCount,0) ELSE 0 END),'',''
							,''DivisionIDs:'''''',ISNULL(DivisionIDs,''''),'''''',''
							,''ConversionPercent:'',(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END),'',''
							,''AverageDaysToConvert:'',ISNULL(intAverageDaysToConvert,0),'',''
							,''CampaignIncome:'',ISNULL(monCampaignIncome,0),'',''
							,''CampaignExpense:'',ISNULL(monCampaignExpense,0),'',''
							,''GrossProfit:'',ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0),''}'') AS MonthTotal
						FROM
							#TEMPYearFinal
					) AS SourceTable
					pivot
					(
						MIN(MonthTotal)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Campaign'

			PRINT @sql
			EXECUTE (@sql)
	

		DROP TABLE #TEMPYearFinal
		DROP TABLE #TempYearMonth
	END
	ELSE
	BEGIN
		SELECT 
			LD.numListItemID
			,LD.vcData 
			,ISNULL((SELECT
						SUM(monDealAmount)
					FROM
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID
					INNER JOIN
						OpportunityMaster OM
					ON
						DM.numDivisionID = OM.numDivisionId
					WHERE 
						CI.numDomainID=@numDomainID 
						AND DM.numDomainID = @numDomainID
						AND OM.numDomainId=@numDomainID
						AND CI.vcHow=LD.numListItemID
						AND OM.tintOppType=1
						AND OM.tintOppStatus = 1
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,OM.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate),0) monTotalIncome
			,ISNULL((SELECT 
						COUNT(DM.numDivisionID) 
					FROM 
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID 
					WHERE 
						CI.numDomainID=@numDomainID 
						AND CI.vcHow=LD.numListItemID 
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,DM.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate),0) numLeadCount
			,ISNULL((SELECT 
						COUNT(numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate),0) numCustomerCount
			,Stuff((SELECT 
						CONCAT(N', ',numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate 
				FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') AS DivisionIDs
			,ISNULL((SELECT 
						AVG(DATEDIFF(DAY,dtOrgCreatedDate,dtFirstSalesOrderDate))
					FROM
					(
						SELECT
							DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
							,DM.bintCreatedDate
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate),0) intAverageDaysToConvert
			,ISNULL((SELECT 
						SUM(monDealAmount)
					FROM
						OpportunityMaster 
					WHERE
						numDomainId=@numDomainID
						AND tintOppType=1
						AND tintOppStatus=1
						AND numDivisionID IN (SELECT
													numDivisionID
												FROM
												(
													SELECT
														DM.numDivisionID
														,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
													FROM
														CompanyInfo CI 
													INNER JOIN 
														DivisionMaster DM 
													ON 
														CI.numCompanyId = DM.numCompanyID
													INNER JOIN
														OpportunityMaster OM
													ON
														DM.numDivisionID = OM.numDivisionId
													WHERE 
														CI.numDomainID=@numDomainID 
														AND DM.numDomainID = @numDomainID
														AND OM.numDomainId=@numDomainID
														AND CI.vcHow=LD.numListItemID
														AND OM.tintOppType=1
														AND OM.tintOppStatus = 1
													GROUP BY
														DM.numDivisionID
												) TEMP
												WHERE
													DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate)),0) monCampaignIncome
			,ISNULL((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID=BD.numBillID WHERE BH.numDomainID=@numDomainID AND ISNULL(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN @dtFromDate AND @dtToDate),0) AS monCampaignExpense
		INTO
			#TEMPFinal
		FROM 
			ListDetails LD
		WHERE
			LD.numDomainID = @numDomainID
			AND LD.numListID = 18
			AND (ISNULL(@numLeadSource,0)=0 OR LD.numListItemID=@numLeadSource)
	
		SELECT
			ISNULL(vcData,'') AS Campaign
			,CONCAT('{'
					,'TotalIncome:',ISNULL(monTotalIncome,0),','
					,'LeadCount:',ISNULL(numLeadCount,0),','
					,'CostPerLead:',(CASE WHEN ISNULL(numLeadCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numLeadCount,0) ELSE 0 END),','
					,'CustomerCount:',ISNULL(numCustomerCount,0),','
					,'CostPerCustomer:',(CASE WHEN ISNULL(numCustomerCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numCustomerCount,0) ELSE 0 END),','
					,'DivisionIDs:''',ISNULL(DivisionIDs,''),''','
					,'ConversionPercent:',(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END),','
					,'AverageDaysToConvert:',ISNULL(intAverageDaysToConvert,0),','
					,'CampaignIncome:',ISNULL(monCampaignIncome,0),','
					,'CampaignExpense:',ISNULL(monCampaignExpense,0),','
					,'GrossProfit:',ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0),'}') AS Totals
		FROM
			#TEMPFinal
		ORDER BY 
			vcData

		DROP TABLE #TEMPFinal
	END
END
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN            
	SET @dtFromDate = DATEADD (MS , 000 , @dtFromDate)
	SET @dtToDate = DATEADD (MS , 997 , @dtToDate)
                                                    
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS DECIMAL(20,5)
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,			
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)), 
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
			,0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId,
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	IF @bitAddTotalRow = 1
	BEGIN
		INSERT INTO #tempDirectReport
		(
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType, 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal
		)
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			CONCAT('Total ',vcAccountType), 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			1
		FROM 
			#tempDirectReport COA
		WHERE 
			ISNULL(COA.numAccountId,0) > 0
			AND ISNULL(COA.bitTotal,0) = 0
			AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

		UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
		DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
	END

	
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				FORMAT(@dtFromDate,'MMM') AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SET @sql = CONCAT('SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,',@columns,'
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ',@pivotcolumns,' )
					) AS P ORDER BY Struc, [Type] desc')


		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
		DROP TABLE #tempViewDataYear
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SET @sql = CONCAT('SELECT
							ParentId,
							vcCompundParentKey,
							numAccountId,
							numAccountTypeID,
							vcAccountType,
							LEVEL,
							vcAccountCode,
							Struc,
							[Type],
							bitTotal,',@columns,'
						FROM
						(
							SELECT 
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''#'' + COA.Struc + ''#'') AS Struc,
								(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
								ISNULL(SUM(Amount),0)  AS Amount,
								V.MonthYear
							FROM 
								#tempDirectReport COA 
							LEFT OUTER JOIN 
								#tempViewDataQuarter V 
							ON  
								V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
						) AS SourceTable
						pivot
						(
							SUM(AMOUNT)
							FOR [MonthYear] IN ( ',@pivotcolumns,' )
						) AS P ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS DECIMAL(20,5) = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			(CASE 
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
			END) AS Amount,
			V.datEntry_Date
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		LEFT JOIN 
			#VIEW_JOURNALBS V 
		ON  
			COA.numAccountId = V.numAccountID 
			AND datEntry_Date <= @dtToDate
		WHERE 
			COA.[numAccountId] IS NOT NULL
			AND ISNULL(COA.bitTotal,0) = 0

		SELECT 
			COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
			COA.bitTotal,
			(Case 
					When COA.bitProfitLoss = 1
					THEN @ProifitLossAmount
					ELSE ((CASE WHEN CHARINDEX(('#' + COA.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) + SUM(ISNULL(Amount,0)))
			END) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + (CASE WHEN ISNULL(@bitAddTotalRow,0)=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '' ELSE '%' END)
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitProfitLoss
		ORDER BY 
			Struc, [Type] desc

		DROP TABLE #tempViewData
	END

	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0),
@bitAddTotalRow BIT
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO 
		#View_Journal 
	SELECT 
		numAccountId
		,AccountTypeCode
		,AccountCode
		,datEntry_Date
		,Debit
		,Credit 
	FROM 
		view_journal_master 
	WHERE 
		numDomainId = @numDomainID 
		AND (numClassIDDetail=@numAccountClass OR @numAccountClass=0)
		AND (numDivisionId=@numDivisionID OR @numDivisionID=0)

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc, bitTotal, bitIsSubAccount)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitTotal,bitIsSubAccount)
	AS
	(
		SELECT 
			ParentId,
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			bitTotal,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(CONCAT(d.Struc ,'#',[COA].[numAccountId]) AS VARCHAR(300)) AS Struc,
			0,
			ISNULL(COA.bitIsSubAccount,0)
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,ISNULL(COA.bitIsSubAccount,0)
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1			
	)

	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitTotal,
		bitIsSubAccount
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	IF @bitAddTotalRow = 1
	BEGIN
		INSERT INTO #tempDirectReport
		(
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType, 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal,
			bitIsSubAccount
		)
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			CONCAT('Total ',vcAccountType), 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			1,
			bitIsSubAccount
		FROM 
			#tempDirectReport COA
		WHERE 
			ISNULL(COA.numAccountId,0) > 0
			AND ISNULL(COA.bitTotal,0) = 0
			AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

		UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
		DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
	END

	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL,'-1',0,0
	UNION ALL
	SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL,'-2',0,0

	IF ISNULL(@bitAddTotalRow,0) = 1
	BEGIN
		INSERT INTO #tempDirectReport
		SELECT '','-1',-1,'Total Ordinary Income/Expense',0,NULL,NULL,'-1#Total',1,0
		UNION ALL
		SELECT '','-2',-2,'Total Other Income and Expenses',0,NULL,NULL,'-2#Total',1,0
	END	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc
		,[LEVEL] = [LEVEL] - 1
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-1
	WHERE 
		[vcAccountCode] NOT IN ('010302','010402')
		AND [LEVEL] = 1

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date,
		COA.bitIsSubAccount
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.numAccountId = COA.numAccountId
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL
		AND ISNULL(COA.bitTotal,0) = 0

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			FORMAT(@dtFromDate,'MMM') AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
			SET @Where = CONCAT('	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					LEFT(DATENAME(mm,datEntry_Date),3) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ',@PivotColumns ,')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''',@ProfitLossCurrentColumns,@ProfitLossSumCurrentColumns,',2,1,0
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''',@ProfitLossOpeningColumns,@ProfitLossSumOpeningColumns,',2,1,0 ORDER BY Struc')

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
			SET @Where = CONCAT('	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,',@numDomainID,') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,',@numDomainID,') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
					GROUP BY dbo.GetFiscalyear(datEntry_Date,',@numDomainID,'),dbo.GetFiscalQuarter(datEntry_Date,',@numDomainID, '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ', @PivotColumns,')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''',@ProfitLossCurrentColumns,@ProfitLossSumCurrentColumns ,',2,1,0
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''',@ProfitLossOpeningColumns,@ProfitLossSumOpeningColumns ,',2,1,0 ORDER BY Struc')

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN		
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = CONCAT(' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2,1,0
				UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''',@ProfitLossSumOpeningColumns ,',2,1,0 ORDER BY Struc')
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
GO
SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetConversionPercentOrders')
DROP PROCEDURE USP_GetConversionPercentOrders
GO
CREATE PROCEDURE [dbo].[USP_GetConversionPercentOrders]                                 
	@numDomainID NUMERIC(18,0)
	,@ClientTimezoneOffset INT
	,@vcDivisionIds VARCHAR(MAX)                                          
AS                                                                
BEGIN  
	SELECT
		CONCAT(CompanyInfo.vcCompanyName,' (',dbo.FormatedDateFromDate(DivisionMaster.bintCreatedDate,@numDomainID),')') vcCompanyName
		,vcPOppName
		,monDealAmount
		,dbo.fn_GetContactName(DivisionMaster.numRecOwner) vcRecordOwner
		,dbo.fn_GetContactName(DivisionMaster.numAssignedTo) vcAssignee
		,DATEDIFF(DAY,DivisionMaster.bintCreatedDate,TableFirstOrder.bintCreatedDate) intDaysTookToConvert
	FROM
		DivisionMaster
	INNER JOIN
		CompanyInfo 
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	CROSS APPLY
	(
		SELECT TOP 1 
			CONCAT(vcPOppName,' (',dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID),')') vcPOppName
			,OpportunityMaster.bintCreatedDate
			,ISNULL(monDealAmount,0) monDealAmount
		FROM
			OpportunityMaster
		WHERE
			numDomainID=@numDomainID
			AND OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
		ORDER BY
			OpportunityMaster.bintCreatedDate ASC
	) TableFirstOrder
	WHERE
		DivisionMaster.numDomainID = @numDomainID
		AND LEN(ISNULL(@vcDivisionIds,'')) > 0 AND DivisionMaster.numDivisionID IN (SELECT Id FROM dbo.SplitIDs(@vcDivisionIds,','))
	ORDER BY
		vcCompanyName
END
/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as DECIMAL(20,5);set @Amt=0                        
declare @Paid as DECIMAL(20,5);set @Paid=0      
                 
declare @AmountDueSO as DECIMAL(20,5);set @AmountDueSO=0                    
declare @AmountPastDueSO as DECIMAL(20,5);set @AmountPastDueSO=0 

declare @AmountDuePO as DECIMAL(20,5);set @AmountDuePO=0                    
declare @AmountPastDuePO as DECIMAL(20,5);set @AmountPastDuePO=0 
                   
declare @CompanyCredit as DECIMAL(20,5);set @CompanyCredit=0   
          
declare @PCreditMemo as DECIMAL(20,5);set @PCreditMemo=0            
declare @SCreditMemo as DECIMAL(20,5);set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(DECIMAL(20,5),case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then REPLACE(dbo.fn_GetListItemName(numCompanyCredit),',','') else 0 end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
DECLARE @intShippingCompany AS NUMERIC(18,0)
DECLARE @numPartnerSource NUMERIC(18,0)
DECLARE @bitAutoCheckCustomerPart BIT
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,''), @numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0), @intShippingCompany=ISNULL(intShippingCompany,0), 
@numPartnerSource=ISNULL(numPartenerSource,0),@bitAutoCheckCustomerPart=ISNULL(bitAutoCheckCustomerPart,0) FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo,
(CASE WHEN (SELECT COUNT(*) FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END) AS bitUseShippersAccountNo,
@numDefaultShippingServiceID AS numDefaultShippingServiceID,
@intShippingCompany AS intShippingCompany,
@numPartnerSource AS numPartnerSource,
@bitAutoCheckCustomerPart AS bitAutoCheckCustomerPart

GO
/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By: Debasish Tapan Nag                                                                          
----Purpose: Returns the available form fields from the database                                                                              
----Created Date: 07/09/2005                                
----exec usp_getDynamicFormFields 1,'',1,1,0                            
----Modified by anoop jayaraj                                                                         
----Modified by Gangadhar
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getdynamicformfields' ) 
    DROP PROCEDURE usp_getdynamicformfields
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormFields]
    @numDomainId NUMERIC(9),
    @cCustomFieldsAssociated VARCHAR(10),
    @numFormId INT,
	@numSubFormId NUMERIC(9),
    @numAuthGroupId NUMERIC(9),
	@numBizDocTemplateID numeric(9)
AS 
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18),numFormGroupId NUMERIC(18))
   
   
   CREATE TABLE #tempAddedFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18),numFormGroupId NUMERIC(18))
 
   INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)
     SELECT  CONVERT(VARCHAR(15), numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    bitIsRequired as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    ISNULL(boolAOIField, 0) boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId,
					0 AS numFormGroupId
			FROM    View_DynamicColumns
            WHERE   numFormID = @numFormId 
					AND numAuthGroupID = @numAuthGroupId
                    AND numDomainID = @numDomainId
                    AND isnull(numRelCntType,0)=@numBizDocTemplateID
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
					AND bitDeleted=0 

            UNION

            SELECT  CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType,
					 vcAssociatedControlType,
                    ISNULL(numListID, 0) numListID,
                    vcFieldName vcDbColumnName,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    ISNULL(tintColumn,0) as intColumnNum,
                    isnull(tintRow,0) as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE 'V' END vcFieldDataType,
                    ISNULL(boolAOIField, 0),
                    numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
					,0 AS numFormGroupId
			FROM    View_DynamicCustomColumns                                            
            WHERE   numDomainID = @numDomainId
                    AND numFormID = @numFormId
                    AND numAuthGroupID = @numAuthGroupId
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                    AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))     
					 and isnull(numRelCntType,0)=@numBizDocTemplateID         


        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)                         
            SELECT  CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitRequired,0) boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
			FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID=@numDomainID
                    AND numFieldId NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId
                            and isnull(numRelCntType,0)=@numBizDocTemplateID)
           
			UNION
            
			SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    isnull(L.vcFieldType,'C') as vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                    AND Fld_id NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
							AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
        

		SELECT * FROM #tempAvailableFields WHERE numFormFieldId NOT IN (SELECT numFormFieldId FROM #tempAddedFields)
		AND #tempAvailableFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAvailableFields.vcLookBackTableName,'CSGrid','') END)
		 UNION            
        SELECT * FROM #tempAddedFields WHERE 1=1
		AND #tempAddedFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAddedFields.vcLookBackTableName,'CSGrid','') END)
        
        DROP TABLE #tempAvailableFields
        DROP TABLE #tempAddedFields
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0),
@bitAddTotalRow BIT
AS                                                          
BEGIN 

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END
 

;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal,bitIsSubAccount)
AS
(
  -- anchor
	SELECT 
		CAST('-1' AS VARCHAR) AS ParentId,
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST([ATD].[vcAccountType] AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		1 AS LEVEL, 
		CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0103' THEN '1' WHEN '0106' THEN '2' WHEN '0104' THEN '3' END),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc
		,0
		,0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND [ATD].[vcAccountCode] IN('0103','0104','0106')
		AND ATD.bitActive = 1
	UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),CONCAT((CASE vcAccountCode WHEN '0103' THEN '1' WHEN '0106' THEN '2' WHEN '0104' THEN '3' END),[ATD].[vcAccountCode]),'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	UNION ALL
	-- recursive
	SELECT 
		D.vcCompundParentKey AS ParentId, 
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST([ATD].[vcAccountType] AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		LEVEL + 1, 
		CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS varchar(300))  AS Struc,
		0,
		0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD 
	JOIN 
		[DirectReport] D 
	ON 
		D.[numAccountTypeID] = [ATD].[numParentID]
		AND D.bitTotal = 0
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND ATD.bitActive = 1
	UNION ALL
	SELECT 
		D.vcCompundParentKey AS ParentId, 
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		LEVEL + 1, 
		CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
		1,
		0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD 
	JOIN 
		[DirectReport] D 
	ON 
		D.[numAccountTypeID] = [ATD].[numParentID]
		AND D.bitTotal = 0
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND ATD.bitActive = 1
		AND ISNULL(@bitAddTotalRow,0) = 1
),
DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,bitTotal,bitIsSubAccount)
AS
(
	-- anchor
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType,
		vcAccountCode, 
		LEVEL,
		CAST(numAccountID AS NUMERIC(18)),
		Struc,
		bitTotal,
		CAST(0 AS BIT)
	FROM 
		DirectReport 
	UNION ALL
	-- recursive
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)), 
		CAST(COA.[vcAccountName] AS VARCHAR(300)),
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST([COA].[numAccountId] AS VARCHAR) AS VARCHAR(300))  AS Struc,
		0,
		ISNULL(COA.bitIsSubAccount,0)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		AND D.bitTotal = 0
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND COA.bitActive = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 0
	UNION ALL
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)),
		CAST(COA.[vcAccountName] AS VARCHAR(300)),
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
		,0
		,ISNULL(COA.bitIsSubAccount,0)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.numAccountId = COA.numParentAccId
		AND D.bitTotal = 0
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND ISNULL(COA.bitActive,0) = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 1
)

  
SELECT 
	ParentId
	,vcCompundParentKey
	,numAccountTypeID
	,vcAccountType
	,LEVEL
	,vcAccountCode
	,numAccountId
	,Struc
	,bitTotal
	,bitIsSubAccount 
INTO 
	#tempDirectReport 
FROM 
	DirectReport1 

IF @bitAddTotalRow = 1
BEGIN
	INSERT INTO #tempDirectReport
	(
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitTotal
	)
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		CONCAT('Total ',vcAccountType), 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		CONCAT(COA.Struc,'#Total'),
		1
	FROM 
		#tempDirectReport COA
	WHERE 
		ISNULL(COA.numAccountId,0) > 0
		AND ISNULL(COA.bitTotal,0) = 0
		AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

	UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
	DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
END


INSERT INTO #tempDirectReport
SELECT '','-1',-1,'Operating Income/Expense',0,NULL,NULL,'-1',0,0
UNION ALL
SELECT '','-2',-2,'Other Income/Expense',0,NULL,NULL,'-2',0,0

IF ISNULL(@bitAddTotalRow,0) = 1
BEGIN
	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Net Operating Income',0,NULL,NULL,'-1#Total',1,0
	UNION ALL
	SELECT '','-2',-2,'Net Other Income',0,NULL,NULL,'-2#Total',1,0
END	

DECLARE @cogsStruc VARCHAR(300)

SELECT @cogsStruc=Struc FROM #tempDirectReport WHERE vcAccountCode = '0106'

INSERT INTO #tempDirectReport SELECT '-1','-4',-4,'Gross Profit',0,NULL,NULL,CONCAT('-1#',@cogsStruc,'#grossprofit'),1,0

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc,[LEVEL]=[Level]-1 WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


SELECT 
	COA.ParentId
	,COA.vcCompundParentKey
	,COA.numAccountTypeID
	,COA.vcAccountType
	,COA.LEVEL
	,COA.vcAccountCode
	,COA.numAccountId
	,COA.Struc
	,(CASE 
		WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
		THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
		ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
	END) AS Amount
	,V.datEntry_Date
	,COA.bitTotal
	,COA.bitIsSubAccount
INTO 
	#tempViewData
FROM 
	#tempDirectReport COA 
JOIN 
	View_Journal_Master V 
ON  
	V.numAccountId = COA.numAccountId
	AND V.numDomainID=@numDomainId 
	AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (V.numClassIDDetail=@numAccountClass OR @numAccountClass=0)
	AND (V.numDivisionId=@numDivisionID OR @numDivisionID=0)
WHERE 
	COA.[numAccountId] IS NOT NULL
	AND ISNULL(COA.bitTotal,0) = 0

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
		MONTH(DATEADD(d,1,new_date)) AS 'mm',
		DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE DATEADD(d,1,new_date) < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal, bitIsSubAccount'
		SET @Where = CONCAT('	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END) 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ', @PivotColumns, ')
				) AS p ORDER BY Struc')

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
		SET @Where = CONCAT('	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,',@numDomainID,') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,',@numDomainID,') AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END) 
				GROUP BY dbo.GetFiscalyear(datEntry_Date,',@numDomainID,'),dbo.GetFiscalQuarter(datEntry_Date,' ,@numDomainID,'),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ',@PivotColumns ,')
				) AS p ORDER BY Struc')
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 AND ISNULL(bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = CONCAT(' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				ORDER BY Struc')
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=0,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=1,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlayoutinfoddl')
DROP PROCEDURE usp_getlayoutinfoddl
GO
CREATE PROCEDURE [dbo].[usp_GetLayoutInfoDdl]                                
@numUserCntId as numeric(9)=0,                              
@intcoulmn as numeric(9),                              
@numDomainID as numeric(9)=0 ,                    
--@Ctype as char ,              
@PageId as numeric(9)=4,              
@numRelation as numeric(9)=0,
@numFormID as numeric(9)=0,
@tintPageType TINYINT             
as                        
                      
 if( @intcoulmn <> 0 )--Fields added to layout
begin                         
                        
         
SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,
convert(char(1),bitCustom) as bitCustomField,tintRow,bitRequired
FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID  and tintColumn = @intcoulmn      
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END)  order by tintRow  
       
if @pageid =1 or @pageid =4  OR @pageid =12 OR @pageid =13 OR @pageid =14
 begin    
 
 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired
  from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numRelation=@numRelation and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType
  
end      
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8    or @PageId= 11               
begin 

 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired
  from View_DynamicCustomColumns
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and  tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType

end       
                      
end                        
                        
                          
if @intcoulmn = 0 --Available Fields
begin                          
                      
 
 SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,'0' as bitCustomField,bitRequired
FROM View_DynamicDefaultColumns
 WHERE numFormId=@numFormID AND numDomainID=@numDomainID  AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END
		 ELSE 0 END)
 AND numFieldId NOT IN 
 (SELECT numFieldId FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID       
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 1=(CASE WHEN @tintPageType=2 THEN Case WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN Case WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		 ELSE 0 END)) ORDER BY ISNULL(vcCultureFieldName,vcFieldName)

       
if @pageid=4 or @pageid=1  OR @pageid =12 OR @pageid =13  OR @pageid =14                
begin           

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired
  from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId 
						  left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelation and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns_RelationShip where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp,numOrder 

end        
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                
begin 

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired
  from CFW_Fld_Master CFM left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 
                          
end       
      
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetParentCategory]    Script Date: 09/25/2009 16:25:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva      
-- [USP_GetParentCategory] 72,''

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getparentcategory')
DROP PROCEDURE usp_getparentcategory
GO
CREATE PROCEDURE [dbo].[USP_GetParentCategory]
    @numDomainId AS NUMERIC(9),
    @vcInitialCode VARCHAR(50) = ''
AS 
    BEGIN    

--if @vcInitialCode<>'010101'
--	  begin
        SELECT  
                ISNULL(C.[vcAccountName],'') + ' (' + ATD.[vcAccountType] + ')' AS vcAccountName1,
                ATD.[vcAccountCode] AS vcAccountTypeCode,
                ATD.[numAccountTypeID],
				ATD.[vcAccountType],
				ATD.[numParentID],
				ATD.[numDomainID],
				ATD.[dtCreateDate],
				ATD.[dtModifiedDate],
				C.[numAccountId],
				C.[numParntAcntTypeId],
				C.[numOriginalOpeningBal],
				C.[numOpeningBal],
				C.[dtOpeningDate],
				C.[bitActive],
				C.[bitFixed],
				C.[numDomainId],
				C.[numListItemID],
				C.[bitDepreciation],
				C.[bitOpeningBalanceEquity],
				C.[monEndingOpeningBal],
				C.[monEndingBal],
				C.[dtEndStatementDate],
				C.[chBizDocItems],
				C.[numAcntTypeId],
				C.[vcAccountCode],
				ISNULL(C.[vcAccountName],'') vcAccountName,
				C.[vcAccountDescription],
				C.[bitProfitLoss],ISNULL(C.vcStartingCheckNumber,'') AS vcStartingCheckNumber
        FROM    [Chart_Of_Accounts] C
                INNER JOIN [AccountTypeDetail] ATD 
                ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
        WHERE   C.[numDomainId] = @numDomainId AND ISNULL(ATD.bitActive,0) = 1
  AND (C.[vcAccountCode] LIKE @vcInitialCode + '%' OR LEN(@vcInitialCode)=0) AND C.bitActive = 1
  ORDER BY ATD.vcAccountCode, C.[vcAccountCode]
--     Declare @RootAccountId as numeric(9)  
--    Set @RootAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = @numDomainId) --and numAccountId = 1   
--      
--         
-- Select * From Chart_Of_Accounts COA inner join ListDetails LD on COA.numAcntType= LD.numListItemID Where COA.numParntAcntId =@RootAccountId and COA.numDomainId=@numDomainId and COA.numAccountId <>@RootAccountId    
--         
--
--	end
-- else
--	begin
--		SELECT  
--                ISNULL(C.[vcAccountName],'') + ' (' + ATD.[vcAccountType] + ')' AS vcAccountName1,
--                ATD.[vcAccountCode] AS vcAccountTypeCode,
--                ATD.[numAccountTypeID],
--				ATD.[vcAccountCode],
--				ATD.[vcAccountType],
--				ATD.[numParentID],
--				ATD.[numDomainID],
--				ATD.[dtCreateDate],
--				ATD.[dtModifiedDate],
--				C.[numAccountId],
--				C.[numParntAcntTypeId],
--				C.[numOriginalOpeningBal],
--				C.[numOpeningBal],
--				C.[dtOpeningDate],
--				C.[bitActive],
--				C.[bitFixed],
--				C.[numDomainId],
--				C.[numListItemID],
--				C.[bitDepreciation],
--				C.[bitOpeningBalanceEquity],
--				C.[monEndingOpeningBal],
--				C.[monEndingBal],
--				C.[dtEndStatementDate],
--				C.[chBizDocItems],
--				C.[numAcntTypeId],
--				C.[vcAccountCode],
--				ISNULL(C.[vcAccountName],'') vcAccountName,
--				C.[vcAccountDescription],
--				C.[bitProfitLoss],ISNULL(C.vcStartingCheckNumber,'') AS vcStartingCheckNumber
--        FROM    [Chart_Of_Accounts] C
--                INNER JOIN [AccountTypeDetail] ATD 
--                ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
--        WHERE   C.[numDomainId] = @numDomainId AND C.bitActive = 1
--  AND (C.[vcAccountCode] LIKE @vcInitialCode + '%' OR  C.[vcAccountCode] LIKE '010201%' OR  LEN(@vcInitialCode)=0)
--  ORDER BY C.[vcAccountCode]
--	end
END
--Created By                                                          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT             
                             
                                    
--                                                           
                                               
AS                                      
  
                                                 
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN                           
          
IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
BEGIN
    SELECT 
		numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintcolumn as intcoulmn,'' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
		,numListID,0 numListItemID,PopupFunctionName
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
		,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
		,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
	FROM 
		View_DynamicColumns
	WHERE 
		numFormId=@numFormID 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID  
		AND ISNULL(bitCustom,0)=0 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType 
		AND 1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
				WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
				ELSE 0 END) 
	UNION 
	SELECT 
		numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN (SELECT vcData FROM listdetails WHERE numlistitemid = dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' 
			THEN STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE
				dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
		END AS vcValue,                    
		CONVERT(BIT,1) bitCustomField,'' vcPropertyName, CONVERT(BIT,1) AS bitCanBeUpdated,numListID,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
			ELSE 0 
		END numListItemID,'' PopupFunctionName,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID ,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
		,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
	FROM 
		View_DynamicCustomColumns_RelationShip
	WHERE 
		grp_id=@PageId 
		AND numRelation=@numRelCntType 
		AND numDomainID=@numDomainID 
		AND subgrp=0
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType
	ORDER BY 
		tintRow,intcoulmn   
END                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			when vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case WHEN vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
                             
 end  
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN

	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))
			WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END  
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)
  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	IF @bitAddTotalRow = 1
	BEGIN
		INSERT INTO #tempDirectReport
		(
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType, 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal
		)
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			CONCAT('Total ',vcAccountType), 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			1
		FROM 
			#tempDirectReport COA
		WHERE 
			ISNULL(COA.numAccountId,0) > 0
			AND ISNULL(COA.bitTotal,0) = 0
			AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

		UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
		DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
	END

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) 
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SET @sql = CONCAT('SELECT
							ParentId,
							vcCompundParentKey,
							numAccountId,
							numAccountTypeID,
							vcAccountType,
							LEVEL,
							vcAccountCode,
							Struc,
							[Type],
							bitTotal,',@columns,'
						FROM
						(
							SELECT 
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''#'' + COA.Struc + ''#'') AS Struc,
								(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
								ISNULL(SUM(Amount),0) AS Amount,
								V.MonthYear
							FROM 
								#tempDirectReport COA 
							LEFT OUTER JOIN 
								#tempViewDataYear V 
							ON  
								V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
							UNION
							SELECT 
								'''', 
								''-1'',
								NULL,
								NULL,
								''Total'',
								-1, 
								NULL, 
								1,
								''-1'',
								2,
								ISNULL((SELECT 
											ISNULL(SUM(Debit) - SUM(Credit),0)
										FROM 
											VIEW_Journal_Master
										WHERE 
											numDomainID = ',@numDomainId,'
											AND (AccountCode LIKE ''0101%'' 
											OR AccountCode LIKE ''0102%''
											OR AccountCode LIKE ''0103%''
											OR AccountCode LIKE ''0104%''
											OR AccountCode LIKE ''0105%''
											OR AccountCode LIKE ''0106%'')
											AND (numClassIDDetail=',ISNULL(@numAccountClass,0) , ' OR ',ISNULL(@numAccountClass,0),' = 0)
											AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),0),
								MonthYear
							FROM
								#TempYearMonth
						) AS SourceTable
						pivot
						(
							SUM(AMOUNT)
							FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
						) AS P 
						ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0
		
		SET @sql = CONCAT('SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
						UNION
						SELECT 
							'''',
							''-1'', 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL,
							1,
							''-1'',
							2,
							ISNULL((SELECT 
										ISNULL(SUM(Debit) - SUM(Credit),0)
									FROM 
										VIEW_Journal_Master
									WHERE 
										numDomainID = ',@numDomainId,'
										AND (AccountCode LIKE ''0101%'' 
										OR AccountCode LIKE ''0102%''
										OR AccountCode LIKE ''0103%''
										OR AccountCode LIKE ''0104%''
										OR AccountCode LIKE ''0105%''
										OR AccountCode LIKE ''0106%'')
										AND (numClassIDDetail=',ISNULL(@numAccountClass,0),' OR ',ISNULL(@numAccountClass,0),' = 0)
										AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),0),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ',@pivotcolumns,' )
					) AS P 
					ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS DECIMAL(20,5) = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -@monProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE 
							ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.bitTotal,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
			ISNULL(SUM(Amount),0) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + (CASE WHEN @bitAddTotalRow=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '' ELSE '%' END)			
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,COA.bitTotal
		UNION
		SELECT 
			'', 
			'-1',
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			1,
			'-1' AS Struc,
			2,
			ISNULL((SELECT 
						SUM(Debit) - SUM(Credit)
					FROM 
						VIEW_Journal_Master
					WHERE 
						numDomainID = @numDomainId
						AND (AccountCode LIKE '0101%' 
						OR AccountCode LIKE '0102%'
						OR AccountCode LIKE '0103%'
						OR AccountCode LIKE '0104%'
						OR AccountCode LIKE '0105%'
						OR AccountCode LIKE '0106%')
						AND (numClassIDDetail=ISNULL(@numAccountClass,0) OR ISNULL(@numAccountClass,0) = 0)
						AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) AS Total
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItems')
DROP PROCEDURE USP_GetWareHouseItems
GO



CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL,
@numWarehouseID numeric(18,0)=0,
@numWLocationID AS NUMERIC(18,0)=0 --05052018 Change: Warehouse Location added

                         
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
DECLARE @bitAssembly BIT
DECLARE @bitMatrix BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END )
	,@bitMatrix=bitMatrix
	,@bitAssembly=ISNULL(bitAssembly,0)
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


DECLARE @ID AS NUMERIC(18,0)  = 0                       
DECLARE @numCusFlDItemID AS VARCHAR(20)                        
DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        
DECLARE @Fld_ValueMatrix NUMERIC(18,0)
DECLARE @Fld_ValueNameMatrix VARCHAR(300)

IF @bitMatrix = 1
BEGIN
	SET @ColName='I.numItemCode,1' 

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.fld_id) ID
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value
		,CASE 
			WHEN CFW_Fld_Master.fld_type='SelectBox' THEN REPLACE(dbo.GetListIemName(Fld_Value),'''','''''')
			WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
			ELSE CAST(Fld_Value AS VARCHAR)
		END AS FLD_ValueName
	INTO 
		#tempTableMatrix
	FROM
		CFW_Fld_Master 
	INNER JOIN
		ItemGroupsDTL 
	ON 
		CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
		AND ItemGroupsDTL.tintType = 2
	LEFT JOIN
		ItemAttributes
	ON
		CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
		AND ItemAttributes.numItemCode = @numItemCode
	LEFT JOIN
		ListDetails LD
	ON
		CFW_Fld_Master.numlistid = LD.numListID
	WHERE
		CFW_Fld_Master.numDomainID = @numDomainId
		AND ItemGroupsDTL.numItemGroupID = @numItemGroupID
	GROUP BY
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.bitAutocomplete
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 

	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM #tempTableMatrix
                         
	WHILE @i <= @iCount
	 BEGIN                        
		SELECT @fld_label=Fld_label,@Fld_ValueMatrix=FLD_Value,@Fld_ValueNameMatrix=FLD_ValueName FROM #tempTableMatrix WHERE ID=@i
                          
		SET @str = @str+ CONCAT (',',@Fld_ValueMatrix,' as [',@fld_label,']')
	
		IF @byteMode=1                                        
			SET @str = @str+ CONCAT (',''',@Fld_ValueNameMatrix,''' as [',@fld_label,'Value]')                                   
                          
		SET @i = @i + 1
	 END
END
ELSE
BEGIN
	SET @ColName='WareHouseItems.numWareHouseItemID,0' 

	--Create a Temporary table to hold data                                                            
	create table #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                                                                      
		numCusFlDItemID NUMERIC(9),
		Fld_Value VARCHAR(20)                                                         
	)

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		numOppAccAttrID
		,''
	FROM 
		ItemGroupsDTL 
	WHERE 
		numItemGroupID=@numItemGroupID 
		AND tintType=2    

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END          
                   
                          
                 
                      
                         
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) AS TotalOnHand,
(ISNULL(WareHouseItems.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
CAST(ISNULL(numReorder,0) AS FLOAT) as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 OR @bitAssembly=1 THEN 0 ELSE CAST(ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppID=OM.numOppID WHERE OM.numDomainID=' + CAST(@numDomainID AS VARCHAR) + ' AND tintOppType=1 AND tintOppStatus=0 AND OI.numItemCode=WareHouseItems.numItemID AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) END as [Requisitions]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',ISNULL(@bitLot,0),')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute,
I.numItemcode,
ISNULL(I.bitKitParent,0) AS bitKitParent,
ISNULL(I.bitSerialized,0) AS bitSerialized,
ISNULL(I.bitLotNo,0) as bitLotNo,
ISNULL(I.bitAssembly,0) as bitAssembly,
ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) [IsDeleteKitWarehouse],
CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END AS bitChildItemWarehouse,
I.numAssetChartAcntId,
(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'    
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'        
   
print CAST(@str1 AS NTEXT)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@bitAssembly bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @bitAssembly, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID
,WDTL.numWareHouseItemID
,vcSerialNo
,WDTL.vcComments as Comments
,WDTL.numQty
,WDTL.numQty as OldQty
,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )' 
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'   
                         
print CAST(@str1 AS NTEXT)                      
exec (@str1)                       
                      
IF @bitMatrix = 1
BEGIN
	SELECT
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTableMatrix

	DROP TABLE #tempTableMatrix
END
ELSE
BEGIN                      
	SELECT 
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master
	ON 
		numCusFlDItemID=Fld_ID                      

	DROP TABLE #tempTable
END

                      


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else'' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetItemDetails')
DROP PROCEDURE USP_Import_GetItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS  
BEGIN
	SELECT
		Item.numItemCode
		,Item.numShipClass
		,Item.vcItemName
		,Item.monListPrice
		,Item.txtItemDesc
		,Item.vcModelID
		,Item.vcManufacturer
		,Item.numBarCodeId
		,Item.fltWidth
		,Item.fltHeight
		,Item.fltWeight
		,Item.fltLength
		,Item.monAverageCost
		,Item.bitSerialized
		,Item.bitKitParent
		,Item.bitAssembly
		,Item.IsArchieve
		,Item.bitLotNo
		,Item.bitAllowBackOrder
		,Item.numIncomeChartAcntId
		,Item.numAssetChartAcntId
		,Item.numCOGsChartAcntId
		,Item.numItemClassification
		,Item.numItemGroup
		,Item.numPurchaseUnit
		,Item.numSaleUnit
		,Item.numItemClass
		,Item.numBaseUnit
		,Item.vcSKU
		,Item.charItemType
		,Item.bitTaxable
		,ItemExtendedDetails.txtDesc
		,Item.vcExportToAPI
		,Item.bitAllowDropShip
		,Item.bitMatrix
		,ISNULL((SELECT MAX(numReOrder) FROM WarehouseItems WHERE numItemID=Item.numItemCOde),0) numReOrder
	FROM
		Item
	LEFT JOIN
		ItemExtendedDetails
	ON
		Item.numItemCode = ItemExtendedDetails.numItemCode
	WHERE
		numDomainID = @numDomainID
		AND Item.numItemCode = @numItemCode
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
	,@numReOrder FLOAT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,NULLIF(@numIncomeChartAcntId,0)
			,NULLIF(@numAssetChartAcntId,0)
			,NULLIF(@numCOGsChartAcntId,0)
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=NULLIF(@numCOGsChartAcntId,0),numAssetChartAcntId=NULLIF(@numAssetChartAcntId,0),                                      
			numIncomeChartAcntId=NULLIF(@numIncomeChartAcntId,0),fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 

	UPDATE WareHouseItems SET numReorder=ISNULL(@numReOrder,0) WHERE numDomainID=@numDomain AND numItemID=@numItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

/****** Object:  StoredProcedure [dbo].[USP_InsertDepositHeaderDet]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertdepositheaderdet')
DROP PROCEDURE usp_insertdepositheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertDepositHeaderDet]                              
(@numDepositId AS NUMERIC(9)=0,        
@numChartAcntId AS NUMERIC(9)=0,                             
@datEntry_Date AS DATETIME,                                          
@numAmount AS DECIMAL(20,5),        
@numRecurringId AS NUMERIC(9)=0,                               
@numDomainId AS NUMERIC(9)=0,              
@numUserCntID AS NUMERIC(9)=0,
@strItems TEXT=''
,@numPaymentMethod NUMERIC
,@vcReference VARCHAR(500)
,@vcMemo VARCHAR(1000)
,@tintDepositeToType TINYINT=1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,@numDivisionID NUMERIC
,@tintMode TINYINT =0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,@tintDepositePage TINYINT --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,@numTransHistoryID NUMERIC(9)=0,
@numReturnHeaderID NUMERIC(9)=0,
@numCurrencyID numeric(9) =0,
@fltExchangeRate FLOAT = 1,
@numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
 SET @fltExchangeRate=1
 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
 

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

DECLARE  @hDocItem INT
 
 IF @tintMode != 2
 BEGIN
	 IF @numRecurringId=0 SET @numRecurringId=NULL 
	 IF @numDivisionID=0 SET @numDivisionID=NULL
	 IF @numReturnHeaderID=0 SET @numReturnHeaderID=NULL
	 
	 IF @numDepositId=0                                  
	  BEGIN                              
		  INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES            
	   (@numChartAcntId,@datEntry_Date,@numAmount,@numRecurringId,@numDomainId,@numUserCntID,GETUTCDATE(),@numPaymentMethod,@vcReference,@vcMemo,@tintDepositeToType,@numDivisionID,@tintDepositePage,@numTransHistoryID,@numReturnHeaderID,@numCurrencyID,ISNULL(@fltExchangeRate,1),@numAccountClass)                                        
	                         
	   SET @numDepositId = SCOPE_IDENTITY()                                        
	  END                            
	                        
	 IF @numDepositId<>0                        
	  BEGIN                        
		UPDATE DepositMaster SET numChartAcntId=@numChartAcntId,dtDepositDate=@datEntry_Date,monDepositAmount=@numAmount,numRecurringId=@numRecurringId,
	   numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE(),numPaymentMethod =@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo,tintDepositeToType =@tintDepositeToType,numDivisionID=@numDivisionID,tintDepositePage=@tintDepositePage,numTransHistoryID=@numTransHistoryID ,numCurrencyID=@numCurrencyID,fltExchangeRate=@fltExchangeRate
	   WHERE numDepositId=@numDepositId AND numDomainId=@numDomainId
	  END            
 END 
  
  IF @tintMode = 1
  BEGIN
  
 
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
				-- When user unchecks deposit entry of undeposited fund, reset original entry
				UPDATE dbo.DepositMaster 
				SET bitDepositedToAcnt = 0 WHERE numDepositId IN (  
				
				 SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numChildDepositID>0 
				 AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
												WITH(numDepositeDetailID NUMERIC(9))X)
				
				)
			
			
              DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
              AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
				 FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
				WITH(numDepositeDetailID NUMERIC(9))X)
			
			  INSERT INTO dbo.DepositeDetails
					  ( 
						numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate
					  )
			  SELECT 
					 @numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,GETUTCDATE(),GETUTCDATE()
			  FROM   (SELECT *
					  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID=0]', 2)
								WITH (
								numDepositeDetailID NUMERIC,
								numChildDepositID NUMERIC(9),
								monAmountPaid DECIMAL(20,5),
								numPaymentMethod NUMERIC(18, 0),
								vcMemo VARCHAR(1000),
								vcReference VARCHAR(500),
								numClassID	NUMERIC(18, 0),	
								numProjectID	NUMERIC(18, 0),
								numReceivedFrom	NUMERIC(18, 0),
								numAccountID NUMERIC(18, 0)
										)) X 
								
				UPDATE dbo.DepositeDetails
				SET
						[monAmountPaid] = X.monAmountPaid,
						[numChildDepositID] = X.numChildDepositID,
						[numPaymentMethod] = X.numPaymentMethod,
						[vcMemo] = X.vcMemo,
						[vcReference] = X.vcReference,
						[numClassID] = X.numClassID,
						[numProjectID] = X.numProjectID,
						[numReceivedFrom] = X.numReceivedFrom,
						[numAccountID] = X.numAccountID,
						dtModifiedDate=GETUTCDATE()
				FROM (SELECT *
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID>0]', 2)
												WITH (
												numDepositeDetailID NUMERIC,
												numChildDepositID NUMERIC(9),
												monAmountPaid DECIMAL(20,5),
												numPaymentMethod NUMERIC(18, 0),
												vcMemo VARCHAR(1000),
												vcReference VARCHAR(500),
												numClassID	NUMERIC(18, 0),	
												numProjectID	NUMERIC(18, 0),
												numReceivedFrom	NUMERIC(18, 0),
												numAccountID NUMERIC(18, 0)
														)) X 
				WHERE X.numDepositeDetailID =DepositeDetails.numDepositeDetailID AND dbo.DepositeDetails.numDepositID = @numDepositId




				UPDATE dbo.DepositMaster SET bitDepositedToAcnt=1 WHERE numDomainId=@numDomainID AND  numDepositId IN (
									   SELECT numChildDepositID
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
												WITH (numChildDepositID NUMERIC(9)) WHERE numChildDepositID > 0
				)
				
								
  								
          EXEC sp_xml_removedocument
            @hDocItem
        END
  
  
  END 
  
  
  
  IF @tintMode = 0 OR @tintMode = 2
  BEGIN
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
				  EXEC sp_xml_preparedocument
					@hDocItem OUTPUT ,
					@strItems
						
					  SELECT 
					  numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid INTO #temp
					  FROM   (SELECT *
							  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH (numDepositeDetailID NUMERIC(9),numOppBizDocsID NUMERIC(9),numOppID NUMERIC(9),monAmountPaid DECIMAL(20,5))) X
				 	
					
					IF @tintMode = 0
					BEGIN
						UPDATE OBD
						SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
						FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
						WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
							 AND DD.numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM #temp WHERE numDepositeDetailID>0)
														
													
						  DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
						  AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
									FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
									WITH(numDepositeDetailID NUMERIC(9))X)
--							 AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
--														FROM #temp WHERE numDepositeDetailID>0)
					 END
		              
		              
					  UPDATE  DD SET [monAmountPaid] = DD.monAmountPaid +  X.monAmountPaid 
								FROM DepositeDetails DD JOIN #temp X ON DD.numOppBizDocsID=X.numOppBizDocsID AND 
								DD.numOppID = X.numOppID
								WHERE X.numDepositeDetailID = 0 AND DD.numDepositID = @numDepositId
										
										
					  INSERT INTO dbo.DepositeDetails
							  ( numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate )
					  SELECT @numDepositId, numOppBizDocsID , numOppID , monAmountPaid,GETUTCDATE(),GETUTCDATE()
					  FROM #temp WHERE numDepositeDetailID=0 AND numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositId)
										
						
						IF @tintMode = 0
						BEGIN
								UPDATE  dbo.DepositeDetails
								SET     [monAmountPaid] = X.monAmountPaid ,
										numOppBizDocsID = X.numOppBizDocsID ,
										numOppID = X.numOppID,
										dtModifiedDate=GETUTCDATE()
								FROM    #temp X
								WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
										AND dbo.DepositeDetails.numDepositID = @numDepositId
						END


					UPDATE dbo.OpportunityBizDocs 
					SET dbo.OpportunityBizDocs.monAmountPaid = (SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numOppID),numModifiedBy=@numUserCntID
					WHERE  OpportunityBizDocs.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)

					--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,7	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

					--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,14	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

														
  					UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  							WHERE numDepositId=@numDepositId
		  				
		  								
  					DROP TABLE #temp
		  								
				  EXEC sp_xml_removedocument
					@hDocItem
        END
        
        ELSE
        BEGIN
			--IF @tintMode = 0
			--BEGIN
				UPDATE OBD
				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
				WHERE DD.numDepositID = @numDepositId 
											
				DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
				  
				UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  					WHERE numDepositId=@numDepositId
  		  
			--END
        END 
    END
  
  SELECT @numDepositId   
  
  BEGIN TRY
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocsId NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocsId
	)
	SELECT 
		numOppBizDocsId 
	FROM 
		DepositeDetails 
	WHERE 
		numDepositId=@numDepositId
		AND ISNULL(numOppBizDocsId,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	DECLARE @numOppBizDocsId NUMERIC(18,0)

	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @numOppBizDocsId=numOppBizDocsId FROM @TEMP WHERE ID=@i
		EXEC USP_OpportunityBizDocs_CT @numDomainID,@numUserCntID,@numOppBizDocsId
		SET @i = @i + 1
	END

	
  END TRY
  BEGIN CATCH
	-- DO NOT RAISE ERROR
  END CATCH      
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseAvailability')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseAvailability
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseAvailability]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@tintWarehouseAvailability TINYINT
	,@bitDisplayQtyAvailable BIT
)
AS
BEGIN
	SELECT STUFF((SELECT
					CONCAT(', ',vcWareHouse,(CASE WHEN @bitDisplayQtyAvailable=1 THEN CONCAT('(',numOnHand,')') ELSE '' END))
				FROM
				(
					SELECT 
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
						,SUM(numOnHand) numOnHand
					FROM
						WarehouseItems
					INNER JOIN
						Warehouses
					ON
						WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
					WHERE
						WarehouseItems.numDomainID=@numDomainID
						AND WarehouseItems.numItemID=@numItemCode
					GROUP BY
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
				) TEMP
				WHERE
					1 = (CASE WHEN @tintWarehouseAvailability=2 THEN (CASE WHEN Temp.numOnHand > 0 THEN 1 ELSE 0 END) ELSE 1 END)
				ORDER BY
					vcWareHouse
				FOR XML PATH('')),1,1,'')
END
/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListForBankReconciliation]    Script Date: 05/07/2009 22:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_EntryListForBankReconciliation')
DROP PROCEDURE USP_Journal_EntryListForBankReconciliation
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListForBankReconciliation]
               @numChartAcntId AS NUMERIC(9),
               @tintFlag       TINYINT=0,
               @numDomainId    AS NUMERIC(9),
               @bitDateHide AS BIT=0,
               @numReconcileID AS NUMERIC(9),
               @tintReconcile AS TINYINT=0 --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
AS
  BEGIN
  
  DECLARE @numCreditAmt NUMERIC(9)
  DECLARE @numDebitAmt NUMERIC(9)
  DECLARE @dtStatementDate AS DATETIME
  
  SELECT @dtStatementDate=dtStatementDate FROM BankReconcileMaster WHERE numReconcileID=@numReconcileID

  create table #TempBankRecon
  (numChartAcntId numeric(18),
  numTransactionId numeric(18),
  bitReconcile bit,
  EntryDate date,
  numDivisionID NUMERIC(18,0),
  CompanyName varchar(500),
  Deposit float,
  Payment float,
  Memo  varchar(250),
  TransactionType varchar(100),
  JournalId numeric(18),
  numCheckHeaderID numeric(18),
  vcReference varchar(1000),
  tintOppType INTEGER,bitCleared BIT,numCashCreditCardId NUMERIC(18),numOppId NUMERIC(18),numOppBizDocsId NUMERIC(18),
  numDepositId NUMERIC(18),numCategoryHDRID NUMERIC(9),tintTEType TINYINT,numCategory NUMERIC(18),numUserCntID NUMERIC(18),dtFromDate DATETIME,
  numBillId NUMERIC(18),numBillPaymentID NUMERIC(18),numLandedCostOppId NUMERIC(18),bitMatched BIT,vcCheckNo VARCHAR(20),tintType TINYINT,vcOrder VARCHAR(10)) /* tintType: 0=No Match, 3=Perfect Match, 2=only amount match with single bank statement row, 1 = only amount match with multiple bank statement row  */
  
  IF (@tintFlag = 1)
	SET @numDebitAmt = 0
  ELSE 	IF (@tintFlag = 2)
	SET @numCreditAmt = 0 
  
  insert into #TempBankRecon
  
    SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   ISNULL(GJD.bitReconcile,0) bitReconcile,	
           GJH.datEntry_Date AS EntryDate,
		   ISNULL(DM.numDivisionID,0),
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
			(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           GJD.varDescription as Memo,
            case when isnull(GJH.numCheckHeaderID,0) <> 0 THEN case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
					when 0 then 'Cash' else 'Checks' end 			
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then 'Cash'
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then 'Charge'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then 'BizDocs Invoice'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then 'BizDocs Purchase'
			when isnull(GJH.numDepositId,0) <> 0 then 'Deposit'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then 'Receive Amt'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then 'Vendor Amt'
			when isnull(GJH.numCategoryHDRID,0)<>0 then 'Time And Expenses'
			When ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			When ISNULL(GJH.numBillId,0) <>0 then 'Bill' 
			When ISNULL(GJH.numBillPaymentID,0) <>0 then 'Pay Bill' 
			When GJH.numJournal_Id <>0 then 'Journal' End  as TransactionType,
			GJD.numJournalId AS JournalId,
			isnull(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			ISNULL((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID=GJH.numDomainID AND numBizDocsPaymentDetId = isnull(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) vcReference,
			isnull(Opp.tintOppType,0),ISNULL(GJD.bitCleared,0) AS bitCleared,
			ISNULL(GJH.numCashCreditCardId,0) AS numCashCreditCardId,ISNULL(GJH.numOppId,0) AS numOppId,
			isnull(GJH.numOppBizDocsId,0) AS numOppBizDocsId,isnull(GJH.numDepositId,0) AS numDepositId,
			isnull(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,ISNULL(GJH.numBillId,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
			0 AS bitMatched
			,ISNULL(CAST(CheckHeader.numCheckNo AS VARCHAR),'')
			,0
			,''
    FROM   General_Journal_Header GJH
           INNER JOIN General_Journal_Details GJD
             ON GJH.numJournal_Id = GJD.numJournalId
           LEFT OUTER JOIN DivisionMaster AS DM
             ON GJD.numCustomerId = DM.numDivisionID
           LEFT OUTER JOIN CompanyInfo AS CI
             ON DM.numCompanyID = CI.numCompanyId
            Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
			Left outer join (select * from OpportunityMaster WHERE tintOppType=1 ) Opp on GJH.numOppId=Opp.numOppId    
			LEFT OUTER JOIN TimeAndExpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			LEFT OUTER JOIN CheckHeader ON GJH.numCheckHeaderID = CheckHeader.numCheckHeaderID
    WHERE  
		   ((GJH.[datEntry_Date] <= @dtStatementDate AND @bitDateHide=1) OR @bitDateHide=0)
           AND GJD.numChartAcntId = @numChartAcntId
           AND GJD.numDomainId = @numDomainId
           AND (GJD.numCreditAmt <> @numCreditAmt OR @numCreditAmt IS null)
           AND (GJD.numDebitAmt <> @numDebitAmt OR @numDebitAmt IS null)
		   AND (ISNULL(GJH.numReconcileID,0) = 0 OR (ISNULL(GJH.numReconcileID,0) = @numReconcileID  AND ISNULL(bitReconcileInterest,0) = 1))
           AND 1=(CASE WHEN @tintReconcile=0 THEN CASE WHEN ISNULL(GJD.bitReconcile,0)=0 AND (ISNULL(GJD.numReconcileID,0)=0 OR ISNULL(GJD.numReconcileID,0)=@numReconcileID) THEN 1 ELSE 0 END
					   WHEN @tintReconcile=1 THEN CASE WHEN ISNULL(GJD.numReconcileID,0)=@numReconcileID THEN 1 ELSE 0 END
					   WHEN @tintReconcile=2 THEN CASE WHEN  ((ISNULL(GJD.bitReconcile,0)=1 OR ISNULL(GJD.bitCleared,0)=1) AND ISNULL(GJD.numReconcileID,0)=@numReconcileID) OR (ISNULL(GJD.bitReconcile,0)=0 AND ISNULL(GJD.numReconcileID,0)=0) THEN 1 ELSE 0 END END)
			ORDER BY GJH.datEntry_Date

	SELECT
		B1.ID,
		dbo.FormatedDateFromDate(B1.dtEntryDate,@numDomainID) as dtEntryDate,
		ISNULL(vcReference,'') as [vcReference],
		[fltAmount],
		ISNULL(vcPayee,'') as vcPayee,
		ISNULL(vcDescription,'') as vcDescription,
		ISNULL(B1.bitCleared,0) bitCleared,
		ISNULL(B1.bitReconcile,0) bitReconcile,
		0 AS numBizTransactionId,
		0 AS bitMatched,
		0 AS bitDuplicate,
		0 AS tintType
	INTO 
		#TempStatememnt
	FROM
		BankReconcileFileData B1
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(B1.bitReconcile,0) = 0

	IF EXISTS (SELECT * FROM BankReconcileMatchRule WHERE numDomainID = @numDomainId AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,',')))
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numRuleID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numRuleID
		)
		SELECT
			ID
		FROM
			BankReconcileMatchRule
		WHERE
			numDomainID = @numDomainId
			AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,','))
		ORDER BY
			tintOrder


		DECLARE @i AS INT = 1
		DECLARE @numRuleID AS NUMERIC(18,0)
		DECLARE @bitMatchAllConditions AS BIT
		DECLARE @Count AS INT 
		SELECT @Count = COUNT(*) FROM @TEMP

		WHILE @i <= @Count
		BEGIN
			SELECT @numRuleID=numRuleID,@bitMatchAllConditions=bitMatchAllConditions FROM @TEMP T1 INNER JOIN BankReconcileMatchRule BRMR ON T1.numRuleID=BRMR.ID WHERE T1.ID = @i 

			UPDATE
				TS
			SET 
				numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
				bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
				bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
				tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
			FROM
				#TempStatememnt TS
			INNER JOIN
				BankReconcileFileData B1
			ON
				TS.ID = B1.ID
				AND ISNULL(TS.numBizTransactionId,0) = 0
				AND ISNULL(TS.bitMatched,0) = 0
			OUTER APPLY
			(
				SELECT TOP 1
					t1.numTransactionId
				FROM
					#TempBankRecon t1
				OUTER APPLY
				(
					SELECT 
						(CASE 
							WHEN @bitMatchAllConditions = 1
							THEN
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 0 ELSE 1 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN LOWER((CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END)) LIKE '%' + LOWER(BRMRC.vcTextToMatch) + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T3
								WHERE
									T3.isMatched = 0)
							ELSE
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T4
								WHERE
									T4.isMatched = 1)
						END) AS bitMatched
				) TEMPConditions
				WHERE
					t1.EntryDate = B1.dtEntryDate
					AND TEMPConditions.bitMatched = 1
					AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
					AND ISNULL(t1.bitMatched,0) = 0
			) AS TEMP1
			OUTER APPLY
			(
				SELECT TOP 1
					t1.ID
				FROM
					BankReconcileFileData t1
				WHERE
					t1.numReconcileID <> @numReconcileID
					AND t1.numDomainID=@numDomainId
					AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
					AND t1.dtEntryDate = B1.dtEntryDate
					AND B1.vcPayee = t1.vcPayee
					AND B1.fltAmount = t1.fltAmount
					AND B1.vcReference = t1.vcReference
					AND B1.vcDescription=t1.vcDescription
			) AS TEMP2
			WHERE
				numReconcileID=@numReconcileID
				

			UPDATE 
				t2
			SET
				bitMatched = 1,
				tintType = 3,
				vcOrder=CONCAT('D',t1.ID)
			FROM
				#TempBankRecon t2
			INNER JOIN
				#TempStatememnt t1
			ON
				t2.numTransactionId=ISNULL(t1.numBizTransactionId,0)
			WHERE 
				ISNULL(t2.bitMatched,0)=0 

			SET @i = @i + 1
		END
	END

	UPDATE
		TS
	SET 
		numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
		bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
		bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
		tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
	FROM
		#TempStatememnt TS
	INNER JOIN
		BankReconcileFileData B1
	ON
		TS.ID = B1.ID
		AND ISNULL(TS.numBizTransactionId,0) = 0
		AND ISNULL(TS.bitMatched,0) = 0
	OUTER APPLY
	(
		SELECT TOP 1
			t1.numTransactionId
		FROM
			#TempBankRecon t1
		WHERE
			t1.EntryDate = B1.dtEntryDate
			AND (CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcPayee)) > 0 OR CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcDescription)) > 0 OR (CHARINDEX('CHECK',UPPER(B1.vcDescription)) > 0 AND LEN(t1.vcCheckNo) > 0 AND CHARINDEX(UPPER(t1.vcCheckNo),UPPER(B1.vcDescription)) > 0))
			AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
	) AS TEMP1
	OUTER APPLY
	(
		SELECT TOP 1
			t1.ID
		FROM
			BankReconcileFileData t1
		WHERE
			t1.numReconcileID <> @numReconcileID
			AND t1.numDomainID=@numDomainId
			AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
			AND t1.dtEntryDate = B1.dtEntryDate
			AND B1.vcPayee = t1.vcPayee
			AND B1.fltAmount = t1.fltAmount
			AND B1.vcReference = t1.vcReference
			AND B1.vcDescription=t1.vcDescription
	) AS TEMP2
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(Ts.bitMatched,0) = 0

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('D',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY SINGAL JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		bitMatched = 1
		,tintType = 2
		,numBizTransactionId = (SELECT numTransactionId FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount))
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt .fltAmount OR Payment*-1=#TempStatememnt .fltAmount)) = 1

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('C',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY MULTIPLE JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		tintType = 1
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount)) > 1

	UPDATE 
		t2
	SET 
		tintType=1
		,vcOrder=CONCAT('B',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		ISNULL(t1.tintType,0)=1
		AND (t2.Deposit = t1.fltAmount OR t2.Payment*-1=t1.fltAmount)
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	 
	SELECT * FROM #TempBankRecon ORDER BY tintType DESC, vcOrder ASC,EntryDate ASC
	SELECT * FROM #TempStatememnt WHERE ISNULL(bitDuplicate,0)=0 ORDER BY tintType DESC

	SELECT 
		(SELECT COUNT(*) FROM #TempStatememnt) AS TotalRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitDuplicate=1) AS DuplicateRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitMatched=1) AS MatchedRecords

	drop table #TempBankRecon    
	drop table #TempStatememnt       
  END
--SELECT * FROM [AccountTypeDetail] Where len(vcaccountcode)>4
--DELETE FROM [AccountTypeDetail] WHERE [numAccountTypeID]>2232
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAccountType')
DROP PROCEDURE USP_ManageAccountType
GO
CREATE PROCEDURE USP_ManageAccountType
(@numAccountTypeID NUMERIC(9)=0 OUTPUT,
@vcAccountCode varchar(50),
@vcAccountType varchar(100),
@numParentID NUMERIC(9)=null,
@numDomainID NUMERIC(9),
@tintMode TINYINT,
@bitActive BIT = 1)
AS 
BEGIN
DECLARE @newAccountCode VARCHAR(50)
IF @tintMode = 1
BEGIN

	SELECT @newAccountCode=dbo.GetAccountTypeCode(@numDomainID,@numParentID,0);
	INSERT INTO AccountTypeDetail (
		vcAccountCode,
		vcAccountType,
		numParentID,
		numDomainID,
		dtCreateDate,
		dtModifiedDate,
		bitSystem,
		bitActive
	) VALUES ( 
		@newAccountCode,
		@vcAccountType,
		@numParentID,
		@numDomainID,
		GETUTCDATE(),
		GETUTCDATE(),
		0,
		ISNULL(@bitActive,1))
	SET @numAccountTypeID = SCOPE_IDENTITY()
END

IF @tintMode = 2
BEGIN
	IF	(SELECT  LEN(vcAccountCode) FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID) > 4
	BEGIN
--		SELECT @newAccountCode= dbo.GetAccountTypeCode(@numDomainID,@numParentID);
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=@newAccountCode,
--			vcAccountType=@vcAccountType,
--			numParentID=@numParentID
--		WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID
--		-- Update Child if any 
		
		
--Commented by chintan, reason:any account faling under given account type is using prefix of account type code,when changing account type code,then child account codes become invalid.
--		EXEC USP_UpdateAccountTypeCode @numDomainID,@numAccountTypeID,@numParentID
		
		UPDATE [AccountTypeDetail] SET [vcAccountType]  = @vcAccountType,bitActive=ISNULL(@bitActive,1) WHERE [numAccountTypeID]=@numAccountTypeID AND [numDomainID]=@numDomainID
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=dbo.GetAccountTypeCode(numDomainID,@numAccountTypeID),
--			numParentID=@numAccountTypeID
--		WHERE numParentID=@numAccountTypeID AND numDomainID=@numDomainID
--		
	END
		
END

IF @tintMode = 3
BEGIN
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE bitSystem=1 AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('SYSTEM', 16, 1);
			RETURN
		END
		
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE (vcAccountCode LIKE '01020102%' OR vcAccountCode LIKE '01010105') AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('AR/AP', 16, 1);
			RETURN
		END
		IF EXISTS(SELECT * FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId=@numAccountTypeID)
		BEGIN
			RAISERROR('CHILD_ACCOUNT', 16, 1);
			RETURN
		END
		IF (SELECT COUNT(*) FROM [AccountTypeDetail] WHERE numParentID=@numAccountTypeID) >0
		BEGIN
			RAISERROR('CHILD', 16, 1);
			RETURN
		END
		ELSE
		BEGIN
		--only allow deltion of where account code lenth is > 4
			DELETE FROM [AccountTypeDetail] WHERE numDomainID=@numDomainID AND  numAccountTypeID=@numAccountTypeID AND LEN(vcAccountCode) > 4	
		END
		

END


	
	
	
END

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = '',
 @vcTaxID VARCHAR(100) = ''
AS   
	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END             
	
	IF @numDivisionId = ISNULL((SELECT numDivisionID FROM Domain WHERE numDomainID=@numDomainID),0)                            
	BEGIN
		EXEC usp_SetUsersWithDomains
			@numUserID= 0,                                    
			@vcUserName = @vcGivenName,                                    
			@vcUserDesc = '',                              
			@numGroupId = 0,                                           
			@numUserDetailID = @numcontactId,                              
			@numUserCntID = @numUserCntID,                              
			@strTerritory = '' ,                              
			@numDomainID = @numDomainID,
			@strTeam = '',
			@vcEmail = @vcEmail,
			@vcPassword = '',          
			@Active  = 0,
			@numDefaultClass = 0,
			@numDefaultWarehouse = 0,
			@vcLinkedinId = null,
			@intAssociate = 0,
			@ProfilePic =null
			,@bitPayroll = 0
			,@monHourlyRate = 0
	END
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_ManageAuthorizationGroup]    Script Date: 07/26/2008 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageauthorizationgroup')
DROP PROCEDURE usp_manageauthorizationgroup
GO
CREATE PROCEDURE [dbo].[USP_ManageAuthorizationGroup]
@numDomainID as numeric(9)=0,
@numGroupID as numeric(9)=0,
@vcGroupName as varchar(100)='',
@tinTGroupType as tinyint=null
AS
IF @numGroupID =0
BEGIN
	INSERT INTO AuthenticationGroupMaster 
	(
		vcGroupName,
		numDomainID,
		tintGroupType
	)
	VALUES
	(
		@vcGroupName,
		@numDomainID,
		@tinTGroupType
	)

	SELECT @numGroupID = SCOPE_IDENTITY()

	--MainTab From TabMaster 
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,1,0,1,1,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,36,0,1,2,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,7,0,1,3,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,80,0,1,4,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,3,0,1,5,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,45,0,1,6,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,4,0,1,7,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,44,0,1,8,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,6,0,1,9,0)

	IF (SELECT COUNT(*) FROM TreeNavigationAuthorization WHERE numDomainID=@numDomainID AND numGroupID=@numGroupID) = 0
	BEGIN
		INSERT  INTO dbo.TreeNavigationAuthorization
		(
			numGroupID,
			numTabID,
			numPageNavID,
			bitVisible,
			numDomainID,
			tintType
		)
		SELECT  
			@numGroupID as numGroupID,
			PND.numTabID,
			numPageNavID,
			1,
			@numDomainID AS [numDomainID],
			@tinTGroupType AS [tintType]
		FROM    
			dbo.PageNavigationDTL PND
		WHERE
			PND.numTabID IS NOT NULL
		UNION ALL 
		SELECT  
			numGroupID AS [numGroupID],
			1 AS [numTabID],
			ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
			1 AS [bitVisible],
			@numDomainID AS [numDomainID],
			@tinTGroupType AS [tintType]
		FROM    
			dbo.ListDetails LD
		CROSS JOIN 
			dbo.AuthenticationGroupMaster AG
		WHERE   
			numListID = 27
			AND ISNULL(constFlag, 0) = 1
			AND AG.numDomainID = @numDomainID
			AND AG.numGroupID=@numGroupID
			AND numListItemID NOT IN (293,294,295,297,298,299)
	END
END
ELSE
BEGIN
	UPDATE 
		AuthenticationGroupMaster 
	SET 
		vcGroupName=@vcGroupName,
		numDomainID=@numDomainID,
		tintGroupType=@tinTGroupType
	WHERE 
		numGroupID=@numGroupID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance DECIMAL(20,5),
    @monEndBalance DECIMAL(20,5),
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount DECIMAL(20,5),
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount DECIMAL(20,5),
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS DECIMAL(20,5),@numDebitAmt AS DECIMAL(20,5) 
		
		SELECT 
			@numCreditAmt=ISNULL(SUM(numCreditAmt),0)
			,@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
		FROM 
			General_Journal_Details 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID 
			AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
			AND numJournalId NOT IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0)
		OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))
	
		SELECT 
			[numReconcileID]
			,[numDomainID]
			,[numCreatedBy]
			,[dtCreatedDate]
			,[dtStatementDate]
			,[monBeginBalance]
			,[monEndBalance]
			,[numChartAcntId]
			,[monServiceChargeAmount]
			,[numServiceChargeChartAcntId]
			,[dtServiceChargeDate]
			,[monInterestEarnedAmount]
			,[numInterestEarnedChartAcntId]
			,[dtInterestEarnedDate]
			,[bitReconcileComplete]
			,[dtReconcileDate]
			,@numCreditAmt AS Payment
			,@numDebitAmt AS Deposit
			,dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby
			,dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
		FROM 
			[BankReconcileMaster] 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillPayment' ) 
    DROP PROCEDURE USP_ManageBillPayment
GO

CREATE PROCEDURE [dbo].[USP_ManageBillPayment]
    @numBillPaymentID NUMERIC(18, 0) =0 output,
    @dtPaymentDate DATETIME ,
    @numAccountID NUMERIC(18, 0) ,
    @numPaymentMethod NUMERIC(18, 0) ,
    @numUserCntID NUMERIC(18, 0) ,
    @numDomainID NUMERIC(18, 0) ,
    @strItems TEXT,
    @tintMode TINYINT=0,
    @monPaymentAmount DECIMAL(20,5)=0,
    @numDivisionID NUMERIC(18,0),
    @numReturnHeaderID NUMERIC(18,0),
    @numCurrencyID NUMERIC(18,0)=0,
    @fltExchangeRate FLOAT=1,
    @numAccountClass NUMERIC(18,0)=0
AS 
BEGIN
SET NOCOUNT ON
    DECLARE @numBillPaymentID_X NUMERIC(18,0)
    DECLARE @dtPaymentDate_X DATETIME
    DECLARE @numAccountID_X NUMERIC(18,0)
    DECLARE @numPaymentMethod_X NUMERIC(18,0)
    DECLARE @numUserCntID_X NUMERIC(18,0)
    DECLARE @numDomainID_X NUMERIC(18,0)
    DECLARE @strItems_X VARCHAR(MAX)
    DECLARE @tintMode_X TINYINT=0
    DECLARE @monPaymentAmount_X DECIMAL(20,5)
    DECLARE @numDivisionID_X NUMERIC(18,0)
    DECLARE @numReturnHeaderID_X NUMERIC(18,0)
    DECLARE @numCurrencyID_X NUMERIC(18,0)
    DECLARE @fltExchangeRate_X FLOAT
    DECLARE @numAccountClass_X NUMERIC(18,0)

    SET @numBillPaymentID_X = @numBillPaymentID
	SET @dtPaymentDate_X = @dtPaymentDate
	SET @numAccountID_X = @numAccountID
	SET @numPaymentMethod_X = @numPaymentMethod
	SET @numUserCntID_X = @numUserCntID
	SET @numDomainID_X = @numDomainID
	SET @strItems_X = @strItems
	SET @tintMode_X = @tintMode
	SET @monPaymentAmount_X = @monPaymentAmount
	SET @numDivisionID_X = @numDivisionID
	SET @numReturnHeaderID_X = @numReturnHeaderID
	SET @numCurrencyID_X = @numCurrencyID
	SET @fltExchangeRate_X = @fltExchangeRate
	SET @numAccountClass_X = @numAccountClass
    
BEGIN TRY 
        BEGIN TRAN  
        
			IF ISNULL(@numCurrencyID_X,0) = 0 
			BEGIN
			 SET @fltExchangeRate_X=1
			 SELECT @numCurrencyID_X= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID_X	
			END
 
        
			IF @tintMode_X=1
			BEGIN
				--Validation of closed financial year
				EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID_X,@dtPaymentDate_X
				
				
				IF EXISTS ( SELECT  [numBillPaymentID] FROM    [dbo].[BillPaymentHeader] WHERE   [numBillPaymentID] = @numBillPaymentID_X ) 
					BEGIN
						UPDATE  [dbo].[BillPaymentHeader] 
						SET     [dtPaymentDate] = @dtPaymentDate_X ,[numAccountID] = @numAccountID_X ,
								[numPaymentMethod] = @numPaymentMethod_X ,[numModifiedDate] = GETUTCDATE() ,
								[numModifiedBy] = @numUserCntID_X,monPaymentAmount=@monPaymentAmount_X,numCurrencyID=@numCurrencyID_X,fltExchangeRate=@fltExchangeRate_X
						WHERE   [numBillPaymentID] = @numBillPaymentID_X AND numDomainID =@numDomainID_X
					END
				ELSE 
					BEGIN
						INSERT  INTO [dbo].[BillPaymentHeader]
								( [dtPaymentDate] ,[numAccountID] ,[numPaymentMethod] ,[dtCreateDate] ,[numCreatedBy] ,[numModifiedDate] ,
								  [numModifiedBy],numDomainID,monPaymentAmount,numDivisionID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass
								)
						VALUES  ( 
								  @dtPaymentDate_X ,@numAccountID_X ,@numPaymentMethod_X ,GETUTCDATE() ,@numUserCntID_X ,GETUTCDATE() ,
								  @numUserCntID_X,@numDomainID_X,@monPaymentAmount_X,@numDivisionID_X,@numReturnHeaderID_X,@numCurrencyID_X,@fltExchangeRate_X,@numAccountClass_X
								)
						SET @numBillPaymentID = SCOPE_IDENTITY()
						SET @numBillPaymentID_X = @numBillPaymentID
					END
				END	
				
				
				DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems_X) <> '' 
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems_X
--						INSERT  INTO [dbo].[BillPaymentDetails]
--								( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount])
					SELECT  X.tintBillType ,ISNULL(X.numOppBizDocsID,0) AS  numOppBizDocsID,ISNULL(X.numBillID,0) AS numBillID ,X.monAmount INTO #temp
								FROM    ( SELECT    * FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH ( tintBillType TINYINT,numOppBizDocsID NUMERIC(18, 0),numBillID NUMERIC(18, 0),monAmount DECIMAL(20,5))
								) X
					
					 EXEC sp_xml_removedocument @hDocItem
					 
					IF @tintMode_X = 1
					BEGIN			
						--Revert back all amount for BillHeader
						 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
						 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
						 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
						 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
						 --Revert back all amount for OpportunityBizDocs
						UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
						FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
						WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
						--Delete all BillPaymentDetails entries 				 
						DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
					END		
						
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numBillID=X.numBillID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=2
				
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numOppBizDocsID=X.numOppBizDocsID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=1	
				
				INSERT  INTO [dbo].[BillPaymentDetails]
					( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount],dtAppliedDate)
				 SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=1 AND X.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
				UNION
				SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=2 AND X.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
						

			   UPDATE dbo.BillHeader SET monAmtPaid = monAmtPaid + X.monAmount,
					bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid + X.monAmount) = 0 THEN 1 ELSE 0 END 
					FROM  #temp X 
					WHERE X.numBillID>0 AND dbo.BillHeader.numBillID = X.numBillID  
			
									 
				UPDATE dbo.OpportunityBizDocs SET monAmountPaid = monAmountPaid + monAmount
						FROM #temp X  
						WHERE X.numOppBizDocsID>0 AND dbo.OpportunityBizDocs.numOppBizDocsId = X.numOppBizDocsID
				
				--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,15	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,16	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				DROP TABLE #temp
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
			ELSE IF @tintMode_X=2
			BEGIN
				--Revert back all amount for BillHeader
				 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
				 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
				 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
				 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
				--Revert back all amount for OpportunityBizDocs
				UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
				FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
				WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
				--Delete all BillPaymentDetails entries 				 
				DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

	BEGIN TRY
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocsId NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocsId
	)
	SELECT 
		numOppBizDocsId 
	FROM 
		BillPaymentDetails 
	WHERE 
		numBillPaymentID=@numBillPaymentID_X
		AND ISNULL(numOppBizDocsId,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	DECLARE @numOppBizDocsId NUMERIC(18,0)

	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @numOppBizDocsId=numOppBizDocsId FROM @TEMP WHERE ID=@i
		EXEC USP_OpportunityBizDocs_CT @numDomainID,@numUserCntID,@numOppBizDocsId
		SET @i = @i + 1
	END

	
  END TRY
  BEGIN CATCH
	-- DO NOT RAISE ERROR
  END CATCH   
END
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @intNoOfPartners AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(1000),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
    @tintLogin TINYINT,
    @intNoofPartialSubscribed AS INTEGER,
    @intNoofMinimalSubscribed AS INTEGER,
	@dtEmailStartDate DATETIME,
	@dtEmailEndDate DATETIME,
	@intNoOfEmail INT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT
AS 
    IF @numSubscriberID = 0 
        BEGIN                                 
            IF NOT EXISTS ( SELECT  *
                            FROM    Subscribers
                            WHERE   numDivisionID = @numDivisionID
                                    AND numDomainID = @numDomainID ) 
                BEGIN              
                    SET @bitExists = 0                              
                    DECLARE @vcCompanyName AS VARCHAR(100)                             
                    DECLARE @numCompanyID AS NUMERIC(9)                                   
                    SELECT  @vcCompanyName = vcCompanyName,
                            @numCompanyID = C.numCompanyID
                    FROM    CompanyInfo C
                            JOIN DivisionMaster D ON D.numCompanyID = C.numCompanyID
                    WHERE   D.numDivisionID = @numDivisionID                                    
                                
                    DECLARE @numNewCompanyID AS NUMERIC(9)                                   
                    DECLARE @numNewDivisionID AS NUMERIC(9)                                    
                    DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
                    INSERT  INTO Domain
                            (
                              vcDomainName,
                              vcDomainDesc,
                              numDivisionID,
                              numAdminID,
                              tintLogin,tintDecimalPoints,tintChrForComSearch,tintChrForItemSearch, bitIsShowBalance,tintComAppliesTo,tintCommissionType, charUnitSystem, bitExpenseNonInventoryItem
                            )
                    VALUES  (
                              @vcCompanyName,
                              @vcCompanyName,
                              @numDivisionID,
                              @numAdminContactID,
                              @tintLogin,2,1,1,0,3,1,'E',1
                            )                                    
                    SET @numTargetDomainID = SCOPE_IDENTITY()                                       
                                       
                    INSERT  INTO CompanyInfo
                            (
                              vcCompanyName,
                              numCompanyType,
                              numCompanyRating,
                              numCompanyIndustry,
                              numCompanyCredit,
                              vcWebSite,
                              vcWebLabel1,
                              vcWebLink1,
                              vcWebLabel2,
                              vcWebLink2,
                              vcWebLabel3,
                              vcWebLink3,
                              vcWeblabel4,
                              vcWebLink4,
                              numAnnualRevID,
                              numNoOfEmployeesId,
                              vcHow,
                              vcProfile,
                              bitPublicFlag,
                              numDomainID
                            )
                            SELECT  vcCompanyName,
                                    93,--i.e employer --numCompanyType,
                                    numCompanyRating,
                                    numCompanyIndustry,
                                    numCompanyCredit,
                                    vcWebSite,
                                    vcWebLabel1,
                                    vcWebLink1,
                                    vcWebLabel2,
                                    vcWebLink2,
                                    vcWebLabel3,
                                    vcWebLink3,
                                    vcWeblabel4,
                                    vcWebLink4,
                                    numAnnualRevID,
                                    numNoOfEmployeesId,
                                    vcHow,
                                    vcProfile,
                                    bitPublicFlag,
                                    numDomainID
                            FROM    CompanyInfo
                            WHERE   numCompanyID = @numCompanyID                            
                    SET @numNewCompanyID = SCOPE_IDENTITY()                            
                                     
                    INSERT  INTO DivisionMaster
                            (
                              numCompanyID,
                              vcDivisionName,
                              numGrpId,
                              bitPublicFlag,
                              tintCRMType,
                              numStatusID,
                              tintBillingTerms,
                              numBillingDays,
                              tintInterestType,
                              fltInterest,
                              vcComPhone,
                              vcComFax,
                              numDomainID
                            )
                            SELECT  @numNewCompanyID,
                                    vcDivisionName,
                                    numGrpId,
                                    bitPublicFlag,
                                    2,
                                    numStatusID,
                                    tintBillingTerms,
                                    numBillingDays,
                                    tintInterestType,
                                    fltInterest,
                                    vcComPhone,
                                    vcComFax,
                                    numDomainID
                            FROM    DivisionMaster
                            WHERE   numDivisionID = @numDivisionID                     
                    SET @numNewDivisionID = SCOPE_IDENTITY()     
                                            
                            INSERT INTO dbo.AddressDetails (
								vcAddressName,
								vcStreet,
								vcCity,
								vcPostalCode,
								numState,
								numCountry,
								bitIsPrimary,
								tintAddressOf,
								tintAddressType,
								numRecordID,
								numDomainID
							) SELECT 
									 vcAddressName,
									 vcStreet,
									 vcCity,
									 vcPostalCode,
									 numState,
									 numCountry,
									 bitIsPrimary,
									 tintAddressOf,
									 tintAddressType,
									 @numNewDivisionID,
									 numDomainID FROM dbo.AddressDetails WHERE numRecordID= @numDivisionID AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = SCOPE_IDENTITY()                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              SCOPE_IDENTITY(),
                              @numNewContactID,
                              1
                            )                  
                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'System Administrator'
                       ) = 0 
                        BEGIN                       
                         
                            DECLARE @numGroupId1 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'System Administrator',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId1 = SCOPE_IDENTITY()                        
                            SET @TargetGroupId = @numGroupId1                     
                       
INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId1,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster
--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId1,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,1,0,1,1,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,36,0,1,2,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,7,0,1,3,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,80,0,1,4,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,3,0,1,5,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,45,0,1,6,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,4,0,1,7,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,44,0,1,8,0)
INSERT  INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,6,0,1,9,0)

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


--Added by Anoop for Dashboard +++++++++++++++++++

                            IF ( SELECT COUNT(*)
                                 FROM   customreport
                                 WHERE  numDomainId = @numTargetDomainID
                               ) = 0 
                                BEGIN                  
                                    EXEC CreateCustomReportsForNewDomain @numTargetDomainID,
                                        @numNewContactID              
                                END 

                            DECLARE @numGroupId2 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Executive',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 





                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Sales Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 






                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Warehouse Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                           
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           


                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 258
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Executive'
                                                            )

                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 259
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Sales Staff'
                                                            )


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 260
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Warehouse Staff'
                                                            )



------ Added by Anoop for Dashboard-----------       
                        END                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'Default Extranet'
                       ) = 0 
                        BEGIN                       
                                             
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Default Extranet',
                                      @numTargetDomainID,
                                      2,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY()                        
                      
                       
                            INSERT  INTO GroupAuthorization
                                    (
                                      numGroupID,
                                      numModuleID,
                                      numPageID,
                                      intExportAllowed,
                                      intPrintAllowed,
                                      intViewAllowed,
                                      intAddAllowed,
                                      intUpdateAllowed,
                                      intDeleteAllowed,
                                      numDomainID                      
                       
                                    )
                                    SELECT  @numGroupId2,
                                            x.numModuleID,
                                            x.numPageID,
                                            x.intExportAllowed,
                                            x.intPrintAllowed,
                                            x.intViewAllowed,
                                            x.intAddAllowed,
                                            x.intUpdateAllowed,
                                            x.intDeleteAllowed,
                                            @numTargetDomainID
                                    FROM    ( SELECT    *
                                              FROM      GroupAuthorization
                                              WHERE     numGroupid = 2
                                            ) x           
        
                          
--MainTab From TabMaster for Customer:46
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            46,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Customer:46
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            46,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


                          
--MainTab From TabMaster  for Employer:93
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                           93,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Employer:93
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            93,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 
      
                   
                        END                      
----                    IF ( SELECT COUNT(*)
----                         FROM   AuthenticationGroupMaster
----                         WHERE  numDomainId = @numTargetDomainID
----                                AND vcGroupName = 'Default PRM'
----                       ) = 0 
----                        BEGIN                       
----                         
----                            DECLARE @numGroupId3 AS NUMERIC(9)                      
----                            INSERT  INTO AuthenticationGroupMaster
----                                    (
----                                      vcGroupName,
----                                      numDomainID,
----                                      tintGroupType,
----                                      bitConsFlag 
----                                    )
----                            VALUES  (
----                                      'Default PRM',
----                                      @numTargetDomainID,
----                                      3,
----                                      0
----                                    )                      
----                            SET @numGroupId3 = SCOPE_IDENTITY()                        
----                      
----                       
----                            INSERT  INTO GroupAuthorization
----                                    (
----                                      numGroupID,
----                                      numModuleID,
----                                      numPageID,
----                                      intExportAllowed,
----                                      intPrintAllowed,
----                                      intViewAllowed,
----                                      intAddAllowed,
----                                      intUpdateAllowed,
----                                      intDeleteAllowed,
----                                      numDomainID                   
----                   
----                                    )
----                                    SELECT  @numGroupId3,
----                                            x.numModuleID,
----                                            x.numPageID,
----                                            x.intExportAllowed,
----                                            x.intPrintAllowed,
----                                            x.intViewAllowed,
----                                            x.intAddAllowed,
----                                            x.intUpdateAllowed,
----                                            x.intDeleteAllowed,
----                                            @numTargetDomainID
----                                    FROM    ( SELECT    *
----                                              FROM      GroupAuthorization
----                                              WHERE     numGroupid = 46
----                                            ) x             
----        
------                            INSERT  INTO GroupTabDetails
------                                    (
------                                      numGroupId,
------                                      numTabId,
------                                      numRelationShip,
------                                      bitallowed,
------                                      numOrder
------                                    )
------                                    SELECT  @numGroupId3,
------                                            x.numTabId,
------                                            x.numRelationShip,
------                                            x.bitallowed,
------                                            x.numOrder
------                                    FROM    ( SELECT    *
------                                              FROM      GroupTabDetails
------                                              WHERE     numGroupid = 46
------                                            ) x         
----                 
----                        END                      
                    
       

      
--- Set default Chart of Accounts 
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = SCOPE_IDENTITY()                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag,
									  numShippingZone,
									  vcAbbreviations
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag,
											numShippingZone,
											vcAbbreviations
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,ISNULL(fltExchangeRate,1),1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1, fltExchangeRate=1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = SCOPE_IDENTITY()                        
                      
                      
                              
                END                                
            ELSE 
                BEGIN                        
                    SELECT  @numSubscriberID = numSubscriberID
                    FROM    Subscribers
                    WHERE   numDivisionID = @numDivisionID
                            AND numDomainID = @numDomainID                                
                    UPDATE  Subscribers
                    SET     bitDeleted = 0
                    WHERE   numSubscriberID = @numSubscriberID                                
                END                                 
                                   
                     
        END                                    
    ELSE 
        BEGIN                                    
                                  
            DECLARE @bitOldStatus AS BIT                                  
            SET @bitExists = 1                                 
            SELECT  @bitOldStatus = bitActive,
                    @numTargetDomainID = numTargetDomainID
            FROM    Subscribers
            WHERE   numSubscriberID = @numSubscriberID                                    
                                  
            IF ( @bitOldStatus = 1
                 AND @bitActive = 0
               ) 
                UPDATE  Subscribers
                SET     dtSuspendedDate = GETUTCDATE()
                WHERE   numSubscriberID = @numSubscriberID                                  
                                  
            UPDATE  Subscribers
            SET     intNoofUsersSubscribed = @intNoofUsersSubscribed,
                    intNoOfPartners = @intNoOfPartners,
                    dtSubStartDate = @dtSubStartDate,
                    dtSubEndDate = @dtSubEndDate,
                    bitTrial = @bitTrial,
                    numAdminContactID = @numAdminContactID,
                    bitActive = @bitActive,
                    vcSuspendedReason = @vcSuspendedReason,
                    numModifiedBy = @numUserContactID,
                    dtModifiedDate = GETUTCDATE(),
                    intNoofPartialSubscribed = @intNoofPartialSubscribed,
                    intNoofMinimalSubscribed = @intNoofMinimalSubscribed,
					dtEmailStartDate= @dtEmailStartDate,
					dtEmailEndDate= @dtEmailEndDate,
					intNoOfEmail=@intNoOfEmail,
					bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
					bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin
            WHERE   numSubscriberID = @numSubscriberID                 
                       
                         UPDATE  Domain
                    SET     tintLogin = @tintLogin,
							numAdminID = @numAdminContactID
                    WHERE   numdomainID = @numTargetDomainID   
                           
            DECLARE @hDoc1 INT                                                              
            EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
            INSERT  INTO UserMaster
                    (
                      vcUserName,
                      vcMailNickName,
                      vcUserDesc,
                      numGroupID,
                      numDomainID,
                      numCreatedBy,
                      bintCreatedDate,
                      numModifiedBy,
                      bintModifiedDate,
                      bitActivateFlag,
                      numUserDetailId,
                      vcEmailID,
                      vcPassword
                    )
                    SELECT  UserName,
                            UserName,
                            UserName,
                            0,
                            @numTargetDomainID,
                            @numUserContactID,
                            GETUTCDATE(),
                            @numUserContactID,
                            GETUTCDATE(),
                            Active,
                            numContactID,
                            Email,
                            vcPassword
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                                        WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                            ) X                                
                
                              
            UPDATE  UserMaster
            SET     vcUserName = X.UserName,
                    numModifiedBy = @numUserContactID,
                    bintModifiedDate = GETUTCDATE(),
                    vcEmailID = X.Email,
                    vcPassword = X.vcPassword,
                    bitActivateFlag = Active
            FROM    ( SELECT    numUserID AS UserID,
                                numContactID,
                                Email,
                                vcPassword,
                                UserName,
                                Active
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                                WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                    ) X
            WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
            EXEC sp_xml_removedocument @hDoc1                               
                                   
        END                                    
    SELECT  @numSubscriberID
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWorkFlowQueue' ) 
    DROP PROCEDURE USP_ManageWorkFlowQueue
GO
CREATE PROCEDURE USP_ManageWorkFlowQueue
    @numWFQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0,
    @numFormID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @tintWFTriggerOn TINYINT,
    @tintMode TINYINT,
    @vcColumnsUpdated VARCHAR(1000)='',
    @numWFID NUMERIC(18,0)=0,
    @bitSuccess BIT=0,
    @vcDescription VARCHAR(1000)=''
    
AS 
BEGIN
	IF @tintMode=1
	BEGIN
		IF @numWFQueueID>0
		   BEGIN
	  		 UPDATE [dbo].[WorkFlowQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numWFQueueID] = @numWFQueueID AND [numDomainID] = @numDomainID
		   END
		ELSE
		   BEGIN
	 		 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --Added By:Sachin Sadhu||Date:10thFeb2014
			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
			 --case of Field(s) Update
			 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=4)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --End of Script:Sachin
		  END
	 END
	  ELSE IF @tintMode = 2
        BEGIN
			INSERT INTO [dbo].[WorkFlowQueueExecution] ([numWFQueueID], [numWFID], [dtExecutionDate], [bitSuccess], [vcDescription])
			SELECT @numWFQueueID, @numWFID, GETUTCDATE(), @bitSuccess, @vcDescription


			IF ISNULL(@bitSuccess,0) = 0
			BEGIN
				UPDATE WorkFlowQueue SET tintProcessStatus=6 WHERE numWFQueueID=@numWFQueueID
			END
        END    
END



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numItemGroup VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
		,vcShippingLabel VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;
	END

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) 
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE (CASE WHEN @numViewID = 6 THEN 'OpportunityBizDocs.dtCreatedDate' ELSE 'OpportunityMaster.bintCreatedDate' END)
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemGroup
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,ISNULL(OpportunityBizDocs.vcBizDocID,'''')
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(ItemGroups.vcItemGroup,'''')
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
				THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		LEFT JOIN
			OpportunityBizDocs 
		ON
			TEMPOrder.numOppId = OpportunityBizDocs.numOppID
			AND TEMPOrder.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
			AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ItemGroups
		ON
			Item.numItemGroup = ItemGroups.numItemGroupID
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		WHERE
			1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(TEMPOrder.numOppBizDocID,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)',
			(CASE WHEN @numViewID=2 AND @tintPackingViewMode=3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),'
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.numItemGroup' THEN 'ISNULL(ItemGroups.vcItemGroup,'''')'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetShippingLabelForPrint')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetShippingLabelForPrint
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetShippingLabelForPrint]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		vcShippingLabelImage
	FROM
		ShippingReport
	INNER JOIN
		ShippingBox
	ON
		ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
	WHERE
		ShippingReport.numDomainId = @numDomainID
		AND ShippingReport.numOppID=@numOppID
		AND ShippingReport.numOppBizDocId=@numOppBizDocID
		AND LEN(ISNULL(vcShippingLabelImage,'')) > 0
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(Opp.numShippingService,0) numOrderShippingService,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.dtExpectedDate,'') AS dtExpectedDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name AS vcProcessName,
		(CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppID=Opp.numOppId AND numBizDocId IN (287,296,644)),0) > 0 THEN 1 ELSE 0 END) bitAuthorativeBizDocAdded
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID=SLP.Slp_Id
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,
			 STUFF((SELECT 
						CONCAT('<br/>',OpportunityBizDocs.vcBizDocID,': ',vcTrackingDetail)
					FROM 
						ShippingReport 
					INNER JOIN 
						OpportunityBizDocs 
					ON 
						ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
					WHERE 
						ShippingReport.numOppId=Opp.numOppID
						AND ISNULL(ShippingReport.vcTrackingDetail,'') <> ''
					FOR XML PATH(''), TYPE).value('(./text())[1]','varchar(max)'), 1, 5, '') AS vcTrackingDetail,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(Opp.numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
		,dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2) vcShipState
		,Opp.dtExpectedDate
		,ISNULL(Opp.numProjectID,0) numProjectID
		,ISNULL(PM.vcProjectName,'') AS numProjectIDName
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		 LEFT JOIN ProjectsMaster PM ON Opp.numProjectID = PM.numProId
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	UPDATE OpportunityItems SET numProjectID=@numProjectID WHERE numOppId=@numOppId

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_ChangeOrganization')
DROP PROCEDURE USP_OpportunityMaster_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId = @numOppID

	DECLARE @vcStreet varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
	DECLARE @numState numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
	DECLARE @tintAddressOf TINYINT

	SELECT 
		@vcCompanyName=vcCompanyName
		,@numCompanyId=div.numCompanyID 
	FROM 
		CompanyInfo Com                            
	JOIN 
		divisionMaster Div                            
	ON 
		div.numCompanyID=com.numCompanyID                            
	WHERE 
		div.numdivisionID=@numNewDivisionID

	BEGIN TRY
	BEGIN TRANSACTION

		UPDATE OpportunityMaster SET numDivisionId=@numNewDivisionID,numContactId=@numNewContactID,tintBillToType=2,numBillToAddressID=0,tintShipToType=2,numShipToAddressID=0 WHERE numDomainId=@numDomainID AND numOppId = @numOppID
		
		--Update Billing Address Details
		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@tintAddressOf=ISNULL(tintAddressOf,0)
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails AD
		WHERE 
			AD.numDomainID=@numDomainID 
			AND AD.numRecordID=@numNewDivisionID 
			AND AD.tintAddressOf = 2 
			AND AD.tintAddressType = 1 
			AND AD.bitIsPrimary=1       

  		EXEC dbo.USP_UpdateOppAddress
			@numOppID = @numOppID, --  numeric(9, 0)
			@byteMode = 0, --  tinyint
			@vcStreet = @vcStreet, --  varchar(100)
			@vcCity = @vcCity, --  varchar(50)
			@vcPostalCode = @vcPostalCode, --  varchar(15)
			@numState = @numState, --  numeric(9, 0)
			@numCountry = @numCountry, --  numeric(9, 0)
			@vcCompanyName = @vcCompanyName, --  varchar(100)
			@numCompanyId = 0, --  numeric(9, 0)
			@vcAddressName = @vcAddressName,
			@bitCalledFromProcedure = 1,
			@numContact = @numContact,
			@bitAltContact = @bitAltContact,
			@vcAltContact = @vcAltContact

		--Update Shipping Address Details
		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@tintAddressOf=ISNULL(tintAddressOf,0)
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails AD
		WHERE 
			AD.numDomainID=@numDomainID 
			AND AD.numRecordID=@numNewDivisionID 
			AND AD.tintAddressOf = 2 
			AND AD.tintAddressType = 2 
			AND AD.bitIsPrimary=1       

  		EXEC dbo.USP_UpdateOppAddress
			@numOppID = @numOppID, --  numeric(9, 0)
			@byteMode = 1, --  tinyint
			@vcStreet = @vcStreet, --  varchar(100)
			@vcCity = @vcCity, --  varchar(50)
			@vcPostalCode = @vcPostalCode, --  varchar(15)
			@numState = @numState, --  numeric(9, 0)
			@numCountry = @numCountry, --  numeric(9, 0)
			@vcCompanyName = @vcCompanyName, --  varchar(100)
			@numCompanyId =0, --  numeric(9, 0)
			@vcAddressName = @vcAddressName,
			@bitCalledFromProcedure = 1,
			@numContact = @numContact,
			@bitAltContact = @bitAltContact,
			@vcAltContact = @vcAltContact


		--Delete Tax of old organization and add for new organization
		DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId

		--Insert Tax for Opportunity Items
		INSERT INTO dbo.OpportunityItemsTaxItems 
		(
			numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID
		) 
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.ItemTax IT 
		ON 
			OI.numItemCode=IT.numItemCode 
		JOIN
			TaxItems TI 
		ON 
			TI.numTaxItemID = IT.numTaxItemID 
		WHERE 
			OI.numOppId=@numOppID 
			AND IT.bitApplicable=1
		UNION
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			0,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.Item I 
		ON 
			OI.numItemCode=I.numItemCode
		WHERE 
			OI.numOppId=@numOppID 
			AND I.bitTaxable=1 
		UNION
		SELECT
			@numOppId,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
		FROM
			dbo.OpportunityItems OI 
		INNER JOIN
			ItemTax IT
		ON
			IT.numItemCode = OI.numItemCode
		INNER JOIN
			TaxDetails TD
		ON
			TD.numTaxID = IT.numTaxID
			AND TD.numDomainId = @numDomainId
		WHERE
			OI.numOppId = @numOppID

		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numOppID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numOppID,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetFieldValue')
DROP PROCEDURE USP_OpportunityMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numOppID NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_ChangeOrganization')
DROP PROCEDURE USP_ProjectsMaster_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numProId NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId = @numProId


	BEGIN TRY
	BEGIN TRANSACTION

		UPDATE 
			ProjectsMaster 
		SET 
			numDivisionId=@numNewDivisionID
			,numCustPrjMgr=@numNewContactID
		WHERE 
			numDomainId=@numDomainID 
			AND numProId = @numProId
		

		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numProjectID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numProId,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)

		DELETE FROM AddressDetails WHERE numRecordID=@numProId AND tintAddressOf=4

		INSERT INTO dbo.AddressDetails 
		(
			vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,tintAddressOf
			,tintAddressType
			,numRecordID
			,numDomainID
		)
		SELECT 
			vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,4,tintAddressType
			,@numProId
			,numDomainID
		FROM 
			dbo.AddressDetails
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numNewDivisionID 
			AND tintAddressOf = 2 
			AND tintAddressType = 2
			AND bitIsPrimary=1      
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetFieldValue')
DROP PROCEDURE USP_ProjectsMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numProId NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numAddressID'
	BEGIN
		SELECT numAddressID FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'numIntPrjMgr'
	BEGIN
		SELECT numIntPrjMgr FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'vcCompanyName'
	BEGIN
		SELECT 
			CompanyInfo.vcCompanyName
		FROM 
			ProjectsMaster WITH(NOLOCK)
		INNER JOIN DivisionMaster WITH(NOLOCK) ON ProjectsMaster.numDivisionId=DivisionMaster.numDivisionID 
		INNER JOIN CompanyInfo WITH(NOLOCK) ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			ProjectsMaster.numDomainId=@numDomainID 
			AND ProjectsMaster.numProId=@numProId
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'(CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT,
	@bitDisplaySaveNew BIT,
	@bitDisplayAddToCart BIT,
	@bitDisplayCreateInvoice BIT,
	@bitDisplayShipVia BIT,
	@bitDisplayComments BIT,
	@bitDisplayReleaseDate BIT,
	@bitDisplayCustomerPart#Entry BIT,
	@bitDisplayItemGridOrderAZ BIT,
	@bitDisplayItemGridItemID BIT,
	@bitDisplayItemGridSKU BIT,
	@bitDisplayItemGridUnitListPrice BIT,
	@bitDisplayItemGridUnitSalePrice BIT,
	@bitDisplayItemGridDiscount BIT,
	@bitDisplayItemGridItemRelaseDate BIT,
	@bitDisplayItemGridLocation BIT,
	@bitDisplayItemGridShipTo BIT,
	@bitDisplayItemGridOnHandAllocation BIT,
	@bitDisplayItemGridDescription BIT,
	@bitDisplayItemGridNotes BIT,
	@bitDisplayItemGridAttributes BIT,
	@bitDisplayItemGridInclusionDetail BIT,
	@bitDisplayItemGridItemClassification BIT,
	@bitDisplayExpectedDate BIT,
	@bitDisplayItemGridItemPromotion BIT,
	@bitDisplayApplyPromotionCode BIT,
	@bitDisplayPayButton BIT,
	@bitDisplayPOSPay BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount,
			bitDisplaySaveNew,
			bitDisplayAddToCart,
			bitDisplayCreateInvoice,
			bitDisplayShipVia,
			bitDisplayComments,
			bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID,
			bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription,
			bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate,
			bitDisplayItemGridItemPromotion,
			bitDisplayApplyPromotionCode,
			bitDisplayPayButton,
			bitDisplayPOSPay

			)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount,
			@bitDisplaySaveNew,
			@bitDisplayAddToCart,
			@bitDisplayCreateInvoice,
			@bitDisplayShipVia,
			@bitDisplayComments,
			@bitDisplayReleaseDate,
			@bitDisplayCustomerPart#Entry,
			@bitDisplayItemGridOrderAZ,
			@bitDisplayItemGridItemID,
			@bitDisplayItemGridSKU,
			@bitDisplayItemGridUnitListPrice,
			@bitDisplayItemGridUnitSalePrice,
			@bitDisplayItemGridDiscount,
			@bitDisplayItemGridItemRelaseDate,
			@bitDisplayItemGridLocation,
			@bitDisplayItemGridShipTo,
			@bitDisplayItemGridOnHandAllocation,
			@bitDisplayItemGridDescription,
			@bitDisplayItemGridNotes,
			@bitDisplayItemGridAttributes,
			@bitDisplayItemGridInclusionDetail,
			@bitDisplayItemGridItemClassification
			,@bitDisplayExpectedDate
			,@bitDisplayItemGridItemPromotion
			,@bitDisplayApplyPromotionCode
			,@bitDisplayPayButton
			,@bitDisplayPOSPay
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount,
			bitDisplaySaveNew = @bitDisplaySaveNew,
			bitDisplayAddToCart = @bitDisplayAddToCart,
			bitDisplayCreateInvoice = @bitDisplayCreateInvoice,
			bitDisplayShipVia = @bitDisplayShipVia,
			bitDisplayComments = @bitDisplayComments,
			bitDisplayReleaseDate = @bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry = @bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ = @bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID = @bitDisplayItemGridItemID,
			bitDisplayItemGridSKU = @bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice = @bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice = @bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount = @bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate = @bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation = @bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo = @bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation = @bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription = @bitDisplayItemGridDescription,
			bitDisplayItemGridNotes = @bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes = @bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail = @bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification = @bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate=@bitDisplayExpectedDate
			,bitDisplayItemGridItemPromotion=@bitDisplayItemGridItemPromotion
			,bitDisplayApplyPromotionCode=@bitDisplayApplyPromotionCode
			,bitDisplayPayButton=@bitDisplayPayButton
			,bitDisplayPOSPay=@bitDisplayPOSPay
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
	@ClientTimeZoneOffset INT=0
AS                 
BEGIN                
	SELECT 
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		ISNULL((SELECT SUM(monTotAmtBefDiscount) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
			0 AS numReturnHeaderID1,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,@numDomainId,2),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
		UNION
		SELECT
			0 AS numOppBizDocsId,
			RH.numReturnHeaderID,
		    RH.vcRMA,
			OMTI.*,
			OM.tintOppType,
			RH.monTotalTax * -1 AS TaxAmount,
			[dbo].[FormatedDateFromDate](DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate),@numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,212,2),'') AS vcShipState,			
			RH.monBizDocAmount AS GrandTotal
		FROM
			ReturnHeader RH
		INNER JOIN OpportunityMaster OM ON RH.numOppId=OM.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=RH.numOppId
		LEFT JOIN DivisionMaster DM
		ON RH.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		WHERE
			RH.numDomainId=@numDomainID
			AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate) BETWEEN  @dtFromDate AND @dtToDate
			AND RH.numReturnStatus=303
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount <> 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                               
AS                  
BEGIN                     
	CREATE TABLE #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                   
		numUserID VARCHAR(15),                  
		vcEmailID varchar(100),
		vcEmailAlias VARCHAR(100),
		vcEmailAliasPassword VARCHAR(100),                  
		[Name] VARCHAR(100),                  
		vcGroupName varchar(100),                  
		vcDomainName varchar(50),          
		Active varchar(10),
		bitActivateFlag BIT,
		numDefaultWarehouse numeric(18,0)              
	)                  

	DECLARE @strSql AS VARCHAR(8000)                  
	SET @strSql='SELECT 
					numUserID
					,ISNULL(vcEmailID,''-'') as vcEmailID
					,ISNULL(vcEmailAlias,'''') as vcEmailAlias
					,ISNULL(vcEmailAliasPassword,'''') vcEmailAliasPassword
					,ISNULL(ADC.vcfirstname,'''') + '' '' + ISNULL(ADC.vclastname,'''') AS Name
					,ISNULL(vcGroupName,''-'') AS vcGroupName
					,vcDomainName
					,CASE WHEN bitActivateFlag=1 THEN ''Active'' ELSE ''Suspended'' END AS bitActivateFlag
					,ISNULL(bitActivateFlag,0) bitActivateFlag
					,numDefaultWarehouse                          
				FROM 
					UserMaster                    
				JOIN 
					Domain                      
				ON 
					UserMaster.numDomainID =  Domain.numDomainID                     
				LEFT JOIN 
					AdditionalContactsInformation ADC                    
				ON 
					ADC.numContactid=UserMaster.numUserDetailId                   
				LEFT JOIN 
					AuthenticationGroupMaster GM                   
				ON 
					Gm.numGroupID= UserMaster.numGroupID              
				WHERE 
					UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status         
                  
	IF @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

	IF @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

	IF @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   
	INSERT INTO #tempTable
	(                  
		numUserID,                  
		vcEmailID,
		vcEmailAlias,
		vcEmailAliasPassword,               
		[Name],                  
		vcGroupName,                  
		vcDomainName,
		Active,
		bitActivateFlag,
		numDefaultWarehouse
	)                  
	EXEC (@strSql)                   
                  
	DECLARE @firstRec AS INTEGER                   
	DECLARE @lastRec AS INTEGER                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateActiveUsers')
DROP PROCEDURE USP_UserMaster_UpdateActiveUsers
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateActiveUsers]                          
	@numDomainID NUMERIC(18,0)
	,@vcUsers VARCHAR(MAX)                         
AS                  
BEGIN 
	DECLARE @hDocItem INT
	
	IF ISNULL(@vcUsers,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcUsers

		DECLARE @TEMP TABLE
		(
			numUserID NUMERIC(18,0),
			bitActivateFlag BIT
		)

		INSERT INTO @TEMP
		(
			numUserID,
			bitActivateFlag
		)
		SELECT
			numUserID,
			bitActivateFlag
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Users',2)
		WITH
		(
			numUserID NUMERIC(18,0),
			bitActivateFlag BIT
		)

		EXEC sp_xml_removedocument @hDocItem 


		DECLARE @numNewActiveUsers AS INT
		
		SET @numNewActiveUsers = ISNULL((SELECT
											COUNT(*)
										FROM
											UserMaster 
										LEFT JOIN
											@TEMP TEMP
										ON
											UserMaster.numUserId = TEMP.numUserID
										WHERE
											UserMaster.numDomainID = @numDomainID
											AND (CASE WHEN ISNULL(TEMP.numUserID,0) > 0 THEN ISNULL(TEMP.bitActivateFlag,0) ELSE ISNULL(UserMaster.bitActivateFlag,0) END) = 1),0)
		
		IF EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND @numNewActiveUsers > ISNULL((ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10),0))
		BEGIN
			RAISERROR('SUBSCRIBED_USERS_EXCEED',16,1)
		END
		ELSE
		BEGIN
			UPDATE
				UM
			SET
				UM.bitActivateFlag = T.bitActivateFlag
			FROM
				UserMaster UM
			INNER JOIN
				@TEMP T
			ON
				UM.numUserId = T.numUserID
			WHERE
				UM.numDomainID=@numDomainID
		END
	END 
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0,
@bitAutoClosedTaskConfirmed As BIT=0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0 OR @numProjectId>0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask
		)
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		DECLARE @numTaskValidatorId AS NUMERIC(18,0)=0
		DECLARE @numTaskSlNo AS NUMERIC(18,0)=0
		DECLARE @numStageOrder AS NUMERIC(18,0)=0
		DECLARE @numStagePercentageId AS NUMERIC(18,0)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask,@numTaskSlNo=numOrder
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
		SELECT @numTaskValidatorId=numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=(SELECT TOP 1 slp_id FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)

		DECLARE @bitRunningDynamicMode AS BIT=0
		SELECT TOP 1 
			@bitRunningDynamicMode=bitRunningDynamicMode,
			@numStageOrder=numStageOrder ,
			@numStagePercentageId=numStagePercentageId
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numStageDetailsId
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND (@numOppId>0 OR @numProjectId>0))
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				DECLARE @numReferenceTaskId AS NUMERIC=0
				DECLARE @numReferenceStageDetailsId AS NUMERIC=0
				SELECT TOP 1 @numReferenceTaskId=numReferenceTaskId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
				SELECT TOP 1 @numReferenceStageDetailsId=numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numReferenceTaskId
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask,
				numReferenceTaskId
			)
			SELECT 
				@numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1, 
				numTaskId
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=@numReferenceTaskId AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numReferenceStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) AND ST.numOppId=0 AND ST.numProjectId=0 AND ST.numDomainId=@numDomainId 					
			END

	
					
		END
		IF((@bitAutoClosedTaskConfirmed=1 AND (@numOppId>0 OR @numProjectId>0) AND @numTaskValidatorId=2 AND @bitTaskClosed=1) OR ((@numOppId>0 OR @numProjectId>0) AND @numTaskValidatorId=1 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
		END
		IF(((@numOppId>0 OR @numProjectId>0) AND @numTaskValidatorId=3 AND @bitTaskClosed=1) OR (@bitAutoClosedTaskConfirmed=1 AND (@numOppId>0 OR @numProjectId>0) AND @numTaskValidatorId=4 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
			IF(ISNULL(@numStageOrder,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder<@numStageOrder AND numStagePercentageId=@numStagePercentageId AND (numOppId=@numOppId AND numProjectId=@numProjectId))
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId<@numStageDetailsId AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId AND numProjectId=@numProjectId)
			END
			UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId<@numStagePercentageId AND numOppId=@numOppId AND numProjectId=@numProjectId)
		END
	
		UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		IF(@numHours>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numHours=@numHours
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numMinutes>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numMinutes=@numMinutes
			WHERE
				numTaskId=@numTaskId
		END
		IF(LEN(@vcTaskName)>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				vcTaskName=@vcTaskName
			WHERE
				numTaskId=@numTaskId
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
	IF(@numOppId>0 OR @numProjectId>0)
	BEGIN
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId) AND bitSavedTask=1)
		SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId) AND bitSavedTask=1 AND bitTaskClosed=1)
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId)AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId) AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			IF(@numOppId=0)
			BEGIN
				SET @numOppId = NULL
			END
			IF(@numProjectId=0)
			BEGIN
				SET @numProjectId = NULL
			END
			INSERT INTO ProjectProgress
			(
				numOppId,
				numProId,
				numDomainId,
				intTotalProgress
			)VALUES(
				@numOppId,
				@numProjectId,
				@numDomainID,
				@intTotalProgress
			)
		END
	END
END 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicColumnsMasterConfig')
DROP VIEW View_DynamicColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicColumnsMasterConfig]
AS  
	SELECT 
	  DFM.numFieldId, 
	  DFM.numModuleID, 
	  ISNULL(DFV.vcNewFormFieldName, 
	  ISNULL(DFFM.vcFieldName, DFM.vcFieldName)) AS vcFieldName, 
      DFG.vcNewFieldName AS vcCultureFieldName, 
	  DFG.numCultureID, 
	  DFM.vcDbColumnName, 
	  ISNULL(DFFM.vcPropertyName, DFM.vcPropertyName) AS vcPropertyName, 
	  DFM.vcLookBackTableName, 
	  DFM.vcFieldDataType, 
	  DFM.vcFieldType, 
	  ISNULL(DFFM.vcAssociatedControlType, DFM.vcAssociatedControlType) AS vcAssociatedControlType, 
	  ISNULL(DFV.vcToolTip, DFM.vcToolTip) AS vcToolTip, 
	  DFG.vcToolTip AS vcCultureToolTip, 
	  ISNULL(DFM.vcListItemType, '') AS vcListItemType, 
	  DFM.numListID, 
	  ISNULL(DFFM.PopupFunctionName, DFM.PopupFunctionName) AS PopupFunctionName, 
	  ISNULL(DFFM.[order] + 1, DFM.[order] + 1) AS tintOrder, 
	  ISNULL(DFC.intRowNum, DFM.tintRow) AS tintRow, 
	  ISNULL(DFC.intColumnNum, DFM.tintColumn) AS tintColumn, 
      ISNULL(DFFM.bitInResults, ISNULL(DFM.bitInResults, 0)) AS bitInResults, ISNULL(DFFM.bitDeleted, ISNULL(DFM.bitDeleted, 0)) AS bitDeleted, 
      ISNULL(DFFM.bitAllowEdit, ISNULL(DFM.bitAllowEdit, 0)) AS bitAllowEdit, ISNULL(DFFM.bitDefault, ISNULL(DFM.bitDefault, 0)) AS bitDefault, 
      ISNULL(DFFM.bitSettingField, ISNULL(DFM.bitSettingField, 0)) AS bitSettingField, ISNULL(DFFM.bitAddField, ISNULL(DFM.bitAddField, 0)) AS bitAddField, 
      ISNULL(DFFM.bitDetailField, ISNULL(DFM.bitDetailField, 0)) AS bitDetailField, ISNULL(DFFM.bitAllowSorting, ISNULL(DFM.bitAllowSorting, 0)) AS bitAllowSorting, 
      ISNULL(DFFM.bitImport, ISNULL(DFM.bitImport, 0)) AS bitImport, ISNULL(DFFM.bitExport, ISNULL(DFM.bitExport, 0)) AS bitExport, ISNULL(DFFM.bitAllowFiltering, 
      ISNULL(DFM.bitAllowFiltering, 0)) AS bitAllowFiltering, 
	  ISNULL(DFFM.bitInlineEdit, 
	  ISNULL(DFM.bitInlineEdit, 0)) AS bitInlineEdit, 
	  ISNULL(DFFM.bitRequired, 
      ISNULL(DFM.bitRequired, 0)) AS bitRequired, 
	  DFM.intFieldMaxLength, 
	  ISNULL(DFV.bitIsRequired, 0) AS bitIsRequired, 
	  ISNULL(DFV.bitIsEmail, 0) AS bitIsEmail, 
      ISNULL(DFV.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
	  ISNULL(DFV.bitIsNumeric, 0) AS bitIsNumeric, 
	  ISNULL(DFV.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
	  DFV.intMaxLength, 
	  DFV.intMinLength, 
	  ISNULL(DFV.bitFieldMessage, 0) AS bitFieldMessage, 
	  ISNULL(DFV.vcFieldMessage, '') AS vcFieldMessage, 
	  CASE 
		WHEN DFM.vcAssociatedControlType = 'SelectBox' 
		THEN isnull ((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = DFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
	  END AS ListRelID, 
	  DFC.numFormId, 
	  DFC.intRowNum,
	  DFC.intColumnNum,
	  --DFC.numUserCntID, 
      DFC.numDomainId, 
	  DFC.tintPageType, 
	  DFC.numRelCntType, 
	  ISNULL(DFC.bitCustom, 0) AS bitCustom, 
	  --DFC.numViewId, 
	  DFC.numGroupID AS numAuthGroupID, 
	  DFC.bitGridConfiguration,
      --ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
	  ISNULL(DFFM.intSectionID, 0) AS intSectionID, 
	  DFM.vcOrigDbColumnName, 
	  ISNULL(DFFM.bitWorkFlowField, ISNULL(DFM.bitWorkFlowField, 0)) AS bitWorkFlowField, 
	  ISNULL(DFM.intColumnWidth, 0) AS intColumnWidth, 
      ISNULL(DFFM.bitAllowGridColor, 0) AS bitAllowGridColor,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName
FROM            
	dbo.BizFormWizardMasterConfiguration AS DFC 
INNER JOIN
    dbo.DycFormField_Mapping AS DFFM 
ON 
	DFC.numFormId = DFFM.numFormID AND 
	DFC.numFieldId = DFFM.numFieldID 
INNER JOIN
    dbo.DycFieldMaster AS DFM 
ON 
	DFFM.numModuleID = DFFM.numModuleID AND 
	DFC.numFieldId = DFM.numFieldId 
LEFT OUTER JOIN
    dbo.DynamicFormField_Validation AS DFV 
ON 
	DFV.numDomainId = DFC.numDomainId AND 
	DFFM.numFormID = DFV.numFormId AND 
    DFM.numFieldId = DFV.numFormFieldId 
LEFT OUTER JOIN
    dbo.DycField_Globalization AS DFG 
ON 
	DFG.numDomainID = DFC.numDomainId AND 
	DFM.numFieldId = DFG.numFieldID AND 
    DFM.numModuleID = DFG.numModuleID
LEFT JOIN 
	dbo.FormFieldGroupConfigurarion AS FGF
ON
	DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
WHERE        
	(ISNULL(DFC.bitCustom, 0) = 0)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicCustomColumnsMasterConfig')
DROP VIEW View_DynamicCustomColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicCustomColumnsMasterConfig]
AS  
	SELECT        
		DFC.intRowNum + 1 AS tintOrder, 
		'Cust' + CONVERT(varchar(10), CFM.Fld_id) AS vcDbColumnName, 
		CFM.Fld_label AS vcFieldName, 
		CFM.Fld_type AS vcAssociatedControlType, 
		ISNULL(CFM.numlistid, 0) AS numListID, 
		DFC.numFieldId, 1 AS bitAllowSorting, 
		1 AS bitAllowEdit, 1 AS bitInlineEdit, 
		ISNULL(DFC.intRowNum, 0) AS tintRow, 
		ISNULL(DFC.intColumnNum, 0) AS tintColumn, 
		ISNULL(V.bitIsRequired, 0) AS bitIsRequired, 
		ISNULL(V.bitIsEmail, 0) AS bitIsEmail, 
		ISNULL(V.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
		ISNULL(V.bitIsNumeric, 0) AS bitIsNumeric, 
		ISNULL(V.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
		V.intMaxLength, 
		V.intMinLength, 
		ISNULL(V.bitFieldMessage, 0) AS bitFieldMessage, 
		ISNULL(V.vcFieldMessage, '') AS vcFieldMessage, 
		CASE 
		WHEN fld_type = 'SelectBox' 
		THEN isnull((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = CFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
		END AS ListRelID, 
		DFC.numFormId, 
		DFC.intRowNum,
	    DFC.intColumnNum,
		--DFC.numUserCntID, 
		DFC.numDomainId, 
		DFC.tintPageType, 
		DFC.numRelCntType, 
		DFC.bitCustom, 
		--DFC.numViewId, 
		DFC.numGroupID AS numAuthGroupID, 
		DFC.bitGridConfiguration,
		CFM.Grp_id, 
		CFM.subgrp, 
		CFM.vcURL, 
		CFM.vcToolTip, 
		--ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
		ISNULL(L.vcFieldType, 'C') AS vcFieldType, 
		0 AS intColumnWidth,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName
	FROM            
		dbo.BizFormWizardMasterConfiguration AS DFC 
	INNER JOIN
		dbo.CFW_Fld_Master AS CFM 
	ON 
		DFC.numFieldId = CFM.Fld_id 
	LEFT OUTER JOIN
		dbo.CFw_Grp_Master AS CGM 
	ON 
		CFM.subgrp = CGM.Grp_id 
	LEFT OUTER JOIN
		dbo.CFW_Validation AS V 
	ON 
		V.numFieldID = CFM.Fld_id 
	INNER JOIN
		dbo.CFW_Loc_Master AS L 
	ON 
		CFM.Grp_id = L.Loc_id
	LEFT JOIN 
		dbo.FormFieldGroupConfigurarion AS FGF
	ON
		DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
	WHERE        
		(ISNULL(DFC.bitCustom, 0) = 1)
