/******************************************************************
Project: Release 11.3 Date: 11.MAR.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetKitWeightBasedOnItemSelection')
DROP FUNCTION GetKitWeightBasedOnItemSelection
GO
CREATE FUNCTION [dbo].[GetKitWeightBasedOnItemSelection]
(
	@numItemCode NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS FLOAT  
AS    
BEGIN   
	DECLARE @fltWeight AS FLOAT

	SET @fltWeight = ISNULL((SELECT
								SUM(ISNULL(ID.numQtyItemsReq,0) * ISNULL(I.fltWeight,0))
							FROM
								ItemDetails ID
							INNER JOIN
							(
								SELECT
									SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)) AS numItemCode
								FROM 
									dbo.SplitString(@vcSelectedKitChildItems,',')
							) T2
							ON
								ID.numChildItemID = T2.numItemCode
							INNER JOIN
								Item I 
							ON
								T2.numItemCode = I.numItemCode 
							WHERE
								ID.numItemKitID=@numItemCode),0)
	
	RETURN @fltWeight
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0),
	  @tintCommitAllocation TINYINT,
	  @bitFromBizDoc NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @bitAssembly BIT
	DECLARE @bitKitParent BIT
	DECLARE @bitWorkOrder BIT

	SELECT 
		@bitAssembly=ISNULL(bitAssembly,0)
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitWorkOrder=ISNULL(bitWorkOrder,0) 
	FROM 
		OpportunityItems 
	INNER JOIN 
		Item 
	ON 
		OpportunityItems.numItemCode=Item.numItemCode 
	WHERE 
		numOppId=@numOppID AND numoppitemtCode=@numOppItemID

	IF @bitKitParent = 1
	BEGIN
		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,bitDropship,bitKitParent,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKI.numOppItemID,
			OI.bitDropShip,
			ISNULL(I.bitKitParent,0),
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1,
			(CASE 
				WHEN OKI.numWareHouseItemId > 0 AND ISNULL(OI.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,0,1,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
							ELSE
								CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,1,0,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li><b><span style="color:red">',I.vcItemName, ':</b> ','</span>')
							ELSE
								CONCAT('<li><b>',I.vcItemName, ':</b> ') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(OKI.numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(OKI.numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
									END) 
							END
					END 
				ELSE 
					(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN 
							CONCAT('<li><b>',I.vcItemName, ':</b> ')
						ELSE
							CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
					END)
			END)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId=OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		LEFT JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		LEFT JOIN
			ItemDetails ID
		ON
			ID.numItemKitID = OI.numItemCode
			AND ID.numChildItemID=I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
		WHERE
			OM.numOppId = @numOppID
			AND OI.numoppitemtCode=@numOppItemID
		ORDER BY
			ISNULL(ID.sintOrder,1)

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2,
			(CASE 
				WHEN OKCI.numWareHouseItemId > 0 AND ISNULL(c.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,0,1,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,1,0,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityKitChildItems OKCI
		LEFT JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppID
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID


		INSERT INTO @TEMPTABLEFINAL
		(
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		)
		SELECT
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		FROM 
			@TEMPTABLE 
		WHERE 
		tintLevel=1
		
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

		WHILE @i <= @iCount
		BEGIN
			IF (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 0 OR (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 1 AND (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0))
			BEGIN
				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))
			END

			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
			BEGIN
				--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

				SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)

				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</li>')
			END

			SET @i = @i + 1
		END

		SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	END
	ELSE IF @bitAssembly=1 AND @bitWorkOrder=1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPFINAL TABLE
		(
			ID INT IDENTITY(1,1)
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		SET @i  = 1

		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END



		INSERT INTO @TEMPFINAL
		(
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		)
		SELECT
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		FROM 
			@TEMP
		WHERE 
			ItemLevel=2
		
		SET @i = 1

		SELECT @iCount = COUNT(*) FROM @TEMPFINAL

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT
																																		(CASE 
																																			WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
																																			THEN 
																																				CONCAT(I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
																																			ELSE
																																				CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
																																		END)
																																	FROM
																																		@TEMPFINAL T1
																																	INNER JOIN
																																		Item I
																																	ON
																																		T1.numItemCode = I.numItemCode
																																	LEFT JOIN
																																		WareHouseItems WI
																																	ON
																																		T1.numWarehouseItemID = WI.numWareHouseItemID
																																	LEFT JOIN
																																		UOM
																																	ON
																																		UOM.numUOMId = (CASE WHEN ISNULL(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
																																	WHERE 
																																		ID = @i))

			SET @i = @i + 1
		END
	END
	

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AccountsReports')
DROP PROCEDURE USP_AccountsReports
GO
CREATE PROCEDURE [dbo].[USP_AccountsReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintReportType TINYINT,
    @numAccountClass AS NUMERIC(9)=0
as                 

IF @tintReportType=1 --Report Name: Check Register
BEGIN
	SELECT CH.numCheckHeaderID,CH.numCheckNo,CH.dtCheckDate,CH.monAmount,CH.numDivisionID,C.vcCompanyName,COA.vcAccountName
	FROM dbo.CheckHeader CH 
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	JOIN dbo.Chart_Of_Accounts COA ON CH.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE CH.numDomainID=@numDomainID --AND ISNULL(bitIsPrint,0)=0 
	AND CH.dtCheckDate BETWEEN  @dtFromDate AND @dtToDate
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	order by dtCheckDate,numCheckNo
END

ELSE IF @tintReportType=2 --Report Name: Sales Journal Detail (By GL Account)
BEGIN
	SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt 
	FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId 
	AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL 
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.ReturnHeader RH ON RH.numDomainID=@numDomainID AND RH.numReturnHeaderID=GJH.numReturnID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID AND RH.tintReceiveType=2 AND RH.tintReturnType IN(1,3) AND  RH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=3 --Report Name: Purchase Journal
BEGIN
SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt
FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=2 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.BillHeader BH ON BH.numBillID=GJH.numBillID AND BH.numDomainId=@numDomainID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=4 --Report Name: Invoice Register
BEGIN

	SELECT OM.vcPOppName,OBD.vcBizDocID,OBD.dtFromDate AS FromDate,C.vcCompanyName,OM.monDealAMount,OBD.monDealAMount AS monBizDocAmount,OBD.monAmountPaid
	FROM OpportunityMaster OM
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.bitAuthoritativeBizDocs=1 
	JOIN dbo.DivisionMaster D ON OM.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)

	UNION ALL

	SELECT RH.vcRMA,RH.vcBizDocName,RH.dtCreatedDate AS FromDate,C.vcCompanyName,DM.monDepositAmount * -1 AS monDealAMount,0 AS monBizDocAmount,0 AS monAmountPaid 
	FROM dbo.ReturnHeader RH
	JOIN dbo.DepositMaster DM ON RH.numReturnHeaderID=DM.numReturnHeaderID AND DM.numDomainID=@numDomainID
	JOIN dbo.DivisionMaster D ON RH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE RH.tintReceiveType=2 AND  RH.numDomainId=@numDomainID
	AND RH.dtCreatedDate BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY FromDate

END

ELSE IF @tintReportType=5 --Report Name: Receipt Journal (Summary format)
BEGIN
	
	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE ISNULL(GJH.numDepositId,0)>0 AND GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName

END

ELSE IF @tintReportType=6 --Report Name: Total Receipts (Detailed)
BEGIN

	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,GJD.varDescription AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=6
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=7
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	LEFT JOIN dbo.DivisionMaster D ON GJD.numCustomerId=D.numDivisionID
	LEFT JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(DD.monAmountPaid AS VARCHAR(20)) +')',NULL,NULL,
	DM.numDepositId AS numReference,8 AS tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId = DD.numDepositID 
	JOIN dbo.OpportunityBizDocs OBD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId AND DD.numOppID=OM.numOppId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY datEntry_Date,numReference,tintReferenceType	
	
END

ELSE IF @tintReportType=7 --Report Name: Total Disbursement Journal (Summary)
BEGIN

	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE (ISNULL(GJH.numBillPaymentID,0)>0 OR ISNULL(GJH.numCheckHeaderID,0)>0) AND GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName
	
END

ELSE IF @tintReportType=8 --Report Name: Cash disbursement Journal (Detailed)
BEGIN

	SELECT ISNULL(GJH.numCheckHeaderID,0) AS numCheckHeaderID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
	GJH.datEntry_Date,COA.vcAccountName,numDebitAmt,numCreditAmt,GJD.tintReferenceType INTO #tempGJ 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainID=@numDomainID AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (ISNULL(GJH.numCheckHeaderID,0)>0 OR ISNULL(GJH.numBillPaymentID,0)>0)
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	SELECT GJH.datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=1
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		
	UNION ALL
	SELECT GJH.datEntry_Date AS datEntry_Date,NULL,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=2
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=8
	LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.tintReferenceType=8
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL AS numCheckNo,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=9
	JOIN dbo.DivisionMaster D ON BPH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE BPH.numDomainID=@numDomainID
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(BPD.monAmount AS VARCHAR(20)) +')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,COA.vcAccountName + '(' + CAST(GJD.numDebitAmt AS VARCHAR(20)) + ' : Bill-' + CONVERT(VARCHAR(10),BH.numBillID)+')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
	JOIN dbo.General_Journal_Header GJH ON BH.numBillID=GJH.numBillID JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE BPH.numDomainID=@numDomainID AND GJD.numDebitAmt>0
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	ORDER BY datEntry_Date,numReference,tintReferenceType
	
	DROP TABLE #tempGJ
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(20)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(50)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
DECLARE @vcOrigDbColumnName as varchar(50)
DECLARE @bitAllowSorting AS CHAR(1)
DECLARE @bitAllowEdit AS CHAR(1)
DECLARE @bitCustomField AS BIT;
DECLARE @ListRelID AS NUMERIC(9)
DECLARE @intColumnWidth INT
DECLARE @bitAllowFiltering AS BIT;
DECLARE @vcfieldatatype CHAR(1)

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN
select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   PRINT @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
 select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType='LI'                               
    begin                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    end                              
    else if @vcListItemType='S'                               
    begin           
       IF @ColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
    end                           
   end       
           
                      
   if (@SortColumnName<>'')                        
  begin                          
 select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType1='LI'                   
    begin     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    end                              
    else if @vcListItemType1='S'           
    begin                
        IF @SortColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType1='T'                  
    begin                              
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    end                           
  end                          
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='T'                               
		begin                              
		  set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
    else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='DateField' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by C.vcCompanyName, T.ID' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ChartOfAccounts_GetARAndChildAccounts' ) 
    DROP PROCEDURE USP_ChartOfAccounts_GetARAndChildAccounts
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ChartOfAccounts_GetARAndChildAccounts]
(
    @numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numAccountID NUMERIC(18,0)
		,vcAccountName VARCHAR(300)
		,numDefaultARAccountID NUMERIC(18,0)
	)

	DECLARE @numAccountID NUMERIC(18,0)
	DECLARE @vcAccountCode VARCHAR(300)

	SELECT 
		@numAccountID=Chart_Of_Accounts.numAccountID
		,@vcAccountCode=Chart_Of_Accounts.vcAccountCode
	FROM 
		AccountingCharges 
	INNER JOIN 
		Chart_Of_Accounts 
	ON 
		AccountingCharges.numAccountID = Chart_Of_Accounts.numAccountId 
	WHERE 
		AccountingCharges.numDomainID=@numDomainID 
		AND Chart_Of_Accounts.numDomainId=@numDomainID 
		AND numChargeTypeId=4

	IF ISNULL(@numAccountID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numAccountID
			,vcAccountName
			,numDefaultARAccountID
		)
		SELECT
			numAccountId
			,vcAccountName
			,@numAccountID
		FROM
			Chart_Of_Accounts
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(bitActive,0) = 1
			AND vcAccountCode LIKE ISNULL(@vcAccountCode,'-') + '%'
	END

	SELECT * FROM @TEMP
END
GO



/****** Object:  StoredProcedure [dbo].[USP_CheckAndDepositAmountForBizdoc]    Script Date: 02/19/2010 17:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndDepositAmountForBizdoc')
DROP PROCEDURE USP_CheckAndDepositAmountForBizdoc
GO
CREATE PROCEDURE [dbo].[USP_CheckAndDepositAmountForBizdoc]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numAmount				DECIMAL(20,5),
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
		
	SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
				WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
				WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
				ELSE 0
		   END AS [IsPaidInFull],
		   [DM].[numDivisionID],
		   [DM].[numDomainId],
		   [DD].[numOppBizDocsID],
		   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
		   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
		   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
		   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	FROM [dbo].[DepositMaster] AS DM 
	JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	WHERE [DM].[numDomainId] = @numDomainID
	AND [DM].[numDivisionID] = @numDivisionID
	AND DD.[numOppBizDocsID] = @numOppBizDocId
	GROUP BY [OBD].[monDealAmount],
			 [DM].[numDivisionID],
			 [DM].[numDomainId],
			 [DD].[numOppBizDocsID]

END

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as DECIMAL(20,5)= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as DECIMAL(20,5)= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0,
 @vcVendorInvoiceName  VARCHAR(100) = NULL,
 @numARAccountID NUMERIC(18,0) = 0
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;

	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF ISNULL(@numFromOppBizDocsId,0) > 0
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BizDocQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OBD.numOppId
						AND OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND OpportunityBizDocs.numBizDocId = @numBizDocId
				) AS OtherSameBizDocType
				WHERE
					OBD.numOppBizDocsId = @numFromOppBizDocsId
			) X
			WHERE
				ISNULL(X.FromBizDocQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BizDocQty,0))) > 0
		BEGIN
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
			RAISERROR('BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE',16,1)
		END
	END

	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END

	IF @numOppBizDocsId = 0 AND @numBizDocId = 296 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		IF ISNULL(@numFromOppBizDocsId,0) = 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			RAISERROR('FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING',16,1)
		END
		ELSE IF ISNULL(@numFromOppBizDocsId,0) > 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			SET @numSourceBizDocId = @numFromOppBizDocsId
		END
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId > 0)
			)
		BEGIN
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			--To insert Tracking Id of previous record into the new record
			IF @vcTrackingNo IS NULL OR @vcTrackingNo = ''
			BEGIN
				SELECT TOP 1 @vcTrackingNo = vcTrackingNo FROM OpportunityBizDocs WHERE numOppId = @numOppId ORDER BY numOppBizDocsId DESC 
			END
			INSERT INTO OpportunityBizDocs
			(
				numOppId
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,[dtShippedDate]
				,[bitAuthoritativeBizDocs]
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,[numMasterBizdocSequenceID]
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
				,numARAccountID
			)
			VALUES     
			(
				@numOppId
				,@numBizDocId
				,@numUserCntID
				,@dtCreatedDate
				,@numUserCntID
				,GETUTCDATE()
				,@vcComments
				,@bitPartialFulfillment
				,@numShipVia
				,@vcTrackingURL
				,@dtFromDate
				,@numBizDocStatus
				,@dtShipped
				,ISNULL(@bitAuthBizdoc,0)
				,@tintDeferred
				,@bitRentalBizDoc
				,@numBizDocTempID
				,@vcTrackingNo
				,@vcRefOrderNo
				,@numSequenceId
				,0
				,@fltExchangeRateBizDoc
				,0
				,@numSequenceId
				,@bitShippingGenerated
				,@numSourceBizDocId
				,@vcVendorInvoiceName
				,@numARAccountID
			)
			
        
			SET @numOppBizDocsId = SCOPE_IDENTITY()
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
			if @tintDeferred=1
			BEGIN
				exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
			END

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

			IF @numFromOppBizDocsId>0
			BEGIN
				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus
				)
				SELECT 
					@numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
				FROM 
					OpportunityBizDocs OBD 
				JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
				JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
				JOIN Item I ON OBDI.numItemCode=I.numItemCode
				WHERE 
					OBD.numOppId=@numOppId 
					AND OBD.numOppBizDocsId=@numFromOppBizDocsId
			END
			ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				IF DATALENGTH(@strBizDocItems)>2
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

						IF @bitRentalBizDoc=1
						BEGIN
							update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
							from OpportunityItems OI
							Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
								WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
							on OBZ.OppItemID=OI.numoppitemtCode
							where OBZ.OppItemID=OI.numoppitemtCode
						END
	
						insert into                       
					   OpportunityBizDocItems                                                                          
					   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
					   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
					   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					   WITH                       
					   (OppItemID numeric(9),                               
						Quantity FLOAT,
						Notes varchar(500),
						monPrice DECIMAL(20,5),
						dtRentalStartDate datetime,dtRentalReturnDate datetime
						)) OBZ
					   on OBZ.OppItemID=OI.numoppitemtCode
					   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
					EXEC sp_xml_removedocument @hDocItem 
				END
			END
			ELSE
			BEGIN
				IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
				BEGIN
					DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

					SELECT TOP 1
						@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
					FROM 
						OpportunityBizDocs 
					WHERE 
						numOppId=@numOppId 
						AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
						AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
					ORDER BY
						numOppBizDocsId

					INSERT INTO OpportunityBizDocItems
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					INNER JOIN
						(
							SELECT 
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM 
								OpportunityBizDocs 
							JOIN 
								OpportunityBizDocItems 
							ON 
								OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
							WHERE 
								numOppId=@numOppId 
								AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
						) TempBizDoc
					ON
						TempBizDoc.numOppItemID = OI.numoppitemtCode
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


					UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
				END
				ELSE
				BEGIN
		   			INSERT INTO OpportunityBizDocItems                                                                          
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc 
							WHEN 1 
							THEN 1 
							ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
						END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
						(
							SELECT 
								SUM(OBDI.numUnitHour) AS numUnitHour
							FROM 
								OpportunityBizDocs OBD 
							JOIN 
								dbo.OpportunityBizDocItems OBDI 
							ON 
								OBDI.numOppBizDocId  = OBD.numOppBizDocsId
								AND OBDI.numOppItemID = OI.numoppitemtCode
							WHERE 
								numOppId=@numOppId 
								AND 1 = (CASE 
											WHEN @bitAuthBizdoc = 1 
											THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
											WHEN @numBizDocId = 304 --Deferred Income
											THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
											ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
										END)
						) TempBizDoc
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
				END
			END

			SET @numOppBizDocsID = @numOppBizDocsId
			--select @numOppBizDocsId
		END
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated,
			vcVendorInvoice=@vcVendorInvoiceName,
			numARAccountID=@numARAccountID
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5),
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5)
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		IF @numBizDocId = 297 OR @numBizDocId = 299
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate
				(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT 
					@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

				SET @numSequenceId = 1
			END
			ELSE
			BEGIN
				SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
				UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			END
		END

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN @numSequenceId ELSE numSequenceId END,
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
		-- Allocate Inventory
		EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_CallCompleted' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_CallCompleted
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_CallCompleted]
(
    @numID NUMERIC(18,0)
	,@bitSuccess BIT
	,@vcMessage VARCHAR(MAX)
	,@vcExceptionMessage VARCHAR(MAX)
	,@vcStackStrace VARCHAR(MAX)
)
AS 
BEGIN
	UPDATE
		ECommerceCreditCardTransactionLog
	SET
		bitNSoftwareCallCompleted = 1
		,bitSuccess = @bitSuccess
		,vcMessage=@vcMessage
		,vcExceptionMessage=@vcExceptionMessage
		,vcStackStrace=@vcStackStrace
	WHERE
		ID=@numID

	SELECT SCOPE_IDENTITY()

END
GO
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_CallStarted' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_CallStarted
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_CallStarted]
(
    @numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@monAmount DECIMAL(20,5)
)
AS 
BEGIN
	INSERT INTO ECommerceCreditCardTransactionLog
	(
		numDomainID
		,numOppID
		,monAmount
		,bitNsoftwareCallStarted
		,bitNSoftwareCallCompleted
	)
	VALUES
	(
		@numDomainID
		,@numOppID
		,@monAmount
		,1
		,0
	)

	SELECT SCOPE_IDENTITY()

END
GO
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID]
(
    @numID NUMERIC(18,0)
	,@numTransHistoryID NUMERIC(18,0)
)
AS 
BEGIN
	UPDATE
		ECommerceCreditCardTransactionLog
	SET
		numTransHistoryID=@numTransHistoryID
	WHERE
		ID=@numID

	SELECT SCOPE_IDENTITY()

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS DECIMAL(20,5)
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                FORMAT(monPrice,'0.################') monPrice,
				CONVERT(DECIMAL(18, 4),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') vcBaseUOM,
				ISNULL(dbo.fn_GetUOMName(numUOMId),'') vcUOM,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID=RI.numOppBizDocItemID),isnull(i.monAverageCost,'0')) END) as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount]
				,ISNULL(I.bitKitParent,0) AS bitKitParent
				,ISNULL(I.bitAssembly,0) AS bitAssembly,
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
							ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
				,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount
				,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef
				,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
				,ISNULL(numOppBizDocID,0) numOppBizDocID
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsUsingDomainID]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsusingdomainid')
DROP PROCEDURE usp_getmasterlistitemsusingdomainid
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingDomainID]            
@numDomainID as numeric(9)=0,            
@numListID as numeric(9)=0          
AS
BEGIN            
	DECLARE @bit3PL BIT, @bitEDI BIT
	SELECT @bit3PL=ISNULL(bit3PL,0),@bitEDI=ISNULL(bitEDI,0) FROM Domain WHERE numDomainID=@numDomainID
      
	SELECT 
		Ld.numListItemID
		,ISNULL(LDN.vcName,vcData) AS vcData
		,Ld.constFlag
		,bitDelete
		,ISNULL(intSortOrder,0) intSortOrder
		,ISNULL(numListType,0) as numListType
		,(SELECT vcData FROM ListDetails WHERE numListId=27 and numListItemId=ld.numListType) AS vcListType
		,(CASE WHEN ld.numListType=1 THEN 'Sales' WHEN ld.numListType=2 THEN 'Purchase' ELSE 'All' END) AS vcOrderType
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END) AS vcOppOrOrder
		,numListType
		,tintOppOrOrder,ISNULL(SOR.bitEnforceMinOrderAmount,'False') AS bitEnforceMinOrderAmount, ISNULL(SOR.fltMinOrderAmount,0) AS fltMinOrderAmount
		,(SELECT ISNULL(LD.vcData,'') WHERE LD.numListID=27 ) AS vcOriginalBizDocName
		,ISNULL(numListItemGroupId,0) AS numListItemGroupId
		,(SELECT TOP 1 vcData FROM ListDetails WHERE numListItemId=ld.numListItemGroupId) AS vcListItemGroupName
		,ISNULL(CASE WHEN LD.numListID=30 THEN (SELECT TOP 1 vcColorScheme FROM DycFieldColorScheme WHERE numDomainId=@numDomainID AND numFieldID=18 AND numFormID=34 AND vcFieldValue=LD.numListItemID) ELSE '' END,'') AS vcColorScheme
	FROM 
		ListDetails LD          
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID = LO.numListItemID 
		AND lo.numDomainId = @numDomainID
	LEFT JOIN 
		ListDetailsName LDN 
	ON 
		LDN.numDOmainID=@numDomainID 
		AND LDN.numListID=@numListID 
		AND LDN.numListItemID=LD.numListItemID
	LEFT JOIN
		SalesOrderRule SOR ON LD.numListItemID = SOR.numListItemID
	WHERE 
		(Ld.numDomainID=@numDomainID OR Ld.constFlag=1) 
		AND Ld.numListID=@numListID           
		AND 1 = (CASE 
					WHEN LD.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END)
					WHEN LD.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	ORDER BY 
		intSortOrder
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReceivePayment' ) 
    DROP PROCEDURE USP_GetReceivePayment
GO
CREATE PROCEDURE [dbo].USP_GetReceivePayment
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDepositID NUMERIC = 0,
    @numDivisionID NUMERIC(9) = 0,
    @numCurrencyID numeric(9),
    @numAccountClass NUMERIC(18,0)=0,
	@numCurrentPage INT=1
AS 
BEGIN
	DECLARE @numPageSize INT = 40

		
	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)

	SELECT @numDiscountItemID = ISNULL(numDiscountServiceItemID,0),@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainID = @numDomainID
	PRINT @numDiscountItemID
			
	DECLARE @BaseCurrencySymbol 
	nVARCHAR(3);SET @BaseCurrencySymbol='$'
	SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId)
    
	SELECT [T].[numDivisionID],T.bitChild,dbo.[fn_GetComapnyName](T.[numDivisionID]) AS vcCompanyName,AD2.[vcCity]
	,isnull(dbo.fn_GetState(AD2.numState),'') AS vcState
	INTO #tempOrg
	FROM (SELECT @numDivisionID AS numDivisionID,cast(0 AS BIT) AS bitChild
	UNION
	SELECT [CA].[numDivisionID],CAST(1 AS BIT) AS bitChild
	FROM [dbo].[CompanyAssociations] AS CA 
	WHERE [CA].[numDomainID]=@numDomainID AND  ca.[numAssociateFromDivisionID]=@numDivisionID AND CA.[bitChildOrg]=1) AS T
	LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=@numDomainID 
	AND AD2.numRecordID= T.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocID
	)
	SELECT 
		numOppBizDocsID
	FROM
	(
		SELECT
			OpportunityBizdocs.numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM    
			OpportunityMaster
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		INNER JOIN 
			OpportunityBizdocs
		ON
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN
			DepositeDetails 
		ON 
			DepositeDetails.numOppBizDocsID = OpportunityBizdocs.numOppBizDocsID
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND DepositeDetails.numDepositID = @numDepositID
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		UNION
		SELECT
			numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM 
			OpportunityMaster
		INNER JOIN
			OpportunityBizdocs 
		ON 
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
			AND OpportunityBizdocs.monDealAmount - OpportunityBizdocs.monAmountPaid > 0
			AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OpportunityMaster.numOppId AND numOppBizDocsId=OpportunityBizdocs.numOppBizDocsID AND numDepositID=@numDepositID) = 0
			AND OpportunityBizdocs.numOppBizDocsId NOT IN ( SELECT numDocID FROM dbo.DocumentWorkflow D WHERE D.cDocType='B' AND ISNULL(tintApprove,0) = 0 /*discard invoice which are Pending to approve */ )
			AND (OpportunityMaster.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	) TEMP
	ORDER BY 
		bitChild,dtCreatedDate DESC


	DECLARE @TEMPFinal TABLE
	(
		numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMPFinal SELECT numOppBizDocID FROM @TEMP ORDER BY ID OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY


	SELECT  
		om.vcPOppName ,
		om.numoppid ,
		OBD.[numoppbizdocsid] ,
		OBD.[vcbizdocid] ,
		OBD.[numbizdocstatus] ,
		OBD.[vccomments] ,
		OBD.monAmountPaid AS monAmountPaid ,
		OBD.monDealAmount ,
		OBD.monCreditAmount monCreditAmount ,
		ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
		[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
		dtFromDate AS [dtOrigFromDate],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1
			--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)), dtFromDate), 1)
			THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)), dtFromDate), @numDomainID)
			WHEN 0
			THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
		END AS dtDueDate ,
		om.numDivisionID ,
                
		(OBD.monDealAmount - ISNULL(OBD.monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) + ISNULL(DD.monAmountPaid,0)  ) AS baldue ,
		T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
		DD.numDepositID ,
		DD.monAmountPaid monAmountPaidInDeposite ,
		ISNULL(DD.numDepositeDetailID,0) numDepositeDetailID,
		obd.dtCreatedDate,
		OM.numContactId,obd.vcRefOrderNo
		,OM.numCurrencyID
		,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
		,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
		,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscount],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscountPaidInDays],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numNetDueInDays],
		CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
					JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
					WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
					AND OBDI.numItemCode = @numDiscountItemID
					AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
		(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
		,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
			JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
	WHERE   
		OBD.[bitauthoritativebizdocs] = 1
		AND om.tintOppType = 1
		AND om.numDomainID = @numDomainID
		AND DD.numDepositID = @numDepositID
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	UNION
	SELECT  om.vcPOppName ,
			om.numoppid ,
			OBD.[numoppbizdocsid] ,
			OBD.[vcbizdocid] ,
			OBD.[numbizdocstatus] ,
			OBD.[vccomments] ,
			OBD.monAmountPaid AS monAmountPaid ,
			OBD.monDealAmount ,
			OBD.monCreditAmount monCreditAmount ,
			ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
			[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
			dtFromDate AS [dtOrigFromDate],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1
				--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				WHEN 0
				THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
			END AS dtDueDate ,
			om.numDivisionID ,
			(OBD.monDealAmount - ISNULL(monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) ) AS baldue ,
			T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
			0 numDepositID ,
			0 monAmountPaidInDeposite ,
			0 numDepositeDetailID,
			dtCreatedDate,
			OM.numContactId,obd.vcRefOrderNo
			,OM.numCurrencyID
			,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
			,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
			,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscount],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscountPaidInDays],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numNetDueInDays],
			CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						AND OBDI.numItemCode = @numDiscountItemID
						AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
			(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
			,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
	WHERE   
		OBD.[bitauthoritativebizdocs] = 1
		AND om.tintOppType = 1
		AND om.numDomainID = @numDomainID
		AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
		AND OBD.monDealAmount - OBD.monAmountPaid > 0
		AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OM.numOppId AND numOppBizDocsId=OBD.numOppBizDocsID AND numDepositID=@numDepositID) = 0
		AND OBD.numOppBizDocsId NOT IN ( SELECT numDocID FROM dbo.DocumentWorkflow D WHERE D.cDocType='B' AND ISNULL(tintApprove,0) = 0 /*discard invoice which are Pending to approve */ )
		AND (OM.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)      
	ORDER BY 
		T.bitChild,dtCreatedDate DESC
	
	SELECT COUNT(*) AS TotalRecords,@numPageSize AS PageSize FROM @TEMP

	DROP TABLE #tempOrg
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetDetails')
DROP PROCEDURE dbo.USP_Item_GetDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_GetDetails]                                        
@numItemCode as numeric(9),
@numWarehouseItemID as NUMERIC(9),
@ClientTimeZoneOffset AS INT,
@vcSelectedKitChildItems VARCHAR(MAX)                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, 
		vcItemName, 
		ISNULL(txtItemDesc,'') txtItemDesc,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, 
		charItemType, 
		(CASE WHEN charItemType='P' THEN ISNULL(W.monWListPrice,0) ELSE ISNULL(monListPrice,0) END) AS monListPrice,                   
		numItemClassification, 
		isnull(bitTaxable,0) as bitTaxable, 
		vcSKU AS vcSKU, 
		isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
		I.numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
		numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
		else isnull(intDisplayOrder,0) end as intDisplayOrder 
		FROM ItemImages  
		WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
		numOnHand as numOnHand,                      
		numOnOrder as numOnOrder,                      
		numReorder as numReorder,                      
		numAllocation as numAllocation,                      
		numBackOrder as numBackOrder,                   
		(CASE 
			WHEN ISNULL(bitKitParent,0)=1 
					AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 
					AND (CASE 
							WHEN ISNULL(bitKitParent,0) = 1 
							THEN 
								(CASE 
									WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
									THEN 1
									ELSE 0
								END)
							ELSE 0 
						END) = 0
			THEN 
				dbo.GetKitWeightBasedOnItemSelection(@numItemCode,@vcSelectedKitChildItems)
			ELSE 
				ISNULL(fltWeight,0) 
		END) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		isnull(I.vcManufacturer,'') as vcManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,W.numWareHouseID) else 0 end AS numWOQty,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(I.numContainer,0) AS [numContainer],
		ISNULL(I.numNoItemIntoContainer,0) AS [numNoItemIntoContainer],
		ISNULL(Vendor.monCost,0) AS monVendorCost, CPN.CustomerPartNo
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode AND
		W.numWareHouseItemID = @numWarehouseItemID           
	LEFT JOIN 
		ItemExtendedDetails IED   
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor
	ON
		I.numVendorID = Vendor.numVendorID
		AND Vendor.numItemCode = I.numItemCode    
	LEFT JOIN 
		CustomerPartNumber CPN   
	ON 
		I.numItemCode = CPN.numItemCode        
	WHERE 
		I.numItemCode=@numItemCode 

---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9),
@bitTaskRelation AS BIT=0
as
BEGIN
if @byteMode =1
BEGIN
	IF(@bitTaskRelation=1)
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND bitTaskRelation=1)
		BEGIN
			insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation)
			values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation)
			set @numFieldRelID=@@identity 
		END
		ELSE 
			set @numFieldRelID=0
	END
	ELSE
	BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID AND (numModuleID IS NULL OR numModuleID=@numModuleID))
	BEGIN
		insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,numModuleID,bitTaskRelation)
		values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@numModuleID,@bitTaskRelation)
		set @numFieldRelID=@@identity 
	END
	ELSE
	BEGIN
		SET @numFieldRelID=0
	END
END
END
else if @byteMode =2
begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

END
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END
/****** Object:  StoredProcedure [dbo].[USP_ManageItemList]    Script Date: 07/26/2008 16:19:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--Author:Anoop Jayaraj
--Modified By:Sachin Sadhu
--History: Added One Field numListType to save  bizdoc type wise BizDoc status     (added on 12thJun)    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemlist')
DROP PROCEDURE usp_manageitemlist
GO
CREATE PROCEDURE [dbo].[USP_ManageItemList]                
@numListItemID as numeric(9) OUTPUT,                  
@numDomainID as numeric(9)=0,                     
@vcData as varchar(100)='',             
@numListID as numeric(9)=0,                      
@numUserCntID as numeric(9),
@numListType numeric(9,0),
@tintOppOrOrder TINYINT,
@bitEnforceMinOrderAmount bit = 0,
@fltMinOrderAmount FLOAT = 0,
@numListItemGroupId NUMERIC=0,
@vcColorScheme VARCHAR(500)=''            
as                    
if @numListItemID =0                    
                    
begin         
	declare @sintOrder as smallint     
	Declare @numListItemIDPK as numeric(9)   

	update ListDetails set  sintOrder=0 where sintOrder is null   
	   
	select @sintOrder=(select max(sintOrder)+1 from ListDetails where numListID=@numListID)      
      
                 
	insert into ListDetails(numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder)                    
	values(@numListID,@vcData,@numUserCntID,getutcdate(),@numUserCntID,getutcdate(),0,@numDomainID,0,@sintOrder,@numListType,@tintOppOrOrder)     
  
	Set @numListItemIDPK = SCOPE_IDENTITY()
	SET @numListItemID = SCOPE_IDENTITY()

	Declare @CurrentDate as datetime      
    
	Set @CurrentDate=getutcdate()    
	
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		INSERT INTO SalesOrderRule
			(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
		VALUES
			(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
	END
	--If @numListID=64    
	--Begin    
	--Exec usp_InsertNewChartAccountDetails @numParntAcntId=10,@numAcntType=818,@vcCatgyName=@vcData,@vcCatgyDescription=@vcData,@numOriginalOpeningBal=null,    
	--     @numOpeningBal=null,@dtOpeningDate=@CurrentDate,@bitActive=1,@numAccountId=0,@bitFixed=0,@numDomainId=@numDomainID,@numListItemID=@numListItemIDPK    
	--End    
end                    
else                    
begin                    
	update ListDetails set vcData=@vcData,                   
	numModifiedBy=@numUserCntID,                  
	bintModifiedDate=getutcdate(),
	numListType=@numListType,
	tintOppOrOrder=@tintOppOrOrder,
	numListItemGroupId=@numListItemGroupId           
	where numListItemID=@numListItemID     
	--To update Category Description in Chart_of_accounts table  
	--update Chart_Of_Accounts Set vcCatgyName=@vcData,vcCatgyDescription=@vcData Where numListItemID=@numListItemID  
	IF(@numListId=30 AND LEN(@vcColorScheme)>0)
	BEGIN
		
	    IF((SELECT COUNT(*) FROM DycFieldColorScheme WHERE numFieldID=18 AND numFormID=34 AND numDomainID=@numDomainID AND vcFieldValue=@numListItemID)=0)
		BEGIN
			INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
			VALUES (18,34,@numDomainID,@numListItemID,'0',@vcColorScheme)
		END
		ELSE
		BEGIN
			UPDATE 
				DycFieldColorScheme
			SET
				vcColorScheme=@vcColorScheme
			WHERE
				numDomainID=@numDomainID AND
				vcFieldValue=@numListItemID AND
				numFieldID=18 AND
				numFormID=34
		END
	END     
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = @fltMinOrderAmount 
			WHERE numListItemID=@numListItemID    
		END
		ELSE
		BEGIN
			INSERT INTO SalesOrderRule
				(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
			VALUES
				(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
		END
	END    
	ELSE IF @bitEnforceMinOrderAmount = 0 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = 0 
			WHERE numListItemID=@numListItemID    
		END
	END             
end
GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount DECIMAL(20,5),
      @monTotalTax DECIMAL(20,5),
      @monTotalDiscount DECIMAL(20,5),
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0,
	  @numReferenceSalesOrder NUMERIC(18,0) = 0,
	  @numOppBizDocsId NUMERIC(18,0) = 0
    )
AS 
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment,numReferenceSalesOrder,numOppBizDocID
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END),@numReferenceSalesOrder,@numOppBizDocsId
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50),
			@vcAltContact VARCHAR(200)     
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9), @numContact NUMERIC(18,0) 
        DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId

            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,NULL,1,@numReturnHeaderID)	
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount DECIMAL(20,5), vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT, bitMarkupDiscount BIT, numOppBizDocItemID NUMERIC(18,0)
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,numOppBizDocItemID,
				bitDiscountType,fltDiscount
				,bitMarkupDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,numOppBizDocItemID,
				bitDiscountType,
				fltDiscount
				,bitMarkupDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0
		
			UPDATE
				ReturnItems
			SET
				monAverageCost = (CASE 
									WHEN @numOppId > 0 
									THEN 
										(CASE 
											WHEN @tintReturnType=2 
											THEN
												ISNULL((SELECT monPrice FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
											ELSE
												ISNULL((SELECT monAvgCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
										END)
									ELSE
										ISNULL((SELECT monAverageCost FROM Item WHERE numItemCode=ReturnItems.numItemCode),0)
								END),
				monVendorCost = (CASE 
									WHEN @numOppId > 0 
									THEN ISNULL((SELECT monVendorCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
									ELSE
										ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)
								END)
			WHERE
				numReturnHeaderID=@numReturnHeaderID
			 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1
			END 
		END
    END   
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numReturnHeaderID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH         
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingBox')
DROP PROCEDURE USP_ManageShippingBox
GO
CREATE PROCEDURE USP_ManageShippingBox
(
	@numShippingReportId NUMERIC(18,0),
    @strItems TEXT,
    @numUserCntId NUMERIC(18,0),
	@tintMode TINYINT,
	@numBoxID NUMERIC(18,0)	OUT,
	@PageMode TINYINT
)
AS 
BEGIN
	DECLARE @hDocItem INT
    
    IF @tintMode = 1 
    BEGIN
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                  
            UPDATE 
				dbo.ShippingBox
            SET
				dtDeliveryDate = X.dtDeliveryDate ,
                monShippingRate = X.monShippingRate,
				fltTotalWeight =X.[fltTotalWeight],
				[fltHeight] =X.[fltHeight],
				[fltWidth] = X.[fltWidth],
				[fltLength] = X.[fltLength],
				[numPackageTypeID] = X.numPackageTypeID,
				[numServiceTypeID] = X.numServiceTypeID,
				[fltDimensionalWeight] = X.fltDimensionalWeight,
				numShipCompany = X.numShipCompany
            FROM
			(
				SELECT 
					*
                FROM 
					OPENXML (@hDocItem, '/NewDataSet/Box', 2)
				WITH 
				(
					dtDeliveryDate DATETIME, 
					monShippingRate DECIMAL(20,5), 
					numBoxID NUMERIC(18,0), 
					[fltTotalWeight] FLOAT, 
					[fltHeight] FLOAT, 
					[fltWidth] FLOAT, 
					[fltLength] FLOAT, 
					numPackageTypeID BIGINT, 
					numServiceTypeID BIGINT, 
					fltDimensionalWeight FLOAT,
					numShipCompany INT
				)
            ) X
            WHERE 
				X.numBoxID = dbo.ShippingBox.numBoxID
				AND numShippingReportID = @numShippingReportId

            EXEC sp_xml_removedocument @hDocItem
                
        END
    END 
  
	IF @tintMode=0
	BEGIN
		DELETE FROM [ShippingReportItems] WHERE [numShippingReportId]=@numShippingReportId	
		DELETE  FROM [ShippingBox] WHERE [numShippingReportId] = @numShippingReportId

        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                                
            INSERT  INTO [ShippingBox]
            (
                [vcBoxName],
                [numShippingReportId],
                [fltTotalWeight],
                [fltHeight],
                [fltWidth],
                [fltLength],
				numCreatedBy,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany
	        )
            SELECT  
				X.vcBoxName,
                @numShippingReportId,
                X.[fltTotalWeight],
                X.[fltHeight],
                X.[fltWidth],
                X.[fltLength],
				@numUserCntId,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany
			FROM
			(
				SELECT
					*
                FROM
					OPENXML (@hDocItem, '/NewDataSet/Box', 2)
                WITH 
				(
					vcBoxName VARCHAR(20), 
					[fltTotalWeight] FLOAT, 
					[fltHeight] FLOAT, 
					[fltWidth] FLOAT, 
					[fltLength] FLOAT, 
					numPackageTypeID BIGINT, 
					numServiceTypeID BIGINT,
					fltDimensionalWeight FLOAT,
					numShipCompany INT
				)
            ) X
				
			SET @numBoxID = SCOPE_IDENTITY()
                                                  
            EXEC sp_xml_removedocument @hDocItem
                                               
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
  
            INSERT  INTO [ShippingReportItems]
            (
                [numShippingReportId],
                [numItemCode],
                [tintServiceType],
                [dtDeliveryDate],
                [monShippingRate],
                [fltTotalWeight],
                [intNoOfBox],
                [fltHeight],
                [fltWidth],
                [fltLength],
                [dtCreateDate],
                [numCreatedBy],
                [numBoxID],
                numOppBizDocItemID,
                intBoxQty
            )
            SELECT 
				@numShippingReportId,
                OBI.[numItemCode],
                ISNULL((SELECT TOP 1 numServiceTypeID FROM ShippingBox WHERE numBoxID=@numBoxID),0),--Unspecified
                NULL,
                0.0
                ,(CASE 
					WHEN ISNULL(bitKitParent,0)=1 
							AND LEN(ISNULL(OI.vcChildKitSelectedItems,'')) > 0 
							AND (CASE 
									WHEN ISNULL(bitKitParent,0) = 1 
									THEN 
										(CASE 
											WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
											THEN 1
											ELSE 0
										END)
									ELSE 0 
								END) = 0
					THEN 
						dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
					ELSE 
						ISNULL(fltWeight,0) 
				END),
                1,
                I.[fltHeight],
                I.[fltWidth],
                I.[fltLength],
                GETUTCDATE(),
                @numUserCntId,
                ( SELECT    [numBoxID]
                    FROM      [ShippingBox]
                    WHERE     vcBoxName = X.vcBoxName
                            AND numShippingReportId = @numShippingReportId
                ),
                X.[numOppBizDocItemID],
                X.intBoxQty
            FROM
			(
				SELECT
					* 
                FROM 
					OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                WITH 
				( 
					vcBoxName VARCHAR(20)
					,numOppBizDocItemID NUMERIC
					,intBoxQty BIGINT 
				)
            ) X     
            INNER JOIN [OpportunityBizDocItems] OBI ON X.numOppBizDocItemID = OBI.[numOppBizDocItemID]
			INNER JOIN OpportunityItems OI ON OBI.numOppItemID=OI.numoppitemtCode
            INNER JOIN Item I ON OBI.[numItemCode] = I.[numItemCode]
                        
            EXEC sp_xml_removedocument @hDocItem

        END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                               
                                                                                       

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,Opp.numCost,Opp.vcNotes as txtNotes,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc, 
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        (CASE 
							WHEN ISNULL(bitKitParent,0)=1 
									AND LEN(ISNULL(opp.vcChildKitSelectedItems,'')) > 0 
									AND (CASE 
											WHEN ISNULL(bitKitParent,0) = 1 
											THEN 
												(CASE 
													WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
													THEN 1
													ELSE 0
												END)
											ELSE 0 
										END) = 0
							THEN 
								dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,opp.vcChildKitSelectedItems)
							ELSE 
								ISNULL(fltWeight,0) 
						END) [fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
					--	ISNULL(W.vcWareHouse, '') AS vcWareHouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode], 
						ISNULL(opp.vcPromotionDetail, '') AS vcPromotionDetail

						, CASE WHEN ISNULL(opp.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE opp.ItemReleaseDate 
						END AS ItemRequiredDate

              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
						LEFT JOIN OpportunityMaster OM ON opp.numOppId = OM.numOppId
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END


	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
       

        SELECT TOP 1
                @vcTaxName = vcTaxName,
                @numTaxItemID = numTaxItemID
        FROM    TaxItems
        WHERE   numDomainID = @numDomainID
                AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS DECIMAL(20,5)
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainID) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainID
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetARAccountID')
DROP PROCEDURE USP_OpportunityBizDocs_GetARAccountID

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetARAccountID]
    @numOppBizDocsId AS NUMERIC(18,0)
AS 
BEGIN
	DECLARE @numARAccountID NUMERIC(18,0)


	SELECT
		@numARAccountID=COA.numAccountId
	FROM
		OpportunityBizDocs OBD
	INNER JOIN
		Chart_Of_Accounts COA
	ON
		OBD.numARAccountID=COA.numAccountId
	WHERE
		numOppBizDocsId=@numOppBizDocsId

	SELECT ISNULL(@numARAccountID,0) AS numARAccountID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@numTotalInsuredValue NUMERIC(18,2),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX),@numTotalCustomsValue NUMERIC(18,2)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	SELECT
		@IsCOD = IsCOD
		,@IsHomeDelivery = IsHomeDelivery
		,@IsInsideDelevery = IsInsideDelevery
		,@IsInsidePickup = IsInsidePickup
		,@IsSaturdayDelivery = IsSaturdayDelivery
		,@IsSaturdayPickup = IsSaturdayPickup
		,@IsAdditionalHandling = IsAdditionalHandling
		,@IsLargePackage=IsLargePackage
		,@vcCODType = vcCODType
		,@vcDeliveryConfirmation = vcDeliveryConfirmation
		,@vcDescription = vcDescription
		,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		,@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
		,@numOrderShippingService = ISNULL(numShippingService,0)
		,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
		,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	LEFT JOIN
		DivisionMasterShippingConfiguration
	ON
		DivisionMasterShippingConfiguration.numDomainID=@numDomainId
		AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
	WHERE
		OpportunityMaster.numDomainId = @numDomainId
		AND DivisionMaster.numDomainID=@numDomainId
		AND OpportunityMaster.numOppId = @numOppID

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
	IF ISNULL(@numOrderShippingCompany,0) > 0
	BEGIN
		IF ISNULL(@numOrderShippingService,0) = 0
		BEGIN
			SET @numOrderShippingService = CASE @numOrderShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numOrderShippingCompany
		SET @numShippingService = @numOrderShippingService
	END
	ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	BEGIN
		IF ISNULL(@numDivisionShippingService,0) = 0
		BEGIN
			SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numDivisionShippingCompany
		SET @numShippingService = @numDivisionShippingService
	END
	ELSE -- USE DOMAIN DEFAULT
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @numShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END



	DECLARE @bitUseBizdocAmount BIT

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END
	ELSE
	BEGIN
		SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,numUsedContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T1.numTotalContainer
		,0 AS numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
			,CEILING(SUM(OBI.numUnitHour)/CAST(I.numNoItemIntoContainer AS FLOAT)) AS numTotalContainer
			,0 AS numUsedContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
		AND T1.numWareHouseID = T2.numWareHouseID

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,(CASE 
					WHEN ISNULL(bitKitParent,0)=1 
							AND LEN(ISNULL(OI.vcChildKitSelectedItems,'')) > 0 
							AND (CASE 
									WHEN ISNULL(bitKitParent,0) = 1 
									THEN 
										(CASE 
											WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
											THEN 1
											ELSE 0
										END)
									ELSE 0 
								END) = 0
					THEN 
						dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
					ELSE 
						ISNULL(fltWeight,0) 
				END)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			OBD.numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty AND numUsedContainer < numTotalContainer)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,(CASE 
											WHEN @numShippingCompany = 88 THEN 19
											WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
											WHEN @numShippingCompany = 91 THEN 7
										END)
										,@numShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > 0 THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/166) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty

								IF @numTempMainContainerQty = 0
								BEGIN
									-- IF NO QTY LEFT TO ADD IN CONTAINER RESET CONAINER QTY AND MAKE FLAG bitItemAdded=0 SO IF numUsedContainer < numTotalContainer THEN NEW SHIPPING BOX WILL BE CREATED
									UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
								END

								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								-- CREATE NEW SHIPING BOX
								INSERT INTO ShippingBox
								(
									vcBoxName
									,numShippingReportId
									,fltTotalWeight
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numPackageTypeID
									,numServiceTypeID
									,fltDimensionalWeight
									,numShipCompany
								)
								VALUES
								(
									CONCAT('Box',@j)
									,@numShippingReportID
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@fltTempHeight
									,@fltTempWidth
									,@fltTempLength
									,GETUTCDATE()
									,@numUserCntID
									,(CASE 
										WHEN @numShippingCompany = 88 THEN 19
										WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
										WHEN @numShippingCompany = 91 THEN 7
									END)
									,@numShippingService
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@numShippingCompany
										
								)

								SELECT @numBoxID = SCOPE_IDENTITY()

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
									
								SET @j = @j + 1

								--SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProcesstoOpportunity')
DROP PROCEDURE USP_ManageProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_ManageProcesstoOpportunity]
@numDomainID as numeric(9)=0,    
@numProcessId as numeric(18)=0,      
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN
 INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId)
     SELECT Slp_Name,
            numdomainid,
            pro_type,
            numCreatedby,
            dtCreatedon,
            numModifedby,
            dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,@numOppId,numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=@numProcessId    
	DECLARE @numNewProcessId AS NUMERIC(18,0)=0
	SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
	IF(@numOppId>0)
	BEGIN
		UPDATE OpportunityMaster SET numBusinessProcessID=@numNewProcessId WHERE numOppId=@numOppId
	END
	
	INSERT INTO StagePercentageDetails(
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		numCreatedBy, 
		bintCreatedDate, 
		numModifiedBy, 
		bintModifiedDate, 
		slp_id, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		dtStartDate, 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		numOppID, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder
	)
	SELECT
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		@numCreatedBy, 
		GETDATE(), 
		@numCreatedBy, 
		GETDATE(), 
		@numNewProcessId, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		GETDATE(), 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		@numOppId, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder
	FROM
		StagePercentageDetails
	WHERE
		slp_id=@numProcessId	

	INSERT INTO StagePercentageDetailsTask(
		numStageDetailsId, 
		vcTaskName, 
		numHours, 
		numMinutes, 
		numAssignTo, 
		numDomainID, 
		numCreatedBy, 
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask,
		numOrder,
		numReferenceTaskId
	)
	SELECT 
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numOppID=@numOppId),
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		@numDomainID,
		@numCreatedBy,
		GETDATE(),
		@numOppId,
		@numProjectId,
		0,
		1,
		CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
		numOrder,
		numTaskId
	FROM 
		StagePercentageDetailsTask AS ST
	LEFT JOIN
		StagePercentageDetails As SP
	ON
		ST.numStageDetailsId=SP.numStageDetailsId
	WHERE
		ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
		ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId)
	ORDER BY ST.numOrder

END 

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0,
@bitAutoClosedTaskConfirmed As BIT=0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask
		)
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		DECLARE @numTaskValidatorId AS NUMERIC(18,0)=0
		DECLARE @numTaskSlNo AS NUMERIC(18,0)=0
		DECLARE @numStageOrder AS NUMERIC(18,0)=0
		DECLARE @numStagePercentageId AS NUMERIC(18,0)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask,@numTaskSlNo=numOrder
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
		SELECT @numTaskValidatorId=numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=(SELECT TOP 1 slp_id FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)

		DECLARE @bitRunningDynamicMode AS BIT=0
		SELECT TOP 1 
			@bitRunningDynamicMode=bitRunningDynamicMode,
			@numStageOrder=numStageOrder ,
			@numStagePercentageId=numStagePercentageId
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numStageDetailsId
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND @numOppId>0)
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				DECLARE @numReferenceTaskId AS NUMERIC=0
				DECLARE @numReferenceStageDetailsId AS NUMERIC=0
				SELECT TOP 1 @numReferenceTaskId=numReferenceTaskId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
				SELECT TOP 1 @numReferenceStageDetailsId=numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numReferenceTaskId
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask,
				numReferenceTaskId
			)
			SELECT 
				@numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1, 
				numTaskId
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numReferenceTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=@numReferenceTaskId AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numReferenceStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) 					
			END

	
					
		END
		IF((@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=2 AND @bitTaskClosed=1) OR (@numOppId>0 AND @numTaskValidatorId=1 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
		END
		IF((@numOppId>0 AND @numTaskValidatorId=3 AND @bitTaskClosed=1) OR (@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=4 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
			IF(ISNULL(@numStageOrder,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder<@numStageOrder AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId<@numStageDetailsId AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId<@numStagePercentageId AND numOppId=@numOppId)
		END
	
		UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		IF(@numHours>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numHours=@numHours
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numMinutes>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numMinutes=@numMinutes
			WHERE
				numTaskId=@numTaskId
		END
		IF(LEN(@vcTaskName)>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				vcTaskName=@vcTaskName
			WHERE
				numTaskId=@numTaskId
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
	IF(@numOppId>0)
	BEGIN
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numOppId=@numOppId AND bitSavedTask=1)
		SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numOppId=@numOppId AND bitSavedTask=1 AND bitTaskClosed=1)
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE numOppId=@numOppId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE numOppId=@numOppId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress
			(
				numOppId,
				numDomainId,
				intTotalProgress
			)VALUES(
				@numOppId,
				@numDomainID,
				@intTotalProgress
			)
		END
	END
END 
