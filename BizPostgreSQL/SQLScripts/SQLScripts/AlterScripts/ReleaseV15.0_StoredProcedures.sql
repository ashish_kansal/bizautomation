/******************************************************************
Project: Release 15.0 Date: 08.MAR.2021
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemProfitAmountOrMargin')
DROP FUNCTION GetOrderItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetOrderItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,TEMP.numItemCode,@numDomainID,numPurchaseUnit) 
																						ELSE ISNULL(TEMP.monAverageCost,0) 
																					END)
																			END)))
		FROM
		(SELECT
			OM.numOppId
			,OI.numoppitemtCode
			,I.numItemCode
			,I.numBaseUnit
			,I.numPurchaseUnit
			,OI.vcAttrValues
			,OI.numUnitHour
			,OI.monTotAmount
			,V.monCost
			,(CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 
				THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
						ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
				ELSE ISNULL(OI.monAvgCost,0) 
			END) monAverageCost
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
			AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = TEMP.numOppId
				AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = TEMP.numOppId
				AND OIInner.numItemCode = TEMP.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(TEMP.vcAttrValues,'')
		) TEMPPO
		
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
				,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,TEMP.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE ISNULL(TEMP.monAverageCost,0) 
																				END)
																	END)) Profit
			FROM
			(SELECT
				OM.numOppId
				,OI.numoppitemtCode
				,I.numItemCode
				,I.numBaseUnit
				,I.numPurchaseUnit
				,OI.vcAttrValues
				,OI.numUnitHour
				,OI.monTotAmount
				,V.monCost
				,(CASE 
					WHEN ISNULL(I.bitKitParent,0) = 1 
					THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
							ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
					ELSE ISNULL(OI.monAvgCost,0) 
				END) monAverageCost
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				DivisionMaster DM
			ON
				OM.numDivisionId = DM.numDivisionID
			INNER JOIN
				CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId = @numOppID
				AND OI.numoppitemtCode = @numOppItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
			) TEMP
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = TEMP.numOppId
					AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = TEMP.numOppId
					AND OIInner.numItemCode = TEMP.numItemCode
					AND OIInner.vcAttrValues = TEMP.vcAttrValues
			) TEMPPO
			
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteItemList]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitemlist')
DROP PROCEDURE usp_deleteitemlist
GO
CREATE PROCEDURE [dbo].[USP_DeleteItemList]          
@numListItemID as numeric(9)=0,            
@numListId as numeric(9)=0  
as              
Begin    

	IF ISNULL((SELECT constFlag FROM ListDetails where numListItemID=@numListItemID),0) = 0
	BEGIN
		IF @numListId = 27 AND EXISTS (SELECT numDomainId FROM AuthoritativeBizDocs WHERE numAuthoritativeSales=@numListItemID OR numAuthoritativePurchase=@numListItemID)
		BEGIN
			RAISERROR('USED_AS_AUTHORITATIVE_BIZDOC',16,1)
		END
		ELSE
		BEGIN
			DELETE FROM ListDetails WHERE numListItemID=@numListItemID   
   
			/* delete dependant dropdown relations if anyone in use */
			DELETE FROM [FieldRelationshipDTL] WHERE [numPrimaryListItemID] = @numListItemID
			DELETE FROM [FieldRelationshipDTL] WHERE [numSecondaryListItemID] = @numListItemID
		END
	END

End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging')
DROP PROCEDURE USP_GetAccountReceivableAging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
	@numDomainId   AS NUMERIC(9)  = 0,
	@numDivisionId AS NUMERIC(9)  = NULL,
	@numUserCntID AS NUMERIC(9),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME = NULL,
	@dtToDate AS DATETIME = NULL,
	@ClientTimeZoneOffset INT = 0,
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
				OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = Opp.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate)) <= CAST(@dtToDate AS DATE) THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= CAST(@dtToDate AS DATE) THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND (CASE 
						WHEN ISNULL(@bitCustomerStatement,0) = 1 
						THEN CONVERT(DATE,OB.[dtCreatedDate]) 
						ELSE CONVERT(DATE,DATEADD(DAY,CASE 
														WHEN Opp.bitBillingTerms = 1 AND @tintBasedOn=1
														THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
														ELSE 0 
													END, dtFromDate)) 
						END) <= @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate
				,TablePayments.monPaidAmount
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
		SELECT
			@numUserCntID,0, numDivisionId,0,0,0,0,NULL,0,SUM(monDepositAmount)
		FROM
		(
			SELECT
				DM.numDivisionID
				,ISNULL(DM.monDepositAmount,0) -  ISNULL(DM.monAppliedAmount,0) monDepositAmount
			FROM
				DepositMaster DM 
			JOIN 
				[dbo].[DivisionMaster] AS DIV 
			ON 
				DIV.[numDivisionID] = DM.[numDivisionID]
			WHERE 
				DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
				AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
				AND ((ISNULL(monDepositAmount,0) - ISNULL(DM.monAppliedAmount,0)) <> 0)
				AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
				AND ISNULL(DM.numReturnHeaderID,0) = 0
			UNION ALL
			SELECT
				DM.numDivisionID
				,ISNULL(DM.monDepositAmount,0) - (CASE 
													WHEN RH.tintReturnType=4 
													THEN ISNULL(monAppliedAmount,0)
													ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate) <= @dtToDate),0) 
												END) monDepositAmount
			FROM
				DepositMaster DM 
			JOIN 
				[dbo].[DivisionMaster] AS DIV 
			ON 
				DIV.[numDivisionID] = DM.[numDivisionID]
			LEFT JOIN	
				ReturnHeader RH
			ON
				DM.numReturnHeaderID = RH.numReturnHeaderID
			WHERE 
				DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
				AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
				AND CAST(ISNULL(monDepositAmount,0) - (CASE 
														WHEN RH.tintReturnType=4 
														THEN ISNULL(monAppliedAmount,0)
														ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate) <= @dtToDate),0) 
													END) AS DECIMAL(18,2)) <> 0
				AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
				AND ISNULL(DM.numReturnHeaderID,0) > 0
		) TEMP
		GROUP BY 
			numDivisionId

INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numCurrentDays],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intCurrentDaysCount],
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numCurrentDaysPaid],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
				0 numCurrentDays,
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
				0 [intCurrentDaysCount],
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numCurrentDaysPaid,
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

UPDATE  TempARRecord1
SET     numCurrentDays = X.numCurrentDays
FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numCurrentDaysPaid = X.numCurrentDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate])>= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
    FROM      [TempARRecord]
    WHERE     numUserCntID =@numUserCntID AND 
			 DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
			  AND 
			  numCurrencyID <>@baseCurrency )

---------------------------------------------------------------------------------------------


-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=  isnull(numCurrentDays,0) + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) - ISNULL(monUnAppliedAmount,0)
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numCurrentDays],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intCurrentDaysCount],
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numCurrentDaysPaid],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Details]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Details')
DROP PROCEDURE USP_GetAccountReceivableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Details]
(
    @numDomainId AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 varchar(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
	
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
    IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    --Get Master data
	 
	 SET @strSql = '
     SELECT DISTINCT OM.[numOppId],
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.[vcPOppName],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 ,0) TotalAmount,--OM.[monPAmount]
                        (ISNULL(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        ISNULL(OB.monDealAmount * ISNULL(OM.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(OM.fltExchangeRate, 1),0) BalanceDue,
						[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') dtDate,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],
                        CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.[numDivisionId]
        FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND (DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' OR ' + CAST(@numDivisionID AS VARCHAR) + '=0)
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1  AND ' + CAST(@tintBasedOn AS VARCHAR) + ' = 1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0'
    
    	--Show Impact of AR journal entries in report as well 
    	
SET @strSql1 = '    	
		UNION 
		 SELECT 
				-1 AS [numOppId],
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' [vcPOppName],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''				
    
   SET @strCredit = ' UNION SELECT -1 AS [numOppId],
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
								0 [numOppBizDocsId],
								'''' [vcBizDocID],
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' [varCurrSymbol],
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) <> 0)
		AND ISNULL(DM.numReturnHeaderID,0) = 0 
		UNION 
			SELECT -1 AS [numOppId],
					-1 AS numJournal_Id,
					numDepositId,tintDepositePage,isnull(DM.numReturnHeaderID,0) as numReturnHeaderID,
                    CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
					0 [numOppBizDocsId],
					'''' [vcBizDocID],
					(ISNULL(monDepositAmount,0) - (CASE 
														WHEN RH.tintReturnType=4 
														THEN ISNULL(monAppliedAmount,0)
														ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
													END)) * -1 AS TotalAmount,
					0 AmountPaid,
					(ISNULL(monDepositAmount,0) - (CASE 
													WHEN RH.tintReturnType=4 
													THEN ISNULL(monAppliedAmount,0)
													ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
													END)) * -1 AS BalanceDue,
					[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
					[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
					0 bitBillingTerms,
					0 intBillingDays,
					DM.dtCreationDate,
					'''' [varCurrSymbol],
					1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		 LEFT JOIN	
				ReturnHeader RH
			ON
				DM.numReturnHeaderID = RH.numReturnHeaderID
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND CAST(ISNULL(monDepositAmount,0) - (CASE 
												WHEN RH.tintReturnType=4 
												THEN ISNULL(monAppliedAmount,0)
												ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
											END) AS DECIMAL(18,2)) <> 0
		AND ISNULL(DM.numReturnHeaderID,0) > 0'  
    
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'

		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'
                             
                    END
                  ELSE
                    BEGIN
                      SET @strSql =@strSql +''
                    END
                    

	PRINT CAST((@strSql + @strSql1 + @strCredit) AS NTEXT)
	EXEC(@strSql + @strSql1 + @strCredit)
  END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = '',
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
						[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') dtDate,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND '+ (CASE 
							WHEN ISNULL(@bitCustomerStatement,0) = 1 
							THEN 'CONVERT(DATE,OB.[dtCreatedDate])' 
							ELSE 'CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 AND ' + CAST(@tintBasedOn AS VARCHAR) + '=1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
									END, dtFromDate))' 
						END) + ' <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) <> 0'     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) <> 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					AND ISNULL(DM.numReturnHeaderID,0) = 0
					UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - (CASE 
																		WHEN RH.tintReturnType=4 
																		THEN ISNULL(monAppliedAmount,0)
																		ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
																	END)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - (CASE 
																	WHEN RH.tintReturnType=4 
																	THEN ISNULL(monAppliedAmount,0)
																	ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
																END)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(DM.numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM
					LEFT JOIN	
						ReturnHeader RH
					ON
						DM.numReturnHeaderID = RH.numReturnHeaderID
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND CAST(ISNULL(monDepositAmount,0) - (CASE 
															WHEN RH.tintReturnType=4 
															THEN ISNULL(monAppliedAmount,0)
															ELSE ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)
														END) AS DECIMAL(18,2)) <> 0
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					AND ISNULL(DM.numReturnHeaderID,0) > 0
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'   
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END
 
   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetBillingTerms')
	DROP PROCEDURE Usp_GetBillingTerms
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_GetBillingTerms]
(
	@numTermsID	INT
	,@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintApplyTo TINYINT
)
AS
BEGIN
	SELECT numTermsID,vcTerms,tintApplyTo,numNetDueInDays,numDiscount,numDiscountPaidInDays,numListItemID,numDomainID, 
	(CAST(numTermsID AS VARCHAR(10)) + '~' + CAST(numNetDueInDays AS VARCHAR(10))) AS [vcValue],
	CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityMaster WHERE dbo.OpportunityMaster.intBillingDays = numTermsID AND [OpportunityMaster].[numDomainId] = @numDomainID ) > 0 THEN 1 ELSE 0 END AS [IsUsed],
	ISNULL(numInterestPercentage,0) AS [numInterestPercentage],
	ISNULL(numInterestPercentageIfPaidAfterDays,0) AS [numInterestPercentageIfPaidAfterDays],
	dbo.fn_GetContactName(numCreatedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,[dtCreatedDate])) AS CreatedBy,
    dbo.fn_GetContactName(numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,[dtModifiedDate])) AS ModifiedBy
	FROM dbo.BillingTerms
	WHERE (numTermsID = @numTermsID OR @numTermsID = 0)
	AND numDomainID = @numDomainID
	AND ISNULL(bitActive,0) = 1
	AND (ISNULL(@tintApplyTo,0) = 0 OR tintApplyTo = @tintApplyTo)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
	@numKitId AS NUMERIC(18,0)                            
	,@numWarehouseItemID NUMERIC(18,0)
	,@bitApplyFilter BIT=0
AS                            
BEGIN

	WITH CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPriceC
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
		,tintView
		,vcFields
		,bitUseInDynamicSKU
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#0#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST('' AS VARCHAR(1000))
			,convert(NUMERIC(18,0),0)
			,numItemCode
			,CONCAT(vcItemName,(CASE WHEN LEN(ISNULL(vcSKU,'')) > 0 THEN CONCAT(' (',vcSKU,')') ELSE '' END)) vcItemName
			,vcSKU
			,ISNULL(Item.bitKitParent,0)
			,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN ISNULL(item.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(item.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(item.numItemGroup,0) > 0 AND (ISNULL(item.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,ISNULL(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
			,ISNULL(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
			,ISNULL(Dtl.bitUseInDynamicSKU,0) bitUseInDynamicSKU
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',(c.StageLevel + 1),'#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.ID
			,Dtl.numItemKitID
			,i.numItemCode
			,CONCAT(i.vcItemName,(CASE WHEN LEN(ISNULL(i.vcSKU,'')) > 0 THEN CONCAT(' (',i.vcSKU,')') ELSE '' END)) vcItemName
			,i.vcSKU
			,ISNULL(i.bitKitParent,0)
			,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN ISNULL(i.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(i.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(i.numItemGroup,0) > 0 AND (ISNULL(i.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
			,ISNULL(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
			,ISNULL(Dtl.bitUseInDynamicSKU,0) bitUseInDynamicSKU
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=@numKitId
	)

	SELECT * INTO #temp FROM CTE

	;WITH Final AS 
	(
		SELECT 
			*
			,1 AS RStageLevel1 
		FROM 
			#temp 
		WHERE 
			numitemcode NOT IN (SELECT numItemKitID FROM #temp)
		UNION ALL
		SELECT 
			t.*
			,c.RStageLevel1 + 1 AS RStageLevel1 
		FROM 
			#temp t 
		JOIN 
			Final c 
		ON 
			t.numitemcode=c.numItemKitID
	)


	UPDATE 
		t 
	SET 
		t.RStageLevel=f.RStageLevel 
	FROM 
		#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
	WHERE 
		t.numitemcode=f.numitemcode 
		AND t.numItemKitID=f.numItemKitID 


	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		DECLARE @numWarehouseID NUMERIC(18,0)
		SELECT @numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
		IF(@bitApplyFilter=0)
		BEGIN
		IF EXISTS 
		(
			SELECT
				c.numItemCode
			FROM
				#temp c 
			WHERE
				c.ItemType='P'
				AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID=@numWarehouseID) = 0
		)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				WareHouseItems.numWareHouseID,numWareHouseItemID
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal

		DROP TABLE #TEMPFinal
	END
	ELSE IF @bitApplyFilter=1
	BEGIN
		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal1
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				1 AS numWareHouseItemID
				,SUM(numOnHand) AS numOnHand
				,SUM(numOnOrder) AS numOnOrder
				,0 AS numReorder
				,0 AS numAllocation
				,0 AS numBackOrder
				,0 AS numItemID
				,'' AS vcWareHouse
				,0 AS monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
			ORDER BY
				numWareHouseItemID
			
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal1 T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal1

		DROP TABLE #TEMPFinal1
	END
	ELSE
	BEGIN
		SELECT DISTINCT 
			c.*
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		ORDER BY 
			c.StageLevel
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(300),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
				AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3)  AS varchar)+'#'+CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3)  AS varchar) 
	WHERE vcDbColumnName='vcOrderedShipped'
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3)  AS varchar)
	WHERE vcDbColumnName='vcPOppName'
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName,ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monAmountPaid '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL(TEMPApproval.numApprovalCount,0) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'',ISNULL(monDealAmount,0),''#^#'',ISNULL(monAmountPaid,0),''#^#'', convert(varchar(10), cast(dtFromDate as date), 101),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order''),(CASE WHEN ISNULL(TEMPMapping.numMappingCount,0) > 0 THEN CONCAT(''&nbsp;<a target="_blank" href="../opportunity/frmOpportunities.aspx?frm=deallist&OpID='',Opp.numOppID,''&SelectedTab=ProductsServices"><i class="fa fa-map-marker" style="color:red;font-size:18px" aria-hidden="true"></i></a>'') ELSE '''' END))' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numReleaseStatus'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE Opp.numReleaseStatus WHEN 1 THEN ''Open'' WHEN 2 THEN ''Purchased'' ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ '),'
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
					SET @strColumns = @strColumns + ' ,dbo.fn_GetListItemName(intUsedShippingCompany) AS ShipVia '
					SET @strColumns = @strColumns + ' ,Opp.intUsedShippingCompany AS intUsedShippingCompany '
					--SET @strColumns = @strColumns + ' ,Opp.numShippingService AS numShippingService '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''intUsedShippingCompany'' AND numModuleID=3) AS ShipViaFieldId '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''numShippingService'' AND numModuleID=3) AS ShippingServiceFieldId '
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' AS vcShipmentService'
				
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=Div.numDivisionID))')

	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId   
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numApprovalCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitItemPriceApprovalRequired,0)=1 GROUP BY OIInner.numOppID) TEMPApproval ON Opp.numOppID=TEMPApproval.numOppID
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numMappingCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitMappingRequired,0)=1 GROUP BY OIInner.numOppID) TEMPMapping ON Opp.numOppID=TEMPMapping.numOppID                                                            
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		DECLARE @vcSortFieldType VARCHAR(50)

		SELECT @vcSortFieldType = Fld_type FROM CFW_Fld_Master WHERE Fld_id=CAST(REPLACE(@columnName,'CFW.Cust','') AS INT)

		IF ISNULL(@vcSortFieldType,'') = 'DateField'
		BEGIN
			SET @columnName='(CASE WHEN ISDATE(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL END)'
		END
		ELSE IF CAST(REPLACE(@columnName,'CFW.Cust','') AS INT) IN (12745,12846)
		BEGIN
			SET @columnName='(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)'
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'
		END            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID,' or Opp.numAssignedTo=',@numUserCntID
						,(CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END)
						,' or ',@strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID
						, ' or Opp.numAssignedTo= ',@numUserCntID
						, CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						, ' or ' + @strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	IF @CurrentPage = -1 AND @PageSize = -1
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',25,' ROWS ONLY;')
	END
	ELSE
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
	END
	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(18,0),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(18,0),
 @numDivisionID NUMERIC(18,0),
 @numWOId NUMERIC(18,0),
 @numProId NUMERIC(18,0),
 @numOppId NUMERIC(18,0),
 @numOppItemId NUMERIC(18,0),
 @numRecId NUMERIC(18,0),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(18,0),
 @numWODetailId NUMERIC(18,0),
 @numCommID NUMERIC(18,0)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=(CASE WHEN vcLookBackTableName='T' THEN 'Tickler' ELSE vcLookBackTableName END),
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		UPDATE DivisionMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
			DECLARE @tintCommitAllocation1 TINYINT
			SELECT @tintCommitAllocation1=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF (@tintOppStatus=0 OR @tintOppStatus=2) AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

					IF @tintCommitAllocation1=1
					BEGIN
		 				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
					END

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
					UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
					EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF ((@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0')) AND @tintCommitAllocation1=1 --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END
		ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
		BEGIN
			UPDATE OpportunityMaster SET numShippingService=0 WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		IF @vcOrigDbColumnName = 'Description'
		BEGIN
			SET @vcOrigDbColumnName = 'textDetails'
		END
		ELSE IF @vcOrigDbColumnName = 'Activity'
		BEGIN
			SET @vcOrigDbColumnName = 'numActivity'
		END 
		ELSE IF @vcOrigDbColumnName = 'Priority'
		BEGIN
			SET @vcOrigDbColumnName = 'numStatus'
		END

		IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId=@numCommID and numDomainID=@numDomainID))
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
		ELSE IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID=@numCommID))
		BEGIN
			SET @strSql='update Activity set Comments=@InlineEditValue where ActivityID=@numCommId'
		END
		ELSE
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Item'
	BEGIN
		IF @vcDbColumnName = 'monListPrice'
		BEGIN
			SET @InlineEditValue = REPLACE(@InlineEditValue,',','')

			IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
			BEGIN
				UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
				SELECT @InlineEditValue AS vcData
			END
			ELSE
			BEGIN
				RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
			END

			RETURN
		END

		SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Warehouses'
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppID=@numOppId AND ISNULL(tintOppStatus,0)=1 AND ISNULL(tintshipped,0)=0)
		BEGIN
			IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue)
			BEGIN
				DECLARE @numTempOppItemID NUMERIC(18,0)
				DECLARE @numTempItemCode NUMERIC(18,0)
				DECLARE @numOldWarehouseItemID NUMERIC(18,0)
				DECLARE @numNewWarehouseItemID NUMERIC(18,0)
				DECLARE @tintCommitAllocation TINYINT 

				SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID


				DECLARE @TEMPItems TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppItemID NUMERIC(18,0)
					,numItemCode NUMERIC(18,0)
					,numWarehouseItemID NUMERIC(18,0)
				)
				INSERT INTO @TEMPItems
				(
					numOppItemID
					,numItemCode
					,numWarehouseItemID
				)
				SELECT
					numoppitemtCode
					,numItemCode
					,numWarehouseItmsID
				FROM 
					OpportunityItems
				WHERE
					numOppID=@numOppID
					AND (numOppItemtcode=@numOppItemId OR ISNULL(@numOppItemId,0) = 0)

				IF EXISTS (SELECT TI.numItemCode FROM @TEMPItems TI LEFT JOIN WareHouseItems WI ON TI.numItemCode=WI.numItemID AND WI.numWareHouseID=@InlineEditValue WHERE WI.numWareHouseItemID IS NULL)
				BEGIN
					RAISERROR('WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM',16,1)
				END
				ELSE
				BEGIN
					DECLARE @j INT = 1
					DECLARE @jCount INT 

					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempOppItemID = numOppItemID
							,@numTempItemCode = numItemCode
							,@numOldWarehouseItemID = numWarehouseItemID
						FROM 
							@TEMPItems TI 
						WHERE 
							ID=@j

						SELECT TOP 1
							@numNewWarehouseItemID = numWareHouseItemID
						FROM
							WareHouseItems 
						WHERE 
							numDomainID=@numDomainID 
							AND numItemID=@numTempItemCode 
							AND numWareHouseID=@InlineEditValue
						ORDER BY
							ISNULL(numWLocationID,0) ASC

						IF @numOldWarehouseItemID <> @numNewWarehouseItemID
						BEGIN
							BEGIN TRY
							BEGIN TRANSACTION 
								--REVERTING BACK THE WAREHOUSE ITEMS                  
								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_RevertDetailsOpp @numOppID,@numTempOppItemID,0,@numUserCntID                  
								END  

								UPDATE OpportunityItems SET numWarehouseItmsID=@numNewWarehouseItemID WHERE numoppitemtCode=@numTempOppItemID

								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numTempOppItemID,@numUserCntID
								END  

								UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=0 WHERE numoppitemtCode=@numTempOppItemID
							COMMIT
							END TRY
							BEGIN CATCH
								IF @@TRANCOUNT > 0
									ROLLBACK

								-- Raise an error with the details of the exception
								DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
								SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY()

								RAISERROR(@ErrMsg, @ErrSeverity, 1)
							END CATCH
						END

						SET @j = @j + 1
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
			END
		END
	END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(18,0),@numUserCntID NUMERIC(18,0),@numDivisionID NUMERIC(18,0),@numContactID NUMERIC(18,0),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(18,0),@numProId NUMERIC(18,0),@numOppId NUMERIC(18,0),@numOppItemId NUMERIC(18,0),@numCaseId NUMERIC(18,0),@numWODetailId NUMERIC(18,0),@numCommID Numeric(18,0),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numOppItemId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			ELSE IF @vcDbColumnName='intDropShip'
			BEGIN
				 SELECT CASE WHEN ISNULL(@InlineEditValue,0)=1 THEN 'Not Available'
						 WHEN ISNULL(@InlineEditValue,0)=2 THEN 'Blank Available'
						 WHEN ISNULL(@InlineEditValue,0)=3 THEN 'Vendor Label'
						 WHEN ISNULL(@InlineEditValue,0)=4 THEN 'Private Label'
						 ELSE 'Select One'
					END AS vcData
			END
			ELSE IF @vcDbColumnName = 'numProjectID'
			BEGIN
				SELECT ISNULL((SELECT vcProjectName FROM ProjectsMaster WHERE numDomainID=@numDomainID AND numProID=@InlineEditValue),'-') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			IF @vcDbColumnName='tintActive'
		  	BEGIN
				SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			END
			ELSE IF @vcDbColumnName='vcWareHouse' AND ISNUMERIC(@InlineEditValue) = 1
			BEGIN
				SELECT @InlineEditValue=vcWareHouse FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue
			END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

 
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems <> 'IA1' AND chBizDocItems <> 'OE1')
	BEGIN
		SET @bitItemIsUsedInOrder =1
    END
	                                      
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(I.numBusinessProcessId,0) numBusinessProcessId,
		ISNULL(I.bitTimeContractFromSalesOrder,0) AS bitTimeContractFromSalesOrder
		,ISNULL(I.bitAutomateReorderPoint,0) bitAutomateReorderPoint
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
		,I.numBusinessProcessId,I.bitTimeContractFromSalesOrder,I.bitAutomateReorderPoint
END
/****** Object:  StoredProcedure [dbo].[usp_LeadDetails]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- modified by anoop jayaraj                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_leaddetails')
DROP PROCEDURE usp_leaddetails
GO
CREATE PROCEDURE [dbo].[usp_LeadDetails]                                                                                   
 @numContactID numeric(9),  
 @numDomainID as numeric(9),  
@ClientTimeZoneOffset  int                                        
AS                                                
BEGIN                                                
 SELECT                                                 
  cmp.vcCompanyName,                                                
  DM.vcDivisionName,                                                
  ADC.vcFirstName,                                                
  ADC.vcLastName,                                                
  cmp.vcWebSite,                                                
  ADC.vcEmail,                                                
--  ConAdd.vcStreet,                                      
  DM.numFollowUpStatus,        
  DM.numTerID,                                                
--  ConAdd.vcCity,                                                
--  ConAdd.vcState,                                                
--  ConAdd.intPostalCode,                                                
--  ConAdd.vcCountry,                                                
  ADC.vcPosition,                                                
  ADC.numPhone,                                                
  ADC.numPhoneExtension,                                                
  cmp.numNoOfEmployeesId,                                                
  cmp.numAnnualRevID,                                                
  cmp.vcHow,                                                
  cmp.vcProfile,                                                
  ADC.txtNotes,                                              
  DM.numCreatedBy,                                       
  dm.numgrpid ,                           
  cmp.txtComments,                             
  cmp.numCompanyType,                         
 ADC.vcTitle,                        
  DM.vcComPhone,                        
  DM.vcComFax,                                  
  ISNULL(dbo.fn_GetContactName(DM.numCreatedBy), '&nbsp;&nbsp;-')+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) AS vcCreatedBy,                                                                
  DM.numModifiedBy,                                        
  ISNULL(dbo.fn_GetContactName(DM.numModifiedBy), '&nbsp;&nbsp;-')+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) AS vcModifiedBy,                                                           
  DM.numRecOwner,                                        
  ISNULL(dbo.fn_GetContactName(DM.numRecOwner), '&nbsp;&nbsp;-') AS vcRecordOwner,               
  DM.numAssignedBy,              
  DM.numAssignedTo,                                       
  DM.numCampaignID   ,      
    dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as address,DM.vcPartnerCode AS vcPartnerCode   ,
	ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
 A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
 (select  count(*) from dbo.GenericDocuments where numRecID= DM.numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount,
(CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,
(CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,
ISNULL(DM.vcBuiltWithJson,'') vcBuiltWithJson
--case when DM.vcBillStreet is null then '' when DM.vcBillStreet = ''then '' else vcBillStreet end +         
-- case when DM.vcBillCity is null then '' when DM.vcBillCity ='' then '' else DM.vcBillCity end +         
-- case when dbo.fn_GetState(DM.vcBilState) is null then '' when dbo.fn_GetState(DM.vcBilState) ='' then '' else ','+ dbo.fn_GetState(DM.vcBilState)end +         
-- case when DM.vcBillPostCode is null then '' when DM.vcBillPostCode ='' then '' else ','+DM.vcBillPostCode end +         
-- case when dbo.fn_GetListName(DM.vcBillCountry,0) is null then '' when dbo.fn_GetListName(DM.vcBillCountry,0) ='' then '' else ','+dbo.fn_GetListName(DM.vcBillCountry,0) end  as address                                         
 FROM                                                  
  CompanyInfo cmp join DivisionMaster DM                                                 
  on cmp.numCompanyID=DM.numCompanyID                                 
  join  AdditionalContactsInformation ADC                                                   
   on  DM.numDivisionID=ADC.numDivisionID    
     LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId                       
 WHERE                                                                  
  DM.tintCRMType = 0                                                                          
  AND ADC.numContactId = @numContactID  and ADC.numDomainID=@numDomainID                                              
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0,
@vcBuiltWithJson NVARCHAR(MAX) = ''
AS   
BEGIN
	DECLARE @numPartenerSource NUMERIC(18,0)      
	DECLARE @numPartenerContact NUMERIC(18,0)     

	IF ISNULL(@numPartner,0) = 0
	BEGIN
		SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
	END
	ELSE 
	BEGIN
		SET @numPartenerSource = @numPartner
	END

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
	IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
	BEGIN
 		SET @numGrpId = 5
	END
                                                                                                               
	IF @numDivisionId is null OR @numDivisionId=0                                                 
	BEGIN
		IF @UpdateDefaultTax=1 
			SET @bitNoTax=0
                                                                                  
		INSERT INTO DivisionMaster                                    
		(                      
			numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,bitPublicFlag,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                                    
			bitLeadBoxFlg,numTerID,numStatusID,numRecOwner,tintBillingTerms,numBillingDays,tintInterestType,fltInterest,numCampaignID,vcComPhone,vcComFax,bitNoTax,
			numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact,vcBuiltWithJson     
		)                                    
		VALUES                      
		(                      
			@numCompanyID,@vcDivisionName,@numGrpId,@numFollowUpStatus,@bitPublicFlag,@numUserCntID,GETUTCDATE(),@numUserCntID,GETUTCDATE(),@tintCRMType,@numDomainID,                       
			@bitLeadBoxFlg,@numTerID,@numStatusID,@numUserCntID,(CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),@numBillingDays,0,0,@numCampaignID,@vcComPhone,            
			@vcComFax,@bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact,@vcBuiltWithJson                        
		)                                  
	
		SELECT @numDivisionID = SCOPE_IDENTITY()

		IF @numBillAddressID > 0 OR @numShipAddressID > 0
		BEGIN
			IF @numBillAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
			END

			IF @numShipAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
			END
		END
		ELSE
		BEGIN
		  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
			IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
			END
			ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END
		END
       --End of Sachin Script                   

		IF @UpdateDefaultTax=1 
		BEGIN
			INSERT INTO DivisionTaxTypes
			SELECT 
				@numDivisionID,numTaxItemID,1 
			FROM 
				TaxItems
			WHERE 
				numDomainID=@numDomainID
			UNION
			SELECT 
				@numDivisionID,0,1 
		END                                                
		                  
		DECLARE @numGroupID NUMERIC
		SELECT TOP 1 
			@numGroupID=numGroupID 
		FROM 
			dbo.AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND tintGroupType=2
		
		IF @numGroupID IS NULL 
			SET @numGroupID = 0 
		
		INSERT INTO ExtarnetAccounts 
		(numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
		VALUES
		(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                                                                 
	END                                                        
	ELSE if @numDivisionId>0                                                      
	BEGIN
		DECLARE @numFollow AS VARCHAR(10)                                        
		DECLARE @binAdded AS VARCHAR(20)                                        
		DECLARE @PreFollow AS VARCHAR(10)                                        
		DECLARE @RowCount AS VARCHAR(2)                                        
		SET @PreFollow=(SELECT COUNT(*) numFollowUpStatus FROM FollowUpHistory WHERE numDivisionID= @numDivisionID)                                        
    
		SET @RowCount=@@rowcount                                        
    
		SELECT 
			@numFollow=numFollowUpStatus,
			@binAdded=bintModifiedDate 
		FROM 
			divisionmaster
		WHERE 
			numDivisionID=@numDivisionID                                         
    
		IF @numFollow <>'0' AND @numFollow <> @numFollowUpStatus                                        
		BEGIN
			SELECT TOP 1 
				numFollowUpStatus 
			FROM 
				FollowUpHistory 
			WHERE 
				numDivisionID= @numDivisionID 
			ORDER BY 
				numFollowUpStatusID DESC                                        
                                         
			IF @PreFollow<>0                                        
			BEGIN
				IF @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
				BEGIN
					INSERT INTO FollowUpHistory
					(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
					VALUES
					(@numFollow,@numDivisionID,@binAdded)                                        
				END                                        
			END                              
			ELSE
			BEGIN
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
				VALUES
				(@numFollow,@numDivisionID,@binAdded)                                   
			END                                                                 
		END                                                                                            
                                                        
		UPDATE 
			DivisionMaster                       
		SET                        
			numCompanyID = @numCompanyID ,                     
			vcDivisionName = @vcDivisionName,                                                        
			numGrpId = @numGrpId,                                                                       
			numFollowUpStatus =@numFollowUpStatus,                                                        
			numTerID = @numTerID,                                                   
			bitPublicFlag =@bitPublicFlag,                                                        
			numModifiedBy = @numUserCntID,                                                  
			bintModifiedDate = getutcdate(),                                                     
			tintCRMType = @tintCRMType,                                                      
			numStatusID = @numStatusID,                                                 
			tintBillingTerms = (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                                                      
			numBillingDays = @numBillingDays,                                                     
			tintInterestType = @tintInterestType,                                                       
			fltInterest =@fltInterest,                          
			vcComPhone=@vcComPhone,                          
			vcComFax= @vcComFax,                      
			numCampaignID=@numCampaignID,    
			bitNoTax=@bitNoTax
			,numCompanyDiff=@numCompanyDiff
			,vcCompanyDiff=@vcCompanyDiff
			,numCurrencyID = @numCurrencyID
			,numPartenerSource=@numPartenerSource
			,numPartenerContact=@numPartenerContact                                                         
		WHERE 
			numDivisionID = @numDivisionID       

		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcBillStreet,                                    
			vcCity =@vcBillCity,                                    
			vcPostalCode=@vcBillPostCode,                                     
			numState=@vcBillState,                                    
			numCountry=@vcBillCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=1         
	   
		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcShipStreet,                                    
			vcCity =@vcShipCity,                                    
			vcPostalCode=@vcShipPostCode,                                     
			numState=@vcShipState,                                    
			numCountry=@vcShipCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=2
	END

    ---Updating if organization is assigned to someone                
	DECLARE @tempAssignedTo AS NUMERIC(9)              
              
	SELECT 
		@tempAssignedTo=isnull(numAssignedTo,0) 
	FROM 
		DivisionMaster 
	WHERE 
		numDivisionID = @numDivisionID                
        
	IF (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
	BEGIN                
		UPDATE 
			DivisionMaster 
		SET 
			numAssignedTo=@numAssignedTo
			,numAssignedBy=@numUserCntID 
		WHERE 
			numDivisionID = @numDivisionID                
	END               
  
	SELECT @numDivisionID
 END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
	END

	If (@charItemType = 'P' AND ISNULL(@bitKitParent,0) = 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
			RETURN
		END
	END

	If @charItemType = 'P' OR ISNULL((SELECT bitExpenseNonInventoryItem FROM Domain WHERE numDomainID=@numDomainID),0) = 1
	BEGIN
		-- This is common check for CharItemType P and Other
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_COGS_ACCOUNT',16,1)
			RETURN
		END
	END

	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
		RETURN
	END

	IF ISNULL(@bitExpenseItem,0) = 1
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			RETURN
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode,0) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode,0) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder
				,tintView = X.tintView
				,vcFields=X.vcFields
				,bitUseInDynamicSKU=X.bitUseInDynamicSKU                                                                                     
			From 
			(
				SELECT 
					QtyItemsReq
					,ItemKitID
					,ChildItemID
					,numUOMId
					,vcItemDesc
					,sintOrder
					,numItemDetailID As ItemDetailID
					,tintView
					,vcFields
					,bitUseInDynamicSKU
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0),
					tintView TINYINT,
					vcFields VARCHAR(500),
					bitUseInDynamicSKU BIT
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_ManagekitParentsForChildItems]    Script Date: 07/26/2008 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managekitparentsforchilditems')
DROP PROCEDURE usp_managekitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_ManagekitParentsForChildItems]
@numItemDetailID NUMERIC(18,0),
@numItemKitID NUMERIC(18,0),  
@numChildItemID NUMERIC(18,0),  
@numQtyItemsReq NUMERIC(18,0),  
@byteMode TINYINT
AS  
IF @byteMode=0  
BEGIN
	IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND (ISNULL(bitAssembly,0) = 1 OR ISNULL(bitKitParent,0) = 1))
	BEGIN
		IF @numItemKitID = @numChildItemID
		BEGIN
			RAISERROR('CAN_NOT_ADD_PARENT_AS_CHILD',16,1)
		END
		ELSE IF EXISTS(SELECT numItemDetailID FROM ItemDetails WHERE numItemKitID=@numItemKitID AND numChildItemID=@numChildItemID)
		BEGIN
			RAISERROR('CHILD_ITEM_ALREADY_ADDED',16,1)
		END
		ELSE IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND ISNULL(bitAssembly,0) = 1) AND EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numChildItemID AND ISNULL(bitKitParent,0) = 1)
		BEGIN
			RAISERROR('CAN_NOT_ADD_KIT_AS_ASSEMBLY_CHILD',16,1)
		END
		ELSE IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND ISNULL(bitKitParent,0) = 1) AND EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numChildItemID AND ISNULL(bitKitParent,0) = 1) AND EXISTS (SELECT numItemDetailID FROM ItemDetails WHERE numChildItemID=@numItemKitID)
		BEGIN
			RAISERROR('CAN_NOT_ADD_KIT_AS_KIT_IS_ALREADY_CHILD_OTHER_ITEM',16,1)
		END
		ELSE 
		BEGIN
			INSERT INTO ItemDetails 
			(numItemKitID,numChildItemID,numQtyItemsReq,sintOrder)  
			VALUES
			(@numItemKitID,@numChildItemID,@numQtyItemsReq,ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemKitID),0) + 1)
		END
	END

	
END  
  
IF @byteMode=1  
BEGIN  
	DELETE FROM ItemDetails WHERE numItemKitID=@numItemKitID and numChildItemID=@numChildItemID
END
IF @byteMode=2
BEGIN  
	DELETE FROM ItemDetails WHERE numItemDetailID=@numItemDetailID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageWarehouse]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managewarehouse')
DROP PROCEDURE usp_managewarehouse
GO
CREATE PROCEDURE [dbo].[USP_ManageWarehouse]      
@byteMode as tinyint=0,      
@numWareHouseID as numeric(9)=0,      
@vcWarehouse as varchar(50),          
@numDomainID as numeric(9)=0,
@numAddressID As numeric(18,0) = 0,
@vcPrintNodeAPIKey AS VARCHAR(100) = '',
@vcPrintNodePrinterID AS VARCHAR(100) = ''
AS      
BEGIN      
	IF @byteMode=0      
	BEGIN      
		IF @numWareHouseID=0      
		BEGIN
			INSERT INTO Warehouses 
			(
				vcWarehouse
				,numDomainID
				,numAddressID
				,vcPrintNodeAPIKey
				,vcPrintNodePrinterID	
			)      
			VALUES 
			(
				@vcWarehouse
				,@numDomainID
				,@numAddressID
				,@vcPrintNodeAPIKey
				,@vcPrintNodePrinterID
			)      
        
			DECLARE @numNewWarehouseID NUMERIC(18,0)
			SET @numNewWarehouseID = SCOPE_IDENTITY()


			INSERT INTO WareHouseItems 
			(
				numItemID, 
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified
			) 
			SELECT
				numItemCode
				,@numNewWarehouseID
				,NULL
				,0
				,0
				,monListPrice
				,''
				,vcSKU
				,numBarCodeId
				,@numDomainID
				,GETDATE()
			FROM
				Item
			WHERE
				numDomainID=@numDomainID
				AND charItemType='P'
				AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)

			INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  
				WHI.numWareHouseItemID
				,ISNULL(WHI.numOnHand,0)
				,ISNULL(WHI.numOnOrder,0)
				,ISNULL(WHI.numReorder,0)
				,ISNULL(WHI.numAllocation,0)
				,ISNULL(WHI.numBackOrder,0)
				,WHI.numDomainID
				,'INSERT WareHouse'
				,1
				,I.numItemCode
				,GETUTCDATE()
				,0
				,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)
				,(SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode)
				,GETUTCDATE()
            FROM 
				Item I 
			INNER JOIN 
				WareHouseItems WHI 
			ON 
				I.numItemCode=WHI.numItemID
			WHERE 
				WHI.numWareHouseID = @numNewWarehouseID
				AND [WHI].[numDomainID] = @numDomainID


			SELECT @numNewWarehouseID     
		END      
		ELSE IF @numWareHouseID>0      
		BEGIN
			UPDATE 
				Warehouses 
			SET    
				vcWarehouse= @vcWarehouse
				,numAddressID=@numAddressID
				,vcPrintNodeAPIKey=@vcPrintNodeAPIKey
				,vcPrintNodePrinterID=@vcPrintNodePrinterID
			WHERE 
				numWareHouseID=@numWareHouseID      
		
			SELECT @numWareHouseID      
		END      
	END
	ELSE IF @byteMode=1      
	BEGIN     
		IF (SELECT COUNT(*) FROM [WareHouseItems] WHERE numWarehouseID = @numWareHouseID  AND (ISNULL(numOnHand,0) > 0 OR ISNULL(numOnOrder,0) > 0 OR ISNULL(numAllocation,0) > 0 OR ISNULL(numBackOrder,0) > 0)) > 0
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE IF EXISTS (SELECT OI.numoppitemtCode FROM WareHouseItems WI INNER JOIN OpportunityItems OI ON WI.numWareHouseItemID=OI.numWarehouseItmsID WHERE WI.numWareHouseID=@numWareHouseID)
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE
		BEGIN
			DELETE FROM WareHouseItems WHERE ISNULL(numWareHouseID,0)=ISNULL(@numWareHouseID,0)
			DELETE FROM Warehouses WHERE numWareHouseID=@numWareHouseID	
		END
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID = 2 AND @tintPackingViewMode=1) 
	BEGIN
		SET @bitGroupByOrder = 1
	END
	
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'Item.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID'
								,'Warehouses.vcWareHouse')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @bitEnablePickListMapping BIT
	DECLARE @bitEnableFulfillmentBizDocMapping BIT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
		,@bitEnablePickListMapping=bitEnablePickListMapping
		,@bitEnableFulfillmentBizDocMapping=bitEnableFulfillmentBizDocMapping
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,(CASE WHEN vcFieldDataType IN ('N','M') THEN 1 ELSE 0 END)
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @vcSelect NVARCHAR(MAX) = CONCAT('SELECT 
												OpportunityMaster.numOppID
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,ISNULL(DivisionMaster.numTerID,0) numTerID
												,ISNULL(OpportunityMaster.intUsedShippingCompany,0) numOppShipVia
												,ISNULL(OpportunityMaster.numShippingService,0) numOppShipService
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocID
												,ISNULL(OpportunityBizDocs.vcBizDocID,'''') vcBizDocID
												,ISNULL(OpportunityBizDocs.monAmountPaid,0) monAmountPaid
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1 THEN ',0 AS numOppItemID' ELSE ',ISNULL(OpportunityItems.numoppitemtCode,0) AS numOppItemID' END),'
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1
														THEN ',0 AS numRemainingQty'
														ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OBIInner.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs OBInner
																																																					INNER JOIN
																																																						OpportunityBizDocItems OBIInner
																																																					ON
																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																					WHERE
																																																						OBInner.numOppId = OpportunityMaster.numOppID
																																																						AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
					 																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
													END)
													,','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs,
													(CASE WHEN EXISTS (SELECT OIInner.numOppItemtcode FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND ISNULL(OIInner.bitRequiredWarehouseCorrection,0)=1) THEN 1 ELSE 0 END) bitRequiredWarehouseCorrection',
													(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN ',ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)) dtShipByDate' ELSE ',CAST(OpportunityMaster.dtReleaseDate AS DATE) AS dtShipByDate' END))

	DECLARE @vcFrom NVARCHAR(MAX) = CONCAT(' FROM
												OpportunityMaster
											INNER JOIN
												DivisionMaster
											ON
												OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
											INNER JOIN
												CompanyInfo
											ON
												DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
											INNER JOIN
												OpportunityItems
											ON
												OpportunityMaster.numOppId = OpportunityItems.numOppId
											LEFT JOIN 
												MassSalesFulfillmentBatchOrders
											ON
												MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
												AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
												AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
											LEFT JOIN
												WareHouseItems
											ON
												OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
											LEFT JOIN
												WareHouses
											ON
												WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
											LEFT JOIN
												WarehouseLocation
											ON
												WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
											INNER JOIN
												Item
											ON
												OpportunityItems.numItemCode = Item.numItemCode
											',(CASE WHEN @numViewID=6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
												OpportunityBizDocs
											ON
												OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
												AND OpportunityBizDocs.numBizDocID= ',(CASE 
																						WHEN @numViewID=4 THEN 287 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																						ELSE 0
																					END),'
												AND @numViewID IN (4,6) 
											LEFT JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
												AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID')

	DECLARE @vcFrom1 NVARCHAR(MAX) = ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= @numDefaultSalesShippingDoc
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WareHouses
									ON
										WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
										AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)'

	DECLARE @vcWhere VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityItems.bitMappingRequired,0) = 0 '
												,(CASE 
													WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
													ELSE ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
												END)
												,(CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END)
												,(CASE 
													WHEN (@numViewID = 1 OR (@numViewID = 2 AND @tintPackingViewMode <> 1)) AND ISNULL(@bitRemoveBO,0) = 1
													THEN ' AND 1 = (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																				END)'
														ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcOrderStauts,'')) > 0 
													THEN (CASE 
															WHEN ISNULL(@bitIncludeSearch,0) = 0 
															THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
															ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
														END)
													ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcFilterValue,'')) > 0
													THEN
														CASE 
															WHEN @tintFilterBy=1 --Item Classification
															THEN ' AND 1 = (CASE 
																				WHEN ISNULL(@bitIncludeSearch,0) = 0 
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			END)'
															ELSE ''
														END
													ELSE ''
												END)
												,(CASE
													WHEN @numViewID = 1 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
													WHEN @numViewID = 2 THEN ' AND 1 = (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0) AND OIInner.numShipFromWarehouse=@numWarehouseID AND OIInner.ItemReleaseDate=ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OBIInner.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs OBInner
																																																															INNER JOIN
																																																																OpportunityBizDocItems OBIInner
																																																															ON
																																																																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																															WHERE
																																																																OBInner.numOppId = OpportunityMaster.numOppID
																																																																AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)'
													WHEN @numViewID = 3
													THEN ' AND 1 = (CASE 
																		WHEN (CASE 
																				WHEN ISNULL(@tintInvoicingType,0)=1 
																				THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																				ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																			END) - ISNULL((SELECT 
																								SUM(OpportunityBizDocItems.numUnitHour)
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityBizDocItems
																							ON
																								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																							WHERE
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																								AND OpportunityBizDocs.numBizDocId=287
																								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																		THEN 1
																		ELSE 0 
																	END)'
												END)
												,(CASE
															WHEN @numViewID = 1 THEN 'AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN ' AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN ' AND 1=1'
															WHEN @numViewID = 3 THEN ' AND 1=1'
															WHEN @numViewID = 4 THEN ' AND 1=1'
															WHEN @numViewID = 5 THEN ' AND 1=1'
															WHEN @numViewID = 6 THEN ' AND 1=1'
															ELSE ' AND 1=0'
													END)
												,(CASE WHEN ISNULL(@numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 OR @numViewID=6 THEN ' AND 1= (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
													THEN 
														' AND 1 = (CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END)'
															ELSE ''
														END)
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	DECLARE @vcWhere1 VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityMaster.tintshipped,0) = 0
												AND ISNULL(OpportunityItems.bitMappingRequired,0) = 0
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)
												AND 1 = (CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
															THEN 1 
															ELSE 0 
														END)
												AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
												AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)'
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	SET @vcWhere = CONCAT(@vcWhere,' ',@vcCustomSearchValue)
	SET @vcWhere1 = CONCAT(@vcWhere1,' ',@vcCustomSearchValue)
	
	DECLARE @vcGroupBy NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(' GROUP BY OpportunityMaster.numDomainID,OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,OpportunityMaster.numShippingService
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtReleaseDate
																	,OpportunityMaster.tintSource
																	,OpportunityMaster.tintSourceType
																	,OpportunityMaster.dtExpectedDate',(CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 1 THEN ',OpportunityItems.ItemReleaseDate' ELSE '' END))
											ELSE '' 
										END)

	DECLARE @vcOrderBy NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
						WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
						WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
						WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
						ELSE 'OpportunityMaster.bintCreatedDate'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcOrderBy1 NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP.vcCompanyNameOrig' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP.charItemTypeOrig=''P''  THEN ''Inventory Item''
															WHEN TEMP.charItemTypeOrig=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP.charItemTypeOrig=''S'' THEN ''Service''
															WHEN TEMP.charItemTypeOrig=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'TEMP.numBarCodeIdOrig'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(TEMP.numItemClassificationOrig)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP.numItemGroupOrig),'''')'
						WHEN 'Item.vcSKU' THEN 'TEMP.vcSKUOrig'
						WHEN 'OpportunityItems.vcItemName' THEN 'TEMP.vcItemNameOrig'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(TEMP.numStatusOrig)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP.vcPoppNameOrig'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(TEMP.numAssignedToOrig)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(TEMP.numRecOwnerOrig)'
						WHEN 'WareHouseItems.vcLocation' THEN 'TEMP.vcLocationOrig'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP.monAmountPaidOrig'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(TEMP.vcBizDocID) DESC, TEMP.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP.numQtyPickedOrig'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP.numQtyShippedOrig'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL(OpportunityItems.numQtyPickedOrig,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP.numUnitHourOrig'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP.dtExpectedDateOrig'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP.ItemReleaseDateOrig'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.numAge' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TEMP.monAmountPaidOrig,0) >= ISNULL(TEMP.monDealAmountOrig,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'TEMP.dtItemReceivedDate'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						ELSE 'TEMP.bintCreatedDateOrig'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcText NVARCHAR(MAX)

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='bitPaidInFull') OR @vcSortColumn='OpportunityMaster.bitPaidInFull'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='numQtyPacked') OR @vcSortColumn='OpportunityItems.numQtyPacked'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
							FROM 
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE 
								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
								AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
						) TempPacked'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcListItemType VARCHAR(10)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                               
	DECLARE @bitCustomField BIT      
	DECLARE @bitIsNumeric BIT
	DECLARE @vcGroupByInclude NVARCHAR(MAX) = ''
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)


	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = (CASE WHEN ISNULL(bitCustomField,0) = 1 THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END),
			@vcLookBackTableName = vcLookBackTableName,
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0),
			@bitIsNumeric=ISNULL(bitIsNumeric,0),
			@vcListItemType=ISNULL(vcListItemType,'')
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j

		IF @vcDbColumnName NOT IN ('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus','vcPaymentStatus','numShipRate')
		BEGIN
			IF ISNULL(@bitCustomField,0) = 0
			BEGIN
				IF (@vcLookBackTableName = 'Item' OR @vcLookBackTableName = 'WareHouseItems' OR @vcLookBackTableName = 'OpportunityItems' OR @vcLookBackTableName='Warehouses') AND ISNULL(@bitGroupByOrder,0)=1
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
					SET @vcGroupByInclude = CONCAT(@vcGroupByInclude,',','TEMP.',@vcDbColumnName)
				END
				ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN 'ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))' ELSE 'CAST(OpportunityMaster.dtReleaseDate AS DATE)' END),' [',@vcDbColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vcInclusionDetails'
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',dbo.GetOrderAssemblyKitInclusionDetails(OpportunityMaster.numOppID,OpportunityItems.numoppitemtCode,OpportunityItems.numUnitHour,',@tintCommitAllocation,',0) [',@vcDbColumnName,']')
				END
				ELSE
				BEGIN
					IF @vcDbColumnName = 'numAge'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtItemReceivedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(Item.dtItemReceivedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bitPaidInFull'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtExpectedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bintCreatedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numQtyPacked'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcInvoiced'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'charItemType'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',@vcDbColumnName,']') 
					END
					ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numStatus'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.GetListIemName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'tintSource'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcWareHouse'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE ISNULL(Warehouses.vcWarehouse,''-'') END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDOpp.vcData,SSOpp.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
					END
					ELSE IF @vcDbColumnName = 'intShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDDiv.vcData,SSDiv.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')

						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')
					END
					ELSE IF @vcAssociatedControlType = 'SelectBox'
					BEGIN
						IF @vcListItemType = 'LI'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',L',CONVERT(VARCHAR(3),@j),'.vcData',' [',@vcDbColumnName,']')

							SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
							SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
						END
						ELSE IF @vcListItemType = 'IG'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',@vcDbColumnName,']') 
						END
						ELSE IF @vcListItemType = 'U'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetContactName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
						END
						ELSE
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
						END
					END
					ELSE IF @vcAssociatedControlType = 'DateField'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-@ClientTimeZoneOffset,',',@vcLookBackTableName,'.',@vcDbColumnName,'),',@numDomainId,') [', @vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextBox'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextArea'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'Label'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
				END
			END
			ELSE IF @bitCustomField = 1
			BEGIN
				IF @Grp_id = 5
				BEGIN
					IF ISNULL(@bitGroupByOrder,0) = 1
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',''''',' [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',@vcDbColumnName,']')
					END
				END
				ELSE
				BEGIN
					IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW',@numFieldId
										,' ON CFW' , @numFieldId , '.Fld_Id='
										,@numFieldId
										, ' and CFW' , @numFieldId
										, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
					END   
					ELSE IF @vcAssociatedControlType = 'CheckBox' 
					BEGIN
						SET @vcSelect =CONCAT( @vcSelect
							, ',case when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=0 then 0 when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=1 then 1 end   ['
							,  @vcDbColumnName
							, ']')
							
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' ON CFW',@numFieldId,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'DateField' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect
							, ',dbo.FormatedDateFromDate(CFW'
							, @numFieldId
							, '.Fld_Value,'
							, @numDomainId
							, ')  [',@vcDbColumnName ,']' )  
					                  
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW', @numFieldId, '.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
										WHEN @bitGroupByOrder = 1 
										THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
										ELSE '' 
									END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'SelectBox' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					        
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW',@numFieldId ,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',L',@numFieldId,'.vcData')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)

						SET @vcText = CONCAT(' left Join ListDetails L'
							, @numFieldId
							, ' on L'
							, @numFieldId
							, '.numListItemID=CFW'
							, @numFieldId
							, '.Fld_Value')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)            
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',OpportunityMaster.numOppID)'),' [',@vcDbColumnName,']')
					END 
				END
			END
		END

		SET @j = @j + 1
	END

	PRINT CAST(@vcSelect AS NTEXT)
	PRINT CAST(@vcFrom AS NTEXT)
	PRINT CAST(@vcWhere AS NTEXT)
	PRINT CAST(@vcGroupBy AS NTEXT)
	PRINT CAST(@vcOrderBy AS NTEXT)

	DECLARE @vcFinal NVARCHAR(MAX) = ''

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSelect = CONCAT(@vcSelect,',',(CASE @vcSortColumn
													WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS vcCompanyNameOrig' 
													WHEN 'Item.charItemType' THEN 'Item.charItemType AS charItemTypeOrig'
													WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS numBarCodeIdOrig'
													WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS numItemClassificationOrig'
													WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS numItemGroupOrig'
													WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS vcSKUOrig'
													WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS vcItemNameOrig'
													WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS numStatusOrig '
													WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS vcPoppNameOrig'
													WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS numAssignedToOrig'
													WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS numRecOwnerOrig'
													WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS vcLocationOrig'
													WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig'
													WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped AS numQtyShippedOrig'
													WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig,OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig'
													WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig'
													WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS ItemReleaseDateOrig'
													WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig, OpportunityMaster.monDealAmount AS monDealAmountOrig'
													WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS dtItemReceivedDateOrig'
													WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
													ELSE 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
												END))

		DECLARE @vcGroupBy1 NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(' GROUP BY TEMP.numOppId
																	,TEMP.numOppItemID
																	,TEMP.numRemainingQty
																	,TEMP.dtAnticipatedDelivery
																	,TEMP.dtShipByDate
																	,TEMP.vcShipStatus
																	,TEMP.vcPaymentStatus
																	,TEMP.numShipRate
																	,TEMP.vcLinkingBizDocs
																	,TEMP.bitRequiredWarehouseCorrection
																	,TEMP.numDivisionID
																	,TEMP.numTerID
																	,TEMP.numOppShipVia
																	,TEMP.numOppShipService
																	,TEMP.numPreferredShipVia
																	,TEMP.numPreferredShipService
																	,TEMP.numOppBizDocID
																	,TEMP.monAmountToPay
																	,TEMP.monAmountPaid
																	,TEMP.dtExpectedDateOrig
																	,TEMP.vcBizDocID
																	,TEMP.numAge
																	,TEMP.bintCreatedDateOrig
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='CompanyInfo' AND vcOrigDbColumnName='vcCompanyName') THEN ',TEMP.vcCompanyName' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='vcPoppName') THEN ',TEMP.vcPoppName' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numAssignedTo') THEN ',TEMP.numAssignedTo' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numRecOwner') THEN ',TEMP.numRecOwner' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numStatus') THEN ',TEMP.numStatus' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bintCreatedDate') THEN ',TEMP.bintCreatedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='txtComments') THEN ',TEMP.txtComments' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='vcBizDocID') THEN ',TEMP.vcBizDocID' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany') THEN ',TEMP.intUsedShippingCompany' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany') THEN ',TEMP.intShippingCompany' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='dtCreatedDate') THEN ',TEMP.dtCreatedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='dtExpectedDate') THEN ',TEMP.dtExpectedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='Item' AND vcOrigDbColumnName='dtItemReceivedDate') THEN ',TEMP.dtItemReceivedDateOrig' ELSE '' END),@vcGroupByInclude)
											ELSE '' 
										END)
		SET @vcFinal= CONCAT('SELECT COUNT(*) OVER() AS TotalRecords,* FROM (',@vcSelect,@vcFrom,@vcWhere,' UNION ',@vcSelect,@vcFrom1,@vcWhere1,') TEMP',@vcGroupBy1,@vcOrderBy1,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END
	ELSE
	BEGIN
		SET @vcFinal = CONCAT(@vcSelect,',COUNT(*) OVER() AS TotalRecords',@vcFrom,@vcWhere,@vcGroupBy,@vcOrderBy,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END

	EXEC sp_executesql @vcFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT,@numBatchID NUMERIC(18,0),@tintPrintBizDocViewMode TINYINT,@tintPendingCloseFilter TINYINT, @bitIncludeSearch BIT, @bitEnablePickListMapping BIT, @bitEnableFulfillmentBizDocMapping BIT,@numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset,@numBatchID,@tintPrintBizDocViewMode,@tintPendingCloseFilter,@bitIncludeSearch,@bitEnablePickListMapping, @bitEnableFulfillmentBizDocMapping,@numPageIndex;

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                      
AS
SET NOCOUNT ON
SET XACT_ABORT ON

BEGIN TRY
	IF ISNULL(@numOppID,0) = 0 AND ISNULL(@numDomainID,0) = 209 AND ISNULL(@vcOppRefOrderNo,'') <> '' AND ISNULL(@tintSourceType,0) = 4
	BEGIN
		IF EXISTS (SELECT numOppID FROM OpportunityMaster WHERE numDomainID=209 AND tintOppType=1 AND vcOppRefOrderNo=@vcOppRefOrderNo)
		BEGIN
			RAISERROR('CUSTOMERPO_ALREADY_EXISTS',16,1)
		END
	END
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
	END

BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)

	

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty,bitMappingRequired,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID
				,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty,ISNULL(X.bitMappingRequired,0),X.vcSKU,X.vcASIN
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,bitMappingRequired BIT,vcASIN VARCHAR(100)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate
				,vcChildKitSelectedItems,numWOQty,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount
				,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty,X.vcSKU,X.vcASIN
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,vcSKU VARCHAR(100),vcASIN VARCHAR(100)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
		,OI.vcSKU = (CASE 
						WHEN EXISTS (SELECT 
											OKI.numOppChildItemID 
										FROM 
											OpportunityKitItems OKI 
										INNER JOIN ItemDetails ID ON ID.numItemKitID=OI.numItemCode AND OKI.numChildItemID=ID.numChildItemID 
										INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode 
										WHERE OKI.numOppItemID=OI.numoppitemtCode AND ISNULL(ID.bitUseInDynamicSKU,0) = 1) 
							THEN 
							STUFF((
									SELECT '-' + 
										STUFF((
											SELECT ',' + I.vcItemName
											FROM 
												OpportunityKitChildItems OKCI
											INNER JOIN 
												Item I
											ON
												OKCI.numItemID = I.numItemCode
											WHERE OKCI.numOppChildItemID=OKI.numOppChildItemID
											for xml path('')
										),1,1,'') 
									FROM 
										OpportunityKitItems OKI 
									INNER JOIN ItemDetails ID ON ID.numItemKitID=OI.numItemCode AND OKI.numChildItemID=ID.numChildItemID 
									INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode 
									WHERE OKI.numOppItemID=OI.numoppitemtCode AND ISNULL(ID.bitUseInDynamicSKU,0) = 1
									ORDER BY
										ISNULL(ID.sintOrder,0) 
									FOR XML PATH('')),1,1,'') 

							ELSE OI.vcSKU 
						END)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1
	BEGIN
		EXEC USP_OpportunityMaster_AddVendor @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderListInEcommerce')
DROP PROCEDURE USP_GetOrderListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @tintShipped INT=0,
 @tintOppStatus INT=0,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0
DECLARE @ClientTimeZoneOffset Int
SET @ClientTimeZoneOffset=-330
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	SELECT distinct OM.numOppID,OM.vcPOppName,(SELECT SUBSTRING((SELECT '$^$' + CAST(numOppBizDocsId AS VARCHAR(18)) +'#^#'+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numBizDOcId=287 FOR XML PATH('')),4,200000)) AS vcBizDoc,

		(SELECT SUBSTRING((SELECT '$^$' + CAST(vcTrackingNo AS VARCHAR(18)) +'#^#'+ vcTrackingUrl
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID FOR XML PATH('')),4,200000)) AS vcShipping,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate) as monDealAmount,
		(isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS monAmountPaid,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate)
		 - (isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS BalanceDue,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numShipVia>0)) AS vcShippingMethod,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID)) AS vcMasterShippingMethod,
		(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID) AS numOppMasterShipVia,
		(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShipVia,
		(SELECT TOP 1 vcTrackingNo FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS vcTrackingNo,
		(SELECT TOP 1 numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = (SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0)) AS numShippingReportId,
		(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShippingBizDocId,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtDeliveryDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0),OM.numDomainId) AS DeliveryDate,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId) AS BillingDate,

		CASE WHEN (SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numOppId=OM.numOppID)=1 THEN
		(CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),(SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc)),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId)
               END) 
		ELSE 'Multiple' END AS DueDate,
		[dbo].[FormatedDateFromDate](OM.dtReleaseDate,OM.numDomainId) AS ReleaseDate,
		[dbo].[FormatedDateFromDate](OM.bintCreatedDate,OM.numDomainId) AS CreatedDate,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=OM.numStatus) AS vcOrderStatus,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		CASE WHEN 
		(select TOP 1 ISNULL(numPaymentMethod,0) from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID)))=0
		THEN
		(select TOP 1 vcData from ListDetails WHERE numListItemID IN (
		select ISNULL(numDefaultPaymentMethod,0) from DivisionMaster where numDivisionId=@numDivisionID)) 
		ELSE  (select TOP 1 vcData from ListDetails WHERE numListItemID IN (
			  select numPaymentMethod from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID))))
		END
		AS PaymentMethod,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus],
		CASE 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,getutcdate())) 
then CONCAT('<b><font color="#FF0000" style="font-size:14px">Today','</font></b>') 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute,-@ClientTimeZoneOffset,getutcdate()))) 
THEN CONCAT('<b><font color=#ED8F11 style="font-size:14px">Tommorow','</font></b>') 
WHEN OM.bintCreatedDate<DateAdd(day,7,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
AND OM.bintCreatedDate>DateAdd(day,1,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
then CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>') ELSE 

CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>')
END AS FormattedCreatedDate,
vcCustomerPO#




		FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

		SELECT DISTINCT COUNT(OM.numOppId) FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0


		----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid DECIMAL(20,5),    
-- TotalAmt DECIMAL(20,5),
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetStateCountryIDs')
DROP PROCEDURE dbo.USP_GetStateCountryIDs
GO

CREATE PROCEDURE [dbo].[USP_GetStateCountryIDs]
    @vcState VARCHAR(50),
	--@vcCountryCode VARCHAR(50),
	@vcCountryName Varchar(50),
    @numDomainID NUMERIC    

AS 
BEGIN
		--DECLARE @vcCountryName Varchar(50)
		DECLARE @numCountryID NUMERIC	
		DECLARE @numStateID NUMERIC

		--SET @vcCountryName = (SELECT TOP 1 vcCountryName from ShippingCountryMaster where vcCountryCode = @vcCountryCode)

		SET @numCountryID = (SELECT TOP 1 numListItemID FROM ListDetails WHERE numListID=40 AND (constFlag=1 OR numDomainID=@numDomainID) AND vcData = @vcCountryName)

		IF ISNULL(@numCountryID,0) = 0 AND ISNULL(@vcCountryName,'') <> ''
		BEGIN

			INSERT INTO ListDetails
				(
					numListID
					,vcData
					,numCreatedBY
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,bitDelete
					,numDomainId
					,constFlag
					,sintOrder
				)
				VALUES
				(
					40
					,@vcCountryName
					,1
					,GETDATE()
					,1
					,GETDATE()
					,0
					,@numDomainID
					,0
					,0
				)

				SET @numCountryID = SCOPE_IDENTITY()			

		END

		IF ISNULL(@numCountryID,0) > 0
		BEGIN
			SET @numStateID = (SELECT TOP 1 numStateID FROM [State] WHERE numDomainID=@numDomainID AND numCountryID=@numCountryID AND (vcState = @vcState OR vcAbbreviations = @vcState))

			IF ISNULL(@numStateID,0) = 0 AND ISNULL(@vcState,'') <> ''
			BEGIN
				
				INSERT INTO [STATE]
				(
					 numCountryID
					,vcState
					,numCreatedBy
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,numDomainID
					,constFlag
					,vcAbbreviations
					,numShippingZone

				)
				VALUES
				(
					@numCountryID
					,@vcState
					,1
					,GETDATE()
					,1
					,GETDATE()
					,@numDomainID
					,0
					,NULL
					,NULL
				)

				SET @numStateID = SCOPE_IDENTITY()		
			END
		END

		

	SELECT @numCountryID AS 'CountryId', @numStateID AS 'StateId'
END
GO


