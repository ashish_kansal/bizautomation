/******************************************************************
Project: Release 10.2 Date: 03.SEP.2018
Comments: ALTER SCRIPTS
*******************************************************************/


UPDATE DynamicFormMaster SET vcFormName='Organizations & Contacts' WHERE numFormId=1 -- OLD VALUE: Advanced Search Page Configuration
UPDATE DynamicFormMaster SET vcFormName='Opportunities & Orders (with items)' WHERE numFormId=15 -- OLD VALUE: Opportunities & Orders / Deals Search
UPDATE DynamicFormMaster SET vcFormName='Items' WHERE numFormId=29 -- OLD VALUE: Item Search
UPDATE DynamicFormMaster SET vcFormName='Cases' WHERE numFormId=17 -- OLD VALUE: Cases Search
UPDATE DynamicFormMaster SET vcFormName='Projects' WHERE numFormId=18 -- OLD VALUE: Projects Search
UPDATE DynamicFormMaster SET vcFormName='Financial Transactions' WHERE numFormId=59 -- OLD VALUE: Advanced Financial Transaction Search

UPDATE DycFormConfigurationDetails SET intColumnNum=1 WHERE numFormId IN (1,15,29,17,18,59)

DELETE FROM DycFormField_Mapping WHERE numFormID=59 AND numFieldID IN (6,14)

DELETE FROM DycFormConfigurationDetails WHERE numFormId=59 AND numFieldID IN (6,14)

ALTER TABLE SavedSearch ADD vcSearchConditionJson VARCHAR(MAX)
ALTER TABLE SavedSearch ADD numSharedFromSearchID NUMERIC(18,0)
ALTER TABLE SavedSearch ADD vcDisplayColumns VARCHAR(MAX)
ALTER TABLE SavedSearch ALTER COLUMN vcSearchQuery VARCHAR(MAX) NULL
ALTER TABLE SavedSearch ALTER COLUMN intSlidingDays INT NULL
ALTER TABLE AdvSerViewConf ADD numUserCntID NUMERIC(18,0)

SET IDENTITY_INSERT DycFieldMaster ON

INSERT INTO DycFieldMaster
(
	numFieldId,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	33,1,'Organization Modified Date','bintModifiedDate','bintModifiedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
),
(
	34,1,'Organization Visited Date','bintVisitedDate','bintVisitedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
)

SET IDENTITY_INSERT DycFieldMaster OFF

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	1,33,1,0,0,'Organization Modified Date','DateField',0,0,0,1
),
(
	1,34,1,0,0,'Organization Visited Date','DateField',0,0,0,1
)

------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	1,'Exclude Organizations with Sales Orders Created Date','dtExcludeOrgWithSalesCreatedDate','dtExcludeOrgWithSalesCreatedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	1,@numFieldID,1,0,0,'Exclude Organizations with Sales Orders Created Date','DateField',0,0,0,0
)

---------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	1,'Exclude Organizations with no sales orders','bitExcludeOrgWithNoSalesOrders','bitExcludeOrgWithNoSalesOrders','DivisionMaster','Y','R','CheckBox','',0,0,0,0,0,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	1,@numFieldID,1,0,0,'Exclude Organizations with no sales orders','CheckBox',0,0,0,0
)

-------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,119,15,0,0,'Order Created Date','DateField',0,0,0,1
)

------------------------------------------------------------

SET IDENTITY_INSERT DycFieldMaster ON

INSERT INTO DycFieldMaster
(
	numFieldId,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	124,3,'Order Modified Date','bintModifiedDate','bintModifiedDate','OpportunityMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
)

SET IDENTITY_INSERT DycFieldMaster OFF

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,124,15,0,0,'Order Modified Date','DateField',0,0,0,1
)

---------------------------------------------------------------

UPDATE DycFieldMaster SET vcListItemType='OT' WHERE numFieldId=577

---------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,577,15,0,0,'Opp/Order Type','SelectBox',0,0,0,1
)

------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	7,143,17,0,0,'Case Created Date','DateField',0,0,0,1
),
(
	7,488,17,0,0,'Case Modified Date','DateField',0,0,0,1
)

UPDATE DycFormField_Mapping SET vcAssociatedControlType='DateField' WHERE numFieldID = 751 AND numFormID = 1


UPDATE DycFormField_Mapping SET vcAssociatedControlType='DateField' WHERE numFormID=15 AND numFieldID=108


UPDATE DycFormField_Mapping SET vcAssociatedControlType='CheckBox' WHERE numFormID=15 AND numFieldID=313

UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFormID=18 AND numFieldId=152

UPDATE DycFieldMaster SET vcListItemType='IC' WHERE numFieldId=311

DELETE FROM DycFormField_Mapping WHERE numFormID=29 AND numFieldID=349