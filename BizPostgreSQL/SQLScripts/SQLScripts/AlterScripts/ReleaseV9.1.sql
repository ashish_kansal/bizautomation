/******************************************************************
Project: Release 9.1 Date: 05.FEBRUARY.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************   PRIYA  *****************************/

---- Insert record for Email Menu 

Insert into PageNavigationDTL
(
numPageNavID,
numModuleID,
numParentID,
vcPageNavName,
vcNavURL,
vcImageURL,
bitVisible,
numTabID,
intSortOrder,
vcAddURL,
bitAddIsPopUp
)

values
(
262,
33,
0,
'Email',
NULL,
NULL,
1,
44,
NULL,
NULL,
NULL
)

--- Update Drip Campaigns menu as Parent Email menu
UPDATE PageNavigationDTL 
SET numParentID = 262, numTabID = 44, numModuleID = 33 -- numParentID = 16, numTabID = 3   1 row
WHERE vcPageNavName = 'Drip Campaigns';

------ Update TabId for Drip Campaigns sub menu with Email TabId

UPDATE TreeNavigationAuthorization
set numTabID = 44 -- numTabID = 3, 506 rows
where numPageNavID = 168



--------------------------------

--- Shift Drip Campaign from Email Menu to Marketing Menu and rename it, remove Surveys/Questionaires sub menu from Marketing tab

UPDATE PageNavigationDTL 
SET numParentID = 15, numTabID = 3, numModuleID = 6, vcPageNavName = 'Automated Follow-ups' --numParentID = 262, numTabID = 44, numModuleID = 33 --------  -- numParentID = 16, numTabID = 3   1 row
WHERE vcPageNavName = 'Drip Campaigns';


UPDATE TreeNavigationAuthorization
set numTabID = 3 -- numTabID = 44  506 rows----------- numTabID = 3, 506 rows
where numPageNavID = 168


UPDATE PageNavigationDTL 
SET bitVisible = 0--bitVisible = 1 
WHERE vcPageNavName = 'Surveys/Questionaires';

-------------------Change Display name of Drip Campaign-----------------------
update DycFieldMaster
set vcFieldName = 'Follow-up Campaign' , vcPropertyName = 'FollowupCampaign'
where vcFieldName = 'Drip Campaign'
-------Delete drip campaign history...

delete from DycFieldMaster
 where numFieldId = 80
 delete from DycFormField_Mapping
 where numFieldID = 80  -- 4 rows

 delete from DycFormConfigurationDetails
 where numFieldId = 80  -- 45 rows

 BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,
	bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,bitInlineEdit,vcGroup
)
VALUES
(
	2,'Last Follow-up','vcLastFollowup','vcLastFollowup','LastFollowup','AdditionalContactsInformation','V','R','Label','',0,1,0,0,0,1,
	0,1,1,0,0,0,'Contact Fields'
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	2,@numFieldID,10,0,0,'Last Follow-up','Label','LastFollowup',1,0,0,1,0,1,1,1,0
)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,
	bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,bitInlineEdit,vcGroup
)
VALUES
(
	2,'Next Follow-up','vcNextFollowup','vcNextFollowup','NextFollowup','AdditionalContactsInformation','V','R','Label','',0,1,0,0,0,1,
	0,1,1,0,0,0,'Contact Fields'
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	2,@numFieldID,10,0,0,'Next Follow-up','Label','NextFollowup',1,0,0,1,0,1,1,1,0

)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


Update DycFieldMaster SET vcFieldName = 'Follow-up campaign' where numFieldId = '74' and vcFieldName = 'Drip Campaign'
Update DycFormField_Mapping SET vcFieldName = 'Follow-up campaign' where numFieldID = '74' and vcFieldName = 'Drip Campaign'


/*************************   SANDEEP  *****************************/

ALTER TABLE Domain ADD bit3PL BIT DEFAULT 0


ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN [monTotAmount] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN [monTotAmtBefDiscount] DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [numPClosingPercent] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityMaster] DROP CONSTRAINT [DF_OpportunityMaster_monPAmount]
GO
ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monPAmount] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityMaster] ADD  CONSTRAINT [DF_OpportunityMaster_monPAmount]  DEFAULT ((0)) FOR [monPAmount]
GO
ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monDealAmount] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monShipCost] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monLandedCostTotal] DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monDealAmountWith4Decimals] DECIMAL(20,5)

ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalExpense] DECIMAL(20,5)
ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalIncome] DECIMAL(20,5)
ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalGrossProfit] DECIMAL(20,5)
ALTER TABLE [dbo].[DepositeDetails] ALTER COLUMN [monAmountPaid] DECIMAL(20,5)

ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monPaymentAmount DECIMAL(20,5)
ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monAppliedAmount DECIMAL(20,5)
ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monRefundAmount DECIMAL(20,5)

ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monBeginBalance DECIMAL(20,5)
ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monEndBalance DECIMAL(20,5)
ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monServiceChargeAmount DECIMAL(20,5)
ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monInterestEarnedAmount DECIMAL(20,5)
ALTER TABLE [dbo].[BankStatementHeader] ALTER COLUMN monAvailableBalance DECIMAL(20,5)
ALTER TABLE [dbo].[BankStatementHeader] ALTER COLUMN monLedgerBalance DECIMAL(20,5)

ALTER TABLE [dbo].[BankStatementTransactions] ALTER COLUMN monAmount DECIMAL(20,5)

ALTER TABLE [dbo].[BillDetails] ALTER COLUMN monAmount DECIMAL(20,5)
ALTER TABLE [dbo].[BillHeader] ALTER COLUMN monAmountDue DECIMAL(20,5)

ALTER TABLE [dbo].[BillHeader] DROP CONSTRAINT [DF_BillHeader_monAmtPaid]
GO
ALTER TABLE [dbo].[BillHeader] ALTER COLUMN monAmtPaid DECIMAL(20,5)

ALTER TABLE [dbo].[BillHeader] ADD  CONSTRAINT [DF_BillHeader_monAmtPaid]  DEFAULT ((0)) FOR [monAmtPaid]
GO
ALTER TABLE [dbo].[BillPaymentDetails] ALTER COLUMN monAmount DECIMAL(20,5)

ALTER TABLE [dbo].[CampaignMaster] ALTER COLUMN monCampaignCost DECIMAL(20,5)
ALTER TABLE [dbo].[CartItems] ALTER COLUMN monPrice DECIMAL(20,5)
ALTER TABLE [dbo].[CartItems] ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)
ALTER TABLE [dbo].[CartItems] ALTER COLUMN decShippingCharge DECIMAL(20,5)
ALTER TABLE [dbo].[CartItems] ALTER COLUMN monTotAmount DECIMAL(20,5)

ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN numOriginalOpeningBal DECIMAL(20,5)
ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN numOpeningBal DECIMAL(20,5)
ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monEndingOpeningBal DECIMAL(20,5)
ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monEndingBal DECIMAL(20,5)
ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monDepreciationCost DECIMAL(20,5)

ALTER TABLE [dbo].[CheckDetails] ALTER COLUMN monAmount DECIMAL(20,5)
DROP INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader]
GO
ALTER TABLE [dbo].[CheckHeader] ALTER COLUMN monAmount DECIMAL(20,5)
CREATE NONCLUSTERED INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader]
(
	[numReferenceID] ASC,
	[tintReferenceType] ASC,
	[numCheckHeaderID] ASC
)
INCLUDE ( 	[vcMemo],
	[monAmount],
	[numCheckNo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80)
GO

ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monDepositAmount DECIMAL(20,5)
ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monAppliedAmount DECIMAL(20,5)
ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monRefundAmount DECIMAL(20,5)
ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monAppliedAmountWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monDepositAmountWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monRefundAmountWith4Decimals DECIMAL(20,5)

ALTER TABLE [dbo].[General_Journal_Header] ALTER COLUMN numAmount DECIMAL(20,5)

ALTER TABLE [dbo].[Item] ALTER COLUMN monListPrice DECIMAL(20,5)
ALTER TABLE [dbo].[Item] ALTER COLUMN monAverageCost DECIMAL(20,5)
ALTER TABLE [dbo].[Item] ALTER COLUMN monCampaignLabourCost DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monItemPrice DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monAmount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monTax DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monTotAmount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocItems] DROP CONSTRAINT [DF_OpportunityBizDocItems_monEmbeddedCostPerItem]
GO
ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monEmbeddedCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocItems] ADD  CONSTRAINT [DF_OpportunityBizDocItems_monEmbeddedCostPerItem]  DEFAULT ((0)) FOR [monEmbeddedCost]
GO
ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monShipCost DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityBizDocs] DROP CONSTRAINT [DF_OpportunityBizDocs_monAmountPaid]
GO
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monAmountPaid DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocs] ADD  CONSTRAINT [DF_OpportunityBizDocs_monAmountPaid]  DEFAULT ((0)) FOR [monAmountPaid]
GO
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monShipCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monDealAmount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocs] DROP CONSTRAINT [DF_OpportunityBizDocs_monEmbeddedCostPerItem]
GO
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monEmbeddedCostPerItem DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocs] ADD  CONSTRAINT [DF_OpportunityBizDocs_monEmbeddedCostPerItem]  DEFAULT ((0)) FOR [monEmbeddedCostPerItem]
GO
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monCreditAmount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monDealAmountWith4Decimals DECIMAL(20,5)

ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monAuthorizedAmt DECIMAL(20,5)
ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monCapturedAmt DECIMAL(20,5)
ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monRefundAmt DECIMAL(20,5)
ALTER TABLE [dbo].[Vendor] ALTER COLUMN monCost DECIMAL(20,5)

ALTER TABLE [dbo].[TimeAndExpense] ALTER COLUMN monAmount DECIMAL(20,5)
ALTER TABLE [dbo].[TimeExpAddAmount] ALTER COLUMN monAmount DECIMAL(20,5)

ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDays DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDays DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDays DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDays DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysOverDue DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysOverDue DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysOverDue DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysOverDue DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysPaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysPaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysPaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysPaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysOverDuePaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysOverDuePaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysOverDuePaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysOverDuePaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN monUnAppliedAmount DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numTotal DECIMAL(20,5)

ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN DealAmount DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN AmountPaid DECIMAL(20,5)
ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN monUnAppliedAmount DECIMAL(20,5)

ALTER TABLE [dbo].[WareHouseItems] ALTER COLUMN monWListPrice DECIMAL(20,5)
ALTER TABLE [dbo].[WareHouseItems_Tracking] ALTER COLUMN monAverageCost DECIMAL(20,5)

ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monAmount DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocAmount DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocUsedAmount DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalTax DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalDiscount DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monAmountWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocAmountWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocUsedAmountWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalTaxWith4Decimals DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalDiscountWith4Decimals DECIMAL(20,5)

ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monTotAmount DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monVendorCost DECIMAL(20,5)
ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monAverageCost DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityItems] DROP CONSTRAINT [DF__Opportuni__monVe__781663CB]
GO
ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monVendorCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityItems] ADD  DEFAULT ((0)) FOR [monVendorCost]
GO
ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monAvgCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monLandedCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monAvgLandedCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityKitChildItems] ALTER COLUMN monAvgCost DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityKitItems] ALTER COLUMN monAvgCost DECIMAL(20,5)

ALTER TABLE [dbo].[OpportunityBizDocsDetails] ALTER COLUMN monAmount DECIMAL(20,5)
ALTER TABLE [dbo].[OpportunityBizDocsPaymentDetails] ALTER COLUMN monAmount DECIMAL(20,5)

ALTER TABLE DivisionMaster ALTER COLUMN monPCreditBalance DECIMAL(20,5)
ALTER TABLE DivisionMaster ALTER COLUMN monSCreditBalance DECIMAL(20,5)
ALTER TABLE Domain ALTER COLUMN monAmountPastDue DECIMAL(20,5)

ALTER TABLE ProjectsMaster ALTER COLUMN monTotalExpense DECIMAL(20,5)
ALTER TABLE ProjectsMaster ALTER COLUMN monTotalIncome DECIMAL(20,5)
ALTER TABLE ProjectsMaster ALTER COLUMN monTotalGrossProfit DECIMAL(20,5)

ALTER TABLE SalesTemplateItems ALTER COLUMN monPrice DECIMAL(20,5)
ALTER TABLE SalesTemplateItems ALTER COLUMN monTotAmount DECIMAL(20,5)
ALTER TABLE SalesTemplateItems ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)

ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping1OrderAmount DECIMAL(20,5)
ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping1Charge DECIMAL(20,5)
ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping2OrderAmount DECIMAL(20,5)
ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping2Charge DECIMAL(20,5)
ALTER TABLE ShippingPromotions ALTER COLUMN monFreeShippingOrderAmount DECIMAL(20,5)
ALTER TABLE ShippingReportItems ALTER COLUMN monShippingRate DECIMAL(20,5)

ALTER TABLE UserMaster ALTER COLUMN monHourlyRate DECIMAL(20,5)
ALTER TABLE UserMaster ALTER COLUMN monOverTimeRate DECIMAL(20,5)
ALTER TABLE UserMaster ALTER COLUMN monOverTimeWeeklyRate DECIMAL(20,5)
ALTER TABLE UserMaster ALTER COLUMN monNetPay DECIMAL(20,5)
ALTER TABLE UserMaster ALTER COLUMN monAmtWithheld DECIMAL(20,5)

ALTER TABLE ShippingBox ALTER COLUMN monShippingRate DECIMAL(20,5)
ALTER TABLE ShippingServiceTypes ALTER COLUMN monRate DECIMAL(20,5)

ALTER TABLE [dbo].[StagePercentageDetails] DROP CONSTRAINT [DF_StagePercentageDetails_monTimeBudget]
ALTER TABLE [dbo].[StagePercentageDetails] DROP CONSTRAINT [DF_StagePercentageDetails_monExpenseBudget]
GO
ALTER TABLE StagePercentageDetails ALTER COLUMN monExpenseBudget DECIMAL(20,5)
ALTER TABLE StagePercentageDetails ALTER COLUMN monTimeBudget DECIMAL(20,5)
ALTER TABLE [dbo].[StagePercentageDetails] ADD  CONSTRAINT [DF_StagePercentageDetails_monExpenseBudget]  DEFAULT ((0)) FOR [monExpenseBudget]
ALTER TABLE [dbo].[StagePercentageDetails] ADD  CONSTRAINT [DF_StagePercentageDetails_monTimeBudget]  DEFAULT ((0)) FOR [monTimeBudget]
GO

ALTER TABLE WorkOrder ALTER COLUMN monAverageCost DECIMAL(20,5)
ALTER TABLE WorkOrderDetails ALTER COLUMN monAverageCost DECIMAL(20,5)

SELECT TABLE_NAME,COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE DATA_TYPE='money' ORDER BY TABLE_NAME