/******************************************************************
Project: Release 11.8 Date: 22.APR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE Domain ADD bitIncludeRequisitions BIT

INSERT INTO ErrorMaster
(
	vcErrorCode,vcErrorDesc,tintApplication
)
VALUES
(
	'ERR081','Contact name is required.',3
)