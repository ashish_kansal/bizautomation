/******************************************************************
Project: Release 7.5 Date: 17.MAY.2017
Comments: STORE PROCEDURES
*******************************************************************/


--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist1')
DROP PROCEDURE usp_accountlist1
GO
CREATE PROCEDURE [dbo].[USP_AccountList1]                                                              
		@CRMType numeric,                                                              
		@numUserCntID numeric,                                                              
		@tintUserRightType tinyint,                                                              
		@tintSortOrder numeric=4,                                                                                  
		@SortChar char(1)='0',  
		@numDomainID as numeric(9)=0,                                                       
		@CurrentPage int,                                                            
		@PageSize int,        
		 @TotRecs int output,                                                           
		@columnName as Varchar(MAX),                                                            
		@columnSortOrder as Varchar(10)    ,                    
		@numProfile as numeric   ,                  
		@bitPartner as BIT,       
		@ClientTimeZoneOffset as int,
		@bitActiveInActive as bit,
		@vcRegularSearchCriteria varchar(MAX)='',
		@vcCustomSearchCriteria varchar(MAX)=''   ,
		@SearchText VARCHAR(100) = ''
AS                                                            
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
           
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END

	

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)   

	DECLARE @Nocolumns AS TINYINT = 0                

	SELECT 
		@Nocolumns =ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
	) TotalRows

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 36 AND numRelCntType=2 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		if @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				36,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,2,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=36 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC   
		END
          
		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END


	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	set @strColumns=' isnull(ADC.numContactId,0)numContactId,isnull(DM.numDivisionID,0)numDivisionID,isnull(DM.numTerID,0)numTerID,isnull(ADC.numRecOwner,0)numRecOwner,isnull(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName'   

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(20)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
    SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC            
                             
	WHILE @tintOrder>0                                                  
	BEGIN
		IF @bitCustom = 0                
		BEGIN
			DECLARE @Prefix AS VARCHAR(5)
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation' 
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster' 
				SET @Prefix = 'DM.'
	
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END
			ELSE IF @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                  
			BEGIN
				IF @vcDbColumnName = 'numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
				END
				ELSE IF @vcListItemType='LI'                                                         
				BEGIN
					IF @numListID=40 
					BEGIN
						SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'  
						                                                 
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
						END                              
						IF @vcDbColumnName='numShipCountry'
						BEGIN
							SET @WhereCondition = @WhereCondition
												+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
												+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
						END
						ELSE
						BEGIN
							SET @WhereCondition = @WhereCondition
											+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
											+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
						END
					 END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
					 END
				END                                                        
				ELSE IF @vcListItemType='S'                                                         
				BEGIN 
					
					                                                           
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END
					      						
					IF @vcDbColumnName='numShipState'
					BEGIN
						SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
							+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
					END
					ELSE IF @vcDbColumnName='numBillState'
					BEGIN
						SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
							+ ' left Join State BillState on BillState.numStateID=AD4.numState '
					END
					ELSE  
					BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
					END		                                           
				END                                                        
				ELSE IF @vcListItemType='T'                                                         
				BEGIN
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END                                                           
				  
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
				END  
				ELSE IF @vcListItemType='C'
				BEGIN
					SET @strColumns=@strColumns+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']' 
					 
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
					END  
					                                                  
					SET @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
				END                       
				ELSE IF @vcListItemType='U'                                                     
				BEGIN
					SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
					END                                                          
				END  
				ELSE IF @vcListItemType='SYS'                                                         
				BEGIN
					SET @strColumns=@strColumns+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                        

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end LIKE ''%' + @SearchText + '%'' '    
					END         
				END                     
			END                                                        
			ELSE IF @vcAssociatedControlType='DateField'                                                    
			BEGIN
				 SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
				 SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                  
					
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
				END
			END      
			ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
			BEGIN
				SET @strColumns = @strColumns + ',AD5.vcCity' + ' [' + @vcColumnName + ']'   

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD5.vcCity LIKE ''%' + @SearchText + '%'' '
				END
				
				SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

			END
			ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
			BEGIN
				SET @strColumns=@strColumns + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD6.vcCity LIKE ''%' + @SearchText + '%'' '
				END  
				
				SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
			END
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

				set @strColumns=@strColumns+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ CONCAT(' OUTER APPLY(SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ', @tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ')

				set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                   
			BEGIN
				SET @strColumns=@strColumns+','
								+ case  
									when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' 
									when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
									else @vcDbColumnName 
								end +' ['+ @vcColumnName+']'                
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
					case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when      
				  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end
					+' LIKE ''%' + @SearchText + '%'' '
				END  
			END   
		 	ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				set @strColumns=@strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END  
			ELSE                                                 
			BEGIN
				 set @strColumns=@strColumns+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
			END             
		END                
		ELSE IF @bitCustom = 1                
		BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
                
			SELECT 
				@vcFieldName=FLd_label,
				@vcAssociatedControlType=fld_type,
				@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
			FROM 
				CFW_Fld_Master
			WHERE 
				CFW_Fld_Master.Fld_Id = @numFieldId
			
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
			BEGIN
				SET @strColumns= @strColumns+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
				SET @WhereCondition= @WhereCondition 
									+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
									+ ' on CFW' + convert(varchar(3),@tintOrder) 
									+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
									+ 'and CFW' + convert(varchar(3),@tintOrder) 
									+ '.RecId=DM.numDivisionId   '                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'       
			BEGIN
				set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
												+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
		   END                
			ELSE IF @vcAssociatedControlType = 'DateField'            
			BEGIN
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                         
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'             
			BEGIN
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
				set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
			BEGIN
				SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=DM.numDivisionId '
			END              
		END          
                
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END     

	  
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
   
	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql=' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VIE.numContactId = ADC.numContactId ' + ISNULL(@WhereCondition,'')

	IF @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID ' 
	
	 -------Change Row Color-------
	Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
	SET @vcCSOrigDbCOlumnName=''
	SET @vcCSLookBackTableName=''

	Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

	insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	where DFCS.numDomainID=@numDomainID and DFFM.numFormID=36 AND DFCS.numFormID=36 and isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
	   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END   
	----------------------------                   
 
	 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
	set @strColumns=@strColumns+',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			set @Prefix = 'ADC.'                  
		 if @vcCSLookBackTableName = 'DivisionMaster'                  
			set @Prefix = 'DM.'
		 if @vcCSLookBackTableName = 'CompanyInfo'                  
			set @Prefix = 'CMP.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END

	IF @columnName LIKE 'CFW.Cust%' 
	BEGIN                
		SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                         
		SET @columnName = 'CFW.Fld_Value'                
	END                                         
	ELSE IF @columnName LIKE 'DCust%' 
    BEGIN                
        SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                   
        SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
        SET @columnName = 'LstCF.vcData'                  
    END
	ELSE IF @columnName = 'Opp.vcLastSalesOrderDate'   
	BEGIN
		SET @columnName = 'TempLastOrder.bintCreatedDate'
	END
	
	IF @columnName = 'CMP.vcPerformance'   
	BEGIN
		SET @columnName = 'TempPerformance.monDealAmount'
	END

	SET @strSql = @strSql + ' WHERE  (ISNULL(ADC.bitPrimaryContact,0)=1   OR ADC.numContactID IS NULL)
							AND CMP.numDomainID=DM.numDomainID     
							AND CMP.numCompanyType=46                                                        
							AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                                            
							AND DM.numDomainID= ' + convert(varchar(15),@numDomainID)

	SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive)
	
	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	

	 IF @bitPartner = 1 
	BEGIN
        SET @strSql = @strSql + 'and (DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numCreatedBy=' + CONVERT(VARCHAR(15), @numUserCntID) + ') '                                                                 
    END
	                                                         
    IF @SortChar <> '0' 
	BEGIN
        SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'''                                                               
	END

    IF @tintUserRightType = 1 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
	END
    ELSE IF @tintUserRightType = 2 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
    END
	               
    IF @numProfile <> 0 
	BEGIN
		SET @strSQl = @strSql + ' and cmp.vcProfile = ' + CONVERT(VARCHAR(15), @numProfile)                                                    
    END
	                                                      
    IF @tintSortOrder = 1 
        SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
    ELSE IF @tintSortOrder = 2 
        SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
    ELSE IF @tintSortOrder = 3 
        SET @strSql = @strSql + ' AND (DM.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                  
    ELSE IF @tintSortOrder = 5 
        SET @strSql = @strSql + ' order by numCompanyRating desc '                                                                     
    ELSE IF @tintSortOrder = 6 
        SET @strSql = @strSql + ' AND DM.bintCreatedDate > ''' + CONVERT(VARCHAR(20), DATEADD(day, -7,GETUTCDATE())) + ''''                                                                    
    ELSE IF @tintSortOrder = 7 
        SET @strSql = @strSql + ' and DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                     
    ELSE IF @tintSortOrder = 8 
        SET @strSql = @strSql + ' and DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID)                  
                                                   
        
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
		BEGIN
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
			set @strSql=@strSql
		END
		ELSE
			set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END


	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcCustomSearchCriteria 
    END  

	DECLARE @firstRec AS INTEGER                                                            
	DECLARE @lastRec AS INTEGER                                                            
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT = 0	
	DECLARE @bitSerialized AS BIT  = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup AS NUMERIC(18,0) =0
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @monListPrice MONEY
	DECLARE @bitRemoveGlobalWarehouse BIT

	SELECT @bitRemoveGlobalWarehouse=ISNULL(bitRemoveGlobalLocation,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@numItemGroup = ISNULL(@numItemGroup,0),
		@vcSKU = ISNULL(vcSKU,0),
		@vcUPC = ISNULL(numBarCodeId,0),
		@monListPrice = ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID)
				BEGIN
					RAISERROR('WAREHOUSE_DETAIL_NOT_FOUND',16,1)
				END
				
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode) AND NOT EXISTS(SELECT numChildItemID FROM dbo.OpportunityKitItems WHERE numChildItemID = @numItemCode) AND NOT EXISTS(SELECT numItemID FROM dbo.OpportunityKitChildItems WHERE numItemID = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
				BEGIN
					RAISERROR('INVALID_ITEM_CODE',16,1)
				END

				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = @@identity


				IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0 AND @bitRemoveGlobalWarehouse = 0
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						-1,
						0,
						0,
						@monWListPrice,
						'',
						@vcWHSKU,
						@vcBarCode,
						@numDomainID,
						GETDATE()
					)  

					SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()
				END



				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=@@identity

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		DECLARE @bitOppOrderExists AS BIT = 0

		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		IF @bitMatrix = 1 AND ISNULL(@bitOppOrderExists,0) <> 1 -- ITEM LEVEL ATTRIBUTES
		BEGIN
			IF ISNULL(@numWareHouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numWareHouseItemID
			
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numWareHouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END

			IF ISNULL(@numGlobalWarehouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numGlobalWarehouseItemID
				
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END   
		END
		ELSE IF DATALENGTH(@strFieldList) > 2 AND ISNULL(@bitOppOrderExists,0) <> 1
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

				declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
				Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
				insert into #tempTable (Fld_ID,Fld_Value)                                        
				SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
				WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
				delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C inner join #tempTable T on C.Fld_ID=T.Fld_ID where C.RecId=@numWareHouseItemID                              
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,Fld_Value,RecId,bitSerialized
				) 

				select Fld_ID,isnull(Fld_Value,'') as Fld_Value,@numWareHouseItemID,0 from #tempTable 

				drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 

		IF ISNULL(@numGlobalWarehouseItemID,0) > 0
		BEGIN        
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numGlobalWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = @dtAdjustmentDate,
			@numDomainID = @numDomainID 
		END

		-- KEEP IT LAST IN SECTION
		IF ISNULL(@numItemGroup,0) > 0 OR ISNULL(@bitMatrix,0) = 1 -- IF IT's MATRIX ITEM THEN 
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = (CASE WHEN @monListPrice > 0 THEN @monListPrice ELSE monWListPrice END), vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
		END
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	
		IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('KitItems_Depend',16,1);
			RETURN
		END	
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX),
	@bitExcludeEmployer BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                         WHERE  PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                        WHERE PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                     WHERE  PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

IF  LEN(@CustomerSelectFields) = 0 AND @CustomerSelectCustomFields = 0
BEGIN
	SET @CustomerSelectFields = 'vcCompanyName'
END

SET @strSQL = 'SELECT numDivisionID, numCompanyID' + CASE WHEN CHARINDEX('vcCompanyName',@CustomerSelectFields) = 0 THEN ',vcCompanyName' ELSE '' END  +
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName,
	CMP.numCompanyType AS numCompanyTypeID, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
BEGIN
	SET @strSQL = @strSQL + ' WHERE (' + @searchWhereCondition +')'

	IF @bitExcludeEmployer = 1
	BEGIN
		SET @strSQL = @strSQL + ' AND numCompanyTypeID <> 93'
	END
END
ELSE IF @bitExcludeEmployer = 1
BEGIN
	SET @strSQL = @strSQL + ' WHERE numCompanyTypeID <> 93'
END


    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

PRINT @strSQL
--SELECT @strSQL
EXEC (@strSQL)

END
GO
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist1')
DROP PROCEDURE usp_companylist1
GO
CREATE PROCEDURE [dbo].[USP_CompanyList1]                                                      
@numRelationType as numeric(9),                                                   
@numUserCntID numeric(9),                                                      
@tintUserRightType tinyint,                                                          
@SortChar char(1)='0',                                                     
@FirstName varChar(100)= '',                                                      
@LastName varChar(100)= '',                                                      
@CustName varChar(100)= '',                                                    
@CurrentPage int,                                                    
@PageSize int,                                                    
@columnName as Varchar(50),                                                    
@columnSortOrder as Varchar(10),                                            
@tintSortOrder numeric=0,                                             
@numProfile as numeric(9)=0,                                    
@numDomainID as numeric(9)=0,                            
@tintCRMType as tinyint,                            
@bitPartner as BIT,
@numFormID AS NUMERIC(9),
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                     
as       
	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END

    declare @firstRec as integer                                                                
  declare @lastRec as integer                                                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                          
  declare @column as varchar(50)                  
set @column = @columnName                  
declare @lookbckTable as varchar(50)                  
set @lookbckTable = ''                                                               
                                                    
  declare @strSql as varchar(MAX)                                                  
set @strSql='with tblCompany as (SELECT  '          
if @tintSortOrder=7 or @tintSortOrder=8  set @strSql=@strSql + ' '             
if @tintSortOrder=5  set @strSql=@strSql + 'ROW_NUMBER() OVER ( order by numCompanyRating desc) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '           
else           
 set @strSql=@strSql +' ROW_NUMBER() OVER (ORDER BY '+@column+' '+ @columnSortOrder+') AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '                                                       
set @strSql=@strSql +'  DM.numDivisionID                                          
    FROM  CompanyInfo CMP                                 
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID 
	left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
	left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId '                                            
                               
if @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                            
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0                           
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                          
set @strSql=@strSql + ' ##JOIN##                                                     
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus 
left Join AddressDetails AD on AD.numRecordId= DM.numDivisionID and AD.tintAddressOf=2 and AD.tintAddressType=1 AND AD.bitIsPrimary=1 and AD.numDomainID= DM.numDomainID                                                   
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL)                                                    
    and CMP.numDomainID=  DM.numDomainID                                                    
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                                    
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                     
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                                    
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                               
if @numRelationType<>'0' set    @strSql=@strSql+' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''                                                    
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                                                     
if @tintUserRightType=1 set @strSql=@strSql + ' AND ((DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or ( CA.bitShareportal=1 and CA.bitDeleted=0))' else ')' end                                                 
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ') '                     
if @tintSortOrder=1  set @strSql=@strSql + ' AND DM.numStatusID=2 '                                                                
else if @tintSortOrder=2  set @strSql=@strSql + ' AND DM.numStatusID=3 '                                                                          
else if @tintSortOrder=3  set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                       
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                                                                       
else if @tintSortOrder=6  set @strSql=@strSql + ' AND DM.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                                                      
else if @tintSortOrder=7  set @strSql=@strSql + ' and DM.numCreatedby='+convert(varchar(15),@numUserCntID)                    
--+ ' ORDER BY DM.bintCreateddate desc '                                                              
else if @tintSortOrder=8  set @strSql=@strSql + ' and DM.numModifiedby='+convert(varchar(15),@numUserCntID)          
           
--if (@tintSortOrder=7 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X   '                                                     
--else if (@tintSortOrder=8 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X  '                                                               
--else 
if @tintSortOrder=9  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''              
                                       
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                                            
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                                              
                                                  
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria
                                                  
                                                    
  set @strSql=@strSql + ')'                      
   
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)           
declare @numListID AS numeric(9)                                        
declare @vcAssociatedControlType varchar(20)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(MAX)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)             
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)            
declare @vcColumnName AS VARCHAR(500)                          
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                 
declare @DefaultNocolumns as tinyint                      
set @Nocolumns=0  

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType) TotalRows

                    
  set @DefaultNocolumns=  @Nocolumns  
                       
while @DefaultNocolumns>0                      
begin                     
 set @DefaultNocolumns=@DefaultNocolumns-1                    
end              
   

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    


set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'                       

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelationType AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN      

	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select @numFormID,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=@numFormID and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc   
	END          
    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
 END                                                  
                        
--    set @DefaultNocolumns=  @Nocolumns  
Declare @ListRelID as numeric(9)     
      select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                                                    
while @tintOrder>0                                                        
begin                                             
     print @vcFieldName                                               
    if @bitCustom = 0            
 begin                      
       
      
 declare @Prefix as varchar(5)                            
      if @vcLookBackTableName = 'AdditionalContactsInformation'                            
		set @Prefix = 'ADC.'                            
      else if @vcLookBackTableName = 'DivisionMaster'                            
		set @Prefix = 'DM.'   
		
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
	
 if @vcAssociatedControlType='SelectBox'                                                              
        begin                                                              
                                                                  
     if @vcListItemType='LI'                                                               
     BEGIN
     	IF @numListID=40 
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'          
			
			IF @vcDbColumnName='numShipCountry'
			BEGIN
				SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
			END
			ELSE
			BEGIN
				SET @WhereCondition = @WhereCondition
								+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
								+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
			END
		 END
		 ELSE
		 BEGIN    
		  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                             
		  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
		 END
     end    
       else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end                                                            
     else if @vcListItemType='S'                                                               
     begin       
		IF @vcDbColumnName='numShipState'
		BEGIN
			SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
				+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
		END
		ELSE IF @vcDbColumnName='numBillState'
		BEGIN
			SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
				+ ' left Join State BillState on BillState.numStateID=AD4.numState '
		END
                                                       
--      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                              
--      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                              
     end                                                              
     else if @vcListItemType='T'                                                               
     begin                                                              
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                              
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
     end                             
     else if   @vcListItemType='U'                                                           
    begin                             
                                
                                  
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                              
    end
	ELSE IF @vcListItemType='SYS'                                                         
	BEGIN
		SET @strSql=@strSql+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                              
	END                                  
    end           
          
else if @vcAssociatedControlType='DateField'                                                        
 begin                  
             
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '            
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                      
   end          
ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
BEGIN
	SET @strSql = @strSql + ',AD5.vcCity' + ' [' + @vcColumnName + ']'  
				
	SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

END
ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
BEGIN
	SET @strSql=@strSql + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
						
	SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
END                
else if @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                       
begin          
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when          
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                    
             
 end          
else                                                  
begin      
 set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end    
            
 end            
else if @bitCustom = 1            
begin            
               
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                           
    from CFW_Fld_Master                                            
   where  CFW_Fld_Master.Fld_Id = @numFieldId             
            
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'    
          
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin              
                 
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
    else if @vcAssociatedControlType = 'CheckBox'              
   begin              
                 
-- OLD    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+']'               
set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'               
  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                       
   end              
   else if @vcAssociatedControlType = 'DateField'          
   begin    
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'                
   begin              
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                      
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END                    
end                
   
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                  
                       
                                      
                                                       
end                             


set @strSql=@strSql+' ,TotalRowCount,
	ISNULL(VIE.Total,0) as TotalEmail,
	ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
  From CompanyInfo CMP                                                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                                      
    left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
    left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId
   '+@WhereCondition+       
' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                                          
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL) and 
  CMP.numDomainID=  DM.numDomainID and DM.numDomainID = ' + convert(varchar(18),@numDomainID) + '
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)     
  
  

SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)              
-- new code added by sojan 05 Mar 2010

--declare @vcTestSql varchar(8000);
--declare @FieldCount numeric(8);
--
--set @vcTestSql = substring(@strSql, charindex('select',@strSql),len(@strSql))
--select @FieldCount= [dbo].[uf_charCount] (@vcTestSql,',')
--
--set @vcTestSql=' union select count(*),' + replicate('Null,',@FieldCount-2)
--
--set @vcTestSql=substring(@vcTestSql,1,len(@vcTestSql)-1)
--set @vcTestSql = @vcTestSql +' from tblCompany order by RowNumber'   
--
--set @strSql=REPLACE(@strSql,'|',',') + @vcTestSql

-- old code
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblCompany order by RowNumber'                                                 
      
SET @strSql =@strSql + ' ORDER BY '+@column+' '+ @columnSortOrder+' '                                
print @strSql                              
                      
exec (@strSql) 


SELECT * FROM #tempForm

DROP TABLE #tempForm
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT,
	@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDFAPID NUMERIC(18,0)
	DECLARE @numDFDaysID NUMERIC(18,0)
	DECLARE @bitLastYear BIT
	DECLARE @bitIncludeOpenReleaseDates BIT
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @dtLastExecution DATETIME

	
	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		---------------------------------------

			DECLARE @bitWarehouseFilter BIT = 0
			DECLARE @bitItemClassificationFilter BIT = 0
			DECLARE @bitItemGroupFilter BIT = 0

			IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
			BEGIN
				SET @bitWarehouseFilter = 1
			END

			IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
			BEGIN
				SET @bitItemClassificationFilter = 1
			END

			IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
			BEGIN
				SET @bitItemGroupFilter = 1
			END

	
			DECLARE @TEMPReleaseDates TABLE
			(
				numItemCode NUMERIC(18,0),
				numWarehouseID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQty INT,
				numQtySalesOppProgress INT
			)

			DECLARE @TEMPORDER TABLE
			(
				numOppID NUMERIC(18,0),
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numWarehouseID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQty FLOAT,
				numQtySalesOppProgress FLOAT,
				bitKitParent BIT,
				bitAssembly BIT,
				dtReleaseDate DATE
			)

			-- Based on order/opp release date
			INSERT INTO 
				@TEMPORDER
			SELECT
				OpportunityMaster.numOppID
				,OpportunityItems.numoppitemtcode
				,OpportunityItems.numItemCode
				,WI.numWareHouseID
				,(CASE
					WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
					THEN
						(CASE 
							WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
							THEN
								(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
							ELSE
								WI.numWareHouseItemID
						END)
					ELSE
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
				END) numWarehouseItemID
				,OpportunityItems.numUnitHour - ISNULL(TEMPItemRelease.numQty,0) AS numQty
				,0 AS numQtySalesOppProgress
				,I.bitKitParent
				,I.bitAssembly
				,OpportunityMaster.dtReleaseDate
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId = OpportunityItems.numOppId
			INNER JOIN
				Item I
			ON
				OpportunityItems.numItemCode = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID
			OUTER APPLY
			(
				SELECT
					SUM(numQty) AS numQty
				FROM
					OpportunityItemsReleaseDates OIRD
				WHERE
					OIRD.numOppID = OpportunityMaster.numOppId
					AND OIRD.numOppItemID = OpportunityItems.numoppitemtCode
					AND ((OIRD.dtReleaseDate <= DATEADD(DAY,180,GETDATE()) AND ISNULL(OIRD.tintStatus,0) = 1) OR ISNULL(OIRD.tintStatus,0) = 2)
			) TEMPItemRelease
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND ISNULL(tintOppType,0) = 1
				AND OpportunityMaster.dtReleaseDate IS NOT NULL AND CAST(OpportunityMaster.dtReleaseDate AS DATE) <= DATEADD(DAY,180,GETDATE())
				AND ISNULL(numReleaseStatus,1) = 1
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
					WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
				)
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
					I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
				)
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
					I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
				)

			-- Based on item release date
			INSERT INTO 
				@TEMPORDER
			SELECT
				OI.numOppID
				,OI.numoppitemtcode
				,OI.numItemCode
				,WI.numWareHouseID
				,(CASE
					WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
					THEN
						(CASE 
							WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
							THEN
								(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
							ELSE
								WI.numWareHouseItemID
						END)
					ELSE
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
				END) numWarehouseItemID
				,OI.numUnitHour
				,0
				,I.bitKitParent
				,I.bitAssembly
				,dtReleaseDate
			FROM
				OpportunityItemsReleaseDates OIRD
			INNER JOIN
				OpportunityItems OI
			ON
				OIRD.numOppID=OI.numOppId
				AND OIRD.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			WHERE
				ISNULL(tintStatus,0)=1
				AND dtReleaseDate <= DATEADD(DAY,180,GETDATE())
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
					WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
				)
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
					I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
				)
				AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
					I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
				)


			-- Based on sales opp total progress
			INSERT INTO 
				@TEMPORDER
			SELECT
				OI.numOppID
				,OI.numoppitemtcode
				,OI.numItemCode
				,WI.numWareHouseID
				,(CASE
					WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
					THEN
						(CASE 
							WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
							THEN
								(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
							ELSE
								WI.numWareHouseItemID
						END)
					ELSE
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
				END) numWarehouseItemID
				,0
				,CEILING(CAST(OI.numUnitHour - ISNULL(TEMPItemRelease.numQty,0) AS FLOAT) * (PP.intTotalProgress/100)) AS numQty
				,I.bitKitParent
				,I.bitAssembly
				,intPEstimatedCloseDate
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppID=OI.numOppId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			OUTER APPLY
			(
				SELECT
					SUM(numQty) AS numQty
				FROM
					OpportunityItemsReleaseDates OIRD
				WHERE
					OIRD.numOppID = OM.numOppId
					AND OIRD.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OIRD.tintStatus,0) = 2
			) TEMPItemRelease
			LEFT JOIN
				ProjectProgress PP
			ON
				OM.numOppId = PP.numOppId
			WHERE
				tintOppType = 1
				AND ISNULL(tintOppStatus,0)=0
				AND intPEstimatedCloseDate IS NOT NULL AND CAST(OM.intPEstimatedCloseDate AS DATE) <= DATEADD(DAY,180,GETDATE())
				AND ISNULL(OM.numReleaseStatus,1) = 1
				AND ISNULL(PP.intTotalProgress,0) > 0
				AND
				(
					@bitWarehouseFilter = 0 OR
					WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
				)
				AND
				(
					@bitItemClassificationFilter = 0 OR
					I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
				)
				AND
				(
					@bitItemGroupFilter = 0 OR
					I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
				)
	

			-- GET KIT ITEMS CHILD ITEMS
			;WITH CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,bitKitParent,numItemGroup,numWarehouseID,numWarehouseItemID,numQty,numQtySalesOppProgress,dtReleaseDate) AS
			(
				SELECT 
					numOppChildItemID
					,t1.numOppId
					,t1.numOppItemID
					,numChildItemID
					,I.bitKitParent
					,I.numItemGroup
					,WI.numWarehouseID
					,WI.numWarehouseItemID
					,t1.numQty * numQtyItemsReq_Orig
					,t1.numQtySalesOppProgress * numQtyItemsReq_Orig
					,t1.dtReleaseDate
				FROM 
					@TEMPORDER AS t1
				INNER JOIN
					OpportunityKitItems OKI
				ON
					t1.numOppID = OKI.numOppId
					AND t1.numOppItemID = OKI.numOppItemID
				INNER JOIN
					Item I
				ON
					OKI.numChildItemID = I.numItemCode
				INNER JOIN
					WarehouseItems WI
				ON
					OKI.numWareHouseItemId = WI.numWarehouseItemID
				UNION ALL
				SELECT
					OKCI.numOppKitChildItemID
					,OKCI.numOppId
					,OKCI.numOppItemID
					,OKCI.numItemID
					,I.bitKitParent
					,I.numItemGroup
					,WI.numWarehouseID
					,WI.numWarehouseItemID
					,c.numQty * numQtyItemsReq_Orig
					,c.numQtySalesOppProgress * OKCI.numQtyItemsReq_Orig
					,c.dtReleaseDate
				FROM
					OpportunityKitChildItems OKCI
				INNER JOIN	
					CTE c
				ON
					OKCI.numOppChildItemID = c.numOppChildItemID
				INNER JOIN
					Item I
				ON
					OKCI.numItemID = I.numItemCode
				INNER JOIN
					WarehouseItems WI
				ON
					OKCI.numWareHouseItemId = WI.numWarehouseItemID
			)

			INSERT INTO
				@TEMPORDER
			SELECT
				numOppID,
				numOppItemID,
				numItemCode,
				c.numWareHouseID,
				(CASE
					WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
					THEN
						(CASE 
							WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
							THEN
								(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
							ELSE
								c.numWareHouseItemID
						END)
					ELSE
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
				END),
				numQty,
				numQtySalesOppProgress,
				bitKitParent,
				0,
				dtReleaseDate
			FROM
				CTE c
			WHERE
				ISNULL(bitKitParent,0) = 0

			-- GET ASSEMBLY CHILD ITEMS
			;WITH CTEAssembly(numItemCode, bitAssembly, numItemGroup, numWareHouseID,numWarehouseItemID, numQty, numQtySalesOppProgress,dtReleaseDate) AS 
			( 
				SELECT   
					numChildItemID,
					I.bitAssembly,
					I.numItemGroup,
					ISNULL(WI.numWareHouseID, 0),
					ISNULL(WI.numWareHouseItemID, 0),
					CAST(DTL.numQtyItemsReq * T1.numQty AS FLOAT),
					CAST(DTL.numQtyItemsReq * T1.numQtySalesOppProgress AS FLOAT),
					T1.dtReleaseDate
				FROM
					@TEMPORDER T1
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numItemKitID = T1.numItemCode
				INNER JOIN
					Item I
				ON
					Dtl.numChildItemID = I.numItemCode
				INNER JOIN
					WareHouseItems WI
				ON
					Dtl.numWareHouseItemId = WI.numWareHouseItemID
				WHERE 
					ISNULL(T1.bitAssembly,0) = 1
				UNION ALL
				SELECT 
					I.numItemCode,
					I.bitAssembly,
					I.numItemGroup,
					ISNULL(WI.numWareHouseID, 0),
					ISNULL(WI.numWareHouseItemID, 0),
					CAST(DTL.numQtyItemsReq * c.numQty AS FLOAT),
					CAST(DTL.numQtyItemsReq * c.numQtySalesOppProgress AS FLOAT),
					c.dtReleaseDate
				FROM
					ItemDetails Dtl
				INNER JOIN 
					CTEAssembly c 
				ON 
					Dtl.numItemKitID = c.numItemCode
				INNER JOIN
					Item I
				ON
					Dtl.numChildItemID = I.numItemCode
				INNER JOIN
					WareHouseItems WI
				ON
					Dtl.numWareHouseItemId = WI.numWareHouseItemID
				WHERE    
					Dtl.numChildItemID != c.numItemCode			
			)

			INSERT INTO
				@TEMPORDER
			SELECT
				0,
				0,
				numItemCode,
				c.numWareHouseID,
				(CASE
					WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
					THEN
						(CASE 
							WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
							THEN
								(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
							ELSE
								c.numWareHouseItemID
						END)
					ELSE
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
				END),
				numQty,
				numQtySalesOppProgress,
				0,
				0,
				dtReleaseDate
			FROM
				CTEAssembly c
			WHERE
				ISNULL(bitAssembly,0) = 0


			INSERT INTO 
				@TEMPReleaseDates
			SELECT
				T1.numItemCode
				,T1.numWarehouseID
				,T1.numWarehouseItemID
				,SUM(numQty)
				,SUM(numQtySalesOppProgress)
			FROM
				@TEMPORDER T1
			WHERE
				ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
				AND ISNULL(T1.numWarehouseItemID,0) > 0
			GROUP BY
				T1.numItemCode
				,T1.numWarehouseID
				,T1.numWarehouseItemID

		---------------------------------------
		
		DECLARE @TotalRowCount AS INTEGER = 0
		DECLARE @numAnalysisDays INT
		DECLARE @numForecastDays INT

		-- Get Selected Historical Analysis Pattern, Forecast Days and is Last Year
		SELECT 
			@numDomainID=numDomainID, 
			@numDFAPID=numDFAPID, 
			@numDFDaysID=numDFDaysID, 
			@bitLastYear=bitLastYear, 
			@bitIncludeOpenReleaseDates=bitIncludeOpenReleaseDates,
			@dtLastExecution = dtExecutionDate
		FROM 
			DemandForecast 
		WHERE 
			numDFID = @numDFID

		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numDFAPID

		-- Get Days for forecast
		SELECT  @numForecastDays = numDays FROM DemandForecastDays WHERE numDFDaysID = @numDFDaysID

		-- Check if get last week from last year
		IF ISNULL(@bitLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETDATE())
		END


		DECLARE @TEMPOppMaster TABLE
		(
			numItemCode NUMERIC(18,0),
			numWarehouseID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT
		)

		-- Get total quantity of each item sold in sales order between from and to date and insert into temp table
		INSERT INTO @TEMPOppMaster
		(
			numItemCode,
			numWarehouseID,
			numWarehouseItemID,
			numQuantitySold
		)
		SELECT
			numItemCode,
			numWarehouseID,
			numWarehouseItemID,
			ISNULL(numQtySold,0)
		FROM
			dbo.GetDFHistoricalSalesOrderData(@numDFID,@numDomainID,@dtFromDate,@dtToDate)


		-- RLEASE DATES AND SALE OPPORTUNITY PROGRESS NUMBER		
		DECLARE @TEMPItems TABLE
		(
			numItemCode NUMERIC(18,0),
			numWarehouseID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT,
			num7Days INT DEFAULT 0,
			num7DaysTotalProgess INT DEFAULT 0,
			num8Days INT DEFAULT 0,
			num8DaysTotalProgess INT DEFAULT 0,
			num15Days INT DEFAULT 0,
			num15DaysTotalProgess INT DEFAULT 0,
			num16Days INT DEFAULT 0,
			num16DaysTotalProgess INT DEFAULT 0,
			num30Days INT DEFAULT 0,
			num30DaysTotalProgess INT DEFAULT 0,
			num31Days INT DEFAULT 0,
			num31DaysTotalProgess INT DEFAULT 0,
			num60Days INT DEFAULT 0,
			num60DaysTotalProgess INT DEFAULT 0,
			num61Days INT DEFAULT 0,
			num61DaysTotalProgess INT DEFAULT 0,
			num90Days INT DEFAULT 0,
			num90DaysTotalProgess INT DEFAULT 0,
			num91Days INT DEFAULT 0,
			num91DaysTotalProgess INT DEFAULT 0,
			num180Days INT DEFAULT 0,
			num180DaysTotalProgess INT DEFAULT 0
		)

		-- 7 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num7Days
			,num7DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,7,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 8 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num8Days
			,num8DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,8,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
		
		-- 15 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num15Days
			,num15DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,15,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 16 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num16Days
			,num16DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,16,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 30 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num30Days
			,num30DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,30,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 31 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num31Days
			,num31DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,31,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 60 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num60Days
			,num60DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,60,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 61 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num61Days
			,num61DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,61,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 90 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num90Days
			,num90DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,90,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 91 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num91Days
			,num91DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,91,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID

		-- 180 Days
		INSERT INTO @TEMPItems
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,num180Days
			,num180DaysTotalProgess
		)
		SELECT
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID
			,SUM(numQty)
			,SUM(numQtySalesOppProgress)
		FROM
			@TEMPORDER T1
		WHERE
			ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
			AND ISNULL(T1.numWarehouseItemID,0) > 0
			AND dtReleaseDate <= DATEADD(DAY,180,GETDATE())
		GROUP BY
			T1.numItemCode
			,T1.numWarehouseID
			,T1.numWarehouseItemID


		DECLARE @TempItemsFinal TABLE
		(
			numItemCode NUMERIC(18,0),
			numWarehouseID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			vc7Days VARCHAR(100),
			vc8To15Days VARCHAR(100),
			vc16To30Days VARCHAR(100),
			vc31To60Days VARCHAR(100),
			vc61To90Days VARCHAR(100),
			vc91To180Days VARCHAR(100)
		)

		INSERT INTO 
			@TempItemsFinal
		SELECT
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
			,CONCAT(SUM(num7Days),' ',SUM(num7DaysTotalProgess),' ','#HISTORICALQty#')
			,CONCAT('(',SUM(num8Days),'-',SUM(num15Days),') (',SUM(num8DaysTotalProgess),'-',SUM(num15DaysTotalProgess),') ','#HISTORICALQty#')
			,CONCAT('(',SUM(num16Days),'-',SUM(num30Days),') (',SUM(num16DaysTotalProgess),'-',SUM(num30DaysTotalProgess),') ','#HISTORICALQty#')
			,CONCAT('(',SUM(num31Days),'-',SUM(num60Days),') (',SUM(num31DaysTotalProgess),'-',SUM(num60DaysTotalProgess),') ','#HISTORICALQty#')
			,CONCAT('(',SUM(num61Days),'-',SUM(num90Days),') (',SUM(num61DaysTotalProgess),'-',SUM(num90DaysTotalProgess),') ','#HISTORICALQty#')
			,CONCAT('(',SUM(num91Days),'-',SUM(num180Days),') (',SUM(num91DaysTotalProgess),'-',SUM(num180DaysTotalProgess),') ','#HISTORICALQty#')
		FROM
			@TEMPItems
		GROUP BY
			numItemCode
			,numWarehouseID
			,numWarehouseItemID

		UPDATE
			T1
		SET
			vc7Days = REPLACE(vc7Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 7),')'))
			,vc8To15Days = REPLACE(vc8To15Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 8),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 15),')'))
			,vc16To30Days = REPLACE(vc16To30Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 16),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 30),')'))
			,vc31To60Days = REPLACE(vc31To60Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 31),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 60),')'))
			,vc61To90Days = REPLACE(vc61To90Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 61),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 90),')'))
			,vc91To180Days = REPLACE(vc91To180Days,'#HISTORICALQty#',CONCAT('(',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 91),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 180),')'))
		FROM
			@TempItemsFinal T1
		INNER JOIN
			@TEMPOppMaster T2
		ON
			T1.numItemCode = T2.numItemCode
			AND T1.numWarehouseID = T2.numWarehouseID
			AND T1.numWarehouseItemID = T2.numWarehouseItemID
		WHERE
			ISNULL(T2.numQuantitySold,0) > 0

		

		INSERT INTO 
			@TempItemsFinal
		SELECT 
			T2.numItemCode
			,T2.numWarehouseID
			,T2.numWarehouseItemID
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 7),')')
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 8),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 15),')')
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 16),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 30),')')
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 31),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 60),')')
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 61),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 90),')')
			,CONCAT('(0-0) (0-0) (',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 91),'-',CEILING((CAST(T2.numQuantitySold AS FLOAT)/@numAnalysisDays) * 180),')')
		FROM
			@TEMPOppMaster T2
		LEFT JOIN
			@TempItemsFinal T1
		ON
			T1.numItemCode = T2.numItemCode
			AND T1.numWarehouseID = T2.numWarehouseID
			AND T1.numWarehouseItemID = T2.numWarehouseItemID
		WHERE
			T1.numItemCode IS NULL 
			AND T1.numWarehouseID IS NULL
			AND T1.numWarehouseItemID IS NULL

		UPDATE 
			@TempItemsFinal 
		SET 
			vc7Days = REPLACE(vc7Days,'#HISTORICALQty#','(0)')
			,vc8To15Days = REPLACE(vc8To15Days,'#HISTORICALQty#','(0-0)')
			,vc16To30Days = REPLACE(vc16To30Days,'#HISTORICALQty#','(0-0)')
			,vc31To60Days = REPLACE(vc31To60Days,'#HISTORICALQty#','(0-0)')
			,vc61To90Days = REPLACE(vc61To90Days,'#HISTORICALQty#','(0-0)')
			,vc91To180Days	 = REPLACE(vc91To180Days,'#HISTORICALQty#','(0-0)')

		SELECT 
			ROW_NUMBER() OVER (ORDER BY TEMP.numItemCode) AS NUMBER,
			TEMP.*
		INTO 
			#TEMP
		FROM
		(
			SELECT
				Item.numItemCode
				,W.numWareHouseID
				,Item.numVendorID AS numVendorID
				,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
				,Item.vcItemName
				,Item.charItemType AS vcItemType
				,Item.txtItemDesc
				,CONCAT(ISNULL(WI.numOnHand,0),' / ', ISNULL(WI.numAllocation,0),' / ', ISNULL(WI.numOnOrder,0),' / ', ISNULL(WI.numBackOrder,0),' / ', ISNULL(WI.numReOrder,0)) AS vcInventory
				,W.vcWareHouse
				,vc7Days
				,vc8To15Days
				,vc16To30Days
				,vc31To60Days
				,vc61To90Days
				,vc91To180Days
				,(CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) AS numQtyToPurchase
				--,(CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING((dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) * ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0)))  ELSE 0 END) AS numQtyToPurchase
				,ISNULL(Item.numPurchaseUnit,0) AS numUOM
				,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
				,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),'-') AS vcUOM
				,T1.numWarehouseItemID
			FROM
				@TempItemsFinal T1
			INNER JOIN
				Item
			ON
				Item.numItemCode = T1.numItemCode
			INNER JOIN
				Warehouses W
			ON
				T1.numWarehouseID = W.numWareHouseID
			OUTER APPLY
			(
				SELECT
					SUM(numBackOrder) as numBackOrder
					,SUM(numOnOrder) as numOnOrder
					,SUM(numOnHand) as numOnHand
					,SUM(numAllocation) as numAllocation
					,SUM(numReOrder) as numReOrder
				FROM
					WareHouseItems
				WHERE
					numWarehouseID = T1.numWarehouseID
					AND numItemID = Item.numItemCode
			) WI
		) AS TEMP
		WHERE
			numWarehouseItemID > 0
		
		SELECT @TotalRowCount = COUNT(*) FROM #TEMP
		
		SELECT *,ISNULL(@TotalRowCount,0) AS TotalRowCount FROM #TEMP WHERE NUMBER BETWEEN ((@numPageIndex - 1) * @numPageSize + 1) AND (@numPageIndex * @numPageSize) ORDER BY numItemCode

		--Update Last Execution Date
		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetDomains')
DROP PROCEDURE dbo.USP_ElasticSearch_GetDomains
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetDomains]
	
AS 
BEGIN
	SELECT
		numDomainId
	FROM
		Domain
	WHERE
		ISNULL(bitElasticSearch,0) = 1
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetOrganizations')
DROP PROCEDURE dbo.USP_ElasticSearch_GetOrganizations
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetOrganizations]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @query AS NVARCHAR(MAX);

	DECLARE @CustomFields AS VARCHAR(1000)
	SET @CustomFields = STUFF((SELECT ',CFW' + CAST(FLD_ID AS VARCHAR) FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=1 FOR XML PATH(''),TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')

	SELECT @query = CONCAT('SELECT
		DivisionMaster.numDivisionID
		,CompanyInfo.vcCompanyName
		,ISNULL(LDComDiff.vcData,'''') numCompanyDiff
		,ISNULL(DivisionMaster.vcCompanyDiff,'''') vcCompanyDiff
		,ISNULL(LDCMPProfile.vcData,'''') vcProfile
		,ISNULL(LDCMPType.vcData,'''') numCompanyType
		,ISNULL(vcComPhone,'''') vcComPhone
		,ISNULL(LDCMPNoOfEmployees.vcData,'''') numNoOfEmployeesId
		,ISNULL(LDCMPAnnualRevenue.vcData,'''') numAnnualRevID
		,ISNULL(vcWebSite,'''') vcWebSite 
		,ISNULL(LDTerritory.vcData,'''') numTerID
		,ISNULL(LDLeadSource.vcData,'''') vcHow
		,ISNULL(LDCMPIndustry.vcData,'''') numCompanyIndustry
		,ISNULL(LDCMPRating.vcData,'''') numCompanyRating
		,ISNULL(LDDMStatus.vcData,'''') numStatusID
		,ISNULL(LDCMPCreditLimit.vcData,'''') numCompanyCredit
		,ISNULL(vcComFax,'''') vcComFax
		,ISNULL(txtComments,'''') txtComments
		,ISNULL(AdditionalContactsInformation.vcFirstName,'''') vcFirstName
		,ISNULL(AdditionalContactsInformation.vcLastName,'''') vcLastName
		,ISNULL(ShippingAddress.vcStreet,'''') vcShipStreet
		,ISNULL(ShippingAddress.vcCity,'''') vcShipCity
		,ISNULL(ShippingAddress.vcPostalCode,'''') vcShipPostCode
		,ISNULL(LDShipCountry.vcData,'''') numShipCountry 
		,ISNULL(ShipState.vcState,'''') numShipState
		,ISNULL(BillingAddress.vcStreet,'''') vcShipStreet
		,ISNULL(BillingAddress.vcCity,'''') vcShipCity
		,ISNULL(BillingAddress.vcPostalCode,'''') vcShipPostCode
		,ISNULL(LDBillCountry.vcData,'''') numShipCountry 
		,ISNULL(BillState.vcState,'''') numShipState 
		,ISNULL(LDDMFollowup.vcData,'''') numFollowUpStatus
		,ISNULL(BillingTerms.vcTerms,'''') numBillingDays
		,DivisionMaster.vcPartnerCode
		,CustomFields.*
	FROM 
		CompanyInfo
	INNER JOIN
		DivisionMaster
	ON
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	LEFT JOIN
		ListDetails LDCMPRating
	ON
		CompanyInfo.numCompanyCredit = LDCMPRating.numListItemID
		AND LDCMPRating.numListID = 2
	LEFT JOIN
		ListDetails LDCMPCreditLimit
	ON
		CompanyInfo.numCompanyRating = LDCMPCreditLimit.numListItemID
		AND LDCMPCreditLimit.numListID = 3
	LEFT JOIN
		ListDetails LDCMPIndustry
	ON
		CompanyInfo.numCompanyIndustry = LDCMPIndustry.numListItemID
		AND LDCMPIndustry.numListID = 4
	LEFT JOIN
		ListDetails LDCMPType
	ON
		CompanyInfo.numCompanyType = LDCMPType.numListItemID
		AND LDCMPType.numListID = 5
	LEFT JOIN
		ListDetails LDCMPAnnualRevenue
	ON
		CompanyInfo.numAnnualRevID = LDCMPAnnualRevenue.numListItemID
		AND LDCMPAnnualRevenue.numListID = 6
	LEFT JOIN
		ListDetails LDCMPNoOfEmployees
	ON
		CompanyInfo.numNoOfEmployeesId = LDCMPNoOfEmployees.numListItemID
		AND LDCMPNoOfEmployees.numListID = 7
	LEFT JOIN
		ListDetails LDCMPProfile
	ON
		CompanyInfo.vcProfile = LDCMPProfile.numListItemID
		AND LDCMPProfile.numListID = 21
	LEFT JOIN
		ListDetails LDDMFollowup
	ON
		DivisionMaster.numFollowUpStatus = LDDMFollowup.numListItemID
		AND LDDMFollowup.numListID = 30
	LEFT JOIN
		ListDetails LDDMStatus
	ON
		DivisionMaster.numStatusID = LDDMStatus.numListItemID
		AND LDDMStatus.numListID = 1
	LEFT JOIN
		BillingTerms
	ON
		DivisionMaster.numBillingDays = BillingTerms.numTermsID
	LEFT JOIN
		ListDetails LDComDiff
	ON 
		DivisionMaster.numCompanyDiff = LDComDiff.numListItemID
		AND LDComDiff.numListID = 438
	LEFT JOIN
		ListDetails LDTerritory
	ON 
		DivisionMaster.numTerID = LDTerritory.numListItemID
		AND LDTerritory.numListID = 78
	LEFT JOIN
		ListDetails LDLeadSource
	ON 
		CompanyInfo.vcHow = LDLeadSource.numListItemID
		AND LDLeadSource.numListID = 18
	LEFT JOIN
		AdditionalContactsInformation
	ON
		AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
		AND ISNULL(bitPrimaryContact,0) = 1
	LEFT JOIN
		AddressDetails BillingAddress
	ON
		BillingAddress.numRecordID = DivisionMaster.numDivisionID AND 
		BillingAddress.tintAddressOf = 2 AND 
		BillingAddress.tintAddressType = 1 AND 
		BillingAddress.bitIsPrimary = 1
	LEFT JOIN
		ListDetails LDBillCountry
	ON 
		BillingAddress.numCountry = LDBillCountry.numListItemID
		AND LDBillCountry.numListID = 40
	LEFT JOIN
		[State] BillState
	ON 
		BillingAddress.numState = BillState.numStateID
		AND BillState.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		AddressDetails ShippingAddress
	ON
		ShippingAddress.numRecordID = DivisionMaster.numDivisionID AND 
		ShippingAddress.tintAddressOf = 2 AND 
		ShippingAddress.tintAddressType = 2 AND 
		ShippingAddress.bitIsPrimary = 1
	LEFT JOIN
		ListDetails LDShipCountry
	ON 
		ShippingAddress.numCountry = LDShipCountry.numListItemID
		AND LDShipCountry.numListID = 40
	LEFT JOIN
		[State] ShipState
	ON 
		ShippingAddress.numState = ShipState.numStateID
		AND ShipState.numDomainID = DivisionMaster.numDomainID
	OUTER APPLY
	(
		SELECT 
		* 
		FROM 
		(
			SELECT  
				CONCAT(''CFW'',CFW_Fld_Values.Fld_ID) Fld_ID,
				dbo.fn_GetCustFldStringValue(CFW_FLD_Values.Fld_ID, CFW_FLD_Values.RecId, CFW_FLD_Values.Fld_Value) Fld_Value 
			FROM 
				CFW_Fld_Master
			LEFT JOIN 
				CFW_Fld_Values
			ON
				CFW_Fld_Values.Fld_ID = CFW_Fld_Master.Fld_id
			WHERE 
				RecId = DivisionMaster.numDivisionID
		) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
	 ) CustomFields
	WHERE
		CompanyInfo.numDomainID =',@numDomainID)

	EXEC SP_EXECUTESQL @query
END
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value] FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]'												     
											     																		    
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0
    )
AS 
    BEGIN
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,1  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,72  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,1,1  -> Item Images
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 31,2,1,1  -> Item Assembly
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,0,1  -> Item Vendor
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 36,2,1,1  -> Organization 
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 43,2,1,1  -> Organization Correspondence
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,1,1  -> Item WareHouse
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 98,2,1,1  -> Address Details
		
        IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
            BEGIN

                SELECT  20 AS [intImportMasterID],
                        'Item' AS [vcImportName]
                UNION ALL
                SELECT  48 AS [intImportMasterID],
                        'Item WareHouse' AS [vcImportName]
                UNION ALL
                SELECT  31 AS [intImportMasterID],
                        'Kit sub-items' AS [vcImportName]
                UNION ALL
                SELECT  54 AS [intImportMasterID],
                        'Item Images' AS [vcImportName]
                UNION ALL
                SELECT  55 AS [intImportMasterID],
                        'Item Vendor' AS [vcImportName]
                --UNION ALL
                --SELECT  65 AS [intImportMasterID],
                --        'Item Web API Details' AS [vcImportName]
                UNION ALL
                SELECT  133 AS [intImportMasterID],
                        'Organization' AS [vcImportName]
                UNION ALL
				SELECT  134 AS [intImportMasterID],
                        'Contact' AS [vcImportName]
                UNION ALL
                SELECT  43 AS [intImportMasterID],
                        'Organization Correspondence' AS [vcImportName]                        
                UNION ALL
                SELECT  98 AS [intImportMasterID],
                        'Address Details' AS [vcImportName]              		
            END
	
        IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
            BEGIN
	
		
                SELECT DISTINCT
                        intSectionID AS [Id],
                        vcSectionName AS [Data]
                FROM    dbo.DycFormSectionDetail
                WHERE   numFormID = @numFormID			
		
		
		/*SELECT DISTINCT  DMAP.intSectionID AS [Id], 
						 DFSD.vcSectionName AS [Data]					 
		FROM dbo.DycFormField_Mapping DMAP 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.intSectionID = DMAP.intSectionID
											   AND DFSD.numFormID = DMAP.numFormID
		WHERE DMAP.numFormID = @numFormID */
		
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    DCOL.tintOrder
                          FROM      dbo.View_DynamicDefaultColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(DCOL.bitDeleted, 0) = 0
                          UNION ALL
                          SELECT    -1,
                                    CAST(CFm.Fld_id AS NUMERIC(18,0)),
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    100 AS [tintOrder]
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
                        ) X
                ORDER BY X.tintOrder,
                        X.intSectionID        		
        
        
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    ISNULL(DCOL.tintColumn, 0) AS SortOrder,
                                    ISNULL(DCOL.tintRow,0) AS tintRow
                          FROM      dbo.View_DynamicColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(tintPageType, 0) = 4
                          UNION ALL
                          SELECT    -1,
                                    CAST(CFm.Fld_id AS NUMERIC(18,0)),
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    DFCD.intColumnNum AS [SortOrder],
                                    ISNULL(DFCD.intRowNum,0) AS tintRow
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                                    JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id
                                                                             AND DFCD.numFormID = @numFormID
                                                                             AND DFCD.bitCustom = 1
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND DFCD.numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
                                    AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)                          
                        ) X
                ORDER BY X.SortOrder
                        ,X.intSectionID 
            END	
    END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM dbo.Import_File_Master
-- USP_GET_IMPORT_MAPPED_DATA 39,1,48
CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_DATA] 
(
	@intImportFileID	BIGINT,
	@numDomainID		NUMERIC(18,0),
	@numFormID			NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWHERE AS VARCHAR(MAX)
	
	SET @strWHERE = ' WHERE 1=1 '
	
	IF ISNULL(@intImportFileID,0) <> 0
		BEGIN
			SET @strWHERE = @strWHERE + ' AND PARENT.intImportFileID = ' + CONVERT(VARCHAR,@intImportFileID)
		END
		
		SET @strSQL = 'SELECT   PARENT.intImportFileID AS [ImportFileID],
			   					CONCAT((CASE PARENT.tintImportType WHEN 1 THEN ''Update'' WHEN 2 THEN ''Import'' ELSE ''Import/Update'' END),'' ('',IMPORT.vcFormName,'')'') AS [ImportType],
								PARENT.vcImportFileName AS [ImportFileName],
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''Pending ''
									WHEN PARENT.tintStatus = 1 THEN ''Completed''
									WHEN PARENT.tintStatus = 2 THEN ''Pending''	
									WHEN PARENT.tintStatus = 3 THEN ''Incomplete''	
								END) AS [Status],								
								dbo.FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS [CreatedDate],
								dbo.fn_GetContactName(PARENT.numUserContactID) AS [CreatedBy] ,
								ISNULL(
										CASE WHEN ISNULL(tintStatus,0) = 1
											 THEN CONCAT((CASE WHEN ISNULL(numRecordAdded,0) <> 0 THEN CAST(numRecordAdded AS VARCHAR) + '' records added. '' ELSE '''' END),
												  (CASE WHEN ISNULL(numRecordUpdated,0) <> 0 THEN CAST(numRecordUpdated AS VARCHAR) + '' records updated. '' ELSE '''' END) ,
												  (CASE WHEN ISNULL(numErrors,0) <> 0 THEN ''Error occured in '' + CAST(numErrors AS VARCHAR) + '' records. '' ELSE '''' END))
												 
											 ELSE	''''
												  
											END	  	   
								,''-'') AS [Records],
								PARENT.tintStatus AS [intStatus],
								PARENT.numMasterID AS [MasterID],
								ISNULL(HISTORY.vcHistory_Added_Value,'''') AS [ItemCodes],
								ISNULL(CASE WHEN (ISNULL(tintStatus,0) = 1 OR ISNULL(tintStatus,0) = 3)
											THEN 
											     CASE WHEN ISNULL(numRecordAdded,0) > 0 AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          ELSE 0
												 END
									   END,0)	AS [IsForRollback]
									   ,ISNULL(PARENT.tintItemLinkingID,0) tintItemLinkingID,tintImportType
						FROM dbo.Import_File_Master PARENT 
						INNER JOIN dbo.DynamicFormMaster IMPORT ON PARENT.numMasterID = IMPORT.numFormId
						LEFT JOIN dbo.Import_History HISTORY ON HISTORY.intImportFileID = PARENT.intImportFileID '
						+ @strWHERE + ' AND PARENT.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) +
						' ORDER BY PARENT.intImportFileID DESC'
		PRINT @strSQL
		EXEC SP_EXECUTESQL @strSQL
			
		-------------------------------------------------------------------------------------------------------------
		
		SELECT * FROM 
		(SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID]
		FROM dbo.View_DynamicDefaultColumns PARENT  
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.numDomainID = PARENT.numDomainID
		WHERE IFFM.intImportFileID = @intImportFileID
		  AND PARENT.numDomainID = @numDomainID
		  AND numFormID = @numFormID
	
		UNION ALL
		
		SELECT DISTINCT IFFM.numFormFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CFW.Fld_label AS [dbFieldName],	 
						CFW.Fld_label AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE CFW.fld_Type
						END	 vcAssociatedControlType,
						0 AS bitDefault,
						'R' AS vcFieldType,
						'' AS vcListItemType,
						'' AS  vcLookBackTableName,
						CFW.Fld_label AS [vcOrigDbColumnName],
						'' AS vcPropertyName,
						1 AS [bitCustomField],
						0
						--IFM.numDomainID AS [DomainID]
		FROM dbo.Import_File_Field_Mapping IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFormFieldID = CFW.Fld_id 
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE 1=1
		  --AND DFSD.numDomainID = CFW.numDomainID
		  AND IFFM.intImportFileID = @intImportFileID
		  AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
		  AND Grp_id = DFSD.Loc_Id ) TABLE1
		ORDER BY intImportFileID DESC 
		
	/*
	SELECT * FROM Import_File_Field_Mapping		
	SELECT * FROM Import_File_Master
	SELECT * FROM View_DynamicDefaultColumns	
	SELECT * FROM CFW_Fld_Master
	*/
	/*
		SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID] 
		FROM View_DynamicDefaultColumns PARENT
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.Grp_id = PARENT.numFormId AND CFW.numDomainID = PARENT.numDomainID
		WHERE numFormID = 48 AND PARENT.numDomainID = 1 AND IFFM.intImportFileID = 2 */	
		
--		SELECT  CHILD.numFormFieldID AS [FormFieldID],
--				CHILD.intMapColumnNo AS [ImportFieldID],
--				CHILD.intImportFileID,
--				CHILD.intImportTransactionID,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcDbColumnName
--				END AS [dbFieldName],	 
--				--DYNAMIC_FIELD.vcDbColumnName AS [dbFieldName],
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcFormFieldName
--				END AS [FormFieldName],	 
--				--DYNAMIC_FIELD.vcFormFieldName AS [FormFieldName],
--				CASE WHEN CFW.fld_Type = 'Drop Down List Box'
--					 THEN 'SelectBox'
--					 ELSE ISNULL(DYNAMIC_FIELD.vcAssociatedControlType,'') 
--				END	 vcAssociatedControlType,
--				ISNULL(DYNAMIC_FIELD.bitDefault,0) bitDefault,
--				ISNULL(DYNAMIC_FIELD.vcFieldType,'') vcFieldType,
--				ISNULL(DYNAMIC_FIELD.vcListItemType,'') vcListItemType,
--				ISNULL(DYNAMIC_FIELD.vcLookBackTableName,'') vcLookBackTableName,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcOrigDbColumnName
--				END AS [vcOrigDbColumnName],
--				ISNULL(DYNAMIC_FIELD.vcPropertyName,'') vcPropertyName,
--				ISNULL(bitCustomField,0) AS [bitCustomField],
--			    PARENT.numDomainID AS [DomainID]
--		FROM Import_File_Field_Mapping CHILD 
--		INNER JOIN dbo.Import_File_Master PARENT ON PARENT.intImportFileID = CHILD.intImportFileID
--		INNER JOIN dbo.Import_Master IMPORT ON IMPORT.intImportMasterID = PARENT.numMasterID
--		LEFT JOIN DynamicFormFieldMaster DYNAMIC_FIELD ON CHILD.numFormFieldID = DYNAMIC_FIELD.numFormFieldId 
--		LEFT JOIN dbo.DycFormField_Mapping DYNAMIC_FIELD_CHILD ON CHILD.numFormFieldID = DYNAMIC_FIELD_CHILD.numFieldId 
--		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD_CHILD.= DYNAMIC_FIELD
--		LEFT JOIN dbo.CFW_Fld_Master CFW ON CHILD.numFormFieldID = CFW.Fld_id
--		WHERE CHILD.intImportFileID = @intImportFileID

END		
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadList1]    Script Date: 09/08/2010 14:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Created By Anoop Jayaraj                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist1')
DROP PROCEDURE usp_getleadlist1
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList1]                                                                          
 @numGrpID numeric,                                                                              
 @numUserCntID numeric,
 @tintUserRightType tinyint,
 @CustName varchar(100)='',                                                                              
 @FirstName varChar(100)= '',                                                                              
 @LastName varchar(100)='',                                                                            
 @SortChar char(1)='0' ,                                                                            
 @numFollowUpStatus as numeric(9)=0 ,                                                                            
 @CurrentPage int,                                                                            
 @PageSize int,                                                                            
 @columnName as Varchar(50)='',                                                                            
 @columnSortOrder as Varchar(10)='',                                                    
 @ListType as tinyint=0,                                                    
 @TeamType as tinyint=0,                                                     
 @TeamMemID as numeric(9)=0,                                                
 @numDomainID as numeric(9)=0    ,                  
 @ClientTimeZoneOffset as int  ,              
 @bitPartner as bit,
 @bitActiveInActive as bit,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                                                  
AS       

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
END
ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
END
                                                                             
declare @strSql as varchar(MAX)                                                                             


declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                           
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                            
 
  DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                                                                                           
                                                              
 set @strSql= '
DECLARE @numDomain NUMERIC
SET @numDomain='+ convert(varchar(15),@numDomainID)+';

 with tblLeads as ( SELECT     '         
 set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@columnName)>0 THEN ' CONVERT(VARCHAR(Max),' + @columnName + ')'  WHEN @columnName='numRecOwner' THEN 'DM.numRecOwner' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID    
   left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId ##JOIN##                                           
   
 WHERE                                                                               
   DM.tintCRMType=0  and  (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL)
  AND CMP.numDomainID=DM.numDomainID                                                          
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                                 
 AND DM.numDomainID=@numDomain'               
      
SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive) 
        
if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+               
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '              
                                                                                    
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                            
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                            
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                                           
                                                                                   
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                            
if @tintUserRightType=1 AND @numGrpID<>2 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')' 


if @ListType=0 -- when MySelf radiobutton is checked
begin                                                    
IF @tintUserRightType=1 AND @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                
if @tintUserRightType=1 AND @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                     
end                                          
                                      
else if @ListType=1 -- when Team Selected radiobutton is checked
begin                                 
if @TeamMemID=0 -- When All team members is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+'))' + ' or ' + @strShareRedordWith +')'                                                                       
                                                   
 end                                                  
else-- When All team member name is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) + ' or ' + @strShareRedordWith +')'                                                         
                                                   
 end                                                   
end

IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria

  set @strSql=@strSql+')'
--set @strSql=@strSql+'), AddUnreadMailCount AS 
--             ( SELECT ISNULL(VIE.Total,0) AS TotalEmail,t.* FROM tblLeads t left JOIN View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = @numDomain AND  VIE.numContactId = t.numContactId
--                            )
--             , AddActionCount AS
--             ( SELECT    ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem,U.* FROM AddUnreadMailCount U LEFT JOIN  VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = U.numDivisionID 
--                            '                                  
                                                                           
declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)                  
set @tintOrder=0                                                
set @WhereCondition =''               
                 
declare @Nocolumns as tinyint              
--declare @DefaultNocolumns as tinyint         
set @Nocolumns=0              
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1) TotalRows
 

--Set @DefaultNocolumns=  @Nocolumns
  

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      


--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 34 AND numRelCntType=1 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
	BEGIN
					INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 34,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=34 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

	END

	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END        
Declare @ListRelID as numeric(9)  
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
 


while @tintOrder>0                                                
begin                                                
     
     if @bitCustomField = 0            
 begin             
     declare @Prefix as varchar(5)                  
     if @vcLookBackTableName = 'AdditionalContactsInformation'                  
   set @Prefix = 'ADC.'                  
     if @vcLookBackTableName = 'DivisionMaster'                  
   set @Prefix = 'DM.'       
   
          SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                                         
 IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strSql=@strSql+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END
			ELSE if @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                                                    
        begin                                                    
     
	IF @vcDbColumnName = 'numPartenerSource'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
	END
	IF @vcDbColumnName = 'numPartenerContact'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerContact) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=DM.numPartenerContact) AS vcPartenerContact' 
	END
      
     if @vcListItemType='LI'                                                     
     begin                                                    
		 IF @numListID=40 
		 BEGIN
			
				SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
				IF @vcDbColumnName='numShipCountry'
				BEGIN
					SET @WhereCondition = @WhereCondition
										+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
										+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
				END
				ELSE
				BEGIN
					SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
				END


		 END
		 ELSE
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                   
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName		 	
		 END
     end                                                    
     else if @vcListItemType='S'                                                     
     begin        
			IF @vcDbColumnName='numShipState'
			BEGIN
				SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
					+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
			END
			ELSE IF @vcDbColumnName='numBillState'
			BEGIN
				SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
					+ ' left Join State BillState on BillState.numStateID=AD4.numState '
			END
			ELSE  
			BEGIN
				SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
			END		                                            
     end                                                    
     else if @vcListItemType='T'                                                     
     begin                                                    
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end  
      else if @vcListItemType='AG'                                                     
     begin                                                    
      set @strSql=@strSql+',AG'+ convert(varchar(3),@tintOrder)+'.vcGrpName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join Groups AG'+ convert(varchar(3),@tintOrder)+ ' on AG'+convert(varchar(3),@tintOrder)+ '.numGrpId=DM.'+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='DC'                                                     
     begin                                                    
      set @strSql=@strSql+',DC'+ convert(varchar(3),@tintOrder)+'.vcECampName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ECampaign DC'+ convert(varchar(3),@tintOrder)+ ' on DC'+convert(varchar(3),@tintOrder)+ '.numECampaignID=ADC.'+@vcDbColumnName                                                    
     end                   
     else if   @vcListItemType='U'                                                 
    begin                   
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
    end                  
    end                                                    
      else if @vcAssociatedControlType='DateField'                                                  
 begin            
       
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
   end    
          
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                 
begin    
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when    
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'              
       
 end 
 else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end       
 else                                               
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
 end          
                
end            
else if @bitCustomField = 1            
begin            
               
     

   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
            
   print @vcAssociatedControlType   
   print @numFieldId           
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin            
               
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end  
   else if @vcAssociatedControlType = 'CheckBox'         
   begin            
               
    --set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
    set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'DateField'        
   begin            
               
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'           
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                     
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end   
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END        
   BEGIN
		SET @strSql = @strSql + CONCAT(',dbo.GetCustFldValue(',@numFieldId,',1,DM.numDivisionID)') + ' [' + @vcColumnName + ']'
	END                 
end      
            
    
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
        
    
          
end                     
 
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=34 AND DFCS.numFormID=34 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  set @strSql=@strSql+' ,TotalRowCount
 ,ISNULL(ADC.numECampaignID,0) as numECampaignID
 ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
 ,ISNULL(VIE.Total,0) as TotalEmail,
 ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
   left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId                                               
   join tblLeads T on T.numDivisionID=DM.numDivisionID   '+@WhereCondition


SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
		--set @strSql=@strSql+' left join #tempColorScheme tCS on convert(varchar(11),DateAdd(day,tCS.vcFieldValue,GETUTCDATE()))=' + @Prefix + 'convert(varchar(11),DateAdd(day,tCS.vcFieldValue,' + @vcCSOrigDbCOlumnName + '))'
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111) 
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

 set @strSql=@strSql+' WHERE                                               
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1 AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'
                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNamedPriceLevel')
DROP PROCEDURE USP_GetNamedPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_GetNamedPriceLevel]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	IF OBJECT_ID('tempdb..#PriceTbl') IS NOT NULL DROP TABLE #PriceTbl
		
	SELECT DISTINCT ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
	INTO #PriceTbl
	FROM [PricingTable] pt
	INNER JOIN Item
	ON Item.numItemCode = pt.numItemCode 
	AND Item.numDomainID = @numDomainID
	WHERE tintRuleType = 3
	ORDER BY [Id] 

	SELECT pt.Id
			,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
	FROM #PriceTbl pt
	LEFT JOIN PricingNamesTable pnt
	ON pt.Id = pnt.tintPriceLevel
	AND pnt.numDomainID = @numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceNamesTable')
DROP PROCEDURE dbo.USP_GetPriceNamesTable
GO
CREATE PROCEDURE [dbo].[USP_GetPriceNamesTable]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT  
		[pricingNamesID],
        [numDomainID],
        [tintPriceLevel],
 		ISNULL(vcPriceLevelName,'') vcPriceLevelName
    FROM 
		[PricingNamesTable]
	WHERE 
		numDomainID = @numDomainID
END
--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceTable')
DROP PROCEDURE USP_GetPriceTable
GO
CREATE PROCEDURE USP_GetPriceTable
   @numPriceRuleID NUMERIC,
    @numItemCode AS NUMERIC 
	,@numDomainID AS NUMERIC
AS 
BEGIN

	WITH PricingCte AS 
	(
		SELECT  pt.[numPricingID],
		pt.[numPriceRuleID],
		pt.[intFromQty],
		pt.[intToQty],
		pt.[tintRuleType],
		pt.[tintDiscountType],
		CAST(CONVERT(DECIMAL(18, 4), pt.[decDiscount]) AS VARCHAR) decDiscount,
		ROW_NUMBER() OVER(ORDER BY [numPriceRuleID]) as rowId
		FROM    [PricingTable] pt
		WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode
	)
	SELECT  [numPricingID]
	,[numPriceRuleID]
	,[intFromQty]
	,[intToQty]
	,[tintRuleType]
	,[tintDiscountType]
	,decDiscount
	,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName]
	FROM PricingCte
	FULL JOIN PricingNamesTable pnt ON pnt.tintPriceLevel = rowId
	WHERE rowId IS NOT NULL OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY [numPriceRuleID] 

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                              
--- Modified By Gangadhar 03/05/2008    
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getprospectslist1' ) 
    DROP PROCEDURE usp_getprospectslist1
GO
CREATE PROCEDURE [dbo].[USP_GetProspectsList1]
    @CRMType NUMERIC,
    @numUserCntID NUMERIC,
    @tintUserRightType TINYINT,
    @tintSortOrder NUMERIC = 4,
    @numDomainID AS NUMERIC(9) = 0,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numProfile AS NUMERIC,
    @bitPartner AS BIT,
    @ClientTimeZoneOffset AS INT,
	@bitActiveInActive as bit,
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='' ,
	@SearchText VARCHAR(100) = '',
	 @TotRecs int output 
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)
    
	DECLARE @Nocolumns AS TINYINT = 0
    
	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3
	) TotalRows


	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 35 AND numRelCntType=3 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
			select 35,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,3,1,0,intColumnWidth
			 FROM View_DynamicDefaultColumns
			 where numFormId=35 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
			order by tintOrder asc   
		END    
       
		INSERT INTO 
			#tempForm
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
        WHERE 
			numFormId = 35
			AND numUserCntID = @numUserCntID
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND ISNULL(bitSettingField, 0) = 1
			AND ISNULL(bitCustom, 0) = 0
        UNION
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormId = 35
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND numRelCntType = 3
            AND ISNULL(bitCustom, 0) = 1
        ORDER BY 
			tintOrder ASC  
	END  

		

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=''

	 SET @strColumns =' ISNULL(ADC.numContactId,0) numContactId, ISNULL(DM.numDivisionID,0) numDivisionID, ISNULL(DM.numTerID,0)numTerID, ISNULL(ADC.numRecOwner,0) numRecOwner, ISNULL(DM.tintCRMType,0) AS tintCRMType, CMP.vcCompanyName '              

	DECLARE @tintOrder AS TINYINT                                                      
    DECLARE @vcFieldName AS VARCHAR(50)                                                      
    DECLARE @vcListItemType AS VARCHAR(3)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
    DECLARE @numListID AS NUMERIC(9)                                                      
    DECLARE @vcDbColumnName VARCHAR(20)                          
    DECLARE @WhereCondition VARCHAR(MAX)                           
    DECLARE @vcLookBackTableName VARCHAR(2000)                    
    DECLARE @bitCustom AS BIT
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)              
    DECLARE @vcColumnName AS VARCHAR(500)  
    SET @tintOrder = 0                                                      
    SET @WhereCondition = ''
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
    FROM    #tempForm
    ORDER BY tintOrder ASC            
 
                                               
    WHILE @tintOrder > 0  
    BEGIN
            IF @bitCustom = 0 
                BEGIN            
                    DECLARE @Prefix AS VARCHAR(5)                        
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'                        
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'  

					SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
					BEGIN
						SET @WhereCondition = @WhereCondition
														+ CONCAT(' OUTER APPLY(SELECT
																			SUM(monDealAmount) AS monDealAmount
																			,AVG(ProfitPer) AS fltProfitPer
																		FROM
																		(
																			SELECT
																				OM.numDivisionId
																				,monDealAmount
																				,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																			FROM
																				OpportunityMaster OM
																			INNER JOIN
																				Domain D
																			ON
																				OM.numDomainID = D.numDomainID
																			INNER JOIN
																				OpportunityItems OI
																			ON
																				OM.numOppId = OI.numOppId
																			INNER JOIN
																				Item I
																			ON
																				OI.numItemCode = I.numItemCode
																			LEFT JOIN
																				Vendor V
																			ON
																				V.numVendorID = I.numVendorID
																				AND V.numItemCode = I.numItemCode
																			WHERE
																				OM.numDomainId = ',@numDomainID,'
																				AND OM.numDivisionId = DM.numDivisionID
																				AND ISNULL(I.bitContainer,0) = 0
																				AND tintOppType = 1
																				AND tintOppStatus=1
																				AND 1 = (CASE ', @tintPerformanceFilter,' 
																						WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						ELSE 1
																						END)
																			GROUP BY
																				OM.numDivisionId
																				,OM.numOppId
																				,monDealAmount
																		) T1
																		GROUP BY
																			T1.numDivisionId ) AS TempPerformance ')

						set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
					END
					ELSE IF @vcDbColumnName='bitEcommerceAccess'
					BEGIN
						SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
					END
                    ELSE IF @vcAssociatedControlType = 'SelectBox' or @vcAssociatedControlType='ListBox'     
                        BEGIN                                                          
                            IF @vcDbColumnName = 'numPartenerSource'
							BEGIN
								SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
								SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
							END
							ELSE IF @vcListItemType = 'LI' 
                                BEGIN    
                                    IF @numListID = 40 
                                        BEGIN
                                            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipCountry'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
											END
											ELSE
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
											END
                                        END
                                    ELSE 
                                        BEGIN                                                      
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											                                              
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                END                                                          
                            ELSE 
                                IF @vcListItemType = 'S' 
                                    BEGIN     
											
											                                                           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State BillState on BillState.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END
                                                 
                                                                                          
                                    END                                                          
                                ELSE 
                                    IF @vcListItemType = 'T' 
                                        BEGIN                                                          
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'     
												
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END  
											                                                     
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'C' 
                                            BEGIN                                                    
                                                SET @strColumns = @strColumns + ',C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcCampaignName' + ' ['
                                                    + @vcColumnName + ']'    
												
												IF LEN(@SearchText) > 0
												BEGIN 
												SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
												END  
												                                                
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left Join CampaignMaster C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numCampaignId=DM.'
                                                    + @vcDbColumnName                                                    
                                            END                       
                                        ELSE 
                                            IF @vcListItemType = 'U' 
                                                BEGIN                         
                            
                              
                                                    SET @strColumns = @strColumns
                                                        + ',dbo.fn_GetContactName('
                                                        + @Prefix
                                                        + @vcDbColumnName
                                                        + ') ['
                                                        + @vcColumnName + ']'  
														
													IF LEN(@SearchText) > 0
													  BEGIN 
														SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
													  END       
													                                                        
                                                END                        
                        END       
      
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN              
                                SET @strColumns = @strColumns
                                    + ',case when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
                                SET @strColumns = @strColumns
                                    + 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName + '),'
                                    + CONVERT(VARCHAR(10), @numDomainId)
                                    + ') end  [' + @vcColumnName + ']'    
									
								
								IF LEN(@SearchText) > 0
								BEGIN
									SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
								END
								
								              
                            END      
            
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox'
                                OR @vcAssociatedControlType = 'Label' 
                                BEGIN      
                                    SET @strColumns = @strColumns + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'    
										  
										  
									IF LEN(@SearchText) > 0
									BEGIN
										SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
										CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END
										+' LIKE ''%' + @SearchText + '%'' '
									END  		  
										              
         
                                END 
                          else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end         
                            ELSE 
                                BEGIN    
                                    SET @strColumns = @strColumns + ', '
                                        + @vcDbColumnName + ' ['
                                        + @vcColumnName + ']'              
       
                                END                                    
                   
                END                  
            ELSE 
                IF @bitCustom = 1 
                    BEGIN                  
      
						SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
 
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                                 
                        IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'   
                            BEGIN                              
                                SET @strColumns = @strColumns + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcColumnName + ']'                     
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=DM.numDivisionId   '                                                           
                            END    
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox'  
                                BEGIN              
set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'        
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=DM.numDivisionId   '                                                     
                                END       
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField'  
                                    BEGIN                  
                     
                                        SET @strColumns = @strColumns
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcColumnName + ']'                     
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=DM.numDivisionId   '                                                           
                                    END                  
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox'
                                        BEGIN                   
                                            SET @vcDbColumnName = 'DCust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                  
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'                                                            
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                   
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=DM.numDivisionId    '                                                           
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                  
                                        END      
								ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
								BEGIN
									SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

								SET @WhereCondition= @WhereCondition 
													+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
													+ ' on CFW' + convert(varchar(3),@tintOrder) 
													+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
													+ 'and CFW' + convert(varchar(3),@tintOrder) 
													+ '.RecId=DM.numDivisionId '
								END                       
                    END    
                    

		  select top 1 
		   @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
		  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc
            IF @@rowcount = 0 
                SET @tintOrder = 0 
    END  

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   

		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql='	FROM  CompanyInfo CMP                                       
						join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
						left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
						left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID =  '+ convert(varchar(15),@numDomainID )+' AND  VOA.numDivisionId = DM.numDivisionID 
						left join View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = '+ convert(varchar(15),@numDomainID )+' AND  VIE.numContactId = ADC.numContactId '
		IF @tintSortOrder = 9
		BEGIN
			SET @StrSql=@StrSql+ ' join Favorites F on F.numContactid=DM.numDivisionID '
		END 

		SET @StrSql=@StrSql + ISNULL(@WhereCondition,'')

		 -------Change Row Color-------
		Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
		SET @vcCSOrigDbCOlumnName=''
		SET @vcCSLookBackTableName=''

		Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

		insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
		from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
		join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
		where DFCS.numDomainID=@numDomainID and DFFM.numFormID=35 AND DFCS.numFormID=35 and isnull(DFFM.bitAllowGridColor,0)=1

		IF(SELECT COUNT(*) FROM #tempColorScheme)>0
		BEGIN
		   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
		END   
		----------------------------                        
 
		 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
		set @strColumns=@strColumns+' ,tCS.vcColorScheme '
		END

		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
				set @Prefix = 'ADC.'                  
			 if @vcCSLookBackTableName = 'DivisionMaster'                  
				set @Prefix = 'DM.'
			 if @vcCSLookBackTableName = 'CompanyInfo'                  
				set @Prefix = 'CMP.'   
 
			IF @vcCSAssociatedControlType='DateField'
			BEGIN
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
			END
			ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
			BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
			END
		END

		IF @columnName LIKE 'CFW.Cust%' 
        BEGIN                   
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                           
            SET @columnName = 'CFW.Fld_Value'                  
        END                                           
		ELSE IF @columnName LIKE 'DCust%' 
        BEGIN                                                      
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @columnName = 'LstCF.vcData' 
        END        

		IF @columnName = 'CMP.vcPerformance'   
		BEGIN
			SET @columnName = 'TempPerformance.monDealAmount'
		END

		SET @StrSql = @StrSql + ' WHERE (numCompanyType = 0 OR numCompanyType=46) 
									AND (ISNULL(ADC.bitPrimaryContact,0) = 1 OR ADC.numContactID IS NULL)
									AND CMP.numDomainID=DM.numDomainID   
									AND DM.tintCRMType= ' + CONVERT(VARCHAR(2), @CRMType) + ' AND DM.numDomainID = ' + CONVERT(VARCHAR(20),@numDomainID)


		IF @tintUserRightType = 1 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
		END
		ELSE IF @tintUserRightType = 2 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
		END	

		IF @SortChar <> '0' 
		BEGIN
			SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'' '                                                               
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                     
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
					SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END
		ELSE
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                   
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
        SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
			BEGIN
				-- WE ARE MANAGING CONDITION IN OUTER APPLY
				set @strSql=@strSql
			END
			ELSE
				set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
		END


		IF @vcCustomSearchCriteria <> '' 
			SET @strSql=@strSql + ' AND ' + @vcCustomSearchCriteria
                                            


		
		DECLARE @firstRec AS INTEGER                                                              
		DECLARE @lastRec AS INTEGER                                                              
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )     
		
		DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
		exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT                                                                   
       
		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #tempColorScheme 
		 
END	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRecordByLinkingID')
DROP PROCEDURE dbo.USP_GetRecordByLinkingID
GO
CREATE PROCEDURE [dbo].[USP_GetRecordByLinkingID]
(
	@numDomainID NUMERIC(18,0),
	@intImportType NUMERIC(18,0),
    @tintLinkingID TINYINT,
	@vcValue AS VARCHAR(500)
)
AS
BEGIN
	DECLARE @ItemCount INT

	If @intImportType = 20 -- Item
	BEGIN
		IF @tintLinkingID = 1 --ItemCode
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
		ELSE IF @tintLinkingID = 2 --SKU
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcSKU)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_SKU',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcSKU)= LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 3 --UPC
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(numBarCodeId)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_UPC',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_UPC',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND LOWER(numBarCodeId)=LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 4 --ModelID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcModelID)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_ModelID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ModelID',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcModelID)= LOWER(@vcValue)
			END
		END
	END
	ELSE IF @intImportType = 133 -- Organization
	BEGIN
		IF @tintLinkingID = 1 -- Non Biz Company ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizCompanyID)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numDivisionID,numCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizCompanyID)= LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 2 -- Organization ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numDivisionID,numCompanyID  FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
	END
	ELSE IF @intImportType = 134 -- Contact
	BEGIN
		IF @tintLinkingID = 1 -- Non Biz Contact ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM ImportActionItemReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizContactID)=@vcValue

			IF @ItemCount = 0
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numContactID,numDivisionID,numCompanyID  FROM ImportActionItemReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizContactID)=@vcValue
			END
		END
		ELSE IF @tintLinkingID = 2 -- Contact ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
	END
	
END
/****** Object:  StoredProcedure [dbo].[usp_GetUpdatableFieldList]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                          
--Purpose: Returns the list of fields from the database that can be mass updated            
--Created Date: 18/03/2006            
                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getupdatablefieldlist')
DROP PROCEDURE usp_getupdatablefieldlist
GO
CREATE PROCEDURE [dbo].[usp_GetUpdatableFieldList]            
 @numDomainID numeric (9),
 @numFormID NUMERIC = 1
AS   

IF @numFormID = 1 
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID
	 UNION  
		 select Fld_label,'Custom~'+case when grp_id=1 then 'Division~LI~' when grp_id=4 then 'Contacts~LI~' end + convert(varchar(10),isnull(numlistid,0))+'~'+Case Fld_type WHEN 'SelectBox' THEN 'N~' ELSE 'V~' END + Fld_type +'~'+convert(varchar(10),fld_id) +'~' as value  
		 from  CFW_Fld_Master  
		 where (Grp_id=1 or Grp_id=4) and fld_type<>'link' and numDomainID=@numDomainID
	 UNION  
		select 'Organization Record Owner','Ownership~Division~~~~SelectBox~~~' as value  
	 UNION  
		select 'Contact Record Owner','Ownership~Contacts~~~~SelectBox~~~' as value	
	 UNION  
		select 'Drip Campaign','DripCampaign~Contacts~~~~SelectBox~~~' as value	
END         
ELSE IF @numFormID = 15
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID and vcOrigDbColumnName!='numContactType'
END 
ELSE IF @numFormID = 29 -- adv search (item search)
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID AND vcLookBackTableName <> 'WareHouseItmsDTL'
		 AND vcDbColumnName <> 'vcPartNo' AND vcDbColumnName <> 'monAverageCost'
		 AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
UNION 
		 SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ ISNULL(vcListItemType,'') +'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and numFormID=29 AND vcLookBackTableName = 'WareHouseItems'
		  AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
Union 
		 SELECT 'Tax : ' + isnull(vcTaxName,'') as vcFormFieldName, 'Tax~Tax~~0~Y~CheckBox~'+convert(varchar(10),numTaxItemId)+'~' as Value        
		 FROM TaxItems            
		 WHERE numDomainID=@numDomainID 
END

 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetContactDetails')
DROP PROCEDURE USP_Import_GetContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
AS  
BEGIN
	SELECT
		numDivisionId
		,numContactID
		,vcFirstName
		,vcLastName
		,vcEmail
		,numContactType
		,numPhone
		,numPhoneExtension
		,numCell
		,numHomePhone
		,vcFax
		,vcCategory
		,numTeam
		,vcPosition
		,vcTitle
		,vcDepartment
		,txtNotes
		,ISNULL((SELECT TOP 1 numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND numContactId=@numContactID),'') vcNonBizContactID
		,bitPrimaryContact
	FROM
		AdditionalContactsInformation
	WHERE
		numDomainID=@numDomainID
		AND numContactId=@numContactID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetItemDetails')
DROP PROCEDURE USP_Import_GetItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS  
BEGIN
	SELECT
		Item.numItemCode
		,Item.numShipClass
		,Item.vcItemName
		,Item.monListPrice
		,Item.txtItemDesc
		,Item.vcModelID
		,Item.vcManufacturer
		,Item.numBarCodeId
		,Item.fltWidth
		,Item.fltHeight
		,Item.fltWeight
		,Item.fltLength
		,Item.monAverageCost
		,Item.bitSerialized
		,Item.bitKitParent
		,Item.bitAssembly
		,Item.IsArchieve
		,Item.bitLotNo
		,Item.bitAllowBackOrder
		,Item.numIncomeChartAcntId
		,Item.numAssetChartAcntId
		,Item.numCOGsChartAcntId
		,Item.numItemClassification
		,Item.numItemGroup
		,Item.numPurchaseUnit
		,Item.numSaleUnit
		,Item.numItemClass
		,Item.numBaseUnit
		,Item.vcSKU
		,Item.charItemType
		,Item.bitTaxable
		,ItemExtendedDetails.txtDesc
		,Item.vcExportToAPI
		,Item.bitAllowDropShip
		,Item.bitMatrix
	FROM
		Item
	LEFT JOIN
		ItemExtendedDetails
	ON
		Item.numItemCode = ItemExtendedDetails.numItemCode
	WHERE
		numDomainID = @numDomainID
		AND Item.numItemCode = @numItemCode
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetOrganizationDetails')
DROP PROCEDURE USP_Import_GetOrganizationDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetOrganizationDetails]  
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
AS  
BEGIN
	SELECT
		CI.vcCompanyName
		,CI.vcProfile
		,CI.numCompanyType
		,CI.numNoOfEmployeesId
		,CI.numAnnualRevID
		,CI.vcWebSite
		,CI.vcHow
		,CI.numCompanyIndustry
		,CI.numCompanyRating
		,CI.numCompanyCredit
		,CI.txtComments
		,CI.vcWebLink1
		,CI.vcWebLink2
		,CI.vcWebLink3
		,CI.vcWebLink4
		,DM.numCompanyDiff
		,DM.vcCompanyDiff
		,DM.numCampaignID
		,DM.vcComPhone
		,DM.numAssignedTo
		,DM.numFollowUpStatus
		,DM.numTerID
		,DM.numStatusID
		,DM.vcComFax
		,DM.bitPublicFlag
		,DM.bitActiveInActive
		,DM.numDivisionID
		,DM.numGrpID
		,DM.tintCRMType
		,DM.numCurrencyID
		,DM.bitNoTax
		,DM.numBillingDays
		,DM.bitOnCreditHold
		,DM.numDefaultExpenseAccountID
		,ISNULL((SELECT TOP 1 numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID),'') vcNonBizCompanyID
	FROM
		DivisionMaster DM
	INNER JOIN 
		CompanyInfo CI
	ON
		DM.numCompanyID=CI.numCompanyId
	WHERE
		DM.numDomainID=@numDomainID
		AND DM.numDivisionID=@numDivisionID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveContactDetails')
DROP PROCEDURE USP_Import_SaveContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcFirstName VARCHAR(50)
	,@vcLastName VARCHAR(50)
	,@vcEmail VARCHAR(80)
	,@numContactType NUMERIC(18,0)
	,@numPhone VARCHAR(15)
	,@numPhoneExtension VARCHAR(7)
	,@numCell VARCHAR(15)
	,@numHomePhone VARCHAR(15)
	,@vcFax VARCHAR(15)
	,@vcCategory NUMERIC(18,0)
	,@numTeam NUMERIC(18,0)
	,@vcPosition NUMERIC(18,0)
	,@vcTitle VARCHAR(100)
	,@vcDepartment NUMERIC(18,0)
	,@txtNotes NVARCHAR(MAX)
	,@bitIsPrimaryContact BIT
	,@vcStreet VARCHAR(100)
	,@vcCity VARCHAR(50)
	,@numState NUMERIC(18,0)
	,@numCountry NUMERIC(18,0)
	,@vcPostalCode VARCHAR(15)
AS
BEGIN
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetContactDetails

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		IF ISNULL(@numContactID,0) = 0
		BEGIN
			IF(SELECT COUNT(numContactId) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID)=0
			BEGIN
				SET @bitIsPrimaryContact = 1
			END

			IF ISNULL(@numContactType,0) = 0
			BEGIN
				SET @numContactType = 70
			END

			INSERT into AdditionalContactsInformation                                    
			(                  
				numContactType,                  
				vcDepartment,                  
				vcCategory,                  
				vcGivenName,                  
				vcFirstName,                  
				vcLastName,                   
				numDivisionId,                  
				numPhone,                  
				numPhoneExtension,                  
				numCell,                  
				numHomePhone,                          
				vcFax,                  
				vcEmail,                
				vcPosition,                  
				txtNotes,                  
				numCreatedBy,                                    
				bintCreatedDate,             
				numModifiedBy,            
				bintModifiedDate,                   
				numDomainID,               
				numRecOwner,                  
				numTeam,                  
				numEmpStatus,
				vcTitle
				,bitPrimaryContact     
			)                                    
			VALUES                                    
			(                  
				@numContactType,                  
				@vcDepartment,                  
				@vcCategory,                  
				CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),                  
				@vcFirstName ,                  
				@vcLastName,                   
				@numDivisionId ,                  
				@numPhone ,                                    
				@numPhoneExtension,                  
				@numCell ,                  
				@NumHomePhone ,                  
				@vcFax ,                  
				@vcEmail ,                  
				@vcPosition,                                   
				@txtNotes,                  
				@numUserCntID,                  
				GETUTCDATE(),             
				@numUserCntID,                  
				GETUTCDATE(),                   
				@numDomainID,                  
				@numUserCntID,                   
				@numTeam,                  
				658,
				@vcTitle
				,@bitIsPrimaryContact                
			)                                    
                     
			SET @numContactID= SCOPE_IDENTITY()

			INSERT INTO dbo.AddressDetails 
			(
	 			vcAddressName,
	 			vcStreet,
	 			vcCity,
	 			vcPostalCode,
	 			numState,
	 			numCountry,
	 			bitIsPrimary,
	 			tintAddressOf,
	 			tintAddressType,
	 			numRecordID,
	 			numDomainID
			) 
			VALUES
			(
				'Primary'
				,@vcStreet
				,@vcCity
				,@vcPostalCode
				,@numState
				,@numCountry
				,1
				,1
				,0
				,@numContactID
				,@numDomainID
			)                  
		END
		ELSE
		BEGIN
			UPDATE 
				AdditionalContactsInformation 
			SET                                    
				numContactType=@numContactType,                                    
				vcGivenName=CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),
				vcFirstName=@vcFirstName,
				vcLastName =@vcLastName,
				numDivisionId =@numDivisionId,
				numPhone=@numPhone,
				numPhoneExtension=@numPhoneExtension,
				numCell =@numCell,
				NumHomePhone =@NumHomePhone,
				vcFax=@vcFax,
				vcEmail=@vcEmail,
				vcPosition=@vcPosition,
				txtNotes=@txtNotes,
				numModifiedBy=@numUserCntID,
				bintModifiedDate=GETUTCDATE(),
				bitPrimaryContact=@bitIsPrimaryContact
			WHERE 
				numContactId=@numContactId
		END

		DECLARE @numCount AS NUMERIC(18,0) = 0

		IF ISNULL(@vcEmail,'') <> ''
		BEGIN 
			SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail AND numDomainId=@numDomainID

			IF @numCount = 0 
			BEGIN
				INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
				VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
			END
		END 
		ELSE
		BEGIN
			UPDATE EmailMaster set numContactId=@numcontactId WHERE vcEmailID=@vcEmail and numDomainId=@numDomainID
		END
	END

	SELECT ISNULL(@numContactID,0) AS numContactID             
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice MONEY
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost MONEY
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,@numIncomeChartAcntId
			,@numAssetChartAcntId
			,@numCOGsChartAcntId
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc) VALUES (@numItemCode,@txtDesc)

		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc) VALUES (@numItemCode,@txtDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveOrganizationDetails')
DROP PROCEDURE USP_Import_SaveOrganizationDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveOrganizationDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@vcCompanyName VARCHAR(100)
	,@vcProfile NUMERIC(18,0)
	,@numCompanyType NUMERIC(18,0)
	,@numNoOfEmployeesId NUMERIC(18,0)
	,@numAnnualRevID NUMERIC(18,0)
	,@vcWebSite VARCHAR(255)
	,@vcHow NUMERIC(18,0)
	,@numCompanyIndustry NUMERIC(18,0)
	,@numCompanyRating NUMERIC(18,0)
	,@numCompanyCredit NUMERIC(18,0)
	,@txtComments NVARCHAR(MAX)
	,@vcWebLink1 VARCHAR(255)
	,@vcWebLink2 VARCHAR(255)
	,@vcWebLink3 VARCHAR(255)
	,@vcWebLink4 VARCHAR(255)
	,@numCompanyDiff NUMERIC(18,0)
	,@vcCompanyDiff VARCHAR(100)
	,@numCampaignID NUMERIC(18,0)
	,@vcComPhone VARCHAR(50)
	,@numAssignedTo NUMERIC(18,0)
	,@numFollowUpStatus NUMERIC(18,0)
	,@numTerID NUMERIC(18,0)
	,@numStatusID NUMERIC(18,0)
	,@vcComFax VARCHAR(50)
	,@bitPublicFlag BIT
	,@bitActiveInActive BIT
	,@numGrpID NUMERIC(18,0)
	,@tintCRMType TINYINT
	,@numCurrencyID NUMERIC(18,0)
	,@bitNoTax BIT
	,@numBillingDays NUMERIC(18,0)
	,@bitOnCreditHold BIT
	,@numDefaultExpenseAccountID NUMERIC(18,0)
AS  
BEGIN
BEGIN TRY
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetOrganizationDetails

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT numDivisionID FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID)
	BEGIN
		BEGIN TRANSACTION
			DECLARE @numCompanyID NUMERIC(18,0)

			SELECT @numCompanyID = numCompanyID FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID

			IF ISNULL(@numCompanyID,0) > 0
			BEGIN
				UPDATE
					CompanyInfo
				SET
					vcCompanyName=@vcCompanyName
					,vcProfile=@vcProfile
					,numCompanyType=@numCompanyType
					,numNoOfEmployeesId=@numNoOfEmployeesId
					,numAnnualRevID=@numAnnualRevID
					,vcWebSite=@vcWebSite
					,vcHow=@vcHow
					,numCompanyIndustry=@numCompanyIndustry
					,numCompanyRating=@numCompanyRating
					,numCompanyCredit=@numCompanyCredit
					,txtComments=@txtComments
					,vcWebLink1=@vcWebLink1
					,vcWebLink2=@vcWebLink2
					,vcWebLink3=@vcWebLink3
					,vcWebLink4=@vcWebLink4
					,numModifiedBy=@numUserCntID              
					,bintModifiedDate=GETUTCDATE()
				WHERE
					numDomainID=@numDomainID
					AND numCompanyId=@numCompanyID
			END

			DECLARE @numFollow NUMERIC(18,0)                     

			SELECT 
				@numFollow=ISNULL(numFollowUpStatus,0)
			FROM 
				DivisionMaster 
			WHERE 
				numDivisionID=@numDivisionID 
				AND numDomainId= @numDomainID                    
			
			IF @numFollow <> @numFollowUpStatus                          
			BEGIN                          
				SELECT TOP 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID AND numDomainId= @numDomainID ORDER BY numFollowUpStatusID DESC                          
			                    
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
				VALUES
				(@numFollow,@numDivisionID,GETUTCDATE(),@numDomainID)                                  
			END                          
			
			---Updating if organization is assigned to someone            
			DECLARE @tempAssignedTo NUMERIC(18,0)          
			SELECT @tempAssignedTo=ISNULL(numAssignedTo,0) FROM DivisionMaster WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID           
			 
			IF @tempAssignedTo <> @numAssignedTo AND  @numAssignedTo<>'0'        
			BEGIN            
				UPDATE DivisionMaster SET numAssignedTo=@numAssignedTo,numAssignedBy=@numUserCntID WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID           
			END           
			ELSE IF ISNULL(@numAssignedTo,0) = 0
			BEGIN          
				UPDATE DivisionMaster SET numAssignedTo=0,numAssignedBy=0 WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID        
			END  
			                                      
			UPDATE 
				DivisionMaster 
			SET                                                                               
				numModifiedBy = @numUserCntID,                                            
				bintModifiedDate = GETUTCDATE()                                        
				,numCompanyDiff=@numCompanyDiff
				,vcCompanyDiff=@vcCompanyDiff
				,numCampaignID=@numCampaignID
				,vcComPhone=@vcComPhone
				,numFollowUpStatus=@numFollowUpStatus
				,numTerID=@numTerID
				,numStatusID=@numStatusID
				,vcComFax=@vcComFax
				,bitPublicFlag=@bitPublicFlag
				,bitActiveInActive=@bitActiveInActive
				,numGrpID=@numGrpID
				,tintCRMType=@tintCRMType
				,numCurrencyID=@numCurrencyID
				,bitNoTax=@bitNoTax
				,numBillingDays=@numBillingDays
				,bitOnCreditHold=@bitOnCreditHold
				,numDefaultExpenseAccountID=@numDefaultExpenseAccountID
			WHERE 
				numDivisionID = @numDivisionID 
				AND numDomainId= @numDomainID                                       
		COMMIT
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ImportFileReport_Insert' ) 
    DROP PROCEDURE USP_ImportFileReport_Insert
GO

CREATE PROCEDURE USP_ImportFileReport_Insert
	@intImportFileID BIGINT,
	@intRowNumber INT,
	@vcMessage varchar(1000),
	@numRecordID NUMERIC(18,0),
	@vcErrorMessage VARCHAR(MAX),
	@vcStackStrace VARCHAR(MAX)
AS 
BEGIN
	INSERT INTO Import_File_Report
	(
		intImportFileID
		,intRowNumber
		,vcMessage
		,numRecordID
		,vcErrorMessage
		,vcStackStrace
	)
	VALUES
	(
		@intImportFileID
		,@intRowNumber
		,@vcMessage
		,@numRecordID
		,@vcErrorMessage
		,@vcStackStrace
	)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_ImportPriceLevel')
DROP PROCEDURE USP_Item_ImportPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_Item_ImportPriceLevel]
(
	@numItemCode nUMERIC(18,0)
	,@tintRuleType INT
	,@tintDiscountType INT
	,@monPriceLevel1 MONEY
	,@monPriceLevel2 MONEY
	,@monPriceLevel3 MONEY
	,@monPriceLevel4 MONEY
	,@monPriceLevel5 MONEY
	,@monPriceLevel6 MONEY
	,@monPriceLevel7 MONEY
	,@monPriceLevel8 MONEY
	,@monPriceLevel9 MONEY
	,@monPriceLevel10 MONEY
	,@monPriceLevel11 MONEY
	,@monPriceLevel12 MONEY
	,@monPriceLevel13 MONEY
	,@monPriceLevel14 MONEY
	,@monPriceLevel15 MONEY
	,@monPriceLevel16 MONEY
	,@monPriceLevel17 MONEY
	,@monPriceLevel18 MONEY
	,@monPriceLevel19 MONEY
	,@monPriceLevel20 MONEY
	,@intPriceLevel1FromQty INT
	,@intPriceLevel1ToQty INT
	,@intPriceLevel2FromQty INT
	,@intPriceLevel2ToQty INT
	,@intPriceLevel3FromQty INT
	,@intPriceLevel3ToQty INT
	,@intPriceLevel4FromQty INT
	,@intPriceLevel4ToQty INT
	,@intPriceLevel5FromQty INT
	,@intPriceLevel5ToQty INT
	,@intPriceLevel6FromQty INT
	,@intPriceLevel6ToQty INT
	,@intPriceLevel7FromQty INT
	,@intPriceLevel7ToQty INT
	,@intPriceLevel8FromQty INT
	,@intPriceLevel8ToQty INT
	,@intPriceLevel9FromQty INT
	,@intPriceLevel9ToQty INT
	,@intPriceLevel10FromQty INT
	,@intPriceLevel10ToQty INT
	,@intPriceLevel11FromQty INT
	,@intPriceLevel11ToQty INT
	,@intPriceLevel12FromQty INT
	,@intPriceLevel12ToQty INT
	,@intPriceLevel13FromQty INT
	,@intPriceLevel13ToQty INT
	,@intPriceLevel14FromQty INT
	,@intPriceLevel14ToQty INT
	,@intPriceLevel15FromQty INT
	,@intPriceLevel15ToQty INT
	,@intPriceLevel16FromQty INT
	,@intPriceLevel16ToQty INT
	,@intPriceLevel17FromQty INT
	,@intPriceLevel17ToQty INT
	,@intPriceLevel18FromQty INT
	,@intPriceLevel18ToQty INT
	,@intPriceLevel19FromQty INT
	,@intPriceLevel19ToQty INT
	,@intPriceLevel20FromQty INT
	,@intPriceLevel20ToQty INT
	,@vcPriceLevel1Name VARCHAR(300)
	,@vcPriceLevel2Name VARCHAR(300)
	,@vcPriceLevel3Name VARCHAR(300)
	,@vcPriceLevel4Name VARCHAR(300)
	,@vcPriceLevel5Name VARCHAR(300)
	,@vcPriceLevel6Name VARCHAR(300)
	,@vcPriceLevel7Name VARCHAR(300)
	,@vcPriceLevel8Name VARCHAR(300)
	,@vcPriceLevel9Name VARCHAR(300)
	,@vcPriceLevel10Name VARCHAR(300)
	,@vcPriceLevel11Name VARCHAR(300)
	,@vcPriceLevel12Name VARCHAR(300)
	,@vcPriceLevel13Name VARCHAR(300)
	,@vcPriceLevel14Name VARCHAR(300)
	,@vcPriceLevel15Name VARCHAR(300)
	,@vcPriceLevel16Name VARCHAR(300)
	,@vcPriceLevel17Name VARCHAR(300)
	,@vcPriceLevel18Name VARCHAR(300)
	,@vcPriceLevel19Name VARCHAR(300)
	,@vcPriceLevel20Name VARCHAR(300)
) 
AS
BEGIN
	IF ISNULL(@intPriceLevel1FromQty,0) = 0 OR ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_1',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 OR ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_2',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 OR ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_3',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 OR ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_4',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 OR ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_5',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 OR ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_6',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 OR ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_7',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 OR ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_8',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 OR ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_9',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 OR ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_10',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 OR ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_11',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 OR ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_12',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 OR ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_13',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 OR ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_14',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 OR ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_15',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 OR ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_16',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 OR ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_17',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 OR ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_18',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 OR ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_19',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 OR ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_20',16,1)
		RETURN
	END


	DECLARE @Defualt TABLE
	(
		PriceLevelID INT
		,tintRuleType TINYINT
		,tintDiscountType TINYINT
		,monPrice MONEY
		,intFromQty INT
		,intToQty INT
		,vcName VARCHAR(300)
	)

	INSERT INTO @Defualt
	(
		PriceLevelID
		,monPrice
		,intFromQty
		,intToQty
		,vcName
	)
	VALUES
	(1,-1,-1,-1,'-1'),
	(2,-1,-1,-1,'-1'),
	(3,-1,-1,-1,'-1'),
	(4,-1,-1,-1,'-1'),
	(5,-1,-1,-1,'-1'),
	(6,-1,-1,-1,'-1'),
	(7,-1,-1,-1,'-1'),
	(8,-1,-1,-1,'-1'),
	(9,-1,-1,-1,'-1'),
	(10,-1,-1,-1,'-1'),
	(11,-1,-1,-1,'-1'),
	(12,-1,-1,-1,'-1'),
	(13,-1,-1,-1,'-1'),
	(14,-1,-1,-1,'-1'),
	(15,-1,-1,-1,'-1'),
	(16,-1,-1,-1,'-1'),
	(17,-1,-1,-1,'-1'),
	(18,-1,-1,-1,'-1'),
	(19,-1,-1,-1,'-1'),
	(20,-1,-1,-1,'-1')

	-- FIRST REPLACE -1 WITH EXISTING ITEM PRICES, REPLEACE -1 IN FROM AND TO QTY ONLY WHEN RuleType is Deduct From List Price or Add to vendor Cost
	UPDATE
		t1
	SET
		t1.monPrice = ISNULL(t2.decDiscount,t1.monPrice)
		,tintRuleType = (CASE WHEN ISNULL(@tintRuleType,0) <> -1 THEN @tintRuleType ELSE ISNULL(t2.tintRuleType,t1.tintRuleType) END)
		,tintDiscountType = (CASE WHEN ISNULL(@tintDiscountType,0) <> -1 THEN @tintDiscountType ELSE ISNULL(t2.tintDiscountType,t1.tintDiscountType) END)
		,t1.intFromQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intFromQty ELSE t1.intFromQty END)
		,t1.intToQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intToQty ELSE t1.intToQty END)
		,t1.vcName = ISNULL(t2.vcName,'')
	FROM
		@Defualt t1
	LEFT JOIN
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) AS RowID 
			,decDiscount
			,intFromQty
			,intToQty
			,tintRuleType
			,tintDiscountType
			,vcName
		FROM 
			PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0
	) t2
	ON
		t1.PriceLevelID=t2.RowID

	DECLARE @localRuleType TINYINT
	DECLARE @localDiscountType TINYINT

	SELECT TOP 1
		@localRuleType = tintRuleType
		,@localDiscountType = tintDiscountType
	FROM
		@Defualt AS t2
		

	IF ISNULL(@localRuleType,0) = 0
	BEGIN
		RAISERROR('INVALID_PRICE_RULE_TYPE',16,1)
		RETURN
	END
	ELSE IF (@localRuleType = 1 OR @localRuleType = 2) AND @localDiscountType NOT IN (1,2,3)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END
	ELSE IF @localRuleType = 3
	BEGIN
		SET @tintDiscountType = 0
	END

	-- REPLACE -1 WITH NEW PRICES IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY FROM IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY TO IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel1,0) <> -1 THEN ISNULL(@monPriceLevel1,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel1FromQty,0) <> -1 THEN ISNULL(@intPriceLevel1FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel1ToQty,0) <> -1 THEN ISNULL(@intPriceLevel1ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel1Name,'') <> '-1' THEN ISNULL(@vcPriceLevel1Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 1

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel2,0) <> -1 THEN ISNULL(@monPriceLevel2,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel2FromQty,0) <> -1 THEN ISNULL(@intPriceLevel2FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel2ToQty,0) <> -1 THEN ISNULL(@intPriceLevel2ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel2Name,'') <> '-1' THEN ISNULL(@vcPriceLevel2Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 2

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel3,0) <> -1 THEN ISNULL(@monPriceLevel3,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel3FromQty,0) <> -1 THEN ISNULL(@intPriceLevel3FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel3ToQty,0) <> -1 THEN ISNULL(@intPriceLevel3ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel3Name,'') <> '-1' THEN ISNULL(@vcPriceLevel3Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 3

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel4,0) <> -1 THEN ISNULL(@monPriceLevel4,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel4FromQty,0) <> -1 THEN ISNULL(@intPriceLevel4FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel4ToQty,0) <> -1 THEN ISNULL(@intPriceLevel4ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel4Name,'') <> '-1' THEN ISNULL(@vcPriceLevel4Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 4

	UPDATE 
		@Defualt
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel5,0) <> -1 THEN ISNULL(@monPriceLevel5,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel5FromQty,0) <> -1 THEN ISNULL(@intPriceLevel5FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel5ToQty,0) <> -1 THEN ISNULL(@intPriceLevel5ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel5Name,'') <> '-1' THEN ISNULL(@vcPriceLevel5Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 5
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel6,0) <> -1 THEN ISNULL(@monPriceLevel6,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel6FromQty,0) <> -1 THEN ISNULL(@intPriceLevel6FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel6ToQty,0) <> -1 THEN ISNULL(@intPriceLevel6ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel6Name,'') <> '-1' THEN ISNULL(@vcPriceLevel6Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 6

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel7,0) <> -1 THEN ISNULL(@monPriceLevel7,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel7FromQty,0) <> -1 THEN ISNULL(@intPriceLevel7FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel7ToQty,0) <> -1 THEN ISNULL(@intPriceLevel7ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel7Name,'') <> '-1' THEN ISNULL(@vcPriceLevel7Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 7

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel8,0) <> -1 THEN ISNULL(@monPriceLevel8,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel8FromQty,0) <> -1 THEN ISNULL(@intPriceLevel8FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel8ToQty,0) <> -1 THEN ISNULL(@intPriceLevel8ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel8Name,'') <> '-1' THEN ISNULL(@vcPriceLevel8Name,0) ELSE vcName END)
	WHERE
		 PriceLevelID = 8
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel9,0) <> -1 THEN ISNULL(@monPriceLevel9,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel9FromQty,0) <> -1 THEN ISNULL(@intPriceLevel9FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel9ToQty,0) <> -1 THEN ISNULL(@intPriceLevel9ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel9Name,'') <> '-1' THEN ISNULL(@vcPriceLevel9Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 9
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel10,0) <> -1 THEN ISNULL(@monPriceLevel10,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel10FromQty,0) <> -1 THEN ISNULL(@intPriceLevel10FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel10ToQty,0) <> -1 THEN ISNULL(@intPriceLevel10ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel10Name,'') <> '-1' THEN ISNULL(@vcPriceLevel10Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 10
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel11,0) <> -1 THEN ISNULL(@monPriceLevel11,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel11FromQty,0) <> -1 THEN ISNULL(@intPriceLevel11FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel11ToQty,0) <> -1 THEN ISNULL(@intPriceLevel11ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel11Name,'') <> '-1' THEN ISNULL(@vcPriceLevel11Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 11
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel12,0) <> -1 THEN ISNULL(@monPriceLevel12,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel12FromQty,0) <> -1 THEN ISNULL(@intPriceLevel12FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel12ToQty,0) <> -1 THEN ISNULL(@intPriceLevel12ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel12Name,'') <> '-1' THEN ISNULL(@vcPriceLevel12Name,0) ELSE vcName END)
	WHERE
		PriceLevelID = 12
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel13,0) <> -1 THEN ISNULL(@monPriceLevel13,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel13FromQty,0) <> -1 THEN ISNULL(@intPriceLevel13FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel13ToQty,0) <> -1 THEN ISNULL(@intPriceLevel13ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel13Name,'') <> '-1' THEN ISNULL(@vcPriceLevel13Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 13
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel14,0) <> -1 THEN ISNULL(@monPriceLevel14,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel14FromQty,0) <> -1 THEN ISNULL(@intPriceLevel14FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel14ToQty,0) <> -1 THEN ISNULL(@intPriceLevel14ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel14Name,'') <> '-1' THEN ISNULL(@vcPriceLevel14Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 14
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel15,0) <> -1 THEN ISNULL(@monPriceLevel15,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel15FromQty,0) <> -1 THEN ISNULL(@intPriceLevel15FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel15ToQty,0) <> -1 THEN ISNULL(@intPriceLevel15ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel15Name,'') <> '-1' THEN ISNULL(@vcPriceLevel15Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 15
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel16,0) <> -1 THEN ISNULL(@monPriceLevel16,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel16FromQty,0) <> -1 THEN ISNULL(@intPriceLevel16FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel16ToQty,0) <> -1 THEN ISNULL(@intPriceLevel16ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel16Name,'') <> '-1' THEN ISNULL(@vcPriceLevel16Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 16
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel17,0) <> -1 THEN ISNULL(@monPriceLevel17,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel17FromQty,0) <> -1 THEN ISNULL(@intPriceLevel17FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel17ToQty,0) <> -1 THEN ISNULL(@intPriceLevel17ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel17Name,'') <> '-1' THEN ISNULL(@vcPriceLevel17Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 17
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel18,0) <> -1 THEN ISNULL(@monPriceLevel18,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel18FromQty,0) <> -1 THEN ISNULL(@intPriceLevel18FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel18ToQty,0) <> -1 THEN ISNULL(@intPriceLevel18ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel18Name,'') <> '-1' THEN ISNULL(@vcPriceLevel18Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 18
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel19,0) <> -1 THEN ISNULL(@monPriceLevel19,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel19FromQty,0) <> -1 THEN ISNULL(@intPriceLevel19FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel19ToQty,0) <> -1 THEN ISNULL(@intPriceLevel19ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel19Name,'') <> '-1' THEN ISNULL(@vcPriceLevel19Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 19
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel20,0) <> -1 THEN ISNULL(@monPriceLevel20,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel20FromQty,0) <> -1 THEN ISNULL(@intPriceLevel20FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel20ToQty,0) <> -1 THEN ISNULL(@intPriceLevel20ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel20Name,'') <> '-1' THEN ISNULL(@vcPriceLevel20Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 20
	
	DECLARE @MaxPriceLevelIDWithSomeValue AS INT
	
	SELECT @MaxPriceLevelIDWithSomeValue=MAX(PriceLevelID) FROM @Defualt WHERE (monPrice <> -1 OR intFromQty <> -1 OR intToQty <> -1)

	-- NOW REPLACE PRICE -1 TO 0 IN ALL PRICE LEVELS WHICH ARE LESS THAN @MaxPriceLevelIDWithSomeValue 
	-- (THIS REQUIRED BECAUSE IF USER HAS PROVIDED PRICE LEVEL 1 AND PRICE LEVEL 6 COLUMN IN IMPORT FILE THEN WE HAVE TO ADD 0 FROM PRICE LEVEL 2,3,4,5)
	UPDATE
		@Defualt
	SET
		monPrice = (CASE WHEN monPrice = -1 THEN 0 ELSE monPrice END)
		,intFromQty = (CASE WHEN intFromQty = -1 THEN 0 ELSE intFromQty END)
		,intToQty = (CASE WHEN intToQty = -1 THEN 0 ELSE intToQty END)
		,vcName = (CASE WHEN vcName = '-1' THEN '' ELSE vcName END)
	WHERE
		PriceLevelID <= @MaxPriceLevelIDWithSomeValue
		AND (monPrice = -1 OR intFromQty=-1 OR intToQty=-1)


	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND (ISNULL(intFromQty,0) = 0 OR ISNULL(intToQty,0) = 0))
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0',16,1)
		RETURN
	END

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND ISNULL(tintDiscountType,0) = 0)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END

	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM PricingTable WHERE numItemCode=@numItemCode

		INSERT INTO PricingTable
		(
			numPriceRuleID,numItemCode,vcName,decDiscount,tintRuleType,tintDiscountType,intFromQty,intToQty
		)
		SELECT
			0,@numItemCode,vcName,monPrice,tintRuleType,(CASE WHEN tintRuleType = 3 THEN 0 ELSE tintDiscountType END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intFromQty END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intToQty END)
		FROM
			@Defualt
		WHERE
			PriceLevelID <= @MaxPriceLevelIDWithSomeValue

	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDisplayFieldsForBarcode]    Script Date: 15/08/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Manish Anjara
GO
/*
EXEC USP_ItemDisplayFieldsForBarcode 1,'696268,700870,701267,701272,721554,723915,734575,734676',0,1
*/
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDisplayFieldsForBarcode' ) 
    DROP PROCEDURE USP_ItemDisplayFieldsForBarcode
GO
CREATE PROCEDURE [dbo].[USP_ItemDisplayFieldsForBarcode]
    (
      @numDomainID AS NUMERIC(9) = 0,
      @strItemIdList AS TEXT,
      @bitAllItems AS BIT = 0,
      @numUserCntID AS NUMERIC(9) = 0	
    )
AS 
     
    DECLARE @join AS VARCHAR(8000)                  
    SET @join = ''                                                        
           
    DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 67
                        AND numDomainID = @numDomainID
                        AND ISNULL(numRelCntType, 0) = 0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 67
                        AND numDomainID = @numDomainID
                        AND ISNULL(numRelCntType, 0) = 0
            ) TotalRows
       
        
    IF @Nocolumns = 0 
        BEGIN
            INSERT  INTO DycFormConfigurationDetails
                    (
                      numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth,
                      numViewID
                    )
                    SELECT  67,
                            numFieldId,
                            0,
                            Row_number() OVER ( ORDER BY tintRow DESC ),
                            @numDomainID,
                            @numUserCntID,
                            0,
                            1,
                            0,
                            intColumnWidth,
                            0
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 67
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
        END

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 67
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS NVARCHAR(Max)
    DECLARE @strWhere AS NVARCHAR(Max)
    SET @strWhere = ' AND 1=1'
    
    IF @bitAllItems = 0 
    	SET @strWhere = @strWhere + ' AND numItemCode IN (SELECT Item FROM dbo.DelimitedSplit8K(''' + CONVERT(VARCHAR(MAX),@strItemIdList) + ''','',''))' 
    ELSE	
		SET @strWhere = @strWhere + ' AND charItemType = ''P'''
        
    SET @strSql = ' declare @numDomain numeric set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + '; 
					With tblItem AS (SELECT COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,numItemCode'       
	           
    SET @strWhere = @strWhere + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
    SET @join = @join
        + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
    SET @join = @join
        + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
      
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ')'
 
    SET @strSql = @strSql
        + ' select min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
	case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
	isnull(sum(numOnHand),0) as numOnHand,isnull(sum(numOnOrder),0) as numOnOrder,isnull(sum(numReorder),0) as numReorder,isnull(sum(numBackOrder),0) as numBackOrder,                  
	isnull(sum(numAllocation),0) as numAllocation,vcCompanyName,CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,I.vcModelID,
	Cast(CASE WHEN charItemType=''P'' THEN ISNULL(WareHouseItems.monWListPrice,0) 
	     WHEN charItemType=''N'' THEN ISNULL(I.monListPrice,0)  
	     WHEN charItemType=''S'' THEN ISNULL(I.monListPrice,0)
	     ELSE ISNULL(I.monListPrice,0)
	END as decimal(18,2)) AS monListPrice ,
	I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass,
	ISNULL(I.numItemClassification,0) As [numItemClassification],ISNULL(LDC.vcData,'''') AS [ItemClassification],ISNULL(I.bitTaxable,0) As [bitTaxable],ISNULL(I.bitKitParent,0) [bitKitParent],
	ISNULL(I.numVendorID,0) [numVendorID],ISNULL(I.bitAllowBackOrder,0) [bitAllowBackOrder],ISNULL(I.bitSerialized,0) [bitSerialized],ISNULL(I.numItemGroup,0) [numItemGroup],
	ISNULL(I.monCampaignLabourCost,0) [monCampaignLabourCost],ISNULL(I.bitFreeShipping,0) [bitFreeShipping],ISNULL(I.vcUnitofMeasure,'''') [vcUnitofMeasure],
	ISNULL(I.bitLotNo,0) [bitLotNo],ISNULL(I.IsArchieve,0) [IsArchieve],ISNULL(I.numItemClass,0) [numItemClass],ISNULL(I.bitArchiveItem,0) [bitArchiveItem],
	ISNULL(UOM1.vcUnitName,'''') [vcBaseUnit],ISNULL(UOM2.vcUnitName,'''') [vcSaleUnit],ISNULL(UOM3.vcUnitName,'''') [vcPurchaseUnit],ISNULL(com.vcCompanyName,'''') [vcVendor],
	ISNULL(V.vcPartNo,'''') [vcPartNo],ISNULL(V.intMinQty,0) [intMinQty],ISNULL(I.numBaseUnit,0) [numBaseUnit],ISNULL(I.numSaleUnit,0) [numSaleUnit],
	ISNULL(I.numPurchaseUnit,0) [numPurchaseUnit],ISNULL(I.bitAssembly,0) As [bitAssembly],ISNULL(vcItemGroup,'''') AS [vcItemGroup] '
	
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

    SET @strSql = @strSql
        + ' INTO #tblItem FROM tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
	left join WareHouseItems on numItemID=I.numItemCode                                  
	left join divisionmaster div  on I.numVendorid = div.numDivisionId                                    
	left join companyInfo com on com.numCompanyid=div.numcompanyID   
	left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
	LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
	LEFT JOIN ListDetails LDC ON LDC.numListItemID = i.numItemClassification
	LEFT JOIN UOM UOM1 ON UOM1.numUOMID = I.numBaseUnit
	LEFT JOIN UOM UOM2 ON UOM2.numUOMID = I.numSaleUnit
	LEFT JOIN UOM UOM3 ON UOM3.numUOMID = I.numPurchaseUnit
	LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID AND V.numVendorID = div.numDivisionId AND V.numItemCode=I.numItemCode 
	LEFT JOIN dbo.ItemGroups IG ON  IG.numItemGroupID = I.numItemGroup  
	group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight
	,fltWeight,monAverageCost,I.vcSKU,LD.vcData,numItemClassification,bitTaxable,vcSKU,bitKitParent,I.numVendorID,bitAllowBackOrder,bitSerialized,numItemGroup,
	monCampaignLabourCost,bitFreeShipping,vcUnitofMeasure,bitLotNo,IsArchieve,numItemClass,bitArchiveItem,vcCompanyName,vcPartNo,intMinQty,UOM1.vcUnitName 
	,UOM2.vcUnitName,UOM3.vcUnitName,I.numBaseUnit,I.numSaleUnit,I.numPurchaseUnit,I.bitAssembly,vcItemGroup,LDC.vcData,WareHouseItems.monWListPrice '  
																																		   
    IF @bitLocation = 1																													   
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
    --SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(8000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
   
    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9)
        )
        

    INSERT  INTO #tempForm
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    ISNULL(vcCultureFieldName, vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage vcFieldMessage,
                    ListRelID
            FROM    View_DynamicColumns
            WHERE   numFormId = 67
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    --AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitCustom, 0) = 0
                    AND ISNULL(numRelCntType, 0) = 0
            UNION
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID
            FROM    View_DynamicCustomColumns
            WHERE   numFormId = 67
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    --AND tintPageType = 1
                    AND ISNULL(bitCustom, 0) = 1
                    AND ISNULL(numRelCntType, 0) = 0
            ORDER BY tintOrder ASC      
            
    
            
    SET @strSql = @strSql
        + ' select  TotalRowCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,
					CASE WHEN charItemType = ''P'' THEN ''Inventory Item'' 
						 WHEN charItemType = ''N'' THEN ''Non Inventory Item''
						 WHEN charItemType = ''A'' THEN ''Assembly Item''
						 WHEN charItemType = ''S'' THEN ''Service Item'' 
						 ELSE '''' 
					END AS [ItemType],numOnHand, LD.vcData AS numShipClass,numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,
				   vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,
				   (Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost,monStockValue,vcSKU,numItemClassification,CASE WHEN bitTaxable = 1 THEN ''Yes'' ELSE ''No'' END AS bitTaxable,
				   CASE WHEN bitKitParent = 1 THEN ''Yes'' ELSE ''No'' END AS bitKitParent,numVendorID,
				   CASE WHEN bitAllowBackOrder = 1 THEN ''Yes'' ELSE ''No'' END AS bitAllowBackOrder,
				   CASE WHEN bitSerialized = 1 THEN ''Yes'' ELSE ''No'' END AS bitSerialized,numItemGroup,vcItemGroup,monCampaignLabourCost,
				   CASE WHEN bitFreeShipping = 1 THEN ''Yes'' ELSE ''No'' END AS bitFreeShipping,vcUnitofMeasure,
				   CASE WHEN bitLotNo = 1 THEN ''Yes'' ELSE ''No'' END AS bitLotNo,CASE WHEN IsArchieve = 1 THEN ''Yes'' ELSE ''No'' END AS IsArchieve,numItemClass,
				   CASE WHEN bitArchiveItem = 1 THEN ''Yes'' ELSE ''No'' END AS bitArchiveItem,vcBaseUnit,vcSaleUnit,vcPurchaseUnit,vcVendor,vcPartNo,intMinQty,
				   CASE WHEN bitAssembly = 1 THEN ''Yes'' ELSE ''No'' END AS bitAssembly,numBaseUnit,numSaleUnit,numPurchaseUnit,ItemClassification '     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0 
                BEGIN
                    PRINT @vcDbColumnName
                    IF @vcDbColumnName = 'vcPathForTImage' 
                        BEGIN
                            SET @strSql = @strSql
                                + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                            SET @WhereCondition = @WhereCondition
                                + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
                        END
                END                              
			   
            ELSE 
	            PRINT @vcDbColumnName
                IF @bitCustom = 1 
                    BEGIN      
                  
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                        IF @vcAssociatedControlType = 'TextBox'
                            OR @vcAssociatedControlType = 'TextArea' 
                            BEGIN                
                   
                                SET @strSql = @strSql + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcDbColumnName + ']'                   
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                         
                            END   
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox' 
                                BEGIN            
               
                                    SET @strSql = @strSql
                                        + ',case when isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,0)=1 then ''Yes'' end   [' + @vcDbColumnName + ']'              
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                     
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField' 
                                    BEGIN              
                   
                                        SET @strSql = @strSql
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcDbColumnName + ']'                   
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode   '                                                         
                                    END                
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox' 
                                        BEGIN                
                                            SET @vcDbColumnName = 'Cust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                
                                            SET @strSql = @strSql + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' [' + @vcDbColumnName + ']'                                                          
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values_Item CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=temp.numItemCode    '                                                         
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                
                                        END                 
                    END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder ,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql
        + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '
        + CONVERT(VARCHAR(20), @numDomainID) + ' ' + @WhereCondition

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql
        )  

    UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcDbColumnName
                                  ELSE vcDbColumnName
                             END 
    SELECT  *
    FROM    #tempForm

    DROP TABLE #tempForm

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_IMPORT_FILE_DATA')
DROP PROCEDURE USP_MANAGE_IMPORT_FILE_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MANAGE_IMPORT_FILE_DATA]
(
   @intImportFileID			BIGINT	OUTPUT,	
   @vcImportFileName		VARCHAR(1000),
   @numMasterID				NUMERIC,
   @numRecordAdded			NUMERIC,
   @numRecordUpdated		NUMERIC,
   @numErrors				NUMERIC,
   @numDuplicates			NUMERIC,
   @dtCreateDate			DATETIME,
   @dtImportDate			DATETIME,
   @numDomainID				NUMERIC,
   @numUserContactID		NUMERIC,
   @tintStatus				TINYINT,
   @btHasSendEmail			BIT,
   @vcHistory_Added_Value	VARCHAR(MAX),
   @dtHistoryDateTime		DATETIME,
   @intMode					INT,
   @tintItemLinkingID TINYINT,
   @tintImportType TINYINT
)
		--> 0 - FOR INSERT
		--> 1 - FOR UPDATE
		--> 2 - FOR UPDATE ROLLBACK STATUS
AS 
BEGIN
	IF @intMode = 0 -- FOR INSERT
		BEGIN
			INSERT INTO Import_File_Master
			(
				vcImportFileName
				,numMasterID
				,numRecordAdded
				,numRecordUpdated
				,numErrors
				,numDuplicates				   
				,dtCreateDate
				,dtImportDate
				,numDomainID
				,numUserContactID
				,tintStatus
				,btHasSendEmail
				,tintItemLinkingID
				,tintImportType
			)
			 VALUES
			(	
				@vcImportFileName	
				,@numMasterID			
				,@numRecordAdded		
				,@numRecordUpdated	
				,@numErrors			
				,@numDuplicates		
				,GETUTCDATE()		
				,GETUTCDATE()		
				,@numDomainID			
				,@numUserContactID	
				,@tintStatus			
				,@btHasSendEmail
				,@tintItemLinkingID
				,@tintImportType
			)

			SET @intImportFileID = @@IDENTITY	   
			
			INSERT INTO dbo.Import_History
			        (intImportFileID,vcHistory_Added_Value ,dtHistoryDateTime )
			VALUES  (@intImportFileID,@vcHistory_Added_Value,@dtHistoryDateTime )
			
		END  
		
	IF @intMode = 1 -- FOR UPDATE	         
		BEGIN
			
			UPDATE dbo.Import_History SET vcHistory_Added_Value = @vcHistory_Added_Value
									WHERE intImportFileID = @intImportFileID  
									  					
			DECLARE @numHistoryID AS NUMERIC
			SELECT @numHistoryID = numHistoryID FROM dbo.Import_History WHERE intImportFileID = @intImportFileID
			
			UPDATE Import_File_Master SET [vcImportFileName] = @vcImportFileName
										  ,[numMasterID] = @numMasterID
										  ,[numRecordAdded] = @numRecordAdded
										  ,[numRecordUpdated] = @numRecordUpdated
										  ,[numErrors] = @numErrors
										  ,[numDuplicates] = @numDuplicates
										  ,[numHistoryID] = @numHistoryID
										  ,[dtImportDate] = GETUTCDATE()
										  ,[tintStatus] = @tintStatus
										  ,[btHasSendEmail] = @btHasSendEmail
			WHERE intImportFileID = @intImportFileID
			  AND numDomainID = @numDomainID
			  AND numUserContactID =  @numUserContactID 
		END	
		
	IF @intMode = 2 -- UPDATE STATUS FOR ROLLBACK
		BEGIN
			UPDATE Import_File_Master SET tintStatus = @tintStatus 
			WHERE intImportFileID = @intImportFileID
			  AND numDomainID = @numDomainID
			  AND numUserContactID = @numUserContactID 
			  AND ISNULL(numRecordAdded,0) > 0
		END	         		
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_ITEM_VENDOR')
DROP PROCEDURE USP_MANAGE_ITEM_VENDOR
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.USP_MANAGE_ITEM_VENDOR
(
	@numVendorTcode		NUMERIC(18,0) OUTPUT,
	@numItemKitID		NUMERIC(18,0),
	@numVendorID		NUMERIC(18,0),
	@vcPartNo			VARCHAR(100),
	@numDomainID		NUMERIC(18, 0),
	@monCost			MONEY ,
	@intMinQty			INT,
	@bitIsPrimaryVendor	BIT,
	@intMode			BIT,
	@vcNotes VARCHAR(300) = ''
)
AS 
BEGIN
	IF (SELECT COUNT(*) FROM Vendor WHERE numDomainID = @numDomainID AND numItemCode = @numItemKitID) = 0
	BEGIN
		SET @bitIsPrimaryVendor = 1
	END

	IF @intMode = 0
	BEGIN		
		SELECT @numVendorTcode = numVendorTcode FROM dbo.Vendor  WHERE numDomainID = @numDomainID AND numItemCode = @numItemKitID AND numVendorID = @numVendorID
		
		IF ISNULL(@numVendorTcode,0) = 0    
			BEGIN
				INSERT INTO  dbo.Vendor ( numVendorID, vcPartNo, numDomainID, monCost, numItemCode, intMinQty, vcNotes ) 
								 VALUES ( @numVendorID,@vcPartNo,@numDomainID,@monCost,@numItemKitID,@intMinQty, @vcNotes) 	
			
				 PRINT @numVendorTcode 
				 SET @numVendorTcode = @@IDENTITY		 
				 PRINT @numVendorTcode 
			END
		ELSE
			BEGIN
					SELECT  @numItemKitID = numItemCode, @numDomainID = numDomainID, @numVendorID = numVendorID 
					FROM dbo.Vendor WHERE numVendorTcode = @numVendorTcode
											
					UPDATE dbo.Vendor	SET	vcPartNo = @vcPartNo, monCost = @monCost, intMinQty = @intMinQty, vcNotes=@vcNotes
					WHERE 1=1
					AND numVendorTcode = @numVendorTcode
					AND numDomainID = @numDomainID						
			END 
		
		IF @bitIsPrimaryVendor = 1
			BEGIN
				UPDATE item SET numVendorID = @numVendorID WHERE numItemCode = @numItemKitID AND numDomainID = @numDomainID
			END			 					
	END
	
--	 PRINT @numVendorTcode 
--	 SET @numVendorTcode = @@IDENTITY		 
--	 PRINT @numVendorTcode 
	 
	IF @intMode = 1
	BEGIN
		DELETE FROM dbo.Vendor WHERE 1=1 AND numVendorTcode = @numVendorTcode
	END
END
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0
AS   

DECLARE @numPartenerSource NUMERIC(18,0)      
DECLARE @numPartenerContact NUMERIC(18,0)     


IF ISNULL(@numPartner,0) = 0
BEGIN
	--Added By Prasanta Pradhan
	--Get Partenr Source and Code by Email and Partenr Code
	SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
END
ELSE 
BEGIN
	SET @numPartenerSource = @numPartner
END


SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
 -- Added By Manish Anjara As On : 21st Jam,2013
 -- Check whether GroupID Or tintCRMType is not passed then we need to add record as prospect (means numGroupID = 0, tintCRMType = 1)
 IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
 BEGIN
 	SET @numGrpId = 0
 	SET @tintCRMType = 1
 END
                                                                                                               
 IF @numDivisionId is null OR @numDivisionId=0                                                 
 BEGIN    
 if @UpdateDefaultTax=1 set @bitNoTax=0
                                                                                  
    INSERT INTO DivisionMaster                                    
     (                      
      numCompanyID,                      
      vcDivisionName,                      
      numGrpId,                      
      numFollowUpStatus,               
      bitPublicFlag,                      
      numCreatedBy,                      
      bintCreatedDate,                                    
      numModifiedBy,                      
      bintModifiedDate,                      
      tintCRMType,                      
      numDomainID,                                    
      bitLeadBoxFlg,                      
      numTerID,                     
      numStatusID,                                    
      numRecOwner,                              
      tintBillingTerms,                                    
      numBillingDays,                      
      tintInterestType,                      
      fltInterest,                      
      numCampaignID,            
      vcComPhone,            
      vcComFax,    
   bitNoTax,numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact        
 )                                    
     VALUES                      
    (                      
    @numCompanyID,                       
    @vcDivisionName,                       
                      
    @numGrpId,                       
    @numFollowUpStatus,                               
    @bitPublicFlag,                       
    @numUserCntID,                       
    getutcdate(),                       
    @numUserCntID,                       
    getutcdate(),                       
    @tintCRMType,                       
    @numDomainID,                       
    @bitLeadBoxFlg,                       
    @numTerID,                       
    @numStatusID,                                                        
    @numUserCntID,                                
    (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                      
    @numBillingDays,                      
    0,                      
    0,                      
    @numCampaignID,            
    @vcComPhone,            
    @vcComFax,    
    @bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact                         
  )                                  
	SELECT @numDivisionID = SCOPE_IDENTITY()

	IF @numBillAddressID > 0 OR @numShipAddressID > 0
	BEGIN
		IF @numBillAddressID > 0
		BEGIN
			UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
		END

		IF @numShipAddressID > 0
		BEGIN
			UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
		END
	END
	ELSE
	BEGIN
	  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
		IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
        BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
			union
			SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
        END
		ELSE
		BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
			union
			SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
		END
	END
       --End of Sachin Script                   
     



  if @UpdateDefaultTax=1 
  begin
   insert into DivisionTaxTypes
   select @numDivisionID,numTaxItemID,1 from TaxItems
   where numDomainID=@numDomainID
   union
   select @numDivisionID,0,1 
  end                                                


	                             
	DECLARE @numGroupID NUMERIC
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0 
	insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
      values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                      
                                                                          
     SELECT @numDivisionID                                                  
                                                   
 END                                                        
 ELSE if @numDivisionId>0                                                      
 BEGIN                                                                    
   --Inserting into Follow up History                                       
    declare @numFollow as varchar(10)                                        
    declare @binAdded as varchar(20)                                        
    declare @PreFollow as varchar(10)                                        
    declare @RowCount as varchar(2)                                        
    set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID)                                        
    set @RowCount=@@rowcount                                        
    select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID                                         
    if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                                        
     begin                                        
   select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc                                        
                                         
  if @PreFollow<>0                                        
  begin                                        
                                         
   if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
      begin                                        
                                          
            insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                        
          end                                        
  end                              
  else                                        
  begin                                        
                                         
  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                   
  end                                        
                                     
  end                                        
    -----                                                      
                                                        
   UPDATE DivisionMaster                       
  SET                        
   numCompanyID = @numCompanyID ,                     
   vcDivisionName = @vcDivisionName,                                                        
     numGrpId = @numGrpId,                                                                       
     numFollowUpStatus =@numFollowUpStatus,                                                        
     numTerID = @numTerID,                                                   
     bitPublicFlag =@bitPublicFlag,                                                        
     numModifiedBy = @numUserCntID,                                                  
     bintModifiedDate = getutcdate(),                                                     
     tintCRMType = @tintCRMType,                                                      
     numStatusID = @numStatusID,                                                 
     tintBillingTerms = (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                                                      
     numBillingDays = @numBillingDays,                                                     
     tintInterestType = @tintInterestType,                                                       
     fltInterest =@fltInterest,                          
     vcComPhone=@vcComPhone,                          
     vcComFax= @vcComFax,                      
     numCampaignID=@numCampaignID,    
     bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,numCurrencyID = @numCurrencyID,numPartenerSource=@numPartenerSource,numPartenerContact=@numPartenerContact                                                         
   WHERE numDivisionID = @numDivisionID       

    Update dbo.AddressDetails set
       vcStreet=@vcBillStreet,                                    
       vcCity =@vcBillCity,                                    
       vcPostalCode=@vcBillPostCode,                                     
       numState=@vcBillState,                                    
       numCountry=@vcBillCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=1         
	   Update dbo.AddressDetails set
       vcStreet=@vcShipStreet,                                    
       vcCity =@vcShipCity,                                    
       vcPostalCode=@vcShipPostCode,                                     
       numState=@vcShipState,                                    
       numCountry=@vcShipCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=2         
	  
             
  end 

    ---Updating if organization is assigned to someone                
  declare @tempAssignedTo as numeric(9)              
  set @tempAssignedTo=null               
  select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID                
print @tempAssignedTo              
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
  begin                
    update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID                
  end               
  else if  (@numAssignedTo =0)              
  begin              
   update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID              
                  
                
                                                                      
   SELECT @numDivisionID                                                   
 END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemImages]    Script Date: 04/13/2012 17:45:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemImages')
DROP PROCEDURE USP_ManageItemImages
GO
/*
BEGIN TRANSACTION
declare @p1 bigint
set @p1=0
exec USP_ManageItemImages @numItemImageId=@p1 output,@numItemCode=195142,@numDomainID=1,@vcPathForImage='i06314.jpg',@vcPathForTImage='i06314.jpg',@bitDefault=0,@intDisplayOrder=0
select @p1

ROLLBACK
*/
CREATE PROCEDURE [dbo].[USP_ManageItemImages]
(
	@numItemImageId  NUMERIC(18)=0 OUTPUT,
	@numItemCode	 NUMERIC(18),
	@numDomainId     NUMERIC(18),
	@vcPathForImage	 VARCHAR(300),
	@vcPathForTImage VARCHAR(300),
	@bitDefault	     BIT,
	@intDisplayOrder INT,
	@bitIsImage	     BIT
)

AS 

BEGIN
  
  IF ISNULL(@bitDefault,0) = 1
	BEGIN
		UPDATE ItemImages SET bitDefault = 0 WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitIsImage = 1
	END
  		
  IF ISNULL(@numItemImageId,0) = 0
	  BEGIN
	        
		 IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode AND bitIsImage = 1 AND numDomainId = @numDomainId )
			   BEGIN
				 SET @bitDefault = 1
			   END 
	     ELSE
			     SET @bitDefault = 0
		 
		  IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainId )
				 BEGIN
					SET @intDisplayOrder = 1 	
				 END
		  ELSE
				 BEGIN
				 	SELECT @intDisplayOrder = MAX(ISNULL(intDisplayOrder,0)) + 1 FROM ItemImages 
						WHERE numItemCode = @numItemCode 
						 AND numDomainId = @numDomainId
				 END
	       
		-- PRINT(@intDisplayOrder)
		
		IF((SELECT numItemCode FROM item WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId) > 0)
			 BEGIN
				  INSERT INTO [ItemImages] 
				  ( [numItemCode],[vcPathForImage],[vcPathForTImage],[bitDefault],[intDisplayOrder],[numDomainId] ,[bitIsImage])
				  VALUES
				  ( @numItemCode,@vcPathForImage,ISNULL(@vcPathForTImage,''),@bitDefault,@intDisplayOrder,@numDomainId ,@bitIsImage)
		       
				 SET @numItemImageId = @@IDENTITY   
				 --PRINT @numItemImageId
			 END		  
	   END
   
  ELSE
  
 BEGIN
	
	UPDATE [ItemImages] SET bitDefault = @bitDefault , intDisplayOrder = @intDisplayOrder,vcPathForImage=@vcPathForImage,vcPathForTImage=@vcPathForTImage WHERE numItemImageId = @numItemImageId    
	 END

	 update item set  bintModifiedDate=getutcdate()where numItemCode=@numItemCode  AND numDomainID=@numDomainID
 
END

--exec USP_ManageItemImages @intDisplayOrder = 0 ,@numDomainId = 1 ,@vcPathForImage = "abc" ,@vcPathForTImage = "abc" ,@bitDefault = false ,@numItemCode = 197605
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0)
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P')
			  BEGIN
			   IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
				 RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				 RETURN
				END
			  END
			  -- This is common check for CharItemType P and Other
			  IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
				 RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
				 RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
				 RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				 RETURN
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix,numManufacturer
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix,@numManufacturer
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingNamesTable')
DROP PROCEDURE USP_ManagePricingNamesTable
GO
CREATE PROCEDURE USP_ManagePricingNamesTable
    @numDomainID NUMERIC,
    @numCreatedBy AS NUMERIC,
    @strItems TEXT
AS 
BEGIN
	DELETE  FROM [PricingNamesTable] WHERE numDomainID= @numDomainID
		
    DECLARE @hDocItem INT

	IF CONVERT(VARCHAR(10), @strItems) <> '' 
    BEGIN
        EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
        INSERT  INTO [PricingNamesTable]
        (
            numDomainID,
            [numCreatedBy],
            [dtCreatedDate],
            [tintPriceLevel],
            [vcPriceLevelName]
        )
        SELECT  
			@numDomainID,
			@numCreatedBy,
			GETUTCDATE(),
			X.[tintPriceLevel],
			X.vcPriceLevelName
        FROM    
		( 
			SELECT * FROM 
			OPENXML (@hDocItem, '/NewDataSet/Item', 2)
            WITH ( tintPriceLevel TINYINT, vcPriceLevelName VARCHAR(300))
        ) X

        EXEC sp_xml_removedocument @hDocItem
    END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingTable')
DROP PROCEDURE USP_ManagePricingTable
GO
CREATE PROCEDURE USP_ManagePricingTable
    @numPriceRuleID NUMERIC,
    @numItemCode AS NUMERIC,
    @strItems TEXT
AS 
    BEGIN
		
        DELETE  FROM [PricingTable]
        WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode
		
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
                INSERT  INTO [PricingTable]
                        (
                          numPriceRuleID,
                          [intFromQty],
                          [intToQty],
                          [tintRuleType],
                          [tintDiscountType],
                          [decDiscount],
						  numItemCode
						  --,vcName	
                        )
                        SELECT  X.numPriceRuleID,
                                X.[intFromQty],
                                X.[intToQty],
                                X.[tintRuleType],
                                CASE ISNULL(X.[tintDiscountType],0) WHEN 0 THEN 1 ELSE ISNULL(X.[tintDiscountType],0) END,
								CAST(CONVERT(DECIMAL(18, 4), X.[decDiscount]) AS VARCHAR) AS decDiscount,
								X.numItemCode
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                                            WITH ( numPriceRuleID NUMERIC, intFromQty INT, intToQty INT, tintRuleType TINYINT, tintDiscountType TINYINT, decDiscount DECIMAL(18,4),numItemCode NUMERIC, vcName VARCHAR(300))
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.numShipmentMethod,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.numShipmentMethod IS NULL THEN '-' WHEN Mst.numShipmentMethod = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.numShipmentMethod) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.intUsedShippingCompany,0) AS intUsedShippingCompany,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS MONEY 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)
	SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
			
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
			
	DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
	SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
	--Updating the Average Cost
	DECLARE @monAvgCost AS MONEY 
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  	SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
	UPDATE  
		item
	SET 
		monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
	WHERE 
		numItemCode = @numItemCode
    

	

	IF @numWLocationID = -1 AND ISNULL(@numSelectedWarehouseItemID,0) > 0		
	BEGIN
		-- FIRST DECRESE OnOrder OF GLOBAL LOCATION
		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE 
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
	WHERE
		[numoppitemtCode] = @numOppItemID

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @CalPrice AS MONEY = 0.0000
	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		SELECT 
			TOP 1 
			@monLastPrice = monPrice,
			@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
		END
		ELSE IF @bitCalAmtBasedonDepItems = 1 AND @CalPrice > 0 AND @tintOppType = 1
		BEGIN
			SET @tintPricingBasedOn = 4
			SET @monPrice = @CalPrice
			SET @tintRuleType = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
