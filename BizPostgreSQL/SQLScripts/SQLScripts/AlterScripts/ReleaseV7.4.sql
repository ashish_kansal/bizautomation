/******************************************************************
Project: Release 7.3 Date: 02.MAY.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassStockTransfer]    Script Date: 01-May-17 6:16:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MassStockTransfer](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numFromWarehouseItemID] [numeric](18, 0) NOT NULL,
	[numToWarehouseItemID] [numeric](18, 0) NOT NULL,
	[numQty] [numeric](18, 0) NOT NULL,
	[numTransferredBy] [numeric](18, 0) NOT NULL,
	[dtTransferredDate] [datetime] NOT NULL,
	[bitMassTransfer] [bit] NOT NULL,
	[vcSerialLot] [varchar](max) NULL,
	[numFromQtyBefore] [decimal](18, 4) NOT NULL,
	[numToQtyBefore] [decimal](18, 4) NOT NULL,
	[numFromQtyAfter] [decimal](18, 4) NOT NULL,
	[numToQtyAfter] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_MassStockTransfer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

--------------------------------------------------------------------------------


UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmMassStockTransfer.aspx' WHERE vcPageNavName='Stock Transfer'

---------------------------------------------------------------------------------

ALTER TABLE Domain ADD bitRemoveGlobalLocation BIT DEFAULT 1

-----------------------------------------------------------------------------------


BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup
	)
	VALUES
	(
		1,'e-commerce Access','bitEcommerceAccess','bitEcommerceAccess','IsEcommerceAccess','ExtarnetAccounts','Y','R','CheckBox',0,1,0,1,0,1,1,1,1,1,'Org.Details Fields'
	)

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID, numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
	)
	VALUES
	(
		1,@numFieldID,34,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
	),
	(
		1,@numFieldID,35,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
	),
	(
		1,@numFieldID,36,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
	)


	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup
	)
	VALUES
	(
		2,'e-commerce password','vcPassword','vcPassword','vcPassword','ExtranetAccountsDtl','V','R','TextBox',0,1,0,1,0,1,1,1,1,1,'Contact Fields'
	)

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
	)
	VALUES
	(
		2,@numFieldID,10,1,1,'e-commerce password','TextBox','vcPassword',1,0,0,1,1,1,0
	)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH


------------------------------------------

ALTER TABLE Item ADD numManufacturer NUMERIC(18,0)