/******************************************************************
Project: Release 14.2 Date: 23.SEP.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[SalesOrderLineItemsPOLinking]    Script Date: 17-Sep-20 5:23:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SalesOrderLineItemsPOLinking](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numSalesOrderID] [numeric](18, 0) NOT NULL,
	[numSalesOrderItemID] [numeric](18, 0) NOT NULL,
	[numPurchaseOrderID] [numeric](18, 0) NOT NULL,
	[numPurchaseOrderItemID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_SalesOrderLineItemsPOLinking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------


USE [Production.2014]
GO

/****** Object:  Table [dbo].[ItemMarketplaceMapping]    Script Date: 21-Sep-20 10:43:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ItemMarketplaceMapping](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numMarketplaceID] [numeric](18, 0) NOT NULL,
	[vcMarketplaceUniqueID] [varchar](300) NOT NULL,
 CONSTRAINT [PK_ItemMarketplaceMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

---------------------------------------------

ALTER TABLE Contracts ALTER COLUMN [numHours] NUMERIC(18,0)
ALTER TABLE Contracts ALTER COLUMN [numMinutes] NUMERIC(18,0)