/******************************************************************
Project: Release 10.6 Date: 11.DEC.2018
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
	)

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0)
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO 
			@TEMPSelectedKitChilds
		SELECT 
			OKI.numChildItemID,
			OKI.numWareHouseItemId,
			OKCI.numItemID,
			OKCI.numWareHouseItemId
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKI.numOppChildItemID = OKCI.numOppChildItemID
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
	END
	ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
	BEGIN
		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
		)
		SELECT 
			SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
			SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))
		FROM 
			dbo.SplitString(@vcSelectedKitChildItems,',')

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)
	END


	;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
	(
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(numUOMId,0)
		FROM 
			[ItemDetails] ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE   
			[numItemKitID] = @numItemCode
			AND 1 = (CASE WHEN ISNULL(I.bitKitParent,0) = 1 THEN (CASE WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) THEN 1 ELSE 0 END) ELSE 1 END)
		UNION ALL
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(ID.numUOMId,0)
		FROM 
			CTE As Temp1
		INNER JOIN
			[ItemDetails] ID
		ON
			ID.numItemKitID = Temp1.numItemCode
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE
			Temp1.bitKitParent = 1
			AND Temp1.bitCalAmtBasedonDepItems = 1
			AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID)
	)

	INSERT INTO @TEMPitems
	(
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	)
	SELECT
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	FROM
		CTE

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		VALUES
		(
			0
			,0
		)
	END
	ELSE
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			1
			,(CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
		FROM
		(
			SELECT  
				ISNULL(CASE 
						WHEN I.[charItemType]='P' 
						THEN 
							CASE 
							WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
							THEN
								ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
							ELSE
								(CASE @tintKitAssemblyPriceBasedOn 
										WHEN 2 THEN ISNULL(I.monAverageCost,0) 
										WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
										ELSE WI.[monWListPrice]
								END) 
							END
						ELSE (CASE @tintKitAssemblyPriceBasedOn WHEN 2 THEN ISNULL(I.monAverageCost,0) ELSE I.[monListPrice] END)
					END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
			FROM 
				@TEMPitems ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.numItemCode = I.[numItemCode]
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode = V.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1 
					*
				FROM
					WareHouseItems WI
				WHERE 
					WI.numItemID=I.numItemCode 
					AND WI.numWareHouseID = @numWarehouseID
				ORDER BY
					WI.numWareHouseItemID
			) AS WI
		) T1
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCalculatedPriceForKitAssembly')
DROP FUNCTION GetCalculatedPriceForKitAssembly
GO
CREATE FUNCTION [dbo].[GetCalculatedPriceForKitAssembly]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
	)

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0)
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO 
			@TEMPSelectedKitChilds
		SELECT 
			OKI.numChildItemID,
			OKI.numWareHouseItemId,
			OKCI.numItemID,
			OKCI.numWareHouseItemId
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKI.numOppChildItemID = OKCI.numOppChildItemID
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
	END
	ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
	BEGIN
		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
		)
		SELECT 
			SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
			SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))
		FROM 
			dbo.SplitString(@vcSelectedKitChildItems,',')

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)
	END


	;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
	(
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(numUOMId,0)
		FROM 
			[ItemDetails] ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE   
			[numItemKitID] = @numItemCode
			AND 1 = (CASE WHEN ISNULL(I.bitKitParent,0) = 1 THEN (CASE WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) THEN 1 ELSE 0 END) ELSE 1 END)
		UNION ALL
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(ID.numUOMId,0)
		FROM 
			CTE As Temp1
		INNER JOIN
			[ItemDetails] ID
		ON
			ID.numItemKitID = Temp1.numItemCode
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE
			Temp1.bitKitParent = 1
			AND Temp1.bitCalAmtBasedonDepItems = 1
			AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID)
	)

	INSERT INTO @TEMPitems
	(
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	)
	SELECT
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	FROM
		CTE

	
	SELECT
		@monCalculatedPrice = (CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
	FROM
	(
		SELECT  
			ISNULL(CASE 
					WHEN I.[charItemType]='P' 
					THEN 
						CASE 
						WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
						THEN
							ISNULL(dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,''),0)
						ELSE
							(CASE @tintKitAssemblyPriceBasedOn 
									WHEN 2 THEN ISNULL(I.monAverageCost,0) 
									WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
									ELSE WI.[monWListPrice]
							END) 
						END
					ELSE (CASE @tintKitAssemblyPriceBasedOn WHEN 2 THEN ISNULL(I.monAverageCost,0) ELSE I.[monListPrice] END)
				END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
		FROM 
			@TEMPitems ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.numItemCode = I.[numItemCode]
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1 
				*
			FROM
				WareHouseItems WI
			WHERE 
				WI.numItemID=I.numItemCode 
				AND WI.numWareHouseID = @numWarehouseID
			ORDER BY
				WI.numWareHouseItemID
		) AS WI
	) T1


	RETURN @monCalculatedPrice
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0),
	  @tintCommitAllocation TINYINT,
	  @bitFromBizDoc NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @bitAssembly BIT
	DECLARE @bitKitParent BIT
	DECLARE @bitWorkOrder BIT

	SELECT 
		@bitAssembly=ISNULL(bitAssembly,0)
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitWorkOrder=ISNULL(bitWorkOrder,0) 
	FROM 
		OpportunityItems 
	INNER JOIN 
		Item 
	ON 
		OpportunityItems.numItemCode=Item.numItemCode 
	WHERE 
		numOppId=@numOppID AND numoppitemtCode=@numOppItemID

	IF @bitKitParent = 1
	BEGIN
		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,bitDropship,bitKitParent,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKI.numOppItemID,
			OI.bitDropShip,
			ISNULL(I.bitKitParent,0),
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1,
			(CASE 
				WHEN OKI.numWareHouseItemId > 0 AND ISNULL(OI.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,0,1,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
							ELSE
								CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,1,0,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li><b><span style="color:red">',I.vcItemName, ':</b> ','</span>')
							ELSE
								CONCAT('<li><b>',I.vcItemName, ':</b> ') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
									END) 
							END
					END 
				ELSE 
					(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN 
							CONCAT('<li><b>',I.vcItemName, ':</b> ')
						ELSE
							CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
					END)
			END)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId=OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		LEFT JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
		WHERE
			OM.numOppId = @numOppID
			AND OI.numoppitemtCode=@numOppItemID

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2,
			(CASE 
				WHEN OKCI.numWareHouseItemId > 0 AND ISNULL(c.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,0,1,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,1,0,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityKitChildItems OKCI
		LEFT JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppID
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID


		INSERT INTO @TEMPTABLEFINAL
		(
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		)
		SELECT
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		FROM 
			@TEMPTABLE 
		WHERE 
		tintLevel=1
		
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

		WHILE @i <= @iCount
		BEGIN
			IF (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 0 OR (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 1 AND (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0))
			BEGIN
				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))
			END

			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
			BEGIN
				--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

				SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)

				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</li>')
			END

			SET @i = @i + 1
		END

		SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	END
	ELSE IF @bitAssembly=1 AND @bitWorkOrder=1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPFINAL TABLE
		(
			ID INT IDENTITY(1,1)
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		SET @i  = 1

		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END



		INSERT INTO @TEMPFINAL
		(
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		)
		SELECT
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		FROM 
			@TEMP
		WHERE 
			ItemLevel=2
		
		SET @i = 1

		SELECT @iCount = COUNT(*) FROM @TEMPFINAL

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT
																																		(CASE 
																																			WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
																																			THEN 
																																				CONCAT(I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
																																			ELSE
																																				CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
																																		END)
																																	FROM
																																		@TEMPFINAL T1
																																	INNER JOIN
																																		Item I
																																	ON
																																		T1.numItemCode = I.numItemCode
																																	LEFT JOIN
																																		WareHouseItems WI
																																	ON
																																		T1.numWarehouseItemID = WI.numWareHouseItemID
																																	LEFT JOIN
																																		UOM
																																	ON
																																		UOM.numUOMId = (CASE WHEN ISNULL(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
																																	WHERE 
																																		ID = @i))

			SET @i = @i + 1
		END
	END
	

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_AddDelShippingExceptions' ) 
    DROP PROCEDURE USP_AddDelShippingExceptions
GO
CREATE PROCEDURE USP_AddDelShippingExceptions
   
     @numDomainID NUMERIC(9)
	,@numClassificationID NUMERIC(18, 0) 
	,@PercentAbove FLOAT 
	,@FlatAmt FLOAT 
	,@tintMode	TINYINT
	,@numShippingExceptionID NUMERIC(9) =0
AS 
BEGIN
      IF @tintMode = 0 
        BEGIN
			IF EXISTS(
						SELECT * FROM dbo.ShippingExceptions SE
						 WHERE SE.numClassificationID = @numClassificationID AND SE.numDomainID = @numDomainID
			          )
                BEGIN
	                  raiserror('DUPLICATE',16,1);
		              RETURN ;
                END		
			
			INSERT INTO ShippingExceptions
			(
				 numDomainID
				,numClassificationID
				,PercentAbove
				,FlatAmt
			)
			VALUES
			(
				 @numDomainID
				,@numClassificationID
				,@PercentAbove
				,@FlatAmt
			)
	END
	IF @tintMode = 1 
    BEGIN
        DELETE FROM dbo.ShippingExceptions
        WHERE numShippingExceptionID = @numShippingExceptionID 
            AND numDomainID=@numDomainID
    END	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkcodeforpromotion')
DROP PROCEDURE usp_checkcodeforpromotion
GO
CREATE PROCEDURE [dbo].[USP_CheckCodeForPromotion]
	 @numDomainID AS NUMERIC(18,0)
	,@txtCouponCode AS VARCHAR(100)
	,@numProdId AS NUMERIC(18,0) = 0
	,@numDivisionId AS NUMERIC(18,0)
AS
BEGIN	

	DECLARE @numCouponCode NUMERIC(18,0)
	DECLARE @intCodeUsageLimit NUMERIC(18,0)
	DECLARE @intCodeUsed INT
	DECLARE @ERRORMESSAGE AS VARCHAR(50)
	DECLARE @intUsedCountForDiv INT

	SET @numCouponCode = (SELECT COUNT(DC.vcDiscountCode) FROM PromotionOffer PO
							INNER JOIN DiscountCodes DC
							ON PO.numProId = DC.numPromotionID
							WHERE numProId = @numProdId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
							AND DC.vcDiscountCode = @txtCouponCode)

	IF @numCouponCode = 1
	BEGIN 

		SET @intCodeUsageLimit = (SELECT DC.CodeUsageLimit FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numProdId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
								AND DC.vcDiscountCode = @txtCouponCode)

		IF @intCodeUsageLimit = 0
		BEGIN 
			SELECT 1
		END
		ELSE IF @intCodeUsageLimit > 0
		BEGIN 

			SET @intUsedCountForDiv = (SELECT COUNT(DCU.numDivisionId) FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @txtCouponCode)

			IF @intUsedCountForDiv = 0
			BEGIN 
				SELECT 1
			END
			ELSE IF @intUsedCountForDiv > 0
			BEGIN 

				SET @intCodeUsed = (SELECT intCodeUsed FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @txtCouponCode)				
								
				IF @intCodeUsed < @intCodeUsageLimit
				BEGIN
					SELECT 1
				END
				ELSE
				BEGIN				
					SET @ERRORMESSAGE = 'CODE_USAGE_LIMIT_EXCEEDS' 

					RAISERROR(@ERRORMESSAGE,16,1)
					RETURN
				END
			END
		END
	END	

	IF @numCouponCode = 0
	BEGIN
		
		SET @ERRORMESSAGE = 'INVALID_CODE' 

		RAISERROR(@ERRORMESSAGE,16,1)
		RETURN

	END

END 
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
	FROM 
		UserMaster U                              
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	join
		UnitPriceApprover UPA
	ON
		D.numDomainID = UPA.numDomainID
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       
	IF (SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
					WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
			AND bitActive=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Priya Sharma
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneShippingRule')
DROP PROCEDURE Usp_CreateCloneShippingRule
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneShippingRule]
(
	 @numRuleId BIGINT OUTPUT
	,@vcRuleName VARCHAR(50)
	,@numDomainID NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
)
AS 

BEGIN
--Rule 	
--Shipping Charges
--Zip Codes
BEGIN TRY
BEGIN TRANSACTION

IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId) > 0
 BEGIN
 		RAISERROR ( 'DUPLICATE RULE',16, 1 )
 		RETURN ;
 END
ELSE IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE numDomainID = @numDomainID AND numRelationship = @numRelationship AND numProfile = @numProfile) > 0
 BEGIN
 		RAISERROR ( 'DUPLICATE RELATIONSHIP PROFILE',16, 1 )
 		RETURN ;
 END
 
 ELSE
BEGIN

	DECLARE @numNewRuleID AS BIGINT
	
	INSERT INTO ShippingRules
	(vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
	numRelationship,numProfile, numWareHouseID)
	SELECT @vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
	@numRelationship,@numProfile, numWareHouseID
	FROM ShippingRules WHERE numRuleID = @numRuleId AND numDomainID = @numDomainID
	 
	SELECT @numNewRuleID = SCOPE_IDENTITY()
	 
	INSERT INTO dbo.ShippingServiceTypes 
	(numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,numRuleID,numDomainID)
	SELECT numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,@numNewRuleID,numDomainID
	FROM dbo.ShippingServiceTypes WHERE numRuleID = @numRuleId AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ShippingRuleStateList( numRuleID,numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostalRange )
	SELECT @numNewRuleID, numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostalRange FROM ShippingRuleStateList WHERE numRuleID = @numRuleId

	SET @numRuleId = @numNewRuleID

END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
------- Created by Priya 

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteorderprodiscountcode')
DROP PROCEDURE usp_deleteorderprodiscountcode
GO
CREATE PROCEDURE [dbo].[USP_DeleteOrderProDiscountCode]
    @numProId AS NUMERIC(9) = 0,   
    @numDomainID AS NUMERIC(9) = 0,
	@numDiscountId AS NUMERIC(9),
	@numOppId AS NUMERIC(9),
	@numDivisionId AS NUMERIC(9)
AS 
BEGIN TRY
BEGIN TRANSACTION	
   
	   DELETE FROM DiscountCodes
	   WHERE numDiscountId = @numDiscountId AND numPromotionID = @numProId

	   DECLARE @intDiscountUsage AS INTEGER

	   SET @intDiscountUsage = (SELECT COUNT(intCodeUsed) FROM DiscountCodeUsage WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId)

	   IF @intDiscountUsage > 0
	   BEGIN

		 UPDATE DiscountCodeUsage
			SET intCodeUsed = @intDiscountUsage - 1
		 WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId

	   END

	   UPDATE OpportunityMaster 
			SET numDiscountID = NULL
		WHERE numOppId = @numOppId AND numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId AND numDomainId = @numDomainID

		DECLARE @numDiscountLineItem AS NUMERIC
		SET @numDiscountLineItem = (SELECT ISNULL(numShippingServiceItemID,0) FROM Domain WHERE numDomainId = @numDomainID)

		DELETE FROM OpportunityItems		
		WHERE numItemCode = @numDiscountLineItem AND numOppId = @numOppId
   	
		
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
      
  
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteShippingRule' ) 
    DROP PROCEDURE USP_DeleteShippingRule
GO
CREATE PROCEDURE USP_DeleteShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9)
AS 
BEGIN

	DELETE FROM dbo.PromotionOfferContacts WHERE numProId=@numRuleID AND tintRecordType=2
	DELETE FROM dbo.PromotionOfferItems WHERE numProId=@numRuleID AND tintRecordType=2
    
    DELETE FROM dbo.ShippingRuleStateList WHERE numRuleID = @numRuleID 
    DELETE FROM dbo.ShippingServiceTypes WHERE numRuleID =  @numRuleID
  
    DELETE  FROM dbo.ShippingRules
    WHERE   numRuleID = @numRuleID
            AND numDomainID = @numDomainID
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteShippingRuleStateList' ) 
    DROP PROCEDURE USP_DeleteShippingRuleStateList
GO
CREATE PROCEDURE USP_DeleteShippingRuleStateList
    @numDomainID NUMERIC,
    @numCountryID NUMERIC,
	@numStateID NUMERIC
AS 
BEGIN
 	
		DELETE FROM ShippingRuleStateList WHERE numDomainID = @numDomainID AND numCountryID = @numCountryID AND numStateID = @numStateID
 	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelPromotionOffer')
DROP PROCEDURE USP_DelPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_DelPromotionOffer]
	@numDomainID AS NUMERIC(18,0),
	@numProId AS NUMERIC(18,0)
AS
BEGIN TRY
BEGIN TRANSACTION
	
	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId 
	DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintRecordType IN (5,6)
	DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId		
	DELETE FROM PromotionOfferOrder WHERE numPromotionID=@numProId	
	DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId

	DELETE FROM DiscountCodes WHERE numPromotionID=@numProId
		
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCCInfo')
DROP PROCEDURE USP_GetCustomerCCInfo
GO
CREATE PROCEDURE [dbo].[USP_GetCustomerCCInfo]  
 @numCCInfoId as bigint=0,
@numContactId as bigint=0,
@bitdefault as int=0,
@numDivisionID NUMERIC(18,0) = 0
as
begin
	if @numCCInfoID=0
	begin
		IF @bitdefault=0
		BEGIN
			IF @numContactID = 0 AND @numDivisionID > 0
			BEGIN
				SELECT TOP 1 @numContactId = numContactID FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID ORDER BY bitPrimaryContact DESC
			END

			SELECT 
				numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
			FROM 
				CustomerCreditCardInfo 
			LEFT JOIN 
				ListDetails 
			ON 
				ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
			WHERE 
				numContactId=@numContactId
			ORDER BY 
				bitIsDefault DESC,numCCInfoID ASC
			-- IMPORTY DEFAULT CARD SOULD BE RETURN FIRST
		END
		ELSE
		BEGIN
			select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
			from CustomerCreditCardInfo left join ListDetails on ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
			where numContactId=@numContactId ORDER BY 
				bitIsDefault DESC,numCCInfoID ASC
			-- IMPORTY DEFAULT CARD SOULD BE RETURN FIRST
		end
	end
	else
	begin
		select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault , vcData
		from CustomerCreditCardInfo left join ListDetails on ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
		where numCCInfoID=@numCCInfoID
		order by numCCInfoID desc
	end
		
end
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatecouponcode')
DROP PROCEDURE usp_getduplicatecouponcode
GO
CREATE PROCEDURE [dbo].[USP_GetDuplicateCouponCode]
    @numProId AS NUMERIC(9) = 0, 
    @numDomainId AS NUMERIC(18,0),   
	@txtCouponCode VARCHAR(100)
	
AS 
BEGIN	
		SELECT   numDiscountID
				,vcDiscountCode
				,numPromotionID
				,vcProName 
		FROM PromotionOffer PO
		INNER JOIN DiscountCodes D ON D.numPromotionID = PO.numProId
		WHERE vcDiscountCode IN (
                    SELECT Id
                    FROM dbo.SplitIDs(@txtCouponCode, ',') )
					AND PO.numDomainId = @numDomainId
					AND	D.numPromotionID <> @numProId
					AND ISNULL(PO.bitEnabled, 0) = 1		

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
	FROM 
		ShippingServiceTypes 
	WHERE 
		numDomainID=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
END      






--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numDomainID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListID as numeric(9)
AS
BEGIN
	IF @byteMode=1
	BEGIN
		SELECT 
			numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON 
			L2.numListID=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID
			AND ISNULL(FieldRelationship.numModuleID,0) <> 14
		UNION
		SELECT 
			numFieldRelID,L1.vcItemName +' - ' + L2.vcItemName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID
			AND ISNULL(FieldRelationship.numModuleID,0) = 14
	END
	ELSE IF @byteMode=2
	BEGIN

		SELECT 
			numFieldRelID
			,ISNULL(FieldRelationship.numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcListName +' - ' + L2.vcListName AS RelationshipName
			,L1.vcListName as Item1
			,L2.vcListName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON 
			L2.numListID=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(FieldRelationship.numModuleID,0) <> 14
		UNION
		SELECT 
			numFieldRelID
			,ISNULL(numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcItemName +' - ' + L2.vcItemName AS RelationshipName
			,L1.vcItemName as Item1
			,L2.vcItemName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(numModuleID,0) = 14

		IF @numPrimaryListItemID >0 
		BEGIN
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcData AS PrimaryListItem
				,L2.vcData AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				ListDetails L1
			ON 
				L1.numListItemID=numPrimaryListItemID
			JOIN 
				ListDetails L2
			ON 
				L2.numListItemID=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND numPrimaryListItemID=@numPrimaryListItemID
				AND ISNULL(numModuleID,0) <> 14
			UNION
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcItemName AS PrimaryListItem
				,L2.vcItemName AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListItemID
			JOIN 
				Item L2
			ON 
				L2.numItemCode=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND numPrimaryListItemID=@numPrimaryListItemID
				AND ISNULL(numModuleID,0) = 14
		END
	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID
	end
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

------- Created by Priya 
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getorderproaudittraildetails')
DROP PROCEDURE usp_getorderproaudittraildetails
GO
CREATE PROCEDURE [dbo].[USP_GetOrderProAuditTrailDetails]
    @numProId AS NUMERIC(9) = 0,   
    @numDomainID AS NUMERIC(9) = 0,
	@numItemCode AS NUMERIC(9) = 0
AS 
BEGIN
   SELECT DC.vcDiscountCode, DC.numDiscountId, vcPOppName, OM.numOppID, OM.numDivisionId,
   (
        SELECT 
			CASE WHEN COUNT(OBDI.numOppBizDocID) > 0 THEN 'Yes'
			ELSE 'No'
			END
		
        FROM OpportunityMaster OM
		INNER JOIN OpportunityItems OI
		ON OM.numOppId = OI.numOppId
		INNER JOIN OpportunityBizDocItems OBDI
		ON OI.numItemCode = OBDI.numItemCode
		INNER JOIN OpportunityBizDocs OBD
		ON OBDI.numOppBizDocID = OBD.numBizDocId
		WHERE OBDI.numItemCode = @numItemCode 
       
    ) As HasBizDoc
   
   FROM OpportunityMaster OM
   INNER JOIN DiscountCodes DC
   ON OM.numDiscountID = DC.numDiscountId
   WHERE OM.numDomainId = @numDomainID
	    AND DC.numPromotionID = @numProId
   	
END      
  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotiondrulefororder')
DROP PROCEDURE usp_getpromotiondrulefororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotiondRuleForOrder]
	 @numDomainID NUMERIC(18,0)
	,@numSubTotal FLOAT = 0
	,@numDivisionID NUMERIC(18,0)
AS
BEGIN	

	DECLARE @numProID AS NUMERIC

	SET @numProID = (
					SELECT TOP 1 PO.numProId		 
					FROM PromotionOffer PO	
					INNER JOIN PromotionOfferOrganizations PORG
					ON PO.numProId = PORG.numProId
					INNER JOIN CompanyInfo C
					ON PORG.numProfile = C.vcProfile and PORG.numRelationship = C.numCompanyType
					INNER JOIN DivisionMaster DM
					ON C.numCompanyId = DM.numCompanyID
					WHERE DM.numDivisionID = @numDivisionID
					AND PO.numDomainId = @numDomainID AND PO.IsOrderBasedPromotion = 1
					AND  1 = (CASE WHEN ISNULL(PO.bitNeverExpires,0) = 1
								THEN 1
									WHEN ISNULL(PO.bitNeverExpires,0) = 0 
									THEN 
									CASE WHEN ((CAST(PO.dtValidFrom as DATE) <= CAST(GETDATE() as DATE) AND CAST(PO.dtValidTo as DATE) >= CAST(GETDATE() as DATE)))
									THEN 1	
									ELSE 0 END
								ELSE 0 END )
				)


	SELECT * FROM PromotionOffer WHERE numProId = @numProID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numProID

END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT  
			[numProId]
			,[vcProName]
			,[numDomainId]
			,[dtValidFrom]
			,[dtValidTo]
			,[bitNeverExpires]
			,[bitApplyToInternalOrders]
			,[bitAppliesToSite]
			,[bitRequireCouponCode]
			,[txtCouponCode]
			,[tintUsageLimit]
			--,[bitFreeShiping]
			--,[monFreeShippingOrderAmount]
			--,[numFreeShippingCountry]
			,[bitDisplayPostUpSell]
			,[intPostSellDiscount]
			--,[bitFixShipping1]
			--,[monFixShipping1OrderAmount]
			--,[monFixShipping1Charge]
			--,[bitFixShipping2]
			--,[monFixShipping2OrderAmount]
			--,[monFixShipping2Charge]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId
    END  

	ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			numProId,
            vcProName,
			(CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate(dtValidFrom,@numDomainID),' to ',dbo.FormatedDateFromDate(dtValidTo,@numDomainID)) END) AS vcDateValidation,

			(CASE WHEN ISNULL(bitRequireCouponCode,0) = 1 THEN
			              (CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
								 STUFF((SELECT CONCAT (',' , vcDiscountCode , ' (', ISNULL(DCU.intCodeUsed,0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = PO.numProId
									   FOR XML PATH ('')), 1, 1, '')   
						      ELSE CONCAT(txtCouponCode,' (', ISNULL(intCouponCodeUsed,0),')') END)
						  
			ELSE '' END) AS vcCouponCode			
			
			,(CASE WHEN ISNULL(bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
						CONCAT('Buy ', 
							CASE tintDiscountType WHEN 1 THEN
										STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
										   FROM PromotionOfferOrder POO					   
										   WHERE POO.numPromotionID =  PO.numProId
										   FOR XML PATH ('')), 1, 1, '')  
									WHEN 2 THEN 
										STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
												   FROM PromotionOfferOrder POO					   
												   WHERE POO.numPromotionID =  PO.numProId
												   FOR XML PATH ('')), 1, 1, '')
							ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,bitEnabled, tintCustomersBasedOn, tintOfferTriggerValueTypeRange, fltOfferTriggerValueRange, [IsOrderBasedPromotion]
        FROM  
			PromotionOffer PO
		WHERE 
			numDomainID = @numDomainID
			
		ORDER BY
			dtCreated DESC
    END
	  
   	
END      
  
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotionofferdetailsfororder')
DROP PROCEDURE usp_getpromotionofferdetailsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferDetailsForOrder] 

@numPromotionID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(9)=0

AS
BEGIN
	SELECT   [numProId]
			,[vcProName]
			,[numDomainId]
			,[dtValidFrom]
			,[dtValidTo]
			,[bitNeverExpires]
			,[bitApplyToInternalOrders]
			,[bitAppliesToSite]
			,[bitRequireCouponCode]
			--,[txtCouponCode]
			--,[tintUsageLimit]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintDiscountType]
			,[IsOrderBasedPromotion]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numPromotionID AND numDomainId = @numDomainID

	SELECT numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numPromotionID AND numDomainId = @numDomainID

	SELECT DISTINCT numDiscountId, CodeUsageLimit,
                STUFF((SELECT ',' + vcDiscountCode 
                       FROM DiscountCodes  
                       WHERE numPromotionID = @numPromotionID
                       FOR XML PATH ('')), 1, 1, '') AS vcDiscountCode
	FROM DiscountCodes
	WHERE numPromotionID = @numPromotionID;
	
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingExceptions' ) 
    DROP PROCEDURE USP_GetShippingExceptions
GO
CREATE PROCEDURE USP_GetShippingExceptions
   
     @numDomainID NUMERIC(9)
	
AS 
BEGIN
    
	SELECT numShippingExceptionID,
		(SELECT vcData from ListDetails LD JOIN ShippingExceptions SE ON LD.numListItemID = SE.numClassificationID 
							WHERE LD.numListItemID = SE.numClassificationID) AS vcClassification
		,PercentAbove
		,FlatAmt
		
	 FROM ShippingExceptions WHERE numDomainID = @numDomainID

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingexceptionsfororder')
DROP PROCEDURE usp_getshippingexceptionsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetShippingExceptionsForOrder]
	 @numDomainID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX)
	,@ShippingAmt Float
	,@bitFreeShipping BIT
AS
BEGIN	

	--DECLARE @vcItemCodes VARCHAR(MAX) = '8,240'
  
  DECLARE @tempExceptions TABLE
    (
		SNO INT IDENTITY(1,1),
		vcItemName VARCHAR(200),
		bitApplyException BIT,
		FlatAmt FLOAT,
		numItemCode NUMERIC
    )


	INSERT INTO @tempExceptions (vcItemName, bitApplyException, FlatAmt, numItemCode)
		SELECT '',0,0, id 
		FROM  dbo.SplitIDs(@vcItemCodes, ',')  
	
--select * from 	@tempExceptions	

	DECLARE @totalRecords INT
	DECLARE @I INT

	SELECT @I = 1
	SELECT @totalRecords = COUNT(numItemCode) FROM @tempExceptions
	WHILE (@I <= @totalRecords)
	BEGIN
		DECLARE @numclassification NUMERIC

		SET @numclassification = (SELECT I.numItemClassification FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I )

		UPDATE @tempExceptions
		SET vcItemName = (SELECT I.vcItemName FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I),

		bitApplyException = (SELECT CASE WHEN  (SELECT COUNT(numClassificationID) FROM ShippingExceptions WHERE numDomainID = @numDomainID AND numClassificationID = @numclassification) > 0 
											THEN 1
											ELSE 0 END
											),
		FlatAmt = (SELECT 
						CASE WHEN @ShippingAmt > 0 THEN
							(SELECT (@ShippingAmt * PercentAbove)/100 FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						WHEN @ShippingAmt = 0 AND @bitFreeShipping = 1 THEN
						(SELECT FlatAmt FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						ELSE 0 END )
		WHERE SNO = @I

	SET @I = @I + 1 

	END						
				
		SELECT vcItemName, bitApplyException, FlatAmt, numItemCode FROM @tempExceptions WHERE bitApplyException = 1
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRelationshipsProfiles' ) 
    DROP PROCEDURE USP_GetShippingRelationshipsProfiles
GO
CREATE PROCEDURE USP_GetShippingRelationshipsProfiles
   
    @numDomainID NUMERIC(9)
AS 
BEGIN
    SELECT 
	(SELECT CONCAT( (SELECT vcData from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ' / ' , 
								(SELECT vcData from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)
							)) AS vcRelProfile,

	(SELECT CONCAT( (SELECT numListItemID from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , '-' , 
								(SELECT numListItemID from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)
							)) AS numRelProfileID

	FROM ShippingRules SR 
	WHERE numDomainID = @numDomainID
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRule' ) 
    DROP PROCEDURE USP_GetShippingRule
GO
CREATE PROCEDURE USP_GetShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9),
    @byteMode AS TINYINT,
	@numRelProfileID AS VARCHAR(50)
AS 
BEGIN
    IF @byteMode = 0 
        BEGIN  
            SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
                             ELSE tintFixedShippingQuotesMode END 
						AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
                    
            FROM    dbo.ShippingRules SR
            WHERE   ( numRuleID = @numRuleID
                      OR @numRuleID = 0
                    )
                    AND numDomainID = @numDomainID
        END
    ELSE IF @byteMode = 1 
    BEGIN 
		DECLARE @numRelationship AS NUMERIC
		DECLARE @numProfile AS NUMERIC
				
		SET @numRelationship=(parsename(replace(@numRelProfileID,'-','.'),2))
		SET @numProfile=(parsename(replace(@numRelProfileID,'-','.'),1))

		SELECT minShippingCost,bitEnableStaticShippingRule FROM Domain WHERE numDomainID = @numDomainID

		IF @numRelationship > 0 AND @numProfile > 0
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom
                , (SELECT STUFF((SELECT ', ' + dbo.fn_GetState(numStateID) + ' ,' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					join ShippingRules S on ss.numRuleID = s.numRuleID
					WHERE SR.numRuleID = SS.numRuleID			
					FOR XML PATH ('')),1,2,'')) AS vcShipTo
				, (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', monRate)
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                        
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID AND numRelationship = @numRelationship AND numprofile = @numProfile
		END

		ELSE
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom

                ,  STUFF((SELECT  ', ' + dbo.fn_GetState(numStateID)  + (
					CASE WHEN LEN( STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
											WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SS.numCountryID and s.numStateID = ss.numStateID FOR XML PATH ('')),1,1,'')) > 0
						THEN CONCAT (' (',STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
												WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SS.numCountryID and s.numStateID = ss.numStateID FOR XML PATH ('')),1,1,''), ') ')
						ELSE '' END)
					FROM ShippingRuleStateList SS					
					WHERE SR.numRuleID = SS.numRuleID
					GROUP BY numCountryID,numStateID
					FOR XML PATH ('')),1,2,'') AS vcShipTo

				, (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', monRate)
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                         
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID  
		END

    END 
      ELSE IF @byteMode = 2 --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            BEGIN
				SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = @numRuleID AND numDomainID = @numDomainID
			END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingruledetails')
DROP PROCEDURE usp_getshippingruledetails
GO
CREATE PROCEDURE [dbo].[USP_GetShippingRuleDetails]
	 @numDomainID NUMERIC(18,0)
	,@numWareHouseId VARCHAR(20)
	,@ZipCode VARCHAR(20)
	,@StateId NUMERIC(18,0)	
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
AS
BEGIN	
	DECLARE @numRuleID AS NUMERIC

	IF ISNULL(@numSiteID,0) > 0 AND ISNULL(@numDivisionID,0) = 0
	BEGIN
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SELECT 
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM 
			eCommerceDTL 
		WHERE 
			numSiteId=@numSiteID

		SET @numRuleID = (SELECT TOP 1 
							SR.numRuleID		 
						FROM 
							ShippingRules SR						
						INNER JOIN 
							ShippingRuleStateList SRS
						ON 
							SR.numRuleID = SRS.numRuleID
						WHERE 
							SR.numProfile = @numProfile
							AND SR.numRelationship = @numRelationship
							AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
							AND SR.numDomainId = @numDomainID 
							AND ((SRS.numStateID = @StateId AND ISNULL(SRS.vcZipPostal,'') = '') OR (SRS.numStateID = @StateId AND (@ZipCode LIKE (SRS.vcZipPostal + '%'))))
						ORDER BY
							(CASE 
								WHEN (SRS.numStateID = @StateId AND (@ZipCode LIKE (SRS.vcZipPostal + '%'))) THEN 1
								WHEN (SRS.numStateID = @StateId AND ISNULL(SRS.vcZipPostal,'') = '') THEN 2 Else 3
							END)
					)
	END
	ELSE
	BEGIN
		SET @numRuleID = (SELECT TOP 1 
							SR.numRuleID		 
						FROM 
							ShippingRules SR	
						INNER JOIN 
							CompanyInfo C
						ON 
							SR.numProfile = C.vcProfile 
							AND SR.numRelationship = C.numCompanyType					
						INNER JOIN 
							ShippingRuleStateList SRS
						ON 
							SR.numRuleID = SRS.numRuleID
						INNER JOIN 
							DivisionMaster DM
						ON 
							C.numCompanyId = DM.numCompanyID
						WHERE 
							DM.numDivisionID = @numDivisionID
							AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
							AND SR.numDomainId = @numDomainID 
							AND ((SRS.numStateID = @StateId AND ISNULL(SRS.vcZipPostal,'') = '') OR (SRS.numStateID = @StateId AND (@ZipCode LIKE (SRS.vcZipPostal + '%'))))
						ORDER BY
							(CASE 
								WHEN (SRS.numStateID = @StateId AND (@ZipCode LIKE (SRS.vcZipPostal + '%'))) THEN 1
								WHEN (SRS.numStateID = @StateId AND ISNULL(SRS.vcZipPostal,'') = '') THEN 2 Else 3
							END)
					)
	END

	SELECT * FROM ShippingRules WHERE numRuleID = @numRuleID 

	SELECT ROW_NUMBER() OVER(ORDER BY numServiceTypeID ASC) AS RowNum, * FROM ShippingServiceTypes WHERE numRuleID = @numRuleID 
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRuleStateList' ) 
    DROP PROCEDURE USP_GetShippingRuleStateList
GO
CREATE PROCEDURE USP_GetShippingRuleStateList
    @numDomainID NUMERIC,
    @numRuleID NUMERIC
AS 
BEGIN
 	
    SELECT DISTINCT  numStateID,
            numCountryID,
            
            dbo.fn_GetListItemName(numCountryID) vcCountry,
            dbo.fn_GetState(numStateID) vcState,
            numRuleID,
            numDomainID,
           
			STUFF((SELECT  ', ' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					WHERE SS.numStateID = SL.numStateID	AND numRuleID = 20079					
			FOR XML PATH ('')),1,2,'') AS vcZipPostalRange
    FROM    dbo.ShippingRuleStateList SL
    WHERE   numRuleID = @numRuleID
            AND numDomainID = @numDomainID
			GROUP BY numStateID, numRuleStateID,numCountryID,numRuleID,numDomainID,vcZipPostal
 	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingrulewarehouseslist')
DROP PROCEDURE usp_getshippingrulewarehouseslist
GO
CREATE PROCEDURE USP_GetShippingRuleWarehousesList			
	@numDomainID NUMERIC(9)
AS 
BEGIN
	
	SELECT vcWareHouse, numWareHouseID FROM Warehouses W
	WHERE numDomainID = @numDomainID

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingServiceTypes' ) 
    DROP PROCEDURE USP_GetShippingServiceTypes
GO
CREATE PROCEDURE USP_GetShippingServiceTypes
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @numServiceTypeID NUMERIC = 0,
    @tintMode TINYINT = 0
AS 
BEGIN
	
    --IF NOT EXISTS ( SELECT  *
    --                FROM    dbo.ShippingServiceTypes
    --                WHERE   numDomainID = @numDomainID
    --                        AND numRuleID = @numRuleID ) 
    --    BEGIN
    --        INSERT  INTO dbo.ShippingServiceTypes ( numShippingCompanyID,
				--									vcServiceName,
    --                                                intNsoftEnum,
    --                                                intFrom,
    --                                                intTo,
    --                                                fltMarkup,
    --                                                bitMarkupType,
    --                                                monRate,
    --                                                bitEnabled,
    --                                                numRuleID,
    --                                                numDomainID )
    --                SELECT  numShippingCompanyID,
				--			vcServiceName,
    --                        intNsoftEnum,
    --                        intFrom,
    --                        intTo,
    --                        fltMarkup,
    --                        bitMarkupType,
    --                        monRate,
    --                        bitEnabled,
    --                        @numRuleID,
    --                        @numDomainID
    --                FROM    dbo.ShippingServiceTypes
    --                WHERE   numDomainID = 0
    --    END

    IF @tintMode = 2 
    BEGIN

			IF NOT EXISTS ( SELECT  * FROM    dbo.ShippingServiceTypes
								WHERE numDomainID = @numDomainID
									AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
						  ) 
				BEGIN
					INSERT INTO dbo.ShippingServiceTypes
					(	numShippingCompanyID,
						vcServiceName,
						intNsoftEnum,
						intFrom,
						intTo,
						fltMarkup,
						bitMarkupType,
						monRate,
						bitEnabled,
						numRuleID,
						numDomainID 
					)
					SELECT  numShippingCompanyID,
									vcServiceName,
									intNsoftEnum,
									intFrom,
									intTo,
									fltMarkup,
									bitMarkupType,
									monRate,
									bitEnabled,
									NULL,
									@numDomainID
					FROM    dbo.ShippingServiceTypes
					WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
				END

            		SELECT DISTINCT * FROM (SELECT	numServiceTypeID,
										vcServiceName,
										intNsoftEnum,
										intFrom,
										intTo,
										fltMarkup,
										bitMarkupType,
										monRate,
										bitEnabled,
										numRuleID,
										numDomainID,
										numShippingCompanyID,
										CASE WHEN numShippingCompanyID = 91 THEN 'Fedex' 
												WHEN numShippingCompanyID = 88 THEN 'UPS'
												WHEN numShippingCompanyID = 90 THEN 'USPS'
												ELSE 'Other'
										END AS vcShippingCompany	 	
									FROM    dbo.ShippingServiceTypes
									WHERE   numDomainID = @numDomainID
											AND ISNULL(numRuleID,0) = 0
											AND ( numServiceTypeID = @numServiceTypeID
													OR @numServiceTypeID = 0
												)
												AND intNsoftEnum > 0
													
									UNION ALL
										
									SELECT	101,'Amazon Standard',101,0,0,0,1,0,1,0,0,0,'Amazon' vcShippingCompany	 	
										
									UNION ALL
										
									SELECT	102,'ShippingMethodStandard',102,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 		
										
									UNION ALL
										
									SELECT	103,'Other',103,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 	
													
									) TABLE1 WHERE (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0) ORDER BY vcShippingCompany
    END
      IF @tintMode=1
      BEGIN
	  	  SELECT  numServiceTypeID,
                    vcServiceName,
                    intNsoftEnum,
                    intFrom,
                    intTo,
                    fltMarkup,
                    bitMarkupType,
                    monRate,
                    bitEnabled,
                    numRuleID,
                    numDomainID
            FROM    dbo.ShippingServiceTypes
            WHERE   numDomainID = @numDomainID
                    AND numRuleID = @numRuleID
                    AND ( numServiceTypeID = @numServiceTypeID
                          OR @numServiceTypeID = 0
                        )
                    AND ISNULL(intNsoftEnum,0)=0
	  END  
        

END
/****** Object:  StoredProcedure [dbo].[USP_GetState]    Script Date: 07/26/2008 16:18:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstate')
DROP PROCEDURE usp_getstate
GO
CREATE PROCEDURE [dbo].[USP_GetState]       
@numDomainID as numeric(9)=0,    
@numCountryID as numeric(9)=0,
@vcAbbreviations AS VARCHAR(300)=''      
AS
BEGIN
	IF LEN(ISNULL(@vcAbbreviations,'')) > 0 AND ISNULL(@numCountryID,0) > 0
	BEGIN
		SELECT 
			numStateID 
		FROM 
			State  
		JOIN 
			ListDetails 
		ON 
			numListItemID = numCountryID     
		WHERE 
			State.numDomainID=@numDomainID 
			AND numCountryID=@numCountryID 
			AND @vcAbbreviations IN (vcAbbreviations,vcState)
		ORDER BY 
			vcState
	END
	ELSE IF LEN(@vcAbbreviations) > 0 AND ISNULL(@numCountryID,0) = 0
	BEGIN
		SELECT 
			numStateID
			,numCountryID
			,vcState
		FROM 
			State  
		JOIN 
			ListDetails 
		ON 
			numListItemID = numCountryID     
		WHERE 
			State.numDomainID=@numDomainID 
			AND (LOWER(ISNULL(vcAbbreviations,'')) = LOWER(@vcAbbreviations) OR LOWER(ISNULL(vcState,'')) = LOWER(@vcAbbreviations))
		ORDER BY 
			vcState
	END
	ELSE
	BEGIN   
		SELECT 
			numStateID
			,vcState
			,numCountryID
			,LD.vcData
			,vcAbbreviations
			,ISNULL(numShippingZone,0) AS numShippingZone
			,ISNULL(LDShippingZone.vcData,'') AS vcShippingZone
		FROM 
			State  
		JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = numCountryID     
		LEFT JOIN
			ListDetails LDShippingZone
		ON
			State.numShippingZone = LDShippingZone.numListItemID
		WHERE 
			State.numDomainID = @numDomainID 
			AND numCountryID=@numCountryID
		ORDER BY 
			vcState
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Priya 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstateforchangezip')
DROP PROCEDURE usp_getstateforchangezip
GO
CREATE PROCEDURE [dbo].[USP_GetStateForChangeZip]       
@numDomainID as numeric(9)=0,    
@vcState AS VARCHAR(300)=''      
as     

BEGIN   
	SELECT 
		numStateID
		,vcState
		
	FROM 
		State  	
	WHERE 
		State.numDomainID = @numDomainID 
		AND vcState = @vcState
order by  vcState
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuseddiscountcodes')
DROP PROCEDURE usp_getuseddiscountcodes
GO
CREATE PROCEDURE [dbo].[USP_GetUsedDiscountCodes] 

@numPromotionID AS NUMERIC(9)=0

AS
BEGIN
	

	SELECT  CodeUsageLimit, vcDiscountCode
                
	FROM DiscountCodes DC
	INNER JOIN DiscountCodeUsage DCU
	ON DC.numDiscountId = DCU.numDiscountId
	WHERE numPromotionID = @numPromotionID
	AND DCU.intCodeUsed > 0;
	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserSMTPDetails')
DROP PROCEDURE USP_GetUserSMTPDetails
GO
CREATE PROCEDURE [dbo].[USP_GetUserSMTPDetails]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
  SELECT       
 S.[numDomainId]
      ,S.[numUserId]
      ,S.[vcSMTPUserName]
      ,S.[vcSMTPPassword]
      ,S.[vcSMTPServer]
      ,S.[numSMTPPort]
      ,S.[bitSMTPSSL]
      ,S.[bitSMTPServer]
      ,S.[bitSMTPAuth]
FROM SMTPUserDetails S JOIN Domain D on D.numDomainID =  S.numDomainID      
   WHERE  S.numUserId=(CASE WHEN ISNULL(@numUserID,0)=0 THEN -1 ELSE @numUserID END) AND S.numDomainID=@numDomainID                            
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			tintGroupType=1 -- Application Users                    
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,monHourlyRate
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,ISNULL(bitSMTPAuth,ISNULL(@bitSMTPAuth,0)) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,ISNULL([vcSMTPServer],ISNULL(@vcSMTPServer,'')) vcSMTPServer
			,ISNULL(numSMTPPort,ISNULL(@numSMTPPort,0)) numSMTPPort
			,ISNULL(bitSMTPSSL,ISNULL(@bitSMTPSSL,0)) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterList')
DROP PROCEDURE USP_GetWorkFlowMasterList
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowMasterList]      
@numDomainId as numeric(9),  
@numFormID NUMERIC(18,0), 
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr  as Varchar(50)   
    
as       
    SET NOCOUNT ON
     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
      numWFID NUMERIC(18,0)                                                       
 )     
declare @strSql as varchar(8000)                                                  
    
set  @strSql='Select numWFID from WorkFlowMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
    
if @SortChar<>'0' set @strSql=@strSql + ' And vcWFName like '''+@SortChar+'%'''     

if @numFormID <> 0 set @strSql=@strSql + ' And numFormID = '+ convert(varchar(15),@numFormID) +''   

if @SearchStr<>'' set @strSql=@strSql + ' And (vcWFName like ''%'+@SearchStr+'%'' or 
vcWFDescription like ''%'+@SearchStr+'%'') ' 
    
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
insert into #tempTable(numWFID) exec(@strSql)    
    
 declare @firstRec as integer                                                        
 declare @lastRec as integer                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                                                        
 set @lastRec= (@CurrentPage*@PageSize+1)                                                         

 SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
Select ROW_NUMBER()  OVER (ORDER BY ID ASC) AS 'RowNo', WM.numWFID,WM.numDomainID,WM.vcWFName,WM.vcWFDescription,dbo.fn_StripHTML(WM.vcWFAction) AS vcWFAction ,CASE WHEN wm.bitActive=1 THEN 'Active' ELSE 'InActive' END AS Status,WM.numCreatedBy,
		WM.vcDateField,WM.intDays,WM.intActionOn,WM.numModifiedBy,WM.dtCreatedDate,WM.dtModifiedDate,WM.bitActive,WM.numFormID,WM.tintWFTriggerOn,DFM.vcFormName,CASE WHEN (WM.tintWFTriggerOn=1 AND wm.intDays=0)THEN 'Create' WHEN WM.tintWFTriggerOn=2 THEN 'Edit' WHEN WM.tintWFTriggerOn=3 THEN 'Date Field' WHEN WM.tintWFTriggerOn=4 THEN 'Fields Update' WHEN WM.tintWFTriggerOn=5 THEN 'Delete' WHEN WM.tintWFTriggerOn=6 THEN 'A/R Aging' ELSE 'NA' end AS TriggeredOn
from WorkFlowMaster WM join #tempTable T on T.numWFID=WM.numWFID
JOIN DynamicFormMaster DFM ON WM.numFormID=DFM.numFormID AND DFM.tintFlag=3
   WHERE ID > @firstRec and ID < @lastRec order by ID
   
   DROP TABLE #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,NULLIF(@numIncomeChartAcntId,0)
			,NULLIF(@numAssetChartAcntId,0)
			,NULLIF(@numCOGsChartAcntId,0)
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=NULLIF(@numCOGsChartAcntId,0),numAssetChartAcntId=NULLIF(@numAssetChartAcntId,0),                                      
			numIncomeChartAcntId=NULLIF(@numIncomeChartAcntId,0),fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitApplyToInternalOrders BIT,
	@bitAppliesToSite BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,

	--@tintOfferTriggerValueType TINYINT,
	--@fltOfferTriggerValue INT,
	--@tintOfferBasedOn TINYINT,
	--@tintDiscountType TINYINT,
	--@fltDiscountValue FLOAT,
	--@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	--@tintOfferTriggerValueTypeRange TINYINT,
	--@fltOfferTriggerValueRange FLOAT
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
	
	
--BEGIN TRY
--BEGIN TRANSACTION	

		--IF ISNULL(@bitAppliesToSite,0)=1 AND ISNULL(@numProId,0) > 0 AND (SELECT COUNT(*) FROM PromotionOfferSites WHERE numPromotionID=@numProId) = 0
		--BEGIN
		--	RAISERROR ( 'SELECT_SITES',16, 1 )
 	--		RETURN;
		--END

		--IF @tintOfferBasedOn <> 4
		--BEGIN
		--	IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
		--	BEGIN
		--		RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 	--			RETURN;
		--	END
		--END

		--IF ISNULL(@tintDiscoutBaseOn,0) <> 3 AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintDiscoutBaseOn,0) AND tintRecordType=6) = 0
		--BEGIN
		--	RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 	--		RETURN;
		--END

		/*Check For Duplicate Customer-Item relationship 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn				
				 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			DECLARE @ERRORMESSAGE AS VARCHAR(50)
			SET @ERRORMESSAGE = 'RELATIONSHIPPROFILE ALREADY EXISTS'							
					
			RAISERROR(@ERRORMESSAGE,16,1)
			RETURN

		END

		IF ISNULL(@bitAppliesToSite,0) = 0
		BEGIN
			DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitApplyToInternalOrders = @bitApplyToInternalOrders
			,bitAppliesToSite = @bitAppliesToSite
			,bitRequireCouponCode = @bitRequireCouponCode			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		CREATE TABLE Tempdata
		(
			numPromotionID NUMERIC,
			numDomainID NUMERIC,
			numOrderAmount VARCHAR(MAX),
			fltDiscountValue VARCHAR(MAX)
		)

		INSERT Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

		;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
		(
			SELECT
				numPromotionID,
				numDomainID,
				LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
				STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM Tempdata
			UNION all

			SELECT
				numPromotionID,
				numDomainID,
				LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
				STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM tmp
			WHERE
				OrderAmount > '' AND fltDiscountValue > ''
		)

		INSERT INTO PromotionOfferOrder
		SELECT
			numPromotionID,				
			numOrderAmount,
			numfltDiscountValue,
			numDomainID

		FROM tmp
				
		DROP TABLE Tempdata
---------------------------Insert Discount Codes-------------------------------------------------------------------

		DECLARE @CouponCountEntered AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC WHERE numPromotionID = @numProId )
		DECLARE @CouponCountINTable AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC 
										 WHERE numPromotionID = @numProId 
												AND vcDiscountCode IN  (
													SELECT  Id
													FROM    dbo.SplitIDs(@txtCouponCode, ',') )) 

	IF (@CouponCountEntered <> @CouponCountINTable )

	BEGIN

			DELETE DC FROM DiscountCodes DC			 
			 WHERE numPromotionID = @numProId 
					AND DC.numDiscountId NOT IN (SELECT numDiscountId FROM DiscountCodeUsage)
		
		IF @bitRequireCouponCode = 1
			BEGIN

				CREATE TABLE DiscountTempdata
				(
					numPromotionID NUMERIC,
					CodeUsageLimit TINYINT,
					vcDiscountCode VARCHAR(MAX)			
				)

				INSERT DiscountTempdata SELECT @numProId, @tintUsageLimit, @txtCouponCode

				;WITH tmp1(numPromotionID, CodeUsageLimit, vcDiscountCode, DiscountCode) AS
			(
				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(vcDiscountCode, CHARINDEX(',', vcDiscountCode + ',') - 1),
					STUFF(vcDiscountCode, 1, CHARINDEX(',', vcDiscountCode + ','), '')
				
				FROM DiscountTempdata
				UNION all

				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(DiscountCode, CHARINDEX(',', DiscountCode + ',') - 1),
					STUFF(DiscountCode, 1, CHARINDEX(',', DiscountCode + ','), '')
				FROM tmp1
				WHERE
					DiscountCode > '' 
			)	

			INSERT INTO DiscountCodes
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM tmp1
			WHERE tmp1.vcDiscountCode NOT IN ( SELECT vcDiscountCode FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId)
				
			DROP TABLE DiscountTempdata		

		END	
	END	
	
	ELSE
	BEGIN
		UPDATE DiscountCodes
		SET CodeUsageLimit = @tintUsageLimit
		WHERE numPromotionID = @numProId
	END
		       
        SELECT  @numProId

		---------------------------------------------------------------
--	COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage1 NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	DELETE FROM PromotionOffer WHERE numProId = @numProId
--	DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId
--	DELETE FROM DiscountCodes WHERE numPromotionID = @numProId
--	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId


--	SELECT 
--		@ErrorMessage1 = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage1, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH


	END
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetCalculatedPrice')
DROP PROCEDURE USP_Item_GetCalculatedPrice
GO
CREATE PROCEDURE [dbo].[USP_Item_GetCalculatedPrice]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcChildKitSelectedItem VARCHAR(MAX)
AS
BEGIN
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)

	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE  numSiteId=@numSiteID
	

	IF ISNULL(@numWarehouseID,0) > 0 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID)
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END
	ELSE 
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode ORDER BY numWareHouseItemID ASC
	END


	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	SELECT @tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numItemCode=@numItemCode

	SELECT
		ISNULL(monPrice,0) monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numItemCode,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,@vcChildKitSelectedItem)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetChildKits')
DROP PROCEDURE USP_Item_GetChildKits
GO
CREATE PROCEDURE [dbo].[USP_Item_GetChildKits]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS  
BEGIN
	SELECT 
		I.numItemCode
		,I.vcItemName
	FROM
		ItemDetails ID
	INNER JOIN
	 	Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE
		I.numDomainID=@numDomainID
		AND ID.numItemKitID = @numItemCode
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetKits')
DROP PROCEDURE USP_Item_GetKits
GO
CREATE PROCEDURE [dbo].[USP_Item_GetKits]
	@numDomainID NUMERIC(18,0)
AS  
BEGIN
	SELECT 
		I.numItemCode
		,I.vcItemName
	FROM
		Item I
	WHERE
		I.numDomainID=@numDomainID
		AND ISNULL(I.bitKitParent,0) = 1
		AND (SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode	WHERE numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1) = 0
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON; 
	DECLARE @numMaxOrder INT
	SET @numMaxOrder = ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemCode),0)
	 

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,tintOrder INT
	)

	INSERT INTO  @TEMP
	(	
		numItemCode
		,tintOrder
	)
	SELECT
		Item.numItemCode
		,1
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
	WHERE
		ItemDetails.numItemKitID = @numItemCode
		AND (SELECT 
				COUNT(*) 
			FROM 
				FieldRelationship FR 
			WHERE 
				FR.numDomainID=@numDomainID 
				AND FR.numModuleID = 14 
				AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails ID WHERE ID.numItemKitID=@numItemCode) 
				AND FR.numSecondaryListID=Item.numItemCode) = 0
	ORDER BY
		ISNULL(NULLIF(sintOrder,0),@numMaxOrder + 1),numItemDetailID


	DECLARE @TEMPDependedKits TABLE
	(
		numParentKitItemCode NUMERIC(18,0)
		,numChildKitItemCode NUMERIC(18,0)
	)

	INSERT INTO @TEMPDependedKits
	(
		numParentKitItemCode
		,numChildKitItemCode
	)
	SELECT
		numPrimaryListID
		,numSecondaryListID
	FROM
		FieldRelationship FR
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
	GROUP BY
		numPrimaryListID
		,numSecondaryListID


	;WITH CTE (ID,numItemCode,tintOrder) AS
	(
		SELECT
			T1.ID
			,T1.numItemCode
			,T1.tintOrder
		FROM
			@TEMP T1
		UNION ALL
		SELECT 
			C1.ID
			,T2.numChildKitItemCode
			,C1.tintOrder + 1
		FROM
			@TEMPDependedKits T2
		INNER JOIN	
			ItemDetails
		ON
			T2.numChildKitItemCode = ItemDetails.numChildItemID
			AND ItemDetails.numItemKitID = @numItemCode
		INNER JOIN
			CTE C1
		ON
			T2.numParentKitItemCode=C1.numItemCode
	)
  
	SELECT
		Item.numItemCode,
		Item.vcItemName,
		ISNULL(Item.bitKitSingleSelect,0) AS bitKitSingleSelect,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numSecondaryListID)
				FROM 
					FieldRelationship FR
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14
					AND numPrimaryListID=Item.numItemCode
				GROUP BY
					numSecondaryListID
				FOR XML PATH('')),1,1,''),'') vcDependedKits,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numPrimaryListID)
				FROM 
					FieldRelationship FR 
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14 
					AND numSecondaryListID=Item.numItemCode
				GROUP BY
					numPrimaryListID
				FOR XML PATH('')),1,1,''),'') vcParentKits
	FROM
		CTE C1
	INNER JOIN
		Item
	ON
		C1.numItemCode = Item.numItemCode
	ORDER BY
		C1.ID,C1.tintOrder
END  
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_GetDependedChildKitItems')
DROP PROCEDURE USP_ItemDetails_GetDependedChildKitItems
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_GetDependedChildKitItems]  
	@numDomainID NUMERIC(18,0)
	,@numMainKitItemCode NUMERIC(18,0)
	,@numChildKitItemCode NUMERIC(18,0)
	,@numChildKitSelectedItem VARCHAR(MAX)
AS  
BEGIN  
	SELECT 
		numSecondaryListID AS numChildKitItemCode
		,ISNULL(STUFF((SELECT 
						CONCAT(',',numSecondaryListID)
					FROM 
						FieldRelationship FRInner 
					WHERE 
						FRInner.numDomainID=@numDomainID
						AND FRInner.numModuleID = 14
						AND FRInner.numPrimaryListID=@numChildKitItemCode
					GROUP BY
						numSecondaryListID
					FOR XML PATH('')),1,1,''),'') vcDependedKits
	FROM
		FieldRelationship FR
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FR.numPrimaryListID=@numChildKitItemCode
		AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0
	ORDER BY
		FR.numSecondaryListID


	SELECT 
		FR.numSecondaryListID AS numChildKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		CONCAT(Item.vcItemName,' (Item Code:',Item.numItemCode,', SKU:',Item.vcSKU,')') AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty
	FROM
		FieldRelationshipDTL FRD
	INNER JOIN
		FieldRelationship FR
	ON
		FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN
		ItemDetails
	ON
		FR.numSecondaryListID = ItemDetails.numItemKitID
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND Item.numItemCode = FRD.numSecondaryListItemID
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FR.numPrimaryListID=@numChildKitItemCode
		AND FRD.numPrimaryListItemID IN (SELECT Id FROM dbo.SplitIDs(@numChildKitSelectedItem,','))
	ORDER BY 
		sintOrder
END
GO
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   
	   
	DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	
	IF(@vcCookieId!=null OR @vcCookieId!='')   
	BEGIN                    
		SET @PromotionOffers=(SELECT TOP 1 
								CASE 
									WHEN P.tintDiscountType=1  
									THEN (I.monListPrice*fltDiscountValue)/100
									WHEN P.tintDiscountType=2
									THEN fltDiscountValue
									WHEN P.tintDiscountType=3
									THEN fltDiscountValue*I.monListPrice
									ELSE 0 
								END
							FROM CartItems AS C
							LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
							LEFT JOIN Item I ON I.numItemCode=C.numItemCode
							WHERE 
								I.numItemCode=@numItemCode 
								AND I.numItemCode=@numDomainID 
								AND C.vcCookieId=@vcCookieId 
								AND C.numUserCntId=@numUserCntID 
								AND C.fltDiscount=0 
								AND (I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) 
									OR I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
								AND P.numDomainId=@numDomainID 
								AND ISNULL(bitEnabled,0)=1 
								AND ISNULL(bitAppliesToSite,0)=1 
								AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
	END
	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql=' With tblItem AS (SELECT 
										I.numItemCode
										,vcItemName
										,txtItemDesc
										,charItemType
										,ISNULL(I.bitKitParent,0) bitKitParent
										,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
										,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,I.numItemCode,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE ' +  
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0)' END)
										+'END) AS monListPrice
										,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,I.numItemCode,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

										from Item I ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										+ '                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
																							  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID='+ convert(varchar(15),@numDomainID) + ' AND I.numItemCode='+ convert(varchar(15),@numItemCode) +
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										+ '           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT @strSql
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9)
AS
BEGIN
	if @byteMode =1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID AND (numModuleID IS NULL OR numModuleID=@numModuleID))
		BEGIN
			INSERT INTO FieldRelationship
			(
				numPrimaryListID
				,numSecondaryListID
				,numDomainID
				,numModuleID
			)
			VALUES
			(
				@numPrimaryListID
				,@numSecondaryListID
				,@numDomainID
				,@numModuleID
			)
		
			SET @numFieldRelID=@@identity 
		END
		ELSE
		BEGIN
			SET @numFieldRelID=0
		END 
	END
	else if @byteMode =2
	begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

	end
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingGlobalValues' ) 
    DROP PROCEDURE USP_ManageShippingGlobalValues
GO
CREATE PROCEDURE USP_ManageShippingGlobalValues   
     @numDomainID NUMERIC(9)
	,@minShippingCost FLOAT
	,@bitEnableStaticShipping BIT
AS 
BEGIN
      
	IF EXISTS(
				SELECT * FROM dbo.Domain 
					WHERE numDomainID = @numDomainID
			    )
        BEGIN
	           UPDATE Domain
				   SET	 minShippingCost = @minShippingCost
						,bitEnableStaticShippingRule = @bitEnableStaticShipping  
				WHERE numDomainID = @numDomainID  
        END	
		
	ELSE
		BEGIN
			INSERT INTO Domain
			(
				 numDomainID
				,minShippingCost
				,bitEnableStaticShippingRule
			)
			VALUES
			(
				 @numDomainID
				,@minShippingCost
				,@bitEnableStaticShipping
			)
		END
			
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingRule')
DROP PROCEDURE USP_ManageShippingRule
GO
CREATE PROCEDURE USP_ManageShippingRule
			@numRuleID NUMERIC(9),
			@vcRuleName VARCHAR(50),
		   	@vcDescription VARCHAR(1000),
		   	@tintBasedOn tinyint,
		   	@tinShippingMethod tinyint,
		   	--@tintAppliesTo tinyint,
		   	--@tintContactsType tinyint,
		   	@tintType TINYINT,
		   	@numSiteID NUMERIC(9),
		   	@numDomainID NUMERIC(9),
		   	@tintTaxMode TINYINT,
		   	@tintFixedShippingQuotesMode TINYINT,
			@bitFreeshipping BIT,
			@FreeShippingAmt INT = NULL,
			@numRelationship NUMERIC(9),
			@numProfile NUMERIC(9),
			@strWarehouseIds VARCHAR(1000)
AS 
BEGIN
	
	
IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId AND numSiteId=@numSiteId AND numRuleID<>@numRuleID)>0
 BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
 END
ELSE IF (SELECT COUNT(*) FROM dbo.ShippingRules WHERE numRelationship = @numRelationship AND numProfile = @numProfile AND numDomainId=@numDomainId AND numRuleID <> @numRuleID) > 0
BEGIN
 		RAISERROR ( 'DUPLICATE RELATIONSHIP AND PROFILE',16, 1 )
 		RETURN ;
 END

    IF ISNULL(@numRuleID,0) = 0
        BEGIN  
           INSERT INTO dbo.ShippingRules (	
				 vcRuleName
                ,vcDescription
                ,tintBasedOn
                ,tinShippingMethod
                --tintAppliesTo,
				--tintContactsType,
                ,tintIncludeType
                ,numSiteID
                ,numDomainID
                ,tintTaxMode
                ,tintFixedShippingQuotesMode 
				,bitFreeShipping
				,FreeShippingOrderAmt
				,numRelationship
				,numProfile
				,numWareHouseID
				)
           VALUES   (
                      @vcRuleName
                      ,@vcDescription
                      ,@tintBasedOn
                      ,@tinShippingMethod
                      --@tintAppliesTo,
                     --@tintContactsType,
                      ,@tintType
                      ,@numSiteID
                      ,@numDomainID 
                      ,@tintTaxMode 
                      ,@tintFixedShippingQuotesMode
					  ,@bitFreeshipping
					  ,CASE WHEN @bitFreeshipping = 0 THEN NULL Else @FreeShippingAmt End
					  ,@numRelationship
					  ,@numProfile
					  ,@strWarehouseIds
		   	  )
		   
           SELECT  numRuleID = SCOPE_IDENTITY()
  
        END  
    ELSE 
        BEGIN  
          
			   UPDATE   dbo.ShippingRules
			   SET      vcRuleName = @vcRuleName
						,vcDescription = @vcDescription
						,tintBasedOn = @tintBasedOn
						,tinShippingMethod = @tinShippingMethod
						--tintAppliesTo = @tintAppliesTo,
						--tintContactsType = @tintContactsType,
						,tintIncludeType = @tintType 
						,tintTaxMode = @tintTaxMode
                        ,tintFixedShippingQuotesMode = @tintFixedShippingQuotesMode
						,bitFreeShipping = @bitFreeshipping
						,FreeShippingOrderAmt = (CASE WHEN @bitFreeshipping = 0 THEN NULL Else @FreeShippingAmt End)
						,numRelationship = @numRelationship
						,numProfile = @numProfile
						,numWareHouseID = @strWarehouseIds
			   WHERE    numRuleID = @numRuleID
						AND numDomainID = @numDomainID
						AND numSiteID = @numSiteID
			          

            SELECT  @numRuleID
        END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingRuleStates' ) 
    DROP PROCEDURE USP_ManageShippingRuleStates
GO
CREATE PROCEDURE USP_ManageShippingRuleStates
    --@numRuleStateID NUMERIC,    
    @numCountryID NUMERIC,
    @numStateID NUMERIC,
    @numRuleID NUMERIC,
    @numDomainID NUMERIC,
    --@tintMode	TINYINT,
    --@vcFromZip	VARCHAR(20),
    --@vcToZip	VARCHAR(20),
    --@tintFlag	TINYINT,
    --@numSiteId  NUMERIC,
    --@tinShippingMethod INT ,
	@vcZipPostalRange VARCHAR(200)
AS 
BEGIN
	
   -- IF @tintFlag = 0 
       -- BEGIN
			--IF EXISTS(
			--			SELECT * FROM dbo.ShippingRules SR LEFT JOIN dbo.ShippingRuleStateList SRSL
			--			 on  SR.numRuleID = SRSL.numRuleId WHERE SR.numSiteId = @numSiteId AND SRSL.numCountryId=@numCountryId AND SRSL.numStateId = @numStateID AND SR.numDomainID = @numDomainID AND tinShippingMethod = @tinShippingMethod
			--          )
   --             BEGIN
	  --                raiserror('PRIMARY',16,1);
		 --             RETURN ;
   --             END			
			--ELSE
			IF LEN(@vcZipPostalRange) = 0
			BEGIN
				IF EXISTS (
							 SELECT * FROM dbo.ShippingRules SR LEFT JOIN dbo.ShippingRuleStateList SRSL ON  SR.numRuleID = SRSL.numRuleId 
								WHERE SRSL.numRuleID = @numRuleID AND SRSL.numCountryId=@numCountryId 
								AND SRSL.numStateId = @numStateID AND SR.numDomainID = @numDomainID 
							)
                BEGIN
	                  RAISERROR('DUPLICATE STATE',16,1);
		              RETURN ;
                END
			END
			ELSE IF LEN(@vcZipPostalRange) > 0
			BEGIN
				IF EXISTS (
							 SELECT * FROM dbo.ShippingRules SR LEFT JOIN dbo.ShippingRuleStateList SRSL ON  SR.numRuleID = SRSL.numRuleId 
								WHERE SRSL.numRuleID = @numRuleID AND SRSL.numCountryId=@numCountryId 
								AND SRSL.numStateId = @numStateID AND SR.numDomainID = @numDomainID AND ISNULL(vcZipPostal,'') IN (SELECT Id FROM dbo.SplitIDs(@vcZipPostalRange, ',') )
							)
                BEGIN
	                  RAISERROR('DUPLICATE STATE AND ZIP',16,1);
		              RETURN ;
                END
			END

			DECLARE @temp TABLE
			(
				numCountryID NUMERIC,
				numStateID NUMERIC,
				numRuleID NUMERIC,
				numDomainID NUMERIC
			)

			INSERT INTO @temp (numCountryID, numStateID, numRuleID, numDomainID)
			VALUES (@numCountryID, @numStateID, @numRuleID, @numDomainID)					

			INSERT INTO ShippingRuleStateList
			(
				numCountryID,				
				numStateID,
				numRuleID,
				numDomainID,
				vcZipPostal
			)

			SELECT * from @temp
			OUTER APPLY(
							SELECT  Id
							FROM    dbo.SplitIDs(@vcZipPostalRange, ',') 
						) temp									
				
				
				
  	  --  END
    --IF @tintFlag = 1 
    --    BEGIN
    --        DELETE  FROM dbo.ShippingRuleStateList
    --        WHERE   numRuleStateID = @numRuleStateID 
    --          AND numDomainID=@numDomainID
    --    END	
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID <> @numWFID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID
			AND numWFID <> @numWFID
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
				
				EXCEPT

				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2				
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	IF EXISTS (SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND bitActive=1)
	BEGIN
		DECLARE @numRule2OrderStatus NUMERIC(18,0)
		DECLARE @numRule3OrderStatus NUMERIC(18,0)
		DECLARE @numRule4OrderStatus NUMERIC(18,0)

		SELECT
			@numRule2OrderStatus= (CASE WHEN bitRule2IsActive = 1 THEN numRule2OrderStatus ELSE 0 END)
			,@numRule3OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule3OrderStatus ELSE 0 END)
			,@numRule4OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule4OrderStatus ELSE 0 END)
		FROM
			SalesFulfillmentConfiguration
		WHERE 
			numDomainID=@numDomainID 
			AND bitActive=1

		IF EXISTS (SELECT * FROM WorkFlowConditionList WHERE numFieldID=101 AND vcFilterValue IN (@numRule2OrderStatus,@numRule3OrderStatus,@numRule4OrderStatus) AND [numWFID] = @numWFID)
		BEGIN
			RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
			RETURN -1
		END
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetOppAddressDetails')
DROP PROCEDURE USP_OPPGetOppAddressDetails
GO
CREATE PROCEDURE [dbo].[USP_OPPGetOppAddressDetails]
(
    @numOppId AS NUMERIC(18,0) = NULL,
    @numDomainID AS NUMERIC(18,0)  = 0,
	@numOppBizDocID AS NUMERIC(18,0) = 0
)
AS

  DECLARE  @tintOppType  AS TINYINT, @tintBillType  AS TINYINT, @tintShipType  AS TINYINT
 
  DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC,@numContactID AS NUMERIC, @numBillToAddressID NUMERIC(18,0), @numShipToAddressID NUMERIC(18,0),@numVendorAddressID NUMERIC(18,0)
      
  SELECT  @tintOppType = tintOppType,
		  @tintBillType = tintBillToType,
		  @tintShipType = tintShipToType,
          @numDivisionID = numDivisionID,
          @numContactID = numContactID,
		  @numBillToAddressID = ISNULL(numBillToAddressID,0),
		  @numShipToAddressID = ISNULL(numShipToAddressID,0),
		  @numVendorAddressID= ISNULL(numVendorAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId


	 IF @tintOppType=2
		BEGIN
			--************Vendor Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcBillStreet,'') + '</pre>' + isnull(AD.vcBillCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numBillState),'') + ' ' + isnull(AD.vcBillPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcFullAddress,
					isnull(AD.vcBillStreet,'') AS vcStreet,
					isnull(AD.vcBillCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numBillState),'') AS vcState,
					isnull(AD.vcBillPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcBillCompanyName) > 0 THEN AD.vcBillCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numBillCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltBillingContact,0) = 1 
						THEN ISNULL(vcAltBillingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numBillingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numBillingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId=DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID=@numDomainID AND DivisionMaster.numCompanyID=AD.numBillCompanyId AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE 
							WHEN ISNULL(bitAltContact,0) = 1 
							THEN ISNULL(vcAltContact,'') 
							ELSE 
								(CASE 
									WHEN ISNULL(numContact,0) > 0 
									THEN 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
									ELSE 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
									END
								)  END) AS vcContact
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			END

			--************Vendor Shipping Address************
			IF @numVendorAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numVendorAddressID
			END
			ELSE IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcShipStreet,'') + '</pre>' + isnull(AD.vcShipCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numShipState),'') + ' ' + isnull(AD.vcShipPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcFullAddress,
					isnull(AD.vcShipStreet,'') AS vcStreet,
					isnull(AD.vcShipCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numShipState),'') AS vcState,
					isnull(AD.vcShipPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcShipCompanyName) > 0 THEN AD.vcShipCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numShipCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltShippingContact,0) = 1 
						THEN ISNULL(vcAltShippingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numShippingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numShippingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numShipCompanyID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			
			 --************Employer Billing Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' +isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			 
			 
			 --************Employer Shipping Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
	  END

	IF @tintOppType=1 
		BEGIN
			--************Customer Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			END
		
			--************Customer Shipping Address************
			IF (SELECT ISNULL(bitDropShipAddress,0)  FROM OpportunityMaster WHERE numOppId = @numOppId AND numDomainId = @numDomainID) = 1
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
			ELSE IF ISNULL(@numOppBizDocID,0) > 0 AND (SELECT COUNT(*) FROM (SELECT DISTINCT numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode WHERE OBDI.numOppBizDocID=@numOppBizDocID AND ISNULL(OI.numShipToAddressID,0) > 0) TEMP) = 1 AND @tintShipType <> 2
			BEGIN
				SET @numShipToAddressID = (SELECT TOP 1 numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode WHERE OBDI.numOppBizDocID=@numOppBizDocID AND ISNULL(OI.numShipToAddressID,0) > 0)

				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
	
			--************Employer Billing Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	  END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- ORDER BASED Promotion Logic-------------

DECLARE @numDiscountID NUMERIC(18,0) = NULL
IF @numPromotionId > 0
Begin

		IF ISNULL(@PromCouponCode,'') = ''
		BEGIN

			SET @numDiscountID =  (SELECT DC.numDiscountId FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numPromotionId AND numDomainId = @numDomainID)
		END

		ELSE IF ISNULL(@PromCouponCode,'') <> ''
		BEGIN
			DECLARE @numCouponCode NUMERIC(18,0)
			DECLARE @intCodeUsageLimit NUMERIC(18,0)
			DECLARE @intCodeUsed INT		
			DECLARE @intUsedCountForDiv INT

			SET @numCouponCode = (SELECT COUNT(DC.vcDiscountCode) FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
								AND DC.vcDiscountCode = @PromCouponCode)
		
			IF @numCouponCode = 0 
			BEGIN	 
				RAISERROR('INVALID_CODE',16,1)
				RETURN
			END
			ELSE IF @numCouponCode = 1
			BEGIN 

				SET @numDiscountID =  (SELECT DC.numDiscountId FROM PromotionOffer PO
									INNER JOIN DiscountCodes DC
									ON PO.numProId = DC.numPromotionID
									WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
									AND DC.vcDiscountCode = @PromCouponCode)

				SET @intCodeUsageLimit = (SELECT DC.CodeUsageLimit FROM PromotionOffer PO
									INNER JOIN DiscountCodes DC
									ON PO.numProId = DC.numPromotionID
									WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
									AND DC.vcDiscountCode = @PromCouponCode)


				--IF @intCodeUsageLimit > 0
				--BEGIN 
				SET @intUsedCountForDiv = (SELECT COUNT(DCU.numDivisionId) FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)			

					IF @intUsedCountForDiv = 0
					BEGIN
						INSERT INTO DiscountCodeUsage
						(numDiscountId, numDivisionId, intCodeUsed)
						VALUES
						(@numDiscountID, @numDivisionId, 1)
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit = 0
					BEGIN
						UPDATE DiscountCodeUsage
							SET intCodeUsed = @intCodeUsed + 1
						WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit > 0
					BEGIN
						SET @intCodeUsed = (SELECT intCodeUsed FROM DiscountCodeUsage DCU
											INNER JOIN DiscountCodes DC
											ON  DCU.numDiscountId = DC.numDiscountId
											WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)

						IF @intCodeUsed < @intCodeUsageLimit
						BEGIN				
							UPDATE DiscountCodeUsage
								SET intCodeUsed = @intCodeUsed + 1
							WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
						END
						ELSE
						BEGIN	
							RAISERROR('CODE_USAGE_LIMIT_EXCEEDS',16,1)
							RETURN
						END
					END
			--	END
			END	
		END

				
END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			                
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numOppItemID NUMERIC(18,0),
					numItemCode NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numoppitemtCode,
					numItemCode,
					vcChildKitSelectedItems
				FROM
					OpportunityItems
				WHERE
					numOppId = @numOppID
					AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO @TempKitConfiguration
				SELECT
					numoppitemtCode,
					numItemCode,
					vcChildKitSelectedItems
				FROM
					OpportunityItems OI
				WHERE
					numOppId = @numOppID
					AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UniversalSMTPIMAP_Get')
DROP PROCEDURE dbo.USP_UniversalSMTPIMAP_Get
GO
CREATE PROCEDURE [dbo].[USP_UniversalSMTPIMAP_Get]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT
		vcSMTPServer
		,numSMTPPort
		,bitSMTPSSL
		,bitSMTPAuth
		,vcIMAPServer
		,numIMAPPort
		,bitIMAPSSL
		,bitIMAPAuth
	FROM
		UniversalSMTPIMAP
	WHERE
		numDomainID=@numDomainID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UniversalSMTPIMAP_Save')
DROP PROCEDURE dbo.USP_UniversalSMTPIMAP_Save
GO
CREATE PROCEDURE [dbo].[USP_UniversalSMTPIMAP_Save]
(
	@numDomainID NUMERIC(18,0),
    @vcSMTPServer VARCHAR(100),
	@numSMTPPort NUMERIC(18,0),
	@bitSMTPSSL BIT,
	@bitSMTPAuth BIT,
	@vcIMAPServer VARCHAR(100),
	@numIMAPPort NUMERIC(18,0),
	@bitIMAPSSL BIT,
	@bitIMAPAuth BIT
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM UniversalSMTPIMAP WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			UniversalSMTPIMAP
		SET
			vcSMTPServer=@vcSMTPServer
			,numSMTPPort=@numSMTPPort
			,bitSMTPSSL=@bitSMTPSSL
			,bitSMTPAuth=@bitSMTPAuth
			,vcIMAPServer=@vcIMAPServer
			,numIMAPPort=@numIMAPPort
			,bitIMAPSSL=@bitIMAPSSL
			,bitIMAPAuth=@bitIMAPAuth
		WHERE
			numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		INSERT INTO UniversalSMTPIMAP
		(
			numDomainID
			,vcSMTPServer
			,numSMTPPort
			,bitSMTPSSL
			,bitSMTPAuth
			,vcIMAPServer
			,numIMAPPort
			,bitIMAPSSL
			,bitIMAPAuth
		)
		VALUES
		(
			@numDomainID
			,@vcSMTPServer
			,@numSMTPPort
			,@bitSMTPSSL
			,@bitSMTPAuth
			,@vcIMAPServer
			,@numIMAPPort
			,@bitIMAPSSL
			,@bitIMAPAuth
		)
	END
END
GO


/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                               
AS                  
BEGIN                     
	CREATE TABLE #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                   
		numUserID VARCHAR(15),                  
		vcEmailID varchar(100),
		vcEmailAlias VARCHAR(100),
		vcEmailAliasPassword VARCHAR(100),                  
		[Name] VARCHAR(100),                  
		vcGroupName varchar(100),                  
		vcDomainName varchar(50),          
		Active varchar(10),
		numDefaultWarehouse numeric(18,0)              
	)                  

	DECLARE @strSql AS VARCHAR(8000)                  
	SET @strSql='SELECT 
					numUserID
					,ISNULL(vcEmailID,''-'') as vcEmailID
					,ISNULL(vcEmailAlias,'''') as vcEmailAlias
					,ISNULL(vcEmailAliasPassword,'''') vcEmailAliasPassword
					,ISNULL(ADC.vcfirstname,'''') + '' '' + ISNULL(ADC.vclastname,'''') AS Name
					,ISNULL(vcGroupName,''-'') AS vcGroupName
					,vcDomainName
					,CASE WHEN bitActivateFlag=1 THEN ''Active'' ELSE ''Suspended'' END AS bitActivateFlag
					,numDefaultWarehouse                          
				FROM 
					UserMaster                    
				JOIN 
					Domain                      
				ON 
					UserMaster.numDomainID =  Domain.numDomainID                     
				LEFT JOIN 
					AdditionalContactsInformation ADC                    
				ON 
					ADC.numContactid=UserMaster.numUserDetailId                   
				LEFT JOIN 
					AuthenticationGroupMaster GM                   
				ON 
					Gm.numGroupID= UserMaster.numGroupID              
				WHERE 
					UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status         
                  
	IF @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

	IF @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

	IF @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   
	INSERT INTO #tempTable
	(                  
		numUserID,                  
		vcEmailID,
		vcEmailAlias,
		vcEmailAliasPassword,               
		[Name],                  
		vcGroupName,                  
		vcDomainName,
		Active,
		numDefaultWarehouse
	)                  
	EXEC (@strSql)                   
                  
	DECLARE @firstRec AS INTEGER                   
	DECLARE @lastRec AS INTEGER                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateEmailAlias')
DROP PROCEDURE dbo.USP_UserMaster_UpdateEmailAlias
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateEmailAlias]
(
	@numDomainID NUMERIC(18,0),
	@numUserID NUMERIC(18,0),
    @vcEmailAlias VARCHAR(100),
	@vcSMTPPassword VARCHAR(100)
)
AS
BEGIN
	UPDATE
		UserMaster
	SET
		vcEmailAlias=@vcEmailAlias
		,vcEmailAliasPassword=(CASE WHEN LEN(ISNULL(@vcEmailAlias,'')) > 0 THEN @vcSMTPPassword ELSE vcSmtpPassword END)
	WHERE
		numDomainID=@numDomainID
		AND numUserId=@numUserID
END
GO