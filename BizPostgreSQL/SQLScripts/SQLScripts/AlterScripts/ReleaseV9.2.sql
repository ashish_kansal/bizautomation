/******************************************************************
Project: Release 9.2 Date: 21.FEBRUARY.2018
Comments: ALTER SCRIPTS
*******************************************************************/

UPDATE CFw_Grp_Master SET Grp_Name='Default Settings' WHERE Grp_Name='Accounting' AND Loc_Id=13




ALTER table PromotionOfferOrderBased ADD bitEnabled bit
/*********************************************************************Admin - Price Rules************************************************************************************************/
declare @numPageNavID numeric(18), @numPMPageNavID numeric(18),@numParentID numeric(18)

select @numPageNavID=numPageNavID from PageNavigationDTL where vcPageNavName like '%Price Rules%'
select @numParentID=numPageNavID from PageNavigationDTL where vcPageNavName = 'Administration'

update PageNavigationDTL
set vcPageNavName='Price Rules', vcImageUrl='~/images/PriceRules.png',numParentID=@numParentID
where numPageNavID = @numPageNavID
--where vcPageNavName like '%Price Rules%'

select @numPMPageNavID=numPageNavID from PageNavigationDTL where vcPageNavName like '%Promotion management%'


update PageNavigationDTL
set vcImageUrl='~/images/Admin-Price-Control.png',numParentID=@numParentID
where numPageNavID = @numPMPageNavID