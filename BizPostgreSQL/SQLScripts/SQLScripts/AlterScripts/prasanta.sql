CREATE TABLE EcommerceBlogs(
	numBlogsID integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 252153 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
	numDomainId integer NULL,
	numSiteId integer NULL,
	vcContent text NULL,
	vcTitle text NULL,
	vcUrl varchar(5000) NULL,
 CONSTRAINT PK_EcommerceBlogs PRIMARY KEY 
(
	numBlogsID
)
)


CREATE TABLE EcommercePages(
	numContentID integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 252153 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
	numDomainId integer NULL,
	numSiteId integer NULL,
	vcContent text NULL,
	vcUrl varchar(5000) NULL,
 CONSTRAINT PK_ContentManagement PRIMARY KEY 
(
	numContentID
)
) 

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO PageElementMaster
--(
--	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
--)
--VALUES
--(
--	(SELECT MAX(numElementID) FROM PageElementMaster)+1,'OrderOverview','~/UserControls/OrderOverview.ascx','{#OrderOverview#}',1,0,0,0
--)
--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType,bitEditor
--)
--VALUES
--(
--	(SELECT MAX(numElementID) FROM PageElementMaster)+1,'Html Customize','HtmlEditor',1
--)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitDefaultProfileURL  BIT
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitInventoryInvoicing BIT DEFAULT 0

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--UPDATE Domain SET bitAutolinkUnappliedPayment=1
--ALTER TABLE ProjectsMaster ADD bitDisplayTimeToExternalUsers BIT DEFAULT 0
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO GenericDocuments
--(
--	VcFileName, vcDocName, numDocCategory, cUrlType, vcFileType, numDocStatus, vcDocDesc, numDomainID, numCreatedBy, 
--	bintCreatedDate, numModifiedBy, bintModifiedDate, vcSubject, vcDocumentSection, numRecID, tintCheckOutStatus, intDocumentVersion, 
--	numLastCheckedOutBy, dtCheckOutDate, dtCheckInDate, tintDocumentType, numOldSpecificDocID, numModuleID, vcContactPosition, BizDocOppType, 
--	BizDocType, BizDocTemplate, vcGroupsPermission, numCategoryId, bitLastFollowupUpdate, numFollowUpStatusId, bitUpdateFollowupStatus, numFormFieldGroupId
--)
--SELECT 
--	'#SYS#EMAIL_ALERT:PROJECT_TASK_SUMMARY', 'Email Alert: Project Summary Report', 369, '', '', 0, '<div class="modal-body" style="font-size:16px !important">
--   <center>
--      <h3> Project Work Completed</h3>
--   </center>
--   <label style="padding-left:10px;" id="lblProjectProgressName">##ProjectName##</label>:  &nbsp;<span id="lblProjectProgressDescription">##ProjectDescription##</span><br>
--   <label style="padding-left:10px;">For: &nbsp;</label><span id="lblProjectProgressCompanyFor">##ProjectCompanyFor##</span><br>
--   <label style="padding-left:10px;">By: &nbsp;</label><span id="lblProjectProgressCompanyBy">##ProjectCompanyBy##</span><br>
--   {##ProjectLineTask## }
--   <div id="divProjectProgressTaskDetails">
--      <div style="margin-top:5px;padding:10px;background-color:##color##">
--         <div class="pull-left">
--            <div class="form-group" style="margin-bottom: 0px;">
--               <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblProjectProgressTotalProgress">##TotalProgress##%</span></label>
--               <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
--                  <div class="progress-bar progress-bar-primary divProjectProgressTotalProgress" style="width: ##TotalProgress##%;"></div>
--               </div>
--            </div>
--         </div>
--         <div class="pull-right"><label>On</label>:<span>##FinishDate##</span> <label>From</label>: <span id="">##DurationTakenForProject##</span>, <span id="">##StartTiime##</span><label> &nbsp;To:</label> <span id="">##FinishTime## </span></div>
--         <div class="clearfix"></div>
--         <label>Work Description: </label><span>##TaskName##</span> <br><label>By: </label><span>##AssignedTo##</span> <br><label>Balance Remaining: </label><span>##BalanceTime##</span> <br>
--      </div>
--   </div>
--   {##/ProjectLineTask## }
--</div>'
--	, numDomainID, 0, GETUTCDATE(), 0, GETUTCDATE(), 'Project Summary Report For ##ProjectName##', NULL, NULL, NULL, NULL, 
--	NULL, NULL, NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
--FROM Domain
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitEnableSmartyStreets BIT DEFAULT 0
--ALTER TABLE Domain ADD vcSmartyStreetsAPIKeys VARCHAR(500) NULL

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--INSERT INTO GenericDocuments
--(
--	VcFileName, vcDocName, numDocCategory, cUrlType, vcFileType, numDocStatus, vcDocDesc, numDomainID, numCreatedBy, 
--	bintCreatedDate, numModifiedBy, bintModifiedDate, vcSubject, vcDocumentSection, numRecID, tintCheckOutStatus, intDocumentVersion, 
--	numLastCheckedOutBy, dtCheckOutDate, dtCheckInDate, tintDocumentType, numOldSpecificDocID, numModuleID, vcContactPosition, BizDocOppType, 
--	BizDocType, BizDocTemplate, vcGroupsPermission, numCategoryId, bitLastFollowupUpdate, numFollowUpStatusId, bitUpdateFollowupStatus, numFormFieldGroupId
--)
--SELECT 
--	'#SYS#EMAIL_ALERT:TIMECONTRACT_USED', 'Email Alert: Time Contract Used', 369, '', '', 0, 'Hi ##CustomerContactName##<br/> Here is a summary of what we did today.<br/><br/><b>Work Description:</b> ##TaskTitle##<br/><b>Duration & Schedule:</b> ##TaskDuration##,##TaskStartEndTime##, on ##TaskDate##<br/><b>Balance Remaining :</b> ##TimeContractBalance##<br/>'
--	, numDomainID, 0, GETUTCDATE(), 0, GETUTCDATE(), 'Time Contract Used For ##TaskTitle##', NULL, NULL, NULL, NULL, 
--	NULL, NULL, NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
--FROM Domain

--INSERT INTO CFw_Grp_Master( Grp_Name, Loc_Id, numDomainID, tintType, vcURLF) SELECT 'Collaboration',6,numDomainId,2,'' FROM Domain
--INSERT INTO CFw_Grp_Master( Grp_Name, Loc_Id, numDomainID, tintType, vcURLF) SELECT 'Collaboration',3,numDomainId,2,'' FROM Domain
--INSERT INTO CFw_Grp_Master( Grp_Name, Loc_Id, numDomainID, tintType, vcURLF) SELECT 'Collaboration',2,numDomainId,2,'' FROM Domain

--CREATE TABLE [dbo].[TopicMessageAttachments](
--	[numAttachmentId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numTopicId] [numeric](18, 0) NULL,
--	[numMessageId] [numeric](18, 0) NULL,
--	[vcAttachmentName] [varchar](500) NULL,
--	[vcAttachmentUrl] [varchar](500) NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[dtmCreatedOn] [datetime] NULL,
--	[numUpdatedBy] [numeric](18, 0) NULL,
--	[dtmUpdatedOn] [datetime] NULL,
--	[numDomainId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_TopicMessageAttachments] PRIMARY KEY CLUSTERED 
--(
--	[numAttachmentId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--CREATE TABLE [dbo].[MessageMaster](
--	[numMessageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numTopicId] [numeric](18, 0) NULL,
--	[numParentMessageId] [numeric](18, 0) NULL,
--	[intRecordType] [int] NULL,
--	[numRecordId] [numeric](18, 0) NULL,
--	[vcMessage] [varchar](max) NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[dtmCreatedOn] [datetime] NULL,
--	[numUpdatedBy] [numeric](18, 0) NULL,
--	[dtmUpdatedOn] [datetime] NULL,
--	[bitIsInternal] [bit] NULL,
--	[numDomainId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_MessageMaster] PRIMARY KEY CLUSTERED 
--(
--	[numMessageId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
--GOkan 

--CREATE TABLE [dbo].[TopicMaster](
--	[numTopicId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[intRecordType] [int] NULL,
--	[numRecordId] [numeric](18, 0) NULL,
--	[vcTopicTitle] [varchar](max) NULL,
--	[numCreateBy] [numeric](18, 0) NULL,
--	[dtmCreatedOn] [datetime] NULL,
--	[numUpdatedBy] [numeric](18, 0) NULL,
--	[dtmUpdatedOn] [datetime] NULL,
--	[bitIsInternal] [bit] NULL,
--	[numDomainId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_TopicMaster] PRIMARY KEY CLUSTERED 
--(
--	[numTopicId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
--GO

--ALTER TABLE ContractsLog ADD vcDetails VARCHAR(500) DEFAULT NULL
--ALTER TABLE ContractsLog ADD numUsedTime NUMERIC(18,2) DEFAULT 0
--ALTER TABLE ContractsLog ADD numBalance NUMERIC(18,2) DEFAULT 0
--ALTER TABLE ContractsLog ADD numContractId NUMERIC(18,2) DEFAULT 0
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Incident','../ContractManagement/frmContractListv2.aspx?type=3',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=3',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
-------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Time','../ContractManagement/frmContractListv2.aspx?type=1',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=1',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------------------------------------------------

--UPDATE PageNavigationDTL SET vcNavURL='#',bitAddIsPopUp=0,vcAddURL=NULL  WHERE vcPageNavName='Contracts'
--UPDATE TabMaster SET vcURL='#',bitAddIsPopUp=0,vcAddURL=NULL  WHERE numTabName='Contracts' AND tintTabType=1

--INSERT  INTO dbo.NameTemplate
--(
--	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
--)
--SELECT 
--	numDomainId,'Incidents','Incident-',1,1,4,6
--FROM   
--	Domain
--ALTER TABLE Activity ADD bitTimeAddedToContract BIT DEFAULT 0
--ALTER TABLE Communication ADD bitTimeAddedToContract BIT DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD bitTimeAddedToContract BIT DEFAULT 0
--CREATE TABLE [dbo].[ContractsLog](
--	[numContractsLogId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[intType] [int] NULL,
--	[numDivisionID] [numeric](18, 0) NULL,
--	[numReferenceId] [numeric](18, 0) NULL,
--	[dtmCreatedOn] [datetime] NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_ContractsLog] PRIMARY KEY CLUSTERED 
--(
--	[numContractsLogId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--ALTER TABLE Contracts ADD numIncidentsUsed NUMERIC(18,2)
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Warranty','../ContractManagement/frmContractListv2.aspx?type=2',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=2',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
-------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),0,'Contracts','../ContractManagement/frmContractListv2.aspx?type=1',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=1',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--UPDATE TabMaster SET vcAddURL='ContractManagement/frmMngContract.aspx?type=1',
--vcURL= 'ContractManagement/frmContractListv2.aspx?type=1'
--WHERE numTabName='Contracts' AND tintTabType=1

--ALTER TABLE Domain ADD bitDisplayContractElement BIT DEFAULT 0  
--ALTER TABLE Domain ADD vcEmployeeForContractTimeElement VARCHAR(500) DEFAULT NULL  

--ALTER TABLE Contracts ADD numModifiedBy NUMERIC(18,0)
--ALTER TABLE Contracts ADD dtmModifiedOn DATETIME

--ALTER TABLE Contracts ADD numCreatedBy NUMERIC(18,0)
--ALTER TABLE Contracts ADD dtmCreatedOn DATETIME
--ALTER TABLE Contracts ADD timeUsed NUMERIC(18,2)

--CREATE TABLE [dbo].[Contracts](
--	[numContractId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[intType] [int] NULL,
--	[numDomainId] [numeric](18, 0) NOT NULL,
--	[numDivisonId] [numeric](18, 0) NULL,
--	[numIncidents] [numeric](18, 0) NULL,
--	[numIncidentLeft] [numeric](18, 0) NULL,
--	[numHours] [numeric](18, 2) NULL,
--	[numMinutes] [numeric](18, 2) NULL,
--	[timeLeft] [numeric](18, 0) NULL,
--	[vcItemClassification] [varchar](max) NULL,
--	[numWarrantyDays] [numeric](18, 0) NULL,
--	[vcNotes] [varchar](max) NULL,
--	[vcContractNo] [varchar](500) NULL,
-- CONSTRAINT [PK_Contracts] PRIMARY KEY CLUSTERED 
--(
--	[numContractId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
--GO

--ALTER TABLE [dbo].[Contracts] ADD  CONSTRAINT [DF_Contracts_intType]  DEFAULT ((1)) FOR [intType]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Contract,2-Warranty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Contracts', @level2type=N'COLUMN',@level2name=N'intType'
--GO
-----------------------------------------------------------------------------------------------
--INSERT  INTO dbo.NameTemplate
--(
--	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
--)
--SELECT 
--	numDomainId,'Contract','Time-',1,1,4,4
--FROM   
--	Domain	

--INSERT  INTO dbo.NameTemplate
--(
--	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
--)
--SELECT 
--	numDomainId,'Warrantee','Warrantee-',1,1,4,5
--FROM   
--	Domain	

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(3,'Drop-Ship','intDropShip','intDropShip','intDropShip','DivisionMaster','V','R','SelectBox',46,1,1,1,0,0,0,1,0,1,1,1,0,'')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(41,'Drop-Ship','R','SelectBox','intDropShip',0,0,'V','intDropShip',0,'DivisionMaster',0,7,1,'intDropShip',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,41,0,0,'Drop-Ship','SelectBox','intDropShip',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--ALTER TABLE DivisionMaster ADD intDropShip INT DEFAULT 0
--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(2,'Drop-Ship','intDropShip','intDropShip','intDropShip','DivisionMaster','V','R','SelectBox',46,1,1,1,0,0,0,1,0,1,1,1,0,'')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(36,'Drop-Ship','R','SelectBox','intDropShip',0,0,'V','intDropShip',0,'DivisionMaster',0,7,1,'intDropShip',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,36,1,1,'Drop-Ship','SelectBox','intDropShip',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--UPDATE ListMaster SET vcListName='Vendor Lead-Times',numModuleId=1 where numListID=338
--UPDATE ListMaster SET bitFixed=0 where numListID=338
--UPDATE ListDetails SET bitDelete=0 where numListID=338 AND constFlag=1
--ALTER TABLE PurchaseIncentives ADD vcBuyingQty VARCHAR(100)
--ALTER TABLE PurchaseIncentives DROP COLUMN decBuyingQty
--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Order, Total, Paid' where numFormID=39 AND vcDbColumnName='vcPoppName'
--UPDATE DycFieldMaster SET vcFieldName='Order, Total, Paid' WHERE vcDbColumnName='vcPoppName' AND numModuleID=3
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Order, Total, Paid' WHERE numFormFieldId=96 AND numFormId=39

--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Invoice - Amount (Order Balance)' WHERE numFormFieldId=885 AND numFormId=39

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Item ADD bitTimeContractFromSalesOrder  BIT
--ALTER TABLE CFW_Fld_Master ADD vcItemsLocation VARCHAR(MAX)
--UPDATE DycFieldMaster SET vcFieldName='Matrix Group' WHERE vcFieldName='Item Group'
--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Matrix Group' WHERE vcFormFieldName='Item Group'
--UPDATE DycFormField_Mapping SET vcFieldName='Matrix Group' WHERE vcFieldName='Item Group'
--ALTER TABLE Domain ADD vcElectiveItemFields VARCHAR(500)
--CREATE TABLE [dbo].[PurchaseIncentives](
--	[numPurchaseIncentiveId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NULL,
--	[decBuyingQty] [decimal](18, 2) NULL,
--	[intType] [int] NULL,
--	[vcIncentives] [varchar](500) NULL,
--	[numDomainId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_PurchaseIncentives] PRIMARY KEY CLUSTERED 
--(
--	[numPurchaseIncentiveId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO


--DELETE FROM DycFormField_Mapping WHERE numFieldID IN ( 
--SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='CalAmount' AND numModuleID=3) AND vcFieldName='Order Sub-total' AND numFormID=39

--DELETE FROM DycFormField_Mapping WHERE numFieldID IN ( 
--SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monDealAmount' AND numModuleID=3) AND vcFieldName='Inv-Grand-Tot' AND numFormID=39

--DELETE FROM DycFormField_Mapping WHERE numFieldID =(
--SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3) AND numFormID=39

--UPDATE DycFormField_Mapping SET vcFieldName='Order, Total, Paid' WHERE numFieldID =
--(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcPoppName' AND numModuleID=3) AND numFormID=39

--UPDATE DycFormField_Mapping SET vcFieldName='Invoice - Amount (Order Balance)' WHERE numFieldID =
--(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='tintInvoicing' AND numModuleID=3) AND numFormID=39


--UPDATE DycFormField_Mapping SET bitInlineEdit=0 WHERE numFieldID =
--(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcPoppName' AND numModuleID=3) AND numFormID=39

--DELETE FROM DycFormField_Mapping WHERE numFieldID =(
--SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3) AND numFormID=39

--DELETE FROM DycFormField_Mapping WHERE numFieldID =(
--SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3) AND numFormID=39

--UPDATE DycFieldMaster SET bitAllowFiltering=1 WHERE vcDbColumnName='vcOrderedShipped' AND numModuleID=3
--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(2,'Purchase incentives','Purchaseincentives','Purchaseincentives','Purchaseincentives','DivisionMaster','V','R','Popup',46,1,1,1,0,0,0,1,0,1,1,1,0,'openPurchaseincentivesPopup')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(36,'Purchase incentives','R','Popup','Purchaseincentives',0,0,'V','Purchaseincentives',0,'DivisionMaster',0,7,1,'Purchaseincentives',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,36,0,0,'Purchase incentives','Popup','Purchaseincentives',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--DELETE FROM PageNavigationDTL WHERE vcPageNavName='Activities 1.0'

--DELETE FROM DycFormConfigurationDetails WHERE numFormId=43 AND 
--numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
--vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))

--DELETE FROM DycFormField_Mapping WHERE numFormID=43 AND numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
--vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitUseOnlyActionItems BIT DEFAULT 0
--UPDATE DynamicFormMaster SET vcFormName='Activity Grid Columns' WHERE  vcFormName='Tickler Grid Columns'
--UPDATE BizFormWizardModule SET vcModule='Projects, Cases & Activities' WHERE vcModule='Projects, Cases, & Tickler'

--UPDATE ModuleMaster SET vcModuleName='Activities' WHERE numModuleID=1
--UPDATE PageMaster SET vcPageDesc='Activities' WHERE numModuleID=1 AND numPageID=1

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(3,'Approval Status','intReqPOApproved','intReqPOApproved','intReqPOApproved','OpportunityMaster','N','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0,'')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(41,'Approval Status','R','SelectBox','intReqPOApproved',0,0,'N','intReqPOApproved',0,'OpportunityMaster',0,7,1,'intReqPOApproved',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,41,1,1,'Approval Status','SelectBox','intReqPOApproved',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(3,'Approval Status','intReqPOApproved','intReqPOApproved','intReqPOApproved','OpportunityMaster','N','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0,'')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(40,'Approval Status','R','SelectBox','intReqPOApproved',0,0,'N','intReqPOApproved',0,'OpportunityMaster',0,7,1,'intReqPOApproved',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,40,1,1,'Approval Status','SelectBox','intReqPOApproved',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----USE [Production.2014]
----GO
----CREATE NONCLUSTERED INDEX [NonClusterDomainAssignCreatedActivityId]
----ON [dbo].[Communication] ([numDomainID])
----INCLUDE ([numAssign],[numCreatedBy],[numActivityId])
----GO

--ALTER TABLE OpportunityMaster ADD intReqPOApproved INT DEFAULT 0
--ALTER TABLE ApprovalProcessItemsClassification DROP CONSTRAINT  FK_ApprovalProcessItemsClassification_ApprovalProcessItemsClassification
--UPDATE DycFormField_Mapping SET vcPropertyName='CaseStatus' WHERE numFormID=12 AND vcFieldName='Status'
--DELETE FROM CFw_Grp_Master WHERE Loc_Id=3 AND Grp_Name='Case Details'
--UPDATE PageNavigationDTL SET vcNavUrl='../common/frmTicklerdisplayOld.aspx?SelectedIndex=0' WHERE vcPageNavName='Activities 1.0'
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Tickler'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Activities'),'Activities 1.0','../common/frmTicklerdisplayOld.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Activities' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Activities' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DELETE FROM PageNavigationDTL WHERE numParentID IN(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Activities')


--UPDATE TabMaster SET vcURL='common/frmTicklerdisplay.aspx?SelectedIndex=0' WHERE numTabName='Activities'
--UPDATE TabMaster SET vcAddURL='common/frmAddActivity.aspx',bitAddIsPopUp=1 WHERE numTabName='Activities'
--ALTER TABLE OpportunityMaster ADD bitReqPOApproved BIT DEFAULT 0

--ALTER TABLE Domain ADD bitDisplayCustomField BIT DEFAULT 0
--ALTER TABLE Domain ADD bitFollowupAnytime BIT DEFAULT 0
--ALTER TABLE Domain ADD bitpartycalendarTitle BIT DEFAULT 0
--ALTER TABLE Domain ADD bitpartycalendarLocation BIT DEFAULT 0
--ALTER TABLE Domain ADD bitpartycalendarDescription BIT DEFAULT 0

--ALTER TABLE Domain ADD bitREQPOApproval BIT DEFAULT 0
--ALTER TABLE Domain ADD bitARInvoiceDue BIT DEFAULT 0
--ALTER TABLE Domain ADD bitAPBillsDue BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemsToPickPackShip BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemsToInvoice BIT DEFAULT 0
--ALTER TABLE Domain ADD bitSalesOrderToClose BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemsToPutAway BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemsToBill BIT DEFAULT 0
--ALTER TABLE Domain ADD bitPosToClose BIT DEFAULT 0
--ALTER TABLE Domain ADD bitPOToClose BIT DEFAULT 0
--ALTER TABLE Domain ADD bitBOMSToPick BIT DEFAULT 0

--ALTER TABLE Domain ADD vchREQPOApprovalEmp VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchARInvoiceDue VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchAPBillsDue VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchItemsToPickPackShip VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchItemsToInvoice VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchPriceMarginApproval VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchSalesOrderToClose VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchItemsToPutAway VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchItemsToBill VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchPosToClose VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchPOToClose VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vchBOMSToPick VARCHAR(500) DEFAULT NULL

--ALTER TABLE Domain ADD decReqPOMinValue DECIMAL(18,2) DEFAULT 0

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
--VALUES
--(7,'Solutions','SoultionName','SoultionName','SoultionName','Cases','V','R','Popup',46,1,1,1,0,0,0,1,0,1,1,1,0,'openSolutionPopup')

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(12,'Solutions','R','Popup','SoultionName',0,0,'V','SoultionName',0,'Cases',0,7,1,'SoultionName',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(7,@numFieldID,12,0,0,'Solutions','Popup','SoultionName',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--ALTER TABLE DycFormConfigurationDetails ADD bitDefaultByAdmin BIT
--ALTER TABLE DycFormConfigurationDetails ADD numFormFieldGroupId NUMERIC(18,0)

--ALTER TABLE CaseContacts ADD bitSubscribedEmailAlert BIT DEFAULT 1
--ALTER TABLE ProjectsContacts ADD bitSubscribedEmailAlert BIT DEFAULT 1
--ALTER TABLE OpportunityContact ADD bitSubscribedEmailAlert BIT DEFAULT 1

--ALTER TABLE Cases ADD vcSolutionShortMessage VARCHAR(250) 

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


--INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
--VALUES('Human Resources','Human Resources',1,'',1,1,NULL,NULL,NULL)

--INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
--VALUES('Human Resources','Human Resources',1,'',1,1,NULL,NULL,NULL)

--INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
--SELECT  
--	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--	0,1,15,0,0,0
--FROM    
--	AuthenticationGroupMaster
--WHERE
--	vcGroupName='System Administrator'

--INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Human Resources',1)

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),0,'Human Resources','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
----Internal Users
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
--WHERE 
--	vcPageNavName='Internal Users'

--DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Internal Users')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Internal Users'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
----External Users
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
--WHERE 
--	vcPageNavName='External Users'

--DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='External Users')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='External Users'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

----Manage Authorization / Permissions & Roles
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--	vcPageNavName='Permissions & Roles'
--WHERE 
--	vcPageNavName='Manage Authorization'

--DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Permissions & Roles')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Permissions & Roles'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

----Time & Expense
--DELETE GroupTabDetails WHERE numTabId IN(SELECT numTabId FROM TabMaster WHERE numTabName='Time & Expense' )
--DELETE TabMaster WHERE numTabId IN(SELECT numTabId FROM TabMaster WHERE numTabName='Time & Expense' )
--BEGIN TRY
--BEGIN TRANSACTION
	
--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),'Time & Expense','../TimeAndExpense/frmEmpCal.aspx?frm=contactlist&CntID=RecordID',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----Sales Commissions
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--	vcPageNavName='Sales Commissions',
--	vcImageURL=NULL
--WHERE 
--	vcPageNavName='Commission Rules'

--DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Sales Commissions')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Sales Commissions'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

----Payroll
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--	vcPageNavName='Payroll',
--	vcImageURL=NULL
--WHERE 
--	vcPageNavName='Payroll Expenses'

--DELETE FROM  TreeNavigationAuthorization  WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

----Payroll History
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--	vcImageURL=NULL
--WHERE 
--	vcPageNavName='Payroll History'

--DELETE FROM  TreeNavigationAuthorization  WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll History')
--INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll History'),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

----Resource Planning

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources')
--		,'Resource Planning','../reports/frmCapacityPlanningResources.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------------------------PROCUREMENT------------------------------------------------------------
--INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
--VALUES('Procurement','Procurement',1,'',1,1,NULL,NULL,NULL)

--INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
--SELECT  
--	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
--	0,1,16,0,0,0
--FROM    
--	AuthenticationGroupMaster
--WHERE
--	vcGroupName='System Administrator'

--INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Procurement',1)

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),0,'Procurement','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----Purchase Orders
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
--WHERE 
--	vcPageNavName='Purchase Orders'
--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Orders')

----Purchase Fulfillment
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
--WHERE 
--	vcPageNavName='Purchase Fulfillment'
--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Fulfillment')

----Requisitions / OLD Purchase Opportunities
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
--	vcPageNavName='Requisitions'
--WHERE 
--	vcPageNavName='Purchase Opportunities'
--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Requisitions')

----Demand Planning / OLD Planning & Procurement
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
--	vcPageNavName='Demand Planning'
--WHERE 
--	vcPageNavName='Planning & Procurement'
--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Demand Planning')


----Demand Planning / OLD Planning & Procurement
--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
--WHERE 
--	vcPageNavName='Purchase Returns'
--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Returns')

----Sales Order Opportunity/Order
--UPDATE TabMaster SET numTabName='Orders & Quotes' where numTabName='Orders'

----Contacts
--INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
--VALUES('Contacts','Contacts',1,'Contact/frmContactList.aspx',1,1,NULL,'Contact/newcontact.aspx',1)


--INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
--SELECT  
--	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1),
--	0,1,15,0,0,0
--FROM    
--	AuthenticationGroupMaster
--WHERE
--	vcGroupName='System Administrator'

--UPDATE 
--	PageNavigationDTL 
--SET 
--	numParentID=0,
--	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contacts'),
--	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
--WHERE 
--	vcPageNavName='Contacts'

--UPDATE TreeNodeOrder SET numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
--WHERE numParentID=13

--UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1) WHERE 
--numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contacts')

--DELETE FROM PageNavigationDTL WHERE vcPageNavName='User Administration'


--------------------------------------------------Manufacturing------------------------------------------------------------
--INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
--VALUES('Manufacturing','Manufacturing',1,'',1,1,NULL,NULL,NULL)

--INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
--SELECT  
--	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--	0,1,16,0,0,0
--FROM    
--	AuthenticationGroupMaster
--WHERE
--	vcGroupName='System Administrator'

--INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Manufacturing',1)

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),0,'Manufacturing','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------WORK ORDER--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Work Orders','../Opportunity/frmWorkOrderList.aspx',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),'~/Items/frmManageAssembly.aspx',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Production Planning--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Production Planning','../reports/frmProductionPlanning.aspx?type=3',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Capacity Planning--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Capacity Planning','../reports/frmCapacityPlanningTasks.aspx?type=3',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Performance Evaluation--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Performance Evaluation','../reports/frmPerformanceEvaluation.aspx?type=3',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------------------------------PROJECTS-----------------------------------------------------------------------

--------------------PROJECT--------------------
--UPDATE TabMaster SET vcURL='',vcAddURL='',bitAddIsPopup=0 WHERE numTabName='Projects' AND tintTabType=1 
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),0,'Projects','',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),'',0
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--UPDATE  PageNavigationDTL SET vcNavURL='',vcAddURL='',bitAddIsPopup=0 WHERE vcPageNavName='Projects' AND numParentID=0

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'),'Projects','../projects/frmProjectList.aspx',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),'~/Projects/frmProjectAdd.aspx',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Production Planning--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects' AND numParentID=0),'Production Planning','../reports/frmProductionPlanning.aspx?type=1',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Capacity Planning--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'  AND numParentID=0),'Capacity Planning','../reports/frmCapacityPlanningTasks.aspx?type=1',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------Performance Evaluation--------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'  AND numParentID=0),'Performance Evaluation','../reports/frmPerformanceEvaluation.aspx?type=1',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--------------------------------------------------END MENU WORK------------------------------------------------------------------------

--ALTER TABLE ProjectsMaster ADD numCompletedBy NUMERIC(18,0)
--UPDATE DycFormField_Mapping SET vcFieldName = 'Project Manager' WHERE vcFieldName='Internal Project Manager' AND numFormID=13
--UPDATE DycFormField_Mapping SET vcFieldName = 'Project Site' WHERE vcFieldName='Shipping Address' AND numFormID=13
--ALTER TABLE ProjectsMaster Add numClientBudget NUMERIC(18,2) DEFAULT 0
--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Billed (Balance)','ClientBudgetBalance','ClientBudgetBalance','ClientBudgetBalance','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,0,0,'Billed (Balance)','Label',1,0,0,1,0,1
--)
-----------------------------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Client Budget','ClientBudget','ClientBudget','ClientBudget','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,0,0,'Client Budget','Label',1,0,0,1,0,1
--)
------------------------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Planned Start Date','dtmStartDate','dtmStartDate','PlannedStartDate','ProjectsMaster','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,1,1,'Planned Start Date','DateField',1,0,1,1,1,1
--)

--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Requested Finish','dtmEndDate','dtmEndDate','RequestedFinish','ProjectsMaster','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,1,1,'Requested Finish','DateField',1,0,1,1,1,1
--)

--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Actual Start','dtActualStartDate','dtActualStartDate','ActualStartDate','ProjectsMaster','V','R','DateField','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,0,0,'Actual Start','DateField',1,0,0,1,0,1
--)


--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	5,'Projected Finish','vcProjectedFinish','vcProjectedFinish','ProjectedFinish','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	5,@numFieldID,13,0,0,'Projected Finish','Label',1,0,0,1,0,1
--)
--ALTER TABLE ProjectsMaster Add dtActualStartDate DATETIME DEFAULT NULL
--ALTER TABLE ProjectsMaster Add dtmEndDate DATETIME DEFAULT NULL
--ALTER TABLE ProjectsMaster Add dtmStartDate DATETIME DEFAULT NULL
--ALTER TABLE Domain ADD vcLoginURL VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD vcLogoForLoginBizTheme VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD bitLogoAppliedToLoginBizTheme BIT DEFAULT 0
--UPDATE DynamicFormMaster SET vcFormName ='Prospect (and all Organization relationships that aren�t Leads or Accounts)'  where vcFormName='Prospect'
--UPDATE DynamicFormMaster SET vcFormName ='Prospect (and all Organization relationships that aren�t Leads or Accounts)'  where vcFormName='Prospect'
--ALTER TABLE DynamicFormMaster ALTER COLUMN vcFormName VARCHAR(500);
--UPDATE CFW_Loc_Master SET Loc_Name='Project Details' WHERE Loc_id=11
--ALTER TABLE FormFieldGroupConfigurarion ADD numOrder NUMERIC DEFAULT 0
--DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=
--(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE numModuleID = 13  AND vcPageNavName='Map Custom Fields')--69

--DELETE FROM PageNavigationDTL WHERE numModuleID = 13  AND vcPageNavName='Map Custom Fields'

--UPDATE BizFormWizardModule SET bitActive=0 WHERE numBizFormModuleID =8
--ALTER TABLE BizFormWizardModule ADD bitActive BIT DEFAULT 1 
--ALTER TABLE Domain ADD vcLogoForBizTheme VARCHAR(500) DEFAULT NULL
--ALTER TABLE Domain ADD bitLogoAppliedToBizTheme BIT DEFAULT 0
--ALTER TABLE Domain ADD vcThemeClass VARCHAR(500) DEFAULT NULL

--ALTER TABLE Sales_process_List_Master ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetails ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0

--INSERT  INTO dbo.NameTemplate
--(
--	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
--)
--SELECT 
--	numDomainId,'Work Order','WO-',1,1,4,3
--FROM   
--	Domain	
 
--ALTER TABLE WorkOrder ADD vcWorkOrderName  VARCHAR(500) DEFAULT 0
--ALTER TABLE WorkOrder ADD numBuildProcessId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE WorkOrder ADD dtmStartDate DATETIME DEFAULT NULL
--ALTER TABLE WorkOrder ADD dtmEndDate DATETIME DEFAULT NULL
--ALTER TABLE WorkOrder ADD dtActualStartDate DATETIME DEFAULT NULL


--CREATE TABLE [dbo].[tblStageGradeDetails](
--	[numStageDetailsGradeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numStageDetailsId] [numeric](18, 0) NOT NULL,
--	[numAssigneId] [numeric](18, 0) NOT NULL,
--	[vcGradeId] [varchar](100) NULL,
--	[numDomainId] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[bintModifiedDate] [datetime] NULL,
--	[numTaskId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_tblStageGradeDetails] PRIMARY KEY CLUSTERED 
--(
--	[numStageDetailsGradeId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO


--ALTER TABLE StagePercentageDetails ADD intTaskType INT DEFAULT 0
--ALTER TABLE Item ADD numBusinessProcessId NUMERIC(18,0) DEFAULT 0


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE BizFormWizardMasterConfiguration ADD numFormFieldGroupId NUMERIC(18,0) DEFAULT 0

--CREATE TABLE [dbo].[FormFieldGroupConfigurarion](
--	[numFormFieldGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] NUMERIC(18,0) NOT NULL,
--	[numFormId] [numeric](18, 0) NULL,
--	[numGroupId] [numeric](18, 0) NULL,
--	[vcGroupName] [varchar](500) NULL,
-- CONSTRAINT [PK_FormFieldGroupConfigurarion] PRIMARY KEY CLUSTERED 
--(
--	[numFormFieldGroupId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE SalesOrderConfiguration ADD bitDisplayExpectedDate BIT

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--UPDATE DycFieldMaster SET vcFieldName='Find Messages' WHERE vcFieldName='Subject' AND vcLookBackTableName='EmailHistory'
--UPDATE DycFormField_Mapping SET vcFieldName='Find Messages' WHERE vcFieldName='Subject' AND numFormID=44

--ALTER TABLE InboxTreeSort ADD bitHidden BIT DEFAULT 0

--UPDATE DycFieldMaster SET vcAssociatedControlType='Label' WHERE numFieldId=172
--UPDATE DycFormField_Mapping SET vcAssociatedControlType='Label' WHERE numFieldId=172 AND numFormID=44

--DELETE FROM DycFieldMaster WHERE vcDbColumnName='StockQtyCount'
--DELETE FROM DycFormField_Mapping WHERE numFormID=21 AND vcFieldName='Counted'
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DELETE FROM DycFieldMaster WHERE vcDbColumnName='StockQtyCount'
--DELETE FROM DycFormField_Mapping WHERE numFormID=21 AND vcFieldName='Counted'

--UPDATE DycFieldMaster SET vcFieldName='Counted/Qty to Adjust/Unit Cost' WHERE vcDbColumnName='StockQtyAdjust'
--UPDATE DycFormField_Mapping SET vcFieldName='Counted/Qty to Adjust/Unit Cost' WHERE numFormID=21 AND vcFieldName='Qty to Adjust'
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE DivisionMaster ADD bitAutoCheckCustomerPart BIT DEFAULT 0
--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--149,37,'','Save Inventory',0,0,1,0,0,'',0
--)

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	4,'Counted','StockQtyCount','StockQtyCount','Item','V','R','Label','',0,1,0,0,0,1,0,0,0
--)
--SELECT @numFieldID = SCOPE_IDENTITY()

--SELECT @numFormFieldID = NULL

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,21,0,0,'Counted','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	4,'Qty to Adjust','StockQtyAdjust','StockQtyAdjust','Item','V','R','Label','',0,1,0,0,0,1,0,0,0
--)
--SELECT @numFieldID = SCOPE_IDENTITY()

--SELECT @numFormFieldID = NULL

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,21,0,0,'Qty to Adjust','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	4,'Internal Location','vcLocation','vcLocation','WareHouseItems','V','R','Label','',0,1,0,0,0,1,0,0,0
--)
--SELECT @numFieldID = SCOPE_IDENTITY()

--SELECT @numFormFieldID = NULL

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,21,0,0,'Internal Location','Label',NULL,7,1,7,1,0,0,1,0,1,0,0,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
-------------------------------------------------------------01/07/2019---------------------
--GO
--CREATE NONCLUSTERED INDEX NONCLUSTER_DIVISONID_CONTACTID
--ON [dbo].[OpportunityMaster] ([numDomainId])
--INCLUDE ([numContactId],[numDivisionId])
--GO


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	6,'To','vcTo','vcTo','EmailHistory','V','R','TextArea','',0,1,0,0,0,1,0,0,1
--)
--SELECT @numFieldID = SCOPE_IDENTITY()
----Lead Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(44,'To','R','TextArea','vcTo',0,0,'V','vcTo',0,'EmailHistory',0,7,1,'vcTo',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(6,@numFieldID,44,0,0,'To','TextArea',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--ALTER TABLE InboxTree ADD numLastUid NUMERIC(18,0) DEFAULT 0
--ALTER TABLE InboxTree ADD numOldEmailLastUid NUMERIC(18,0) DEFAULT 0
--ALTER TABLE InboxTreeSort ADD numLastUid NUMERIC(18,0) DEFAULT 0
--ALTER TABLE InboxTreeSort ADD numOldEmailLastUid NUMERIC(18,0) DEFAULT 0


--UPDATE DycFormField_Mapping SET bitAllowEdit=1 WHERE vcFieldName='Follow-up Status' AND numFormID=43
--UPDATE DycFieldMaster SET vcPropertyName='vcCompactContactDetails' WHERE vcDbColumnName='vcCompactContactDetails'

--------------------------------CHANGE FIELD NAME--------------------------------------------
--DELETE FROM DycFormField_Mapping WHERE vcFieldName='Percentage Complete'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Position' WHERE vcFormFieldName='Contact Position'
--UPDATE DycFormField_Mapping SET vcFieldName='Position' WHERE vcFieldName='Contact Position'
--UPDATE DycFieldMaster SET vcFieldName='Position' WHERE vcFieldName='Contact Position'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Position' WHERE vcNewFormFieldName='Contact Position'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-State' WHERE vcFormFieldName='Ship To State'
--UPDATE DycFormField_Mapping SET vcFieldName='Ship-State' WHERE vcFieldName='Ship To State'
--UPDATE DycFieldMaster SET vcFieldName='Ship-State' WHERE vcFieldName='Ship To State'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-State' WHERE vcNewFormFieldName='Ship To State'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-Country' WHERE vcFormFieldName='Ship To Country'
--UPDATE DycFormField_Mapping SET vcFieldName='Ship-Country' WHERE vcFieldName='Ship To Country'
--UPDATE DycFieldMaster SET vcFieldName='Ship-Country' WHERE vcFieldName='Ship To Country'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-Country' WHERE vcNewFormFieldName='Ship To Country'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Rec Owner' WHERE vcFormFieldName='Record Owner'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Rec Owner' WHERE vcFieldName='Record Owner'
--UPDATE DycFieldMaster SET vcFieldName='Org Rec Owner' WHERE vcFieldName='Record Owner'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Rec Owner' WHERE vcNewFormFieldName='Record Owner'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Status' WHERE vcFormFieldName='Organization Status'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Status' WHERE vcFieldName='Organization Status'
--UPDATE DycFieldMaster SET vcFieldName='Org Status' WHERE vcFieldName='Organization Status'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Status' WHERE vcNewFormFieldName='Organization Status'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Profile' WHERE vcFormFieldName='Organization Profile'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Profile' WHERE vcFieldName='Organization Profile'
--UPDATE DycFieldMaster SET vcFieldName='Org Profile' WHERE vcFieldName='Organization Profile'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Profile' WHERE vcNewFormFieldName='Organization Profile'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Dif Val' WHERE vcFormFieldName='Company Differentiation Value'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Dif Val' WHERE vcFieldName='Company Differentiation Value'
--UPDATE DycFieldMaster SET vcFieldName='Org Dif Val' WHERE vcFieldName='Company Differentiation Value'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Dif Val' WHERE vcNewFormFieldName='Company Differentiation Value'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Dif' WHERE vcFormFieldName='Company Differentiation'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Dif' WHERE vcFieldName='Company Differentiation'
--UPDATE DycFieldMaster SET vcFieldName='Org Dif' WHERE vcFieldName='Company Differentiation'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Dif' WHERE vcNewFormFieldName='Company Differentiation'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Bill-State' WHERE vcFormFieldName='Bill To State'
--UPDATE DycFormField_Mapping SET vcFieldName='Bill-State' WHERE vcFieldName='Bill To State'
--UPDATE DycFieldMaster SET vcFieldName='Bill-State' WHERE vcFieldName='Bill To State'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Bill-State' WHERE vcNewFormFieldName='Bill To State'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Bill-Country' WHERE vcFormFieldName='Bill To Country'
--UPDATE DycFormField_Mapping SET vcFieldName='Bill-Country' WHERE vcFieldName='Bill To Country'
--UPDATE DycFieldMaster SET vcFieldName='Bill-Country' WHERE vcFieldName='Bill To Country'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Bill-Country' WHERE vcNewFormFieldName='Bill To Country'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Relationship' WHERE vcFormFieldName='Organization Relationship'
--UPDATE DycFormField_Mapping SET vcFieldName='Relationship' WHERE vcFieldName='Organization Relationship'
--UPDATE DycFieldMaster SET vcFieldName='Relationship' WHERE vcFieldName='Organization Relationship'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Relationship' WHERE vcNewFormFieldName='Organization Relationship'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Rating' WHERE vcFormFieldName='Organization Rating'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Rating' WHERE vcFieldName='Organization Rating'
--UPDATE DycFieldMaster SET vcFieldName='Org Rating' WHERE vcFieldName='Organization Rating'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Rating' WHERE vcNewFormFieldName='Organization Rating'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Phone' WHERE vcFormFieldName='Organization Phone'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Phone' WHERE vcFieldName='Organization Phone'
--UPDATE DycFieldMaster SET vcFieldName='Org Phone' WHERE vcFieldName='Organization Phone'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Phone' WHERE vcNewFormFieldName='Organization Phone'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Created' WHERE vcFormFieldName='Organization Created Date'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Created' WHERE vcFieldName='Organization Created Date'
--UPDATE DycFieldMaster SET vcFieldName='Org Created' WHERE vcFieldName='Organization Created Date'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Created' WHERE vcNewFormFieldName='Organization Created Date'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Department' WHERE vcFormFieldName='Department Name'
--UPDATE DycFormField_Mapping SET vcFieldName='Department' WHERE vcFieldName='Department Name'
--UPDATE DycFieldMaster SET vcFieldName='Department' WHERE vcFieldName='Department Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Department' WHERE vcNewFormFieldName='Department Name'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Type' WHERE vcFormFieldName='Contact Type'
--UPDATE DycFormField_Mapping SET vcFieldName='Type' WHERE vcFieldName='Contact Type'
--UPDATE DycFieldMaster SET vcFieldName='Type' WHERE vcFieldName='Contact Type'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Type' WHERE vcNewFormFieldName='Contact Type'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Title' WHERE vcFormFieldName='Contact Title'
--UPDATE DycFormField_Mapping SET vcFieldName='Title' WHERE vcFieldName='Contact Title'
--UPDATE DycFieldMaster SET vcFieldName='Title' WHERE vcFieldName='Contact Title'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Title' WHERE vcNewFormFieldName='Contact Title'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Team' WHERE vcFormFieldName='Contact Team'
--UPDATE DycFormField_Mapping SET vcFieldName='Team' WHERE vcFieldName='Contact Team'
--UPDATE DycFieldMaster SET vcFieldName='Team' WHERE vcFieldName='Contact Team'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Team' WHERE vcNewFormFieldName='Contact Team'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Street' WHERE vcFormFieldName='Contact Street'
--UPDATE DycFormField_Mapping SET vcFieldName='Street' WHERE vcFieldName='Contact Street'
--UPDATE DycFieldMaster SET vcFieldName='Street' WHERE vcFieldName='Contact Street'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Street' WHERE vcNewFormFieldName='Contact Street'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Status' WHERE vcFormFieldName='Contact Status'
--UPDATE DycFormField_Mapping SET vcFieldName='Status' WHERE vcFieldName='Contact Status'
--UPDATE DycFieldMaster SET vcFieldName='Status' WHERE vcFieldName='Contact Status'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Status' WHERE vcNewFormFieldName='Contact Status'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='State' WHERE vcFormFieldName='Contact State'
--UPDATE DycFormField_Mapping SET vcFieldName='State' WHERE vcFieldName='Contact State'
--UPDATE DycFieldMaster SET vcFieldName='State' WHERE vcFieldName='Contact State'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='State' WHERE vcNewFormFieldName='Contact State'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Zip Code' WHERE vcFormFieldName='Contact Postal Code'
--UPDATE DycFormField_Mapping SET vcFieldName='Zip Code' WHERE vcFieldName='Contact Postal Code'
--UPDATE DycFieldMaster SET vcFieldName='Zip Code' WHERE vcFieldName='Contact Postal Code'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Zip Code' WHERE vcNewFormFieldName='Contact Postal Code'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ext' WHERE vcFormFieldName='Contact Phone Ext'
--UPDATE DycFormField_Mapping SET vcFieldName='Ext' WHERE vcFieldName='Contact Phone Ext'
--UPDATE DycFieldMaster SET vcFieldName='Ext' WHERE vcFieldName='Contact Phone Ext'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ext' WHERE vcNewFormFieldName='Contact Phone Ext'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Phone' WHERE vcFormFieldName='Contact Phone'
--UPDATE DycFormField_Mapping SET vcFieldName='Phone' WHERE vcFieldName='Contact Phone'
--UPDATE DycFieldMaster SET vcFieldName='Phone' WHERE vcFieldName='Contact Phone'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Phone' WHERE vcNewFormFieldName='Contact Phone'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Home Phone' WHERE vcFormFieldName='Contact Home Phone'
--UPDATE DycFormField_Mapping SET vcFieldName='Home Phone' WHERE vcFieldName='Contact Home Phone'
--UPDATE DycFieldMaster SET vcFieldName='Home Phone' WHERE vcFieldName='Contact Home Phone'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Home Phone' WHERE vcNewFormFieldName='Contact Home Phone'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Fax' WHERE vcFormFieldName='Contact Fax'
--UPDATE DycFormField_Mapping SET vcFieldName='Fax' WHERE vcFieldName='Contact Fax'
--UPDATE DycFieldMaster SET vcFieldName='Fax' WHERE vcFieldName='Contact Fax'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Fax' WHERE vcNewFormFieldName='Contact Fax'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Email' WHERE vcFormFieldName='Contact Email'
--UPDATE DycFormField_Mapping SET vcFieldName='Email' WHERE vcFieldName='Contact Email'
--UPDATE DycFieldMaster SET vcFieldName='Email' WHERE vcFieldName='Contact Email'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Email' WHERE vcNewFormFieldName='Contact Email'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Contact Created' WHERE vcFormFieldName='Contact Created Date'
--UPDATE DycFormField_Mapping SET vcFieldName='Contact Created' WHERE vcFieldName='Contact Created Date'
--UPDATE DycFieldMaster SET vcFieldName='Contact Created' WHERE vcFieldName='Contact Created Date'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Contact Created' WHERE vcNewFormFieldName='Contact Created Date'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Country' WHERE vcFormFieldName='Contact Country'
--UPDATE DycFormField_Mapping SET vcFieldName='Country' WHERE vcFieldName='Contact Country'
--UPDATE DycFieldMaster SET vcFieldName='Country' WHERE vcFieldName='Contact Country'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Country' WHERE vcNewFormFieldName='Contact Country'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='City' WHERE vcFormFieldName='Contact City'
--UPDATE DycFormField_Mapping SET vcFieldName='City' WHERE vcFieldName='Contact City'
--UPDATE DycFieldMaster SET vcFieldName='City' WHERE vcFieldName='Contact City'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='City' WHERE vcNewFormFieldName='Contact City'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Cell' WHERE vcFormFieldName='Contact Cell Phone'
--UPDATE DycFormField_Mapping SET vcFieldName='Cell' WHERE vcFieldName='Contact Cell Phone'
--UPDATE DycFieldMaster SET vcFieldName='Cell' WHERE vcFieldName='Contact Cell Phone'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Cell' WHERE vcNewFormFieldName='Contact Cell Phone'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Category' WHERE vcFormFieldName='Contact Category'
--UPDATE DycFormField_Mapping SET vcFieldName='Category' WHERE vcFieldName='Contact Category'
--UPDATE DycFieldMaster SET vcFieldName='Category' WHERE vcFieldName='Contact Category'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Category' WHERE vcNewFormFieldName='Contact Category'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Amt Paid' WHERE vcFormFieldName='Total Amount Paid'
--UPDATE DycFormField_Mapping SET vcFieldName='Amt Paid' WHERE vcFieldName='Total Amount Paid'
--UPDATE DycFieldMaster SET vcFieldName='Amt Paid' WHERE vcFieldName='Total Amount Paid'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Amt Paid' WHERE vcNewFormFieldName='Total Amount Paid'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-State' WHERE vcFormFieldName='Ship-to State'
--UPDATE DycFormField_Mapping SET vcFieldName='Ship-State' WHERE vcFieldName='Ship-to State'
--UPDATE DycFieldMaster SET vcFieldName='Ship-State' WHERE vcFieldName='Ship-to State'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-State' WHERE vcNewFormFieldName='Ship-to State'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-Service' WHERE vcFormFieldName='Shipping Service'
--UPDATE DycFormField_Mapping SET vcFieldName='Ship-Service' WHERE vcFieldName='Shipping Service'
--UPDATE DycFieldMaster SET vcFieldName='Ship-Service' WHERE vcFieldName='Shipping Service'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-Service' WHERE vcNewFormFieldName='Shipping Service'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Name' WHERE vcFormFieldName='Sales Order Name'
--UPDATE DycFormField_Mapping SET vcFieldName='Name' WHERE vcFieldName='Sales Order Name'
--UPDATE DycFieldMaster SET vcFieldName='Name' WHERE vcFieldName='Sales Order Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Name' WHERE vcNewFormFieldName='Sales Order Name'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Recur-Type' WHERE vcFormFieldName='Recurrence Type'
--UPDATE DycFormField_Mapping SET vcFieldName='Recur-Type' WHERE vcFieldName='Recurrence Type'
--UPDATE DycFieldMaster SET vcFieldName='Recur-Type' WHERE vcFieldName='Recurrence Type'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Recur-Type' WHERE vcNewFormFieldName='Recurrence Type'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Organization' WHERE vcFormFieldName='Organization Name'
--UPDATE DycFormField_Mapping SET vcFieldName='Organization' WHERE vcFieldName='Organization Name'
--UPDATE DycFieldMaster SET vcFieldName='Organization' WHERE vcFieldName='Organization Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Organization' WHERE vcNewFormFieldName='Organization Name'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Status' WHERE vcFormFieldName='Order Status'
--UPDATE DycFormField_Mapping SET vcFieldName='Status' WHERE vcFieldName='Order Status'
--UPDATE DycFieldMaster SET vcFieldName='Status' WHERE vcFieldName='Order Status'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Status' WHERE vcNewFormFieldName='Order Status'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Next-Order' WHERE vcFormFieldName='Next Billing Date'
--UPDATE DycFormField_Mapping SET vcFieldName='Next-Order' WHERE vcFieldName='Next Billing Date'
--UPDATE DycFieldMaster SET vcFieldName='Next-Order' WHERE vcFieldName='Next Billing Date'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Next-Order' WHERE vcNewFormFieldName='Next Billing Date'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Marketplace' WHERE vcFormFieldName='Marketplace Order ID'
--UPDATE DycFormField_Mapping SET vcFieldName='Marketplace' WHERE vcFieldName='Marketplace Order ID'
--UPDATE DycFieldMaster SET vcFieldName='Marketplace' WHERE vcFieldName='Marketplace Order ID'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Marketplace' WHERE vcNewFormFieldName='Marketplace Order ID'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Inv-Grand-Tot' WHERE vcFormFieldName='Invoice Grand-total'
--UPDATE DycFormField_Mapping SET vcFieldName='Inv-Grand-Tot' WHERE vcFieldName='Invoice Grand-total'
--UPDATE DycFieldMaster SET vcFieldName='Inv-Grand-Tot' WHERE vcFieldName='Invoice Grand-total'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Inv-Grand-Tot' WHERE vcNewFormFieldName='Invoice Grand-total'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Estimated Close' WHERE vcFormFieldName='Estimated close date'
--UPDATE DycFormField_Mapping SET vcFieldName='Estimated Close' WHERE vcFieldName='Estimated close date'
--UPDATE DycFieldMaster SET vcFieldName='Estimated Close' WHERE vcFieldName='Estimated close date'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Estimated Close' WHERE vcNewFormFieldName='Estimated close date'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Cust PO#' WHERE vcFormFieldName='Customer PO#'
--UPDATE DycFormField_Mapping SET vcFieldName='Cust PO#' WHERE vcFieldName='Customer PO#'
--UPDATE DycFieldMaster SET vcFieldName='Cust PO#' WHERE vcFieldName='Customer PO#'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Cust PO#' WHERE vcNewFormFieldName='Customer PO#'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Last Name' WHERE vcFormFieldName='Contact Last Name'
--UPDATE DycFormField_Mapping SET vcFieldName='Last Name' WHERE vcFieldName='Contact Last Name'
--UPDATE DycFieldMaster SET vcFieldName='Last Name' WHERE vcFieldName='Contact Last Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Last Name' WHERE vcNewFormFieldName='Contact Last Name'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='First Name' WHERE vcFormFieldName='Contact First Name'
--UPDATE DycFormField_Mapping SET vcFieldName='First Name' WHERE vcFieldName='Contact First Name'
--UPDATE DycFieldMaster SET vcFieldName='First Name' WHERE vcFieldName='Contact First Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='First Name' WHERE vcNewFormFieldName='Contact First Name'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Last Order Date' WHERE vcFormFieldName='Last Sales Order Date'
--UPDATE DycFormField_Mapping SET vcFieldName='Last Order Date' WHERE vcFieldName='Last Sales Order Date'
--UPDATE DycFieldMaster SET vcFieldName='Last Order Date' WHERE vcFieldName='Last Sales Order Date'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Last Order Date' WHERE vcNewFormFieldName='Last Sales Order Date'

--UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Credit' WHERE vcFormFieldName='Organization Credit Limit'
--UPDATE DycFormField_Mapping SET vcFieldName='Org Credit' WHERE vcFieldName='Organization Credit Limit'
--UPDATE DycFieldMaster SET vcFieldName='Org Credit' WHERE vcFieldName='Organization Credit Limit'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Credit' WHERE vcNewFormFieldName='Organization Credit Limit'
-----------------------------------------END-------------------------------------
--UPDATE DycFieldMaster SET vcAssociatedControlType='TextBox' WHERE vcDbColumnName='vcCompactContactDetails'
--UPDATE DycFieldMaster SET bitAllowSorting=0 WHERE vcDbColumnName='vcCompactContactDetails'
--UPDATE DynamicFormFieldMaster SET vcAssociatedControlType='TextBox' WHERE vcDbColumnName='vcCompactContactDetails'
--UPDATE DynamicFormFieldMaster SET bitAllowSorting=0 WHERE vcDbColumnName='vcCompactContactDetails'
--UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldId IN(select numFieldId from DycFieldMaster where vcDbColumnName='vcCompactContactDetails')
--UPDATE DycFormField_Mapping SET bitAllowSorting=0 WHERE numFieldId IN(select numFieldId from DycFieldMaster where vcDbColumnName='vcCompactContactDetails')
--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	1,'Contact','vcCompactContactDetails','vcContactName','AdditionalContactsInformation','V','R','DateField','',0,1,0,0,0,1,0,0,1
--)
--SELECT @numFieldID = SCOPE_IDENTITY()
----Lead Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,34,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Prospect Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(35,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,35,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Contact Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(10,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(32,@numFieldID,10,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Case Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(12,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(7,@numFieldID,12,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


----Account Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(36,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,36,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


------Sales Opportunity
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(38,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(10,@numFieldID,38,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Purchase Opportunity
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(40,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(10,@numFieldID,40,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

------Sales Order
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(39,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(10,@numFieldID,39,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Purchase Order
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(41,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(10,@numFieldID,41,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--143,44,'','Accounts',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--144,44,'','Contacts',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--145,44,'','Leads',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--146,44,'','Opportunities/Orders',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--147,44,'','Prospects',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--148,44,'','Relationships',1,0,0,0,0,'',0
--)

--SET IDENTITY_INSERT modulemaster ON;
--INSERT INTO modulemaster(numModuleId,vcModuleName,tintGroupType)VALUES(44,'Add Action Item',1)
--SET IDENTITY_INSERT modulemaster OFF;

--ALTER TABLE Sales_process_List_Master ADD numProjectId NUMERIC(18,0)
--ALTER TABLE ProjectsMaster ADD numBusinessProcessId NUMERIC(18,0)
--ALTER TABLE GenericDocuments ADD numFollowUpStatusId NUMERIC DEFAULT 0
--ALTER TABLE GenericDocuments ADD bitUpdateFollowupStatus BIT DEFAULT 0
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	1,'Last Modified','bintModifiedDate','bintModifiedDate','CompanyInfo','V','R','DateField','',0,1,0,1,0,1,0,0,1
--)
--SELECT @numFieldID = SCOPE_IDENTITY()
----Lead Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,34,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Prospect Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(35,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,35,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Account Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(36,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,36,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


----Tickler Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(43,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(1,@numFieldID,43,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	1,'Last Follow-up','dtLastFollowUp','dtLastFollowUp','DivisionMaster','V','R','DateField','',0,1,0,1,0,1,0,0,1
--)
--SELECT @numFieldID = SCOPE_IDENTITY()
----Lead Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,34,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Prospect Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(35,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,35,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Account Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(36,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,36,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


----Tickler Form
--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(43,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(1,@numFieldID,43,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------
--ALTER  TABLE DivisionMaster ADD dtLastFollowUp DATETIME DEFAULT NULL
--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--137,43,'','Accounts',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--138,43,'','Contacts',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--139,43,'','Leads',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--140,43,'','Opportunities/Orders',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--141,43,'','Prospects',1,0,0,0,0,'',0
--)

--INSERT INTO PageMaster(
--numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
--)VALUES(
--142,43,'','Relationships',1,0,0,0,0,'',0
--)

--SET IDENTITY_INSERT modulemaster ON;
--INSERT INTO modulemaster(numModuleId,vcModuleName,tintGroupType)VALUES(43,'Send Announcement',1)
--SET IDENTITY_INSERT modulemaster OFF;

--ALTER TABLE GenericDocuments ADD vcGroupsPermission VARCHAR(500)
--ALTER TABLE GenericDocuments ADD numCategoryId NUMERIC DEFAULT 0
--ALTER TABLE GenericDocuments ADD bitLastFollowupUpdate BIT DEFAULT 0
--UPDATE ListMaster SET vcListName='Category' WHERE numListID=29

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE StagePercentageDetailsTask ADD numReferenceTaskId NUMERIC DEFAULT 0
--ALTER TABLE ListDetails ADD numListItemGroupId NUMERIC DEFAULT 0
--SET IDENTITY_INSERT ListMaster ON;

--INSERT INTO ListMaster 
--(
--	numListID,vcListName,bitDeleted,bitFixed,numDomainID,bitFlag,vcDataType,numModuleID,numCreatedBy
--)
--VALUES
--(
--	1047,'Follow up Status Group',0,1,1,1,'string',1,1
--)
--exec USP_GetMasterListItems @ListID=72,@numDomainID=1047
--SET IDENTITY_INSERT ListMaster OFF;

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Sales_process_List_Master ADD numOppId NUMERIC DEFAULT 0
--ALTER TABLE Sales_process_List_Master ADD numTaskValidatorId NUMERIC DEFAULT 0

--ALTER TABLE StagePercentageDetailsTask ADD numOrder NUMERIC DEFAULT 0
--ALTER TABLE StagePercentageDetails ADD numStageOrder NUMERIC DEFAULT 0
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE FieldRelationship ADD bitTaskRelation BIT DEFAULT 0
--CREATE TABLE [dbo].[ProjectProcessStageDetails](
--	[numProjectStageDetailsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numOppId] [numeric](18, 0) NULL,
--	[numProjectId] [numeric](18, 0) NULL,
--	[numStageDetailsId] [numeric](18, 0) NULL,
--	[dtmStartDate] [datetime] NULL,
-- CONSTRAINT [PK_OrderStageDetails] PRIMARY KEY CLUSTERED 
--(
--	[numProjectStageDetailsId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE StagePercentageDetailsTask ADD numOppId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD numProjectId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD numParentTaskId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD bitDefaultTask BIT DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD bitTaskClosed BIT DEFAULT 0
--ALTER TABLE StagePercentageDetailsTask ADD bitSavedTask BIT DEFAULT 0

--CREATE TABLE [dbo].[StagePercentageDetailsTask](
--	[numTaskId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numStageDetailsId] [numeric](18, 0) NULL,
--	[vcTaskName] [varchar](500) NULL,
--	[numHours] [numeric](18, 0) NULL,
--	[numMinutes] [numeric](18, 0) NULL,
--	[numAssignTo] [numeric](18, 0) NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[dtmCreatedOn] [datetime] NULL,
-- CONSTRAINT [PK_StagePercentageDetailsTask] PRIMARY KEY CLUSTERED 
--(
--	[numTaskId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE [dbo].[StagePercentageDetailsTask] ADD  CONSTRAINT [DF_StagePercentageDetailsTask_dtmCreatedOn]  DEFAULT (getdate()) FOR [dtmCreatedOn]
--GO

--ALTER TABLE StagePercentageDetails ADD bitIsDueDaysUsed BIT DEFAULT 0
--ALTER TABLE StagePercentageDetails ADD numTeamId NUMERIC(18,0) DEFAULT 0
--ALTER TABLE StagePercentageDetails ADD bitRunningDynamicMode BIT DEFAULT 0

--ALTER TABLE Sales_process_List_Master ADD bitAssigntoTeams BIT DEFAULT 0
--ALTER TABLE Sales_process_List_Master ADD bitAutomaticStartTimer BIT DEFAULT 0

--UPDATE StagePercentageMaster SET numStagePercentage=66 WHERE numStagePercentageId=16

--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(14,50,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(15,33,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(16,33,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(17,34,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(18,25,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(19,25,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(20,25,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(22,20,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(21,20,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(23,20,1)
--INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(24,20,1)

------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Pick, Pack, & Ship','PickPackShip','PickPackShip','PickPackShip','PickPackShip','V','R','EditBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(26,'Pick, Pack, & Ship','R','PickPackShip','PickPackShip',0,0,'V','PickPackShip',1,'PickPackShip',0,7,1,'PickPackShip',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,26,1,1,'Pick, Pack, & Ship','PickPackShip','PickPackShip',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------------------------
--ALTER TABLE Domain ADD numAuthorizePercentage NUMERIC(18,0) DEFAULT 0


--ALTER TABLE OpportunityBizDocs ADD numSourceBizDocId NUMERIC(18,0)

--ALTER TABLE eCommerceDTL ADD bitHideAddtoCart BIT DEFAULT 0

--ALTER TABLE OpportunityBizDocs ADD dtExpectedDeliveryDate datetime


--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		57,
--		'ManufacturerList',
--		'~/UserControls/ManufacturerList.ascx',
--		'{#ManufacturerList#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		57,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		56,
--		'Fulfilment',
--		'~/UserControls/fullfilment.ascx',
--		'{#fulfilment#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		56,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)


--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		55,
--		'Support',
--		'~/UserControls/Support.ascx',
--		'{#Support#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		55,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

--ALTER TABLE Domain ADD vcSupportTabs VARCHAR(300) DEFAULT 'Support'
--ALTER TABLE Domain ADD bitSupportTabs BIT DEFAULT 0

--UPDATE 
--	Domain 
--SET 
--	vcSupportTabs='Support',
--	bitSupportTabs=0

--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		54,
--		'ShippingStatus',
--		'~/UserControls/ShippingStatus.ascx',
--		'{#ShippingStatus#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		54,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)
------------------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		53,
--		'SerialLotReturn',
--		'~/UserControls/SerialLotReturn.ascx',
--		'{#SerialLotReturn#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		53,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

-------------------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		52,
--		'ItemReturn',
--		'~/UserControls/ItemReturn.ascx',
--		'{#ItemReturn#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		52,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)
--------------------------------------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		51,
--		'OpenRMA',
--		'~/UserControls/OpenRMA.ascx',
--		'{#OpenRMA#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		51,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

----------------------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		50,
--		'OpenCases',
--		'~/UserControls/OpenCases.ascx',
--		'{#OpenCases#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		50,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)
--------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		49,
--		'ItemPurchasedHistory',
--		'~/UserControls/ItemPurchasedHistory.ascx',
--		'{#ItemPurchasedHistory#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		49,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

--------------------------------

--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		48,
--		'SalesOpportunity',
--		'~/UserControls/SalesOpportunity.ascx',
--		'{#SalesOpportunity#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		48,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

--------------------------------------------------
--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		47,
--		'customerstatement',
--		'~/UserControls/customerstatement.ascx',
--		'{#customerstatement#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		47,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

--UPDATE 
--	Domain 
--SET 
--	vcSalesOrderTabs='Sales Orders',
--	vcSalesQuotesTabs='Sales Quotes',
--	vcItemPurchaseHistoryTabs='Item Purchase History',
--	vcItemsFrequentlyPurchasedTabs= 'Items Frequently Purchased',
--	vcOpenCasesTabs='Open Cases',
--	vcOpenRMATabs='Open RMAs',
--	bitSalesOrderTabs=0,
--	bitSalesQuotesTabs=0,
--	bitItemPurchaseHistoryTabs=0,
--	bitItemsFrequentlyPurchasedTabs=0,
--	bitOpenCasesTabs=0,
--	bitOpenRMATabs=0

--ALTER TABLE Domain ADD vcSalesOrderTabs VARCHAR(300) DEFAULT 'Sales Orders'
--ALTER TABLE Domain ADD vcSalesQuotesTabs VARCHAR(300) DEFAULT 'Sales Quotes'
--ALTER TABLE Domain ADD vcItemPurchaseHistoryTabs VARCHAR(300) DEFAULT 'Item Purchase History'
--ALTER TABLE Domain ADD vcItemsFrequentlyPurchasedTabs VARCHAR(300) DEFAULT 'Items Frequently Purchased'
--ALTER TABLE Domain ADD vcOpenCasesTabs VARCHAR(300) DEFAULT 'Open Cases'
--ALTER TABLE Domain ADD vcOpenRMATabs VARCHAR(300) DEFAULT 'Open RMAs'


--ALTER TABLE Domain ADD bitSalesOrderTabs BIT DEFAULT 0
--ALTER TABLE Domain ADD bitSalesQuotesTabs BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemPurchaseHistoryTabs BIT DEFAULT 0
--ALTER TABLE Domain ADD bitItemsFrequentlyPurchasedTabs BIT DEFAULT 0
--ALTER TABLE Domain ADD bitOpenCasesTabs BIT DEFAULT 0
--ALTER TABLE Domain ADD bitOpenRMATabs BIT DEFAULT 0



--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		46,
--		'Gallery',
--		'~/UserControls/Gallery.ascx',
--		'{#Gallery#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		46,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)

----------------------------------------------------------------

--INSERT INTO PageElementMaster
--	(
--		 [numElementID],
--		 [vcElementName],
--		 [vcUserControlPath],
--		 vcTagName,
--		 bitCustomization,
--		 bitAdd,
--		 bitDelete
--	) 
--	VALUES
--	(
--		45,
--		'Sitemap',
--		'~/UserControls/Sitemap.ascx',
--		'{#Sitemap#}',
--		1,
--		0,
--		0
--	)

--	INSERT INTO PageElementAttributes
--	(
--		numElementID, 
--		vcAttributeName, 
--		vcControlType, 
--		vcControlValues, 
--		bitEditor
--	)
--	VALUES
--	(
--		45,
--		'Html Customize',
--		'HtmlEditor',
--		NULL,
--		1
--	)


--ALTER TABLE ActivityFormAuthentication ADD bitAttendee BIT DEFAULT 0

--ALTER TABLE ActivityFormAuthentication ADD bitAttendee BIT DEFAULT 0

-------------------------------------------------------

--CREATE TABLE [dbo].[ActivityFormAuthentication](
--	[numActivityAuthenticationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numGroupId] [numeric](18, 0) NULL,
--	[bitFollowupStatus] [bit] NULL,
--	[bitPriority] [bit] NULL,
--	[bitActivity] [bit] NULL,
--	[bitCustomField] [bit] NULL,
--	[bitFollowupAnytime] [bit] NULL,
-- CONSTRAINT [PK_ActivityFormAuthentication] PRIMARY KEY CLUSTERED 
--(
--	[numActivityAuthenticationId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitFollowupStatus]  DEFAULT ((0)) FOR [bitFollowupStatus]
--GO

--ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitPriority]  DEFAULT ((0)) FOR [bitPriority]
--GO

--ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitActivity]  DEFAULT ((0)) FOR [bitActivity]
--GO

--ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitCustomField]  DEFAULT ((0)) FOR [bitCustomField]
--GO

--ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitFollowupAnytime]  DEFAULT ((0)) FOR [bitFollowupAnytime]
--GO


-------------------------------------------------

--INSERT INTO BizFormWizardModule(vcModule) values('Activity')

-----------------------------------

INSERT INTO DynamicFormMaster
			(numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, bitDeleted
			, tintFlag, bitWorkFlow, vcLocationID, bitAllowGridColor, numBizFormModuleID)
		VALUES
			(130,'Activity Form','Y','Y',0,0,NULL,NULL,NULL,11)

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 43)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,43,'Organization Rating',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 43)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,43,'Organization Status',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 43)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,43,'Territory',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 7 AND numFormID = 43)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,7,43,'Campaign',0,0,0,0,1)
--END

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Source','numPartenerSource','numPartenerSource','numPartenerSource','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(38,'Partner Source','R','SelectBox','numPartenerSource',0,0,'V','numPartenerSource',1,'OpportunityMaster',0,7,1,'numPartenerSource',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,38,1,1,'Partner Source','SelectBox','numPartenerSource',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(38,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,38,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


-------Sales Order


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Source','numPartenerSource','numPartenerSource','numPartenerSource','DivisionMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Partner Source','R','SelectBox','numPartenerSource',0,0,'V','numPartenerSource',1,'DivisionMaster',0,7,1,'numPartenerSource',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,34,1,1,'Partner Source','SelectBox','numPartenerSource',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(39,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,39,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','DivisionMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'DivisionMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,34,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(39,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,39,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--ALTER TABLE DivisionMaster ADD numPartenerSource NUMERIC(18,0) DEFAULT 0
--ALTER TABLE DivisionMaster ADD numPartenerContact NUMERIC(18,0) DEFAULT 0

--ALTER TABLE OpportunityMaster ADD numPartenerContact NUMERIC(18,0) DEFAULT 0

--ALTER TABLE [State] ADD vcAbbreviations VARCHAR(200)

--ALTER TABLE HTMLFormURL ADD numSubFormId NUMERIC(18) DEFAULT 0 

--ALTER TABLE LeadsubForms ADD vcFormName VARCHAR(500)

--ALTER TABLE LeadsubForms ADD numDomainId NUMERIC
----------------------------------------------------------

--ALTER TABLE DycFormConfigurationDetails ADD numSubFormID NUMERIC

--------------------------------------
--CREATE TABLE [dbo].[LeadsubForms](
--	[numSubFormId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[bitByPassRoutingRules] [bit] NULL,
--	[numAssignTo] [numeric](18, 0) NULL,
--	[bitDripCampaign] [bit] NULL,
--	[numDripCampaign] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_LeadsubForms] PRIMARY KEY CLUSTERED 
--(
--	[numSubFormId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]



----------------------------------------------------------

--CREATE TABLE [dbo].[OverrideAssignTo](
--	[numAssignedContact] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numContactId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_OverrideAssignTo] PRIMARY KEY CLUSTERED 
--(
--	[numAssignedContact] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]


--ALTER TABLE CartItems ADD vcCoupon VARCHAR(200)


--ALTER TABLE domain ADD bitchkOverRideAssignto BIT DEFAULT 0

/***************************************************************************/
/*********************** LAST UPDATE ON 17 JULY 2016 ******************/
/***************************************************************************/

--ALTER TABLE CartItems ADD bitParentPromotion BIT DEFAULT 0

--ALTER TABLE CartItems ADD PromotionID NUMERIC(18,0)
--ALTER TABLE CartItems ADD PromotionDesc VARCHAR(MAX)


--ALTER TABLE eCommerceDTL ADD vcPreSellUp VARCHAR(200)
--ALTER TABLE eCommerceDTL ADD vcPostSellUp VARCHAR(200)

--ALTER TABLE ItemExtendedDetails ADD txtShortDesc NTEXT

--ALTER TABLE OpportunityMaster ADD bitIsInitalSalesOrder BIT DEFAULT 0

--ALTER TABLE DOMAIN ADD bitMarginPriceViolated BIT DEFAULT 0

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(4,'Item Short HTML Description','txtShortDesc','txtShortDesc','txtShortDesc','ItemExtendedDetails','V','R','EditBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(20,'Item Short HTML Description','R','EditBox','txtShortDesc',0,0,'V','txtShortDesc',1,'ItemExtendedDetails',0,7,1,'txtShortDesc',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,20,1,1,'Item Short HTML Description','EditBox','txtShortDesc',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)

--UPDATE DycFieldMaster SET bitImport=1 WHERE numFieldId=@numFieldID

--UPDATE 
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--ALTER TABLE OpportunityBizDocs ADD bitShippingGenerated BIT DEFAULT 0

--ALTER TABLE DOMAIN ADD numDefaultSalesShippingDoc NUMERIC(18,2) DEFAULT 0
--ALTER TABLE DOMAIN ADD numDefaultPurchaseShippingDoc NUMERIC(18,2) DEFAULT 0

--ALTER TABLE OpportunityItems ADD numSortOrder NUMERIC(18,0)

--ALTER TABLE OpportunityItems
--ALTER COLUMN numCost decimal(30, 16)
----======================================================================
--ALTER TABLE ItemGroups ADD bitCombineAssemblies BIT DEFAULT 0
--ALTER TABLE ItemGroups ADD numMapToDropdownFld NUMERIC(18,0) DEFAULT 0
--ALTER TABLE ItemGroups ADD numProfileItem NUMERIC(18,0) DEFAULT 0
--ALTER TABLE ItemGroupsDTL ADD numListid NUMERIC(18,0) DEFAULT 0

--======================================================

--ALTER TABLE OpportunityMaster ADD numPartner NUMERIC(18,0)

--ALTER TABLE DivisionMaster ADD vcPartnerCode VARCHAR(100)

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--SET @numFieldID=(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcFieldName='Partner Source' AND numModuleID=3)
--INSERT INTO ReportFieldGroupMappingMaster
--			(numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering)
--			VALUES(3,@numFieldID,'Partner Source','SelectBox','N',1,1,0,1)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--SET @numFieldID=(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcFieldName='Partner Code' AND numModuleID=1)
--INSERT INTO ReportFieldGroupMappingMaster
--			(numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering)
--			VALUES(1,@numFieldID,'Partner Code','TextBox','V',1,1,0,1)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Source','numPartner','numPartner','numPartner','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(90,'Partner Source','R','SelectBox','numPartner',0,0,'V','numPartner',1,'OpportunityMaster',0,7,1,'numPartner',1,7,1,1,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,90,1,1,'Partner Source','SelectBox','numPartner',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Source','numPartner','numPartner','numPartner','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(39,'Partner Source','R','SelectBox','numPartner',0,0,'V','numPartner',1,'OpportunityMaster',0,7,1,'numPartner',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,39,1,1,'Partner Source','SelectBox','numPartner',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Partner Code','vcPartnerCode','vcPartnerCode','PartnerCode','DivisionMaster','V','R','TextBox',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(35,'Partner Code','R','TextBox','vcPartnerCode',0,0,'V','vcPartnerCode',0,'DivisionMaster',0,7,1,'PartnerCode',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,35,1,1,'Partner Code','TextBox','PartnerCode',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(1,'Partner Code','vcPartnerCode','vcPartnerCode','PartnerCode','DivisionMaster','V','R','TextBox',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(34,'Partner Code','R','TextBox','vcPartnerCode',0,0,'V','vcPartnerCode',0,'DivisionMaster',0,7,1,'PartnerCode',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(1,@numFieldID,34,1,1,'Partner Code','TextBox','PartnerCode',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


/***************************************************************************/
/*********************** LAST UPDATE ON 28 May 2016 ******************/
/***************************************************************************/

=====================================================================


--/****** Object:  Table [dbo].[Approval_transaction_log]    Script Date: 5/28/2016 4:32:09 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Approval_transaction_log](
--	[int_transaction_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numModuleId] [numeric](18, 0) NULL,
--	[numRecordId] [numeric](18, 0) NULL,
--	[numApprovedBy] [numeric](18, 0) NULL,
--	[dtmApprovedOn] [datetime] NULL CONSTRAINT [DF_Approval_transaction_log_dtm_approved_on]  DEFAULT (getdate()),
-- CONSTRAINT [PK_Approval_transaction_log] PRIMARY KEY CLUSTERED 
--(
--	[int_transaction_id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO


--ALTER TABLE Item
--	ADD numNoItemIntoContainer NUMERIC(18,0) DEFAULT 0

--======================================================================

--ALTER TABLE Item
--	ADD numContainer NUMERIC(18,0) DEFAULT 0

--=====================================================================


--BEGIN TRY
--BEGIN TRANSACTION
	

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,bitAddIsPopUp,numTabID,intSortOrder,vcAddURL
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),37,173,'Containers','../Items/frmItemList.aspx?Page=Containers&ItemGroup=0',1,1,80,1,'~/Items/frmNewItem.aspx?FormID=86&ItemType=Containers'
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		80,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



--======================================================================

--ALTER TABLE Item
--	ADD bitContainer BIT DEFAULT 0

--=====================================================================


--ALTER TABLE Domain
--	ADD bitApprovalforOpportunity BIT

--ALTER TABLE Domain 
--	ADD intOpportunityApprovalProcess INT

--ALTER TABLE OpportunityMaster
--	ADD intPromotionApprovalStatus INT DEFAULT 0

--ALTER TABLE OpportunityMaster
--	ADD intChangePromotionStatus INT DEFAULT 0

--====================================================================


--BEGIN TRY
--BEGIN TRANSACTION
	

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),1,0,'Activities','',1,36,1
--	),
--	(
--		(@numMAXPageNavID + 2),1,(@numMAXPageNavID + 1),'Action Items & Meetings','../common/frmTicklerdisplay.aspx?SelectedIndex=0',1,36,1
--	),
--	(
--		(@numMAXPageNavID + 3),1,(@numMAXPageNavID + 1),'Opportunities & Projects','../common/frmTicklerdisplay.aspx?SelectedIndex=1',1,36,1
--	),
--	(
--		(@numMAXPageNavID + 4),1,(@numMAXPageNavID + 1),'Cases','../common/frmTicklerdisplay.aspx?SelectedIndex=2',1,36,1
--	),
--	(
--		(@numMAXPageNavID + 5),1,(@numMAXPageNavID + 1),'Approval Requests','../common/frmTicklerdisplay.aspx?SelectedIndex=3',1,36,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		36,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		36,
--		(@numMAXPageNavID + 2),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		36,
--		(@numMAXPageNavID + 3),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		36,
--		(@numMAXPageNavID + 4),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		36,
--		(@numMAXPageNavID + 5),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--===================================================================


----------------------------------------------------------------


--INSERT INTO BizFormWizardModule(vcModule) values('Web to Case')

-----------------------------------

--INSERT INTO DynamicFormMaster
--			(numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, bitDeleted
--			, tintFlag, bitWorkFlow, vcLocationID, bitAllowGridColor, numBizFormModuleID)
--		VALUES
--			(128,'Web to Case Form','Y','Y',0,0,NULL,NULL,NULL,10)

----------------------------------------

----INSERT INTO DynamicFormFieldMaster
----			(numFormID, vcFormFieldName, vcFieldType, vcAssociatedControlType, vcDbColumnName, vcListItemType, numListID, bitDeleted, vcFieldDataType, vcOrigDbColumnName, bitAllowEdit, vcLookBackTableName, bitDefault, [order], bitInResults)
----			VALUES
----			(128,'Email','R','EditBox','vcEmail','',0,0,'V','vcEmail',1,'AdditionalContactsInformation',0,NULL,1)

--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,128,128,0,0,'Email','TextBox',1,0,0,1,2113)

--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,127,128,0,0,'Subject','TextBox',1,0,0,1,1629)
			
--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,129,128,0,0,'Priority','SelectBox',1,0,0,1,1631)

--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,130,128,0,0,'Origin','SelectBox',1,0,0,1,1632)

--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,145,128,0,0,'Description','TextArea',1,0,0,1,1643)

--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,133,128,0,0,'Type','SelectBox',1,0,0,1,1635)
			


--INSERT INTO DycFormField_Mapping
--			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
--			VALUES
--			(7,136,128,0,0,'Reason','SelectBox',1,0,0,1,1638)


-------------------------------------------------------------------------------------------------------

--INSERT INTO AccountingChargeTypes (vcChageType, vcChargeAccountCode, chChargeCode)
--VALUES('Reimbursable Expense Receivable','','RE')

--ALTER TABLE TimeAndExpense ADD numServiceItemID numeric(18,0)

--ALTER TABLE TimeAndExpense ADD numClassID numeric(18,0)

--ALTER TABLE TimeAndExpense ADD numApprovalComplete numeric(18,0) DEFAULT 0

-----------------------------------------------------------

--ALTER TABLE DOMAIN ADD intTimeExpApprovalProcess INT DEFAULT 0
-------------------------------------------------------------

--ALTER TABLE TimeAndExpense ADD numExpId numeric(18,0)


--/****** Object:  Table [dbo].[ApprovalConfig]    Script Date: 5/28/2016 4:18:11 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalConfig](
--	[numConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numModule] [numeric](18, 0) NULL,
--	[numUserId] [numeric](18, 0) NULL,
--	[numLevel1Authority] [numeric](18, 0) NULL,
--	[numLevel2Authority] [numeric](18, 0) NULL,
--	[numLevel3Authority] [numeric](18, 0) NULL,
--	[numLevel4Authority] [numeric](18, 0) NULL,
--	[numLevel5Authority] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_ApprovalConfig] PRIMARY KEY CLUSTERED 
--(
--	[numConfigID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID
--			)
--	SELECT @numPageNavID ,13,66,'Approval Process','../admin/frmManageApprovalConfig.aspx',NULL,1,-1
	

	
--	SELECT * INTO #temp FROM
--    (
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0);
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
--		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--						CASE WHEN bitFixed = 1
--							 THEN CASE WHEN EXISTS ( SELECT numTabName
--													 FROM   TabDefault
--													 WHERE  numTabId = T.numTabId
--															AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--									   THEN ( SELECT TOP 1
--														numTabName
--											  FROM      TabDefault
--											  WHERE     numTabId = T.numTabId
--														AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--											)
--									   ELSE T.numTabName
--								  END
--							 ELSE T.numTabName
--						END numTabname,
--						vcURL,
--						bitFixed,
--						1 AS tintType,
--						T.vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      TabMaster T
--						CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--						LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--			  WHERE     bitFixed = 1
--						AND D.numDomainId <> -255
--						AND t.tintTabType = 1
	          
--			  UNION ALL
--			  SELECT    -1 AS [numTabID],
--						'Administration' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--						WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--			) TABLE2       		
	                    
--		INSERT  INTO dbo.TreeNavigationAuthorization
--			(
--			  numGroupID,
--			  numTabID,
--			  numPageNavID,
--			  bitVisible,
--			  numDomainID,
--			  tintType
--			)
--			SELECT  numGroupID,
--					PND.numTabID,
--					numPageNavID,
--					bitVisible,
--					numDomainID,
--					tintType
--			FROM    dbo.PageNavigationDTL PND
--					JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--					WHERE PND.numPageNavID= @numPageNavID
			
--			DROP TABLE #tempData1
			
--			SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp

	-----------------------------------------------------------
	-----------------------------------------------------------


--INSERT INTO RecordHistoryModuleMaster VALUES(7,'Transaction')


--CREATE TABLE [dbo].[TrackNotification](
--	[numTrackId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numUserId] [numeric](18, 0) NULL,
--	[numModuleId] [numeric](18, 0) NULL,
--	[numRecordId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_TrackNotification] PRIMARY KEY CLUSTERED 
--(
--	[numTrackId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER DATABASE [Production.2014] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE


--INSERT INTO [dbo].[ModuleMaster](vcModuleName, tintGroupType)VALUES('Global Alert Panel',1)
--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(118,42,'','Organization',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(119,42,'','Case',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(120,42,'','Opportunity',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(121,42,'','Order',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(122,42,'','Projects',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(123,42,'','Email',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(124,42,'','Tickler',1,0,0,0,0,'',0)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(125,42,'','Process',1,0,0,0,0,'',0)


--CREATE TABLE [dbo].[tbl_LastExecution](
--	[dtm_LastExecution] [datetime] NULL
--) ON [PRIMARY]


--INSERT INTO [dbo].[tbl_LastExecution]
--           ([dtm_LastExecution])
--     VALUES
--           ('2016-02-04 00:00:00.000')

--CREATE VIEW [dbo].[View_CompanyAlert]
--AS
--SELECT        numCompanyId, vcCompanyName, numCompanyType, numCompanyRating, numCompanyIndustry, numCompanyCredit, txtComments, vcWebSite, vcWebLabel1, vcWebLink1, vcWebLabel2, vcWebLink2, 
--                         vcWebLabel3, vcWeblabel4, vcWebLink3, vcWebLink4, numAnnualRevID, numNoOfEmployeesId, vcHow, vcProfile, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitPublicFlag, 
--                         numDomainID
--FROM            dbo.CompanyInfo
--WHERE        (bintCreatedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_CaseAlert]
--AS
--SELECT        numCaseId, numDivisionID, numContactId, vcCaseNumber, intTargetResolveDate, textSubject, numStatus, numPriority, textDesc, textInternalComments, numReason, numOrigin, numType, numRecOwner, 
--                         numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainID, tintSupportKeyType, numAssignedBy, numAssignedTo, numContractId
--FROM            dbo.Cases
--WHERE        (bintModifiedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_OpportunityAlert]
--AS
--SELECT        numOppId, tintOppType, numContactId, numDivisionId, bintAccountClosingDate, txtComments, bitPublicFlag, tintSource, vcPOppName, intPEstimatedCloseDate, numPClosingPercent, numCampainID, 
--                         monPAmount, lngPConclAnalysis, tintActive, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainId, numRecOwner, tintOppStatus, tintshipped, bitOrder, numSalesOrPurType, 
--                         numAssignedTo, numAssignedBy, numUserClosed, tintBillToType, tintShipToType, numCurrencyID, fltExchangeRate, monDealAmount, numStatus, numBusinessProcessID, fltDiscountTotal, bitDiscountTypeTotal, 
--                         bitStockTransfer, bintOppToOrder, tintSourceType, tintTaxOperator, bitBillingTerms, intBillingDays, bitInterestType, fltInterest, fltDiscount, bitDiscountType, numDiscountAcntType, numOppBizDocTempID, 
--                         monShipCost, vcWebApiOrderNo, vcOppRefOrderNo, vcCouponCode, dtItemReceivedDate, bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass, 
--                         bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, bitUseShippersAccountNo, vcLanedCost, monLandedCostTotal, bintClosedDate, monDealAmountWith4Decimals, 
--                         vcRecurrenceType, bitRecurred, bitFromWorkOrder, bitStopMerge, numBillToAddressID, numShipToAddressID, numShipmentMethod
--FROM            dbo.OpportunityMaster
--WHERE        (bintModifiedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_ProjectsAlert]
--AS
--SELECT        numProId, vcProjectID, numIntPrjMgr, numOppId, intDueDate, numCustPrjMgr, numDivisionId, txtComments, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainId, numRecOwner, 
--                         tintProStatus, bintProClosingDate, numAssignedby, numAssignedTo, numContractId, vcProjectName, numAccountID, numProjectType, numProjectStatus, numAddressID, dtCompletionDate, monTotalExpense, 
--                         monTotalIncome, monTotalGrossProfit
--FROM            dbo.ProjectsMaster
--WHERE        (bintModifiedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_EmailAlert]
--AS
--SELECT        numEmailHstrID, vcSubject, vcBody, bintCreatedOn, numNoofTimes, vcItemId, vcChangeKey, bitIsRead, vcSize, bitHasAttachments, dtReceivedOn, tintType, chrSource, vcCategory, vcBodyText, numDomainID, 
--                         numUserCntId, numUid, numNodeId, vcFrom, vcTo, vcCC, vcBCC, numListItemId, numEmailId, IsReplied, bitArchived, numOldNodeID, bitInvisible
--FROM            dbo.EmailHistory
--WHERE        (bintCreatedOn >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_TicklerAlert]
--AS
--SELECT        numCommId, bitTask, numContactId, numDivisionId, textDetails, intSnoozeMins, intRemainderMins, numStatus, numActivity, numAssign, tintSnoozeStatus, tintRemStatus, numOppId, numCreatedBy, 
--                         dtCreatedDate, numModifiedBy, dtModifiedDate, numDomainID, bitClosedFlag, vcCalendarName, bitOutlook, dtStartTime, dtEndTime, numAssignedBy, bitSendEmailTemp, numEmailTemplate, tintHours, bitAlert, 
--                         CaseId, CaseTimeId, CaseExpId, numActivityId, bitEmailSent, bitFollowUpAnyTime, dtEventClosedDate, LastSnoozDateTimeUtc
--FROM            dbo.Communication
--WHERE        (dtModifiedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--CREATE VIEW [dbo].[View_ProcessAlert]
--AS
--SELECT        numStageDetailsId, numStagePercentageId, tintConfiguration, vcStageName, numDomainId, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, slp_id, numAssignTo, vcMileStoneName, 
--                         tintPercentage, bitClose, tinProgressPercentage, dtStartDate, dtEndDate, numParentStageID, intDueDays, numProjectID, numOppID, vcDescription, bitFromTemplate, bitExpenseBudget, monExpenseBudget, 
--                         bitTimeBudget, monTimeBudget
--FROM            dbo.StagePercentageDetails
--WHERE        (bintModifiedDate >=
--                             (SELECT        TOP (1) dtm_LastExecution
--                               FROM            dbo.tbl_LastExecution))

--ALTER TABLE Domain ADD bitApprovalforTImeExpense bit




--ALTER TABLE AdditionalContactsInformation ADD vcLinkedinId VARCHAR(30) NULL
--ALTER TABLE AdditionalContactsInformation ADD vcLinkedinUrl VARCHAR(300) NULL

--===============================================================================

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(2,'Linkedin ID','vcLinkedinId','vcLinkedinId','LinkedinId','AdditionalContactsInformation','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(10,'Linkedin ID','R','Label','vcLinkedinId',0,0,'V','vcLinkedinId',0,'Contacts',0,7,1,'LinkedinId',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,10,1,1,'Linkedin ID','Label','LinkedinId',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ROLLBACK

--================================================================================
--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(2,'Linkedin URL','vcLinkedinUrl','vcLinkedinUrl','LinkedinUrl','AdditionalContactsInformation','V','R','Website',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(10,'Linkedin URL','R','Label','vcLinkedinUrl',0,0,'V','vcLinkedinUrl',0,'Contacts',0,7,1,'LinkedinId',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(2,@numFieldID,10,1,1,'Linkedin URL','Website','LinkedinUrl',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ALTER TABLE UserMaster ADD intAssociate int NULL
--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(1,539,97,0,0,'Organization ID','','',7,1,7,1,0,0,1,0,1,0,1,null)

--ROLLBACK

--============================================================

--ALTER TABLE UserMaster ADD vcLinkedinId VARCHAR(300) NULL

--UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextArea' WHERE numFormID=26 and numModuleID=3 and numFieldID=253
--UPDATE DynamicFormFieldMaster SET vcAssociatedControlType='TextArea' where numFormFieldId=1381
--UPDATE DycFieldMaster SET vcAssociatedControlType='TextArea'  where numFieldId=253

--CREATE TABLE [dbo].[TrackNotification](
--	[numTrackId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numUserId] [numeric](18, 0) NULL,
--	[numModuleId] [numeric](18, 0) NULL,
--	[numRecordId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_TrackNotification] PRIMARY KEY CLUSTERED 
--(
--	[numTrackId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--ALTER TABLE OpportunityItems ADD vcNotes varchar(1000) NULL
--==========================

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Notes','vcNotes','vcNotes','Notes','OpportunityItems','V','R','TextArea',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(26,'Notes','R','TextArea','vcNotes',0,0,'V','vcNotes',0,'OpportunityItems',0,7,1,'Notes',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,26,1,1,'Notes','TextArea','Notes',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ROLLBACK

--ALTER TABLE UserMaster ADD intAssociate int NULL
--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(1,539,97,0,0,'Organization ID','','',7,1,7,1,0,0,1,0,1,0,1,null)

--INSERT INTO [dbo].[PageMaster]
--	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
--	VALUES
--	(125,42,'','Process',1,0,0,0,0,'',0)

--ALTER TABLE VendorShipmentMethod ADD bitPrimary bit
--ALTER TABLE OpportunityMaster ADD numShipmentMethod numeric(18, 0)


