/******************************************************************
Project: Release 10.5 Date: 27.NOV.2018
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCartItemInclusionDetails')
DROP FUNCTION GetCartItemInclusionDetails
GO
CREATE FUNCTION [dbo].[GetCartItemInclusionDetails]
(
	@numCartID NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)    
AS    
BEGIN   
	DECLARE @vcInclusionDetails AS VARCHAR(MAX)

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		vcChildKitItemSelection VARCHAR(MAX),
		numItemCode NUMERIC(18,0),
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	--FIRST LEVEL CHILD
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		CI.numCartId,
		CI.vcChildKitItemSelection,
		ID.numChildItemID,
		0,
		CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		1,
		CASE 
			WHEN ISNULL(I.bitKitParent,0)=1
			THEN
				CONCAT('<li><b>',I.vcItemName, ':</b> ') 
			ELSE 
				CONCAT('<li>',I.vcItemName, ' (', CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
		END
	FROM 
		CartItems CI
	INNER JOIN
		ItemDetails ID
	ON
		CI.numItemCode = ID.numItemKitID
	INNER JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
	WHERE
		CI.numCartId = @numCartID
	
	
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		c.numCartId,
		c.vcChildKitItemSelection,
		ID.numChildItemID,
		c.numItemCode,
		CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		2,
		CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
	FROM 
		@TEMPTABLE c
	CROSS APPLY
	(
		SELECT
			SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			dbo.Split(c.vcChildKitItemSelection,',') 
	) AS t2
	INNER JOIN
		ItemDetails ID
	ON
		ID.numItemKitID = t2.numKitItemID
		AND ID.numChildItemID = t2.numKitChildItemID
		AND c.numItemCode = t2.numKitItemID
	INNER JOIN
		Item I
	ON
		t2.numKitChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(MAX)
	);

	INSERT INTO @TEMPTABLEFINAL
	(
		numCartID,
		numItemCode,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	)
	SELECT
		numCartID,
		numItemCode,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	FROM 
		@TEMPTABLE 
	WHERE 
		tintLevel=1
		
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

	WHILE @i <= @iCount
	BEGIN
		SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))

		IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
		BEGIN
			--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

			SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)

			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</li>')
		END

		SET @i = @i + 1
	END

	SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	
	RETURN @vcInclusionDetails
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0),
	  @tintCommitAllocation TINYINT,
	  @bitFromBizDoc NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @bitAssembly BIT
	DECLARE @bitKitParent BIT
	DECLARE @bitWorkOrder BIT

	SELECT 
		@bitAssembly=ISNULL(bitAssembly,0)
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitWorkOrder=ISNULL(bitWorkOrder,0) 
	FROM 
		OpportunityItems 
	INNER JOIN 
		Item 
	ON 
		OpportunityItems.numItemCode=Item.numItemCode 
	WHERE 
		numOppId=@numOppID AND numoppitemtCode=@numOppItemID

	IF @bitKitParent = 1
	BEGIN
		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,bitDropship,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKI.numOppItemID,
			OI.bitDropShip,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1,
			(CASE 
				WHEN OKI.numWareHouseItemId > 0 AND ISNULL(OI.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,0,1,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
							ELSE
								CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,1,0,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li><b><span style="color:red">',I.vcItemName, ':</b> ','</span>')
							ELSE
								CONCAT('<li><b>',I.vcItemName, ':</b> ') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
									END) 
							END
					END 
				ELSE 
					(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN 
							CONCAT('<li><b>',I.vcItemName, ':</b> ')
						ELSE
							CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
					END)
			END)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId=OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		LEFT JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
		WHERE
			OM.numOppId = @numOppID
			AND OI.numoppitemtCode=@numOppItemID

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2,
			(CASE 
				WHEN OKCI.numWareHouseItemId > 0 AND ISNULL(c.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,0,1,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,1,0,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityKitChildItems OKCI
		LEFT JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppID
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID


		INSERT INTO @TEMPTABLEFINAL
		(
			numOppItemCode,
			bitDropship,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		)
		SELECT
			numOppItemCode,
			bitDropship,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		FROM 
			@TEMPTABLE 
		WHERE 
		tintLevel=1
		
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))

			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
			BEGIN
				--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

				SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)

				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</li>')
			END

			SET @i = @i + 1
		END

		SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	END
	ELSE IF @bitAssembly=1 AND @bitWorkOrder=1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPFINAL TABLE
		(
			ID INT IDENTITY(1,1)
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		SET @i  = 1

		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END



		INSERT INTO @TEMPFINAL
		(
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		)
		SELECT
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		FROM 
			@TEMP
		WHERE 
			ItemLevel=2
		
		SET @i = 1

		SELECT @iCount = COUNT(*) FROM @TEMPFINAL

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT
																																		(CASE 
																																			WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
																																			THEN 
																																				CONCAT(I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
																																			ELSE
																																				CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
																																		END)
																																	FROM
																																		@TEMPFINAL T1
																																	INNER JOIN
																																		Item I
																																	ON
																																		T1.numItemCode = I.numItemCode
																																	LEFT JOIN
																																		WareHouseItems WI
																																	ON
																																		T1.numWarehouseItemID = WI.numWareHouseItemID
																																	LEFT JOIN
																																		UOM
																																	ON
																																		UOM.numUOMId = (CASE WHEN ISNULL(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
																																	WHERE 
																																		ID = @i))

			SET @i = @i + 1
		END
	END
	

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeDeleteOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeDeleteOppertunity')
DROP PROCEDURE USP_CheckCanbeDeleteOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeDeleteOppertunity]              
	@numOppId AS NUMERIC(18,0),
	@tintError AS TINYINT=0 output
AS
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)
	DECLARE @OppStatus TINYINT
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                           
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT                                                                                                                           
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @tintShipped AS TINYINT 

	SET @tintError=0          
	SELECT 
		@OppType=tintOppType,
		@OppStatus=tintOppStatus,
		@tintShipped=tintShipped,
		@numDomainID=numDomainId 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@numOppId
 
	IF @OppType=2 AND @OppStatus=1
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=ISNULL(numWarehouseItmsID,0),
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@numOppId 
			AND (bitDropShip=0 OR bitDropShip is NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@numoppitemtCode AND numOppItemID=@numoppitemtCode)
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @tintError=0 SET @tintError=1
				END
				ELSE
				BEGIN
					SET @numUnits = @numUnits - (SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@numoppitemtCode AND numOppItemID=@numoppitemtCode)
				END
			END

			IF @tintError = 0
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0),
					@onOrder=ISNULL(numonOrder,0) 
				FROM 
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID 
  
				IF @onHand < @numQtyReceived
				BEGIN  
					IF @tintError=0 SET @tintError=1
				END  
				 
				IF @onOrder < (@numUnits-@numQtyReceived)
				BEGIN  
					if @tintError=0 set @tintError=1
				END
			END
	
			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode 
				AND numOppId=@numOppId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId AS NUMERIC(18,0),
@bitCheck AS BIT=0
AS              
BEGIN         
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @Sel AS int   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                                
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT                                                                                 
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT

	SET @Sel=0          
	SELECT 
		@OppType=tintOppType,
		@numDomainID=numDomainId
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@intOpportunityId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 and bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@intOpportunityId 
			AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			-- Selected Global Warehouse Location 
			IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@intOpportunityId AND numOppItemID=@numoppitemtCode)
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@intOpportunityId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @Sel=0 SET @Sel=1
				END
				ELSE
				BEGIN
					SET @numUnits = @numUnits - (SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@intOpportunityId AND numOppItemID=@numoppitemtCode)
				END
			END
			
			IF @Sel = 0
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0) 
				FROM
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID

				IF @onHand < @numUnits
				BEGIN  
					IF @Sel=0 SET @Sel=1
				END
			END

			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END      


	IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
	SELECT @Sel
END
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
    (
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numCartId NUMERIC(18,0) = 0,
      @bitDeleteAll BIT=0,
	  @numSiteID NUMERIC(18,0) = 0
    )
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numCartId <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numCartId = @numCartId AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	

			DECLARE @numItemCode NUMERIC(18,0)
			SELECT @numItemCode=numItemCode FROM CartItems WHERE numCartId=@numCartId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numCartId = @numCartId 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numItemCode AND numDomainId = @numDomainId ))	--AND bitrequired = 1

			EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,0,@numSiteID
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END
END

 



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExtranetAccountsDtl_ResetPassword')
DROP PROCEDURE USP_ExtranetAccountsDtl_ResetPassword
GO
CREATE PROCEDURE [dbo].[USP_ExtranetAccountsDtl_ResetPassword]
	@numDomainID AS NUMERIC(18,0),
    @vcResetID AS VARCHAR(500),
    @vcNewPassword AS VARCHAR(100)
AS 
BEGIN
	IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID=@vcResetID AND DATEDIFF(MINUTE, vcResetLinkCreatedTime, GETUTCDATE()) <= 10)
	BEGIN
		UPDATE  
			ExtranetAccountsDtl
        SET
			vcPassword = @vcNewPassword
        WHERE 
			vcResetLinkID = @vcResetID

		UPDATE
			ExtranetAccountsDtl
		SET
			vcResetLinkID = ''
		WHERE 
			vcResetLinkID = @vcResetID
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_RESET_LINK',16,1)
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExtranetAccountsDtl_ValidateResetLink')
DROP PROCEDURE USP_ExtranetAccountsDtl_ValidateResetLink
GO
CREATE PROCEDURE [dbo].[USP_ExtranetAccountsDtl_ValidateResetLink]
@numDomainID NUMERIC(18,0),
@vcResetID VARCHAR(500)
as
BEGIN
	DECLARE @bitValidResetID BIT = 0

	IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID=@vcResetID AND DATEDIFF(MINUTE, vcResetLinkCreatedTime, GETUTCDATE()) <= 10)
	BEGIN
		SET @bitValidResetID = 1
	END

	SELECT @bitValidResetID
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value], vcAbbreviations FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]'
												 											     
								WHEN @strDBColumnName = 'numShippingService'
								THEN @strSQL + 'SELECT 
													ShippingService.numShippingServiceID AS [Key]
													,ShippingService.vcShipmentService AS [Value]
													,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'''') vcAbbreviations
												FROM 
													ShippingService 
												LEFT JOIN
													ShippingServiceAbbreviations
												ON
													ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
												WHERE 
													ShippingService.numDomainID=' + CONVERT(VARCHAR,@numDomainID) + ' OR ISNULL(ShippingService.numDomainID,0) = 0'					     																		    
								WHEN @strDBColumnName = 'numChartAcntId'
								THEN @strSQL + 'SELECT 
													numAccountId AS [Key]
													,vcAccountName AS [Value] 
												FROM 
													Chart_Of_Accounts 
												WHERE 
													numDomainId=' + CONVERT(VARCHAR,@numDomainID)
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0
    )
AS 
BEGIN
		
    IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
    BEGIN

        SELECT 20 AS [intImportMasterID],'Item' AS [vcImportName]
        UNION ALL
        SELECT  48 AS [intImportMasterID],'Item WareHouse' AS [vcImportName]
        UNION ALL
        SELECT  31 AS [intImportMasterID],'Kit sub-items' AS [vcImportName]
        UNION ALL
        SELECT  54 AS [intImportMasterID],'Item Images' AS [vcImportName]
        UNION ALL
        SELECT  55 AS [intImportMasterID],'Item Vendor' AS [vcImportName]
        UNION ALL
        SELECT  133 AS [intImportMasterID],'Organization' AS [vcImportName]
        UNION ALL
		SELECT  134 AS [intImportMasterID],'Contact' AS [vcImportName]
        UNION ALL
        SELECT  43 AS [intImportMasterID],'Organization Correspondence' AS [vcImportName]                        
        UNION ALL
        SELECT  98 AS [intImportMasterID],'Address Details' AS [vcImportName]
		UNION ALL
        SELECT  136 AS [intImportMasterID],'Import Sales Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  137 AS [intImportMasterID],'Import Purchase Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  140 AS [intImportMasterID],'Import Accounting Entries' AS [vcImportName]           		
    END
	
        IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
            BEGIN
	
		
                SELECT DISTINCT
                        intSectionID AS [Id],
                        vcSectionName AS [Data]
                FROM    dbo.DycFormSectionDetail
                WHERE   numFormID = @numFormID			
		
		
		/*SELECT DISTINCT  DMAP.intSectionID AS [Id], 
						 DFSD.vcSectionName AS [Data]					 
		FROM dbo.DycFormField_Mapping DMAP 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.intSectionID = DMAP.intSectionID
											   AND DFSD.numFormID = DMAP.numFormID
		WHERE DMAP.numFormID = @numFormID */
		
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    DCOL.tintOrder
                          FROM      dbo.View_DynamicDefaultColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(DCOL.bitDeleted, 0) = 0
                          UNION ALL
                          SELECT    -1,
                                    CAST(CFm.Fld_id AS NUMERIC(18,0)),
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    100 AS [tintOrder]
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
                        ) X
                ORDER BY X.tintOrder,
                        X.intSectionID        		
        
        
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    ISNULL(DCOL.tintColumn, 0) AS SortOrder,
                                    ISNULL(DCOL.tintRow,0) AS tintRow
                          FROM      dbo.View_DynamicColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(tintPageType, 0) = 4
                          UNION ALL
                          SELECT    -1,
                                    CAST(CFm.Fld_id AS NUMERIC(18,0)),
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    DFCD.intColumnNum AS [SortOrder],
                                    ISNULL(DFCD.intRowNum,0) AS tintRow
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                                    JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id
                                                                             AND DFCD.numFormID = @numFormID
                                                                             AND DFCD.bitCustom = 1
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND DFCD.numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
                                    AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)                          
                        ) X
                ORDER BY X.SortOrder
                        ,X.intSectionID 
            END	
END
GO


/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
	@numUserCntId numeric(18,0),
	@numDomainId numeric(18,0),
	@vcCookieId varchar(100),
	@bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   ,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,I.txtItemDesc AS txtItemDesc,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection,
			   (CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
				
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection
			   ,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId	
				
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #View_Journal SELECT numAccountId,AccountTypeCode,AccountCode, datEntry_Date,Debit,Credit FROM view_journal_master WHERE numDomainId = @numDomainID AND (numClassIDDetail=@numAccountClass OR @numAccountClass=0);

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
	AS
	(
		SELECT 
			ParentId,
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(CONCAT(d.Struc ,'#',[COA].[numAccountId]) AS VARCHAR(300)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc 
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
	UNION ALL
	SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL,'-2'
	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc 
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'


	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-1
	WHERE 
		[vcAccountCode] NOT IN ('010302','010402')
		AND [LEVEL] = 1

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.numAccountId = COA.numAccountId
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			DATENAME(mm, @dtFromDate) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN		
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = ' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%''
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2
				UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
/****** Object:  StoredProcedure [dbo].[USP_GetImportConfiguration]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimportconfiguration')
DROP PROCEDURE usp_getimportconfiguration
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE  PROCEDURE [dbo].[USP_GetImportConfiguration]
@numRelation as numeric,              
@numDomain as numeric,
@ImportType as tinyint             
as                
--    
--if  (select count(*) from RecImportConfg where numDomainid=@numDomain and numRelationShip=@numRelation)=0    
--set @numDomain = 0    
IF @ImportType=1
BEGIN   

--Available fields               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where numFormID=9               
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and numRelationShip=@numRelation and Importtype=1)              
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/L' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id IN (1,12,13,14) /*Leads/Prospects/Accounts*/ and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='L' and numRelationShip=@numRelation  and Importtype=1)          
          
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/C' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=4 /*Contact Details*/and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain     
and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='C' and numRelationShip=@numRelation and Importtype=1)          
) order by vcFormFieldName       
              
              
             
                
--Added fields          
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where DTL.numDomainId=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 0 and Importtype=1          
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 1   and Importtype=1        
order by intcolumn  


end
else  if @ImportType=2 OR @ImportType=4
begin   
               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where bitDeleted=0 AND numFormID in( 20,27) --,26,27,28
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and Importtype=@ImportType)
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/I' as numFormFieldId  from CFW_Fld_Master                                                    
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=5  and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='I'   and Importtype=@ImportType)          
) order by vcFormFieldName       
              
             
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where bitDeleted=0 AND DTL.numDomainId=@numDomain  and bitCustomFld = 0 and Importtype=@ImportType         
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and bitCustomFld = 1   and Importtype=@ImportType      
order by intcolumn  


end
else  if @ImportType=3
begin   
(select vcFormFieldName,convert(varchar(10),numFormFieldId)  as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=25)              
end

else  if @ImportType=5 --Import Assembly Items
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=31
 order by numFormFieldId       

end
else  if @ImportType=6 --Import Tax Details
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=45
 order by numFormFieldId       

end
else  if @ImportType=7 --Import Update Item Warehouse Information/Matrix Item
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,[order],convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName,numListID from DynamicFormFieldMaster where numFormID=48
UNION
 select Fld_label as vcFormFieldName,Fld_id as numFormFieldId,100 as [order],convert(bit,0) as bitCustomFld,'C' as cCtype,fld_type as vcAssociatedControlType,'' as vcDbColumnName,CFW_Fld_Master.numListID
 from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numItemGroupID=@numRelation and tintType=2 
order by [order] 

end
ELSE IF @ImportType IN (136,137,140)
BEGIN
	SELECT
		numFieldID
		,vcFieldName
	FROM 
		DycFormField_Mapping 
	WHERE 
		numFormID=@ImportType               
		AND numFieldID NOT IN (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainid=@numDomain AND numFormId=@ImportType)
                         
	--Added fields          
	SELECT
		numFieldID
		,vcFieldName
	FROM 
		View_DynamicColumns                   
	WHERE 
		numDomainID=@numDomain 
		AND numFormId=@ImportType
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** Email ***************************/
		SET @numTabID = 44
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '',
		   0,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
			'',
			0,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	UPDATE 
		t1
	SET 
		t1.bitVisible = (SELECT bitVisible FROM @TEMP t2 WHERE t2.ID = t1.numParentID)
	FROM
		@TEMP t1
	WHERE
		ISNULL(t1.numParentID,0) > 0

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementMaster]    Script Date: 08/08/2009 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPageElementMaster')
DROP PROCEDURE USP_GetPageElementMaster
GO
CREATE PROCEDURE [dbo].[USP_GetPageElementMaster]
	@numSiteID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		[numElementID],
		[vcElementName],
		[vcUserControlPath],
		vcTagName,
		ISNULL(bitCustomization,0) bitCustomization
		,ISNULL(bitAdd,0) AS bitAdd
		,ISNULL(bitDelete,0) AS bitDelete
	FROM 
		[PageElementMaster]	
	WHERE
		(ISNULL(numSiteID,0) = 0 OR ISNULL(numSiteID,0) = @numSiteID)
	ORDER BY
		vcElementName
END

--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,OI.numWarehouseItmsID
										,WI.numWarehouseID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcItemName'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable ORDER BY [row]; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItem')
DROP PROCEDURE USP_GetSimilarItem
GO
CREATE PROCEDURE USP_GetSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@byteMode TINYINT,
@vcCookieId VARCHAR(MAX)='',
@numUserCntID NUMERIC(18)=0,
@numSiteID NUMERIC(18,0)=0,
@numOppID NUMERIC(18,0) = 0,
@numWarehouseID NUMERIc(18,0) = 0
AS 
BEGIN

If @byteMode=1
BEGIN
 select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode 
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=3
BEGIN
SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
                       WareHouseItems W ON I.numItemCode = W.numItemID
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=2
BEGIN
	SELECT DISTINCT I.vcItemName AS vcItemName,I.txtItemDesc AS txtItemDesc,I.numSaleUnit,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip 
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as fltUOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	--WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID 
	WHERE 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0)=0) 
		) AS W
	WHERE 
		SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END 

ELSE IF @byteMode=4 -- Pre up sell
BEGIN
SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc],
(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode 
                       OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0)
	) AS W
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1
 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)
END 

ELSE IF @byteMode=5 -- Post up sell
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	DECLARE @numPromotionID NUMERIC(18,0) = 0
	DECLARE @vcPromotionDescription VARCHAR(MAX) = ''
	DECLARE @intPostSellDiscount INT=0
	DECLARE @vcPromotionName VARCHAR(100) = ''
	
	SELECT TOP 1 
		@intPostSellDiscount=ISNULL(P.intPostSellDiscount,0)
		,@numPromotionID=C.numPromotionID
		,@vcPromotionDescription= CONCAT('Post sell Discount: ', OM.vcPOppName)
		,@vcPromotionName = P.vcProName 
	FROM 
		OpportunityItems AS C 
	INNER JOIN
		OpportunityMaster OM
	ON
		C.numOppId=OM.numOppId
	LEFT JOIN 
		PromotionOffer AS P 
	ON 
		P.numProId=C.numPromotionID 
	WHERE 
		OM.numDomainId=@numDomainID
		AND OM.numOppID = @numOppID
		AND ISNULL(OM.bitPostSellDiscountUsed,0) = 0
		AND ISNULL(C.numPromotionID,0) > 0
		AND ISNULL(P.bitDisplayPostUpSell,0)=1
		AND ISNULL(P.intPostSellDiscount,0) BETWEEN 1 AND 100

	SELECT DISTINCT 
		I.numItemCode
		,I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,I.vcModelID
		,Category.vcCategoryName
		,1 AS numUnitHour
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN [monWListPrice] ELSE monListPrice END,0) monListPrice
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN (monWListPrice-((monWListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) ELSE (monListPrice-((monListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) END,0) monOfferListPrice
		,ISNULL(W.numWarehouseItemID,0) AS numWarehouseItemID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END) AS fltDiscount
		,0 AS bitDiscountType
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @numPromotionID ELSE 0 END) AS numPromotionID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionDescription ELSE '' END) AS vcPromotionDescription
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionName ELSE '' END) AS vcPromotionName
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0)
		,I.numItemCode
		,I.numDomainId
		,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor
		,I.vcSKU
		,I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc]
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		Category 
	INNER JOIN 
		ItemCategory 
	ON 
		Category.numCategoryID = ItemCategory.numCategoryID 
	INNER JOIN
        Item I 
	ON 
		ItemCategory.numItemID = I.numItemCode
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		OI.numOppId=OM.numOppId	
	INNER JOIN
        SimilarItems SI 
	ON 
		SI.numParentItemCode=OI.numItemCode
		AND SI.bitPostUpSell = 1
	OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND numWareHouseID = @numWarehouseID
	) AS W
	WHERE 
		SI.numDomainId = @numDomainID 
		AND OM.numDomainID=@numDomainID 
		AND OM.numOppId=@numOppID
		AND 1 = (CASE WHEN charItemType='P' THEN CASE WHEN ISNULL(W.numWareHouseItemID,0) > 0 THEN 1 ELSE 0 END ELSE 1 END)
		
		

	UPDATE OpportunityMaster SET bitPostSellDiscountUsed=1 WHERE numDomainId=@numDomainID AND numOppID = @numOppID
END 

ELSE IF @byteMode=6 -- Pre up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID
	

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)

END 
ELSE IF @byteMode=7 -- Post up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers, PO.vcProName
	,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionRule, PO.numProId, SI.numParentItemCode 
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	LEFT JOIN PromotionOfferItems AS POI ON POI.numValue = I.numItemCode
	LEFT JOIN PromotionOffer PO on PO.numProId = POI.numProId 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPostUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)

END 
ELSE IF @byteMode=8 -- Pre up sell for Internal Orders
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	SELECT DISTINCT 
		I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,SI.*
		,ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID
		,ISNULL(vcRelationship,'') vcRelationship
		,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
		,dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
		,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
		,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
		FROM PromotionOffer AS P 
		LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
		WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
		(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
		I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
		AND P.numDomainId=@numDomainID 
				AND ISNULL(bitEnabled,0)=1 
				AND ISNULL(bitAppliesToSite,0)=1 
				AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
				AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
				AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		) AS PromotionOffers
		,SI.numParentItemCode
		,(
			CASE WHEN ISNULL(bitKitParent,0) = 1
			THEN
				(CASE 
					WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
					THEN 1 
					ELSE 0 
				END)
			ELSE
				0
			END
		) bitHasChildKits
		,Category.vcCategoryName
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0)=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)
END 
END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetSitePages]    Script Date: 08/08/2009 16:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitePages')
DROP PROCEDURE USP_GetSitePages
GO
CREATE PROCEDURE [dbo].[USP_GetSitePages]
          @numPageID   NUMERIC(9),
          @numSiteID   NUMERIC(9),
          @numDomainID NUMERIC(9)
AS
  SELECT numPageID,
		 numSiteID,
         ISNULL(vcPageName,'') vcPageName,
         ISNULL(vcPageURL,'') vcPageURL,
         ISNULL(tintPageType,2) tintPageType,
         ISNULL(vcPageTitle,'') vcPageTitle,
         [numTemplateID],
         (SELECT vcTemplateName FROM [SiteTemplates] st WHERE st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
         --ISNULL(MT.vcMetaTag,'') vcMetaTag,
         ISNULL([vcMetaDescription],'') AS vcMetaDescription,
         ISNULL([vcMetaKeywords],'') AS vcMetaKeywords,
         ISNULL(MT.numMetaID,0) numMetaID,
         ISNULL((SELECT vcHostName FROM Sites WHERE [numSiteID] = @numSiteID),'') vcHostName,
		 CASE WHEN [bitIsActive] = 1 THEN 'Inactivate'
		      ELSE 'Activate'
		 END AS 'Status',
		 [bitIsActive],
		 ISNULL([bitIsMaintainScroll],0) AS bitIsMaintainScroll
  FROM   SitePages SP
         LEFT OUTER JOIN MetaTags MT
           ON MT.numReferenceID = SP.numPageID
  WHERE  (numPageID = @numPageID
           OR @numPageID = 0)
         AND numSiteID = @numSiteID
         AND numDomainID = @numDomainID
	ORDER BY
		vcPageName

  IF @numPageID > 0
    BEGIN
      --Select StyleSheets for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 0
      --Select Javascripts for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 1
    END
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID NUMERIC(18,0),
@ClientTimeZoneOffset  INT,
@numWOStatus NUMERIC(18,0),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000),
@tintFilter TINYINT = 0,
@numOppID NUMERIC(18,0),
@numOppItemID NUMERIC(18,0)                                                                
AS                            
BEGIN
	SET @vcWOId=ISNULL(@vcWOId,'')
	
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID   
    
	DECLARE @TEMP TABLE
	(
		numID INT IDENTITY(1,1),ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
		numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
		bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
		numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
	)

	DECLARE @TEMPWO TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @TEMPWORKORDER TABLE
	(
		ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
		bitWorkOrder BIT, bitReadyToBuild BIT
	) 

	INSERT INTO 
		@TEMPWORKORDER
	SELECT 
		1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS FLOAT),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		1 AS bitWorkOrder,
		(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS bitReadyToBuild
	FROM 
		WorkOrder
	WHERE  
		numDomainID=@numDomainID
		AND (numOppID=@numOppID OR ISNULL(@numOppID,0)=0)
		AND (numOppItemID=@numOppItemID OR ISNULL(@numOppItemID,0)=0)
		AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
		AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
		AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
		AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
	ORDER BY
		bintCreatedDate DESC,
		numWOId ASC

	;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
						bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
	(
		SELECT
			ItemLevel,
			numParentWOID,
			numWOId,
			ID,
			numItemCode,
			numQty,
			numWareHouseItemId,
			vcInstruction,
			numAssignedTo,
			bintCreatedDate,
			numCreatedBy,
			bintCompliationDate,
			numWOStatus,
			numOppId,
			1 AS bitWorkOrder,
			bitReadyToBuild
		FROM
			@TEMPWORKORDER t
		UNION ALL
		SELECT 
			c.ItemLevel+2,
			c.numWOID,
			WorkOrder.numWOId,
			CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
			WorkOrder.numItemCode,
			CAST(WorkOrder.numQtyItemsReq AS FLOAT),
			WorkOrder.numWareHouseItemId,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
			WorkOrder.numAssignedTo,
			WorkOrder.bintCreatedDate,
			WorkOrder.numCreatedBy,
			WorkOrder.bintCompliationDate,
			WorkOrder.numWOStatus,
			WorkOrder.numOppId,
			1 AS bitWorkOrder,
			CASt((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT) AS bitReadyToBuild
		FROM 
			WorkOrder
		INNER JOIN 
			CTEWorkOrder c 
		ON 
			WorkOrder.numParentWOID = c.numWOID
	)

	INSERT 
		@TEMP
	SELECT
		ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		OpportunityMaster.vcPOppName,
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
	FROM 
		CTEWorkOrder
	INNER JOIN 
		Item
	ON
		CTEWorkOrder.numItemKitID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN 
		OpportunityMaster
	ON
		CTEWorkOrder.numOppID = OpportunityMaster.numOppId

	INSERT INTO 
		@TEMP
	SELECT
		t1.ItemLevel + 1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		OpportunityMaster.vcPOppName,
		0 AS bitWorkOrder,
		CASE 
			WHEN Item.charItemType='P' AND ISNULL(t1.bitReadyToBuild,0) = 0
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(t1.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		WorkOrderDetails
	INNER JOIN
		@TEMP t1
	ON
		WorkOrderDetails.numWOId = t1.numWOID
	INNER JOIN 
		Item
	ON
		WorkOrderDetails.numChildItemID = Item.numItemCode
		AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster 
	ON
		WorkOrderDetails.numPOID = OpportunityMaster.numOppId

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempWOID NUMERIC(18,0)
	INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

	SELECT @iCount=COUNT(*) FROM @TEMPWO

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

		UPDATE
			T1
		SET
			bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
		FROM
			@TEMP T1
		WHERE
			numWOID=@numTempWOID

		SET @i = @i + 1
	END

	If @tintFilter = 2 --Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 0 ELSE 1 END) AND bitWorkOrder=1
	END
	ELSE IF @tintFilter = 3 --Un-Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 1 ELSE 0 END) AND bitWorkOrder=1
	END

	DECLARE @TEMPPagingID TABLE
	(
		ID INT
		,numWOID NUMERIC(18,0)
		,numParentWOID NUMERIC(18,0)
	)
	INSERT INTO @TEMPPagingID
	(
		ID
		,numWOID
		,numParentWOID
	)
	SELECT
		numID
		,numWOID
		,numParentWOID
	FROM	
		@TEMP 
	WHERE 
		ISNULL(numParentWOID,0) = 0
	ORDER BY
		numWOID
	OFFSET
		(@CurrentPage - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

	;WITH CTEID (numID,numWOID,numParentWOID) AS
	(
		SELECT
			ID
			,numWOID
			,numParentWOID
		FROM	
			@TEMPPagingID 
		UNION ALL
		SELECT
			T.numID
			,T.numWOID
			,T.numParentWOID
		FROM
			@TEMP T
		INNER JOIN
			CTEID C
		ON
			T.numParentWOID = C.numWOID
		WHERE
			ISNULL(T.numParentWOID,0) > 0
	)

	SELECT * FROM @TEMP T1 WHERE T1.numID IN (SELECT numID FROM CTEID) ORDER BY numParentWOID,ItemLevel

	DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

	SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMP WHERE ISNULL(numParentWOID,0)=0

	SELECT @TOTALROWCOUNT AS TotalRowCount 
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numCartId AS NUMERIC OUTPUT,
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice DECIMAL(20,5),
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount DECIMAL(20,5),
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 5),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount DECIMAL(20,5),
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numPromotionID NUMERIC(18,0) = 0,
	@vcPromotionDescription VARCHAR(MAX) = '',
	@numParentItemCode NUMERIC(18,0) = 0,
	@vcChildKitItemSelection VARCHAR(MAX) = ''
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	
	INSERT INTO [dbo].[CartItems]
    (
        [numUserCntId],
        [numDomainId],
        [vcCookieId],
        [numOppItemCode],
        [numItemCode],
        [numUnitHour],
        [monPrice],
        [numSourceId],
        [vcItemDesc],
        [numWarehouseId],
        [vcItemName],
        [vcWarehouse],
        [numWarehouseItmsID],
        [vcItemType],
        [vcAttributes],
        [vcAttrValues],
        [bitFreeShipping],
        [numWeight],
        [tintOpFlag],
        [bitDiscountType],
        [fltDiscount],
        [monTotAmtBefDiscount],
        [ItemURL],
        [numUOM],
        [vcUOMName],
        [decUOMConversionFactor],
        [numHeight],
        [numLength],
        [numWidth],
        [vcShippingMethod],
        [numServiceTypeId],
        [decShippingCharge],
        [numShippingCompany],
        [tintServicetype],
        [monTotAmount],
		[PromotionID],
		[PromotionDesc],
		numParentItem
		,vcChildKitItemSelection
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numPromotionID,
		@vcPromotionDescription,
		@numParentItemCode,
		@vcChildKitItemSelection
    )

	SET @numCartId = SCOPE_IDENTITY()

	DECLARE @numItemClassification As NUMERIC(18,0)

	SELECT 
		@numItemClassification=numItemClassification
	FROM
		Item
	WHERE
		numItemCode=@numItemCode


	IF(@postselldiscount=0)
	BEGIN

		/************************** First check if any promotion can be applied *********************************/
		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1),
			numPromotionID NUMERIC(18,0),
			vcPromotionDescription VARCHAR(MAX)
		)

		INSERT INTO 
			@TEMPPromotion
		SELECT
			CI.PromotionID,
			CI.PromotionDesc
		FROM 
			CartItems CI
		INNER JOIN
			PromotionOffer PO
		ON
			CI.PromotionID = PO.numProId
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.bitParentPromotion,0)=1
			AND ISNULL(CI.PromotionID,0) > 0
			AND ISNULL(bitEnabled,0)=1
			AND 1=(CASE PO.tintDiscoutBaseOn
					 WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
					 WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
					 WHEN 3 THEN (CASE PO.tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
					ELSE 0
				END)
		ORDER BY
			numCartId ASC

		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType INT
		DECLARE @tintDiscoutBaseOn INT
		DECLARE @IsPromotionApplied BIT = 0

		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMPPromotion

		IF @Count > 0
		BEGIN
			DECLARE @i AS INT = 1
		
			WHILE @i <= @Count
			BEGIN
				SELECT @numPromotionID=numPromotionID,@vcPromotionDescription=vcPromotionDescription FROM @TEMPPromotion WHERE ID=@i

				SELECT
					@tintDiscountType=tintDiscountType,
					@fltDiscountValue=fltDiscountValue
				FROM
					PromotionOffer 
				WHERE
					numProId=@numPromotionID
			
				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numCartId

					SET @IsPromotionApplied = 1
					BREAK
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountAmount AS FLOAT
				
					SELECT
						@usedDiscountAmount = SUM(fltDiscount)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= monTotAmount THEN 0 ELSE (monTotAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= monTotAmount THEN monTotAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountQty AS FLOAT

					SELECT
						@usedDiscountQty = SUM(fltDiscount/monPrice)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (monTotAmount-((@fltDiscountValue - @usedDiscountQty)*monPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (numUnitHour*monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) *monPrice) END) 
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END

				END

				SET @i = @i + 1
			END
		END


		IF @IsPromotionApplied = 0
		BEGIN
			/************************** Check if any promotion can be trigerred *********************************/
			EXEC USP_ECommerceTiggerPromotion @numDomainID,@numUserCntId,@vcCookieId,@numCartID,@numItemCode,@numItemClassification,@numUnitHour,@monPrice,@numSiteID
		
		END

	END


	SELECT @numCartId

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory,                   
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON; 
	DECLARE @numMaxOrder INT
	SET @numMaxOrder = ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemCode),0)
	 

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,tintOrder INT
	)

	INSERT INTO  @TEMP
	(	
		numItemCode
		,tintOrder
	)
	SELECT
		Item.numItemCode
		,1
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
	WHERE
		ItemDetails.numItemKitID = @numItemCode
		AND (SELECT COUNT(*) FROM ChildKitsRelation CKR WHERE CKR.numChildKitItemCode=Item.numItemCode) = 0
	ORDER BY
		ISNULL(NULLIF(sintOrder,0),@numMaxOrder + 1),numItemDetailID


	DECLARE @TEMPDependedKits TABLE
	(
		numParentKitItemCode NUMERIC(18,0)
		,numChildKitItemCode NUMERIC(18,0)
	)

	INSERT INTO @TEMPDependedKits
	(
		numParentKitItemCode
		,numChildKitItemCode
	)
	SELECT
		numParentKitItemCode
		,numChildKitItemCode
	FROM
		ChildKitsRelation
	WHERE
		ChildKitsRelation.numItemCode=@numItemCode 
	GROUP BY
		numParentKitItemCode
		,numChildKitItemCode


	;WITH CTE (ID,numItemCode,tintOrder) AS
	(
		SELECT
			T1.ID
			,T1.numItemCode
			,T1.tintOrder
		FROM
			@TEMP T1
		UNION ALL
		SELECT 
			C1.ID
			,T2.numChildKitItemCode
			,C1.tintOrder + 1
		FROM
			@TEMPDependedKits T2
		INNER JOIN	
			ItemDetails
		ON
			T2.numChildKitItemCode = ItemDetails.numChildItemID
			AND ItemDetails.numItemKitID = @numItemCode
		INNER JOIN
			CTE C1
		ON
			T2.numParentKitItemCode=C1.numItemCode
	)
  
	SELECT
		Item.numItemCode,
		Item.vcItemName,
		ISNULL(Item.bitKitSingleSelect,0) AS bitKitSingleSelect,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numChildKitItemCode)
				FROM 
					ChildKitsRelation 
				WHERE 
					numItemCode=@numItemCode 
					AND numParentKitItemCode=Item.numItemCode
				GROUP BY
					numChildKitItemCode
				FOR XML PATH('')),1,1,''),'') vcDependedKits,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numParentKitItemCode)
				FROM 
					ChildKitsRelation 
				WHERE 
					numItemCode=@numItemCode 
					AND numChildKitItemCode=Item.numItemCode
				GROUP BY
					numParentKitItemCode
				FOR XML PATH('')),1,1,''),'') vcParentKits
	FROM
		CTE C1
	INNER JOIN
		Item
	ON
		C1.numItemCode = Item.numItemCode
	ORDER BY
		C1.ID,C1.tintOrder
END  
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_GetDependedChildKitItems')
DROP PROCEDURE USP_ItemDetails_GetDependedChildKitItems
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_GetDependedChildKitItems]  
	@numDomainID NUMERIC(18,0)
	,@numMainKitItemCode NUMERIC(18,0)
	,@numChildKitItemCode NUMERIC(18,0)
	,@numChildKitSelectedItem VARCHAR(MAX)
AS  
BEGIN  
	SELECT DISTINCT
		numChildKitItemCode
		,ISNULL(STUFF((SELECT 
					CONCAT(',',numChildKitItemCode)
				FROM 
					ChildKitsRelation 
				WHERE 
					numItemCode=@numMainKitItemCode 
					AND numParentKitItemCode=numChildKitItemCode
				GROUP BY
					numChildKitItemCode
				FOR XML PATH('')),1,1,''),'') vcDependedKits
	FROM 
		ChildKitsRelation 
	WHERE 
		numItemCode=@numMainKitItemCode 
		AND numParentKitItemCode=@numChildKitItemCode
	ORDER BY
		numChildKitItemCode


	SELECT 
		ChildKitsRelation.numChildKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		CONCAT(Item.vcItemName,' (Item Code:',Item.numItemCode,', SKU:',Item.vcSKU,')') AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty
	FROM 
		ChildKitsRelation
	INNER JOIN
		ItemDetails
	ON
		ChildKitsRelation.numChildKitItemCode = ItemDetails.numItemKitID
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND Item.numItemCode = ChildKitsRelation.numChildKitChildItemCode
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	WHERE
		ChildKitsRelation.numItemCode=@numMainKitItemCode 
		AND ChildKitsRelation.numParentKitItemCode=@numChildKitItemCode
		AND ChildKitsRelation.numParentKitChildItemCode IN (SELECT Id FROM dbo.SplitIDs(@numChildKitSelectedItem,','))
	ORDER BY 
		sintOrder
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetKitChildItems' ) 
    DROP PROCEDURE USP_ItemDetails_GetKitChildItems
GO
CREATE PROCEDURE USP_ItemDetails_GetKitChildItems  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  

	SELECT 
		@numItemCode AS numKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		CONCAT(Item.vcItemName,' (Item Code:',Item.numItemCode,', SKU:',Item.vcSKU,')') AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty
	FROM 
		ItemDetails
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
	WHERE
		ItemDetails.numItemKitID = @numItemCode 
	ORDER BY 
		sintOrder
END  
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_ValidateKitSelection')
DROP PROCEDURE USP_ItemDetails_ValidateKitSelection
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_ValidateKitSelection]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcKitChildItems VARCHAR(MAX)
AS  
BEGIN  
	DECLARE @bitValidKitSelection BIT = 1

	--TODO: Prepare validation logic

	SELECT @bitValidKitSelection
END
GO
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   
	   
	DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	
	IF(@vcCookieId!=null OR @vcCookieId!='')   
	BEGIN                    
		SET @PromotionOffers=(SELECT TOP 1 
								CASE 
									WHEN P.tintDiscountType=1  
									THEN (I.monListPrice*fltDiscountValue)/100
									WHEN P.tintDiscountType=2
									THEN fltDiscountValue
									WHEN P.tintDiscountType=3
									THEN fltDiscountValue*I.monListPrice
									ELSE 0 
								END
							FROM CartItems AS C
							LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
							LEFT JOIN Item I ON I.numItemCode=C.numItemCode
							WHERE 
								I.numItemCode=@numItemCode 
								AND I.numItemCode=@numDomainID 
								AND C.vcCookieId=@vcCookieId 
								AND C.numUserCntId=@numUserCntID 
								AND C.fltDiscount=0 
								AND (I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) 
									OR I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
								AND P.numDomainId=@numDomainID 
								AND ISNULL(bitEnabled,0)=1 
								AND ISNULL(bitAppliesToSite,0)=1 
								AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
	END
	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql=' With tblItem AS (SELECT 
										I.numItemCode
										,vcItemName
										,txtItemDesc
										,charItemType
										,ISNULL(I.bitKitParent,0) bitKitParent
										,ISNULL(I.bitMatrix,0) bitMatrix,' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0)' END)
										+' AS monListPrice
										,ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
										END,0) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

										from Item I ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										+ '                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
																							  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID='+ convert(varchar(15),@numDomainID) + ' AND I.numItemCode='+ convert(varchar(15),@numItemCode) +
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										+ '           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT @strSql
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(numCompanyType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		--KEEP THIS BELOW ABOVE IF CONDITION
		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
			,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
			,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
		DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
		SET @searchPositionColumnGroupBy = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
					SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )
				END

			END

		       SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
												( 
													 SELECT
														I.numDomainID
														,I.numItemCode
														,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
														,I.bitMatrix
														,I.bitKitParent
														,I.numItemGroup
														,vcItemName
														,I.bintCreatedDate
														,vcManufacturer
														,ISNULL(txtItemDesc,'''') txtItemDesc
														,vcSKU
														,fltHeight
														,fltWidth
														,fltLength
														,vcModelID
														,fltWeight
														,numBaseUnit
														,numSaleUnit
														,numPurchaseUnit
														,bitFreeShipping
														,C.vcCategoryName
														,C.vcDescription as CategoryDesc
														,charItemType
														,monListPrice
														,bitAllowBackOrder
														,bitShowInStock
														,numVendorID
														,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
														,SUM(numOnHand) numOnHand
														,SUM(numAllocation) numAllocation
													 FROM      
														Item AS I
													 INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
													 LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
													 INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
													 INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
													 INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
													  ',@Join,' ',@DefaultSortJoin)

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterItemAttributes
			END

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			SET @strSQL = @strSQL + @Where
	
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         

            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = CONCAT(@strSQL,' GROUP BY
												I.numDomainID
												,I.numItemCode
												,I.bitMatrix
												,I.bitKitParent
												,I.numItemGroup
												,vcItemName
												,I.bintCreatedDate
												,vcManufacturer
												,txtItemDesc
												,vcSKU
												,fltHeight
												,fltWidth
												,fltLength
												,vcModelID
												,fltWeight
												,numBaseUnit
												,numSaleUnit
												,numPurchaseUnit
												,bitFreeShipping
												,C.vcCategoryName
												,C.vcDescription
												,charItemType
												,monListPrice
												,bitAllowBackOrder
												,bitShowInStock
												,numVendorID
												,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
			IF @numDomainID = 204
			BEGIN
				SET @strSQL = @strSQL + 'DELETE FROM 
											#TEMPItems
										WHERE 
											numItemCode IN (
																SELECT 
																	F.numItemCode
																FROM 
																	#TEMPItems AS F
																WHERE 
																	ISNULL(F.bitMatrix,0) = 1 
																	AND EXISTS (
																				SELECT 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																				FROM 
																					#TEMPItems t1
																				WHERE 
																					t1.vcItemName = F.vcItemName
																					AND t1.bitMatrix = 1
																					AND t1.numItemGroup = F.numItemGroup
																				GROUP BY 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																				HAVING 
																					Count(t1.numItemCode) > 1
																			)
															)
											AND numItemCode NOT IN (
																					SELECT 
																						Min(numItemCode)
																					FROM 
																						#TEMPItems AS F
																					WHERE
																						ISNULL(F.bitMatrix,0) = 1 
																						AND Exists (
																									SELECT 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																									FROM 
																										#TEMPItems t2
																									WHERE 
																										t2.vcItemName = F.vcItemName
																									   AND t2.bitMatrix = 1
																									   AND t2.numItemGroup = F.numItemGroup
																									GROUP BY 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																									HAVING 
																										Count(t2.numItemCode) > 1
																								)
																					GROUP BY 
																						vcItemName, bitMatrix, numItemGroup
																				);'

			END
									  
									  
			SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  

			SET @strSQL = CONCAT(@strSQL,'SELECT  
										Rownumber
										,I.numItemCode
										,vcCategoryName
										,CategoryDesc
										,vcItemName
										,txtItemDesc
										,vcManufacturer
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,bitFreeShipping
										,', 
											(CASE 
											WHEN @tintPreLoginPriceLevel > 0 
											THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
											ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
											END)
											,' AS monListPrice,
											ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
											',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
													WHEN tintRuleType = 1 AND tintDiscountType = 1
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 1 AND tintDiscountType = 2
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
													WHEN tintRuleType = 2 AND tintDiscountType = 1
													THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 2 AND tintDiscountType = 2
													THEN ISNULL(Vendor.monCost,0) + decDiscount
													WHEN tintRuleType = 3
													THEN decDiscount
												END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
												WHEN tintRuleType = 1 AND tintDiscountType = 1 
												THEN decDiscount 
												WHEN tintRuleType = 1 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
												WHEN tintRuleType = 2 AND tintDiscountType = 1
												THEN decDiscount
												WHEN tintRuleType = 2 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
												WHEN tintRuleType = 3
												THEN 0
											END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
										,vcPathForImage
										,vcpathForTImage
										,UOM AS UOMConversionFactor
										,UOMPurchase AS UOMPurchaseConversionFactor
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
													THEN (CASE 
															WHEN bitAllowBackOrder = 1 THEN 1
															WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
															ELSE 1
														END)
													ELSE 1
										END) AS bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													THEN ( CASE WHEN bitShowInStock = 1
																THEN (CASE 
																		WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																		WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																		WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																		ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																	END)
																ELSE ''''
															END )
													ELSE ''''
												END ) AS InStock
										, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
										,(SELECT 
												COUNT(numProId) 
											FROM 
												PromotionOffer PO
											WHERE 
												numDomainId=',CAST(@numDomainID AS VARCHAR(20)),' 
												AND ISNULL(bitEnabled,0)=1
												AND ISNULL(bitAppliesToSite,0)=1 
												AND ISNULL(bitRequireCouponCode,0)=0
												AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
												AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
												AND (1 = (CASE 
															WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													OR
													1 = (CASE 
															WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													)
											)  IsOnSale',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                                    FROM 
										#tmpPagedItems I
									LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1',
									(CASE 
										WHEN @tintPreLoginPriceLevel > 0 
										THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
										ELSE '' 
									END),(CASE 
										WHEN @numDomainID = 172
										THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
										ELSE '' 
									END),' 
									OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													WHERE  W.numItemID = I.numItemCode 
													AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
													AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
			PRINT @strSQL

			IF LEN(ISNULL(@fldList,'')) > 0
			BEGIN
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
			END

			IF LEN(ISNULL(@fldList1,'')) > 0
			BEGIN
					
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
			END
			ELSE
			BEGIN
				SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
			END

			SET  @strSQL = @strSQL +' order by Rownumber';
              
                                       
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
	
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


/****** Object:  StoredProcedure [dbo].[USP_ManagekitParentsForChildItems]    Script Date: 07/26/2008 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managekitparentsforchilditems')
DROP PROCEDURE usp_managekitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_ManagekitParentsForChildItems]  
@numItemKitID as numeric(9)=0,  
@numChildItemID as numeric(9)=0,  
@numQtyItemsReq as numeric(9)=0,  
@byteMode as tinyint
AS  
IF @byteMode=0  
BEGIN
	IF NOT EXISTS(SELECT numItemDetailID FROM ItemDetails WHERE numItemKitID=@numItemKitID AND numChildItemID=@numChildItemID)
	BEGIN
		INSERT INTO ItemDetails 
		(numItemKitID,numChildItemID,numQtyItemsReq,sintOrder)  
		VALUES
		(@numItemKitID,@numChildItemID,@numQtyItemsReq,ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemKitID),0) + 1)  
	END
	ELSE
	BEGIN
		RAISERROR('CHILD_ITEM_ALREADY_ADDED',16,1)
	END
END  
  
IF @byteMode=1  
BEGIN  
	DELETE FROM ItemDetails WHERE numItemKitID=@numItemKitID and numChildItemID=@numChildItemID
END
GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount DECIMAL(20,5),
      @monTotalTax DECIMAL(20,5),
      @monTotalDiscount DECIMAL(20,5),
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0,
	  @numReferenceSalesOrder NUMERIC(18,0) = 0
    )
AS 
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment,numReferenceSalesOrder
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END),@numReferenceSalesOrder
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50),
			@vcAltContact VARCHAR(200)     
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9), @numContact NUMERIC(18,0) 
        DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId

            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,NULL,1,@numReturnHeaderID)	
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount DECIMAL(20,5), vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT, bitMarkupDiscount BIT, numOppBizDocItemID NUMERIC(18,0)
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,numOppBizDocItemID,
				bitDiscountType,fltDiscount
				,bitMarkupDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,numOppBizDocItemID,
				bitDiscountType,
				fltDiscount
				,bitMarkupDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0
		
			UPDATE
				ReturnItems
			SET
				monAverageCost = (CASE 
									WHEN @numOppId > 0 
									THEN 
										(CASE 
											WHEN @tintReturnType=2 
											THEN
												ISNULL((SELECT monPrice FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
											ELSE
												ISNULL((SELECT monAvgCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
										END)
									ELSE
										ISNULL((SELECT monAverageCost FROM Item WHERE numItemCode=ReturnItems.numItemCode),0)
								END),
				monVendorCost = (CASE 
									WHEN @numOppId > 0 
									THEN ISNULL((SELECT monVendorCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
									ELSE
										ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)
								END)
			WHERE
				numReturnHeaderID=@numReturnHeaderID
			 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1
			END 
		END
    END   
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numReturnHeaderID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH         
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN

	DECLARE @numOppItemID NUMERIC(18,0)
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @numWLocationID AS NUMERIC(18,0)
	DECLARE @monAmount as DECIMAL(20,5) 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @TotalOnHand FLOAT
	DECLARE @monItemAverageCost DECIMAL(20,5)
	DECLARE @monReturnAverageCost DECIMAL(20,5)
	DECLARE @monNewAverageCost DECIMAL(20,5)
	DECLARE @description AS VARCHAR(100)
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
  	
  	SELECT TOP 1 
		@numReturnItemID=numReturnItemID,
		@itemcode=RI.numItemCode,
		@numUnits=RI.numUnitHourReceived,
  		@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
		@monAmount=RI.monTotAmount,
		@numOppItemID=RI.numOppItemID
		,@monItemAverageCost=ISNULL(I.monAverageCost,0)
		,@monReturnAverageCost=ISNULL(OBDI.monAverageCost,RI.monAverageCost)
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON	
		RI.numOppBizDocItemID = OBDI.numOppBizDocItemID               
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID
				                                   
	WHILE @numReturnItemID>0                                        
	BEGIN   
		IF @numWareHouseItemID>0
		BEGIN        
			SELECT @TotalOnHand=SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
		             
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@numWLocationID=ISNULL(numWLocationID,0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			--Receive : SalesReturn 
			IF (@tintFlag=1 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                IF @onBackOrder >= @numUnits 
                BEGIN            
                    SET @onBackOrder = @onBackOrder - @numUnits            
                    SET @onAllocation = @onAllocation + @numUnits            
                END            
                ELSE 
                BEGIN            
                    SET @onAllocation = @onAllocation + @onBackOrder            
                    SET @numUnits = @numUnits - @onBackOrder            
                    SET @onBackOrder = 0            
                    SET @onHand = @onHand + @numUnits            
                END      
				
				IF @TotalOnHand + @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
				END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
		                            
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode   
			END
			--Revert : SalesReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
				IF @onHand  - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - @numUnits
				END
				ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
				BEGIN						
					SET @numUnits = @numUnits - @onHand
                    SET @onHand = 0 
                            
                    SET @onBackOrder = @onBackOrder + @numUnits
                    SET @onAllocation = @onAllocation - @numUnits  
				END	
				ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					SET @numUnits = @numUnits - @onHand
                    SET @onHand = 0 
                            
                    SET @numUnits = @numUnits - @onAllocation  
                    SET @onAllocation = 0 
                            
					SET @onBackOrder = @onBackOrder + @numUnits
				END

				IF @TotalOnHand - @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				                    
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END
			--Revert : PurchaseReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
			BEGIN
				DECLARE @numQtyReturnRemainingToDelete FLOAT
				SET @numQtyReturnRemainingToDelete = @numUnits
				
				IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID)
				BEGIN
					-- DELETE RETURN QTY
					DECLARE @TEMPReturn TABLE
					(
						ID INT,
						numWarehouseItemID NUMERIC(18,0),
						numReturnedQty FLOAT
					)

					INSERT INTO @TEMPReturn
					(
						ID,
						numWarehouseItemID,
						numReturnedQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
						,numWarehouseItemID
						,SUM(numReturnedQty)
					FROM
						OpportunityItemsReceievedLocationReturn OITLR
					INNER JOIN
						OpportunityItemsReceievedLocation OIRL
					ON
						OITLR.numOIRLID = OIRL.ID
					WHERE
						numReturnID=@numReturnHeaderID
						AND numReturnItemID = @numReturnItemID
					GROUP BY
						numWarehouseItemID

					DECLARE @i AS INT = 1
					DECLARE @iCOUNT AS INT
					DECLARE @numQtyReturned FLOAT
					DECLARE @numTempReturnWarehouseItemID NUMERIC(18,0)

					SELECT @iCOUNT = COUNT(*) FROM @TEMPReturn
							
					WHILE @i <= @iCOUNT
					BEGIN
						SELECT 
							@numTempReturnWarehouseItemID=numWarehouseItemID,
							@numQtyReturned=numReturnedQty
						FROM 
							@TEMPReturn
						WHERE
							ID = @i
						
						-- INCREASE THE OnHand Of Destination Location
						UPDATE
							WareHouseItems
						SET
							numBackOrder = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numBackOrder,0) - @numQtyReturned ELSE 0 END),         
							numAllocation = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numAllocation,0) + @numQtyReturned ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
							numOnHand = (CASE WHEN numBackOrder >= @numQtyReturned THEN numOnHand ELSE ISNULL(numOnHand,0) + @numQtyReturned - ISNULL(numBackOrder,0) END),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempReturnWarehouseItemID

						SET @numQtyReturnRemainingToDelete = ISNULL(@numQtyReturnRemainingToDelete,0) - ISNULL(@numQtyReturned,0)

						DECLARE @descriptionInner VARCHAR(MAX)='Purchase Return Delete (Qty:' + CAST(@numQtyReturned AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempReturnWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @descriptionInner, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId

						SET @i = @i + 1
					END

					DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID
				END

				IF ISNULL(@numQtyReturnRemainingToDelete,0) > 0
				BEGIN
					SET @description='Purchase Return Delete (Qty:' + CAST(@numQtyReturnRemainingToDelete AS VARCHAR(10)) + ')'

					IF @onBackOrder >= @numQtyReturnRemainingToDelete 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numQtyReturnRemainingToDelete            
						SET @onAllocation = @onAllocation + @numQtyReturnRemainingToDelete            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numQtyReturnRemainingToDelete = @numQtyReturnRemainingToDelete - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numQtyReturnRemainingToDelete            
					END
				END

				IF @TotalOnHand + @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				               
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END
			--Receive : PurchaseReturn 
			ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
			BEGIN
				-- FIRST TRY TO RETURN QTY FROM OTHER WAREHOUSES WHERE PURCHASE ORDER IS RECIEVED
				IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation OIRL WHERE numOppItemID=@numOppItemID AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= 0)
				BEGIN
					DECLARE @TEMP TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numReturnedQty FLOAT
					)

					INSERT INTO 
						@TEMP
					SELECT 
						ROW_NUMBER() OVER(ORDER BY OIRL.ID),
						OIRL.ID,
						OIRL.numWarehouseItemID,
						OIRL.numUnitReceieved,
						ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
					FROM
						OpportunityItemsReceievedLocation OIRL
					WHERE 
						numOppItemID=@numOppItemID
						AND (OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0
					ORDER BY
						(OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) DESC

					DECLARE @numTempOnHand AS FLOAT
					DECLARE @numRemainingQtyToReturn AS FLOAT = @numUnits
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempWarehouseItemID	NUMERIC(18,0)
					DECLARE @numRemaingUnits FLOAT

					DECLARE @j AS INT = 1
					DECLARE @jCOUNT AS INT

					SELECT @jCOUNT = COUNT(*) FROM @TEMP
							
					WHILE @j <= @jCOUNT AND @numRemainingQtyToReturn > 0
					BEGIN
						SELECT 
							@numTempOIRLID=numOIRLID,
							@numTempWarehouseItemID=T1.numWarehouseItemID,
							@numTempOnHand = ISNULL(WI.numOnHand,0),
							@numRemaingUnits=ISNULL(numUnitReceieved,0)-ISNULL(numReturnedQty,0)
						FROM
							@TEMP T1
						INNER JOIN
							WareHouseItems WI
						ON
							T1.numWarehouseItemID=WI.numWareHouseItemID
						WHERE
							ID = @j

						IF @numTempOnHand >= @numRemaingUnits
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = ISNULL(numOnHand,0) - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
							)

							SET @descriptionInner = 'Purchase Return Receive (Qty:' + CAST((CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END) AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
						END
						ELSE IF @numTempOnHand > 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = numOnHand - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
							)

							SET @descriptionInner ='Purchase Return Receive (Qty:' + CAST((CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END) AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
						END

						SET @j = @j + 1
					END


					IF @numRemainingQtyToReturn > 0
					BEGIN
						SET @description='Purchase Return Receive (Qty:' + CAST(@numRemainingQtyToReturn AS VARCHAR(10)) + ')'

						IF @onHand  - @numRemainingQtyToReturn >= 0
						BEGIN						
							SET @onHand = @onHand - @numRemainingQtyToReturn
						END
						ELSE
						BEGIN
							RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
							RETURN ;
						END
					END
				END

				IF @TotalOnHand - @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				              
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END 

			IF @tintReturnType=1 OR (@tintReturnType=2 AND @numWareHouseItemID > 0 AND (ISNULL(@numRemainingQtyToReturn,0) > 0 OR ISNULL(@numQtyReturnRemainingToDelete,0) > 0))
			BEGIN
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 

				DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainId = @numDomainId
			END         
		END
		  
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=RI.numItemCode,
			@numUnits=RI.numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=RI.monTotAmount,
			@numOppItemID=RI.numOppItemID
			,@monItemAverageCost=ISNULL(I.monAverageCost,0)
			,@monReturnAverageCost=ISNULL(OBDI.monAverageCost,RI.monAverageCost)
		FROM 
			ReturnItems RI 
		JOIN 
			Item I 
		ON 
			RI.numItemCode=I.numItemCode
		LEFT JOIN
			OpportunityBizDocItems OBDI
		ON	
			RI.numOppBizDocItemID = OBDI.numOppBizDocItemID                                
		WHERE 
			numReturnHeaderID=@numReturnHeaderID 
			AND (charitemtype='P' OR 1=(CASE 
										WHEN @tintReturnType=1 THEN 
											CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
												WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
												ELSE 0 
											END 
										ELSE 0 END)) 
			AND RI.numReturnItemID>@numReturnItemID 
		ORDER BY 
			RI.numReturnItemID
							
		IF @@ROWCOUNT=0 SET @numReturnItemID=0         
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

-- exec USP_ManageWorkOrderStatus @numWOId=10086,@numWOStatus=23184,@numUserCntID=1,@numDomainID=1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrderStatus')
DROP PROCEDURE USP_ManageWorkOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrderStatus]
@numWOId as numeric(9)=0,
@numWOStatus as numeric(9),
@numUserCntID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @CurrentWorkOrderStatus AS NUMERIC(18,0)
	SELECT @numDomain = @numDomainID

	--GET CURRENT STATUS OF WORK ORDER
	SELECT @CurrentWorkOrderStatus=numWOStatus FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	--ASSIGN NEW STATUS TO WORK ORDER
	UPDATE WorkOrder SET numWOStatus=@numWOStatus WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID AND numWOStatus <> 23184 

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
	IF ISNULL(@CurrentWorkOrderStatus,0) <> 23184 AND @numWOStatus=23184
	BEGIN
		DECLARE @PendingChildWO NUMERIC(18,0)

		;WITH CTE (numWOID) AS
		(
			SELECT
				numWOID 
			FROM 
				WorkOrder
			WHERE 
				numParentWOID = @numWOId 
				AND numWOStatus <> 23184
			UNION ALL
			SELECT 
				WorkOrder.numWOID 
			FROM 
				WorkOrder
			INNER JOIN
				CTE c
			ON
				 WorkOrder.numParentWOID = c.numWOID
			WHERE 
				WorkOrder.numWOStatus <> 23184
		)

		SELECT @PendingChildWO = COUNT(*) FROM CTE

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
		IF ISNULL(@PendingChildWO,0) > 0
		BEGIN
			RAISERROR('CHILD WORK ORDER STILL NOT COMPLETED',16,1);
		END

		DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS FLOAT,@numOppId AS NUMERIC(9), @numOppItemID NUMERIC(18,0)

		UPDATE WorkOrder SET bintCompletedDate=GETDATE() WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

		SELECT 
			@numWareHouseItemID=numWareHouseItemID
			,@numQtyItemsReq=numQtyItemsReq
			,@numOppId=ISNULL(numOppId,0) 
		FROM 
			WorkOrder 
		WHERE 
			numWOId=@numWOId 
			AND [numDomainID] = @numDomainID

		EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId
		 
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numWareHouseItemId NUMERIC(18,0)
			,numQtyItemsReq FLOAT
		)

		INSERT INTO @TEMPItems
		(
			numWareHouseItemId
			,numQtyItemsReq
		)		
		SELECT 
			numWareHouseItemId
			,numQtyItemsReq 
		FROM 
			WorkOrderDetails WD 
		JOIN 
			item i 
		ON 
			WD.numChildItemID=i.numItemCode 
		WHERE 
			numWOId=@numWOId 
			AND i.charItemType IN ('P')
			
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		SELECT @iCount=COUNT(*) FROM @TEMPItems			
		
		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numWareHouseItemId=numWareHouseItemId
				,@numQtyItemsReq=numQtyItemsReq
			FROM 
				@TEMPItems 
			WHERE 
				ID=@i

			EXEC USP_UpdatingInventory 1,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId

			SET @i = @i + 1
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			                
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numOppItemID NUMERIC(18,0),
					numItemCode NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numoppitemtCode,
					numItemCode,
					vcChildKitSelectedItems
				FROM
					OpportunityItems
				WHERE
					numOppId = @numOppID
					AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO @TempKitConfiguration
				SELECT
					numoppitemtCode,
					numItemCode,
					vcChildKitSelectedItems
				FROM
					OpportunityItems OI
				WHERE
					numOppId = @numOppID
					AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Opportunity_BulkSales')
DROP PROCEDURE USP_Opportunity_BulkSales
GO
CREATE PROCEDURE [dbo].[USP_Opportunity_BulkSales]  
  
AS  
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT,
		numMSOQID NUMERIC(18,0),
		numDivisionID NUMERIC(18,0),
		numContactID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterOpp TABLE
	(
		ID INT IDENTITY(1,1),
		numOppID NUMERIC(18,0)
	)

	DECLARE @TEMPOppItems TABLE
	(
		ID INT
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(30,16)
		,monTotAmount DECIMAL(20,5)
		,vcItemDesc VARCHAR(1000)
		,numWarehouseItmsID NUMERIC(18,0)
		,ItemType VARCHAR(30)
		,DropShip BIT
		,bitDiscountType BIT
		,fltDiscount DECIMAL(30,16)
		,monTotAmtBefDiscount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,numUOM NUMERIC(18,0)
		,bitWorkOrder BIT
		,numVendorWareHouse NUMERIC(18,0)
		,numShipmentMethod NUMERIC(18,0)
		,numSOVendorId NUMERIC(18,0)
		,numProjectID numeric(18,0)
		,numProjectStageID numeric(18,0)
		,numToWarehouseItemID NUMERIC(18,0)
		,Attributes varchar(500)
		,AttributeIDs VARCHAR(500)
		,vcSKU VARCHAR(100)
		,bitItemPriceApprovalRequired BIT
		,numPromotionID NUMERIC(18,0)
		,bitPromotionTriggered BIT
		,vcPromotionDetail VARCHAR(2000)
		,numSortOrder NUMERIC(18,0)
	)


	-- FIRST GET UNIQUE OPPID THAT NEEDS TO BE RE CREATED WHICH ARE NOT EXECUTED
	INSERT INTO 
		@TEMPMasterOpp
	SELECT
		DISTINCT numOppId
	FROM
		dbo.MassSalesOrderQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM @TEMPMasterOpp

	DECLARE @tempOppID AS INT
	DECLARE @numNewOppID AS NUMERIC(18,0)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numUserCntID AS NUMERIC(18,0)
	DECLARE @tintSource AS NUMERIC(9)
	DECLARE @txtComments AS VARCHAR(1000)
	DECLARE @bitPublicFlag AS BIT
	DECLARE @monPAmount AS DECIMAL(20,5)
	DECLARE @numAssignedTo AS NUMERIC(18,0)                                                                                                                                                                              
	DECLARE @strItems AS VARCHAR(MAX)
	DECLARE @dtEstimatedCloseDate DATETIME                                                            
	DECLARE @lngPConclAnalysis AS NUMERIC(18,0)
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @numOrderStatus NUMERIC(18,0)

	DECLARE @bitBillingTerms AS BIT
	DECLARE @intBillingDays as integer
	DECLARE @bitInterestType as bit
	DECLARE @fltInterest as float
	DECLARE @intShippingCompany AS NUMERIC(18,0)
	DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
	DECLARE @vcCouponCode VARCHAR(100)
	DECLARE @numShipFromWarehouse NUMERIC(18,0)
	DECLARE @numWillCallWarehouseID NUMERIC(18,0)
	DECLARE @dtReleaseDate DATE

	WHILE @i <= @iCount
	BEGIN
		SELECT @tempOppID=numOppID FROM @TEMPMasterOpp WHERE ID = @i

		IF EXISTS (SELECT * FROM OpportunityMaster WHERE numOppId=@tempOppID)
		BEGIN

			SELECT 
				@numDomainID=numDomainId
				,@numUserCntID=numCreatedBy
				,@tintSource=tintSource
				,@txtComments=txtComments
				,@bitPublicFlag=bitPublicFlag
				,@dtEstimatedCloseDate=GETUTCDATE()
				,@lngPConclAnalysis=lngPConclAnalysis
				,@numCurrencyID=numCurrencyID
				,@vcCouponCode=vcCouponCode
				,@numAssignedTo=numAssignedTo
				,@numOrderStatus=numStatus
				,@numShipFromWarehouse=ISNULL(numShipFromWarehouse,0)
				,@dtReleaseDate=dtReleaseDate
			FROM
				OpportunityMaster
			WHERE
				numOppId=@tempOppID

			IF ISNULL(@numShipFromWarehouse,0) > 0
			BEGIN
				SET @numWillCallWarehouseID = @numShipFromWarehouse
			END
			ELSE
			BEGIN
				SET @numWillCallWarehouseID = ISNULL((SELECT TOP 1 numWarehouseItmsID FROM OpportunityItems WHERE numOppId=@tempOppID AND ISNULL(numWarehouseItmsID,0) > 0),0)
			END


			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMPOppItems

			INSERT INTO 
				@TEMPOppItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numoppitemtCode)
				,numoppitemtCode
				,numItemCode
				,numUnitHour
				,monPrice
				,monTotAmount
				,vcItemDesc
				,numWarehouseItmsID
				,vcType
				,bitDropShip
				,bitDiscountType
				,fltDiscount
				,monTotAmtBefDiscount
				,vcItemName
				,numUOMId
				,bitWorkOrder
				,numVendorWareHouse
				,numShipmentMethod
				,numSOVendorId
				,0 AS numProjectID
				,0 AS numProjectStageID
				,numToWarehouseItemID
				,vcAttributes AS Attributes
				,vcAttrValues AS AttributeIDs
				,vcSKU
				,bitItemPriceApprovalRequired
				,numPromotionID
				,bitPromotionTriggered
				,vcPromotionDetail
				,numSortOrder
			FROM 
				OpportunityItems
			WHERE
				numOppId = @tempOppID

			DECLARE @k AS INT = 1
			DECLARE @numTempOppItemID NUMERIC(18,0)
			DECLARE @kCount AS INT
			SELECT @kCount=COUNT(*) FROM @TEMPOppItems
	
			--TODO CREATE ITEMS
			SET @strItems = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempOppItemID = numoppitemtCode
					,@strItems = CONCAT(@strItems
										,'<Item><Op_Flag>1</Op_Flag>'
										,'<numoppitemtCode>',ID,'</numoppitemtCode>'
										,'<numItemCode>',numItemCode,'</numItemCode>'
										,'<numUnitHour>',numUnitHour,'</numUnitHour>'
										,'<monPrice>',monPrice,'</monPrice>'
										,'<monTotAmount>',monTotAmount,'</monTotAmount>'
										,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
										,'<numWarehouseItmsID>',ISNULL(numWarehouseItmsID,0),'</numWarehouseItmsID>'
										,'<ItemType>',ItemType,'</ItemType>'
										,'<DropShip>',ISNULL(DropShip,0),'</DropShip>'
										,'<bitDiscountType>',ISNULL(bitDiscountType,0),'</bitDiscountType>'
										,'<fltDiscount>',ISNULL(fltDiscount,0),'</fltDiscount>'
										,'<monTotAmtBefDiscount>',monTotAmtBefDiscount,'</monTotAmtBefDiscount>'
										,'<vcItemName>',vcItemName,'</vcItemName>'
										,'<numUOM>',ISNULL(numUOM,0),'</numUOM>'
										,'<bitWorkOrder>',ISNULL(bitWorkOrder,0),'</bitWorkOrder>'
										,'<numVendorWareHouse>',ISNULL(numVendorWareHouse,0),'</numVendorWareHouse>'
										,'<numShipmentMethod>',ISNULL(numShipmentMethod,0),'</numShipmentMethod>'
										,'<numSOVendorId>',ISNULL(numSOVendorId,0),'</numSOVendorId>'
										,'<numProjectID>',ISNULL(numProjectID,0),'</numProjectID>'
										,'<numProjectStageID>',ISNULL(numProjectStageID,0),'</numProjectStageID>'
										,'<numToWarehouseItemID>',ISNULL(numToWarehouseItemID,0),'</numToWarehouseItemID>'
										,'<Attributes>',Attributes,'</Attributes>'
										,'<AttributeIDs>',AttributeIDs,'</AttributeIDs>'
										,'<vcSKU>',vcSKU,'</vcSKU>'
										,'<bitItemPriceApprovalRequired>',ISNULL(bitItemPriceApprovalRequired,0),'</bitItemPriceApprovalRequired>'
										,'<numPromotionID>',ISNULL(numPromotionID,0),'</numPromotionID>'
										,'<bitPromotionTriggered>',ISNULL(bitPromotionTriggered,0),'</bitPromotionTriggered>'
										,'<vcPromotionDetail>',vcPromotionDetail,'</vcPromotionDetail>'
										,'<numSortOrder>',ISNULL(numSortOrder,0),'</numSortOrder>')
									
				FROM 
					@TEMPOppItems 
				WHERE 
					ID=@k

			
				IF (SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numOppID = @tempOppID AND numOppItemID=@numTempOppItemID) > 0
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>true</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems>',
											STUFF((SELECT 
											',' + CONCAT(OKI.numChildItemID,'#',OKI.numWareHouseItemId,'-',OKCI.numItemID,'#',OKCI.numWareHouseItemId)
											FROM
												OpportunityKitChildItems OKCI
											INNER JOIN
												OpportunityKitItems OKI
											ON
												OKCI.numOppChildItemID = OKI.numOppChildItemID
											WHERE
												OKCI.numOppID=@tempOppID
												AND OKCI.numOppItemID=@numTempOppItemID
																		 FOR XML PATH('')), 
																		1, 1, ''),'</KitChildItems>')
				END
				ELSE
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>false</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems />')
				END
			

				SET @strItems = CONCAT(@strItems,'</Item>')

				SET @k = @k + 1
			END

			SET @strItems = CONCAT(@strItems,'</NewDataSet>')
		
			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMP

			-- GET ALL ORGANIZATION FOR WHICH ORDER NEEDS TO BE CREATED
			INSERT INTO @TEMP
			(
				ID,
				numMSOQID,
				numDivisionID,
				numContactID
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numMSOQID),
				numMSOQID,
				numDivisionId,
				numContactId
			FROM
				MassSalesOrderQueue
			WHERE
				numOppId = @tempOppID
				AND ISNULL(bitExecuted,0) = 0

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempMSOQID NUMERIC(18,0)
			DECLARE @numTempDivisionID NUMERIC(18,0)
			DECLARE @numTempContactID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMP

			WHILE @j <= @jCount
			BEGIN
			
				SELECT 
					@numTempMSOQID=numMSOQID
					,@numTempDivisionID=numDivisionID
					,@numTempContactID=numContactID
				FROM 
					@TEMP 
				WHERE 
					ID = @j

				SELECT 
					@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
					,@intBillingDays = ISNULL(numBillingDays,0)
					,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
					,@fltInterest=ISNULL(fltInterest ,0)
					,@numAssignedTo=ISNULL(numAssignedTo,0)
					,@intShippingCompany=ISNULL(intShippingCompany,0)
					,@numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0)
				FROM 
					DivisionMaster
				WHERE 
					numDomainID=@numDomainID AND numDivisionID=@numTempDivisionID

				BEGIN TRY
					SET @numNewOppID = 0

					EXEC USP_OppManage 
					@numNewOppID OUTPUT
					,@numTempContactID
					,@numTempDivisionID
					,@tintSource
					,''
					,@txtComments
					,@bitPublicFlag
					,@numUserCntID
					,0
					,@numAssignedTo
					,@numDomainID
					,@strItems
					,''
					,@dtEstimatedCloseDate
					,0
					,@lngPConclAnalysis
					,1
					,0
					,0
					,0
					,@numCurrencyID
					,1
					,@numOrderStatus
					,NULL
					,0
					,0
					,0
					,0
					,1
					,0
					,0
					,@bitBillingTerms
					,@intBillingDays
					,@bitInterestType
					,@fltInterest
					,0
					,0
					,NULL
					,@vcCouponCode
					,NULL
					,NULL
					,NULL
					,NULL
					,0
					,0
					,0
					,@intShippingCompany
					,0
					,@dtReleaseDate
					,0
					,0
					,0
					,0
					,@numWillCallWarehouseID
					,0
					,@numShipFromWarehouse
					,@numDefaultShippingServiceID
					,0

					EXEC USP_OpportunityMaster_CT @numDomainID,@numUserCntID,@numNewOppID

					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,numCreatedOrderID
					)
					VALUES
					(
						@numTempMSOQID
						,1
						,@numNewOppID
					)


					INSERT INTO CFW_Fld_Values_Opp
					(
						Fld_ID
						,Fld_Value
						,RecId
						,bintModifiedDate
						,numModifiedBy
					)
					SELECT
						Fld_ID
						,Fld_Value
						,@numNewOppID
						,GETUTCDATE()
						,numModifiedBy
					FROM
						CFW_Fld_Values_Opp
					WHERE
						RecId=@tempOppID
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE(),ERROR_LINE()
						
					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,vcError
					)
					VALUES
					(
						@numTempMSOQID
						,0
						,ERROR_MESSAGE()
					)
				END CATCH

				SET @j = @j + 1
			END
		END

		SET @i = @i + 1
	END
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT

	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	BEGIN TRANSACTION

		-- CHANGE BIZDIC QTY TO AVAIALLBE QTY TO SHIP
		UPDATE
			OBI
		SET
			OBI.numUnitHour = (CASE 
								WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
								THEN 
									(CASE 
										WHEN (I.charItemType = 'p' AND ISNULL(OBI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
										THEN 
											(CASE WHEN OBI.numUnitHour <= dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN OBI.numUnitHour ELSE dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
										ELSE OBI.numUnitHour
									END) 
								ELSE OBI.numUnitHour
							END)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		--CHANGE TOTAL AMOUNT
		UPDATE
			OBI
		SET
			OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0),
			OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
		END

		BEGIN /* GET ALL ITEMS ADDED TO BIZDOC WITH ASSIGNED SERIAL/LOT# FOR SERIAL/LOT ITEM  */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO
				@TempItemXML
			SELECT
				OBI.numOppItemID,
				OBI.numUnitHour,
				OBI.numWarehouseItmsID,
				STUFF((SELECT 
							',' + WIDL.vcSerialNo + (CASE WHEN ISNULL(I.bitLotNo,0) = 1 THEN '(' + CAST(OWSI.numQty AS VARCHAR) + ')'  ELSE '' END)
						FROM 
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WIDL
						ON
							OWSI.numWarehouseItmsDTLID = WIDL.numWareHouseItmsDTLID
						INNER JOIN
							WareHouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						INNER JOIN
							Item I
						ON
							WI.numItemID = I.numItemCode
						WHERE 
							OWSI.numOppID = @numOppId
							AND OWSI.numOppItemID = OBI.numOppItemID
							AND ISNULL(OWSI.numOppBizDocsId,0) = 0 --THIS CONDITION IS REQUIRED BECAUSE IT IS AUTOFULFILLMENT
					for xml path('')
				),1,1,'') AS vcSerialLot
			FROM 
				OpportunityBizDocItems OBI
			WHERE
				OBI.numOppBizDocID = @numOppBizDocID
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT
		DECLARE @numOnHand AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numChildOnHand AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numKitChildOnHand AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN
										--REMOVE QTY FROM ALLOCATION
										SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

										IF @numKitChildAllocation >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numAllocation = @numKitChildAllocation,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
										END
									END
									ELSE  -- When added fulfillment order
									BEGIN
										--REMOVE QTY FROM ON HAND
										SET @numKitChildOnHand = @numKitChildOnHand - @numKitChildQty

										IF @numKitChildOnHand >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numOnHand = @numKitChildOnHand,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
										END
									END

									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--REMOVE QTY FROM ALLOCATION
								SET @numChildAllocation = @numChildAllocation - @numChildQty

								IF @numChildAllocation >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
								END
							END
							ELSE -- When added fulfillment order
							BEGIN
								--REMOVE QTY FROM ONHAND
								SET @numChildOnHand = @numChildOnHand - @numChildQty

								IF @numChildOnHand >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numOnHand = @numChildOnHand,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
							END     	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation,0),@numOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--REMOVE QTY FROM ALLOCATION
						SET @numAllocation = @numAllocation - @numQty

						IF (@numAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END
					END
					ELSE 
					BEGIN
						--REMOVE QTY FROM ON HAND
						SET @numOnHand = @numOnHand - @numQty

						IF (@numOnHand >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = @numOnHand,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
						END
					END
						
					IF @bitSerial = 1
					BEGIN
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,
							numOppID,
							numOppItemID,
							numWarehouseItmsID,
							numQty,
							numOppBizDocsId	
						)
						SELECT
							(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
							@numOppID,
							@numOppItemID,
							@numWarehouseItemID,
							1,
							@numOppBizDocID
						FROM
							dbo.SplitString(@vcSerialLot,',')


						UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
					END

					IF @bitLot = 1
					BEGIN
						DELETE FROM @TempLot

						INSERT INTO
							@TempLot
						SELECT
							SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
							CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
						FROM
							dbo.SplitString(@vcSerialLot,',')

						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,
							numOppID,
							numOppItemID,
							numWarehouseItmsID,
							numQty,
							numOppBizDocsId	
						)
						SELECT
							(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
							@numOppID,
							@numOppItemID,
							@numWarehouseItemID,
							TEMPLOT.numQty,
							@numOppBizDocID
						FROM
							@TempLot TEMPLOT


						UPDATE	TEMPWareHouseItmsDTL
							SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
						FROM
							WareHouseItmsDTL TEMPWareHouseItmsDTL
						INNER JOIN
							@TempLot TEMPLOT
						ON
							TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
						WHERE
							TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)


	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped Return (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN						
										--PLACE QTY BACK ON ALLOCATION
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = ISNULL(numAllocation,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END
									ELSE -- When added fulfillment order
									BEGIN
										--PLACE QTY BACK ON ONHAND
										UPDATE  
											WareHouseItems
										SET     
											numOnHand = ISNULL(numOnHand,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END

									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped Return (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--PLACE QTY BACK ON ALLOCATION
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = ISNULL(numAllocation,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							ELSE  -- When added fulfillment order
							BEGIN
								--PLACE QTY BACK ON ONHAND
								UPDATE  
									WareHouseItems
								SET     
									numOnHand = ISNULL(numOnHand,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--PLACE QTY BACK ON ALLOCATION
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = ISNULL(numAllocation,0) + @numQty
							,dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID
					END
					ELSE -- When added fulfillment order
					BEGIN
						-- ADD QUANTITY BACK ON ONHAND
						UPDATE  
							WareHouseItems
						SET     
							numOnHand = ISNULL(numOnHand,0) + @numQty
							,dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID
					END

					IF @bitSerial = 1 OR @bitLot = 1
					BEGIN 
						UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=NULL WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_UnReceiveQty')
DROP PROCEDURE USP_OpportunityItems_UnReceiveQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_UnReceiveQty] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numUnitHourToUnreceive FLOAT
AS
BEGIN
	-- CODE LEVE TRANSACTION SCOPE IS USED

	DECLARE @description AS VARCHAR(500)
	DECLARE @fltExchangeRate FLOAT 
	SELECT @fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) FROM OpportunityMaster WHERE numOppId=@numOppID

	DECLARE @TEMPReceievedItems TABLE
	(
		ID INT,
		numOIRLID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numUnitReceieved FLOAT,
		numDeletedReceievedQty FLOAT
	)
		
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @onHand AS FLOAT                                            
	DECLARE @onOrder AS FLOAT                                            
	DECLARE @onBackOrder AS FLOAT                                              
	DECLARE @onAllocation AS FLOAT
	DECLARE @numUnitHourReceived FLOAT
	DECLARE @numDeletedReceievedQty FLOAT
	DECLARE @bitDropship BIT
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID NUMERIC(18,0)
	DECLARE @TotalOnHand AS NUMERIC(18,0)
	DECLARE @monAvgCost AS DECIMAL(20,5)
	DECLARE @monPrice AS DECIMAL(20,5) 

	IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numUnitHourReceived,0) = @numUnitHourToUnreceive)
	BEGIN
		IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId=OM.numOppId WHERE numoppitemtCode=@numOppItemID AND ISNULL(numUnitHourReceived,0) > 0 AND ISNULL(OM.tintshipped,0)=0)
		BEGIN
			SELECT 
				@numItemCode=OI.numItemCode
				,@numUnits=numUnitHour
				,@numUnitHourReceived=numUnitHourReceived
				,@numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
				,@numWareHouseItemID = ISNULL(numWarehouseItmsID,0)
				,@numWLocationID = ISNULL(numWLocationID,0)
				,@monPrice=monTotAmount * @fltExchangeRate
				,@monAvgCost = I.monAverageCost
				,@bitDropship=ISNULL(OI.bitDropship,0)
			FROM
				OpportunityItems OI
			INNER JOIN 
				WareHouseItems WI 
			ON 
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numoppitemtCode=@numOppItemID


			IF @numWareHouseItemID > 0 AND @bitDropship=0
			BEGIN
				SET @TotalOnHand=0  
				SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode

				SELECT  
					@onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
				FROM 
					WareHouseItems
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 


				IF (SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID AND numOppItemID=@numOppItemID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @numOppId
						AND numOppItemID=@numOppItemID
				END

				INSERT INTO
					@TEMPReceievedItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					ISNULL(numDeletedReceievedQty,0)
				FROM
					OpportunityItemsReceievedLocation 
				WHERE
					numDomainID=@numDomainID
					AND numOppID=@numOppId
					AND numOppItemID=@numOppItemID

				DECLARE @j AS INT = 1
				DECLARE @COUNT AS INT
				DECLARE @numTempOIRLID NUMERIC(18,0)
				DECLARE @numTempOnHand FLOAT
				DECLARE @numTempWarehouseItemID NUMERIC(18,0)
				DECLARE @numTempUnitReceieved FLOAT
				DECLARE @numTempDeletedReceievedQty FLOAT				
				DECLARE @vcFromLocation AS VARCHAR(300)

				SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

				WHILE @j <= @COUNT
				BEGIN
					SELECT 
						@numTempOIRLID=TRI.numOIRLID,
						@numTempOnHand= ISNULL(numOnHand,0),
						@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
						@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
						@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
					FROM 
						@TEMPReceievedItems TRI
					INNER JOIN
						WareHouseItems WI
					ON
						TRI.numWarehouseItemID=WI.numWareHouseItemID
					WHERE 
						ID=@j

					
					SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numTempWarehouseItemID

					IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
					BEGIN
						UPDATE
							WareHouseItems
						SET
							numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0)),
							numOnOrder = ISNULL(numOnOrder,0),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempWarehouseItemID

						UPDATE
							WareHouseItems
						SET 
							numOnOrder = ISNULL(numOnOrder,0) + (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0)),
							dtModified = GETDATE()
						WHERE 
							numWareHouseItemID = @numWareHouseItemID

						UPDATE
							OpportunityItems
						SET
							numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0))
						WHERE
							numoppitemtCode=@numOppItemID
					END
					ELSE
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
						RETURN
					END 

					SET @description = CONCAT('PO Qty Unreceived (Qty: ',CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)),')')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID

					SET @numUnitHourReceived = ISNULL(@numUnitHourReceived,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty)

					SET @description= CONCAT('PO Qty Unreceived From Internal Location ',ISNULL(@vcFromLocation,''),' (Qty:',CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)),')')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID

					DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

					SET @j = @j + 1
				END

				--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
				SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE	numoppitemtCode=@numOppItemID
				
				SET @description= CONCAT('PO Qty Unreceived (Qty:',@numUnitHourReceived,')')

				IF @onHand >= @numUnitHourReceived
				BEGIN
					IF @TotalOnHand - @numUnitHourReceived <= 0
					BEGIN
						SET @monAvgCost = 0
					END
					ELSE
					BEGIN
						SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnitHourReceived * (@monPrice/@numUnits))) / ( @TotalOnHand - @numUnitHourReceived )
					END        
				
					PRINT @monAvgCost
					PRINT @numItemCode

					UPDATE  
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @numItemCode

					UPDATE
						WareHouseItems
					SET 
						numOnHand = ISNULL(numOnHand,0) - ISNULL(@numUnitHourReceived,0),
						numOnOrder = ISNULL(numOnOrder,0) + ISNULL(@numUnitHourReceived,0),
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					UPDATE OpportunityItems SET numUnitHourReceived=0,numDeletedReceievedQty=0 WHERE numOppitemtcode = @numOppItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				UPDATE OpportunityItems SET numUnitHourReceived=0 WHERE numOppitemtcode = @numOppItemID
			END
		END
	END
	ELSE 
	BEGIN
		RAISERROR('RECEIVED_QTY_IS_CHANGED_BY_OTHER_USER',16,1)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited
BEGIN 
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @numDomain = OM.numDomainID, @tintCommitAllocation=ISNULL(tintCommitAllocation,1)  FROM [dbo].[OpportunityMaster] AS OM INNER JOIN Domain D ON OM.numDomainId = D.numDomainId WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 AND @tintCommitAllocation=1
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
				DECLARE @numWOID AS NUMERIC(18,0)
				DECLARE @numWOStatus AS NUMERIC(18,0)
				SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

				IF  @tintMode=2
				BEGIN
					SET @description='SO-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END
				ELSE
				BEGIN
					SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END


				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 


				--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
				IF @numWOStatus <> 23184
				BEGIN
					IF @onOrder >= @numUnits
						SET @onOrder = @onOrder - @numUnits
					ELSE
						SET @onOrder = 0
				END

				-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
				-- DECREASE BACKORDER QTY BY QTY TO REVERT
				IF @numUnits < @onBackOrder 
				BEGIN                  
					SET @onBackOrder = @onBackOrder - @numUnits
				END 
				-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
				-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
				-- SET BACKORDER QTY TO 0
				ELSE IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                        
					--REMOVE ITEM FROM ALLOCATION 
					IF (@onAllocation - @numUnits) >= 0
						SET @onAllocation = @onAllocation - @numUnits
						
					--ADD QTY TO ONHAND
					SET @onHand = @onHand + @numUnits
				END

				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   

				--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
				IF @numWOStatus <> 23184
				BEGIN
					EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID,1
				END
			END	
			ELSE
			BEGIN
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF (@tintmode = 1 OR @tintMode=2) AND ISNULL(@bitStockTransfer,0)=0 AND EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomain AND numOppID=@numOppId AND numOppItemID=@numoppitemtCode)
			BEGIN
				DECLARE @TEMPReceievedItems TABLE
				(
					ID INT,
					numOIRLID NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					numUnitReceieved FLOAT,
					numDeletedReceievedQty FLOAT
				)

				DELETE FROM @TEMPReceievedItems

				INSERT INTO
					@TEMPReceievedItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					ISNULL(numDeletedReceievedQty,0)
				FROM
					OpportunityItemsReceievedLocation 
				WHERE
					numDomainID=@numDomain
					AND numOppID=@numOppId
					AND numOppItemID=@numoppitemtCode

				DECLARE @i AS INT = 1
				DECLARE @COUNT AS INT
				DECLARE @numTempOIRLID NUMERIC(18,0)
				DECLARE @numTempOnHand FLOAT
				DECLARE @numTempWarehouseItemID NUMERIC(18,0)
				DECLARE @numTempUnitReceieved FLOAT
				DECLARE @numTempDeletedReceievedQty FLOAT

				SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

				WHILE @i <= @COUNT
				BEGIN
					SELECT 
						@numTempOIRLID=TRI.numOIRLID,
						@numTempOnHand= ISNULL(numOnHand,0),
						@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
						@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
						@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
					FROM 
						@TEMPReceievedItems TRI
					INNER JOIN
						WareHouseItems WI
					ON
						TRI.numWarehouseItemID=WI.numWareHouseItemID
					WHERE 
						ID=@i

					IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
					BEGIN
						UPDATE
							WareHouseItems
						SET
							numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempWarehouseItemID

						UPDATE
							OpportunityItems
						SET
							numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						WHERE
							numoppitemtCode=@numoppitemtCode					
					END
					ELSE
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
						RETURN
					END

					IF @tintMode = 2
					BEGIN
						SET @description='PO DEMOTED TO OPPORTUNITY (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numTempUnitReceieved AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numTempUnitReceieved AS VARCHAR(10)) + ')'
					END

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain

					DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

					SET @i = @i + 1
				END
			END
		
			--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE	numoppitemtCode=@numoppitemtCode

			IF @tintMode = 2
			BEGIN
				SET @description='PO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END

			--Partial Fulfillment
			IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
			BEGIN
				SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
			END
						
			SET @numUnits = @numUnits - @numUnitHourReceived

			IF (@onOrder - @numUnits)>=0
			BEGIN
				--Causing Negative Inventory Bug ID:494
				SET @onOrder = @onOrder - @numUnits	
			END
			ELSE IF (@onHand + @onOrder) - @numUnits >= 0
			BEGIN						
				SET @onHand = @onHand - (@numUnits-@onOrder)
				SET @onOrder = 0
			END
			ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
			BEGIN
				Declare @numDiff numeric
	
				SET @numDiff = @numUnits - @onOrder
				SET @onOrder = 0

				SET @numDiff = @numDiff - @onHand
				SET @onHand = 0

				SET @onAllocation = @onAllocation - @numDiff
				SET @onBackOrder = @onBackOrder + @numDiff
			END
					    
			UPDATE
				WareHouseItems
			SET 
				numOnHand = @onHand,
				numAllocation = @onAllocation,
				numBackOrder = @onBackOrder,
				numOnOrder = @onOrder,
				dtModified = GETDATE()
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			IF (@tintmode = 1 OR @tintMode=2)
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numUnitHourReceived = 0
					,numDeletedReceievedQty = 0
				WHERE
					numoppitemtCode=@numoppitemtCode
			END
				       
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomain                                      
		END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        
		IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savebackorderauthgroup')
DROP PROCEDURE usp_savebackorderauthgroup
GO
CREATE PROCEDURE [dbo].[USP_SaveBackOrderAuthGroup] 
	@numDomainID AS NUMERIC(18,0),
	@strAuthGroupID AS VARCHAR(MAX)
AS
BEGIN

	DELETE from AuthenticationGroupBackOrder WHERE numDomainID=@numDomainID 

	IF LEN(ISNULL(@strAuthGroupID,'')) > 0
	BEGIN
		DECLARE @Tempdata TABLE 
		(
			numDomainID NUMERIC,
			strGroupID VARCHAR(MAX)
		)

		INSERT @Tempdata SELECT @numDomainID, @strAuthGroupID

		;WITH tmp(numDomainID, numGroupID, GroupID) AS
		(
			SELECT
				numDomainID,
				LEFT(strGroupID, CHARINDEX(',', strGroupID + ',') - 1),
				STUFF(strGroupID, 1, CHARINDEX(',', strGroupID + ','), '')
			FROM @Tempdata
			UNION all

			SELECT
				numDomainID,
				LEFT(GroupID, CHARINDEX(',', GroupID + ',') - 1),
				STUFF(GroupID, 1, CHARINDEX(',', GroupID + ','), '')
			FROM tmp
			WHERE
				GroupID > ''
		)


		INSERT INTO 
			AuthenticationGroupBackOrder
		SELECT
			numDomainID,    
			numGroupID
		FROM tmp
	END
END                        
GO
/****** Object:  StoredProcedure [dbo].[USP_SendPassword]    Script Date: 07/26/2008 16:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sendpassword')
DROP PROCEDURE usp_sendpassword
GO
CREATE PROCEDURE [dbo].[USP_SendPassword]
@vcEmail as varchar(100),
@vcPassword as varchar(500) output,
@NoOfRows as tinyint OUTPUT,
@bitPortal AS BIT=1,
@numContactID NUMERIC=0 OUTPUT,
@numDomainID AS NUMERIC 
as
BEGIN
	IF @bitPortal=1
	BEGIN
		SELECT 
			@NoOfRows=COUNT(*) 
		FROM 
			ExtranetAccountsDtl E
		JOIN 
			AdditionalContactsInformation A
		ON 
			E.numContactID=A.numContactID 
		WHERE 
			vcEmail=@vcEmail
			AND A.numDomainID=@numDomainID

		IF @NoOfRows = 1
		BEGIN
			DECLARE @ResetLinkID UNIQUEIDENTIFIER
			SET @ResetLinkID = NEWID()  

			UPDATE 
				E
			SET
				vcResetLinkID = CAST(@ResetLinkID AS VARCHAR(255))
				,vcResetLinkCreatedTime = GETUTCDATE()
			FROM
				ExtranetAccountsDtl	E
			JOIN 
				AdditionalContactsInformation A
			ON
				E.numContactID=A.numContactID 
			WHERE
				vcEmail=@vcEmail
				AND A.numDomainID=@numDomainID
		END

		SELECT TOP 1 
			@vcPassword=ISNULL(vcResetLinkID,'')
			,@numContactID=A.numContactId
		FROM 
			ExtranetAccountsDtl E
		JOIN 
			AdditionalContactsInformation A
		ON
			E.numContactID=A.numContactID 
		WHERE 
			vcEmail=@vcEmail
			AND A.numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		select top 1 @vcPassword=isnull([vcPassword],''),@numContactID=UM.numUserDetailId from [UserMaster] UM
		where [vcEmailID]=@vcEmail
	--	AND UM.numDomainID=@numDomainID 

		select @NoOfRows=count(*) from [UserMaster] UM
		where [vcEmailID]=@vcEmail
	--	AND UM.numDomainID=@numDomainID 
	
	END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost DECIMAL(20,5),
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float
as                            
BEGIN TRY
BEGIN TRANSACTION          
declare @CRMType as integer               
declare @numRecOwner as integer       
DECLARE @numPartenerContact NUMERIC(18,0)=0
	
SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
		
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1        

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
			                       
                            
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0
	-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
  bitBillingTerms,intBillingDays,bitInterestType,fltInterest
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo
  ,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest
		  )        
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
	C.vcCoupon=P.txtCouponCode 
	AND (C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
	AND numUserCntId =@numContactId
	AND ISNULL(bitEnabled,0)=1


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0),(CASE WHEN ISNULL(X.PromotionID,0) > 0 THEN ISNULL((SELECT CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) FROM PromotionOffer PO WHERE numProId=X.PromotionID AND numDomainId=@numDomainID),'') ELSE '' END),ISNULL(vcChildKitItemSelection,'')
FROM dbo.CartItems X WHERE numUserCntId =@numContactId


	--INSERT KIT ITEMS 
		IF (SELECT 
				COUNT(*) 
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId  
				AND ISNULL(OI.bitDropShip,0) = 0
				AND ISNULL(OI.numWarehouseItmsID,0) > 0
				AND I.charItemType = 'P'
				AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		END

			                
		INSERT INTO OpportunityKitItems
		(
			numOppId,
			numOppItemID,
			numChildItemID,
			numWareHouseItemId,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numUOMId,
			numQtyShipped,
			monAvgCost
		)
		SELECT
			@numOppId,
			OI.numoppitemtCode,
			ID.numChildItemID,
			(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
			(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
			ID.numQtyItemsReq,
			ID.numUOMId,
			0,
			ISNULL(I.monAverageCost,0)
		FROM 
			OpportunityItems OI 
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		JOIN 
			ItemDetails ID 
		ON 
			OI.numItemCode=ID.numItemKitID 
		JOIN
			Item I
		ON
			ID.numChildItemID = I.numItemCode
		WHERE 
			OI.numOppId=@numOppId 

		--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
		DECLARE @TempKitConfiguration TABLE
		(
			numOppItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			KitChildItems VARCHAR(MAX)
		)

		INSERT INTO 
			@TempKitConfiguration
		SELECT
			numoppitemtCode,
			numItemCode,
			vcChildKitSelectedItems
		FROM
			OpportunityItems
		WHERE
			numOppId = @numOppID
			AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
					,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				CROSS APPLY
				(
					SELECT
						*
					FROM 
						dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
				) as t2
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode
			WHERE
				Item.charItemType = 'P'
				AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID),0) = 0) > 0
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		END

		INSERT INTO OpportunityKitChildItems
		(
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numItemID,
			numWareHouseItemId,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numUOMId,
			numQtyShipped,
			monAvgCost
		)
		SELECT 
			@numOppID,
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			ItemDetails.numChildItemID,
			(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
			(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
			ItemDetails.numQtyItemsReq,
			ItemDetails.numUOMId,
			0,
			ISNULL(Item.monAverageCost,0)
		FROM
		(
			SELECT 
				t1.numOppItemID
				,t1.numItemCode
				,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
				,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
			FROM 
				@TempKitConfiguration t1
			CROSS APPLY
			(
				SELECT
					*
				FROM 
					dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
			) as t2
		) TempChildItems
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKI.numOppId=@numOppId
			AND OKI.numOppItemID=TempChildItems.numOppItemID
			AND OKI.numChildItemID=TempChildItems.numKitItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numItemCode = TempChildItems.numItemCode
			AND OI.numoppitemtcode = OKI.numOppItemID
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		INNER JOIN
			ItemDetails
		ON
			TempChildItems.numKitItemID = ItemDetails.numItemKitID
			AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
		INNER JOIN
			Item 
		ON
			ItemDetails.numChildItemID = Item.numItemCode

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice DECIMAL(20,5),                                                     
--  monTotAmount DECIMAL(20,5),                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount DECIMAL(20,5),
--  vcShippingMethod VARCHAR(100),decShippingCharge DECIMAL(20,5),dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)     
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
		SELECT
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId 
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME,
	@fltReorderQty FLOAT,
	@fltReorderPoint FLOAT
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	UPDATE Item SET fltReorderQty=@fltReorderQty WHERE numItemCode=@numItemCode
	UPDATE WareHouseItems SET numReorder=@fltReorderPoint WHERE numItemID=@numItemCode


	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDoc INT    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
		SELECT 
			* 
		INTO 
			#temp 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Item',2)    
		WITH 
		( 
			numWareHouseItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,intAdjust FLOAT
		) 
		
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
		INTO 
			#tempSerialLotNO 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
		WITH 
			(
				numItemCode NUMERIC(18,0)
				,numWareHouseID NUMERIC(18,0)
				,numWareHouseItemID NUMERIC(18,0)
				,vcSerialNo VARCHAR(3000)
				,byteMode TINYINT
			) 
		
		EXEC sp_xml_removedocument @hDoc  

		DECLARE @numOnHand FLOAT
		DECLARE @numAllocation FLOAT
		DECLARE @numBackOrder FLOAT
		DECLARE @numWareHouseItemID AS NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
			
		SELECT 
			@numWareHouseItemID = MIN(numWareHouseItemID) 
		FROM 
			#temp
			
		DECLARE @bitLotNo AS BIT = 0  
		DECLARE @bitSerialized AS BIT = 0  

		WHILE @numWareHouseItemID>0    
		BEGIN		  
			SELECT 
				@bitLotNo=ISNULL(Item.bitLotNo,0)
				,@bitSerialized=ISNULL(Item.bitSerialized,0)
			FROM 
				Item 
			INNER JOIN 
				#temp 
			ON 
				item.numItemCode=#temp.numItemCode 
			WHERE 
				#temp.numWareHouseItemID=@numWareHouseItemID 
				AND numDomainID=@numDomainID

			IF @bitLotNo=0 AND @bitSerialized=0
			BEGIN		
			


				IF (SELECT 
						ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
					FROM 
						WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID 
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
				BEGIN
					IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END)
							,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END
					ELSE
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END	  
					SELECT @numTempItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numTempItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = 'Inventory Adjustment', --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate = @dtAdjustmentDate,
					@numDomainId = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
				END
			END	  
				    
			SELECT TOP 1 
				@numWareHouseItemID = numWareHouseItemID 
			FROM 
				#temp 
			WHERE 
				numWareHouseItemID >@numWareHouseItemID 
			ORDER BY 
				numWareHouseItemID  
				    
			IF @@rowcount=0 
				SET @numWareHouseItemID =0    
		END    
    

		DROP TABLE #temp
    
    
		-------------Serial/Lot #s----------------
		DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
		SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
		DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
				@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
		DECLARE @posComma int, @strKeyVal varchar(20)
		    
		WHILE @minRowNo <= @maxRowNo
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
				@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
			SET @posComma=0
		
			SET @vcSerialNo=RTRIM(@vcSerialNo)
			IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

			SET @posComma=PatIndex('%,%', @vcSerialNo)
			WHILE @posComma>1
				BEGIN
					SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
					DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END		
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END
	  
				SET @numWareHouseItmsDTLID=0
				SET @OldQty=0
				SET @vcComments=''
			
				select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
						from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
						vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
				IF @byteMode=0 --Add
					SET @numQty=@OldQty + @strQty
				ELSE --Deduct
					SET @numQty=@OldQty - @strQty
			
				EXEC dbo.USP_AddUpdateWareHouseForItems
						@numItemCode = @numTempItemCode, --  numeric(9, 0)
						@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
						@numWareHouseItemID =@numWareHouseItemID,
						@numDomainID = @numDomainId, --  numeric(9, 0)
						@vcSerialNo = @strName, --  varchar(100)
						@vcComments = @vcComments, -- varchar(1000)
						@numQty = @numQty, --  numeric(18, 0)
						@byteMode = 5, --  tinyint
						@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
						@numUserCntID = @numUserCntID, --  numeric(9, 0)
						@dtAdjustmentDate = @dtAdjustmentDate
	  
				SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
				SET @posComma=PatIndex('%,%',@vcSerialNo)
			END

			SET @minRowNo=@minRowNo+1
		END
	
		DROP TABLE #tempSerialLotNO

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode
	END   
END


--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0,
	@numVendorInvoiceBizDocID NUMERIC(18,0) = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END


	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID


	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
	--ship item from FROM warehouse
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END
		
		-- Receive item from To Warehouse
		SET @numWarehouseItemID=@numToWarehouseItemID
    
	END  
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)

	SET @description='PO Qty Received (Qty:' + CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
	
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
	IF ISNULL(@bitStockTransfer,0) = 0
	BEGIN
		DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
		SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
		--Updating the Average Cost
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
		UPDATE  
			item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	IF ISNULL(@numSelectedWarehouseItemID,0) > 0 AND ISNULL(@numSelectedWarehouseItemID,0) <> @numWareHouseItemID		
	BEGIN
		DECLARE @vcFromLocation AS VARCHAR(300)
		DECLARE @vcToLocation AS VARCHAR(300)
		SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numWareHouseItemID
		SELECT @vcToLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numSelectedWarehouseItemID

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location ',@vcToLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Internal Location ',@vcFromLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE 
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
	@byteMode AS TINYINT=0,  
	@numWareHouseItemID AS NUMERIC(18,0)=0,  
	@Units AS FLOAT,
	@numWOId AS NUMERIC(9)=0,
	@numUserCntID AS NUMERIC(9)=0,
	@numOppId AS NUMERIC(9)=0,
	@numAssembledItemID AS NUMERIC(18,0) = 0,
	@fltQtyRequiredForSingleBuild AS FLOAT = 0   
AS      
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @Description AS VARCHAR(200)
	DECLARE @numItemCode NUMERIC(18)
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @ParentWOID AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
	SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomain
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppId
	END
  
	IF @byteMode=0  -- Aseeembly Item
	BEGIN  
		DECLARE @CurrentAverageCost DECIMAL(20,5)
		DECLARE @TotalCurrentOnHand FLOAT
		DECLARE @newAverageCost DECIMAL(20,5)
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		IF @numWOId > 0
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq_Orig * dbo.fn_UOMConversion(WOD.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				WorkOrderDetails WOD
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = WOD.numChildItemID
			WHERE 
				WOD.numWOId = @numWOId
				AND I.charItemType = 'P'

			UPDATE WorkOrder SET monAverageCost=@newAverageCost WHERE numWOId=@numWOId
		END
		ELSE
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				dbo.ItemDetails ID
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = ID.numChildItemID
			WHERE 
				numItemKitID = @numItemCode
				AND I.charItemType = 'P'

			UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID
		END

		SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
		UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

		IF ISNULL(@numWOId,0) > 0
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN 0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)  
				,numOnOrder = (CASE WHEN @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 THEN numOnOrder ELSE (CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0) - @Units END) END) 
				,dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
		ELSE
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN  0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)
				,dtModified = GETDATE() 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
	END  
	ELSE IF @byteMode=1  --Aseembly Child item
	BEGIN  
		DECLARE @onHand as FLOAT                                    
		DECLARE @onOrder as FLOAT                                    
		DECLARE @onBackOrder as FLOAT                                   
		DECLARE @onAllocation as FLOAT    
		DECLARE @monAverageCost AS DECIMAL(20,5)


		SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

		IF @numWOId > 0
		BEGIN
			UPDATE
				WorkOrderDetails
			SET
				monAverageCost=@monAverageCost
			WHERE
				numWOId=@numWOId
				AND numChildItemID=@numItemCode
				AND numWareHouseItemId=@numWareHouseItemID
		END
		ELSE
		BEGIN
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
			INSERT INTO [dbo].[AssembledItemChilds]
			(
				numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild
			)
			VALUES
			(
				@numAssembledItemID,
				@numItemCode,
				@Units,
				@numWareHouseItemID,
				@monAverageCost,
				@fltQtyRequiredForSingleBuild
			)
		END

		select                                     
			@onHand=isnull(numOnHand,0),                                    
			@onAllocation=isnull(numAllocation,0),                                    
			@onOrder=isnull(numOnOrder,0),                                    
			@onBackOrder=isnull(numBackOrder,0)                                     
		from 
			WareHouseItems 
		where 
			numWareHouseItemID=@numWareHouseItemID  


		IF ISNULL(@numWOId,0) > 0
		BEGIN
			IF @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 -- Allocation When Fulfillment Order (Packing Slip) is added
			BEGIN
				IF @onHand >= @Units
				BEGIN
					--RELEASE QTY FROM ON HAND
					SET @onHand=@onHand-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ONHAND',16,1)
				END
			END
			ELSE
			BEGIN
				IF @onAllocation >= @Units
				BEGIN
					--RELEASE QTY FROM ALLOCATION
					SET @onAllocation=@onAllocation-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ALLOCATION',16,1)
				END
			END
		END
		ELSE
		BEGIN
			--DECREASE QTY FROM ONHAND
			SET @onHand=@onHand-@Units
		END

		 --UPDATE INVENTORY
		UPDATE 
			WareHouseItems 
		SET      
			numOnHand=@onHand,
			numOnOrder=@onOrder,                         
			numAllocation=@onAllocation,
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()                                    
		WHERE 
			numWareHouseItemID=@numWareHouseItemID   
	END

	IF @numWOId>0
	BEGIN
    
		DECLARE @numReferenceID NUMERIC(18,0)
		DECLARE @tintRefType NUMERIC(18,0)

		IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				IF ISNULL(@ParentWOID,0) = 0
				BEGIN
					SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END

			SET @numReferenceID = @numOppId
			SET @tintRefType = 3
		END
		ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

			SET @numReferenceID = @numWOId
			SET @tintRefType=2
		END

		EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReferenceID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=0
	BEGIN
	DECLARE @desc AS VARCHAR(100)
	SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

	  EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @desc, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=1
	BEGIN
	SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

	EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
