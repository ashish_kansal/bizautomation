/******************************************************************
Project: Release 14.9 Date: 11.FEB.2021
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

ALTER TABLE Domain ADD bitDefaultProfileURL  BIT

/******************************************** SANDEEP *********************************************/

UPDATE BizFormWizardModule SET tintSortOrder = tintSortOrder + 1 WHERE tintSortOrder >= 3

--------------------------------

UPDATE BizFormWizardModule SET vcModule='Organizations' WHERE numBizFormModuleID=2

UPDATE BizFormWizardModule SET vcModule='Projects & Cases' WHERE numBizFormModuleID=3

--------------------------------

SET IDENTITY_INSERT BizFormWizardModule ON

INSERT INTO BizFormWizardModule
(
	numBizFormModuleID,vcModule,tintSortOrder
)
VALUES
(
	12,'Contacts',3
)

SET IDENTITY_INSERT BizFormWizardModule OFF

-------------------------------

UPDATE DynamicFormMaster SET numBizFormModuleID=12 WHERE numFormId IN (56,102,114)
UPDATE DynamicFormMaster SET numBizFormModuleID=11 WHERE numFormId=117
UPDATE BizFormWizardModule SET bitActive=0 WHERE numBizFormModuleID=9
ALTER TABLE DynamicFormMaster ADD tintPageType TINYINT 


--------------------------------------

--TODO: Check form id on live before update
UPDATE DynamicFormMaster SET tintPageType=3 WHERE numBizFormModuleID=2 AND numFormId IN (99,100,101,146,147)
UPDATE DynamicFormMaster SET tintPageType=1 WHERE numBizFormModuleID=2 AND numFormId IN (109,110,111,112,113)

--------------------------------------

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	1,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcLastSalesOrderDate'),'Last Order Date','DateField','D',1,0,0,1
)

--------------------

ALTER TABLE ItemDetails ADD tintView TINYINT DEFAULT 1
ALTER TABLE ItemDetails ADD vcFields VARCHAR(500)

UPDATE ItemDetails SET tintView = 1