/******************************************************************
Project: Release 12.2 Date: 27.JUL.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/
ALTER TABLE MassSalesFulfillmentConfiguration ADD tintPendingCloseFilter TINYINT 

-----------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,100,141,0,0,'Assigned To','SelectBox',35,1,35,1,0,0,1,1,1
),
(
	3,111,141,0,0,'Rec Owner','SelectBox',36,1,36,1,0,0,1,1,1
)

ALTER TABLE Item ALTER COLUMN txtItemDesc VARCHAR(2000)
ALTER TABLE OpportunityItems ALTER COLUMN vcItemDesc VARCHAR(2000)


-- FOLLOWING ARE EXECUTED ON PRODUCTION

ALTER TABLE SalesReturnCommission ADD numComRuleID NUMERIC(18,0)

--------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Profit','monProfit','monProfit','Profit','OpportunityMaster','M','R','Label','',0,0,0,0,0,0,0,0,1,1
)


SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
)
VALUES
(
	4,@numFieldID,'Profit','TextBox','M',1,1,0,0
)

---------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Profit Percent','numProfitPercent','numProfitPercent','ProfitPercent','OpportunityMaster','N','R','Label','',0,0,0,0,0,0,0,0,1,1
)


SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
)
VALUES
(
	4,@numFieldID,'Profit Percent','TextBox','N',1,1,0,0
)


/******************************************** PRASANT *********************************************/

UPDATE DycFieldMaster SET vcFieldName='Find Messages' WHERE vcFieldName='Subject' AND vcLookBackTableName='EmailHistory'
UPDATE DycFormField_Mapping SET vcFieldName='Find Messages' WHERE vcFieldName='Subject' AND numFormID=44

ALTER TABLE InboxTreeSort ADD bitHidden BIT DEFAULT 0

UPDATE DycFieldMaster SET vcAssociatedControlType='Label' WHERE numFieldId=172
UPDATE DycFormField_Mapping SET vcAssociatedControlType='Label' WHERE numFieldId=172 AND numFormID=44


DELETE FROM DycFieldMaster WHERE vcDbColumnName='StockQtyCount'
DELETE FROM DycFormField_Mapping WHERE numFormID=21 AND vcFieldName='Counted'

UPDATE DycFieldMaster SET vcFieldName='Counted/Qty to Adjust/Unit Cost' WHERE vcDbColumnName='StockQtyAdjust'
UPDATE DycFormField_Mapping SET vcFieldName='Counted/Qty to Adjust/Unit Cost' WHERE numFormID=21 AND vcFieldName='Qty to Adjust'