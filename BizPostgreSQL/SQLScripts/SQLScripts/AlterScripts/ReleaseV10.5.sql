/******************************************************************
Project: Release 10.5 Date: 27.NOV.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	140,'Import Accounting Entries','N','N',0,2
)

-------------------------------------

INSERT INTO DycFormSectionDetail
(
	numFormID,intSectionID,vcSectionName,Loc_id
)
VALUES
(
	140,1,'Accounting Fields',0
)

-----------------------------------

INSERT INTO ErrorMaster
(
	vcErrorCode,vcErrorDesc,tintApplication
)
VALUES
(
	'ERR077','Please select all required option(s).',3
)

-----------------------------

INSERT INTO ErrorMaster
(
	vcErrorCode,vcErrorDesc,tintApplication
)
VALUES
(
	'ERR078','Reset password is link is valid for 10 minutes only. Please try again.',3
)

-----------------------------------------------

ALTER TABLE CartItems ADD vcChildKitItemSelection VARCHAR(MAX)

ALTER TABLE OpportunityItems ADD vcChildKitSelectedItems VARCHAR(MAX)
--------------------------------------------

ALTER TABLE ExtranetAccountsDtl ADD
vcResetLinkID VARCHAR(300)
,vcResetLinkCreatedTime DATETIME
-------------------------------------------------

INSERT INTO PageElementMaster
(
	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
)
VALUES
(
	58,'ResetPassword','~/UserControls/ResetPassword.ascx','{#ResetPassword#}',1,0,0,0
)

-----------------------------------------

DECLARE @TEMP TABLE
(
	ID INT IDENTITY(1,1)
	,numSiteID NUMERIC(18,0)
	,numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMP
(
	numSiteID
	,numDomainID
)
SELECT
	numSiteID
	,numDomainID
FROM
	Sites

DECLARE @i INT = 1
DECLARE @iCount INT
DECLARE @numSiteID NUMERIC(18,0)
DECLARE @numDomainID NUMERIC(18,0)
DECLARE @numTemplateID NUMERIC(18,0)

SELECT @iCount = COUNT(*) FROM @TEMP

WHILE @i <= @iCount
BEGIN
	SELECT @numSiteID=numSiteID,@numDomainID=numDomainID FROM @TEMP WHERE ID=@i

	INSERT INTO SiteTemplates
	(
		vcTemplateName,numSiteID,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy,txtTemplateHTML
	)
	VALUES
	(
		'ResetPassword',@numSiteID,@numDomainID,GETUTCDATE(),GETUTCDATE(),0,0,'<title></title>
<center>
    <table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
        <tr>
            <td valign="top">
                <!--header starts-->
                {template:header}
                <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td width="260px" valign="top">
                            <!--left panel starts-->
                            {template:leftpanel}
                            <!--left panel ends-->
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                            <!--Content panel starts-->
							{#Error#}<br>
                            {#ResetPassword#}
                            <!--Content panel ends-->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <!--footer starts-->
                {template:footer}
                <!--footer ends-->
            </td>
        </tr>
    </table>
</center>'
	)

	SET @numTemplateID = SCOPE_IDENTITY()

	INSERT INTO SitePages
	(
		vcPageName,vcPageURL,tintPageType,vcPageTitle,numTemplateID,numSiteID,numDomainID,dtCreateDate,dtModifiedDate,bitIsActive,bitIsMaintainScroll,numCreatedBy,numModifiedBy
	)
	VALUES
	(
		'ResetPassword','ResetPassword.aspx',1,'Reset Password',@numTemplateID,@numSiteID,@numDomainID,GETUTCDATE(),GETUTCDATE(),1,0,0,0
	)

	SET @i = @i + 1
END

----------------------------------------------

INSERT INTO PageElementAttributes
(
	numElementID,vcAttributeName,vcControlType,bitEditor
)
VALUES
(
	58,'Html Customize','HtmlEditor',1
)

-------------------------------

SET IDENTITY_INSERT DycFieldMaster ON

INSERT INTO DycFieldMaster
(
	numFieldID,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	931,35,'Date (MM/dd/yyyy)','datEntry_Date','datEntry_Date','General_Journal_Header','V','R','DateField','',0,0,0,0,0,0,0,0,1,1
),
(
	932,35,'Chart of Accounts to Credit','numChartAcntId','numChartAcntId','General_Journal_Details','N','R','SelectBox','COA',0,0,0,0,0,0,0,0,1,1
),
(
	933,35,'Chart of Accounts to Debit','numChartAcntId','numChartAcntId','General_Journal_Details','N','R','SelectBox','COA',0,0,0,0,0,0,0,0,1,1
),
(
	934,35,'Amount','numAmount','numAmount','General_Journal_Header','N','R','TextBox','',0,0,0,0,0,0,0,0,1,1
),
(
	935,35,'Memo','varDescription','varDescription','General_Journal_Details','V','R','TextBox','',0,0,0,0,0,0,0,0,0,0
)

SET IDENTITY_INSERT DycFieldMaster OFF


-------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	35,931,'Date (MM/dd/yyyy)','DateField','',1,1,1,140
),
(
	35,932,'Chart of Accounts to Credit','SelectBox','',1,1,1,140
),
(
	35,933,'Chart of Accounts to Debit','SelectBox','',1,1,1,140
),
(
	35,934,'Amount','TextBox','',1,1,1,140
),
(
	35,3,'Vendor','TextBox','',1,1,1,140
),
(
	35,935,'Memo','TextBox','',1,1,1,140
)

----------------------------

UPDATE DycFormField_Mapping SET bitImport=1,intSectionID=1 WHERE numFormID=140

UPDATE DycFieldMaster SET vcAssociatedControlType='TextBox' WHERE numFieldId=934

----------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ChildKitsRelation]    Script Date: 27-Nov-18 3:12:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ChildKitsRelation](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numParentKitItemCode] [numeric](18, 0) NOT NULL,
	[numParentKitChildItemCode] [numeric](18, 0) NOT NULL,
	[numChildKitItemCode] [numeric](18, 0) NOT NULL,
	[numChildKitChildItemCode] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ChildKitsRelation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


