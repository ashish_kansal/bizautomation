/******************************************************************
Project: Release 12.2 Date: 27.JUL.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommission')
DROP PROCEDURE USP_CalculateCommission
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommission]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT,
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0)
	)

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0)
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OM.numOppId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OI.numoppitemtCode IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBD.numOppBizDocsId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBDI.numOppBizDocItemID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OI.numoppitemtCode,
			0,
			0,
			ISNULL(OI.numUnitHour,0),
			ISNULL(OI.monTotAmount,0),
			0,
			ISNULL(OI.monTotAmount,0),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			OM.numPartner
		FROM 
			OpportunityMaster OM
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission
		ON
			OM.numOppID = BizDocComission.numOppID
			AND OI.numoppitemtCode = BizDocComission.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND BizDocComission.numComissionID IS NULL
			AND OI.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OI.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppMItems.monTotAmount,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OM.numPartner
		FROM 
			OpportunityMaster OM
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission BDC1
		ON
			OM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND OBD.bitAuthoritativeBizDocs = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OBD.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OppM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OppM.numAssignedTo,
			OppM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppMItems.monTotAmount,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OppM.numPartner
		FROM 
			OpportunityBizDocs OppBiz
		INNER JOIN
			OpportunityMaster OppM
		ON
			OppBiz.numOppID = OppM.numOppID
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission BDC1
		ON
			OppM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OppM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN 
				dbo.DepositeDetails DD
			ON 
				DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OppBiz.numOppID 
				AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
		) TempDepositMaster
		WHERE
			OppM.numDomainId = @numDomainID 
			AND OppM.tintOppType = 1
			AND OppM.tintOppStatus = 1
			AND OppBiz.bitAuthoritativeBizDocs = 1
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
			AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END	

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	-- LOOP ALL COMMISSION RULES
	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0

	SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

	DECLARE @TEMPITEM TABLE
	(
		ID INT,
		UniqueID INT,
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	WHILE @i <= @COUNT
	BEGIN
		DECLARE @numComRuleID NUMERIC(18,0)
		DECLARE @tintComBasedOn TINYINT
		DECLARE @tintComAppliesTo TINYINT
		DECLARE @tintComOrgType TINYINT
		DECLARE @tintComType TINYINT
		DECLARE @tintAssignTo TINYINT
		DECLARE @numTotalAmount AS DECIMAL(20,5)
		DECLARE @numTotalUnit AS FLOAT

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
		DELETE FROM @TEMPITEM

		--FETCH COMMISSION RULE
		SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
		INSERT INTO
			@TEMPITEM
		SELECT
			ROW_NUMBER() OVER (ORDER BY ID),
			ID,
			(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM
			@TABLEPAID
		WHERE
			1 = (CASE @tintComAppliesTo
					--SPECIFIC ITEMS
					WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
					WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
					ELSE 1
				END)
			AND 1 = (CASE @tintComOrgType
						-- SPECIFIC ORGANIZATION
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
						ELSE 1
					END)
			AND 1 = (CASE @tintAssignTo 
						-- ORDER ASSIGNED TO
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
						WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
						ELSE 0
					END)


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
		INSERT INTO @TempBizCommission 
		(
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		)
		SELECT
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE @tintComType 
				WHEN 1 --PERCENT
					THEN 
						CASE 
							WHEN ISNULL(@bitCommissionBasedOn,1) = 1
							THEN
								CASE @tintCommissionBasedOn
									--ITEM GROSS PROFIT (VENDOR COST)
									WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									--ITEM GROSS PROFIT (AVERAGE COST)
									WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								END
							ELSE
								monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
						END
				ELSE  --FLAT
					T2.decCommission
			END,
			@numComRuleID,
			@tintComType,
			@tintComBasedOn,
			T2.decCommission,
			@tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM 
			@TEMPITEM AS T1
		CROSS APPLY
		(
			SELECT TOP 1 
				ISNULL(decCommission,0) AS decCommission
			FROM 
				CommissionRuleDtl 
			WHERE 
				numComRuleID=@numComRuleID 
				AND ISNULL(decCommission,0) > 0
				AND 1 = (CASE 
						WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
						THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
						THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						ELSE 0
						END)
		) AS T2
		WHERE
			(CASE 
				WHEN ISNULL(@bitCommissionBasedOn,1) = 1
				THEN
					CASE @tintCommissionBasedOn
						--ITEM GROSS PROFIT (VENDOR COST)
						WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
						--ITEM GROSS PROFIT (AVERAGE COST)
						WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
					END
				ELSE
					monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
			END) > 0 
			AND (
					SELECT 
						COUNT(*) 
					FROM 
						@TempBizCommission TempBDC
					WHERE 
						TempBDC.numUserCntID=T1.numUserCntID 
						AND TempBDC.numOppID=T1.numOppID 
						AND TempBDC.numOppBizDocID = T1.numOppBizDocID 
						AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
						AND TempBDC.numOppItemID = T1.numOppItemID 
						AND tintAssignTo = @tintAssignTo
				) = 0

		SET @i = @i + 1

		SET @numComRuleID = 0
		SET @tintComBasedOn = 0
		SET @tintComAppliesTo = 0
		SET @tintComOrgType = 0
		SET @tintComType = 0
		SET @tintAssignTo = 0
		SET @numTotalAmount = 0
		SET @numTotalUnit = 0
	END

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
	INSERT INTO BizDocComission
	(	
		numDomainID,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn
	)
	SELECT
		@numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		1,
		@numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		@bitCommissionBasedOn,
		@tintCommissionBasedOn
	FROM
		@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionSalesReturn')
DROP PROCEDURE USP_CalculateCommissionSalesReturn
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionSalesReturn]
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	DECLARE @TempCommissionSalesReturn TABLE
	(
		numComPayPeriodID NUMERIC(18,0)
		,numComRuleID NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,tintAssignTo TINYINT
		,numReturnHeaderID NUMERIC(18,0)
		,numReturnItemID NUMERIC(18,0)
		,tintComType TINYINT
		,tintComBasedOn TINYINT
		,decCommission FLOAT
		,monCommission DECIMAL(20,5)
	)

	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	DELETE FROM 
		SalesReturnCommission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommissionReversed,0)=0 
		AND numReturnItemID IN (SELECT 
									ReturnHeader.numReturnHeaderID
								FROM 
									ReturnHeader
								INNER JOIN
									ReturnItems
								ON
									ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
								CROSS APPLY
								(
									SELECT 
										MAX(datEntry_Date) dtDepositDate
									FROM 
										General_Journal_Header 
									WHERE 
										numDomainId=@numDomainId
										AND numReturnID=ReturnHeader.numReturnHeaderID
								) TempDepositMaster
								WHERE
									ReturnHeader.numDomainId = @numDomainID 
									AND ReturnHeader.numReturnStatus = 303
									AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

	INSERT INTO 
		@TempCommissionSalesReturn
	SELECT 
		@numComPayPeriodID
		,BizDocComission.numComRuleID
		,BizDocComission.numUserCntID
		,BizDocComission.tintAssignTo
		,ReturnHeader.numReturnHeaderID
		,ReturnItems.numReturnItemID
		,BizDocComission.tintComType
		,BizDocComission.tintComBasedOn
		,BizDocComission.decCommission
		,CASE tintComType 
			WHEN 1 --PERCENT
				THEN 
					CASE 
						WHEN ISNULL(bitDomainCommissionBasedOn,1) = 1
						THEN
							CASE tintDomainCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monVendorCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monAvgCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
							END
						ELSE
							ISNULL(ReturnItems.monTotAmount,0) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
					END
			ELSE  --FLAT
				BizDocComission.decCommission
		END monCommissionReversed
	FROM
		ReturnHeader
	INNER JOIN
		ReturnItems
	ON
		ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
	INNER JOIN
		BizDocComission
	ON
		ReturnItems.numOppItemID = BizDocComission.numOppItemID
	INNER JOIN
		OpportunityItems 
	ON
		ReturnItems.numOppItemID = OpportunityItems.numoppitemtCode
	CROSS APPLY
	(
		SELECT 
			MAX(datEntry_Date) dtDepositDate
		FROM 
			General_Journal_Header 
		WHERE 
			numDomainId=@numDomainId
			AND numReturnID=ReturnHeader.numReturnHeaderID
	) TempDepositMaster
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 303
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
	
	-- UPDATE FLAT COMMMISSION AMOUNT 
	UPDATE
		T1
	SET
		T1.monCommission= decCommission/(SELECT COUNT(*) FROM @TempCommissionSalesReturn Tinner WHERE Tinner.numUserCntID=T1.numUserCntID AND Tinner.tintComType=2)
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		tintComType=2

	INSERT INTO SalesReturnCommission
	(
		numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommissionReversed
		,bitCommissionReversed
	)
	SELECT
		@numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommission
		,0
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		(SELECT 
			COUNT(*) 
		FROM 
			SalesReturnCommission SRC
		WHERE 
			SRC.numUserCntID=T1.numUserCntID 
			AND SRC.numReturnHeaderID=T1.numReturnHeaderID 
			AND SRC.numReturnItemID = T1.numReturnItemID 
			AND SRC.tintAssignTo = T1.tintAssignTo
		) = 0

END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionCreditMemoOrRefundDetail')
DROP PROCEDURE USP_GetCommissionCreditMemoOrRefundDetail
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionCreditMemoOrRefundDetail]
(
    @numUserId AS NUMERIC(18,0) = 0,
    @numUserCntID AS NUMERIC(18,0) = 0,
    @numDomainId AS NUMERIC(18,0) = 0,
    @ClientTimeZoneOffset AS INT,
    @numComPayPeriodID AS NUMERIC(18,0),
	@numComRuleID AS NUMERIC(18,0) = NULL
)
AS
BEGIN	
	SELECT
		RH.numReturnHeaderID
		,CI.vcCompanyName
		,RH.vcRMA
		,RH.vcBizDocName
		,RH.tintReturnType
		,RH.tintReceiveType
		,RH.monBizDocAmount
		,SUM(decCommission) decCommission
	FROM
		SalesReturnCommission SRC
	INNER JOIN
		ReturnHeader RH
	ON
		SRC.numReturnHeaderID = RH.numReturnHeaderID
	INNER JOIN
		DivisionMaster DM
	ON
		RH.numDivisionId=DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		SRC.numComPayPeriodID = @numComPayPeriodID
		AND SRC.numUserCntID=@numUserCntID
		AND (ISNULL(@numComRuleID,0) = 0 OR SRC.numComRuleID = @numComRuleID)
		AND RH.numDomainId=@numDomainId
	GROUP BY
		RH.numReturnHeaderID
		,CI.vcCompanyName
		,RH.vcRMA
		,RH.vcBizDocName
		,RH.tintReturnType
		,RH.tintReceiveType
		,RH.monBizDocAmount
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportuntityCommission]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetOpportuntityCommission] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetOpportuntityCommission @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportuntitycommission')
DROP PROCEDURE usp_getopportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_GetOpportuntityCommission]
(
	@numUserId AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(18,0)  = 0,
	@numDomainId AS NUMERIC(18,0)  = 0,
	@numComPayPeriodID NUMERIC(18,0),
	@ClientTimeZoneOffset AS INT,
	@numOppBizDocsId AS NUMERIC(9) =0,
	@tintMode AS tinyint,
	@numComRuleID AS NUMERIC(18,0) = NULL,
	@numDivisionID NUMERIC(18,0) = NULL
)
AS
BEGIN
	DECLARE @bitIncludeShippingItem AS BIT
	DECLARE @numShippingItemID AS NUMERIC(18,0)
	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	
	SELECT 
		@bitIncludeShippingItem=bitIncludeTaxAndShippingInCommission
		,@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=ISNULL(numDiscountServiceItemID,0)
		,@tintCommissionType=tintCommissionType
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainId


	IF @tintMode=0  -- Paid Invoice
	BEGIN
		SELECT
			CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocID,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			ISNULL((SELECT SUM(monOrderSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			isnull(OpportunityBizDocs.monAmountPaid,0) monAmountPaid,
			TEMPDEPOSIT.dtDepositDate AS bintCreatedDate,
			OpportunityMaster.vcPOppName AS Name,
			Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(OpportunityBizDocs.monDealAmount,0) as DealAmount,
			Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
					When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
					When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole
			,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			ISNULL((SELECT SUM(monInvoiceSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
		FROM
			BizDocComission
		INNER JOIN
			OpportunityBizDocs
		ON
			BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
		INNER JOIN
			OpportunityMaster
		ON
			BizDocComission.numOppID = OpportunityMaster.numOppId
		INNER JOIN 
			DivisionMaster 
		ON 
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = OpportunityBizDocs.numBizDocId
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OpportunityBizDocs.numOppID 
				AND DD.numOppBizDocsID =OpportunityBizDocs.numOppBizDocsID
		) TEMPDEPOSIT
		WHERE
			numComPayPeriodID=@numComPayPeriodID
			AND ((BizDocComission.numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = @numDivisionID AND tintAssignTo = 3))
			AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= OpportunityBizDocs.monDealAmount
			AND (ISNULL(@numComRuleID,0)=0 OR BizDocComission.numComRuleID=@numComRuleID)
		GROUP BY
			CompanyInfo.vcCompanyName
			,LD.vcData
			,OpportunityBizDocs.numBizDocID
			,OpportunityMaster.numOppId
			,OpportunityBizDocs.numOppBizDocsId
			,OpportunityMaster.monDealAmount
			,OpportunityMaster.vcPOppName
			,OpportunityBizDocs.monAmountPaid
			,OpportunityMaster.tintOppStatus
			,OpportunityBizDocs.monDealAmount
			,OpportunityMaster.numRecOwner
			,OpportunityMaster.numAssignedTo
			,OpportunityBizDocs.vcBizDocID
			,OpportunityBizDocs.dtCreatedDate
			,TEMPDEPOSIT.dtDepositDate
		ORDER BY
			OpportunityBizDocs.dtCreatedDate
	END
	ELSE IF @tintMode=1 -- Non Paid Invoice
	BEGIN
		SELECT
			CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocID,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			ISNULL((SELECT SUM(monOrderSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			isnull(OpportunityBizDocs.monAmountPaid,0) monAmountPaid,
			DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate) AS bintCreatedDate,
			OpportunityMaster.vcPOppName AS Name,
			Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(OpportunityBizDocs.monDealAmount,0) as DealAmount,
			Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
					When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
					When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole
			,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			ISNULL((SELECT SUM(monInvoiceSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
		FROM
			BizDocComission
		INNER JOIN
			OpportunityBizDocs
		ON
			BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
		INNER JOIN
			OpportunityMaster
		ON
			BizDocComission.numOppID = OpportunityMaster.numOppId
		INNER JOIN 
			DivisionMaster 
		ON 
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = OpportunityBizDocs.numBizDocId
		WHERE
			numComPayPeriodID=@numComPayPeriodID
			AND ((BizDocComission.numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = @numDivisionID AND tintAssignTo = 3))
			AND ISNULL(OpportunityBizDocs.monAmountPaid,0) < OpportunityBizDocs.monDealAmount
			AND (ISNULL(@numComRuleID,0)=0 OR BizDocComission.numComRuleID=@numComRuleID)
		GROUP BY
			CompanyInfo.vcCompanyName
			,LD.vcData
			,OpportunityBizDocs.numBizDocID
			,OpportunityMaster.numOppId
			,OpportunityBizDocs.numOppBizDocsId
			,OpportunityMaster.monDealAmount
			,OpportunityMaster.vcPOppName
			,OpportunityBizDocs.monAmountPaid
			,OpportunityMaster.tintOppStatus
			,OpportunityBizDocs.monDealAmount
			,OpportunityMaster.numRecOwner
			,OpportunityMaster.numAssignedTo
			,OpportunityBizDocs.vcBizDocID
			,OpportunityBizDocs.dtCreatedDate
		ORDER BY
			OpportunityBizDocs.dtCreatedDate
  END
	ELSE IF @tintMode=2  -- Paid Invoice Detail
	BEGIN
        SELECT * FROM (Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost         
                              From OpportunityMaster Opp 
							  INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
							  INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND BC.numComPayPeriodID = @numComPayPeriodID 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A					     
    END
    ELSE IF @tintMode=3 -- Non Paid Invoice Detail
	BEGIN
        Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost       
                              From OpportunityMaster Opp 
							  INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
							  INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND BC.numComPayPeriodID = @numComPayPeriodID
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)
    END
	--ELSE IF @tintMode = 4
	--BEGIN
	--	SELECT
	--		vcCompanyName
	--		,'' AS BizDoc
	--		,0 numBizDocID
	--		,OpportunityMaster.numOppId
	--		,0 AS numOppBizDocsId
	--		,ISNULL(OpportunityMaster.monDealAmount,0) AS monSOSubTotal
	--		,CAST(ISNULL(TableGrossProfit.monGPVendorCost,0) AS DECIMAL(20,5)) AS monGPVendorCost
	--		,CAST(ISNULL(TableGrossProfit.monGPAverageCost,0) AS DECIMAL(20,5)) AS monGPAevrageCost
	--		,isnull(OpportunityMaster.monPAmount,0) monAmountPaid
	--		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate) AS bintCreatedDate
	--		,OpportunityMaster.vcPOppName AS Name
	--		,Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus
	--		,isnull(OpportunityMaster.monDealAmount,0) as DealAmount
	--		,Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
	--				When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
	--				When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
	--		end as EmpRole
	--		,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission
	--		,'' vcBizDocID
	--		,SUM(ISNULL(monOrderSubTotal,0)) AS monSubTotAMount
	--	FROM
	--		BizDocComission
	--	INNER JOIN
	--		OpportunityMaster
	--	ON
	--		BizDocComission.numOppID = OpportunityMaster.numOppId
	--	INNER JOIN 
	--		DivisionMaster 
	--	ON 
	--		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	--	INNER JOIN 
	--		CompanyInfo 
	--	ON 
	--		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	--	OUTER APPLY
	--	(
	--		SELECT
	--			SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit))) monGPVendorCost
	--			,SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * ISNULL(monAverageCost,0))) monGPAverageCost
	--		FROM
	--			OpportunityItems
	--		INNER JOIN
	--			Item
	--		ON
	--			OpportunityItems.numItemCode = Item.numItemCode
	--		LEFT JOIN
	--			Vendor
	--		ON
	--			Item.numVendorID = Vendor.numVendorID
	--			AND Item.numItemCode = Vendor.numItemCode
	--		WHERE
	--			OpportunityItems.numOppId = OpportunityMaster.numOppId
	--			AND ISNULL(Item.bitContainer,0) = 0
	--			AND 1  = (CASE WHEN @bitIncludeShippingItem=1 THEN 1 ELSE (CASE WHEN Item.numItemCode = ISNULL(@numShippingItemID,0) THEN 0 ELSE 1 END) END)
	--			AND Item.numItemCode <> ISNULL(@numDiscountItemID,0)
	--	) AS TableGrossProfit
	--	WHERE
	--		numComPayPeriodID=@numComPayPeriodID
	--	GROUP BY
	--		OpportunityMaster.numOppId
	--		,OpportunityMaster.vcPOppName
	--		,OpportunityMaster.monDealAmount
	--		,OpportunityMaster.monPAmount
	--		,OpportunityMaster.tintOppStatus
	--		,OpportunityMaster.numRecOwner
	--		,OpportunityMaster.numAssignedTo
	--		,OpportunityMaster.bintCreatedDate
	--		,CompanyInfo.vcCompanyName
	--		,TableGrossProfit.monGPVendorCost
	--		,TableGrossProfit.monGPAverageCost
	--	ORDER BY
	--		OpportunityMaster.bintCreatedDate
	--END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0
as                    
declare @Identity as numeric(9)                
declare @FromName as varchar(1000)                
declare @FromEmailAdd as varchar(1000)                
declare @startPos as integer                
declare @EndPos as integer                
declare @EndPos1 as integer                
                
  set @startPos=CHARINDEX( '<', @vcMessageFrom)                
  set @EndPos=CHARINDEX( '>', @vcMessageFrom)                
  if @startPos>0                 
  begin                
   set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
   set @startPos=CHARINDEX( '"', @vcMessageFrom)                
   set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
   set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                
                   
                   
  end                 
  else                 
  begin                
   set @FromEmailAdd=@vcMessageFrom                
   set @FromName=@vcMessageFromName                
  end                
declare @updateinsert as bit     
    
if @vcItemId = ''    
begin    
 set @updateinsert  = 1    
end    
else    
begin    
 if (select count(*) from emailhistory  where vcItemId =@vcItemId) > 0    
  begin    
   set @updateinsert  = 0    
  end    
 else    
  begin    
   set @updateinsert  = 1    
  end    
end    
                  
if @updateinsert =0    
begin              
update EmailHistory            
set            
vcSubject=@vcSubject,            
vcBody=@vcBody,      
vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
bintCreatedOn=getutcdate(),            
bitHasAttachments=@bitHasAttachment,            
vcItemId=@vcItemId,            
vcChangeKey=@vcChangeKey,            
bitIsRead=@bitIsRead,            
vcSize=convert(varchar(200),@vcSize),            
chrSource=@chrSource,            
tinttype=@tinttype,            
vcCategory =@vcCategory ,        
dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
where vcItemId = @vcItemId   and numDomainid= @numDomainId      
              
end                
else             
begin            
declare @node as NUMERIC(18,0)
set @node = case when @vcCategory = 'B' then ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4),0) else 0 end 
SET @node=(SELECT TOP 1 numNodeID FROM InboxTreeSort WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntId AND vcNodeName='Sent Mail')
SET @vcMessageTo = @vcMessageTo + '#^#'
SET @vcMessageFrom = @vcMessageFrom + '#^#'
SET @vcCC = @vcCC + '#^#'
SET @vcBCC = @vcBCC + '#^#'

EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

--IF @vcCategory = 'B'
--	SELECT @vcSize =DATALENGTH(@vcBody)
DECLARE @numNodeId AS NUMERIC(18,0)=0
IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')>0)
BEGIN
	SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')
END
ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')>0)
BEGIN
	SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')
END
ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')>0)
BEGIN
	SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')
END
ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')>0)
BEGIN
	SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')
END
Insert into EmailHistory(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
values                          
(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@numNodeId,dbo.fn_GetNewvcEmailString(@vcMessageFrom,@numDomainId),dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId),dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId),dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId),@IsReplied,@bitInvisible)              
set @Identity=SCOPE_IDENTITY()  
           

update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$'))
				where [numEmailHstrID]=@Identity                

update EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID
                
--exec USP_InsertEmailAddCCAndBCC  @vcMessageTo,1,@Identity , @vcMessageToName               
--exec USP_InsertEmailAddCCAndBCC  @vcBCC,2,@Identity ,''               
--exec USP_InsertEmailAddCCAndBCC  @vcCC,3,@Identity  ,@vcCCName          
--exec USP_InsertEmailAddCCAndBCC  @vcMessageFrom,4,@Identity,  @vcMessageFromName      
  
  --No longer being used .. We are calling same function from front end to add attachment to email.. -- by chintan       
--if @bitHasAttachment  = 1          
--begin          
--exec USP_InsertAttachment   @vcFileName,@vcAttachmentItemId,@vcAttachmentType,@Identity           
--end       

SELECT ISNULL(@Identity,0)
end
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRule]
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		DECLARE @isCommissionRuleUsed BIT = 0

		IF (SELECT COUNT(*) FROM BizDocComission WHERE numDomainId=@numDomainID AND numComRuleID=@numComRuleID) > 0
		BEGIN
			SET @isCommissionRuleUsed = 1
		END

		UPDATE  
			CommissionRules
        SET     
			[vcCommissionName] = @vcCommissionName,
            [tintComBasedOn] = CASE WHEN @isCommissionRuleUsed=1 THEN tintComBasedOn ELSE @tintComBasedOn END,
            [tinComDuration] = @tinComDuration,
            [tintComType] = CASE WHEN @isCommissionRuleUsed=1 THEN tintComType ELSE @tintComType END,
			[tintComAppliesTo] = @tintComAppliesTo,
			[tintComOrgType] = @tintComOrgType,
			[tintAssignTo] = @tintAssignTo
        WHERE   
			[numComRuleID] = @numComRuleID 
			AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
			IF @isCommissionRuleUsed = 0
			BEGIN
				DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
				INSERT  INTO [CommissionRuleDtl]
						(
							numComRuleID,
							[intFrom],
							[intTo],
							[decCommission]
						)
						SELECT  @numComRuleID,
								X.[intFrom],
								X.[intTo],
								X.[decCommission]
						FROM    ( SELECT    *
									FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
											WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
								) X  

			END
			
            
			INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
			
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1
			BEGIN
				-- ITEM ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=1
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 2
			BEGIN
				-- ITEM CLASSIFICATION
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=2
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT 
											ISNULL(numComRuleID,0) 
										FROM 
											CommissionRules 
										WHERE 
											numDomainID=@numDomainID 
											AND tintComAppliesTo = @tintComAppliesTo
											AND 1 = (CASE tintComAppliesTo 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=1 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=2 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintComOrgType=@tintComOrgType
											AND 1 = (CASE tintComOrgType 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintAssignTo = @tintAssignTo 
											AND numComRuleID <> @numComRuleID
										)
					) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderStatus')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderStatus]
(
	@numDomainID NUMERIC(18,0)
	,@tintOppType TINYINT
)
AS 
BEGIN
	SELECT 
		ListDetails.numListItemID AS ListItemID
		,ListDetails.vcData AS ListItemValue
		,COUNT(numOppId) AS StatusCount 
	FROM 
		ListDetails
	LEFT JOIN
		OpportunityMaster 
	ON
		OpportunityMaster.numStatus = ListDetails.numListItemID
		AND OpportunityMaster.numDomainID=@numDomainID 
		AND OpportunityMaster.tintOppType = @tintOppType
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
	WHERE
		ListDetails.numDomainID = @numDomainID
		AND ListDetails.numListID=176
		AND ListDetails.numListType = @tintOppType
		AND (ListDetails.tintOppOrOrder = 2 OR ISNULL(ListDetails.tintOppOrOrder,0) = 0)
	GROUP BY
		ListDetails.numListItemID
		,ListDetails.vcData
		,ListDetails.sintOrder
	ORDER BY
		ListDetails.sintOrder
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@bitFullyShippedInvoiced BIT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5  OR (@numViewID=2 AND @tintPackingViewMode=4)
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(1000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'ASC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'ASC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4)
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 2 AND @tintPackingViewMode = 4
							THEN 
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc'
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)',
																			(CASE 
																				WHEN @numViewID = 2 AND @tintPackingViewMode = 4
																				THEN 'WHEN @tintPackingViewMode = 4
																						THEN (CASE 
																								WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
																								THEN 1 
																								ELSE 0 
																							END)'
																				ELSE ''
																			END)
																			,'
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@bitFullyShippedInvoiced,0) = 1 
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4) THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid'  ELSE '' END)) ELSE '' END));

	PRINT CAST(@vcSQL AS NTEXT)

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType;

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,0
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortOrder
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityItems.numoppitemtCode
			,0
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,''''
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(Item.txtItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SELECT * FROM #TEMPResult
	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWLocationID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWLocationID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWLocationID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,'') AS vcKitChildItems
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF ISNULL(@bitGroupByOrder,0) = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWLocationID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numOppId = TEMP.numOppId
						AND T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.numWLocationID,0) = ISNULL(TEMP.numWLocationID,0)
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numOppId
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWLocationID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPoppName
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
		ORDER BY
			MIN(ID)
	END
	ELSE
	BEGIN
		SELECT
			0 AS numOppId
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWLocationID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.numWLocationID,0) = ISNULL(TEMP.numWLocationID,0)
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,'' AS vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWLocationID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
		ORDER BY
			MIN(ID)
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
	@byteMode as tinyint=null,                        
	@numOppId as numeric(9)=null,                        
	@numOppBizDocsId as numeric(9)=null,
	@ClientTimeZoneOffset AS INT=0,
	@numDomainID AS NUMERIC(18,0) = NULL,
	@numUserCntID AS NUMERIC(18,0) = NULL
)                        
AS      
BEGIN
	DECLARE @ParentBizDoc AS NUMERIC(9)=0
	DECLARE @lngJournalId AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
    
	SELECT 
		ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
		ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
		ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
	FROM 
		OpportunityMaster 
	JOIN 
		DivisionMaster 
	ON 
		DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
	JOIN 
		CompanyInfo
	ON 
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	WHERE 
		numOppId = @numOppId
		                                  
	IF @byteMode= 1                        
	BEGIN 
		DECLARE @numBizDocID AS INT
		DECLARE @bitFulfilled AS BIT
		Declare @tintOppType as tinyint
		DECLARE @tintShipped AS BIT
		DECLARE @bitAuthoritativeBizDoc AS BIT

		SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
		SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

		IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
			RETURN
		END

		IF EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		IF EXISTS (SELECT OBPD.* FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397 AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId) > 0
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT',16,1)
			RETURN
		END

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397
		BEGIN
			-- Revert Allocation
			EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
		END

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocsId NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppBizDocsId
		)
		SELECT
			numOppBizDocsId
		FROM
			OpportunityBizDocs 
		WHERE 
			numSourceBizDocId= @numOppBizDocsId

		BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT

			SELECT @iCount = COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @ParentBizDoc=numOppBizDocsId FROM @TEMP WHERE ID = @i

				IF NOT EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @ParentBizDoc)
				BEGIN
					EXEC USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END

				SET @i = @i + 1
			END

			--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
			IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
			BEGIN
				EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
			END

			DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
			DELETE  FROM OpportunityBizDocItems WHERE numOppBizDocID=@numOppBizDocsId              
			DELETE FROM OpportunityRecurring WHERE numOppBizDocID=@numOppBizDocsId and tintRecurringType=4
			UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId
				--Delete All entries for current bizdoc
			--EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
			DELETE OBPD FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId
			DELETE ECI FROM [EmbeddedCostItems] ECI INNER JOIN EmbeddedCost EC ON ECI.numEmbeddedCostID=EC.numEmbeddedCostID WHERE EC.[numOppBizDocID]=@numOppBizDocsId AND EC.[numDomainID] = @numDomainID
			DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId	AND [numDomainID] = @numDomainID
			DELETE OBDD	FROM OpportunityBizDocsDetails OBDD INNER JOIN EmbeddedCost EC ON OBDD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE	numOppBizDocID = @numOppBizDocsId

		

			--Credit Balance
			DECLARE @monCreditAmount AS DECIMAL(20,5);
			SET @monCreditAmount=0
			SET @numDivisionID=0

			SELECT 
				@monCreditAmount=ISNULL(oppBD.monCreditAmount,0)
				,@numDivisionID=Om.numDivisionID
				,@numOppId=OM.numOppId 
			FROM 
				OpportunityBizDocs oppBD 
			JOIN 
				OpportunityMaster Om 
			ON 
				OM.numOppId=oppBD.numOppId 
			WHERE
				numOppBizDocsId = @numOppBizDocsId

			DELETE FROM [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
			IF @tintOppType=1 --Sales
				UPDATE DivisionMaster SET monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			ELSE IF @tintOppType=2 --Purchase
				UPDATE DivisionMaster SET monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


			DELETE FROM DocumentWorkflow WHERE numDocID=@numOppBizDocsId AND cDocType='B'      
			DELETE BA FROM BizDocAction BA INNER JOIN BizActionDetails BAD ON BA.numBizActionId=BAD.numBizActionId WHERE BAD.numOppBizDocsId =@numOppBizDocsId and btDocType=1
			DELETE FROM BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=1


			DELETE GJD FROM General_Journal_Details GJD INNER JOIN General_Journal_Header GJH ON GJD.numJournalID=GJH.numJournal_Id WHERE GJH.numOppId=@numOppId AND GJH.numOppBizDocsId=@numOppBizDocsId AND GJH.numDomainId=@numDomainID
			DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID

			DELETE FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocsId                        

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH             
	END                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
		,0 AS IsFulfillmentGeneratedOnPickList
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId NUMERIC(18,0),
    @numDepartmentId NUMERIC(18,0),
    @numComPayPeriodID NUMERIC(18,0),
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	DECLARE @dtStartDate DATETIME
	DECLARE @dtEndDate DATETIME

	SELECT
		@dtStartDate = DATEADD (MS , 000 , CAST(dtStart AS DATETIME))
		,@dtEndDate = DATEADD (MS , -3 , CAST(dtEnd AS DATETIME))  
	FROM
		CommissionPayPeriod
	WHERE
		numComPayPeriodID=@numComPayPeriodID
	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate DECIMAL(20,5),
		decRegularHrs decimal(20,5),
		decPaidLeaveHrs decimal(20,5),
		monRegularHrsPaid DECIMAL(20,5),
		monAdditionalAmountPaid DECIMAL(20,5),
		monExpense DECIMAL(20,5),
		monReimburse DECIMAL(20,5),
		monReimbursePaid DECIMAL(20,5),
		monCommPaidInvoice DECIMAL(20,5),
		monCommPaidInvoiceDeposited DECIMAL(20,5),
		monCommUNPaidInvoice DECIMAL(20,5),
		monCommPaidCreditMemoOrRefund DECIMAL(20,5),
		monOverPayment DECIMAL(20,5),
		monCommissionEarned DECIMAL(20,5),
		monCommissionPaid DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal  DECIMAL(20,5),
		monTotalAmountDue DECIMAL(20,5),
		monAmountPaid DECIMAL(20,5), 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND ISNULL(UM.bitPayroll,0) = 1
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName


	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @monReimburse DECIMAL(20,5),@monCommPaidInvoice DECIMAL(20,5),@monCommPaidInvoiceDepositedToBank DECIMAL(20,5),@monCommUNPaidInvoice DECIMAL(20,5),@monCommPaidCreditMemoOrRefund DECIMAL(20,5)

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
	DECLARE @bitPartner BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0),
			@bitPartner=ISNULL(bitPartner,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT  
				@decTotalHrsWorked=ISNULL(SUM(numHours),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND (numType=1 OR numType=2) 
				AND numCategory=1

			SET @decTotalHrsWorked = ISNULL(@decTotalHrsWorked,0) + ISNULL((SELECT  
																			SUM(numHours)   
																		FROM 
																			StagePercentageDetailsTaskCommission 
																		WHERE 
																			numComPayPeriodID = @numComPayPeriodID
																			AND numUserCntID=@numUserCntID),0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT  
				@monReimburse=ISNULL(SUM(monTotalAmount),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND numCategory=2
				AND bitReimburse = 1
		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage = 2
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=oppBiz.numOppID 
				AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				AND DM.tintDepositePage=2 
				AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		) TEMPDeposit
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=ISNULL(SUM(BC.numComissionAmount),0) 
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT 
														SUM(monCommissionReversed) 
													FROM 
														SalesReturnCommission 
													WHERE 
														1 = (CASE 
																WHEN ISNULL(@bitPartner,0) = 1 
																THEN (CASE WHEN numUserCntID=@numDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
																ELSE (CASE WHEN numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3 THEN 1 ELSE 0 END)
															END)),0)	
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=ISNULL(@decTotalHrsWorked,0)
			,monReimburse=ISNULL(@monReimburse,0)
			,monCommPaidInvoice=ISNULL(@monCommPaidInvoice,0)
			,monCommPaidInvoiceDeposited=ISNULL(@monCommPaidInvoiceDepositedToBank,0)
			,monCommUNPaidInvoice=ISNULL(@monCommUNPaidInvoice,0)
			,monCommPaidCreditMemoOrRefund=ISNULL(@monCommPaidCreditMemoOrRefund,0)
			,monCommissionEarned = (CASE 
										WHEN ISNULL(bitPartner,0) = 1 
										THEN ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0) 
										ELSE ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
									END)
			,monOverPayment=(CASE 
								WHEN ISNULL(bitPartner,0) = 1 
								THEN ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numDivisionID AND BCInner.tintAssignTo=3),0) 
								ELSE ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numUserCntID AND BCInner.tintAssignTo <> 3),0)
							END)
			,monInvoiceSubTotal = (CASE 
										WHEN ISNULL(bitPartner,0) = 1 
										THEN ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID  AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0)
										ELSE ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID  AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
									END)
			,monOrderSubTotal = (CASE 
									WHEN ISNULL(bitPartner,0) = 1 
									THEN ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0)
									ELSE ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
								END)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(T1.monTotalAmt,0)
		,monRegularHrsPaid=ISNULL(T1.monHourlyAmt,0)
		,monAdditionalAmountPaid=ISNULL(T1.monAdditionalAmt,0)
		,monReimbursePaid=ISNULL(T1.monReimbursableExpenses,0)
		,monCommissionPaid=ISNULL(T1.monCommissionAmt,0)
		,monTotalAmountDue= (temp.decRegularHrs * monHourlyRate) + ISNULL(monReimburse,0) + ISNULL(monCommissionEarned,0) + ISNULL(monOverPayment,0) - ISNULL(monCommPaidCreditMemoOrRefund,0)
	FROM 
		#tempHrs temp  
	LEFT JOIN
	(
		SELECT
			numUserCntID
			,numDivisionID
			,monHourlyAmt
			,monAdditionalAmt
			,monReimbursableExpenses
			,monCommissionAmt
			,monTotalAmt
		FROM
			dbo.PayrollHeader PH
		INNER JOIN
			PayrollDetail PD 
		ON 
			PH.numPayrollHeaderID = PD.numPayrollHeaderID
		WHERE
			PH.numComPayPeriodID = @numComPayPeriodID
	) T1
	ON
		1 = (CASE 
				WHEN ISNULL(bitPartner,0) = 1  
				THEN (CASE WHEN temp.numDivisionID = T1.numDivisionID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN temp.numUserCntID = T1.numUserCntID THEN 1 ELSE 0 END)
			END)
		
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempHrs
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,'CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ISNULL(OpportunityItems.monAvgCost,0) WHEN 3 THEN ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))')
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,'CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ISNULL(OpportunityItems.monAvgCost,0) WHEN 3 THEN ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))')
	END

	
	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,'CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ISNULL(OpportunityItems.monAvgCost,0) WHEN 3 THEN ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,'CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ISNULL(OpportunityItems.monAvgCost,0) WHEN 3 THEN ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))')
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
/****** Object:  StoredProcedure [dbo].[USP_SaveGridColumnWidth]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveAdvanceSearchGridColumnWidth')
DROP PROCEDURE USP_SaveAdvanceSearchGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveAdvanceSearchGridColumnWidth]  
@numDomainID as numeric(9)=0,  
@numUserCntID as numeric(9)=0,
@str as text,
@numViewID int
as  
BEGIN
DECLARE @numGroupID AS NUMERIC(18,0)
SELECT @numGroupID=numGroupID FROM UserMaster WHERE numUserDetailId=@numUserCntID

declare @hDoc as int     
EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

Create table #tempTable(numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)

  insert into #tempTable (numFormId,numFieldId,bitCustom,intColumnWidth)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
  WITH (numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)                                           
     
UPDATE
	t1
SET 
	intColumnWidth=temp.intColumnWidth
FROM
	AdvSerViewConf t1
JOIN
	#tempTable temp
ON
	t1.numFormFieldID = temp.numFieldId 
WHERE
	t1.numFormId = 1
	AND temp.numFormId = 1
	AND t1.tintViewID = @numViewID
	AND t1.numDomainID = @numDomainID
	AND t1.numGroupID = @numGroupID      
	AND ISNULL(t1.bitCustom,0) = temp.bitCustom -- RIGHT NOW CUSTOM FIELDS ARE NOT AVAILABLE FOR SELECTION IN ADVANCE SEARCH GRID
	
	UPDATE 
		DFCD 
	SET 
		intColumnWidth=temp.intColumnWidth
	FROM 
		DycFormConfigurationDetails  DFCD 
	JOIN 
		#tempTable temp
	ON 
		DFCD.numFormId=temp.numFormId 
		AND DFCD.numFieldId=temp.numFieldId 
		AND isnull(DFCD.bitCustom,0)=temp.bitCustom
	WHERE 
		DFCD.numDomainID=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.tintPagetype=1
		AND temp.numFormId <> 1                                               

  drop table #tempTable                                        
 EXEC sp_xml_removedocument @hDoc  
END
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME,
	@fltReorderQty FLOAT,
	@fltReorderPoint FLOAT
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	UPDATE Item SET fltReorderQty=@fltReorderQty WHERE numItemCode=@numItemCode
	UPDATE WareHouseItems SET numReorder=@fltReorderPoint WHERE numItemID=@numItemCode


	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDoc INT    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
		SELECT 
			* 
		INTO 
			#temp 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Item',2)    
		WITH 
		( 
			numWareHouseItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,intAdjust FLOAT
			,monAverageCost DECIMAL(20,5)
		) 
		
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
		INTO 
			#tempSerialLotNO 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
		WITH 
			(
				numItemCode NUMERIC(18,0)
				,numWareHouseID NUMERIC(18,0)
				,numWareHouseItemID NUMERIC(18,0)
				,vcSerialNo VARCHAR(3000)
				,byteMode TINYINT
			) 
		
		EXEC sp_xml_removedocument @hDoc  

		DECLARE @numOnHand FLOAT
		DECLARE @numAllocation FLOAT
		DECLARE @numBackOrder FLOAT
		DECLARE @numWareHouseItemID AS NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
			
		SELECT 
			@numWareHouseItemID = MIN(numWareHouseItemID) 
		FROM 
			#temp
			
		DECLARE @bitLotNo AS BIT = 0  
		DECLARE @bitSerialized AS BIT = 0
		DECLARE @monItemAvgCost DECIMAL(20,5)
		DECLARE @monUnitCost DECIMAL(20,5)
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		DECLARE @TotalOnHand AS FLOAT;
		DECLARE @numNewQtyReceived AS FLOAT

		WHILE @numWareHouseItemID>0    
		BEGIN		  
			SET @monAvgCost = 0
			SET @TotalOnHand = 0
			SET @numNewQtyReceived = 0

			SELECT 
				@bitLotNo=ISNULL(Item.bitLotNo,0)
				,@bitSerialized=ISNULL(Item.bitSerialized,0)
				,@monItemAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost, 0) END)
				,@monUnitCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(#temp.monAverageCost, 0) END)
				,@numNewQtyReceived = ISNULL(#temp.intAdjust,0)
			FROM 
				Item 
			INNER JOIN 
				#temp 
			ON 
				item.numItemCode=#temp.numItemCode 
			INNER JOIN
				WareHouseItems
			ON
				#temp.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			WHERE 
				#temp.numWareHouseItemID=@numWareHouseItemID 
				AND Item.numDomainID=@numDomainID
				AND WareHouseItems.numDomainID=@numDomainID

			IF @bitLotNo=0 AND @bitSerialized=0
			BEGIN
				IF (SELECT 
						ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
					FROM 
						WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID 
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
				BEGIN
					SELECT @numTempItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
					--Updating the Average Cost
					SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numTempItemCode
  					SET @monAvgCost = ((@TotalOnHand * @monItemAvgCost) + (@numNewQtyReceived * @monUnitCost)) / ( @TotalOnHand + @numNewQtyReceived )

					UPDATE  
						item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
						,bintModifiedDate=GETUTCDATE()
						,numModifiedBy=@numUserCntID
					WHERE 
						numItemCode = @numTempItemCode

					IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END)
							,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END
					ELSE
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END	 

					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numTempItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = 'Inventory Adjustment', --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate = @dtAdjustmentDate,
					@numDomainId = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
				END
			END	  
				    
			SELECT TOP 1 
				@numWareHouseItemID = numWareHouseItemID 
			FROM 
				#temp 
			WHERE 
				numWareHouseItemID >@numWareHouseItemID 
			ORDER BY 
				numWareHouseItemID  
				    
			IF @@rowcount=0 
				SET @numWareHouseItemID =0    
		END    
    

		DROP TABLE #temp
    
    
		-------------Serial/Lot #s----------------
		DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
		SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
		DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
				@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
		DECLARE @posComma int, @strKeyVal varchar(20)
		    
		WHILE @minRowNo <= @maxRowNo
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
				@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
			SET @posComma=0
		
			SET @vcSerialNo=RTRIM(@vcSerialNo)
			IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

			SET @posComma=PatIndex('%,%', @vcSerialNo)
			WHILE @posComma>1
				BEGIN
					SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
					DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END		
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END
	  
				SET @numWareHouseItmsDTLID=0
				SET @OldQty=0
				SET @vcComments=''
			
				select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
						from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
						vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
				IF @byteMode=0 --Add
					SET @numQty=@OldQty + @strQty
				ELSE --Deduct
					SET @numQty=@OldQty - @strQty
			
				EXEC dbo.USP_AddUpdateWareHouseForItems
						@numItemCode = @numTempItemCode, --  numeric(9, 0)
						@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
						@numWareHouseItemID =@numWareHouseItemID,
						@numDomainID = @numDomainId, --  numeric(9, 0)
						@vcSerialNo = @strName, --  varchar(100)
						@vcComments = @vcComments, -- varchar(1000)
						@numQty = @numQty, --  numeric(18, 0)
						@byteMode = 5, --  tinyint
						@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
						@numUserCntID = @numUserCntID, --  numeric(9, 0)
						@dtAdjustmentDate = @dtAdjustmentDate
	  
				SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
				SET @posComma=PatIndex('%,%',@vcSerialNo)
			END

			SET @minRowNo=@minRowNo+1
		END
	
		DROP TABLE #tempSerialLotNO

		

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode
	END   
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityItems_OrderwiseItems' ) 
    DROP PROCEDURE USP_OpportunityItems_OrderwiseItems
GO
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_OpportunityItems_OrderwiseItems]         
 -- Add the parameters for the stored procedure here        
    @numDomainId NUMERIC(9) ,    
    @numDivisionId NUMERIC(9) ,    
    @keyword VARCHAR(50) ,    
    @startDate AS DATETIME =NULL,    
    @endDate AS DATETIME =NULL,    
    @imode AS INT ,    
    @CurrentPage INT = 0 ,    
    @PageSize INT = 0 ,    
    @TotRecs INT = 0 OUTPUT    
AS     
    BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
        SET NOCOUNT ON;        
                
	DECLARE @numCompanyID NUMERIC(18,0)
	SELECT @numCompanyID = numCompanyID FROM DivisionMaster where numDivisionID=@numDivisionId and numDomainID = @numDomainId 

    -- Insert statements for procedure here        
        IF ( @imode = 1 )     
            BEGIN        
                   
                CREATE TABLE #tempItems    
                    (    
                      Units FLOAT,    
                      BasePrice DECIMAL(18,2) ,    
                      ItemName VARCHAR(300) ,
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300),    
                      base DECIMAL(18,2) ,    
                      ChargedAmount DECIMAL(18,2) ,    
                      ORDERID NUMERIC(9) ,    
                      ModelId VARCHAR(200) ,    
                      OrderDate VARCHAR(30) ,    
                      tintOppType TINYINT ,    
                      tintOppStatus TINYINT ,    
                      numContactId NUMERIC ,    
                      numDivisionId NUMERIC ,    
                      OppId NUMERIC ,    
                      OrderNo VARCHAR(300)   ,
					  CustomerPartNo  VARCHAR(300),
					  numItemCode NUMERIc(18,0)
                    )        
                                
                INSERT  INTO #tempItems    
                        SELECT  OI.numUnitHour AS Units ,    
                                OI.monPrice AS BasePrice ,    
                                OI.vcItemName AS ItemName ,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))ELSE I.vcSKU END,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,ISNULL(I.numBarCodeId,''))ELSE I.numBarCodeId END,
                                OI.monAvgCost AS base ,    
                                OI.monTotAmount AS ChargedAmount ,    
                                Oi.numItemCode AS ORDERID ,    
                                ISNULL(oi.vcModelID, 'NA') AS ModelId ,    
                                dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,     
                                om.tintOppType ,    
                                om.tintOppStatus ,    
                                Om.numContactId ,    
                                ACI.numDivisionId ,    
                                Om.numOppId AS OppId ,    
								 Om.vcPOppName AS OrderNo ,
								 ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo,
								 OI.numItemCode
                        FROM    dbo.OpportunityItems AS OI    
								INNER JOIN Item I ON OI.numItemCode = I.numItemCode
                                INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId    
                                INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId   
								LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
								LEFT JOIN  CustomerPartNumber CPN  ON OI.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   Om.numDomainId = @numDomainId    
                                AND ACI.numDivisionId = @numDivisionId    
                                AND ( OI.vcItemName LIKE @keyword + '%'    
                                      OR OI.vcModelID LIKE @keyword + '%'    
									  OR CPN.CustomerPartNo LIKE '%' + @keyword + '%'    
      )    
                                AND ( OM.bintCreatedDate >= ISNULL(@startDate,OM.bintCreatedDate)    
                                      AND Om.bintCreatedDate <=ISNULL(@endDate,OM.bintCreatedDate)     
                                    )         
                                            
                DECLARE @firstRec AS INTEGER          
                DECLARE @lastRec AS INTEGER          
          
                SET @firstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @lastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempItems    
                               )          
                   
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempItems    
                        ) a    
                WHERE   RowNumber > @firstRec    
                        AND RowNumber < @lastRec    
                ORDER BY ItemName         
                            
                     
                DROP TABLE #tempItems          
            END        
                   
        ELSE     
            BEGIN       
                   
                CREATE TABLE #tempVItems    
                    (    
					  numDivisionID NUMERIC(18,0),
                      ItemName VARCHAR(300) ,   
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300), 
                      MinOrderQty INT ,    
                      MinCost DECIMAL(18,2) ,    
                      ModelId VARCHAR(200) ,    
                      PartNo VARCHAR(100) ,    
                      numItemCode NUMERIC    ,
					  CustomerPartNo  VARCHAR(100)  
                    )        
                INSERT  INTO #tempVItems    
                        SELECT  @numDivisionId,
								i.vcItemName AS ItemName,
								ISNULL(I.vcSKU,''),
								ISNULL(I.numBarCodeId,''),
                                v.intMinQty AS MinOrderQty,
                                v.monCost AS MinCost,
                                I.vcModelID AS ModelId,
                                v.vcPartNo AS PartNo,
                                v.numItemCode AS numItemCode,
								ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo     
                        FROM    dbo.Item AS I    
                                INNER JOIN dbo.Vendor AS V ON I.numVendorID = V.numVendorID    
								LEFT JOIN  CustomerPartNumber CPN  ON I.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   i.numVendorID = @numDivisionId    
                                AND v.numItemCode = i.numItemCode    
                                AND v.numDomainID = @numDomainId        
                              AND ( I.vcItemName LIKE @keyword + '%'    
                                      OR I.vcModelID LIKE @keyword + '%'    
                                    )    
                 
                 
                DECLARE @VfirstRec AS INTEGER          
                DECLARE @VlastRec AS INTEGER          
          
                SET @VfirstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @VlastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempVItems    
                               )          
                     
                            
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempVItems    
                        ) a    
                WHERE   RowNumber > @VfirstRec    
                        AND RowNumber < @VlastRec    
                ORDER BY ItemName               
                            
                    
                DROP TABLE #tempVItems          
                 
                 
            END           
                    
                    
                      
    END 


