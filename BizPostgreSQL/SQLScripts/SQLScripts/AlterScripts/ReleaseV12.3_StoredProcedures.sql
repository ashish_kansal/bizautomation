/******************************************************************
Project: Release 12.3 Date: 05.AUG.2019
Comments: STORE PROCEDURES
*******************************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging')
DROP PROCEDURE USP_GetAccountReceivableAging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
	@numDomainId   AS NUMERIC(9)  = 0,
	@numDivisionId AS NUMERIC(9)  = NULL,
	@numUserCntID AS NUMERIC(9),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME = NULL,
	@dtToDate AS DATETIME = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
  BEGIN
	SET NOCOUNT ON

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
				OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = Opp.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= CAST(@dtToDate AS DATE)
				) TablePayments
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,OB.[dtCreatedDate]) <= @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate
				,TablePayments.monPaidAmount
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON [DM].[numDivisionID] = DIV.[numDivisionID]
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numCurrentDays],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intCurrentDaysCount],
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numCurrentDaysPaid],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
				0 numCurrentDays,
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
				0 [intCurrentDaysCount],
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numCurrentDaysPaid,
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

UPDATE  TempARRecord1
SET     numCurrentDays = X.numCurrentDays
FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numCurrentDaysPaid = X.numCurrentDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate])>= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
    FROM      [TempARRecord]
    WHERE     numUserCntID =@numUserCntID AND 
			 DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
			  AND 
			  numCurrencyID <>@baseCurrency )

---------------------------------------------------------------------------------------------


-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=  isnull(numCurrentDays,0) + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numCurrentDays],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intCurrentDaysCount],
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numCurrentDaysPaid],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

SET NOCOUNT OFF   
    
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Details]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Details')
DROP PROCEDURE USP_GetAccountReceivableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Details]
(
    @numDomainId AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
  BEGIN
	DECLARE @strSql VARCHAR(8000);
	Declare @strSql1 varchar(8000);
	Declare @strCredit varchar(8000); SET @strCredit=''
	
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
    IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    --Get Master data
	 
	 SET @strSql = '
     SELECT DISTINCT OM.[numOppId],
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.[vcPOppName],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 ,0) TotalAmount,--OM.[monPAmount]
                        (ISNULL(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        isnull(OB.monDealAmount
                                 
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     ,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],
                        CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.[numDivisionId]
        FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0'
    
    	--Show Impact of AR journal entries in report as well 
    	
SET @strSql1 = '    	
		UNION 
		 SELECT 
				-1 AS [numOppId],
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' [vcPOppName],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''				
    
   SET @strCredit = ' UNION SELECT -1 AS [numOppId],
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
								0 [numOppBizDocsId],
								'''' [vcBizDocID],
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' [varCurrSymbol],
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)'  
    
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'

		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'
                             
                    END
                  ELSE
                    BEGIN
                      SET @strSql =@strSql +''
                    END
                    



	SET @strSql =@strSql + @strSql1 + @strCredit
	PRINT @strSql
	EXEC(@strSql)
  END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailMergeData' ) 
    DROP PROCEDURE USP_GetEmailMergeData 
GO
CREATE PROCEDURE USP_GetEmailMergeData
    @numModuleID NUMERIC,
    @vcRecordIDs VARCHAR(8000) = '', -- Seperated by comma
    @numDomainID NUMERIC,
    @tintMode TINYINT = 0,
    @ClientTimeZoneOffset Int,
	@numOppBizDocId NUMERIC = 0
AS 
BEGIN
	
    IF (@numModuleID = 1  or @numModuleID = 0)
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT TOP 1 vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    --vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    --+ ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                    --                             '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    --+ ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                    --                   '') AS OrgShippingAddress ,	
									   
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					WHERE numContactId=ACI.numContactId AND bitIsDefault = 1 
					ORDER BY CC.bitIsDefault DESC) AS PrimaryCreditCardNo,												   		
                  
				   (select TOP 1 U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature]
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
				
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
    IF @numModuleID = 2 
        BEGIN
             SELECT 
                    dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ([dbo].[getdealamount](OM.numOppId,Getutcdate(),0)), 1) )) AS OppOrderSubTotal,
					vcPOppName OpportunityName,
					dbo.Fn_getcontactname(OM.numContactId) OppOrderContact,					
					ISNULL(OM.vcOppRefOrderNo,'') AS OppOrderCustomerPO,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30),ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderInvoiceGrandtotal,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderTotalAmountPaid,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCountry,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCountry,
					(CASE WHEN ISNULL(OM.intUsedShippingCompany,0) = 0 THEN '' 
					ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(OM.intUsedShippingCompany,0)),'') END) AS OppOrderShipVia,
					ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = OM.numShippingService),'') AS OppOrderShippingService,
					ISNULL(OM.txtComments, '') AS OppOrderComments,
					ISNULL(OM.dtReleaseDate,'') AS OppOrderReleaseDate,
					ISNULL(OM.intpEstimatedCloseDate,'') AS OppOrderEstimatedCloseDate,
					dbo.fn_GetOpportunitySourceValue(ISNULL(OM.tintSource,0),ISNULL(OM.tintSourceType,0),OM.numDomainID) AS OppOrderSource,
					--ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=OM.numOppID FOR XML PATH('')),4,200000)),'') AS OppOrderTrackingNo,				

					 (SELECT  
						SUBSTRING(
									(SELECT '$^$' + 
									(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
										WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
									) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
									FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= OM.numOppID and ISNULL(OBD.numShipVia,0) <> 0
						FOR XML PATH('')),4,200000)
						
					 ) AS  OppOrderTrackingNo,					
					
					(SELECT U.txtSignature FROM UserMaster U where U.numUserDetailId=OM.numContactId) as [Signature],

					--(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
					--	INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					--	LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					--	WHERE CC.numContactId=OM.numContactId AND bitIsDefault = 1 
					--	ORDER BY CC.bitIsDefault DESC
					--) AS PrimaryCreditCardNo

					(CASE WHEN @numOppBizDocId > 0 AND EXISTS (SELECT numTransHistoryID FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId) THEN (SELECT vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId)
							ELSE ISNULL((SELECT TOP 1 vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppID = OM.numOppId),'')
						END
					) AS PrimaryCreditCardNo,
					
					ISNULL(CMP.vcCompanyName , '') AS OppOrderAccountName

            FROM    dbo.OpportunityMaster OM
					LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = OM.numCurrencyID
					LEFT JOIN DivisionMaster DM
					ON  OM.numDivisionId = DM.numDivisionID
					LEFT JOIN CompanyInfo CMP
					ON DM.numCompanyID = CMP.numCompanyId					
            WHERE   OM.numDomainID = @numDomainID
                   AND OM.numOppId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )

        END
    IF @numModuleID = 4 
        BEGIN        
            SELECT  vcProjectID ProjectID,
                    vcProjectName ProjectName,
                    dbo.fn_getContactName(numAssignedTo) ProjectAssigneeName,
                    dbo.FormatedDateFromDate(intDueDate, numDomainId) ProjectDueDate
            FROM    dbo.ProjectsMaster
            WHERE   numDomainID = @numDomainID
                    AND numProID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 5 
        BEGIN        
            SELECT  vcCaseNumber CaseNo,
                    textSubject AS CaseSubject,
                    dbo.fn_getContactName(numAssignedTo) CaseAssigneeName,
                    dbo.FormatedDateFromDate(intTargetResolveDate, numDomainID) CaseResoveDate
            FROM    dbo.Cases
            WHERE   numDomainID = @numDomainID
                    AND numCaseId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 6 
        BEGIN        
            SELECT  textDetails TicklerComment,
                    dbo.GetListIemName(bitTask) TicklerType,
                    dbo.fn_getContactName(numAssign) TicklerAssigneeName,
                    dbo.FormatedDateFromDate(dtStartTime, C.numDomainID) TicklerDueDate,
                    dbo.GetListIemName(numStatus) TicklerPriority,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ), C.numDomainID) TicklerStartTime,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ), C.numDomainID) TicklerEndTime,
                    dbo.GetListIemName(bitTask) + ' - ' + dbo.GetListIemName(numStatus) AS TicklerTitle,
                    dbo.fn_getContactName(C.numCreatedBy) TicklerCreatedBy,
					CMP.vcCompanyName OrganizationName
                   
                    
            FROM    dbo.Communication C
			LEFT JOIN DivisionMaster DM
			ON C.numDivisionId = DM.numDivisionID
			LEFT JOIN CompanyInfo CMP
			ON DM.numCompanyId = CMP.numCompanyId
            WHERE   C.numDomainID = @numDomainID
                    AND numCommId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 8 
        BEGIN
            SELECT  ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					C.vcCompanyName OrganizationName,
					dbo.GetListIemName(OB.numBizDocId) BizDoc,
                   OB.monDealAmount BizDocAmount,
                    OB.monAmountPaid BizDocAmountPaid,
                    ( OB.monDealAmount - OB.monAmountPaid ) BizDocBalanceDue,
                    dbo.GetListIemName(ISNULL(OB.numBizDocStatus, 0)) AS BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)  AS BizDocBillingTermsNetDays,
                    dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+   OB.vcBizDocID BizDocID,
                  '
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 	                                   
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 	
                                THEN 	
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms,

						  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 
											 THEN ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)
											 ELSE 0 
										END,OB.dtCreatedDate) AS BizDocDueDate,										
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,OB.numOppBizDocsID,
					isnull(OB.vcTrackingNo,'') AS  vcTrackingNo,
					ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OB.numShipVia  AND vcFieldName='Tracking URL')),'') AS vcTrackingURL,
					'' As BizDocTemplateFromGlobalSettings

					--OM.vcPOppName OpportunityName,
     --               dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName
            FROM    dbo.OpportunityBizDocs OB
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = OB.numOppID
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
            WHERE   OM.numDomainID = @numDomainID
                    AND OB.numOppBizDocsID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 	
        IF @numModuleID = 9 
        BEGIN
            SELECT  
                   ISNULL(TempBizDoc.vcBizDocID,'') BizDocID,
                   ISNULL(TempBizDoc.monDealAmount,0) BizDocAmount,
                   ISNULL(TempBizDoc.monAmountPaid,0) BizDocAmountPaid,
                   ISNULL(TempBizDoc.BizDocBalanceDue,0) BizDocBalanceDue,
				   ISNULL(TempBizDoc.BizDocStatus,'') BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0) AS BizDocBillingTermsNetDays,
                   -- dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+'
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1  		
                                THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms       		
                                    ,
                        --  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) ELSE 0 END,OB.dtCreatedDate) AS BizDocDueDate,
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail
            FROM    dbo.OpportunityMaster OM 
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
					OUTER APPLY
					(
						SELECT TOP 1
							vcBizDocID,
							monDealAmount,
							monAmountPaid,
							(monDealAmount - monAmountPaid) AS BizDocBalanceDue,
							dbo.GetListIemName(ISNULL(numBizDocStatus, 0)) AS BizDocStatus
						FROM
							OpportunityBizDocs 
						WHERE 
							numOppId=OM.numOppID ANd ISNULL(bitAuthoritativeBizDocs,0) = 1
					) AS TempBizDoc
            WHERE   OM.numDomainID = @numDomainID
                    AND OM.numOppID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 
        
        IF @numModuleID = 11 
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.vcProfile,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                                 '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                                       '') AS OrgShippingAddress 
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=1 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END

		 IF @numModuleID = 45
        BEGIN        
            SELECT  C.vcCompanyName OrganizationName,
					(select vcdata from listdetails where numListItemID = C.vcProfile) AS OrgProfile, 		   
					(select A.vcFirstName+' '+A.vcLastName                       
					 from AdditionalContactsInformation A                  
					 join DivisionMaster D                        
					 on D.numDivisionID=A.numDivisionID
					 where A.numContactID=DM.numAssignedTo) AS OrgAssignto,	
					 ISNULL(AD1.vcAddressName,'') AS OrgBillingName,
					 ISNULL(AD1.vcStreet,'') AS OrgBillingStreet,
					 ISNULL(AD1.VcCity,'') AS OrgBillingCity,
					 ISNULL(dbo.fn_GetState(AD1.numState),'') AS OrgBillingState,
					 ISNULL(AD1.vcPostalCode,'') AS OrgBillingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD1.numCountry),'') AS OrgBillingCountry,
					 ISNULL(AD2.vcAddressName,'') AS OrgShippingName,
					 ISNULL(AD2.vcStreet,'') AS OrgShippingStreet,
					 ISNULL(AD2.VcCity,'') AS OrgShippingCity,
					 ISNULL(dbo.fn_GetState(AD2.numState),'') AS OrgShippingState,
					 ISNULL(AD2.vcPostalCode,'') AS OrgShippingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD2.numCountry),'') AS OrgShippingCountry,
					 (select vcdata from listdetails where numListItemID = C.numCompanyCredit) AS  OrgCreditLimit,
					 (select (CAST(BT.vcTerms AS VARCHAR(100) ) + ' (' + CAST(BT.numNetDueInDays AS VARCHAR(10)) + ',' + CAST(BT.numDiscount AS VARCHAR(10)) + ',' +  CAST(BT.numDiscountPaidInDays AS VARCHAR(10)) + ')' )) AS OrgNetTerms,
					 (select vcdata from listdetails where numListItemID = DM.numDefaultPaymentMethod) AS  OrgPaymentMethod,
					 (select vcdata from listdetails where numListItemID = DM.intShippingCompany) AS  OrgPreferredShipVia,
					 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS OrgPreferredParcelShippingService,
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
						WHERE numContactId=ACI.numContactId AND bitIsDefault = 1
						ORDER BY CC.bitIsDefault DESC
					) AS PrimaryCreditCardNo,	
			
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,
					ACI.numPhone ContactPhone,
					ACI.numPhoneExtension ContactPhoneExt,
					ACI.numCell ContactCellPhone,					
					 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=ACI.numContactId),'') AS ContactEcommercepassword,					
					(select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) AS [Signature],
					dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
					LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
        	
--select * from dbo.OpportunityBizDocs	
--    SELECT TOP 1 bitInterestType,
--            *
--    FROM    dbo.OpportunityBizDocs ORDER BY numoppbizdocsid DESC 
END
--exec USP_GetEmailMergeData @numModuleID=6,@vcRecordIDs='18463',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
--exec USP_GetEmailMergeData @numModuleID=1,@vcRecordIDs='1',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
/****** Object:  StoredProcedure [dbo].[usp_GetImapEnabledUsers]    Script Date: 06/04/2009 15:11:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetImapEnabledUsers')
DROP PROCEDURE usp_GetImapEnabledUsers
GO
CREATE PROCEDURE [dbo].[usp_GetImapEnabledUsers]
@tinyMode AS TINYINT=0,
@numDomainId AS NUMERIC(9)=0 
AS 
IF @tinyMode=0
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I
	join Subscribers S on S.numTargetDomainId=I.numDomainId WHERE [bitImap] = 1 and S.bitActive=1 
  
ELSE IF @tinyMode=1 --Get users to whom we need to send notification mail about error in last 10 attempt.
  SELECT DISTINCT I.[numDomainId],[numUserCntId],ISNULL(UM.vcEmailID,'') vcEmailID FROM [ImapUserDetails] I
	JOIN dbo.UserMaster UM ON UM.numUserDetailId = I.numUserCntId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
  WHERE [bitImap] = 0 AND bitNotified=0 AND tintRetryLeft=0 and S.bitActive=1

ELSE IF @tinyMode=2  --Google to Biz Contact Distinct Domain 
  SELECT DISTINCT I.[numDomainId] FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 and S.bitActive=1

ELSE IF @tinyMode=3 --Google to Biz Calendar
   SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0

ELSE IF @tinyMode=4 --Biz to Google Calendar
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitBtoGCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0

ELSE IF @tinyMode=5  --Google to Biz Contact Domain 
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

ELSE IF @tinyMode=6  --Biz to Google Email
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

  
  
  

 
  

  


GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@columnName as Varchar(MAX),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0,
	@tintOppStatus TINYINT = 0                                                           
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(200)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'numPartnerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'tintOppStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE ISNULL(Opp.tintOppStatus,0) WHEN 1 THEN ''Deal Won'' WHEN 2 THEN ''Deal Lost'' ELSE ''Open'' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

    DECLARE @CustomControlType AS VARCHAR(10)      
	IF @columnName like 'CFW.Cust%'             
	BEGIN

		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' ' 
		
		SET @CustomControlType = (SELECT Fld_type FROM CFW_Fld_Master CFWM where CFWM.Fld_id = REPLACE(@columnName,'CFW.Cust',''))

		IF @CustomControlType = 'DateField'
		BEGIN
			 SET @columnName='CAST((CASE WHEN CFW.Fld_Value = ''0'' THEN NULL ELSE CFW.Fld_Value END) AS Date)'   
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'   
		END
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = CONCAT(@strSql,' WHERE Opp.tintOppstatus=',ISNULL(@tintOppStatus,0),' AND DM.numDomainID=',@numDomainID,' AND Opp.tintOppType= ',@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END    
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql, ' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC usp_GetRecentItems 1 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecentitems')
DROP PROCEDURE usp_getrecentitems
GO
CREATE PROCEDURE [dbo].[usp_GetRecentItems]  
--declare    
 @numUserCntID as numeric(9)    
--set @numUserCntID=1     
as

IF @numUserCntID = 0
BEGIN
	--get blank table to avoid error
	select 0 numRecId 
	from communication    
	where  1=0
	RETURN
END
	

declare @v1 int
if exists(select  ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID))
	begin
		select @v1 = ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID)
	end
else
	begin
		set @v1=10
	end


set rowcount @v1;

DECLARE @TEMPTABLE TABLE
(
	numRecordId numeric(18,0),
	bintvisiteddate DATETIME,
	numRecentItemsID numeric(18,0),
	chrRecordType char(10)
)


INSERT INTO 
	@TEMPTABLE
SELECT DISTINCT
	numrecordId,
	bintvisiteddate,
	numRecentItemsID,
	chrRecordType	
FROM    
	RECENTITEMS
WHERE   
	numUserCntId = @numUserCntID
	AND bitdeleted = 0
ORDER BY 
	bintvisiteddate DESC
set rowcount 0

SELECT  DM.numDivisionID AS numRecID ,isnull(CMP.vcCompanyName,'' ) as RecName,                           
     DM.tintCRMType,                             
     CMP.numCompanyID AS numCompanyID,                                           
     ADC.numContactID AS numContactID,          
     DM.numDivisionID,'C'   as Type,bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	 0 as numOppID,
	'~/images/Icon/organization.png' AS vcImageURL
    FROM  CompanyInfo CMP                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
    LEFT join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1         
    join @TEMPTABLE R on R.numRecordID=DM.numDivisionID                                             
    WHERE chrRecordType='C'    
union     
         
SELECT  DM.numDivisionID AS numRecID,    
isnull(ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'),'') as RecName,                     
     DM.tintCRMType,                             
     CMP.numCompanyID AS numCompanyID,                                           
     ADC.numContactID AS numContactID,          
     DM.numDivisionID,'U',bintvisiteddate     ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	 0 as numOppID,
	 '~/images/Icon/contact.png' AS vcImageURL                
    FROM  CompanyInfo CMP                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
    join @TEMPTABLE R on numRecordID=ADC.numContactID   where chrRecordType='U'                                         
    
    
union           
    
select  numOppId as numRecID,isnull(vcPOppName,'') as RecName,          
0,0,0,numDivisionID,'O',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
0 as numOppID,
CASE 
	WHEN tintOppType=1 AND tintOppStatus=0 THEN '~/images/Icon/Sales opportunity.png'
	WHEN tintOppType=1 AND tintOppStatus=2 THEN '~/images/Icon/Sales opportunity.png'
	WHEN tintOppType=1 AND tintOppStatus=1 THEN '~/images/Icon/Sales order.png'
	WHEN tintOppType=2 AND tintOppStatus=0 THEN '~/images/Icon/Purchase opportunity.png'
	WHEN tintOppType=2 AND tintOppStatus=1 THEN '~/images/Icon/Purchase order.png'
ELSE '~/images/icons/cart.png' 
END AS vcImageURL from OpportunityMaster          
join @TEMPTABLE  on numRecordID=numOppId    where  chrRecordType='O'      
    
          
union       
        
select  numProId as numRecID,isnull(vcProjectName,'') as RecName,          
0,0,0,numDivisionID,'P',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/project.png' AS vcImageURL from ProjectsMaster          
join @TEMPTABLE  on numRecordID=numProId  where  chrRecordType='P'    
       
    
union    
           
select  numCaseId as numRecID,    
isnull(vcCaseNumber,'')  as RecName,          
0,0,0,numDivisionID,'S',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/case.png' AS vcImageURL  from Cases          
join @TEMPTABLE  on numRecordID=numCaseId   where  chrRecordType='S'      
    
union    
    
select numCommId as numRecId,    
isnull(    
case when bitTask =971 then 'C - '    
  when bitTask =972 then 'T - '    
  when bitTask =973 then 'N - '    
  when bitTask =974 then 'F - '   
 else convert(varchar(2),dbo.fn_GetListItemName(bitTask))  
end + CONVERT(VARCHAR(100),textDetails),'')    
as RecName,0,0,0,numDivisionID,'A',bintvisiteddate     
 , caseId, caseTimeId, caseExpId,0 as numOppID,'~/images/Icon/action.png' AS vcImageURL  
from communication    
join @TEMPTABLE  on numRecordID=numCommId   where  chrRecordType='A'    
    
    
    union    
        

select  numItemCode as numRecID,    
isnull(vcItemName,'') as RecName, 
0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='I'  

  union    
           
select  numItemCode as numRecID,    
isnull(vcItemName,'') as RecName, 
0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='AI'  
                                    
union    
           
select  numGenericDocId as numRecID,    
isnull(vcDocName,'')  as RecName,          
0,0,0,0,'D',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/Doc.png' AS vcImageURL  from GenericDocuments          
join @TEMPTABLE  on numRecordID=numGenericDocId   where  chrRecordType='D'      

union 

select  numCampaignId as numRecID,isnull(vcCampaignName,'') as RecName,          
0,0,0,0 AS numDivisionID,'M',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/campaign.png' AS vcImageURL from dbo.CampaignMaster        
join @TEMPTABLE  on numRecordID=numCampaignId    where  chrRecordType='M'      
    
union 

select  numReturnHeaderID as numRecID,isnull(vcRMA,'') as RecName,          
0,0,0,numDivisionID,'R',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID, CASE 
																										WHEN tintReturnType=1 THEN '~/images/Icon/Sales Return.png'
																										WHEN tintReturnType=2 THEN '~/images/Icon/Purchase Return.png'
																										WHEN tintReturnType=3 THEN '~/images/Icon/CreditMemo.png'
																										WHEN tintReturnType=4 THEN '~/images/Icon/Refund.png'
																									ELSE 
																										'~/images/Icon/ReturnLastView.png' 
																									END AS vcImageURL from ReturnHeader          
join @TEMPTABLE  on numRecordID=numReturnHeaderID  where  chrRecordType='R'   

UNION

SELECT
	numOppBizDocsId as numRecID,
	isnull(vcBizDocID,'') as RecName,
	0,0,0,0 AS numDivisionID,'B',bintvisiteddate,0 as caseId,0 as caseTimeId,0 as caseExpId,OpportunityMaster.numOppId as numOppID,
	(CASE 
		WHEN OpportunityMaster.tintOppType = 1 THEN '~/images/Icon/Sales BizDoc.png' 
		WHEN OpportunityMaster.tintOppType = 2 THEN '~/images/Icon/Purchase BizDoc.png' 
		ELSE 
			'~/images/Icon/Doc.png' 
	END) AS vcImageURL
FROM
	OpportunityBizDocs
JOIN
	OpportunityMaster
ON
	OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
JOIN
	@TEMPTABLE
ON
	numOppBizDocsId = numRecordId
WHERE
	chrRecordType = 'B'

order by bintVisitedDate desc     


--,Y.numRecentItemsID ORDER BY Y.numRecentItemsID DESC     
--,Y.bintVisitedDate    
--

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL,
@dtSelectedDate AS DATE =NULL,
@bitExcludeEmailFromNonBizContact BIT = 0,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)='' ,
 @intContactFilterType AS INT=0,
 @vcFollowupStatusIds AS VARCHAR(600)='',
 @numRelationShipId AS NUMERIC(18,0)=0,
 @numProfileId AS NUMERIC(18,0)=-0,
 @bitGroupBySubjectThread AS BIT=0,
 @vcSortOrder AS VARCHAR(500)=''
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @bitAllowFiltering AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END          
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)  
DECLARE @vcSorting AS VARCHAR(500)=''   
IF ISNULL(@bitGroupBySubjectThread,0)=1
BEGIN
	SET @vcSorting=@vcSorting +' ORDER BY EH.dtReceivedOn,EH.vcSubject '+@columnSortOrder+' '
END
ELSE
BEGIN
	SET @vcSorting=@vcSorting +' ORDER BY EH.dtReceivedOn  '+@columnSortOrder+' '
END
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '               
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.dtReceivedOn '+@columnSortOrder+' ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,EH.vcSubject,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' FROM [EmailHistory] EH JOIN EmailMaster EM on EM.numEMailId=(EH.numEmailId) LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = EM.numContactId
	left JOIN DivisionMaster DMI ON ACI.numDivisionId = DMI.numDivisionID
	 left JOIN CompanyInfo CIM ON DMI.numCompanyID = CIM.numCompanyId '
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId) 

IF LEN(ISNULL(@srch,'')) = 0 AND @dtSelectedDate IS NOT NULL AND LEN(@dtSelectedDate) > 0
BEGIN
	SET @strSql = @strSql + ' AND CAST(DATEADD(MINUTE,'+ CAST(@ClientTimeZoneOffset AS VARCHAR) +' * -1,EH.dtReceivedOn) AS DATE) = '''+ CONVERT(VARCHAR,@dtSelectedDate) + '''' + ' ' 
END

SET @strSql = @strSql + ' AND chrSource IN(''B'',''I'') '

IF ISNULL(@bitExcludeEmailFromNonBizContact,0) = 1
BEGIN
	SET @strSql = @strSql + ' AND ISNULL(EM.numContactId,0) > 0 '
END
IF ISNULL(@intContactFilterType,0) = 1
BEGIN
SET @strSql = @strSql + ' AND ISNULL(EM.numContactId,0) > 0 '
END
IF ISNULL(@intContactFilterType,0) = 2
BEGIN
SET @strSql = @strSql + ' AND ISNULL(EM.numContactId,0) = 0 '
END
IF LEN(@vcFollowupStatusIds)>0 AND ISNULL(@vcFollowupStatusIds,'0')<>'0'
BEGIN
	SET @strSql=@strSql + ' AND DMI.numFollowUpStatus IN(SELECT Items FROM Split('''+CAST(@vcFollowupStatusIds AS VARCHAR(500))+''','','') WHERE LEN(Items)>0 AND ISNULL(Items,0)<>''0'') '
END
IF ISNULL(@numRelationShipId,0)>0
BEGIN
 SET @strSql=@strSql+' AND CIM.numCompanyType='''+CAST(@numRelationShipId AS VARCHAR(500))+''' '
END
IF ISNULL(@numProfileId,0)>0
BEGIN
 SET @strSql=@strSql+' AND CIM.vcProfile='''+CAST(@numProfileId AS VARCHAR(500))+''' '
END
--Simple Search for All Node
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
IF(ISNULL(@bitGroupBySubjectThread,0)=0)
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND EH.numNodeId= '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
IF ISNULL(@bitGroupBySubjectThread,0)=1
BEGIN
	SET @strSql=@strSql + ' AND EH.numNodeId IN(SELECT numNodeId FROM InboxTreeSort WHERE numUserCntID='+CAST(@numUserCntId AS VARCHAR(500))+' AND numDomainID='+CAST(@numDomainId AS VARCHAR(500))+' AND vcNodeName IN(''Inbox'',''Sent Mail'')) AND
		
	 (SELECT COUNT(numEmailHstrID) FROM EmailHistory AS tempEmails WHERE (tempEmails.vcSubject=EH.vcSubject OR REPLACE(tempEmails.vcSubject,''Re: '','''')=REPLACE(EH.vcSubject,''Re: '','''')) 
	 AND tempEmails.numEmailHstrID<>EH.numEmailHstrID AND tempEmails.vcTo=EH.vcFrom AND numDomainID='''+CAST(@numDomainId AS VARCHAR(500))+''' AND numUserCntId='''+CAST(@numuserCntId AS VARCHAR(500))+''')>0 '
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcFrom LIKE ''%' + REPLACE(@srchFrom,',','%'' OR EH.vcFrom LIKE ''%') + '%'')'
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcTo LIKE ''%' + REPLACE(@srchTo,',','%'' OR EH.vcTo LIKE ''%') + '%'')' 
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF (LEN(@FromDate) > 0 AND  ISDATE(CAST(@FromDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >= '''+ CONVERT(VARCHAR,@FromDate) + ''''
END

IF (LEN(@ToDate) > 0 AND ISDATE(CAST(@ToDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND EH.numEmailHstrID IN(SELECT numEmailHstryId FROM EmailNodeRelationship WHERE numNodeId= '+ CONVERT(VARCHAR,@numNodeID) +' AND EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId) +') '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND EH.numEmailHstrID IN(SELECT numEmailHstryId FROM EmailNodeRelationship WHERE numNodeId= '+ CONVERT(VARCHAR,@numNodeID) +' AND EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId) +') '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria

SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowFiltering BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 AS bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>140 THEN SUBSTRING(EH.vcBodyText,0,140) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText ,isnull(EH.IsReplied,0) As IsReplied,FR.numContactId,EH.dtReceivedOn,CIM.vcCompanyName,EH.vcFrom,EH.vcTo,EH.vcSubject,
		 isnull(EH.numNoofTimes,0) AS numNoofTimes '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowFiltering=bitAllowFiltering,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
					+ '~' + @bitAllowFiltering
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN                                                         
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
					+ '~' + @bitAllowFiltering

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                  
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowFiltering=bitAllowFiltering,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

SET @strSql = @strSql + ', '+'EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image]'

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
--SET @strSql = @strSql + ', '+ ISNULL('EH.numNoofTimes',0) +' AS NoOfTimeOpened From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
	left JOIN DivisionMaster DMI ON ACI.numDivisionId = DMI.numDivisionID                                                                            
 left JOIN CompanyInfo CIM ON DMI.numCompanyID = CIM.numCompanyId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <' + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,07-Dec-13>
-- Description:	<Description,,Fetch New MailBox>
-- =============================================

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxTreeSort_NewSortOrder' ) 
    DROP PROCEDURE USP_InboxTreeSort_NewSortOrder
GO
CREATE PROCEDURE USP_InboxTreeSort_NewSortOrder
   	-- Add the parameters for the stored procedure here
	@numDomainID int,
	@numUserCntID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
	BEGIN

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem)
		VALUES     (NULL,NULL,@numDomainID,@numUserCntID,1,1)
				
		DECLARE @lastParentid INT
		SELECT @lastParentid=@@identity FROM InboxTreeSort

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Inbox',@numDomainID,@numUserCntID,2,1,1)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Email Archive',@numDomainID,@numUserCntID,3,1,2)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Sent Messages',@numDomainID,@numUserCntID,4,1,4)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Calendar',@numDomainID,@numUserCntID,5,1,5)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Custom Folders',@numDomainID,@numUserCntID,6,1,6)

		--ALL RECORDS ARE ALREADY REPLACED WITH MASTER SCRIPT TO APPROPROATE ID FOR USERCNTID BUT
		--THERE ARE SOME RECORDS IN EMAIL HISTORY TABLE FOR SOME USERS WHOSE CORRESPONDING TREE STRUCTURE IS NOT AVILABLE IN INBOXTREESORT TABLE
		--FOLLOWING SCRIPTS UPDATED NODEID IN EMAIL HISTORY TABLE FOR INBOX=0, SENT EMAILS=4 AND DELETED MAILS=2 INCASE EXISTS BEFORE TREE STRUCTURE CREATED
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4),4) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 4 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2),2) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 2 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1),0) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 0 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1))
	END

	DECLARE @TMEP TABLE
	(
		numNodeID int,
		numParentId int,
		vcNodeName varchar(100),
		numSortOrder int,
		bitSystem bit,
		numFixID numeric(18,0),
		bitHidden BIT
	)
	INSERT INTO 
		@TMEP
	SELECT 
		numNodeID,
		CASE WHEN numParentId=0 THEN NULL ELSE numParentID END AS numParentID,
		vcNodeName as vcNodeName,
		numSortOrder, 
		bitSystem,
		numFixID,
		bitHidden
	FROM 
		InboxTreeSort 
	WHERE 
		(numdomainid=@numDomainID) AND (numUserCntId=@numUserCntID )
	ORDER BY 
		numSortOrder asc

	DECLARE @numParentNodeID AS NUMERIC(18,0)
	SELECT @numParentNodeID = numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numParentID,0) = 0 AND ISNULL(vcNodeName,'')=''

	UPDATE @TMEP SET numParentId = NULL WHERE numParentID =@numParentNodeID
	UPDATE @TMEP SET numParentId = @numParentNodeID WHERE numParentID IS NULL AND numNodeID<>@numParentNodeID


	--DELETE FROM @TMEP WHERE numNodeID = @numParentNodeID

	SELECT * FROM @TMEP ORDER BY numSortOrder ASC
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT,
	@numFilteredWarehouseID AS NUMERIC(18,0)=0,
	@vcAsile AS VARCHAR(500)='',
	@vcRack AS VARCHAR(500)='',
	@vcShelf AS VARCHAR(500)='',
	@vcBin AS VARCHAR(500)='',
	@bitApplyFilter As BIT=0
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,I.vcItemName,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC        
		IF((SELECT COUNt(*) FROM #tempForm WHERE vcDbColumnName='StockQtyCount'  OR  vcDbColumnName='StockQtyAdjust')  >0)
		BEGIN
		IF ISNULL(@numFilteredWarehouseID,0)=0
		BEGIN
			SET @strColumns = @strColumns+ ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId'
		END   
		ELSE IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strColumns = @strColumns+ ',WRF.numOnHand as numIndividualOnHandQty,(WRF.numOnHand + WRF.numAllocation) as numAvailabilityQty,I.monAverageCost,WRF.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId '
		END  
		END
		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyCount'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyAdjust'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'     
					                                             
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)=0
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', WIL.vcLocation) FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)>0
				BEGIN
					SET @strColumns = @strColumns + ', WFL.vcLocation AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  
		
		DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		IF(ISNULL(@numFilteredWarehouseID,0)>0)
		BEGIN
			SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
			SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
			SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
			IF(ISNULL(@vcAsile,'')<>'')
			BEGIN
				SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
			END
			IF(ISNULL(@vcRack,'')<>'')
			BEGIN
				SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
			END
			IF(ISNULL(@vcShelf,'')<>'')
			BEGIN
				SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
			END
			IF(ISNULL(@vcBin,'')<>'')
			BEGIN
				SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
			END
		END
		
		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						'+ @vcApplyRackFilter+ '
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                      
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		SET @strWhere=@strWhere+@vcApplyRackFilterSearch
		DECLARE @strFinal AS NVARCHAR(MAX)=''
		IF(ISNULL(@bitApplyFilter,0)=1 AND ISNULL(@numFilteredWarehouseID,0)>0)
		BEGIN
			--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY WFL.vcLocation asc) RowID,WRF.numWareHouseItemId,I.numItemCode INTO #temp2 '
			--,@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql
			--, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere
			--,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec
			--,'; SELECT * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
			SET @strFinal = @strFinal + 'SELECT ROW_NUMBER() OVER(ORDER BY WFL.vcLocation asc) RowID, '+ @strColumns++  ' INTO #temp2' + @strSql + @strWhere +' ;'
			SET @strFinal = @strFinal + ' SELECT @TotalRecords = COUNT(*) FROM #temp2; '
			SET @strFinal = @strFinal + ' SELECT * FROM #temp2 WHERE RowID > '+ CAST(@firstRec AS VARCHAR(100)) +' and RowID <'+CAST(@lastRec AS VARCHAR(100))+';DROP TABLE #temp2; '
		END
		ELSE IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql  , @strWhere ,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT  * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFInal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
/****** Object:  StoredProcedure [dbo].[Usp_ManageInboxTree]    Script Date: 07/26/2008 16:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageinboxtree')
DROP PROCEDURE usp_manageinboxtree
GO
CREATE PROCEDURE [dbo].[Usp_ManageInboxTree]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9),    
@NodeID as numeric(9),    
@NodeName as varchar(5000),    
@mode as tinyint,    
@ParentId as numeric(9),
@ParentNodeText as VARCHAR(5000)='',
@numLastUid as numeric(18,0)=0,
@bitHidden AS BIT=0
as      
    
if @mode = 0      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,CASE WHEN ISNULL(vcNodeName,'')='INBOX' THEN 'Inbox' ELSE ISNULL(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    
			    ORDER BY numSortOrder ASC    
		END     
end      
if @mode = 1      
begin   
	DECLARE @ErrorMessage NVARCHAR(4000)
	SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
		
	BEGIN TRANSACTION

	BEGIN TRY
		--Changed by sachin sadhu|| Date:21stJan2014
    --Purpose:Bug on creating Folders||VirginDrinks
		DECLARE @numSortOrder NUMERIC
		SELECT @numSortOrder=MAX(numSortOrder)+1 FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID   
		IF(@ParentNodeText<>'')
		BEGIN
			SET @ParentId=(SELECT TOP 1 numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName=@ParentNodeText)
		END
		IF(@ParentId=0)
		BEGIN
			SELECT @ParentId=numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID IS NULL
		END

		IF EXISTS (SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID = @ParentId AND LOWER(vcNodeName) = LOWER(@NodeName))
		BEGIN
			RAISERROR ('FOLDER_ALREADY_EXISTS',16,1)
			RETURN -1
		END 
		ELSE
		BEGIN
			INSERT INTO [InboxTree]      
				(vcName      
				,numParentId      
				,[numDomainId],numUserCntId)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId)   
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName      
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId,@numSortOrder) 
		END
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
end      
      
if @mode = 2      
begin      
     
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from inboxTree where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )     
   
 delete [InboxTree] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
--Update Email History with [InboxTreeSort]
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from InboxTreeSort where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )  
      
 delete [InboxTreeSort] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
end  
if @mode=3
BEGIN
	BEGIN TRY
	BEGIN TRAN
		DELETE FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName NOT IN(
			SELECT DISTINCT SUBSTRING(Items,1,CHARINDEX('#',Items)-1) FROM Split(@NodeName,',') WHERE Items<>''  AND CHARINDEX('#',Items)>0
		)
		
		INSERT INTO [InboxTreeSort]      
				(vcNodeName 
				,numLastUid     
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)        
				SELECT 
				DISTINCT
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)),  
				0,    
				0,      
				@numDomainID,
				@numUserCntId ,
				0
				--ROW_NUMBER() OVER( ORDER BY Items )
				FROM Split(@NodeName,',') WHERE Items<>'' AND 
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items))<>'' AND
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) NOT IN(
				SELECT ISNULL(vcNodeName,'') FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID) 
				
				
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName  
				,numLastUid    
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)        
				SELECT 
				DISTINCT
				SUBSTRING(Items,1,CHARINDEX('#',Items)-1),  
				0,    
				(SELECT TOP 1 numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName= SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items))),      
				@numDomainID,
				@numUserCntId,
				0
				--ROW_NUMBER() OVER( ORDER BY Items )
				FROM Split(@NodeName,',') WHERE Items<>'' 
				 AND CHARINDEX('#',Items)>0
				AND SUBSTRING(Items,1,CHARINDEX('#',Items)-1)<>''
				AND SUBSTRING(Items,1,CHARINDEX('#',Items)-1) NOT IN(
				SELECT ISNULL(vcNodeName,'') FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID)
				IF((SELECT COUNT(*) FROM ImapUserDetails WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID AND vcImapServerUrl='outlook.office365.com')>0)
				BEGIN
					UPDATE InboxTreeSort SET vcNodeName='Inbox' WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID AND vcNodeName='INBOX'
				END
				
	COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN	         
	END CATCH
END
if @mode=4
BEGIN
	IF((SELECT TOP 1 ISNULL(numLastUid,0) FROM InboxTreeSort WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID)=0)
	BEGIN
		UPDATE InboxTreeSort SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	END
	UPDATE InboxTree SET numLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END
if @mode=5
BEGIN
	UPDATE InboxTree SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END


if @mode = 7      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)    AND  numNodeID=@NodeId
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,ISNULL(vcNodeName,'') as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )     AND   numNodeID=@NodeId  
                    
			    ORDER BY numSortOrder ASC    
		END     
END
if @mode=8
BEGIN
	UPDATE InboxTreeSort SET bitHidden=@bitHidden WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END

if @mode=9
BEGIN
	UPDATE InboxTreeSort SET numParentID=0,numSortOrder=(SELECT MIN(ISNULL(numSortOrder,0)-1) FROM InboxTreeSort WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID) WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numSortOrder=(ISNULL(numSortOrder,0)+1) WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID<>@NodeID AND ISNULL(numSortOrder,0)>0

END
    
if @mode = 10      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,CASE WHEN ISNULL(vcNodeName,'')='INBOX' THEN 'Inbox' ELSE ISNULL(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    AND ISNULL(bitHidden,0)=0
			    ORDER BY numSortOrder ASC    
		END     
end  
GO

--SELECT SUBSTRING(Items,1,CHARINDEX('#',Items)-1) AS NodeName,SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) AS ParentNodeName FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''
--SELECT SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''
--SELECT Items FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)

	SELECT @numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForPick,0) bitGroupByOrderForPick
			,ISNULL(bitGroupByOrderForShip,0) bitGroupByOrderForShip
			,ISNULL(bitGroupByOrderForInvoice,0) bitGroupByOrderForInvoice
			,ISNULL(bitGroupByOrderForPay,0) bitGroupByOrderForPay
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintInvoicingType,1) tintInvoicingType
			,ISNULL(tintScanValue,1) tintScanValue
			,ISNULL(tintPackingMode,1) tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,ISNULL(tintPendingCloseFilter,1) tintPendingCloseFilter
		FROM	
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForPick
			,0 bitGroupByOrderForShip
			,0 bitGroupByOrderForInvoice
			,0 bitGroupByOrderForPay
			,0 bitGroupByOrderForClose
			,2 tintInvoicingType
			,1 tintScanValue
			,1 tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,1 tintPendingCloseFilter
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5  OR (@numViewID=2 AND @tintPackingViewMode=4)
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(1000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'ASC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'ASC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4)
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 2 AND @tintPackingViewMode = 4
							THEN 
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc'
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)',
																			(CASE 
																				WHEN @numViewID = 2 AND @tintPackingViewMode = 4
																				THEN 'WHEN @tintPackingViewMode = 4
																						THEN (CASE 
																								WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
																								THEN 1 
																								ELSE 0 
																							END)'
																				ELSE ''
																			END)
																			,'
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4) THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid'  ELSE '' END)) ELSE '' END));

	PRINT CAST(@vcSQL AS NTEXT)

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType;

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,0
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortOrder
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityItems.numoppitemtCode
			,0
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,''''
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(Item.txtItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SELECT * FROM #TEMPResult
	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForPick BIT
	,@bitGroupByOrderForShip BIT
	,@bitGroupByOrderForInvoice BIT
	,@bitGroupByOrderForPay BIT
	,@bitGroupByOrderForClose BIT
	,@tintInvoicingType TINYINT
	,@tintScanValue TINYINT
	,@tintPackingMode TINYINT
	,@tintPendingCloseFilter TINYINT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		UPDATE
			MassSalesFulfillmentConfiguration
		SET
			bitGroupByOrderForPick=@bitGroupByOrderForPick
			,bitGroupByOrderForShip=@bitGroupByOrderForShip
			,bitGroupByOrderForInvoice=@bitGroupByOrderForInvoice
			,bitGroupByOrderForPay=@bitGroupByOrderForPay
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintInvoicingType=@tintInvoicingType
			,tintScanValue=@tintScanValue
			,tintPackingMode=@tintPackingMode
			,tintPendingCloseFilter=@tintPendingCloseFilter
		WHERE
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentConfiguration
		(
			numDomainID 
			,numUserCntID
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
		)
		VALUES
		(
			@numDomainID 
			,@numUserCntID
			,@bitGroupByOrderForPick
			,@bitGroupByOrderForShip
			,@bitGroupByOrderForInvoice
			,@bitGroupByOrderForPay
			,@bitGroupByOrderForClose
			,@tintInvoicingType
			,@tintScanValue
			,@tintPackingMode
			,@tintPendingCloseFilter
		)
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,ISNULL(opp.bitWinningBid,0) bitWinningBid
			,ISNULL(opp.numParentOppItemID,0) numParentOppItemID
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
	@byteMode AS TINYINT  = NULL,
	@numOppId AS NUMERIC(18,0)  = NULL,
	@numOppBizDocsId AS NUMERIC(18,0)  = NULL,
	@numDomainID AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(9)  = 0,
	@ClientTimeZoneOffset INT=0
)
AS
BEGIN
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
	DECLARE @byteModeTemp AS TINYINT
	DECLARE @numOppIdTemp AS NUMERIC(18,0)
	DECLARE @numOppBizDocsIdTemp AS NUMERIC(18,0)
	DECLARE @numDomainIDTemp AS NUMERIC(18,0)
	DECLARE @numUserCntIDTemp AS NUMERIC(18,0)
	DECLARE @ClientTimeZoneOffsetTemp INT

	SET @byteModeTemp= @byteMode
	SET @numOppIdTemp = @numOppId
	SET @numOppBizDocsIdTemp=@numOppBizDocsId
	SET @numDomainIDTemp = @numDomainID
	SET @numUserCntIDTemp = @numUserCntID
	SET @ClientTimeZoneOffsetTemp = @ClientTimeZoneOffset


	IF @byteModeTemp = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS DECIMAL(20,5)
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppIdTemp
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppIdTemp)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsIdTemp
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppIdTemp)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainIDTemp
    END
  IF @byteModeTemp = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
			 tintOppStatus,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND numContactID = @numUserCntIDTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainIDTemp OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffsetTemp , dtCreatedDate),@numDomainIDTemp) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainIDTemp
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainIDTemp) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainIDTemp) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainIDTemp  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainIDTemp AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainIDTemp
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsIdTemp
             AND Mst.numDomainID = @numDomainIDTemp
    END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CloneOrderLineItem')
DROP PROCEDURE dbo.USP_OpportunityMaster_CloneOrderLineItem
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CloneOrderLineItem]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		RAISERROR('CLOSED_ORDER',16,1)
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @vcOppItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityItems' AND COLUMN_NAME <> 'numoppitemtCode'
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityKitItems' AND COLUMN_NAME <> 'numOppChildItemID' AND COLUMN_NAME <> 'numOppItemID'
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitChildItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
																	FROM INFORMATION_SCHEMA.COLUMNS
																	WHERE TABLE_NAME = N'OpportunityKitChildItems' AND COLUMN_NAME <> 'numOppKitChildItemID' AND COLUMN_NAME <> 'numOppItemID' AND COLUMN_NAME <> 'numOppChildItemID' 
																	FOR XML PATH('')),1,1,'')

			DECLARE @numNewOppItemID NUMERIC(18,0)
			DECLARE @numNewOppChildItemID NUMERIC(18,0)
			DECLARE @numNewOppKitChildItemID NUMERIC(18,0)
			DECLARE @sqlText AS NVARCHAR(MAX) = CONCAT('INSERT INTO OpportunityItems (',@vcOppItemsColumns,') SELECT ',@vcOppItemsColumns,' FROM OpportunityItems WHERE numOppID=',@numOppID,' AND numoppitemtCode=',@numOppItemID,'; SELECT @numNewOppItemID = SCOPE_IDENTITY()')

			EXEC sp_executesql @sqlText, N'@numNewOppItemID NUMERIC(18,0) OUTPUT', @numNewOppItemID OUTPUT

			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppChildItemID NUMERIC(18,0)
			)

			INSERT INTO @TEMP
			(
				numOppChildItemID
			)
			SELECT 
				numOppChildItemID 
			FROM 
				OpportunityKitItems 
			WHERE 
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempOppChildItemID NUMERIC(18,0)
			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempOppChildItemID=numOppChildItemID FROM @TEMP WHERE ID=@i

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitItems (',@vcOppKitItemsColumns,',numOppItemID) SELECT ',@vcOppKitItemsColumns,',',@numNewOppItemID,' FROM OpportunityKitItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,'; SELECT @numNewOppChildItemID = SCOPE_IDENTITY()')
				EXEC sp_executesql @sqlText, N'@numNewOppChildItemID NUMERIC(18,0) OUTPUT',@numNewOppChildItemID OUTPUT

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitChildItems (',@vcOppKitChildItemsColumns,',numOppItemID,numOppChildItemID) SELECT ',@vcOppKitChildItemsColumns,',',@numNewOppItemID,',',@numNewOppChildItemID,' FROM OpportunityKitChildItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,';')
				EXEC sp_executesql @sqlText

				SET @i = @i + 1
			END

			--Delete Tax for Opportunity Items if item deleted 
			DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

			--Insert Tax for Opportunity Items
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			) 
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				TI.numTaxItemID,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.ItemTax IT 
			ON 
				OI.numItemCode=IT.numItemCode 
			JOIN
				TaxItems TI 
			ON 
				TI.numTaxItemID = IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				0,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.Item I 
			ON 
				OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID 
				AND I.bitTaxable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
				dbo.OpportunityItems OI 
			INNER JOIN
				ItemTax IT
			ON
				IT.numItemCode = OI.numItemCode
			INNER JOIN
				TaxDetails TD
			ON
				TD.numTaxID = IT.numTaxID
				AND TD.numDomainId = @numDomainId
			WHERE
				OI.numOppId = @numOppID
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE 
				OI
			SET
				OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
			FROM 
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				numOppId=@numOppID
				AND ISNULL(I.bitKitParent,0) = 1
  
			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,ISNULL(Opp.vcType,'''') ItemType
									,ISNULL(Opp.vcType,'''') vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,(CASE 
										WHEN ISNULL(I.bitKitParent,0)=1  
												AND (CASE 
														WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
														THEN 1
														ELSE 0
													END) = 0
										THEN 
											dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,Opp.vcChildKitSelectedItems)
										ELSE 
											ISNULL(I.fltWeight,0) 
									END) fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,ISNULL(bitWinningBid,0) bitWinningBid, ISNULL(numParentOppItemID,0) numParentOppItemID,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_SetAsWinningBid')
DROP PROCEDURE dbo.USP_OpportunityMaster_SetAsWinningBid
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_SetAsWinningBid]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE OpportunityItems SET bitWinningBid=0 WHERE numOppId=@numOppID
	UPDATE OpportunityItems SET bitWinningBid=1 WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdateParentKit')
DROP PROCEDURE dbo.USP_OpportunityMaster_UpdateParentKit
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdateParentKit]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0),
	@numParentOppItemID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE 
		OpportunityItems 
	SET 
		numParentOppItemID=@numParentOppItemID
	WHERE 
		numOppId=@numOppID 
		AND numoppitemtCode=@numOppItemID
END
GO
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PayrollHeader_Save')
DROP PROCEDURE USP_PayrollHeader_Save
GO
CREATE PROCEDURE [dbo].[USP_PayrollHeader_Save]
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numComPayPeriodID NUMERIC(18,0),
	@tintMode TINYINT,
    @strPayrollDetail TEXT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT
	DECLARE @i AS INT
	DECLARE @iCount AS INT
	DECLARE @j AS INT
	DECLARE @jCount AS INT
	DECLARE @numUserID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppBizDocID NUMERIC(18,0)
	DECLARE @numTimeAndExpenseCommissionID NUMERIC(18,0)
	DECLARE @monTimeCommisionTotalAmount DECIMAL(20,5)
	DECLARE @monAmountToPay DECIMAL(20,5)
	DECLARE @numPayrollHeaderID NUMERIC(18,0)
	DECLARE @numComissionID NUMERIC(18,0)
	DECLARE @monCommissionAmount DECIMAL(20,5)

	DECLARE @TEMPTimeExpenseCommission TABLE
	(
		ID INT
		,numTimeAndExpenseCommissionID NUMERIC(18,0)
		,monTotalAmount DECIMAL(20,5)
	)

	IF NOT EXISTS (SELECT numPayrollHeaderID FROM PayrollHeader WHERE numComPayPeriodID=@numComPayPeriodID)
	BEGIN
		INSERT INTO PayrollHeader
		(
			numDomainId
			,numCreatedBy
			,dtCreatedDate
			,numComPayPeriodID
		)
		VALUES
		(
			@numDomainId
			,@numUserCntID
			,GETUTCDATE()
			,@numComPayPeriodID
		)

		SET @numPayrollHeaderID = SCOPE_IDENTITY()
	END
	ELSE 
	BEGIN
		UPDATE PayrollHeader SET dtModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numComPayPeriodID=@numComPayPeriodID

		SELECT 
			@numPayrollHeaderID=numPayrollHeaderID 
		FROM 
			PayrollHeader 
		WHERE 
			numComPayPeriodID=@numComPayPeriodID
	END

	IF @tintMode = 1 -- Pay Hours Worked
	BEGIN
		DECLARE @TEMPEmployeeTime TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,monAmountToPay DECIMAL(20,5)
			,monAdditionalAmtToPay DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPEmployeeTime
		(
			numUserCntID
			,monAmountToPay
			,monAdditionalAmtToPay
		)
		SELECT 
			numUserCntID
			,ISNULL(monTimeAmtToPay,0)
			,ISNULL(monAdditionalAmtToPay,0)
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,monTimeAmtToPay DECIMAL(20,5)
			,monAdditionalAmtToPay DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monHourlyAmt = TET.monAmountToPay,
			monAdditionalAmt = TET.monAdditionalAmtToPay
		FROM
			PayrollDetail PD
		INNER JOIN
			@TEMPEmployeeTime TET
		ON
			PD.numUserCntID = TET.numUserCntID
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,monHourlyAmt
			,monAdditionalAmt
		)
		SELECT
			@numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
			,TET.monAdditionalAmtToPay
		FROM
			@TEMPEmployeeTime TET
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND TET.numUserCntID = PD.numUserCntID
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPEmployeeTime)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = numUserCntID
				,@monAmountToPay=monAmountToPay
			FROM 
				@TEMPEmployeeTime 
			WHERE 
				ID=@i

			-- FIRST SET COMMISSION AS NOT PAID
			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
				AND (numType=1 OR numType=2) 
				AND numCategory=1

			UPDATE 
				StagePercentageDetailsTaskCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				TimeAndExpenseCommission
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
				AND (numType=1 OR numType=2) 
				AND numCategory=1
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
				UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				StagePercentageDetailsTaskCommission
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
				UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END
			
			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_HOURS_WORKED_PAYMENT',16,1)
			END

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 2 -- Pay reimbursable expense
	BEGIN
		DECLARE @TEMPEmployeeReimburseExpense TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,monAmountToPay DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPEmployeeReimburseExpense
		(
			numUserCntID
			,monAmountToPay
		)
		SELECT 
			numUserCntID
			,ISNULL(monReimbursableExpenseToPay,0)
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,monReimbursableExpenseToPay DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monReimbursableExpenses = TET.monAmountToPay
		FROM
			PayrollDetail PD
		INNER JOIN
			@TEMPEmployeeReimburseExpense TET
		ON
			PD.numUserCntID = TET.numUserCntID
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,monReimbursableExpenses
		)
		SELECT
			@numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
		FROM
			@TEMPEmployeeReimburseExpense TET
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND TET.numUserCntID = PD.numUserCntID
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPEmployeeReimburseExpense)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = numUserCntID
				,@monAmountToPay=monAmountToPay
			FROM 
				@TEMPEmployeeReimburseExpense 
			WHERE 
				ID=@i

			-- FIRST SET EXPENSE AS NOT PAID
			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserID
				AND numCategory=2
				AND bitReimburse = 1

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				TimeAndExpenseCommission
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserID
				AND numCategory=2
				AND bitReimburse = 1
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END

			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_REIMBURSABLE_EXPENSE_PAYMENT',16,1)
			END
			

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 3 -- pay remaining amount (commission)
	BEGIN
		DECLARE @TEMPCommission TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppBizDocID NUMERIC(18,0)
			,monCommissionPaid DECIMAL(20,5)
		)

		DECLARE @TEMPBizDocCommission TABLE
		(
			ID INT
			,numComissionID NUMERIC(18,0)
			,numComissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPCommission
		(
			numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid
		)
		SELECT 
			numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppBizDocID NUMERIC(18,0)
			,monCommissionPaid DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem


		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monCommissionAmt = ISNULL(TEMP.monCommissionToPay,0)
		FROM
			PayrollDetail PD
		CROSS APPLY
		(
			SELECT
				SUM(monCommissionPaid) monCommissionToPay
			FROM
				@TEMPCommission T1
			WHERE
				ISNULL(T1.numUserCntID,0)=ISNULL(PD.numUserCntID,0)
				AND ISNULL(T1.numDivisionID,0) = ISNULL(PD.numDivisionID,0)
		) TEMP
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,numDivisionID
			,monCommissionAmt
		)
		SELECT
			@numPayrollHeaderID
			,ISNULL(TEMP.numUserCntID,0)
			,ISNULL(TEMP.numDivisionID,0)
			,monCommissionToPay
		FROM
		(
			SELECT
				numUserCntID
				,numDivisionID
				,SUM(monCommissionPaid) monCommissionToPay
			FROM
				@TEMPCommission T1
			GROUP BY
				numUserCntID
				,numDivisionID
		) TEMP
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND ISNULL(TEMP.numUserCntID,0) = ISNULL(PD.numUserCntID,0)
			AND ISNULL(TEMP.numDivisionID,0) = ISNULL(PD.numDivisionID,0)
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPCommission)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = ISNULL(numUserCntID,0)
				,@numDivisionID=ISNULL(numDivisionID,0)
				,@numOppID=ISNULL(numOppID,0)
				,@numOppBizDocID=ISNULL(numOppBizDocID,0)
				,@monAmountToPay=ISNULL(monCommissionPaid,0)
			FROM 
				@TEMPCommission 
			WHERE 
				ID=@i

			-- FIRST SET COMMISSION AS NOT PAID
			UPDATE 
				BizDocComission 
			SET 
				bitCommisionPaid=0
				,monCommissionPaid=0 
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND 1 = (CASE 
							WHEN tintAssignTo=3 
							THEN (CASE WHEN ISNULL(numUserCntID,0)=@numDivisionID THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN ISNULL(numUserCntID,0)=@numUserID THEN 1 ELSE 0 END) 
						END)
				AND ISNULL(numOppID,0)=@numOppID
				AND ISNULL(numOppBizDocID,0)=@numOppBizDocID

			DELETE FROM @TEMPBizDocCommission

			INSERT INTO @TEMPBizDocCommission
			(
				ID
				,numComissionID
				,numComissionAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numComissionID)
				,numComissionID
				,ISNULL(numComissionAmount,0)
			FROM
				BizDocComission
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND 1 = (CASE 
							WHEN tintAssignTo=3 
							THEN (CASE WHEN ISNULL(numUserCntID,0)=@numDivisionID THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN ISNULL(numUserCntID,0)=@numUserID THEN 1 ELSE 0 END) 
						END)
				AND ISNULL(numOppID,0)=@numOppID
				AND ISNULL(numOppBizDocID,0)=@numOppBizDocID
			ORDER BY
				numComissionID


			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPBizDocCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numComissionID=numComissionID
					,@monCommissionAmount=numComissionAmount
				FROM
					@TEMPBizDocCommission
				WHERE
					ID=@j

				IF @monCommissionAmount >= @monAmountToPay
				BEGIN
					UPDATE BizDocComission SET bitCommisionPaid=1,monCommissionPaid=@monAmountToPay WHERE numComissionID=@numComissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
					UPDATE BizDocComission SET bitCommisionPaid=1,monCommissionPaid=@monCommissionAmount WHERE numComissionID=@numComissionID
					SET @monAmountToPay = @monAmountToPay - @monCommissionAmount
				END
				
				SET @j = @j + 1
			END

			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_COMMISSION_PAYMENT',16,1)
			END

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 4 -- pay remaining amount (All e.g Hours worked, reimbursable, overpayment, commission)
	BEGIN
		DECLARE @numTempUserCntID NUMERIC(18,0)
				,@numTempDivisionID NUMERIC(18,0)
				,@monTempHoursWorked DECIMAL(20,5)
				,@monTempAdditionalAmt DECIMAL(20,5)
				,@monTempReimbursableExpense DECIMAL(20,5)
				,@monTempSalesReturn DECIMAL(20,5)
				,@monTempOverpayment DECIMAL(20,5)
				,@monTempCommissionAmount DECIMAL(20,5)

		DECLARE @TEMPCommissionPay TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,monHoursWorked DECIMAL(20,5)
			,monAdditionalAmt DECIMAL(20,5)
			,monReimbursableExpense DECIMAL(20,5)
			,monSalesReturn DECIMAL(20,5)
			,monOverpayment DECIMAL(20,5)
			,monCommissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPCommissionPay
		(
			numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount
		)
		SELECT 
			numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,monHoursWorked DECIMAL(20,5)
			,monAdditionalAmt DECIMAL(20,5)
			,monReimbursableExpense DECIMAL(20,5)
			,monSalesReturn DECIMAL(20,5)
			,monOverpayment DECIMAL(20,5)
			,monCommissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPCommissionPay)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numTempUserCntID=ISNULL(numUserCntID,0)
				,@numTempDivisionID=ISNULL(numDivisionID,0)
				,@monTempHoursWorked=ISNULL(monHoursWorked,0)
				,@monTempAdditionalAmt=ISNULL(monAdditionalAmt,0)
				,@monTempReimbursableExpense=ISNULL(monReimbursableExpense,0)
				,@monTempSalesReturn=ISNULL(monSalesReturn,0)
				,@monTempOverpayment=ISNULL(monOverpayment,0)
				,@monTempCommissionAmount=ISNULL(monCommissionAmount,0)
			FROM
				@TEMPCommissionPay
			WHERE
				ID=@i

			IF @numTempUserCntID > 0
			BEGIN
				IF ISNULL((SELECT 
								SUM(monTotalAmount) 
							FROM 
								TimeAndExpenseCommission 
							WHERE 
								numComPayPeriodID=@numComPayPeriodID 
								AND numUserCntID=@numTempUserCntID 
								AND (numType=1 OR numType=2) 
								AND numCategory=1),0) <> @monTempHoursWorked
				BEGIN
					RAISERROR('INVALID_HOURS_WORKED_PAYMENT',16,1)
				END

				IF ISNULL((SELECT 
								SUM(monTotalAmount) 
							FROM 
								TimeAndExpenseCommission 
							WHERE 
								numComPayPeriodID=@numComPayPeriodID 
								AND numUserCntID=@numTempUserCntID 
								AND numCategory=2
								AND bitReimburse = 1),0) <> @monTempReimbursableExpense
				BEGIN
					RAISERROR('INVALID_REIMBURSABLE_EXPENSE_PAYMENT',16,1)
				END
			END

			IF ISNULL((SELECT 
							SUM(monCommissionReversed) 
						FROM 
							SalesReturnCommission
						WHERE 
							numComPayPeriodID=@numComPayPeriodID 
							AND 1  = (CASE 
										WHEN @numTempDivisionID > 0
										THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
									END)),0) <> @monTempSalesReturn
			BEGIN
				RAISERROR('INVALID_SALES_RETURN_PAYMENT',16,1)
			END

			IF ISNULL((SELECT 
							SUM(monDifference) 
						FROM 
							BizDocComissionPaymentDifference BDCPD
						INNER JOIN
							BizDocComission BDC
						ON
							BDCPD.numComissionID=BDC.numComissionID
						WHERE 
							BDC.numDomainID=@numDomainId
							AND ISNULL(bitDifferencePaid,0) = 0),0) <> @monTempOverpayment
			BEGIN
				RAISERROR('INVALID_OVER_PAYMENT',16,1)
			END

			PRINT @numTempUserCntID
			PRINT @monTempCommissionAmount
			IF ISNULL((SELECT 
							SUM(numComissionAmount) 
						FROM 
							BizDocComission 
						WHERE 
							numComPayPeriodID=@numComPayPeriodID 
							AND 1  = (CASE 
										WHEN @numTempDivisionID > 0
										THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
									END)),0) <> @monTempCommissionAmount
			BEGIN
				RAISERROR('INVALID_COMMISSION_PAYMENT',16,1)
			END

			IF EXISTS (SELECT numPayrollDetailID FROM PayrollDetail WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numUserCntID,0) = @numTempUserCntID AND ISNULL(numDivisionID,0)=@numTempDivisionID)
			BEGIN
				UPDATE
					PayrollDetail
				SET
					monHourlyAmt=@monTempHoursWorked
					,monAdditionalAmt=@monTempAdditionalAmt
					,monReimbursableExpenses=@monTempReimbursableExpense
					,monSalesReturn=@monTempSalesReturn
					,monOverPayment=@monTempOverpayment
					,monCommissionAmt=@monTempCommissionAmount
				WHERE
					numPayrollHeaderID=@numPayrollHeaderID 
					AND ISNULL(numUserCntID,0) = @numTempUserCntID 
					AND ISNULL(numDivisionID,0)=@numTempDivisionID
			END
			ELSE
			BEGIN
				INSERT INTO PayrollDetail
				(
					numDomainId
					,numPayrollHeaderID
					,numUserCntID
					,numDivisionID
					,monHourlyAmt
					,monAdditionalAmt
					,monReimbursableExpenses
					,monSalesReturn
					,monOverPayment
					,monCommissionAmt
				)
				VALUES
				(
					@numDomainId
					,@numPayrollHeaderID
					,@numTempUserCntID
					,@numTempDivisionID
					,@monTempHoursWorked
					,@monTempAdditionalAmt
					,@monTempReimbursableExpense
					,@monTempSalesReturn
					,@monTempOverpayment
					,@monTempCommissionAmount
				)
			END


			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=1
				,monAmountPaid=monTotalAmount 
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND numUserCntID=@numTempUserCntID
			
			UPDATE 
				SalesReturnCommission 
			SET 
				bitCommissionReversed=1
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND 1  = (CASE 
							WHEN @numTempDivisionID > 0
							THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
							ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
						END)

			UPDATE 
				BizDocComissionPaymentDifference
			SET
				bitDifferencePaid=1
				,numComPayPeriodID=@numComPayPeriodID
			WHERE 
				ISNULL(bitDifferencePaid,0) = 0

			UPDATE 
				BizDocComission 
			SET 
				bitCommisionPaid=1
				,monCommissionPaid=numComissionAmount
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND 1  = (CASE 
							WHEN @numTempDivisionID > 0
							THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
							ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
						END)

			SET @i = @i + 1
		END 
	END

	UPDATE
		PayrollDetail
	SET
		monTotalAmt = ISNULL(monHourlyAmt,0) + ISNULL(monAdditionalAmt,0) + ISNULL(monReimbursableExpenses,0) + ISNULL(monCommissionAmt,0) + ISNULL(monOverPayment,0) - ISNULL(monSalesReturn,0)
	WHERE
		numPayrollHeaderID=@numPayrollHeaderID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId NUMERIC(18,0),
    @numDepartmentId NUMERIC(18,0),
    @numComPayPeriodID NUMERIC(18,0),
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	DECLARE @dtStartDate DATETIME
	DECLARE @dtEndDate DATETIME

	SELECT
		@dtStartDate = DATEADD (MS , 000 , CAST(dtStart AS DATETIME))
		,@dtEndDate = DATEADD (MS , -3 , CAST(dtEnd AS DATETIME))  
	FROM
		CommissionPayPeriod
	WHERE
		numComPayPeriodID=@numComPayPeriodID
	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate DECIMAL(20,5),
		decRegularHrs decimal(20,5),
		decPaidLeaveHrs decimal(20,5),
		monRegularHrsPaid DECIMAL(20,5),
		monAdditionalAmountPaid DECIMAL(20,5),
		monExpense DECIMAL(20,5),
		monReimburse DECIMAL(20,5),
		monReimbursePaid DECIMAL(20,5),
		monCommPaidInvoice DECIMAL(20,5),
		monCommPaidInvoiceDeposited DECIMAL(20,5),
		monCommUNPaidInvoice DECIMAL(20,5),
		monCommPaidCreditMemoOrRefund DECIMAL(20,5),
		monOverPayment DECIMAL(20,5),
		monCommissionEarned DECIMAL(20,5),
		monCommissionPaid DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal  DECIMAL(20,5),
		monTotalAmountDue DECIMAL(20,5),
		monAmountPaid DECIMAL(20,5), 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND ISNULL(UM.bitPayroll,0) = 1
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName


	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @monReimburse DECIMAL(20,5),@monCommPaidInvoice DECIMAL(20,5),@monCommPaidInvoiceDepositedToBank DECIMAL(20,5),@monCommUNPaidInvoice DECIMAL(20,5),@monCommPaidCreditMemoOrRefund DECIMAL(20,5)

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
	DECLARE @bitPartner BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0),
			@bitPartner=ISNULL(bitPartner,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT  
				@decTotalHrsWorked=ISNULL(SUM(numHours),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND (numType=1 OR numType=2) 
				AND numCategory=1

			SET @decTotalHrsWorked = ISNULL(@decTotalHrsWorked,0) + ISNULL((SELECT  
																			SUM(numHours)   
																		FROM 
																			StagePercentageDetailsTaskCommission 
																		WHERE 
																			numComPayPeriodID = @numComPayPeriodID
																			AND numUserCntID=@numUserCntID),0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT  
				@monReimburse=ISNULL(SUM(monTotalAmount),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND numCategory=2
				AND bitReimburse = 1
		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage = 2
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=oppBiz.numOppID 
				AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				AND DM.tintDepositePage=2 
				AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		) TEMPDeposit
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=ISNULL(SUM(BC.numComissionAmount),0) 
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT 
														SUM(monCommissionReversed) 
													FROM 
														SalesReturnCommission 
													WHERE 
														1 = (CASE 
																WHEN ISNULL(@bitPartner,0) = 1 
																THEN (CASE WHEN numUserCntID=@numDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
																ELSE (CASE WHEN numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3 THEN 1 ELSE 0 END)
															END)),0)	
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=ISNULL(@decTotalHrsWorked,0)
			,monReimburse=ISNULL(@monReimburse,0)
			,monCommPaidInvoice=ISNULL(@monCommPaidInvoice,0)
			,monCommPaidInvoiceDeposited=ISNULL(@monCommPaidInvoiceDepositedToBank,0)
			,monCommUNPaidInvoice=ISNULL(@monCommUNPaidInvoice,0)
			,monCommPaidCreditMemoOrRefund=ISNULL(@monCommPaidCreditMemoOrRefund,0)
			,monCommissionEarned = (CASE 
										WHEN ISNULL(bitPartner,0) = 1 
										THEN ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0) 
										ELSE ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
									END)
			,monOverPayment=(CASE 
								WHEN ISNULL(bitPartner,0) = 1 
								THEN ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE BCInner.numDomainId=@numDomainId AND (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numDivisionID AND BCInner.tintAssignTo=3),0) 
								ELSE ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE BCInner.numDomainId=@numDomainId AND (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numUserCntID AND BCInner.tintAssignTo <> 3),0)
							END)
			,monInvoiceSubTotal = (CASE 
										WHEN ISNULL(bitPartner,0) = 1 
										THEN ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID  AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0)
										ELSE ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID  AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
									END)
			,monOrderSubTotal = (CASE 
									WHEN ISNULL(bitPartner,0) = 1 
									THEN ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0)
									ELSE ISNULL((SELECT SUM(monInvoiceSubTotal) FROM BizDocComission WHERE numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
								END)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(T1.monTotalAmt,0)
		,monRegularHrsPaid=ISNULL(T1.monHourlyAmt,0)
		,monAdditionalAmountPaid=ISNULL(T1.monAdditionalAmt,0)
		,monReimbursePaid=ISNULL(T1.monReimbursableExpenses,0)
		,monCommissionPaid=ISNULL(T1.monCommissionAmt,0)
		,monTotalAmountDue= (temp.decRegularHrs * monHourlyRate) + ISNULL(monReimburse,0) + ISNULL(monCommissionEarned,0) + ISNULL(monOverPayment,0) - ISNULL(monCommPaidCreditMemoOrRefund,0)
	FROM 
		#tempHrs temp  
	LEFT JOIN
	(
		SELECT
			numUserCntID
			,numDivisionID
			,monHourlyAmt
			,monAdditionalAmt
			,monReimbursableExpenses
			,monCommissionAmt
			,monTotalAmt
		FROM
			dbo.PayrollHeader PH
		INNER JOIN
			PayrollDetail PD 
		ON 
			PH.numPayrollHeaderID = PD.numPayrollHeaderID
		WHERE
			PH.numComPayPeriodID = @numComPayPeriodID
	) T1
	ON
		1 = (CASE 
				WHEN ISNULL(bitPartner,0) = 1  
				THEN (CASE WHEN temp.numDivisionID = T1.numDivisionID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN temp.numUserCntID = T1.numUserCntID THEN 1 ELSE 0 END)
			END)
		
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempHrs
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME,
	@fltReorderQty FLOAT,
	@fltReorderPoint FLOAT
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	UPDATE Item SET fltReorderQty=@fltReorderQty WHERE numItemCode=@numItemCode
	UPDATE WareHouseItems SET numReorder=@fltReorderPoint WHERE numItemID=@numItemCode


	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDoc INT    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
		SELECT 
			* 
		INTO 
			#temp 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Item',2)    
		WITH 
		( 
			numWareHouseItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,intAdjust FLOAT
			,monAverageCost DECIMAL(20,5)
		) 
		
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
		INTO 
			#tempSerialLotNO 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
		WITH 
			(
				numItemCode NUMERIC(18,0)
				,numWareHouseID NUMERIC(18,0)
				,numWareHouseItemID NUMERIC(18,0)
				,vcSerialNo VARCHAR(3000)
				,byteMode TINYINT
			) 
		
		EXEC sp_xml_removedocument @hDoc  

		DECLARE @numOnHand FLOAT
		DECLARE @numAllocation FLOAT
		DECLARE @numBackOrder FLOAT
		DECLARE @numWareHouseItemID AS NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
			
		SELECT 
			@numWareHouseItemID = MIN(numWareHouseItemID) 
		FROM 
			#temp
			
		DECLARE @bitLotNo AS BIT = 0  
		DECLARE @bitSerialized AS BIT = 0  
		DECLARE @monItemAvgCost DECIMAL(20,5)
		DECLARE @monUnitCost DECIMAL(20,5)
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		DECLARE @TotalOnHand AS FLOAT;
		DECLARE @numNewQtyReceived AS FLOAT

		WHILE @numWareHouseItemID>0    
		BEGIN		  
			SET @monAvgCost = 0
			SET @TotalOnHand = 0
			SET @numNewQtyReceived = 0

			SELECT TOP 1
				@bitLotNo=ISNULL(Item.bitLotNo,0)
				,@bitSerialized=ISNULL(Item.bitSerialized,0)
				,@monItemAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost, 0) END)
				,@monUnitCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(#temp.monAverageCost, 0) END)
				,@numNewQtyReceived = ISNULL(#temp.intAdjust,0)
			FROM 
				Item 
			INNER JOIN 
				#temp 
			ON 
				item.numItemCode=#temp.numItemCode 
			INNER JOIN
				WareHouseItems
			ON
				#temp.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			WHERE 
				#temp.numWareHouseItemID=@numWareHouseItemID 
				AND Item.numDomainID=@numDomainID
				AND WareHouseItems.numDomainID=@numDomainID

			IF @bitLotNo=0 AND @bitSerialized=0
			BEGIN		
				IF (SELECT 
						ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
					FROM 
						WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID 
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
				BEGIN
					SELECT @numTempItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
					--Updating the Average Cost
					SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numTempItemCode
  					SET @monAvgCost = ((@TotalOnHand * @monItemAvgCost) + (@numNewQtyReceived * @monUnitCost)) / ( @TotalOnHand + @numNewQtyReceived )

					UPDATE  
						item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
						,bintModifiedDate=GETUTCDATE()
						,numModifiedBy=@numUserCntID
					WHERE 
						numItemCode = @numTempItemCode

					IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END)
							,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END
					ELSE
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END	  
								
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numTempItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = 'Inventory Adjustment', --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate = @dtAdjustmentDate,
					@numDomainId = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
				END
			END	  
				    
			SELECT TOP 1 
				@numWareHouseItemID = numWareHouseItemID 
			FROM 
				#temp 
			WHERE 
				numWareHouseItemID >@numWareHouseItemID 
			ORDER BY 
				numWareHouseItemID  
				    
			IF @@rowcount=0 
				SET @numWareHouseItemID =0    
		END    
    

		DROP TABLE #temp
    
    
		-------------Serial/Lot #s----------------
		DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
		SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
		DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
				@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
		DECLARE @posComma int, @strKeyVal varchar(20)
		    
		WHILE @minRowNo <= @maxRowNo
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
				@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
			SET @posComma=0
		
			SET @vcSerialNo=RTRIM(@vcSerialNo)
			IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

			SET @posComma=PatIndex('%,%', @vcSerialNo)
			WHILE @posComma>1
				BEGIN
					SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
					DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END		
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END
	  
				SET @numWareHouseItmsDTLID=0
				SET @OldQty=0
				SET @vcComments=''
			
				select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
						from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
						vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
				IF @byteMode=0 --Add
					SET @numQty=@OldQty + @strQty
				ELSE --Deduct
					SET @numQty=@OldQty - @strQty
			
				EXEC dbo.USP_AddUpdateWareHouseForItems
						@numItemCode = @numTempItemCode, --  numeric(9, 0)
						@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
						@numWareHouseItemID =@numWareHouseItemID,
						@numDomainID = @numDomainId, --  numeric(9, 0)
						@vcSerialNo = @strName, --  varchar(100)
						@vcComments = @vcComments, -- varchar(1000)
						@numQty = @numQty, --  numeric(18, 0)
						@byteMode = 5, --  tinyint
						@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
						@numUserCntID = @numUserCntID, --  numeric(9, 0)
						@dtAdjustmentDate = @dtAdjustmentDate
	  
				SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
				SET @posComma=PatIndex('%,%',@vcSerialNo)
			END

			SET @minRowNo=@minRowNo+1
		END
	
		DROP TABLE #tempSerialLotNO

		

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode
	END   
END


