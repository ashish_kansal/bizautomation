/******************************************************************
Project: Release 5.2 Date: 09.Dec.2015
Comments: ALTER SCRIPTS
*******************************************************************/

BEGIN TRANSACTION

UPDATE DynamicFormFieldMaster SET vcFormFieldName = 'Tax Value' WHERE numFormID=45 AND numFormFieldId=2024
UPDATE DycFormField_Mapping SET vcFieldName = 'Tax Value' WHERE numFormID=45 AND numFieldID=346

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(9,'Tax Value Type','tintTaxType','tintTaxType','tintTaxType','TaxDetails','N','R','TextBox',7,1,7,1,0,0,1,1,0,0,0,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(45,'Tax Value Type','R','EditBox','tintTaxType',0,0,'N','tintTaxType',0,'TaxDetails',1,7,1,'tintTaxType',1,7,1,0,0,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(9,@numFieldID,45,0,0,'Tax Value Type','TextBox','tintTaxType',7,1,7,1,0,1,1,0,0,0,1,@numFormFieldID)

ROLLBACK


==========================================================

BEGIN TRANSACTION


DECLARE @numFieldID NUMERIC(18,0)
DECLARE @numFormFieldID NUMERIC(18,0)

INSERT INTO DynamicFormFieldMaster
(
	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults
)
VALUES
(
	7,'Ordered Qty','R','EditBox','numTotalUnitHour',0,0,'N','numTotalUnitHour',1,'OpportunityBizDocItems',0,1
)

SELECT @numFormFieldID = SCOPE_IDENTITY()


INSERT INTO DycFieldMaster 
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitAllowFiltering,bitInResults
)
VALUES
(
	3,'Total Units','numTotalUnitHour','numTotalUnitHour','OpportunityBizDocItems','M','R','TextBox',0,1,0,1,0,1,1
)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID
)
VALUES
(
	3,@numFieldID,7,1,1,'Ordered Qty','TextBox',1,0,0,1,@numFormFieldID
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID
)
VALUES
(
	3,@numFieldID,8,1,1,'Ordered Qty','TextBox',1,0,0,1,@numFormFieldID
)

ROLLBACK

========================================================================

ALTER TABLE DivisionMaster ADD
tintPriceLevel INT NULL


========================================================================
GO

ALTER TABLE [dbo].[WareHouseItems] DROP CONSTRAINT [DF_WareHouseItems_numReorder]
GO

ALTER TABLE WarehouseItems ALTER COLUMN numOnHand FLOAT
ALTER TABLE WarehouseItems ALTER COLUMN numOnOrder FLOAT
ALTER TABLE WarehouseItems ALTER COLUMN numReorder FLOAT
ALTER TABLE WarehouseItems ALTER COLUMN numAllocation FLOAT
ALTER TABLE WarehouseItems ALTER COLUMN numBackOrder FLOAT

ALTER TABLE [dbo].[WareHouseItems] ADD  CONSTRAINT [DF_WareHouseItems_numReorder]  DEFAULT ((0)) FOR [numReorder]
GO


ALTER TABLE OpportunityItems ALTER COLUMN numUnitHour FLOAT
ALTER TABLE OpportunityItems ALTER COLUMN [numUnitHourReceived] FLOAT
ALTER TABLE OpportunityItems ALTER COLUMN [numQtyShipped] FLOAT

ALTER TABLE OpportunityKitItems ALTER COLUMN numQtyItemsReq FLOAT
ALTER TABLE OpportunityKitItems ALTER COLUMN numQtyItemsReq_Orig FLOAT
ALTER TABLE OpportunityKitItems ALTER COLUMN [numQtyShipped] FLOAT

ALTER TABLE OpportunityKitChildItems ALTER COLUMN numQtyItemsReq FLOAT
ALTER TABLE OpportunityKitChildItems ALTER COLUMN numQtyItemsReq_Orig FLOAT
ALTER TABLE OpportunityKitChildItems ALTER COLUMN [numQtyShipped] FLOAT

ALTER TABLE OpportunityBizDocItems ALTER COLUMN numUnitHour FLOAT

ALTER TABLE WarehouseItems_Tracking ALTER COLUMN numTotalOnHand FLOAT


ALTER TABLE SalesTemplateItems ALTER COLUMN numUnitHour FLOAT
ALTER TABLE SalesTemplateItems ALTER COLUMN UOMConversionFactor DECIMAL(30,16)

ALTER TABLE ReturnItems ALTER COLUMN monPrice DECIMAL(30,16)
ALTER TABLE ReturnItems ALTER COLUMN numUnitHour FLOAT
ALTER TABLE ReturnItems ALTER COLUMN numUnitHourReceived FLOAT

ALTER TABLE WorkOrderDetails ALTER COLUMN numQtyItemsReq FLOAT
ALTER TABLE WorkOrderDetails ALTER COLUMN numQtyItemsReq_Orig FLOAT
