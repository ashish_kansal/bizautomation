/******************************************************************
Project: Release 11.9 Date: 26.APR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE PromotionOffer ADD monDiscountedItemPrice DECIMAL(20,5)


/******************************************** PRASANT *********************************************/

UPDATE DycFormField_Mapping SET bitAllowEdit=1 WHERE vcFieldName='Follow-up Status' AND numFormID=43
UPDATE DycFieldMaster SET vcPropertyName='vcCompactContactDetails' WHERE vcDbColumnName='vcCompactContactDetails'