/******************************************************************
Project: Release 14.6 Date: 05.DEC.2020
Comments: STORE PROCEDURES
*******************************************************************/


GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNewvcEmailString]    Script Date: 11/02/2011 12:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------Function For Returning new string ----------------------
------Created By : Pinkal Patel Date : 06-Oct-2011
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetNewvcEmailString')
DROP FUNCTION fn_GetNewvcEmailString
GO
CREATE FUNCTION [dbo].[fn_GetNewvcEmailString](
@vcEmailId VARCHAR(2000),
@numDomianID NUMERIC(18,0) = NULL
) RETURNS VARCHAR(2000)
As
BEGIN
DECLARE @vcNewEmailId AS VARCHAR(2000)
DECLARE @Email AS VARCHAR(200)
DECLARE @EmailName AS VARCHAR(200)
DECLARE @EmailAdd AS VARCHAR(200)
DECLARE @StartPos AS INTEGER
DECLARE @EndPos AS INTEGER
DECLARE @numEmailid AS NUMERIC(9)
---DECLARE @numDomainId AS NUMERIC(18, 0)

SET @vcNewEmailId =''
	IF @vcEmailId <> '' 
    BEGIN
        WHILE CHARINDEX('#^#', @vcEmailId) > 0	
            BEGIN
                SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailId, 1,
                                                   CHARINDEX('#^#', @vcEmailId,
                                                             0)+2)))
                IF CHARINDEX('$^$', @Email) > 0 
                    BEGIN
                        SET @EmailName = LTRIM(RTRIM(SUBSTRING(@Email, 0, CHARINDEX('$^$', @Email))))
                        SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, LEN(@EmailName) + 1, CHARINDEX('$^$', @Email) + LEN(@Email) - 1)))
                        SET @EmailAdd = REPLACE(@Emailadd, '$^$', '')
                        SET @EmailAdd = REPLACE(@Emailadd, '#^#', '')                
                    END
                   SET @vcNewEmailId =@vcNewEmailId + (SELECT TOP  1 CAST(ISNULL(numEmailid,0) AS VARCHAR) FROM dbo.EmailMaster WHERE dbo.EmailMaster.vcEmailID=@EmailAdd AND numDomainId=@numDomianID) + '$^$' + @EmailName + '$^$' + @EmailAdd   + '#^#'
				--PRINT @Email
				--SET @vcNewEmailId = @vcNewEmailId + ','
                SET @vcEmailId = LTRIM(RTRIM(SUBSTRING(@vcEmailId,
                                                       CHARINDEX('#^#', @vcEmailId)
                                                       + 3, LEN(@vcEmailId))))
	
		       END
            
			END
			IF LEN(@vcNewEmailId) > 0
			BEGIN
				 SET @vcNewEmailId = LEFT(@vcNewEmailId,LEN(@vcNewEmailId)-3)
			END                
                 RETURN @vcNewEmailId
                 
	END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Category_GetParentCategories')
DROP PROCEDURE dbo.USP_Category_GetParentCategories
GO
CREATE PROCEDURE [dbo].[USP_Category_GetParentCategories]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	;WITH CTE (numCategoryID,numDepCategory,vcCategoryName,tintLevel) AS
	(
		SELECT 
			Category.numCategoryID
			,Category.numDepCategory
			,Category.vcCategoryName
			,0
		FROM
			Category
		WHERE
			Category.numDomainID=@numDomainID
			AND Category.numCategoryID=@numCategoryID
		UNION ALL
		SELECT
			Category.numCategoryID
			,Category.numDepCategory
			,Category.vcCategoryName
			,C.tintLevel + 1
		FROM
			Category
		INNER JOIN
			CTE C
		ON
			Category.numCategoryID = C.numDepCategory
	)

	SELECT * FROM CTE ORDER BY tintLevel DESC
END
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartofChildAccounts' ) 
    DROP PROCEDURE USP_ChartofChildAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE PROCEDURE [dbo].[USP_ChartofChildAccounts]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0,
    @strSortOn AS VARCHAR(11) = NULL,
    @strSortDirection AS VARCHAR(10) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @dtFinYearFrom DATETIME ;
	DECLARE @dtFinYearTo DATETIME ;
	SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                    FROM     FINANCIALYEAR
                    WHERE    dtPeriodFrom <= GETDATE()
                            AND dtPeriodTo >= GETDATE()
                            AND numDomainId = @numDomainId
                    ) ;
	SELECT @dtFinYearTo = MAX(datEntry_Date) FROM General_Journal_Header WHERE numDomainID=@numDomainId


	DECLARE @TEMP TABLE
	(
		numAccountID NUMERIC(18,0)
		,bitIsSubAccount BIT
		,bitActive BIT
		,vcAccountCode VARCHAR(300)
		,numParentAccontID NUMERIC(18,0)
		,OPENING DECIMAL(20,5)
		,numDebitAmt DECIMAL(20,5)
		,numCreditAmt DECIMAL(20,5)
	)

	DECLARE @TEMP1 TABLE
	(
		numAccountID NUMERIC(18,0)
		,bitIsSubAccount BIT
		,bitActive BIT
		,vcAccountCode VARCHAR(300)
		,numParentAccontID NUMERIC(18,0)
		,OPENING DECIMAL(20,5)
		,numDebitAmt DECIMAL(20,5)
		,numCreditAmt DECIMAL(20,5)
	)

	;WITH CTE (numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID) AS
	(
		SELECT 
			numAccountID
			,bitIsSubAccount
			,vcAccountCode
			,numParentAccId
		FROM
			Chart_Of_Accounts
		WHERE
			numDomainId=@numDomainId
			AND ISNULL(numParentAccId,0) = 0
		UNION ALL
		SELECT 
			COA.numAccountID
			,COA.bitIsSubAccount
			,COA.vcAccountCode
			,COA.numParentAccId
		FROM
			Chart_Of_Accounts COA
		INNER JOIN
			CTE C
		ON
			COA.numParentAccId = C.numAccountID
	)

	INSERT INTO @TEMP 
		(numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,OPENING,numDebitAmt,numCreditAmt) 
	SELECT 
		numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,0,0,0 
	FROM 
		CTE

	;WITH CTE (numAccountID,numDebitAmt,numCreditAmt) AS
	(
		SELECT
			GJD.numChartAcntId
			,ISNULL(SUM(GJD.numDebitAmt),0) numDebitAmt
			,ISNULL(SUM(GJD.numCreditAmt),0) numCreditAmt
		FROM
			General_Journal_Header GJH
		INNER JOIN
			dbo.General_Journal_Details GJD
		ON
			GJH.numJournal_Id = GJD.numJournalId
		INNER JOIN
			@TEMP T
		ON
			GJD.numChartAcntId = T.numAccountID
		WHERE
			GJH.numDomainId=@numDomainId
			AND datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo
		GROUP BY
			GJD.numChartAcntId
	)

	UPDATE
		COA
	SET
		numDebitAmt = ISNULL(C.numDebitAmt,0)
		,numCreditAmt = ISNULL(C.numCreditAmt,0)
	FROM 
		@TEMP COA 
	INNER JOIN
		CTE C
	ON
		COA.numAccountID = C.numAccountID

	INSERT INTO @TEMP1 SELECT * FROM @TEMP

	;WITH CTE (numAccountId,numDebitAmt,numCreditAmt,RootID) as
	(
		SELECT 
			COA.numAccountId,
			COA.numDebitAmt,
			COA.numCreditAmt,
			COA.numAccountId as RootID
		FROM 
			@TEMP COA 
		UNION ALL
		SELECT 
			COA.numAccountId,
			COA.numDebitAmt,
			COA.numCreditAmt,
			C.RootID as RootID
		FROM
			@TEMP COA 
		INNER JOIN 
			CTE C 
		ON 
			COA.numParentAccontID=C.numAccountId
	)

	UPDATE 
		COA
	SET 
		numDebitAmt=ISNULL(numDebitAmtIncludingChildren,0),
		numCreditAmt=ISNULL(numCreditAmtIncludingChildren,0)
	FROM 
		@TEMP COA
	INNER JOIN 
	(
            select RootID,
                sum(numDebitAmt) as numDebitAmtIncludingChildren,
				sum(numCreditAmt) as numCreditAmtIncludingChildren
            from CTE C 
            group by RootID
    ) as S
	ON 
		COA.numAccountId = S.RootID
	option (maxrecursion 0) 

	DECLARE @PLSummary TABLE (numAccountId numeric(9),vcAccountName varchar(250),
	numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
	vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

	INSERT INTO  @PLSummary
	SELECT COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,
	0 AS OPENING,
	ISNULL(SUM(numDebitAmt),0) as DEBIT,
	ISNULL(SUM(numCreditAmt),0) as CREDIT,
	ISNULL(COA.bitIsSubAccount,0)
	FROM 
		Chart_of_Accounts COA
	LEFT JOIN 
		@TEMP VJ 
	ON 
		COA.numAccountId = VJ.numAccountID
	WHERE 
		COA.numDomainId=@numDomainId 
		AND COA.bitActive = 1 
		AND (COA.vcAccountCode LIKE '0103%' OR COA.vcAccountCode LIKE '0104%' OR COA.vcAccountCode LIKE '0106%')
	GROUP BY
		COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,COA.bitIsSubAccount


	DECLARE @PLOutPut TABLE (numAccountId numeric(9),vcAccountName varchar(250),
	numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
	vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));

	INSERT INTO @PLOutPut
	SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
		ISNULL(SUM(Opening),0) as Opening,
	ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
	FROM 
		AccountTypeDetail ATD RIGHT OUTER JOIN 
	@PLSummary PL ON
	PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
	AND ATD.numDomainId=@numDomainId AND
	(ATD.vcAccountCode LIKE '0103%' OR
			ATD.vcAccountCode LIKE '0104%' OR
			ATD.vcAccountCode LIKE '0106%')
	WHERE 
	PL.bitIsSubAccount=0
	GROUP BY 
	ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


	DECLARE @CURRENTPL DECIMAL(20,5) ;
	DECLARE @PLOPENING DECIMAL(20,5);
	DECLARE @PLCHARTID NUMERIC(8)
	DECLARE @TotalIncome DECIMAL(20,5);
	DECLARE @TotalExpense DECIMAL(20,5);
	DECLARE @TotalCOGS DECIMAL(20,5);
	DECLARE  @CurrentPL_COA DECIMAL(20,5)
	SET @CURRENTPL =0;	
	SET @PLOPENING=0;

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
	bitProfitLoss=1;

	SELECT @CurrentPL_COA =  ISNULL(sum(numCreditAmt),0)-ISNULL(sum(numDebitAmt),0) FROM @TEMP VJ WHERE numAccountId=@PLCHARTID;

	SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0103')

	SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0104')

	SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0106')

	PRINT 'CurrentPL_COA = ' + CAST(@CurrentPL_COA AS VARCHAR(20))
	PRINT 'TotalIncome = ' + CAST(@TotalIncome AS VARCHAR(20))
	PRINT 'TotalExpense = ' + CAST(@TotalExpense AS VARCHAR(20))
	PRINT 'TotalCOGS = ' + CAST(@TotalCOGS AS VARCHAR(20))

	SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
	PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

	SET @CURRENTPL=@CURRENTPL * (-1)

	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID 
	AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
	AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

	SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(numCreditAmt),0)-ISNULL(sum(numDebitAmt),0) FROM General_Journal_Header GJH INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id=GJD.numJournalId WHERE GJH.numDomainID=@numDomainID
	AND GJD.numChartAcntId=@PLCHARTID AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

	SET @CURRENTPL=@CURRENTPL * (-1)

	PRINT '@PLOPENING = ' + CAST(@PLOPENING AS VARCHAR(20))
	PRINT '@CURRENTPL = ' + CAST(@CURRENTPL AS VARCHAR(20))
	PRINT '-(@PLOPENING + @CURRENTPL) = ' + CAST((@PLOPENING + @CURRENTPL) AS VARCHAR(20))

	IF (@strSortOn = 'ID' AND @strSortDirection = 'ASCENDING') 
    BEGIN  

        SELECT  c.numAccountId,
                [numAccountTypeID],
                c.[numParntAcntTypeId],
                c.[vcAccountCode],
                ISNULL(c.[vcAccountCode], '') + ' ~ '
                + c.vcAccountName AS vcAccountName,
                c.vcAccountName AS vcAccountName1,
                AT.[vcAccountType],
                CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
						WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
						ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
							WHERE COA.numDomainId=@numDomainId 
							AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0)))
				END AS [numOpeningBal],	 
                ISNULL(c.numParentAccId,0) AS [numParentAccId],
                COA.vcAccountName AS [vcParentAccountName],
                (SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
                ISNULL(c.intLevel,1) AS [intLevel]
				,ISNULL(c.bitActive,0) bitActive
        FROM    Chart_Of_Accounts c
		LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
		LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
		OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM     @TEMP1 VJ
                            WHERE 
                                    VJ.numAccountID = c.numAccountID 
								AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                ) AS t1
        WHERE   c.numDomainId = @numDomainId
        --AND c.bitActive = 0
        ORDER BY numAccountID ASC  
  
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)	
								ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
										WHERE COA.numDomainId=@numDomainId 
										AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
							END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]                        
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM     @TEMP1 VJ
                            WHERE VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')) AS t1
        WHERE   c.numDomainId = @numDomainID
        --AND c.bitActive = 0
        ORDER BY c.numAccountID ASC
    END  
  
    IF (@strSortOn = 'DESCRIPTION' AND @strSortDirection = 'ASCENDING') 
    BEGIN         
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								ELSE (SELECT SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
										WHERE COA.numDomainId=@numDomainId 
										AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
							END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]                      
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM  @TEMP1 VJ
                            WHERE VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')) AS t1

        WHERE   c.numDomainId = @numDomainId
            AND c.[numParntAcntTypeId] IS NULL
            --AND c.bitActive = 0
        ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
  
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE  CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA WHERE COA.numDomainId=@numDomainId AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM  @TEMP1 VJ
                            WHERE   VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                    ) AS t1
        WHERE   c.numDomainId = @numDomainID
                AND c.[numParntAcntTypeId] IS NOT NULL
                --AND c.bitActive = 0
        ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
                
	END  
END
GO
	
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteDomain')
DROP PROCEDURE USP_DeleteDomain
GO
CREATE PROCEDURE USP_DeleteDomain
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF ISNULL(@numDomainID,0)=0
	BEGIN
		RAISERROR('Domain id required',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=-255 OR @numDomainID=-1
	BEGIN
		RAISERROR('Verry funny.. can''t delete system account',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=1
	BEGIN
		RAISERROR('Verry funny.. can''t delete yourself',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=72
	BEGIN
		raiserror('Verry funny.. can''t delete developers test account',16,1);
		RETURN ;
	END

	/*Allow deletion only for suspended subscription*/
	IF exists (SELECT * FROM [Subscribers] WHERE bitActive = 1 AND numTargetDomainID = @numDomainID)
	BEGIN
		raiserror('Can''t delete active subscription',16,1);
		RETURN ;
	END

	BEGIN TRY
	--BEGIN TRANSACTION
		DELETE FROM General_Journal_Details WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM General_Journal_Header WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityRecurring WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCost WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CaseOpportunities WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CreditBalanceHistory WHERE ISNULL(numDomainID,0)=@numDomainID

		DELETE 
			SRI 
		FROM 
			ShippingReportItems SRI 
		INNER JOIN 
			ShippingBox SB 
		ON 
			SRI.numBoxID = SB.numBoxID 
		INNER JOIN 
			ShippingReport SR 
		ON 
			SB.numShippingReportID = SR.numShippingReportID 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			SR.[numOppBizDocId] = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			SB 
		FROM 
			ShippingBox SB 
		INNER JOIN 
			ShippingReport SR 
		ON 
			SB.numShippingReportID = SR.numShippingReportID 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			SR.[numOppBizDocId] = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			OBDI 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			OBDI.numOppBizDocID = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE OIRLR FROM OpportunityItemsReceievedLocationReturn OIRLR INNER JOIN ReturnItems RI ON OIRLR.numReturnItemID=RI.numReturnItemID INNER JOIN ReturnHeader RH ON RI.numReturnHeaderID=RH.numReturnHeaderID WHERE RH.numDomainID=@numDomainID
		DELETE OIRLR FROM OpportunityItemsReceievedLocationReturn OIRLR INNER JOIN ReturnHeader RH ON OIRLR.numReturnID=RH.numReturnHeaderID WHERE RH.numDomainID=@numDomainID
		DELETE OKCI FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityMaster OM ON OKCI.numOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
	
		DELETE 
			RT 
		FROM 
			RecurrenceTransaction RT 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			RT.numRecurrOppBizDocID=OBD.numOppBizDocsId 
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			OBTI 
		FROM 
			OpportunityBizDocTaxItems OBTI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBTI.numOppBizDocID=OBD.numOppBizDocsId 
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0)=@numDomainID

		DELETE SAD FROM StageAccessDetail SAD INNER JOIN AdditionalContactsInformation ACI ON SAD.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE RTR FROM RecurringTransactionReport RTR INNER JOIN OpportunityBizDocs OBD ON RTR.numRecTranBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTR FROM RecurringTransactionReport RTR INNER JOIN OpportunityMaster OM ON RTR.numRecTranOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTH FROM RecurrenceTransactionHistory RTH INNER JOIN OpportunityBizDocs OBD ON RTH.numRecurrOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTH FROM RecurrenceTransactionHistory RTH INNER JOIN OpportunityMaster OM ON RTH.numRecurrOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PT FROM ProjectTeam PT INNER JOIN AdditionalContactsInformation ACI ON PT.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE PTR FROM ProjectTeamRights PTR INNER JOIN ProjectsMaster PM ON PTR.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PPSD FROM ProjectProcessStageDetails PPSD INNER JOIN ProjectsMaster PM ON PPSD.numProjectId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PPSD FROM ProjectProcessStageDetails PPSD INNER JOIN OpportunityMaster OM ON PPSD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PT FROM PricingTable PT INNER JOIN PriceBookRules PBR ON PT.numPriceRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PT FROM PricingTable PT INNER JOIN Item I ON PT.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE PBRD FROM PriceBookRuleDTL PBRD INNER JOIN PriceBookRules PBR ON PBRD.numRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PMGS FROM PageMasterGroupSetting PMGS INNER JOIN AuthenticationGroupMaster AGM ON PMGS.numGroupID=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE OA FROM OrgOppAddressModificationHistory OA INNER JOIN UserMaster UM ON OA.numModifiedBy=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE OMTI FROM OpportunityMasterTaxItems OMTI INNER JOIN OpportunityMaster OM ON OMTI.numOppID=OM.numOppID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE OBDPD FROM OpportunityBizDocsPaymentDetails OBDPD INNER JOIn OpportunityBizDocsDetails OBD ON OBDPD.numBizDocsPaymentDetId=OBD.numBizDocsPaymentDetId WHERE ISNULL(OBD.numDomainID,0)=@numDomainID
		DELETE OIL FROM OpportunityItemLinking OIL INNER JOIN OpportunityMaster OM ON OIL.numNewOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OITI FROM OpportunityItemsTaxItems OITI INNER JOIN OpportunityMaster OM ON OITI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OITI FROM OpportunityItemsTaxItems OITI INNER JOIN ReturnHeader RH ON OITI.numReturnHeaderID=RH.numReturnHeaderID WHERE ISNULL(RH.numDomainID,0)=@numDomainID
		DELETE OBDKCI FROM OpportunityBizDocKitChildItems OBDKCI INNER JOIN OpportunityBizDocs OBD ON OBDKCI.numOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OBDKI FROM OpportunityBizDocKitItems OBDKI INNER JOIN OpportunityBizDocs OBD ON OBDKI.numOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE MSOL FROM MassSalesOrderLog MSOL INNER JOIN OpportunityMaster OM ON MSOL.numCreatedOrderID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE MSOL FROM MassSalesOrderQueue MSOL INNER JOIN OpportunityMaster OM ON MSOL.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE EHAD FROM EmailHstrAttchDtls EHAD INNER JOIN EmailHistory EH ON EHAD.numEmailHstrID=EH.numEmailHstrID WHERE ISNULL(EH.numDomainID,0)=@numDomainID
		DELETE EHCB FROM EmailHStrToBCCAndCC EHCB INNER JOIN EmailHistory EH ON EHCB.numEmailHstrID=EH.numEmailHstrID WHERE ISNULL(EH.numDomainID,0)=@numDomainID
		DELETE DW FROM DocumentWorkflow DW INNER JOIN AdditionalContactsInformation ACI ON DW.numContactID=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE DTT FROM DivisionTaxTypes DTT INNER JOIN DivisionMaster DM ON DTT.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE DMSA FROM DivisionMasterShippingAccount DMSA INNER JOIN DivisionMaster DM ON DMSA.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE BCPD FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BDC ON BCPD.numComissionID=BDC.numComissionID WHERE ISNULL(BDC.numDomainID,0)=@numDomainID
		DELETE RT FROM RecurrenceTransaction RT INNER JOIN OpportunityMaster OM ON RT.numRecurrOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE BAD FROM BizActionDetails BAD INNER JOIN OpportunityBizDocs OBD ON BAD.numOppBizDocsId=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OA FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID=OM.numOppID WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OBD FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OC FROM OpportunityContact OC INNER JOIN OpportunityMaster OM ON OC.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OD FROM OpportunityDependency OD INNER JOIN OpportunityMaster OM ON OD.numOpportunityId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OKI FROM OpportunityKitItems OKI INNER JOIN OpportunityMaster OM ON OKI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OL FROM OpportunityLinking OL INNER JOIN OpportunityMaster OM ON OL.numChildOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OL FROM OpportunityLinking OL INNER JOIN OpportunityMaster OM ON OL.numParentOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OSSD FROM OpportunitySubStageDetails OSSD INNER JOIN OpportunityMaster OM ON OSSD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OWSI FROM OppWarehouseSerializedItem OWSI INNER JOIN OpportunityMaster OM ON OWSI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE R FROM Returns R INNER JOIN OpportunityMaster OM ON R.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE AA FROM ActivityAttendees AA INNER JOIN ActivityResource AR ON AA.ActivityID=AR.ActivityID INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE AR FROM ActivityResource AR INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE BEL FROM BroadcastErrorLog BEL INNER JOIN BroadCastDtls BD ON BEL.numBroadcastDtlID=BD.numBroadCastDtlId INNER JOIN Broadcast B ON BD.numBroadCastID=B.numBroadCastId WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CFD FROM CFW_Fld_Dtl CFD INNER JOIN CFW_Fld_Master CFM ON CFD.numFieldId=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFV FROM CFW_FLD_Values CFV INNER JOIN CFW_Fld_Master CFM ON CFV.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVA FROM CFW_Fld_Values_Attributes CFVA INNER JOIN CFW_Fld_Master CFM ON CFVA.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVC FROM CFW_FLD_Values_Case CFVC INNER JOIN CFW_Fld_Master CFM ON CFVC.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVC FROM CFW_FLD_Values_Cont CFVC INNER JOIN CFW_Fld_Master CFM ON CFVC.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVI FROM CFW_FLD_Values_Item CFVI INNER JOIN CFW_Fld_Master CFM ON CFVI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVIE FROM CFW_FLD_Values_Item_EDI CFVIE INNER JOIN CFW_Fld_Master CFM ON CFVIE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVO FROM CFW_Fld_Values_Opp CFVO INNER JOIN CFW_Fld_Master CFM ON CFVO.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOE FROM CFW_Fld_Values_Opp_EDI CFVOE INNER JOIN CFW_Fld_Master CFM ON CFVOE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOI FROM CFW_Fld_Values_OppItems CFVOI INNER JOIN CFW_Fld_Master CFM ON CFVOI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOIE FROM CFW_Fld_Values_OppItems_EDI CFVOIE INNER JOIN CFW_Fld_Master CFM ON CFVOIE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVP FROM CFW_Fld_Values_Product CFVP INNER JOIN CFW_Fld_Master CFM ON CFVP.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVP FROM CFW_FLD_Values_Pro CFVP INNER JOIN CFW_Fld_Master CFM ON CFVP.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVSI FROM CFW_Fld_Values_Serialized_Items CFVSI INNER JOIN CFW_Fld_Master CFM ON CFVSI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CRD FROM ChecksRecurringDetails CRD INNER JOIN CheckHeader CH ON CRD.numCheckId=CH.numCheckHeaderID WHERE ISNULL(CH.numDomainID,0)=@numDomainID
		DELETE C FROM Comments C INNER JOIN StagePercentageDetails SPD ON C.numStageID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE C FROM Comments C INNER JOIN Cases SPD ON C.numCaseID=SPD.numCaseId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE CED FROM ConECampaignDTL CED INNER JOIN ECampaignDTLs ED ON CED.numECampDTLID=ED.numECampDTLId INNER JOIN ECampaign E ON ED.numECampID=E.numECampaignID WHERE ISNULL(E.numDomainID,0)=@numDomainID
		DELETE DD FROM DepositeDetails DD INNER JOIN DepositMaster DM ON DD.numDepositID=DM.numDepositId WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE EIC FROM ECommerceItemClassification EIC INNER JOIN EcommerceRelationshipProfile ERP ON EIC.numECommRelatiionshipProfileID=ERP.ID INNER JOIN Sites S ON ERP.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE OI FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PC FROM ProjectsContacts PC INNER JOIN ProjectsMaster PM ON PC.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PD FROM ProjectsDependency PD INNER JOIN ProjectsMaster PM ON PD.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PO FROM ProjectsOpportunities PO INNER JOIN ProjectsMaster PM ON PO.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PSSD FROM ProjectsSubStageDetails PSSD INNER JOIN ProjectsMaster PM ON PSSD.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE RI FROM ReturnItems RI INNER JOIN ReturnHeader RH ON RI.numReturnHeaderID=RH.numReturnHeaderID WHERE ISNULL(RH.numDomainID,0)=@numDomainID
		DELETE SD FROM StageDependency SD INNER JOIN StagePercentageDetails SPD ON SD.numStageDetailID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE SD FROM StageDependency SD INNER JOIN StagePercentageDetails SPD ON SD.numDependantOnID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE SSD FROM StyleSheetDetails SSD INNER JOIN SitePages SP ON SSD.numPageID=SP.numPageID WHERE ISNULL(SP.numDomainID,0)=@numDomainID
		DELETE SSD FROM StyleSheetDetails SSD INNER JOIN StyleSheets SS ON SSD.numCssID=SS.numCssID WHERE ISNULL(SS.numDomainID,0)=@numDomainID
		DELETE SRC FROM SurveyRespondentsChild SRC INNER JOIN SurveyRespondentsMaster SRM ON SRC.numRespondantID=SRM.numRespondantID WHERE ISNULL(SRM.numDomainID,0)=@numDomainID
		DELETE WID FROM WareHouseItmsDTL WID INNER JOIN WareHouseItems WI ON WID.numWareHouseItemID=WI.numWareHouseItemID WHERE ISNULL(WI.numDomainID,0)=@numDomainID
		DELETE WFAUF FROM WorkFlowActionUpdateFields WFAUF INNER JOIN WorkFlowActionList WFASL ON WFAUF.numWFActionID = WFASL.numWFActionID INNER JOIN WorkFlowMaster WFM ON WFASL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE A FROM Activity A INNER JOIN ActivityResource AR ON A.ActivityID=AR.ActivityID INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE AIC FROM AssembledItemChilds AIC INNER JOIN Item I ON AIC.numAssembledItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE AIC FROM AssembledItemChilds AIC INNER JOIN Item I ON AIC.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE BRMRC FROM BankReconcileMatchRuleCondition BRMRC INNER JOIN BankReconcileMatchRule BRMR ON BRMRC.numRuleID=BRMR.ID WHERE ISNULL(BRMR.numDomainID,0)=@numDomainID
		DELETE BD FROM BillDetails BD INNER JOIN BillHeader BH ON BD.numBillID=BH.numBillID WHERE ISNULL(BH.numDomainID,0)=@numDomainID
		DELETE BCD FROM BroadCastDtls BCD INNER JOIN Broadcast B ON BCD.numBroadCastID=B.numBroadCastId  WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CC FROM CaseContacts CC INNER JOIN Cases C ON CC.numCaseID=C.numCaseId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CS FROM CaseSolutions CS  INNER JOIN Cases C ON CS.numCaseID=C.numCaseId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CPS FROM CategoryProfileSites CPS INNER JOIN Sites S ON CPS.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE CRD FROM CommissionRuleDtl CRD INNER JOIN CommissionRules CR ON CRD.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRI FROM CommissionRuleItems CRI INNER JOIN CommissionRules CR ON CRI.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRO FROM CommissionRuleOrganization CRO INNER JOIN CommissionRules CR ON CRO.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CLO FROM CommunicationLinkedOrganization CLO INNER JOIN Communication C ON CLO.numCommID=C.numCommId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CEC FROM ConECampaign CEC INNER JOIN ECampaign EC ON CEC.numECampaignID=EC.numECampaignID WHERE ISNULL(EC.numDomainID,0)=@numDomainID
		DELETE CCCI FROM CustomerCreditCardInfo CCCI INNER JOIN AdditionalContactsInformation ACI ON CCCI.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE CQRE FROM CustomQueryReportEmail CQRE INNER JOIN ReportListMaster RLM ON CQRE.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE DFIC FROM DemandForecastItemClassification DFIC INNER JOIN DemandForecast DF ON DFIC.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE DFIG FROM DemandForecastItemGroup DFIG INNER JOIN DemandForecast DF ON DFIG.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE DFW FROM DemandForecastWarehouse DFW INNER JOIN DemandForecast DF ON DFW.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE ECD FROM ECampaignDTLs ECD INNER JOIN ECampaign EC ON ECD.numECampID=EC.numECampaignID WHERE ISNULL(EC.numDomainID,0)=@numDomainID
		DELETE ERP FROM EcommerceRelationshipProfile ERP INNER JOIN Sites S ON ERP.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE FRD FROM FieldRelationshipDTL FRD INNER JOIN FieldRelationship FR ON FRD.numFieldRelID=FR.numFieldRelID WHERE ISNULL(FR.numDomainID,0)=@numDomainID
		DELETE FRD FROM FinancialReportDetail FRD INNER JOIN FinancialReport FR ON FRD.numFRID=FR.numFRID WHERE ISNULL(FR.numDomainID,0)=@numDomainID
		DELETE IFR FROM Import_File_Report IFR INNER JOIN Import_File_Master IFM ON IFR.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE MBD FROM MarketBudgetDetails MBD INNER JOIN MarketBudgetMaster MBM ON MBD.numMarketId=MBM.numMarketBudgetId WHERE ISNULL(MBM.numDomainID,0)=@numDomainID
		DELETE MSFWMS FROM MassSalesFulfillmentWMState MSFWMS INNER JOIN MassSalesFulfillmentWM MSFWM ON MSFWMS.numMSFWMID=MSFWM.ID  WHERE ISNULL(MSFWM.numDomainID,0)=@numDomainID
		DELETE MSFWP FROM MassSalesFulfillmentWP MSFWP INNER JOIN MassSalesFulfillmentWM MSFWM ON MSFWP.numMSFWMID=MSFWM.ID  WHERE ISNULL(MSFWM.numDomainID,0)=@numDomainID
		DELETE OBD FROM OperationBudgetDetails OBD INNER JOIN OperationBudgetMaster OBM ON OBD.numBudgetID=OBM.numBudgetID WHERE ISNULL(OBM.numDomainID,0)=@numDomainID
		DELETE OARD FROM OrderAutoRuleDetails OARD INNER JOIN OrderAutoRule OAR ON OARD.numRuleID=OAR.numRuleID WHERE ISNULL(OAR.numDomainID,0)=@numDomainID	
		DELETE PBRI FROM PriceBookRuleItems PBRI INNER JOIN PriceBookRules PBR ON PBRI.numRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PGD FROM ProfileEGroupDTL PGD INNER JOIN ProfileEmailGroup PEG ON PGD.numEmailGroupID=PEG.numEmailGroupID  WHERE ISNULL(PEG.numDomainID,0)=@numDomainID
		DELETE POO FROM PromotionOfferOrganizations POO INNER JOIN PromotionOffer PO ON POO.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POS FROM PromotionOfferSites POS INNER JOIN PromotionOffer PO ON POS.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE RP FROM ResourcePreference RP INNER JOIN Resource R ON RP.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE RD FROM ReviewDetail RD INNER JOIN Review R ON RD.numReviewId=R.numReviewId WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE RLD FROM RoutingLeadDetails RLD INNER JOIN RoutingLeads RL ON RLD.numRoutID=RL.numRoutID WHERE ISNULL(RL.numDomainID,0)=@numDomainID
		DELETE RLV FROM RoutinLeadsValues RLV INNER JOIN RoutingLeads RL ON RLV.numRoutID=RL.numRoutID WHERE ISNULL(RL.numDomainID,0)=@numDomainID
		DELETE SLRC FROM ShippingLabelRuleChild SLRC INNER JOIN ShippingLabelRuleMaster SLRM ON SLRC.numShippingRuleID=SLRM.numShippingRuleID WHERE ISNULL(SLRM.numDomainID,0)=@numDomainID
		DELETE SC FROM SiteCategories SC INNER JOIN Sites S ON SC.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SSD FROM SiteSubscriberDetails SSD INNER JOIN Sites S ON SSD.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SWC FROM SiteWiseCategories SWC INNER JOIN Sites S ON SWC.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SPDTN FROM StagePercentageDetailsTaskNotes SPDTN INNER JOIN StagePercentageDetailsTask SPDT ON SPDTN.numTaskID=SPDT.numTaskId WHERE ISNULL(SPDT.numDomainID,0)=@numDomainID
		DELETE SAM FROM SurveyAnsMaster SAM INNER JOIN SurveyMaster SM ON SAM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SQM FROM SurveyQuestionMaster SQM INNER JOIN SurveyMaster SM ON SQM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SR FROM SurveyResponse SR INNER JOIN SurveyMaster SM ON SR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SWR FROM SurveyWorkflowRules SWR INNER JOIN SurveyMaster SM ON SWR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE TVD FROM TrackingVisitorsDTL TVD INNER JOIN TrackingVisitorsHDR TVH ON TVD.numTracVisitorsHDRID=TVH.numTrackingID WHERE ISNULL(TVH.numDomainID,0)=@numDomainID
		DELETE WAL FROM WorkFlowActionList WAL INNER JOIN WorkFlowMaster WFM ON WAL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WCL FROM WorkFlowConditionList WCL INNER JOIN WorkFlowMaster WFM ON WCL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WTFL FROM WorkFlowTriggerFieldList WTFL INNER JOIN WorkFlowMaster WFM ON WTFL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WOPI FROM WorkOrderPickedItems WOPI INNER JOIN WorkOrderDetails WOD ON WOPI.numWODetailId=WOD.numWODetailId INNER JOIN WorkOrder WO ON WOD.numWOId=WO.numWOId WHERE ISNULL(WO.numDomainID,0)=@numDomainID
		DELETE WSDO FROM WorkScheduleDaysOff WSDO INNER JOIN WorkSchedule WS ON WSDO.numWorkScheduleID=WS.ID WHERE ISNULL(WS.numDomainID,0)=@numDomainID
		DELETE ACL FROM AOIContactLink ACL INNER JOIN AdditionalContactsInformation ACI ON ACL.numContactID=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE BPD FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID WHERE ISNULL(BPH.numDomainID,0)=@numDomainID
		DELETE BL FROM BroadcastingLink BL INNER JOIN Broadcast B ON BL.numBroadcastId=B.numBroadCastId WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CD FROM CampaignDetails CD INNER JOIN CampaignMaster CM ON CD.numCampaignId=CM.numCampaignId WHERE ISNULL(CM.numDomainID,0)=@numDomainID
		DELETE CD FROM CheckDetails CD INNER JOIN CheckHeader CH ON CD.numCheckHeaderID=CH.numCheckHeaderID WHERE ISNULL(CH.numDomainID,0)=@numDomainID
		DELETE CKR FROM ChildKitsRelation CKR INNER JOIN Item I ON CKR.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE CRC FROM CommissionRuleContacts CRC INNER JOIN CommissionRules CR ON CRC.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CA FROM CommunicationAttendees CA INNER JOIN Communication C ON CA.numCommId=C.numCommId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CL FROM ContractsLog CL INNER JOIN Contracts C ON CL.numContractId=C.numContractId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CRF FROM CustReportFields CRF INNER JOIN CustomReport CR ON CRF.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRFL FROM CustReportFilterlist CRFL INNER JOIN CustomReport CR ON CRFL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRK FROM CustReportKPI CRK INNER JOIN CustomReport CR ON CRK.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CROL FROM CustReportOrderlist CROL INNER JOIN CustomReport CR ON CROL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRSL FROM CustReportSummationlist CRSL INNER JOIN CustomReport CR ON CRSL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE D FROM Dashboard D INNER JOIN CustomReport CR ON D.numReportID=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE DAR FROM DashboardAllowedReports DAR INNER JOIn AuthenticationGroupMaster AGM ON DAR.numGrpId=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE DS FROM DashBoardSize DS INNER JOIN UserMaster UM ON DS.numGroupUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE DCU FROM DiscountCodeUsage DCU INNER JOIN DiscountCodes DC ON DCU.numDiscountId=DC.numDiscountId INNER JOIN PromotionOffer PO ON DC.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE DC FROM DiscountCodes DC INNER JOIN PromotionOffer PO ON DC.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE F FROM Favorites F INNER JOIN UserMaster UM ON F.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE FRC FROM ForReportsByCampaign FRC INNER JOIN UserMaster UM ON FRC.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE GTD FROM GroupTabDetails GTD INNER JOIN AuthenticationGroupMaster AGM ON GTD.numGroupId=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE IFFM FROM Import_File_Field_Mapping IFFM INNER JOIN Import_File_Master IFM ON IFFM.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE IH FROM Import_History IH INNER JOIN Import_File_Master IFM ON IH.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE IC FROM ItemCategory IC INNER JOIN Category C ON IC.numCategoryID=C.numCategoryID WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE IC FROM ItemCategory IC INNER JOIN Item I ON IC.numItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE ID FROM ItemDetails ID INNER JOIN Item I ON ID.numChildItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE ID FROM ItemDetails ID INNER JOIN Item I ON ID.numItemKitID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID	
		DELETE IED FROM ItemExtendedDetails IED INNER JOIN Item I ON IED.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE IGD FROM ItemGroupsDTL IGD INNER JOIN ItemGroups IG ON IGD.numItemGroupID=IG.numItemGroupID WHERE ISNULL(IG.numDomainID,0)=@numDomainID
		DELETE IT FROM ItemTax IT INNER JOIN Item I ON IT.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID	
		DELETE MSFBO FROM MassSalesFulfillmentBatchOrders MSFBO INNER JOIN MassSalesFulfillmentBatch MSFB ON MSFBO.numBatchID=MSFB.ID WHERE ISNULL(MSFB.numDomainID,0)=@numDomainID
		DELETE MST FROM MassStockTransfer MST INNER JOIN Item I ON MST.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE MRL FROM MyReportList MRL INNER JOIN UserMaster UM ON MRL.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE OAQE FROM OpportunityAutomationQueueExecution OAQE INNER JOIN OpportunityAutomationQueue OAQ ON OAQE.numOppQueueID=OAQ.numOppQueueID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE PBD FROM ProcurementBudgetDetails PBD INNER JOIN ProcurementBudgetMaster PBM ON PBD.numProcurementId=PBM.numProcurementBudgetId WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE POC FROM PromotionOfferContacts POC INNER JOIN PromotionOffer PO ON POC.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POI FROM PromotionOfferItems POI INNER JOIN PromotionOffer PO ON POI.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POOB FROM PromotionOfferOrderBased POOB INNER JOIN PromotionOffer PO ON POOB.numProID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE RI FROM RecentItems RI INNER JOIN UserMaster UM ON RI.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE RD FROM RemarketDTL RD INNER JOIN Item I ON RD.numItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE RSGL FROM ReportSummaryGroupList RSGL INNER JOIN ReportListMaster RLM ON RSGL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RMAL FROM ReportMatrixAggregateList RMAL INNER JOIN ReportListMaster RLM ON RMAL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RKT FROM ReportKPIThresold RKT INNER JOIN ReportListMaster RLM ON RKT.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RFL FROM ReportFieldsList RFL INNER JOIN ReportListMaster RLM ON RFL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RFL FROM ReportFilterList RFL INNER JOIN ReportListMaster RLM ON RFL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RKGDL FROM ReportKPIGroupDetailList RKGDL INNER JOIN ReportListMaster RLM ON RKGDL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE SRGL FROM ScheduledReportsGroupLog SRGL INNER JOIN ScheduledReportsGroup SRG ON SRGL.numSRGID=SRG.ID WHERE ISNULL(SRG.numDomainID,0)=@numDomainID
		DELETE SRGR FROM ScheduledReportsGroupReports SRGR INNER JOIN ScheduledReportsGroup SRG ON SRGR.numSRGID=SRG.ID WHERE ISNULL(SRG.numDomainID,0)=@numDomainID
		DELETE SMAM FROM SurveyMatrixAnsMaster SMAM INNER JOIN SurveyMaster SM ON SMAM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SCR FROM SurveyCreateRecord SCR INNER JOIN SurveyMaster SM ON SCR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE UR FROM UserRoles UR INNER JOIN UserMaster UM ON UR.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE WFQE FROM WorkFlowQueueExecution WFQE INNER JOIN WorkFlowMaster WFM ON WFQE.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WOD FROM WorkOrderDetails WOD INNER JOIN WorkOrder WO ON WOD.numWOId=WO.numWOId WHERE ISNULL(WO.numDomainID,0)=@numDomainID
		
				
		DELETE FROM SalesTemplateItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingReport WHERE ISNULL(numDomainID,0)=@numDomainID
		
		DELETE FROM EDIQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExtranetAccountsDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityItemsReceievedLocation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityItemsReleaseDates WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunitySalesTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityStageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectProgress WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertEmailDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionContacts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomerPartNumber WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterPromotionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostDefaults WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE EA FROM ExtarnetAccounts EA INNER JOIN DivisionMaster DM ON EA.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE EA FROM ExtarnetAccounts EA INNER JOIN CompanyInfo CI ON EA.numCompanyId=CI.numCompanyId WHERE ISNULL(CI.numDomainID,0)=@numDomainID
		DELETE FROM ExtarnetAccounts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportActionItemReference WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportOrganizationContactReference WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PageElementDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsStageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteBreadCrumb WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteMenu WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Vendor WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM VendorShipmentMethod WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountingCharges WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountTypeDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ApprovalProcessItemsClassification WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileFileData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAlerts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CashCreditCardDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CFW_Fld_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ChartAccountOpening WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckDetails_Old WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COAShippingMapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionPayPeriod WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CompanyAssociations WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContactTypeMapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Correspondence WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DashboardTemplateReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DepositDetails_Old WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DepositMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ErrorDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExportDataSettings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GroupAuthorization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImapUserDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemUOMConversion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ListDetailsName WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalDashboardDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PurchaseOrderFulfillmentSettings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecordHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReturnHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingServiceAbbreviations WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SitePages WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteTemplates WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StyleSheets WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SubscriberHstr WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyRespondentsMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxCountryConfi WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackingCampaignData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountingIntegration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountTypeDetail_delete WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ActivityDisplayConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ActivityFormAuthentication WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdditionalContactsInformation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdditionalContactsInformation_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AddressDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdvSearchCriteria WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdvSerViewConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertDomainDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertHDR WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Approval_transaction_log WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ApprovalConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AssembledItem WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Audit WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthenticationGroupBackOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthenticationGroupMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthoritativeBizDocs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileMatchRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankStatementHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankStatementTransactions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillingTerms WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillPaymentHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizAPIErrorLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAction WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocApprovalRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAttachments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocComission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocFilter WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocStatusApprove WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizFormWizardMasterConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Broadcast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CampaignMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CartItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Cases WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Cases_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Category WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CategoryProfile WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CFw_Grp_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckCompanyDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ClassDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COACreditCardCharge WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COARelationships WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Communication WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Communication_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommunicationBackUP WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CompanyAssets WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Competitor WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContractManagement WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Contracts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContractsContact WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Currency WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomQueryReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomReport26032011 WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustRptScheduler WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DashboardTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DefaultTabs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DeleteThis WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecastDays WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterShippingConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DomainMarketplace WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DomainSFTPDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycCartFilters WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycField_Globalization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycField_Validation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldColorScheme WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldMasterSynonym WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFormConfigurationDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFormField_Mapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFrmConfigBizDocsSumm WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycModuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormAOIConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormConfigurationDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormField_Validation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormFieldMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormMasterParam WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFrmConfigBizDocsSumm WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECampaign WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECampaignAssignee WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECommerceCreditCardTransactionLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM eCommerceDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM eCommercePaymentConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EDIHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EDIQueueLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ElasticSearchBizCart WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ElasticSearchModifiedRecords WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmailBroadcastConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmailBroadcastConfiguration_old WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreEmailHistory:
		DELETE TOP (1000) FROM EmailHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreEmailHistory

		DELETE FROM EmailMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExceptionLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FieldRelationship WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FinancialReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FinancialYear WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FixedAssetDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FollowUpHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Forecast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FormFieldGroupConfigurarion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByAsItsType WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByTeam WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByTerritory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByUser WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GenealEntryAudit WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GenericDocuments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GoogleContactGroups WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM HTMLFormURL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Import_File_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportApiOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InboxTree WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InboxTreeSort WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InitialListColumnConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InventroyReportDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemAPI WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemAttributes WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemCurrencyPrice WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemGroups WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemImages WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemMarketplaceMapping WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM ItemOptionAccAttr WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LandedCostExpenseAccount WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LandedCostVendors WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LeadsubForms WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ListDetails WHERE ISNULL(numDomainID,0)=@numDomainID AND ISNULL(constFlag,0) = 0
		DELETE FROM ListMaster WHERE ISNULL(numDomainID,0)=@numDomainID AND ISNULL(bitFixed,0) = 0
		DELETE FROM ListOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LuceneItemsIndex WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MarketBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassPurchaseFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentBatch WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentShippingConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentWM WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MessageMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM NameTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OperationBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OppFulfillmentBizDocsStatusHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityAutomationQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityAutomationRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityBizDocsDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMasterAPI WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMasterShippingRateError WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityOrderStatus WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OrderAutoRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OrganizationRatingRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OverrideAssignTo WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PackagingRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PageLayoutDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ParentChildCustomFieldMap WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PaymentGatewayDTLID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollTracking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalBizDocs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalWorkflow WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PriceBookRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PricingNamesTable WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProcurementBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProfileEmailGroup WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PromotionOffer WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PromotionOfferOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PurchaseIncentives WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Ratings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecImportConfg WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecordOrganizationChangeHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurrenceConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurrenceErrorLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurringTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RedirectConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RelationsDefaultFilter WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashboard WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashboardAllowedReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashBoardSize WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportKPIGroupListMaster WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM ReportListMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Resource WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Review WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RoutingLeads WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleAction WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleCondition WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Sales_process_List_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderLineItemsPOLinking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesReturnCommission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SavedSearch WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ScheduledReportsGroup WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShareRecord WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingExceptions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingFieldValues WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingLabelRuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingPromotions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingProviderForEComerce WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingRuleStateList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingService WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingServiceTypes WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutBar WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutGrpConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutUsrCnf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SignatureDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SimilarItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Sites WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SMTPUserDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SolutionMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SpecificDocuments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetails_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTask WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTaskCommission WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM State WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TabDefault WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TabMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM tblActionItemData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM tblStageGradeDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TicklerListFilterConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TimeAndExpense WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TimeAndExpenseCommission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TopicMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TopicMessageAttachments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackingVisitorsHDR WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackNotification WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TransactionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TreeNavigationAuthorization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TreeNodeOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrueCommerceLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UnitPriceApprover WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UniversalSMTPIMAP WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UOM WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UOMConversion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserAccessedDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserReportList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserTeams WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserTerritory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Vendortemp WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseForSalesFulfillment WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseItems_Tracking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebAPIDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOppItemDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOrderDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOrderReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebService WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowAlertList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowARAgingExecutionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkflowAutomation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowDateFieldExecutionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkSchedule WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreOpportunityMaster:
		DELETE TOP (1000) FROM OpportunityMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreOpportunityMaster

		deleteMoreWareHouseItems:
		DELETE TOP (1000) FROM WareHouseItems WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreWareHouseItems

		deleteMoreItem:
		DELETE TOP (1000) FROM Item WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreItem

		DELETE FROM Warehouses WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WarehouseLocation WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreDivisionMaster:
		DELETE TOP (1000) FROM DivisionMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreDivisionMaster

		deleteMoreCompanyInfo:
		DELETE TOP (1000) CompanyInfo WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreCompanyInfo

		DELETE FROM Chart_Of_Accounts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterBizDocEmail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Domain WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Subscribers WHERE numTargetDomainID=@numDomainID
	--COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		--IF @@TRANCOUNT > 0
		--	ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterBizDocEmail_GetByOppBizDocID')
DROP PROCEDURE dbo.USP_DivisionMasterBizDocEmail_GetByOppBizDocID
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterBizDocEmail_GetByOppBizDocID]
(
	@numDomainID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		(CASE WHEN LEN(ISNULL(DMBE.vcTo,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcTo) ELSE '' END) vcTo
		,(CASE WHEN LEN(ISNULL(DMBE.vcCC,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcCC) ELSE '' END) vcCC
		,(CASE WHEN LEN(ISNULL(DMBE.vcBCC,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcBCC) ELSE '' END) vcBCC
	FROM	
		OpportunityBizDocs OBD
	INNER JOIN
		OpportunityMaster OM
	ON
		OBD.numOppId = OM.numOppId
	INNER JOIN
		DivisionMasterBizDocEmail DMBE
	ON
		OM.numDivisionId = DMBE.numDivisionID
		AND OBD.numBizDocId = DMBE.numBizDocID
	INNER JOIN
		Domain D
	ON
		DMBE.numDomainID = D.numDomainId
	WHERE
		OBD.numOppBizDocsId = @numOppBizDocID
		AND ISNULL(D.bitUsePreviousEmailBizDoc,0) = 1
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetColumnValue')
DROP PROCEDURE USP_Domain_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'tintCommitAllocation'
	BEGIN
		SELECT ISNULL(tintCommitAllocation,1) AS tintCommitAllocation FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitEDI'
	BEGIN
		SELECT ISNULL(bitEDI,0) AS bitEDI FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitReOrderPoint'
	BEGIN
		SELECT ISNULL(bitReOrderPoint,0) AS bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountOption'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountOption,0) AS tintMarkupDiscountOption FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountValue'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountValue,0) AS tintMarkupDiscountValue FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitDoNotShowDropshipPOWindow'
	BEGIN
		SELECT ISNULL(bitDoNotShowDropshipPOWindow,0) AS bitDoNotShowDropshipPOWindow FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintReceivePaymentTo'
	BEGIN
		SELECT ISNULL(tintReceivePaymentTo,0) AS tintReceivePaymentTo FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'numReceivePaymentBankAccount'
	BEGIN
		SELECT ISNULL(numReceivePaymentBankAccount,0) AS numReceivePaymentBankAccount FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitCloneLocationWithItem'
	BEGIN
		SELECT ISNULL(bitCloneLocationWithItem,0) AS bitCloneLocationWithItem FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePaySuccessURL'
	BEGIN
		SELECT ISNULL(vcBluePaySuccessURL,0) AS vcBluePaySuccessURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePayDeclineURL'
	BEGIN
		SELECT ISNULL(vcBluePayDeclineURL,0) AS vcBluePayDeclineURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitUseDeluxeCheckStock'
	BEGIN
		SELECT ISNULL(bitUseDeluxeCheckStock,0) AS bitUseDeluxeCheckStock FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcPOHiddenColumns'
	BEGIN
		SELECT ISNULL(vcPOHiddenColumns,0) AS vcPOHiddenColumns FROM SalesOrderConfiguration WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintReorderPointBasedOn'
	BEGIN
		SELECT ISNULL(tintReorderPointBasedOn,1) AS tintReorderPointBasedOn FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'intReorderPointDays'
	BEGIN
		SELECT ISNULL(intReorderPointDays,30) AS intReorderPointDays FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'intReorderPointPercent'
	BEGIN
		SELECT ISNULL(intReorderPointPercent,0) AS intReorderPointPercent FROM Domain WHERE numDomainId=@numDomainID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging')
DROP PROCEDURE USP_GetAccountReceivableAging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
	@numDomainId   AS NUMERIC(9)  = 0,
	@numDivisionId AS NUMERIC(9)  = NULL,
	@numUserCntID AS NUMERIC(9),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME = NULL,
	@dtToDate AS DATETIME = NULL,
	@ClientTimeZoneOffset INT = 0,
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
				OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = Opp.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= CAST(@dtToDate AS DATE)
				) TablePayments
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND (CASE 
						WHEN ISNULL(@bitCustomerStatement,0) = 1 
						THEN CONVERT(DATE,OB.[dtCreatedDate]) 
						ELSE CONVERT(DATE,DATEADD(DAY,CASE 
														WHEN Opp.bitBillingTerms = 1 AND @tintBasedOn=1
														THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
														ELSE 0 
													END, dtFromDate)) 
						END) <= @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate
				,TablePayments.monPaidAmount
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON [DM].[numDivisionID] = DIV.[numDivisionID]
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numCurrentDays],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intCurrentDaysCount],
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numCurrentDaysPaid],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
				0 numCurrentDays,
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
				0 [intCurrentDaysCount],
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numCurrentDaysPaid,
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

UPDATE  TempARRecord1
SET     numCurrentDays = X.numCurrentDays
FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numCurrentDaysPaid = X.numCurrentDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate])>= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
    FROM      [TempARRecord]
    WHERE     numUserCntID =@numUserCntID AND 
			 DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
			  AND 
			  numCurrencyID <>@baseCurrency )

---------------------------------------------------------------------------------------------


-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=  isnull(numCurrentDays,0) + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numCurrentDays],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intCurrentDaysCount],
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numCurrentDaysPaid],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Details]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Details')
DROP PROCEDURE USP_GetAccountReceivableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Details]
(
    @numDomainId AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @strSql VARCHAR(8000);
	Declare @strSql1 varchar(8000);
	Declare @strCredit varchar(8000); SET @strCredit=''
	
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
    IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    --Get Master data
	 
	 SET @strSql = '
     SELECT DISTINCT OM.[numOppId],
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.[vcPOppName],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 ,0) TotalAmount,--OM.[monPAmount]
                        (ISNULL(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        ISNULL(OB.monDealAmount * ISNULL(OM.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(OM.fltExchangeRate, 1),0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],
                        CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.[numDivisionId]
        FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND (DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' OR ' + CAST(@numDivisionID AS VARCHAR) + '=0)
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1  AND ' + CAST(@tintBasedOn AS VARCHAR) + ' = 1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0'
    
    	--Show Impact of AR journal entries in report as well 
    	
SET @strSql1 = '    	
		UNION 
		 SELECT 
				-1 AS [numOppId],
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' [vcPOppName],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''				
    
   SET @strCredit = ' UNION SELECT -1 AS [numOppId],
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
								0 [numOppBizDocsId],
								'''' [vcBizDocID],
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' [varCurrSymbol],
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)'  
    
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'

		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'
                             
                    END
                  ELSE
                    BEGIN
                      SET @strSql =@strSql +''
                    END
                    



	SET @strSql =@strSql + @strSql1 + @strCredit
	PRINT @strSql
	EXEC(@strSql)
  END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = '',
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND '+ (CASE 
							WHEN ISNULL(@bitCustomerStatement,0) = 1 
							THEN 'CONVERT(DATE,OB.[dtCreatedDate])' 
							ELSE 'CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 AND ' + CAST(@tintBasedOn AS VARCHAR) + '=1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
									END, dtFromDate))' 
						END) + ' <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND isnull(OB.monDealAmount * OM.fltExchangeRate,0) != 0 '     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'   
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END
 
   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocAttachmnts]    Script Date: 07/26/2008 16:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--exec USP_GetBizDocAttachmnts,
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocattachmnts')
DROP PROCEDURE usp_getbizdocattachmnts
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocAttachmnts]      
 @numBizDocID as numeric(9)=0,      
 @numDomainId as numeric(9)=0,      
 @numAttachmntID as numeric(9)=0,
 @numBizDocTempID as numeric(9)=0     ,
 @intAddReferenceDocument AS NUMERIC(10)=0
as      
       
if (@numAttachmntID=0 and   @numBizDocID <>0)  
begin    
    
 if not exists(select * from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and isnull(numBizDocTempID,0)=@numBizDocTempID)        
  begin        
   insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)        
   values (@numBizDocID,@numDomainID,'BizDoc',1,'BizDoc',@numBizDocTempID)        
  end       
     
 
  IF(@intAddReferenceDocument=1)
  BEGIN
	 (select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder from BizDocAttachments       
	 where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID)
	 UNION
	 (SELECT numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder from BizDocAttachments       
	 where vcURL<>'BizDoc' AND numBizDocID=(SELECT TOP 1 numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=(SELECT TOP 1 numBizDocTempID FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID)  )
	order by numOrder 
  END
  ELSE
  BEGIN
	 select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID from BizDocAttachments       
  where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID order by numOrder      
  END
end    
else if @numAttachmntID>0      
      
select * from BizDocAttachments where numAttachmntID=@numAttachmntID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocType')
DROP PROCEDURE USP_GetBizDocType
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocType]                
@BizDocType as NUMERIC(9)=0,                  
@numDomainID as numeric(9)=0                   
as                    

IF @numDomainID=0 
BEGIN
	select vcData,numListItemID from listdetails where numlistid=27 AND ([numListItemID] =@BizDocType OR @BizDocType =0)
END

ELSE
BEGIN

IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
	EXEC USP_GetMasterListItems 27,@numDomainID
END
ELSE
BEGIN
  SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
  FROM BizDocFilter BDF INNER JOIN listdetails Ld ON BDF.numBizDoc=Ld.numListItemID
  left join listorder LO on Ld.numListItemID= LO.numListItemID AND Lo.numDomainId = @numDomainID
  WHERE Ld.numListID=27 AND BDF.tintBizocType=@BizDocType AND BDF.numDomainID=@numDomainID 
  AND (constFlag=1 or Ld.numDomainID=@numDomainID) ORDER BY LO.intSortOrder 
 END
END
GO
   
/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetails]    Script Date: 09/25/2009 15:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Created by Siva        
-- [USP_GetChartAcntDetails] @numDomainId=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getchartacntdetails' ) 
    DROP PROCEDURE usp_getchartacntdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetails]
    @numDomainId AS NUMERIC(9),
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine
    @numAccountClass AS NUMERIC(9)=0
AS 
    BEGIN    
--        DECLARE @numFinYear INT ;
        DECLARE @dtFinYearFromJournal DATETIME ;
		DECLARE @dtFinYearFrom DATETIME ;

--        SET @numFinYear = ( SELECT  numFinYearId
--                            FROM    FINANCIALYEAR
--                            WHERE   dtPeriodFrom <= @dtFromDate
--                                    AND dtPeriodTo >= @dtFromDate
--                                    AND numDomainId = @numDomainId
--                          ) ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0)
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
/*Timezone Logic.. Stored Date are in UTC/GMT  */
--SET @dtToDate = DATEADD(DAY, DATEDIFF(DAY, '19000101',  @dtToDate), '23:59:59');
--SELECT @dtFromDate,@ClientTimeZoneOffset,DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate)
--        SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate) ;

        /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
--        SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate) ;


--PRINT @dtFromDate
--PRINT @dtToDate
--PRINT @numFinYear 
--PRINT @dtFinYearFrom 

--select * from view_journal where numDomainid=72

		
        CREATE TABLE #PLSummary
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening DECIMAL(20,5),
              Debit DECIMAL(20,5),
              Credit DECIMAL(20,5)
              ,bitIsSubAccount BIT,numParentAccID NUMERIC(9),Balance DECIMAL(20,5) 
            ) ;

        INSERT  INTO #PLSummary
                SELECT  COA.numAccountId,
                        vcAccountName,
                        numParntAcntTypeID,
                        vcAccountDescription,
                        vcAccountCode,
                      /*  ISNULL(( SELECT SUM(ISNULL(monOpening, 0))
                                 FROM   CHARTACCOUNTOPENING CAO
                                 WHERE  numFinYearId = @numFinYear
                                        AND numDomainID = @numDomainId
                                        AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )
                               ), 0)
                        + */ 
						CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR 
							 	  COA.[vcAccountCode] LIKE '0104%' OR 
							 	  COA.[vcAccountCode] LIKE '0106%' 
							 THEN 0 
							 ELSE isnull(t1.OPENING,0) 
						END as OPENING,
                        ISNULL(t.DEBIT, 0) AS DEBIT,
                        ISNULL(t.CREDIT, 0) AS CREDIT
                               ,ISNULL(COA.bitIsSubAccount,0),ISNULL(COA.numParentAccID,0) AS numParentAccID,0
                FROM    Chart_of_Accounts COA
					OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                            AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(Minute,-1,@dtFromDate) AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS t1
					OUTER APPLY(SELECT   SUM(Debit) as DEBIT,SUM(Credit) as CREDIT
                                   FROM   #view_journal VJ
                                 WHERE  VJ.numDomainId = @numDomainId
                                        AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                        AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)
                                 ) as t
                    WHERE   COA.numDomainId = @numDomainId  ;
--SELECT @dtFinYearFrom,@dtFromDate - 1,* FROM #PLSummary
        CREATE TABLE #PLOutPut
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening DECIMAL(20,5),
              Debit DECIMAL(20,5),
              Credit DECIMAL(20,5),numParentAccID NUMERIC(9),Balance DECIMAL(20,5) 
            ) ;

        INSERT  INTO #PLOutPut
                SELECT  ATD.numAccountTypeID,
                        ATD.vcAccountType,
                        ATD.numParentID,
                        '',
                        ATD.vcAccountCode,
                        ISNULL(SUM(Opening), 0) AS Opening,
                        ISNULL(SUM(Debit), 0) AS Debit,
                        ISNULL(SUM(Credit), 0) AS Credit,0,0
                FROM    /*Chart_Of_Accounts c JOIN [AccountTypeDetail] ATD ON ATD.[numAccountTypeID] = c.[numParntAcntTypeId]*/
						[AccountTypeDetail] ATD
                        RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE ATD.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(ATD.vcAccountCode)
                        --(PL.vcAccountCode LIKE c.vcAccountCode OR (PL.vcAccountCode LIKE c.vcAccountCode + '%' AND PL.numParentAccID>0))
                                                          AND ATD.numDomainId = @numDomainId
				--WHERE  PL.bitIsSubAccount=0
                GROUP BY ATD.numAccountTypeID,
                        ATD.vcAccountCode,
                        ATD.vcAccountType,
                        ATD.numParentID ;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount


-- GETTING P&L VALUE

DECLARE @CURRENTPL DECIMAL(20,5) ;
DECLARE @PLOPENING DECIMAL(20,5);
DECLARE @PLCHARTID NUMERIC(8)
DECLARE @TotalIncome DECIMAL(20,5);
DECLARE @TotalExpense DECIMAL(20,5);
DECLARE @TotalCOGS DECIMAL(20,5);
DECLARE  @CurrentPL_COA DECIMAL(20,5)
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = /*@CurrentPL_COA +*/ @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

set @CURRENTPL=@CURRENTPL * (-1)

SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

PRINT '@PLOPENING='+ CAST(@PLOPENING AS VARCHAR(20))
PRINT '@CURRENTPL='+ CAST(@CURRENTPL AS VARCHAR(20))
PRINT '-(@PLOPENING + @CURRENTPL)='+ CAST((-(@PLOPENING + @CURRENTPL)) AS VARCHAR(20))


set @CURRENTPL=@CURRENTPL * (-1)

UPDATE #PLSummary SET Opening = CASE WHEN vcAccountCode = '0105010101' THEN -@PLOPENING ELSE Opening END,
Balance = CASE WHEN vcAccountCode = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END
--WHERE vcAccountCode = '0105010101'
                        

UPDATE PO SET Opening = T.Opening,
Debit = T.Debit,
Credit = T.Credit,
Balance = T.Balance
FROM #PLOutPut PO JOIN (SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
/*ISNULL(SUM(PL.Opening), 0) + ISNULL(SUM(PL.Debit), 0) - ISNULL(SUM(PL.Credit), 0)*/ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
GROUP BY PO.vcAccountCode) T ON T.vcAccountCode = PO.vcAccountCode

--SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
--ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
--RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
--GROUP BY PO.vcAccountCoder

--WHERE vcAccountCode = '0105010101'
--------------------------------------------------------
		--UPDATE #PLSummary SET Opening = @PLOPENING WHERE #PLSummary.vcAccountCode = '0105010101'

		SELECT  P.numAccountId,
				P.vcAccountName,
				P.numParntAcntTypeID, 
				P.vcAccountDescription,
				P.vcAccountCode,
				/*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE P.Opening END) AS*/ P.Opening,
				P.Debit ,
				P.Credit,
				P.numParentAccID,
                /*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ P.Balance,
                CASE WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4 ) + P.[vcAccountCode] 
				ELSE P.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(P.[vcAccountCode]) > 4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode]) - 4 ) + P.[vcAccountName] 
                     ELSE P.[vcAccountName]
                END AS vcAccountName1,
                1 AS TYPE
        FROM    #PLSummary P
                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = P.numAccountId
        WHERE   LEN(ISNULL(P.[vcAccountCode], '')) > 2
        UNION
        SELECT  O.numAccountId,
				O.vcAccountName,
				O.numParntAcntTypeID, 
				O.vcAccountDescription,
				O.vcAccountCode,
				/*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE O.Opening END) AS*/ Opening,
				O.Debit ,
				O.Credit,
				O.numParentAccID,
                /*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ Balance,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountCode]
                     ELSE O.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                      THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountName]
                     ELSE O.[vcAccountName]
                END AS vcAccountName1,
                2 AS TYPE
                --CAST(O.[vcAccountCode] AS deci)  vcSortableAccountCode
        FROM    #PLOutPut O
--                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = O.numAccountId
        WHERE   LEN(ISNULL(O.[vcAccountCode], '')) > 2
        ORDER BY vcAccountCode,TYPE 
 
    --SELECT * FROM #PLOutPut ;
    --SELECT * FROM #PLSummary ;

        SELECT  (SUM(Opening) - @PLOPENING) AS OpeningTotal,
                SUM(Debit) AS DebitTotal,
                SUM(Credit) AS CreditTotal,
                (SUM(Opening) + SUM(Debit) - SUM(Credit) - @PLOPENING)AS BalanceTotal
        FROM    [#PLSummary]

        DROP TABLE #PLOutPut ;
        DROP TABLE #PLSummary ;


    END
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN            
	SET @dtFromDate = DATEADD (MS , 000 , @dtFromDate)
	SET @dtToDate = DATEADD (MS , 997 , @dtToDate)
                                                    
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		datEntry_Date DATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	-- OLD CODE COMMENTED DUE TO PERFORMANCE ISSUE
	--INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	INSERT INTO 
		#VIEW_JOURNALBS
	SELECT     
		@numDomainId,
		dbo.General_Journal_Details.numChartAcntId AS numAccountId, 
		ISNULL(dbo.General_Journal_Details.numClassID,0) AS numAccountClass,
		dbo.General_Journal_Header.datEntry_Date,
		SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, 
		SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit
	FROM         
		dbo.General_Journal_Header 
	INNER JOIN
		dbo.General_Journal_Details 
	ON 
		dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId  
	WHERE
		General_Journal_Header.numDomainId = @numDomainId
		AND General_Journal_Details.numDomainId = @numDomainId
		AND (@numAccountClass=0 OR dbo.General_Journal_Details.numClassID=@numAccountClass)
	GROUP BY 
		dbo.General_Journal_Details.numChartAcntId
		,dbo.General_Journal_Header.datEntry_Date
		,dbo.General_Journal_Details.numClassID

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	-- OLD CODE COMMENTED DUE TO PERFORMANCE ISSUE
	-- INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	INSERT INTO @view_journal
	SELECT 
		dbo.Chart_Of_Accounts.numAccountId
		,dbo.AccountTypeDetail.vcAccountCode
		,dbo.General_Journal_Header.datEntry_Date
		,SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit
		,SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit              
	FROM  
		dbo.General_Journal_Header  
	INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId  
	INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId=dbo.Chart_Of_Accounts.numAccountId
	INNER JOIN dbo.AccountTypeDetail ON dbo.Chart_Of_Accounts.numParntAcntTypeID = dbo.AccountTypeDetail.numAccountTypeID 
	WHERE
		dbo.General_Journal_Header.numDomainID = @numDomainID
		AND dbo.General_Journal_Details.numDomainId = @numDomainID 
		AND (@numAccountClass=0 OR dbo.General_Journal_Details.numClassID=@numAccountClass)
	GROUP BY  
		dbo.Chart_Of_Accounts.numAccountId
		,dbo.AccountTypeDetail.vcAccountCode
		,dbo.General_Journal_Header.datEntry_Date

	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS DECIMAL(20,5)
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate)

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,			
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			--AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			--AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			--AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			--AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)), 
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			--AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
			,0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			--AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId,
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	IF @bitAddTotalRow = 1
	BEGIN
		INSERT INTO #tempDirectReport
		(
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType, 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal
		)
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			CONCAT('Total ',vcAccountType), 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			1
		FROM 
			#tempDirectReport COA
		WHERE 
			ISNULL(COA.numAccountId,0) > 0
			AND ISNULL(COA.bitTotal,0) = 0
			AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

		UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
		DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
	END

	
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				FORMAT(@dtFromDate,'MMM') AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1
		WHERE
			ISNULL(COA.bitTotal,0) = 0
			AND ISNULL(COA.bitProfitLoss,0)=0

		SET @sql = CONCAT('SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,',@columns,'
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ',@pivotcolumns,' )
					) AS P ORDER BY Struc, [Type] desc')


		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
		DROP TABLE #tempViewDataYear
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1
		WHERE
			ISNULL(COA.bitTotal,0) = 0
			AND ISNULL(COA.bitProfitLoss,0)=0

		SET @sql = CONCAT('SELECT
							ParentId,
							vcCompundParentKey,
							numAccountId,
							numAccountTypeID,
							vcAccountType,
							LEVEL,
							vcAccountCode,
							Struc,
							[Type],
							bitTotal,',@columns,'
						FROM
						(
							SELECT 
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''#'' + COA.Struc + ''#'') AS Struc,
								(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
								ISNULL(SUM(Amount),0)  AS Amount,
								V.MonthYear
							FROM 
								#tempDirectReport COA 
							LEFT OUTER JOIN 
								#tempViewDataQuarter V 
							ON  
								V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
						) AS SourceTable
						pivot
						(
							SUM(AMOUNT)
							FOR [MonthYear] IN ( ',@pivotcolumns,' )
						) AS P ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS DECIMAL(20,5) = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			(CASE 
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
			END) AS Amount,
			V.datEntry_Date
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		LEFT JOIN 
			#VIEW_JOURNALBS V 
		ON  
			COA.numAccountId = V.numAccountID 
			AND datEntry_Date <= @dtToDate
		WHERE 
			COA.[numAccountId] IS NOT NULL
			AND ISNULL(COA.bitTotal,0) = 0
			AND ISNULL(COA.bitProfitLoss,0)=0

		SELECT 
			COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
			COA.bitTotal,
			(Case 
					When COA.bitProfitLoss = 1
					THEN @ProifitLossAmount
					ELSE ((CASE WHEN CHARINDEX(('#' + COA.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) + SUM(ISNULL(Amount,0)))
			END) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + (CASE WHEN ISNULL(@bitAddTotalRow,0)=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '' ELSE '%' END)
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitProfitLoss
		ORDER BY 
			Struc, [Type] desc

		DROP TABLE #tempViewData
	END

	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID,
ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
ISNULL(D.bitPosToClose,0) AS bitPosToClose,
ISNULL(D.bitPOToClose,0) AS bitPOToClose,
ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
ISNULL(D.vchPosToClose,'') AS vchPosToClose,
ISNULL(D.vchPOToClose,'') AS vchPOToClose,
ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(tintMailProvider,3) tintMailProvider,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(numARContactPosition,0) numARContactPosition,
ISNULL(bitShowCardConnectLink,0) bitShowCardConnectLink,
ISNULL(vcBluePayFormName,'') vcBluePayFormName,
ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL,
ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL,
ISNULL(bitUseDeluxeCheckStock,0) bitUseDeluxeCheckStock,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys,
ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem,
ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem,
ISNULL(bitUsePredefinedCustomer,0) bitUsePredefinedCustomer,
ISNULL(bitUsePreviousEmailBizDoc,0) bitUsePreviousEmailBizDoc
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitems')
DROP PROCEDURE usp_getmasterlistitems
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItems]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0          
as          
BEGIN          
	DECLARE @bit3PL BIT, @bitEDI BIT
	SELECT @bit3PL=ISNULL(bit3PL,0),@bitEDI=ISNULL(bitEDI,0) FROM Domain WHERE numDomainID=@numDomainID

	SELECT 
		Ld.numListItemID
		,ISNULL(LDN.vcName,ISNULL(vcRenamedListName,vcData)) AS vcData
		,ISNULL(ld.numListType,0) AS numListType
		,ISNULL(LD.constFlag,0) constFlag
	FROM 
		ListDetails Ld        
	LEFT JOIN 
		ListOrder LO 
	ON 
		Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID
	LEFT JOIN
		ListDetailsName LDN
	ON
		LDN.numDomainID = @numDomainID
		AND LDN.numListID = @ListID
		AND LDN.numListItemID = Ld.numListItemID
	WHERE 
		Ld.numListID=@ListID
		AND 1 = (CASE 
					WHEN LD.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END)
					WHEN LD.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
		AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
	ORDER BY 
		ISNULL(intSortOrder,LD.sintOrder)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsForBizDocs]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsforbizdocs')
DROP PROCEDURE usp_getmasterlistitemsforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsForBizDocs]                  
@ListID as numeric(9)=0,                
@numDomainID as numeric(9)=0,            
@numOppId as numeric(9)=0             
as              
Begin               
Declare @numAuthoritativeId as numeric(9)            
Declare @tintOppType as tinyint            
Declare @tintOppStatus as tinyint      
Declare @tintshipped  as tinyint    
set @numAuthoritativeId=0    
 
 Select @tintOppType=tintOppType,@tintOppStatus=tintOppStatus,@tintshipped=tintshipped From OpportunityMaster Where numOppId=@numOppId And  numDomainID=@numDomainID         
             
If @tintOppType=1           
 Select @numAuthoritativeId=numAuthoritativeSales from AuthoritativeBizDocs Where numDomainId=@numDomainID           
Else if @tintOppType=2
 Select @numAuthoritativeId=numAuthoritativePurchase from AuthoritativeBizDocs Where numDomainId=@numDomainID            
 

if @tintOppStatus=1         
Begin        

IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
	 --SELECT numListItemID,case when @numAuthoritativeId=numListItemID then  vcData +' - Authoritative' Else vcData   End as  vcData 
	 SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN 
												CASE WHEN @numAuthoritativeId = LDN.numListItemID 
													THEN  vcName +' - Authoritative' 
												ELSE vcName
												END 
			ELSE CASE WHEN @numAuthoritativeId = Ld.numListItemID 
					THEN vcData +' - Authoritative' 
				ELSE vcData
				END
			END AS vcData 
	 FROM listdetails LD
	 LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID                 
	 WHERE LD.numListID=@ListID 
		and (constFlag=1 or LD.numDomainID=@numDomainID)  
END
ELSE
Begin
  --SELECT Ld.numListItemID,case when @numAuthoritativeId=Ld.numListItemID then  isnull(vcRenamedListName,vcData) +' - Authoritative' Else isnull(vcRenamedListName,vcData)   End as  vcData 
  SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN 
												CASE WHEN @numAuthoritativeId = LDN.numListItemID 
													THEN  ISNULL(vcRenamedListName,vcName ) +' - Authoritative' 
												ELSE ISNULL(vcRenamedListName,vcName)  
												END 
			ELSE CASE WHEN @numAuthoritativeId = Ld.numListItemID 
					THEN ISNULL(vcRenamedListName,vcData) +' - Authoritative' 
				ELSE ISNULL(vcRenamedListName,vcData)  
				END
			END AS vcData ,
						(SELECT COUNT(noOfRecord) FROM(
							SELECT 
(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) As noOfRecord FROM 
OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = BDF.numBizDoc
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
WHERE OM.numOppId=@numOppId AND numBizDocId=BDF.numBizDoc
GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
HAVING (CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
  FROM BizDocFilter BDF 
  INNER JOIN listdetails Ld ON BDF.numBizDoc=Ld.numListItemID
  left join listorder LO on Ld.numListItemID= LO.numListItemID AND Lo.numDomainId = @numDomainID
  LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID   
  WHERE Ld.numListID=@ListID AND BDF.tintBizocType=@tintOppType AND BDF.numDomainID=@numDomainID 
  AND (constFlag=1 or Ld.numDomainID=@numDomainID) ORDER BY totalPendingInvoice desc, LO.intSortOrder        
End        
End               
Else        
Begin        
IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
 --SELECT numListItemID,vcData as  vcData 
 SELECT LD.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN LDN.vcName 
							ELSE LD.vcData 
							END AS vcData
 FROM ListDetails LD       
	LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID  AND LDN.numDomainId = @numDomainID          
 WHERE LD.numListID=@ListID and (constFlag=1 or LD.numDomainID=@numDomainID)         
    And LD.numListItemID <> @numAuthoritativeId          
END
ELSE
Begin
  --SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
  SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN ISNULL(vcRenamedListName,LDN.vcName) 
							ELSE ISNULL(vcRenamedListName,vcData)
							END AS vcData,
							
													(SELECT COUNT(noOfRecord) FROM(
							SELECT 
(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) As noOfRecord FROM 
OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = BDF.numBizDoc
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
WHERE OM.numOppId=@numOppId AND numBizDocId=BDF.numBizDoc
GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
HAVING (CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
  FROM BizDocFilter BDF 
	  INNER JOIN listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
	  LEFT JOIN listorder LO ON Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID
	  LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID   
  WHERE Ld.numListID=@ListID 
	AND BDF.tintBizocType=@tintOppType 
	AND BDF.numDomainID=@numDomainID 
	AND (constFlag=1 OR Ld.numDomainID=@numDomainID) 
	AND Ld.numListItemID <> @numAuthoritativeId 
	AND Ld.numListItemID NOT IN (296)  ORDER BY totalPendingInvoice desc, LO.intSortOrder
  End    
End        
End
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsUsingDomainID]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsusingdomainid')
DROP PROCEDURE usp_getmasterlistitemsusingdomainid
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingDomainID]            
@numDomainID as numeric(9)=0,            
@numListID as numeric(9)=0          
AS
BEGIN            
	DECLARE @bit3PL BIT, @bitEDI BIT
	SELECT @bit3PL=ISNULL(bit3PL,0),@bitEDI=ISNULL(bitEDI,0) FROM Domain WHERE numDomainID=@numDomainID
      
	SELECT 
		DISTINCT Ld.numListItemID
		,ISNULL(LDN.vcName,vcData) AS vcData
		,(SELECT TOP 1 vcData FROM ListDetails WHERE numListItemId=ld.numListItemGroupId) AS vcListItemGroupName
		,ISNULL(CASE WHEN LD.numListID=30 THEN (SELECT TOP 1 vcColorScheme FROM DycFieldColorScheme WHERE numDomainId=@numDomainID AND numFieldID=18 AND numFormID=34 AND vcFieldValue=LD.numListItemID) ELSE '' END,'') AS vcColorScheme
		,Ld.constFlag
		,bitDelete
		,ISNULL(intSortOrder,0) intSortOrder
		,ISNULL(numListType,0) as numListType
		,(SELECT vcData FROM ListDetails WHERE numListId=27 and numListItemId=ld.numListType) AS vcListType
		,(CASE WHEN ld.numListType=1 THEN 'Sales' WHEN ld.numListType=2 THEN 'Purchase' ELSE 'All' END) AS vcOrderType
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END) AS vcOppOrOrder
		,numListType
		,tintOppOrOrder,ISNULL(SOR.bitEnforceMinOrderAmount,'False') AS bitEnforceMinOrderAmount, ISNULL(SOR.fltMinOrderAmount,0) AS fltMinOrderAmount
		,(SELECT ISNULL(LD.vcData,'') WHERE LD.numListID=27 ) AS vcOriginalBizDocName
		,ISNULL(numListItemGroupId,0) AS numListItemGroupId
	FROM 
		ListDetails LD          
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID = LO.numListItemID 
		AND lo.numDomainId = @numDomainID
	LEFT JOIN 
		ListDetailsName LDN 
	ON 
		LDN.numDOmainID=@numDomainID 
		AND LDN.numListID=@numListID 
		AND LDN.numListItemID=LD.numListItemID
	LEFT JOIN
		SalesOrderRule SOR ON LD.numListItemID = SOR.numListItemID
	WHERE 
		(Ld.numDomainID=@numDomainID OR Ld.constFlag=1) 
		AND Ld.numListID=@numListID           
		AND 1 = (CASE 
					WHEN LD.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END)
					WHEN LD.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	ORDER BY 
		intSortOrder
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetMetaTags]    Script Date: 08/08/2009 16:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMetaTags')
DROP PROCEDURE USP_GetMetaTags
GO
CREATE PROCEDURE [dbo].[USP_GetMetaTags]
                @numMetaID      NUMERIC(9)  = 0,
                @tintMetatagFor TINYINT, -- 1 for Page, 2 for Items,
                @numReferenceID NUMERIC(9)
AS
  BEGIN
    IF @numMetaID <> 0
    BEGIN
		SELECT 
			[vcPageTitle],
			[vcMetaKeywords],
			[vcMetaDescription],
			[numReferenceID]
		FROM 
			[MetaTags]
		WHERE 
			[tintMetatagFor] = @tintMetatagFor
    END
    ELSE
    BEGIN
        SELECT 
			[vcPageTitle],
			[vcMetaKeywords],
            [vcMetaDescription],
            [numMetaID]
        FROM 
			[MetaTags]
        WHERE 
			[numReferenceID] = @numReferenceID 
			AND tintMetatagFor =  @tintMetatagFor 
    END
  END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT                                               
AS                                      
BEGIN                                   
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN  
	IF @numFormID=88 AND @pageId=5 AND @numRelCntType=0 AND @tintPageType=2
	BEGIN
		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=212)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,212
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END

		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=216)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,216
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END
	END
	                         
IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
BEGIN
    SELECT 
		numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintcolumn as intcoulmn,'' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
		,numListID,0 numListItemID,PopupFunctionName
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
		,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
		,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicColumns
	WHERE 
		numFormId=@numFormID 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID  
		AND ISNULL(bitCustom,0)=0 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType 
		AND 1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
				WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
				ELSE 0 END) 
	UNION 
	SELECT 
		numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN (SELECT vcData FROM listdetails WHERE numlistitemid = dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' 
			THEN STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE
				dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
		END AS vcValue,                    
		CONVERT(BIT,1) bitCustomField,'' vcPropertyName, CONVERT(BIT,1) AS bitCanBeUpdated,numListID,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
			ELSE 0 
		END numListItemID,'' PopupFunctionName,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID ,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
		,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicCustomColumns_RelationShip
	WHERE 
		grp_id=@PageId 
		AND numDomainID=@numDomainID 
		AND subgrp=0
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType
	ORDER BY 
		tintRow,intcoulmn   
END                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			when vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case WHEN vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
 end  

	IF @PageId = 153
	BEGIN
		SELECT 
			numFieldId
			,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL
			,vcAssociatedControlType as fld_type
			,tintRow
			,tintcolumn as intcoulmn
			,'' as vcValue
			,ISNULL(bitCustom,0) AS bitCustomField
			,vcPropertyName
			,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
			,numListID
			,0 numListItemID
			,PopupFunctionName
			,Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID
			,Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId=@numFormID 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID  
			AND ISNULL(bitCustom,0)=0 
			AND numRelCntType=@numRelCntType 
			AND tintPageType=@tintPageType 
		ORDER BY 
			tintrow,intcoulmn   
	END 
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN

	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))
			WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 153                      
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0
		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0 
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND bitDefault=1 AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)             
			ORDER BY tintrow,intcoulmn                             
		END
	END  

	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END 
END
GO 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			--AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			--AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			--AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			--AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			--AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			--AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)
  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	IF @bitAddTotalRow = 1
	BEGIN
		INSERT INTO #tempDirectReport
		(
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType, 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal
		)
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			CONCAT('Total ',vcAccountType), 
			LEVEL, 
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			1
		FROM 
			#tempDirectReport COA
		WHERE 
			ISNULL(COA.numAccountId,0) > 0
			AND ISNULL(COA.bitTotal,0) = 0
			AND (SELECT COUNT(*) FROM #tempDirectReport COAInner WHERE COAInner.Struc LIKE COA.Struc + '%') > 1

		UPDATE DRMain SET vcAccountType=SUBSTRING(vcAccountType,7,LEN(vcAccountType) - 6) FROM #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=1 AND ISNULL(numAccountId,0) = 0 AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm #tempDirectReport DRMain1 WHERE ISNULL(DRMain1.bitTotal,0)=0 AND ISNULL(DRMain1.numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain1.Struc + '%') = 0)
		DELETE DRMain FROm #tempDirectReport DRMain WHERE ISNULL(DRMain.bitTotal,0)=0 AND ISNULL(numAccountId,0) = 0 AND (SELECT COUNT(*) FROM #tempDirectReport DR1 WHERE ISNULL(DR1.numAccountId,0) > 0 AND DR1.Struc LIKE DRMain.Struc + '%') = 0
	END

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) 
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SET @sql = CONCAT('SELECT
							ParentId,
							vcCompundParentKey,
							numAccountId,
							numAccountTypeID,
							vcAccountType,
							LEVEL,
							vcAccountCode,
							Struc,
							[Type],
							bitTotal,',@columns,'
						FROM
						(
							SELECT 
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''#'' + COA.Struc + ''#'') AS Struc,
								(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
								ISNULL(SUM(Amount),0) AS Amount,
								V.MonthYear
							FROM 
								#tempDirectReport COA 
							LEFT OUTER JOIN 
								#tempViewDataYear V 
							ON  
								V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
							UNION
							SELECT 
								'''', 
								''-1'',
								NULL,
								NULL,
								''Total'',
								-1, 
								NULL, 
								1,
								''-1'',
								2,
								ISNULL((SELECT 
											ISNULL(SUM(Debit) - SUM(Credit),0)
										FROM 
											VIEW_Journal_Master
										WHERE 
											numDomainID = ',@numDomainId,'
											AND (AccountCode LIKE ''0101%'' 
											OR AccountCode LIKE ''0102%''
											OR AccountCode LIKE ''0103%''
											OR AccountCode LIKE ''0104%''
											OR AccountCode LIKE ''0105%''
											OR AccountCode LIKE ''0106%'')
											AND (numClassIDDetail=',ISNULL(@numAccountClass,0) , ' OR ',ISNULL(@numAccountClass,0),' = 0)
											AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),0),
								MonthYear
							FROM
								#TempYearMonth
						) AS SourceTable
						pivot
						(
							SUM(AMOUNT)
							FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
						) AS P 
						ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0
		
		SET @sql = CONCAT('SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + (CASE WHEN ',ISNULL(@bitAddTotalRow,0),'=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '''' ELSE ''%'' END)
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
						UNION
						SELECT 
							'''',
							''-1'', 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL,
							1,
							''-1'',
							2,
							ISNULL((SELECT 
										ISNULL(SUM(Debit) - SUM(Credit),0)
									FROM 
										VIEW_Journal_Master
									WHERE 
										numDomainID = ',@numDomainId,'
										AND (AccountCode LIKE ''0101%'' 
										OR AccountCode LIKE ''0102%''
										OR AccountCode LIKE ''0103%''
										OR AccountCode LIKE ''0104%''
										OR AccountCode LIKE ''0105%''
										OR AccountCode LIKE ''0106%'')
										AND (numClassIDDetail=',ISNULL(@numAccountClass,0),' OR ',ISNULL(@numAccountClass,0),' = 0)
										AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),0),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ',@pivotcolumns,' )
					) AS P 
					ORDER BY Struc, [Type] desc')

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS DECIMAL(20,5) = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -@monProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE 
							ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2
		WHERE
			ISNULL(COA.bitTotal,0) = 0

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.bitTotal,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 AND ISNULL(COA.bitTotal,0) = 0 THEN 1 ELSE 2 END) [Type],
			ISNULL(SUM(Amount),0) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + (CASE WHEN @bitAddTotalRow=1 AND ISNULL(COA.numAccountId,0) > 0 AND COA.bitTotal=0 THEN '' ELSE '%' END)			
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,COA.bitTotal
		UNION
		SELECT 
			'', 
			'-1',
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			1,
			'-1' AS Struc,
			2,
			ISNULL((SELECT 
						SUM(Debit) - SUM(Credit)
					FROM 
						VIEW_Journal_Master
					WHERE 
						numDomainID = @numDomainId
						AND (AccountCode LIKE '0101%' 
						OR AccountCode LIKE '0102%'
						OR AccountCode LIKE '0103%'
						OR AccountCode LIKE '0104%'
						OR AccountCode LIKE '0105%'
						OR AccountCode LIKE '0106%')
						AND (numClassIDDetail=ISNULL(@numAccountClass,0) OR ISNULL(@numAccountClass,0) = 0)
						AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) AS Total
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0,
@numOppBizDocID NUMERIC(18,0)
AS
BEGIN                
	DECLARE @Identity AS NUMERIC(9)                
	DECLARE @FromName AS VARCHAR(1000)                
	DECLARE @FromEmailAdd AS VARCHAR(1000)                
	DECLARE @startPos AS INTEGER                
	DECLARE @EndPos AS INTEGER                
	DECLARE @EndPos1 AS INTEGER                
                
	SET @startPos=CHARINDEX( '<', @vcMessageFrom)                
	SET @EndPos=CHARINDEX( '>', @vcMessageFrom)
	    
	IF @startPos>0                 
	BEGIN                
		set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
		set @startPos=CHARINDEX( '"', @vcMessageFrom)                
		set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
		set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                                           
	END
	ELSE                 
	BEGIN                
		SET @FromEmailAdd=@vcMessageFrom                
		SET @FromName=@vcMessageFromName                
	END                
	
	DECLARE @updateinsert AS BIT
    
	IF @vcItemId = ''    
	BEGIN    
		SET @updateinsert  = 1    
	END    
	ELSE    
	BEGIN    
		IF (SELECT COUNT(*) FROM emailhistory WHERE vcItemId =@vcItemId) > 0    
		BEGIN    
			SET @updateinsert  = 0    
		END    
		ELSE    
		BEGIN    
			SET @updateinsert  = 1    
		END    
	END    
                  
	IF @updateinsert =0    
	BEGIN              
		UPDATE 
			EmailHistory            
		SET            
			vcSubject=@vcSubject,            
			vcBody=@vcBody,      
			vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
			bintCreatedOn=getutcdate(),            
			bitHasAttachments=@bitHasAttachment,            
			vcItemId=@vcItemId,            
			vcChangeKey=@vcChangeKey,            
			bitIsRead=@bitIsRead,            
			vcSize=convert(varchar(200),@vcSize),            
			chrSource=@chrSource,            
			tinttype=@tinttype,            
			vcCategory =@vcCategory ,        
			dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
		WHERE 
			vcItemId = @vcItemId 
			AND numDomainid= @numDomainId
	END
	ELSE             
	BEGIN
		DECLARE @node AS NUMERIC(18,0)
		set @node = case when @vcCategory = 'B' then ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4),0) else 0 end 
		SET @node=(SELECT TOP 1 numNodeID FROM InboxTreeSort WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntId AND vcNodeName='Sent Mail')
		SET @vcMessageTo = @vcMessageTo + '#^#'
		SET @vcMessageFrom = @vcMessageFrom + '#^#'
		SET @vcCC = @vcCC + '#^#'
		SET @vcBCC = @vcBCC + '#^#'

		EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

		DECLARE @numNodeId AS NUMERIC(18,0)=0
		IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')
		END
		
		INSERT INTO EmailHistory
		(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
		VALUES                          
		(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@numNodeId,dbo.fn_GetNewvcEmailString(@vcMessageFrom,@numDomainId),dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId),dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId),dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId),@IsReplied,@bitInvisible)              
		
		SET @Identity=SCOPE_IDENTITY()  
           

		UPDATE EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) WHERE [numEmailHstrID]=@Identity                

		UPDATE EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID      

		IF ISNULL(@numOppBizDocID,0) > 0 AND ISNULL((SELECT bitUsePreviousEmailBizDoc FROM Domain WHERE numDomainID=@numDomainID),0) = 1
		BEGIN
			DECLARE @numDivisionId NUMERIC(18,0)
			DECLARE @numBizDocId NUMERIC(18,0)

			SELECT @numDivisionId=numDivisionId,@numBizDocId=numBizDocId FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE numOppBizDocsId=@numOppBizDocID

			IF ISNULL(@numDivisionId,0) > 0 AND ISNULL(@numBizDocId,0) > 0
			BEGIN
				IF EXISTS (SELECT ID FROm DivisionMasterBizDocEmail WHERE numDomainID=@numDomainId AND numDivisionID=@numDivisionId AND numBizDocID=@numBizDocId)
				BEGIN
					UPDATE
						DivisionMasterBizDocEmail
					SET
						vcTo = dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId)
						,vcCC = dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId)
						,vcBCC = dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId)
					WHERE
						numDomainID=@numDomainId 
						AND numDivisionID=@numDivisionId 
						AND numBizDocID=@numBizDocId
				END
				ELSE
				BEGIN
					INSERT INTO DivisionMasterBizDocEmail
					(
						numDomainID
						,numDivisionID
						,numBizDocID
						,vcTo
						,vcCC
						,vcBCC
					)
					VALUES
					(
						@numDomainId
						,@numDivisionId
						,@numBizDocId
						,dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId)
						,dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId)
						,dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId)
					)
				END
			END
		END
		
		SELECT ISNULL(@Identity,0)
	END
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1')
	BEGIN
		SET @bitItemIsUsedInOrder =1
    END
	                                      
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(I.numBusinessProcessId,0) numBusinessProcessId,
		ISNULL(I.bitTimeContractFromSalesOrder,0) AS bitTimeContractFromSalesOrder
		,ISNULL(I.bitAutomateReorderPoint,0) bitAutomateReorderPoint
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
		,I.numBusinessProcessId,I.bitTimeContractFromSalesOrder,I.bitAutomateReorderPoint
END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	

	DECLARE @strSql1 AS NVARCHAR(MAX)
	DECLARE @strSql2 AS NVARCHAR(MAX)
	DECLARE @strSql3 AS NVARCHAR(MAX)
	DECLARE @strSql4 AS NVARCHAR(MAX)
	
	SET @strSql1 = CONCAT('SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN (',@UOMConversionFactor,' * PricingOption.decDiscount)
										END),ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										MT.vcPageTitle,
										MT.vcMetaKeywords,
										MT.vcMetaDescription,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode')
	SET @strSql2 = CONCAT(' from Item I 
										LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
										LEFT JOIN MetaTags MT ON MT.numReferenceID = I.numItemCode AND MT.tintMetaTagFor =2
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT tintRuleType,tintDiscountType,decDiscount FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit')

	SET @strSql3 = CONCAT(' where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,' GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,MT.vcPageTitle,MT.vcMetaKeywords,MT.vcMetaDescription,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode,Vendor.monCost ',(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.tintRuleType,PricingOption.tintDiscountType,PricingOption.decDiscount' ELSE '' END))

	SET @strSql4 = ' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode from tblItem ORDER BY numOnHand DESC'


	PRINT CAST(CONCAT('WITH tblItem AS (',@strSql1,@strSql2,@strSql3,')',@strSql4) AS NTEXT)

	exec ('WITH tblItem AS (' + @strSql1 + @strSql2 + @strSql3 + ')' + @strSql4)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(18,0) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(18,0),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT,
	@numFilteredWarehouseID AS NUMERIC(18,0)=0,
	@vcAsile AS VARCHAR(500)='',
	@vcRack AS VARCHAR(500)='',
	@vcShelf AS VARCHAR(500)='',
	@vcBin AS VARCHAR(500)='',
	@bitApplyFilter As BIT=0,
	@tintUserRightType TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID

	SET @vcCustomSearchCriteria = REPLACE(@vcCustomSearchCriteria,'ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00_ctl02_ctl00_','')

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,I.vcItemName,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC        
		IF((SELECT COUNt(*) FROM #tempForm WHERE vcDbColumnName='StockQtyCount'  OR  vcDbColumnName='StockQtyAdjust')  >0)
		BEGIN
		IF ISNULL(@numFilteredWarehouseID,0)=0
		BEGIN
			SET @strColumns = @strColumns+ ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId'
		END   
		ELSE IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strColumns = @strColumns+ ',WarehouseItems.numOnHand as numIndividualOnHandQty,(WarehouseItems.numOnHand + WarehouseItems.numAllocation) as numAvailabilityQty,I.monAverageCost,WarehouseItems.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId,
			((isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*isnull(I.monAverageCost ,0)) as StockValue'
		END  
		END
		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyCount'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyAdjust'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'     
					                                             
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)=0
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', WIL.vcLocation) FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)>0
				BEGIN
					SET @strColumns = @strColumns + ', WFL.vcLocation AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  
		
		--DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		--DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		--IF(ISNULL(@numFilteredWarehouseID,0)>0)
		--BEGIN
		--	SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	IF(ISNULL(@vcAsile,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcRack,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcShelf,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcBin,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		--	END
		--END
		
		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = CONCAT(' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						',(CASE 
								WHEN ISNULL(@numFilteredWarehouseID,0) > 0 
								THEN CONCAT(' CROSS APPLY (
											SELECT
												WareHouseItems.numWareHouseItemID,
												numOnHand AS numOnHand,
												ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS numAvailable,
												numBackOrder AS numBackOrder,
												numOnOrder AS numOnOrder,
												numAllocation AS numAllocation,
												numReorder AS numReorder,
												numOnHand * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue,
												WarehouseLocation.vcLocation
											FROM
												WareHouseItems
											LEFT JOIN 
												WarehouseLocation 
											ON 
												WareHouseItems.numWLocationId=WarehouseLocation.numWLocationId
											WHERE
												numItemID = I.numItemCode
												AND (WareHouseItems.numWareHouseID=',ISNULL(@numFilteredWarehouseID,0),' OR ',ISNULL(@numFilteredWarehouseID,0),'=0)'
												,CASE WHEN ISNULL(@vcAsile,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcAisle=''',@vcAsile+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcRack,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcRack=''',@vcRack +'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcShelf,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcShelf=''',@vcShelf+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcBin,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcBin=''',@vcBin+'''') ELSE '' END  
										,') WarehouseItems ')
								ELSE ' OUTER APPLY (
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode) WarehouseItems ' 
							END),'
						  ', @WhereCondition)

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                      
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly	= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN 
		--comment
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria  		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END
		
		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END

		IF @tintUserRightType=1 AND NOT EXISTS (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (SELECT 
																		V.numItemCode
																	FROM 
																		ExtranetAccountsDtl EAD 
																	INNER JOIN 
																		ExtarnetAccounts EA
																	ON 
																		EAD.numExtranetID=EA.numExtranetID 
																	INNER JOIN 
																		Vendor V 
																	ON 
																		EA.numDivisionID=V.numVendorID 
																	WHERE 
																		EAD.numDomainID=',@numDomainID,' 
																		AND V.numDomainID=',@numDomainID,' 
																		AND EAD.numContactID=',@numUserCntID,')')
		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)=''
		IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strFinal = CONCAT(@strFinal,'SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,',WarehouseItems.vcLocation asc) RowID, ',@strColumns,' INTO #temp2',@strSql,@strWhere,' ;')
			SET @strFinal = @strFinal + ' SELECT @TotalRecords = COUNT(*) FROM #temp2; '
			SET @strFinal = @strFinal + ' SELECT * FROM #temp2 WHERE RowID > '+ CAST(@firstRec AS VARCHAR(100)) +' and RowID <'+CAST(@lastRec AS VARCHAR(100))+';DROP TABLE #temp2; '
		END
		ELSE IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql  , @strWhere ,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT  * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFInal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListForBankReconciliation]    Script Date: 05/07/2009 22:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_EntryListForBankReconciliation')
DROP PROCEDURE USP_Journal_EntryListForBankReconciliation
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListForBankReconciliation]
               @numChartAcntId AS NUMERIC(9),
               @tintFlag       TINYINT=0,
               @numDomainId    AS NUMERIC(9),
               @bitDateHide AS BIT=0,
               @numReconcileID AS NUMERIC(9),
               @tintReconcile AS TINYINT=0 --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
AS
  BEGIN
  
  DECLARE @numCreditAmt NUMERIC(9)
  DECLARE @numDebitAmt NUMERIC(9)
  DECLARE @dtStatementDate AS DATETIME
  
  SELECT @dtStatementDate=dtStatementDate FROM BankReconcileMaster WHERE numReconcileID=@numReconcileID

  create table #TempBankRecon
  (numChartAcntId numeric(18),
  numTransactionId numeric(18),
  bitReconcile bit,
  EntryDate date,
  numDivisionID NUMERIC(18,0),
  CompanyName varchar(500),
  Deposit float,
  Payment float,
  Memo  varchar(250),
  TransactionType varchar(100),
  JournalId numeric(18),
  numCheckHeaderID numeric(18),
  vcReference varchar(1000),
  tintOppType INTEGER,bitCleared BIT,numCashCreditCardId NUMERIC(18),numOppId NUMERIC(18),numOppBizDocsId NUMERIC(18),
  numDepositId NUMERIC(18),numCategoryHDRID NUMERIC(9),tintTEType TINYINT,numCategory NUMERIC(18),numUserCntID NUMERIC(18),dtFromDate DATETIME,
  numBillId NUMERIC(18),numBillPaymentID NUMERIC(18),numLandedCostOppId NUMERIC(18),bitMatched BIT,vcCheckNo VARCHAR(20),tintType TINYINT,vcOrder VARCHAR(10)) /* tintType: 0=No Match, 3=Perfect Match, 2=only amount match with single bank statement row, 1 = only amount match with multiple bank statement row  */
  
  IF (@tintFlag = 1)
	SET @numDebitAmt = 0
  ELSE 	IF (@tintFlag = 2)
	SET @numCreditAmt = 0 
  
  insert into #TempBankRecon
  
    SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   ISNULL(GJD.bitReconcile,0) bitReconcile,	
           GJH.datEntry_Date AS EntryDate,
		   ISNULL(DM.numDivisionID,0),
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
			(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           GJD.varDescription as Memo,
            case when isnull(GJH.numCheckHeaderID,0) <> 0 THEN case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
					when 0 then 'Cash' else 'Checks' end 			
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then 'Cash'
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then 'Charge'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then 'BizDocs Invoice'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then 'BizDocs Purchase'
			when isnull(GJH.numDepositId,0) <> 0 then 'Deposit'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then 'Receive Amt'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then 'Vendor Amt'
			when isnull(GJH.numCategoryHDRID,0)<>0 then 'Time And Expenses'
			When ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			When ISNULL(GJH.numBillId,0) <>0 then 'Bill' 
			When ISNULL(GJH.numBillPaymentID,0) <>0 then 'Pay Bill' 
			When GJH.numJournal_Id <>0 then 'Journal' End  as TransactionType,
			GJD.numJournalId AS JournalId,
			isnull(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			ISNULL((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID=GJH.numDomainID AND numBizDocsPaymentDetId = isnull(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) vcReference,
			isnull(Opp.tintOppType,0),ISNULL(GJD.bitCleared,0) AS bitCleared,
			ISNULL(GJH.numCashCreditCardId,0) AS numCashCreditCardId,ISNULL(GJH.numOppId,0) AS numOppId,
			isnull(GJH.numOppBizDocsId,0) AS numOppBizDocsId,isnull(GJH.numDepositId,0) AS numDepositId,
			isnull(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,ISNULL(GJH.numBillId,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
			0 AS bitMatched
			,ISNULL(CAST(CheckHeader.numCheckNo AS VARCHAR),'')
			,0
			,''
    FROM   General_Journal_Header GJH
           INNER JOIN General_Journal_Details GJD
             ON GJH.numJournal_Id = GJD.numJournalId
           LEFT OUTER JOIN DivisionMaster AS DM
             ON GJD.numCustomerId = DM.numDivisionID
           LEFT OUTER JOIN CompanyInfo AS CI
             ON DM.numCompanyID = CI.numCompanyId
            Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
			Left outer join (select * from OpportunityMaster WHERE tintOppType=1 ) Opp on GJH.numOppId=Opp.numOppId    
			LEFT OUTER JOIN TimeAndExpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			LEFT OUTER JOIN CheckHeader ON GJH.numCheckHeaderID = CheckHeader.numCheckHeaderID
    WHERE  
		   ((GJH.[datEntry_Date] <= @dtStatementDate AND @bitDateHide=1) OR @bitDateHide=0)
           AND GJD.numChartAcntId = @numChartAcntId
           AND GJD.numDomainId = @numDomainId
           AND (GJD.numCreditAmt <> @numCreditAmt OR @numCreditAmt IS null)
           AND (GJD.numDebitAmt <> @numDebitAmt OR @numDebitAmt IS null)
		   AND (ISNULL(GJH.numReconcileID,0) = 0 OR (ISNULL(GJH.numReconcileID,0) = @numReconcileID  AND ISNULL(bitReconcileInterest,0) = 1))
           AND 1=(CASE WHEN @tintReconcile=0 THEN CASE WHEN ISNULL(GJD.bitReconcile,0)=0 AND (ISNULL(GJD.numReconcileID,0)=0 OR ISNULL(GJD.numReconcileID,0)=@numReconcileID) THEN 1 ELSE 0 END
					   WHEN @tintReconcile=1 THEN CASE WHEN ISNULL(GJD.numReconcileID,0)=@numReconcileID THEN 1 ELSE 0 END
					   WHEN @tintReconcile=2 THEN CASE WHEN  ((ISNULL(GJD.bitReconcile,0)=1 OR ISNULL(GJD.bitCleared,0)=1) AND ISNULL(GJD.numReconcileID,0)=@numReconcileID) OR (ISNULL(GJD.bitReconcile,0)=0 AND ISNULL(GJD.numReconcileID,0)=0) THEN 1 ELSE 0 END END)
			ORDER BY GJH.datEntry_Date

	SELECT
		B1.ID,
		dbo.FormatedDateFromDate(B1.dtEntryDate,@numDomainID) as dtEntryDate,
		ISNULL(vcReference,'') as [vcReference],
		[fltAmount],
		ISNULL(vcPayee,'') as vcPayee,
		ISNULL(vcDescription,'') as vcDescription,
		ISNULL(B1.bitCleared,0) bitCleared,
		ISNULL(B1.bitReconcile,0) bitReconcile,
		0 AS numBizTransactionId,
		0 AS bitMatched,
		0 AS bitDuplicate,
		0 AS tintType
	INTO 
		#TempStatememnt
	FROM
		BankReconcileFileData B1
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(B1.bitReconcile,0) = 0

	IF EXISTS (SELECT * FROM BankReconcileMatchRule WHERE numDomainID = @numDomainId AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,',')))
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numRuleID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numRuleID
		)
		SELECT
			ID
		FROM
			BankReconcileMatchRule
		WHERE
			numDomainID = @numDomainId
			AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,','))
		ORDER BY
			tintOrder


		DECLARE @i AS INT = 1
		DECLARE @numRuleID AS NUMERIC(18,0)
		DECLARE @bitMatchAllConditions AS BIT
		DECLARE @Count AS INT 
		SELECT @Count = COUNT(*) FROM @TEMP

		WHILE @i <= @Count
		BEGIN
			SELECT @numRuleID=numRuleID,@bitMatchAllConditions=bitMatchAllConditions FROM @TEMP T1 INNER JOIN BankReconcileMatchRule BRMR ON T1.numRuleID=BRMR.ID WHERE T1.ID = @i 

			UPDATE
				TS
			SET 
				numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
				bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
				bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
				tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
			FROM
				#TempStatememnt TS
			INNER JOIN
				BankReconcileFileData B1
			ON
				TS.ID = B1.ID
				AND ISNULL(TS.numBizTransactionId,0) = 0
				AND ISNULL(TS.bitMatched,0) = 0
			OUTER APPLY
			(
				SELECT TOP 1
					t1.numTransactionId
				FROM
					#TempBankRecon t1
				OUTER APPLY
				(
					SELECT 
						(CASE 
							WHEN @bitMatchAllConditions = 1
							THEN
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 0 ELSE 1 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN LOWER((CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END)) LIKE '%' + LOWER(BRMRC.vcTextToMatch) + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T3
								WHERE
									T3.isMatched = 0)
							ELSE
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T4
								WHERE
									T4.isMatched = 1)
						END) AS bitMatched
				) TEMPConditions
				WHERE
					t1.EntryDate = B1.dtEntryDate
					AND TEMPConditions.bitMatched = 1
					AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
					AND ISNULL(t1.bitMatched,0) = 0
			) AS TEMP1
			OUTER APPLY
			(
				SELECT TOP 1
					t1.ID
				FROM
					BankReconcileFileData t1
				WHERE
					t1.numReconcileID <> @numReconcileID
					AND t1.numDomainID=@numDomainId
					AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
					AND t1.dtEntryDate = B1.dtEntryDate
					AND B1.vcPayee = t1.vcPayee
					AND B1.fltAmount = t1.fltAmount
					AND B1.vcReference = t1.vcReference
					AND B1.vcDescription=t1.vcDescription
			) AS TEMP2
			WHERE
				numReconcileID=@numReconcileID
				

			UPDATE 
				t2
			SET
				bitMatched = 1,
				tintType = 3,
				vcOrder=CONCAT('D',t1.ID)
			FROM
				#TempBankRecon t2
			INNER JOIN
				#TempStatememnt t1
			ON
				t2.numTransactionId=ISNULL(t1.numBizTransactionId,0)
			WHERE 
				ISNULL(t2.bitMatched,0)=0 

			SET @i = @i + 1
		END
	END

	UPDATE
		TS
	SET 
		numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
		bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
		bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
		tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
	FROM
		#TempStatememnt TS
	INNER JOIN
		BankReconcileFileData B1
	ON
		TS.ID = B1.ID
		AND ISNULL(TS.numBizTransactionId,0) = 0
		AND ISNULL(TS.bitMatched,0) = 0
	OUTER APPLY
	(
		SELECT TOP 1
			t1.numTransactionId
		FROM
			#TempBankRecon t1
		WHERE
			t1.EntryDate = B1.dtEntryDate
			AND (CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcPayee)) > 0 OR CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcDescription)) > 0 OR (CHARINDEX('CHECK',UPPER(B1.vcDescription)) > 0 AND LEN(t1.vcCheckNo) > 0 AND CHARINDEX(UPPER(t1.vcCheckNo),UPPER(B1.vcDescription)) > 0))
			AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
	) AS TEMP1
	OUTER APPLY
	(
		SELECT TOP 1
			t1.ID
		FROM
			BankReconcileFileData t1
		WHERE
			t1.numReconcileID <> @numReconcileID
			AND t1.numDomainID=@numDomainId
			AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
			AND t1.dtEntryDate = B1.dtEntryDate
			AND B1.vcPayee = t1.vcPayee
			AND B1.fltAmount = t1.fltAmount
			AND B1.vcReference = t1.vcReference
			AND B1.vcDescription=t1.vcDescription
	) AS TEMP2
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(Ts.bitMatched,0) = 0

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('D',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY SINGAL JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		bitMatched = 1
		,tintType = 2
		,numBizTransactionId = (SELECT numTransactionId FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount))
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt .fltAmount OR Payment*-1=#TempStatememnt .fltAmount)) = 1

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('C',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY MULTIPLE JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		tintType = 1
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount)) > 1

	UPDATE 
		t2
	SET 
		tintType=1
		,vcOrder=CONCAT('B',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		ISNULL(t1.tintType,0)=1
		AND (t2.Deposit = t1.fltAmount OR t2.Payment*-1=t1.fltAmount)
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	 
	SELECT * FROM #TempBankRecon ORDER BY tintType DESC, vcOrder ASC,EntryDate ASC
	SELECT * FROM #TempStatememnt WHERE ISNULL(bitDuplicate,0)=0 ORDER BY tintType DESC, ID

	SELECT 
		(SELECT COUNT(*) FROM #TempStatememnt) AS TotalRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitDuplicate=1) AS DuplicateRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitMatched=1) AS MatchedRecords

	drop table #TempBankRecon    
	drop table #TempStatememnt       
  END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
	END

	If (@charItemType = 'P' AND ISNULL(@bitKitParent,0) = 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
			RETURN
		END
	END

	-- This is common check for CharItemType P and Other
	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_COGS_ACCOUNT',16,1)
		RETURN
	END

	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
		RETURN
	END

	IF ISNULL(@bitExpenseItem,0) = 1
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			RETURN
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageMetaTags]    Script Date: 08/08/2009 16:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageMetaTags')
DROP PROCEDURE USP_ManageMetaTags
GO
CREATE PROCEDURE [dbo].[USP_ManageMetaTags]
	@numMetaID NUMERIC(9)=0,
	@tintMetatagFor tinyint, -- 1 for Page, 2 for Items,
	@numReferenceID NUMERIC(9),
	@vcMetaKeywords VARCHAR(4000),
	@vcMetaDescription VARCHAR(4000),
	@vcPageTitle VARCHAR(4000)
AS
BEGIN
	SET NOCOUNT ON
	IF @numMetaID = 0 
	BEGIN
		IF NOT EXISTS (SELECT numMetaID FROM MetaTags WHERE numReferenceID=@numReferenceID AND tintMetatagFor=@tintMetatagFor)
		BEGIN
			INSERT INTO MetaTags 
			(
				[vcPageTitle],
	   			[vcMetaKeywords],
				[vcMetaDescription],
				[tintMetatagFor],
				[numReferenceID]
			)
			VALUES 
			(
				@vcPageTitle,
	   			@vcMetaKeywords,
				@vcMetaDescription,
				@tintMetatagFor,
				@numReferenceID
			)
		END
		ELSE
		BEGIN
			UPDATE 
				MetaTags 
			SET 
				[vcPageTitle] = @vcPageTitle,
	   			[vcMetaKeywords] = @vcMetaKeywords,
				[vcMetaDescription] = @vcMetaDescription,
				[tintMetatagFor] = @tintMetatagFor,
				[numReferenceID] = @numReferenceID
			WHERE 
				numReferenceID=@numReferenceID 
				AND tintMetatagFor=@tintMetatagFor
		END
	END
	ELSE 
	BEGIN
		UPDATE 
			MetaTags 
		SET 
			[vcPageTitle] = @vcPageTitle,
	   		[vcMetaKeywords] = @vcMetaKeywords,
			[vcMetaDescription] = @vcMetaDescription,
			[tintMetatagFor] = @tintMetatagFor,
			[numReferenceID] = @numReferenceID
		WHERE 
			[numMetaID] = @numMetaID
	END
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@tintFlag tinyint -- if 1 then ShowBilled = true, 2 then received but not billed
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitShowOnlyFullyReceived BIT = 0
	DECLARE @tintBillType TINYINT = 1
	
	SELECT @numDivisionID=ISNULL(numDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,0
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(OIInner.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;


	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							0
							,0
							,WorkOrder.numWOID
						FROM
							WorkOrder
						LEFT JOIN
							OpportunityMaster
						ON
							WorkOrder.numOppID = OpportunityMaster.numOppID
						LEFT JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = @numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							Item
						ON
							WorkOrder.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							WorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
						WHERE
							WorkOrder.numDomainId=@numDomainID
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyReceived,0) > 0 AND ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(WorkOrder.numWareHouseItemId,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue);

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType, @numDivisionID;

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,monPrice DECIMAL(20,5)
		,monTotAmount DECIMAL(20,5)
		,vcBilled FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN CONCAT(ISNULL(WorkOrder.vcWorkOrderName,''-''),(CASE WHEN (ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) > 0 THEN CONCAT('' Remaining build qty:'',ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) ELSE '''' END)) ELSE ISNULL(OpportunityMaster.vcPoppName,'''') END)
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate  ELSE OpportunityMaster.bintCreatedDate END)),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo) END)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner) END)
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @numDivisionID NUMERIC(18,0)', @numDomainID, @ClientTimeZoneOffset, @numPageIndex, @numDivisionID;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'''')
										ELSE ISNULL(OpportunityMaster.vcPoppName,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.bintCreatedDate),@numDomainID)
										ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									END)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									END) 
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN 0
										ELSE OpportunityItems.numoppitemtCode
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId
										ELSE ISNULL(OpportunityItems.numWarehouseItmsID,0)
									END)
									,Item.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcInstruction,'''')
										ELSE ISNULL(OpportunityItems.vcItemDesc,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyItemsReq,0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''Units''
										ELSE dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcAttributes,'''')
									END)
									,ISNULL(Item.vcPathForTImage,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numUnitHourReceived,0)
										ELSE ISNULL(OpportunityItems.numUnitHourReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyReceived,0)
										ELSE ISNULL(OpportunityItems.numQtyReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcNotes,'''')
									END)
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN (CASE 
																	WHEN @tintBillType=2 
																	THEN ISNULL(OpportunityItems.numQtyReceived,0) 
																	WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END)  - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,ISNULL(OpportunityItems.monPrice,0)
									,ISNULL(OpportunityItems.monTotAmount,0)
									,ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
												
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
								LEFT JOIN
									WareHouseItems
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId ELSE OpportunityItems.numWarehouseItmsID END) = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.vcWorkOrderName 
																					ELSE OpportunityMaster.vcPoppName
																				END)'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
																						ELSE OpportunityMaster.bintCreatedDate
																					END)'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) 
																						ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
																					END)'
									WHEN 'OpportunityMaster.numRecOwner' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) 
																					ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
																				END)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN '(CASE 
																WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numQtyItemsReq
																ELSE OpportunityItems.numUnitHour
															END)'
									WHEN 'numUnitHourReceived' THEN '(CASE 
																		WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numUnitHourReceived
																		ELSE OpportunityItems.numUnitHourReceived
																	END)'
									ELSE '(CASE 
												WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
												ELSE OpportunityMaster.bintCreatedDate
											END)'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex, @tintBillType, @numDivisionID;
	END
	
	/*Show records which have been billed*/
	IF @numViewID=1 AND  @tintFlag=1 
	BEGIN
	
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where vcBilled>0',@vcCustomWhere)
		SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where vcBilled>0 )
	END
	ELSE IF @numViewID= 3 AND  @tintFlag=2 /* Received but not billed*/
	BEGIN
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where numQtyReceived>0',@vcCustomWhere)
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where numQtyReceived>0 )
	END
	else
	BEGIN 
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 ',@vcCustomWhere)
	END
	


	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID = 2 AND @tintPackingViewMode=1) 
	BEGIN
		SET @bitGroupByOrder = 1
	END
	
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'Item.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID'
								,'Warehouses.vcWareHouse')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @bitEnablePickListMapping BIT
	DECLARE @bitEnableFulfillmentBizDocMapping BIT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
		,@bitEnablePickListMapping=bitEnablePickListMapping
		,@bitEnableFulfillmentBizDocMapping=bitEnableFulfillmentBizDocMapping
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,(CASE WHEN vcFieldDataType IN ('N','M') THEN 1 ELSE 0 END)
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @vcSelect NVARCHAR(MAX) = CONCAT('SELECT 
												OpportunityMaster.numOppID
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,ISNULL(DivisionMaster.numTerID,0) numTerID
												,ISNULL(OpportunityMaster.intUsedShippingCompany,0) numOppShipVia
												,ISNULL(OpportunityMaster.numShippingService,0) numOppShipService
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocID
												,ISNULL(OpportunityBizDocs.vcBizDocID,'''') vcBizDocID
												,ISNULL(OpportunityBizDocs.monAmountPaid,0) monAmountPaid
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1 THEN ',0 AS numOppItemID' ELSE ',ISNULL(OpportunityItems.numoppitemtCode,0) AS numOppItemID' END),'
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1
														THEN ',0 AS numRemainingQty'
														ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OBIInner.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs OBInner
																																																					INNER JOIN
																																																						OpportunityBizDocItems OBIInner
																																																					ON
																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																					WHERE
																																																						OBInner.numOppId = OpportunityMaster.numOppID
																																																						AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
					 																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
													END)
													,','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs,
													(CASE WHEN EXISTS (SELECT OIInner.numOppItemtcode FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND ISNULL(OIInner.bitRequiredWarehouseCorrection,0)=1) THEN 1 ELSE 0 END) bitRequiredWarehouseCorrection',
													(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN ',ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)) dtShipByDate' ELSE ',CAST(OpportunityMaster.dtReleaseDate AS DATE) AS dtShipByDate' END))

	DECLARE @vcFrom NVARCHAR(MAX) = CONCAT(' FROM
												OpportunityMaster
											INNER JOIN
												DivisionMaster
											ON
												OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
											INNER JOIN
												CompanyInfo
											ON
												DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
											INNER JOIN
												OpportunityItems
											ON
												OpportunityMaster.numOppId = OpportunityItems.numOppId
											LEFT JOIN 
												MassSalesFulfillmentBatchOrders
											ON
												MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
												AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
												AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
											LEFT JOIN
												WareHouseItems
											ON
												OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
											LEFT JOIN
												WareHouses
											ON
												WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
											LEFT JOIN
												WarehouseLocation
											ON
												WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
											INNER JOIN
												Item
											ON
												OpportunityItems.numItemCode = Item.numItemCode
											',(CASE WHEN @numViewID=6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
												OpportunityBizDocs
											ON
												OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
												AND OpportunityBizDocs.numBizDocID= ',(CASE 
																						WHEN @numViewID=4 THEN 287 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																						ELSE 0
																					END),'
												AND @numViewID IN (4,6) 
											LEFT JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
												AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID')

	DECLARE @vcFrom1 NVARCHAR(MAX) = ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= @numDefaultSalesShippingDoc
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WareHouses
									ON
										WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
										AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)'

	DECLARE @vcWhere VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityItems.bitMappingRequired,0) = 0 '
												,(CASE 
													WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
													ELSE ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
												END)
												,(CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END)
												,(CASE 
													WHEN (@numViewID = 1 OR (@numViewID = 2 AND @tintPackingViewMode <> 1)) AND ISNULL(@bitRemoveBO,0) = 1
													THEN ' AND 1 = (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																				END)'
														ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcOrderStauts,'')) > 0 
													THEN (CASE 
															WHEN ISNULL(@bitIncludeSearch,0) = 0 
															THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
															ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
														END)
													ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcFilterValue,'')) > 0
													THEN
														CASE 
															WHEN @tintFilterBy=1 --Item Classification
															THEN ' AND 1 = (CASE 
																				WHEN ISNULL(@bitIncludeSearch,0) = 0 
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			END)'
															ELSE ''
														END
													ELSE ''
												END)
												,(CASE
													WHEN @numViewID = 1 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
													WHEN @numViewID = 2 THEN ' AND 1 = (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0) AND OIInner.numShipFromWarehouse=@numWarehouseID AND OIInner.ItemReleaseDate=ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OBIInner.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs OBInner
																																																															INNER JOIN
																																																																OpportunityBizDocItems OBIInner
																																																															ON
																																																																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																															WHERE
																																																																OBInner.numOppId = OpportunityMaster.numOppID
																																																																AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)'
													WHEN @numViewID = 3
													THEN ' AND 1 = (CASE 
																		WHEN (CASE 
																				WHEN ISNULL(@tintInvoicingType,0)=1 
																				THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																				ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																			END) - ISNULL((SELECT 
																								SUM(OpportunityBizDocItems.numUnitHour)
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityBizDocItems
																							ON
																								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																							WHERE
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																								AND OpportunityBizDocs.numBizDocId=287
																								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																		THEN 1
																		ELSE 0 
																	END)'
												END)
												,(CASE
															WHEN @numViewID = 1 THEN 'AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN ' AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN ' AND 1=1'
															WHEN @numViewID = 3 THEN ' AND 1=1'
															WHEN @numViewID = 4 THEN ' AND 1=1'
															WHEN @numViewID = 5 THEN ' AND 1=1'
															WHEN @numViewID = 6 THEN ' AND 1=1'
															ELSE ' AND 1=0'
													END)
												,(CASE WHEN ISNULL(@numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 OR @numViewID=6 THEN ' AND 1= (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
													THEN 
														' AND 1 = (CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END)'
															ELSE ''
														END)
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	DECLARE @vcWhere1 VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityMaster.tintshipped,0) = 0
												AND ISNULL(OpportunityItems.bitMappingRequired,0) = 0
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)
												AND 1 = (CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
															THEN 1 
															ELSE 0 
														END)
												AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
												AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)'
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	SET @vcWhere = CONCAT(@vcWhere,' ',@vcCustomSearchValue)
	SET @vcWhere1 = CONCAT(@vcWhere1,' ',@vcCustomSearchValue)
	
	DECLARE @vcGroupBy NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(' GROUP BY OpportunityMaster.numDomainID,OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,OpportunityMaster.numShippingService
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtReleaseDate
																	,OpportunityMaster.tintSource
																	,OpportunityMaster.tintSourceType
																	,OpportunityMaster.dtExpectedDate',(CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 1 THEN ',OpportunityItems.ItemReleaseDate' ELSE '' END))
											ELSE '' 
										END)

	DECLARE @vcOrderBy NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
						WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
						WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
						WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
						ELSE 'OpportunityMaster.bintCreatedDate'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcOrderBy1 NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP.vcCompanyNameOrig' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP.charItemTypeOrig=''P''  THEN ''Inventory Item''
															WHEN TEMP.charItemTypeOrig=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP.charItemTypeOrig=''S'' THEN ''Service''
															WHEN TEMP.charItemTypeOrig=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'TEMP.numBarCodeIdOrig'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(TEMP.numItemClassificationOrig)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP.numItemGroupOrig),'''')'
						WHEN 'Item.vcSKU' THEN 'TEMP.vcSKUOrig'
						WHEN 'OpportunityItems.vcItemName' THEN 'TEMP.vcItemNameOrig'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(TEMP.numStatusOrig)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP.vcPoppNameOrig'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(TEMP.numAssignedToOrig)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(TEMP.numRecOwnerOrig)'
						WHEN 'WareHouseItems.vcLocation' THEN 'TEMP.vcLocationOrig'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP.monAmountPaidOrig'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(TEMP.vcBizDocID) DESC, TEMP.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP.numQtyPickedOrig'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP.numQtyShippedOrig'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL(OpportunityItems.numQtyPickedOrig,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP.numUnitHourOrig'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP.dtExpectedDateOrig'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP.ItemReleaseDateOrig'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.numAge' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TEMP.monAmountPaidOrig,0) >= ISNULL(TEMP.monDealAmountOrig,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'TEMP.dtItemReceivedDate'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						ELSE 'TEMP.bintCreatedDateOrig'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcText NVARCHAR(MAX)

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='bitPaidInFull') OR @vcSortColumn='OpportunityMaster.bitPaidInFull'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='numQtyPacked') OR @vcSortColumn='OpportunityItems.numQtyPacked'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
							FROM 
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE 
								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
								AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
						) TempPacked'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcListItemType VARCHAR(10)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                               
	DECLARE @bitCustomField BIT      
	DECLARE @bitIsNumeric BIT
	DECLARE @vcGroupByInclude NVARCHAR(MAX) = ''
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)


	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = (CASE WHEN ISNULL(bitCustomField,0) = 1 THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END),
			@vcLookBackTableName = vcLookBackTableName,
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0),
			@bitIsNumeric=ISNULL(bitIsNumeric,0),
			@vcListItemType=ISNULL(vcListItemType,'')
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j

		IF @vcDbColumnName NOT IN ('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus','vcPaymentStatus','numShipRate')
		BEGIN
			IF ISNULL(@bitCustomField,0) = 0
			BEGIN
				IF (@vcLookBackTableName = 'Item' OR @vcLookBackTableName = 'WareHouseItems' OR @vcLookBackTableName = 'OpportunityItems' OR @vcLookBackTableName='Warehouses') AND ISNULL(@bitGroupByOrder,0)=1
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
					SET @vcGroupByInclude = CONCAT(@vcGroupByInclude,',','TEMP.',@vcDbColumnName)
				END
				ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN 'ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))' ELSE 'CAST(OpportunityMaster.dtReleaseDate AS DATE)' END),' [',@vcDbColumnName,']')
				END
				ELSE
				BEGIN
					IF @vcDbColumnName = 'numAge'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtItemReceivedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(Item.dtItemReceivedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bitPaidInFull'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtExpectedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bintCreatedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numQtyPacked'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcInvoiced'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'charItemType'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',@vcDbColumnName,']') 
					END
					ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numStatus'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.GetListIemName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'tintSource'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcWareHouse'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE ISNULL(Warehouses.vcWarehouse,''-'') END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDOpp.vcData,SSOpp.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
					END
					ELSE IF @vcDbColumnName = 'intShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDDiv.vcData,SSDiv.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')

						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')
					END
					ELSE IF @vcAssociatedControlType = 'SelectBox'
					BEGIN
						IF @vcListItemType = 'LI'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',L',CONVERT(VARCHAR(3),@j),'.vcData',' [',@vcDbColumnName,']')

							SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
							SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
						END
						ELSE IF @vcListItemType = 'IG'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',@vcDbColumnName,']') 
						END
						ELSE IF @vcListItemType = 'U'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetContactName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
						END
						ELSE
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
						END
					END
					ELSE IF @vcAssociatedControlType = 'DateField'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-@ClientTimeZoneOffset,',',@vcLookBackTableName,'.',@vcDbColumnName,'),',@numDomainId,') [', @vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextBox'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextArea'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'Label'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
				END
			END
			ELSE IF @bitCustomField = 1
			BEGIN
				IF @Grp_id = 5
				BEGIN
					IF ISNULL(@bitGroupByOrder,0) = 1
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',''''',' [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',@vcDbColumnName,']')
					END
				END
				ELSE
				BEGIN
					IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW',@numFieldId
										,' ON CFW' , @numFieldId , '.Fld_Id='
										,@numFieldId
										, ' and CFW' , @numFieldId
										, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
					END   
					ELSE IF @vcAssociatedControlType = 'CheckBox' 
					BEGIN
						SET @vcSelect =CONCAT( @vcSelect
							, ',case when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=0 then 0 when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=1 then 1 end   ['
							,  @vcDbColumnName
							, ']')
							
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' ON CFW',@numFieldId,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'DateField' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect
							, ',dbo.FormatedDateFromDate(CFW'
							, @numFieldId
							, '.Fld_Value,'
							, @numDomainId
							, ')  [',@vcDbColumnName ,']' )  
					                  
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW', @numFieldId, '.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
										WHEN @bitGroupByOrder = 1 
										THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
										ELSE '' 
									END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'SelectBox' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					        
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW',@numFieldId ,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',L',@numFieldId,'.vcData')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)

						SET @vcText = CONCAT(' left Join ListDetails L'
							, @numFieldId
							, ' on L'
							, @numFieldId
							, '.numListItemID=CFW'
							, @numFieldId
							, '.Fld_Value')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)            
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',OpportunityMaster.numOppID)'),' [',@vcDbColumnName,']')
					END 
				END
			END
		END

		SET @j = @j + 1
	END

	PRINT CAST(@vcSelect AS NTEXT)
	PRINT CAST(@vcFrom AS NTEXT)
	PRINT CAST(@vcWhere AS NTEXT)
	PRINT CAST(@vcGroupBy AS NTEXT)
	PRINT CAST(@vcOrderBy AS NTEXT)

	DECLARE @vcFinal NVARCHAR(MAX) = ''

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSelect = CONCAT(@vcSelect,',',(CASE @vcSortColumn
													WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS vcCompanyNameOrig' 
													WHEN 'Item.charItemType' THEN 'Item.charItemType AS charItemTypeOrig'
													WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS numBarCodeIdOrig'
													WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS numItemClassificationOrig'
													WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS numItemGroupOrig'
													WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS vcSKUOrig'
													WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS vcItemNameOrig'
													WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS numStatusOrig '
													WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS vcPoppNameOrig'
													WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS numAssignedToOrig'
													WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS numRecOwnerOrig'
													WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS vcLocationOrig'
													WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig'
													WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped AS numQtyShippedOrig'
													WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig,OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig'
													WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig'
													WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS ItemReleaseDateOrig'
													WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig, OpportunityMaster.monDealAmount AS monDealAmountOrig'
													WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS dtItemReceivedDateOrig'
													WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
													ELSE 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
												END))

		DECLARE @vcGroupBy1 NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(' GROUP BY TEMP.numOppId
																	,TEMP.numOppItemID
																	,TEMP.numRemainingQty
																	,TEMP.dtAnticipatedDelivery
																	,TEMP.dtShipByDate
																	,TEMP.vcShipStatus
																	,TEMP.vcPaymentStatus
																	,TEMP.numShipRate
																	,TEMP.vcLinkingBizDocs
																	,TEMP.bitRequiredWarehouseCorrection
																	,TEMP.numDivisionID
																	,TEMP.numTerID
																	,TEMP.numOppShipVia
																	,TEMP.numOppShipService
																	,TEMP.numPreferredShipVia
																	,TEMP.numPreferredShipService
																	,TEMP.numOppBizDocID
																	,TEMP.monAmountToPay
																	,TEMP.monAmountPaid
																	,TEMP.dtExpectedDateOrig
																	,TEMP.vcBizDocID
																	,TEMP.numAge
																	,TEMP.bintCreatedDateOrig
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='CompanyInfo' AND vcOrigDbColumnName='vcCompanyName') THEN ',TEMP.vcCompanyName' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='vcPoppName') THEN ',TEMP.vcPoppName' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numAssignedTo') THEN ',TEMP.numAssignedTo' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numRecOwner') THEN ',TEMP.numRecOwner' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numStatus') THEN ',TEMP.numStatus' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bintCreatedDate') THEN ',TEMP.bintCreatedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='txtComments') THEN ',TEMP.txtComments' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='vcBizDocID') THEN ',TEMP.vcBizDocID' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany') THEN ',TEMP.intUsedShippingCompany' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany') THEN ',TEMP.intShippingCompany' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='dtCreatedDate') THEN ',TEMP.dtCreatedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='dtExpectedDate') THEN ',TEMP.dtExpectedDate' ELSE '' END),'
																	',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='Item' AND vcOrigDbColumnName='dtItemReceivedDate') THEN ',TEMP.dtItemReceivedDateOrig' ELSE '' END),@vcGroupByInclude)
											ELSE '' 
										END)
		SET @vcFinal= CONCAT('SELECT COUNT(*) OVER() AS TotalRecords,* FROM (',@vcSelect,@vcFrom,@vcWhere,' UNION ',@vcSelect,@vcFrom1,@vcWhere1,') TEMP',@vcGroupBy1,@vcOrderBy1,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END
	ELSE
	BEGIN
		SET @vcFinal = CONCAT(@vcSelect,',COUNT(*) OVER() AS TotalRecords',@vcFrom,@vcWhere,@vcGroupBy,@vcOrderBy,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END

	EXEC sp_executesql @vcFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT,@numBatchID NUMERIC(18,0),@tintPrintBizDocViewMode TINYINT,@tintPendingCloseFilter TINYINT, @bitIncludeSearch BIT, @bitEnablePickListMapping BIT, @bitEnableFulfillmentBizDocMapping BIT,@numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset,@numBatchID,@tintPrintBizDocViewMode,@tintPendingCloseFilter,@bitIncludeSearch,@bitEnablePickListMapping, @bitEnableFulfillmentBizDocMapping,@numPageIndex;

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWareHouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,bitSerial BIT
		,bitLot BIT
		,bitKit BIT
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
		,numQtyRequiredKit FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,''),'') AS vcKitChildItems
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,''),'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitItems.numOppChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitItems.numOppItemID = T1.numOppItemID OR OpportunityKitItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		Item 
	ON
		OpportunityKitItems.numChildItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitChildItems.numOppChildItemID
		,OpportunityKitChildItems.numOppKitChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitChildItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitChildItems.numOppItemID = T1.numOppItemID OR OpportunityKitChildItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
	INNER JOIN
		Item 
	ON
		OpportunityKitChildItems.numItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitChildItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF ISNULL(@bitGroupByOrder,0) = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numOppId = TEMP.numOppId
						AND T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPoppName
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.bitKit
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	ELSE
	BEGIN
		SELECT
			0 AS numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,'' AS vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.vcSKU
			,TEMP.bitKit
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
	END

	DECLARE @TempOrders TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numQtyPicked FLOAT
	)

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcWarehouseItemIDs XML
		,vcOppItems VARCHAR(MAX)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		WarehouseItemIDs xml,
		vcOppItems VARCHAR(MAX),
		numOppChildItemID NUMERIC(18,0),
		numOppKitChildItemID NUMERIC(18,0)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,bitSerial BIT
		,bitLot BIT
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT		
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,numPickedQty FLOAT
		,vcSerialLot# VARCHAR(MAX)
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseDTL TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @x INT = 1
	DECLARE @xCount INT = 0
	DECLARE @y INT = 1
	DECLARE @yCount INT = 0
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numOppChildItemID NUMERIC(18,0)
	DECLARE @numOppKitChildItemID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT
	DECLARE @vcWarehouseItemIDs VARCHAR(4000)
	DECLARE @vcOppItems VARCHAR(MAX)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numWarehousePickedQty FLOAT
	DECLARE @numFromOnHand FLOAT
	DECLARE @numFromOnOrder FLOAT
	DECLARE @numFromAllocation FLOAT
	DECLARE @numFromBackOrder FLOAT
	DECLARE @numToOnHand FLOAT
	DECLARE @numToOnOrder FLOAT
	DECLARE @numToAllocation FLOAT
	DECLARE @numToBackOrder FLOAT
	DECLARE @vcDescription VARCHAR(300)
	DECLARE @numSerialWarehoueItemID NUMERIC(18,0)
	DECLARE @numMaxID NUMERIC(18,0)
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @vcSerialLotNo VARCHAR(200)	
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numSerialLotQtyToPick FLOAT
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcWarehouseItemIDs = CAST(vcWarehouseItemIDs.query('//WarehouseItemIDs/child::*') AS VARCHAR(MAX))
				,@vcOppItems=ISNULL(vcOppItems,'')
				,@numOppChildItemID=numOppChildItemID
				,@numOppKitChildItemID=numOppKitChildItemID
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems
			DELETE FROM @TEMPWarehouseLocations
			DELETE FROM @TEMPSerialLot

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,@numOppChildItemID
				,@numOppKitChildItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityItems.numWarehouseItmsID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
				AND @numOppChildItemID = 0
				AND @numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityItems.numItemCode = Item.numItemCode

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY OpportunityKitItems.numOppItemID ASC)
				,OpportunityKitItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitItems.numOppChildItemID
				,0
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityKitItems.numWareHouseItemId
				,@numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitItems
			ON
				Temp.numOppItemID = OpportunityKitItems.numOppItemID
				AND OpportunityKitItems.numOppChildItemID = @numOppChildItemID
				AND @numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityKitItems.numChildItemID = Item.numItemCode

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY OpportunityKitChildItems.numOppItemID ASC)
				,OpportunityKitChildItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitChildItems.numOppChildItemID
				,OpportunityKitChildItems.numOppKitChildItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityKitChildItems.numWareHouseItemId
				,@numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitChildItems
			ON
				Temp.numOppItemID = OpportunityKitChildItems.numOppItemID
				AND OpportunityKitChildItems.numOppChildItemID = @numOppChildItemID
				AND OpportunityKitChildItems.numOppKitChildItemID = @numOppKitChildItemID
			INNER JOIN
				OpportunityKitItems
			ON
				OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
			INNER JOIN
				Item
			ON
				OpportunityKitChildItems.numItemID = Item.numItemCode

			IF LEN(ISNULL(@vcWarehouseItemIDs,'')) > 0
			BEGIN
				DECLARE @hDocSerialLotItem int
				EXEC sp_xml_preparedocument @hDocSerialLotItem OUTPUT, @vcWarehouseItemIDs

				INSERT INTO @TEMPWarehouseLocations
				(
					ID
					,numWarehouseItemID
					,numPickedQty
					,vcSerialLot#
				)
				SELECT 
					ROW_NUMBER() OVER(ORDER BY ID DESC)
					,ID
					,ISNULL(Qty,0)
					,ISNULL(SerialLotNo,'') 
				FROM 
					OPENXML (@hDocSerialLotItem, '/NewDataSet/Table1',2)
				WITH 
				(
					ID NUMERIC(18,0),
					Qty FLOAT,
					SerialLotNo VARCHAR(MAX)
				)

				EXEC sp_xml_removedocument @hDocSerialLotItem
			END

			IF ISNULL((SELECT TOP 1 bitSerial FROM @TEMPItems),0) = 1 AND (SELECT COUNT(*) FROM @TEMPWarehouseLocations) > 0
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END
			ELSE IF ISNULL((SELECT TOP 1 bitLot FROM @TEMPItems),0) = 1
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END

			IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
			BEGIN
				RAISERROR('INVALID_SERIALLOT_FORMAT',16,1)
				RETURN
			END
		
			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppID=numOppID
					,@numOppItemID=numOppItemID
					,@numOppChildItemID=numOppChildItemID
					,@numOppKitChildItemID=numOppKitChildItemID
					,@bitSerial=bitSerial
					,@bitLot=bitLot
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID
					AND ISNULL(@numOppChildItemID,0) = 0
					AND ISNULL(@numOppKitChildItemID,0) = 0

				IF NOT EXISTS (SELECT ID FROM @TempOrders WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID)
				BEGIN
					INSERT INTO @TempOrders
					(
						numOppID
						,numOppItemID
						,numOppChildItemID
						,numOppKitChildItemID
						,numQtyPicked
					)
					VALUES
					(
						@numOppID
						,@numOppItemID
						,@numOppChildItemID
						,@numOppKitChildItemID
						,(CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
					)
				END
				ELSE
				BEGIN
					UPDATE
						@TempOrders
					SET
						numQtyPicked=ISNULL(numQtyPicked,0) + ISNULL((CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END),0)
					WHERE
						numOppID=@numOppID
						AND numOppItemID=@numOppItemID
						AND numOppChildItemID=@numOppChildItemID 
						AND numOppKitChildItemID=@numOppKitChildItemID
				END

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)

				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND @numOppChildItemID=0 AND @numOppKitChildItemID=0 AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF EXISTS (SELECT ID FROM @TEMPWarehouseLocations WHERE numWarehouseItemID=@numWarehouseItemID)
				BEGIN
					SELECT 
						@numTempWarehouseItemID = numWarehouseItemID
						,@numWarehousePickedQty = ISNULL(numPickedQty,0)
					FROM 
						@TEMPWarehouseLocations
					WHERE 
						numWarehouseItemID=@numWarehouseItemID

					UPDATE 
						@TEMPWarehouseLocations
					SET
						numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
					WHERE
						numWarehouseItemID=@numTempWarehouseItemID

					IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
					BEGIN
						IF  ISNULL((SELECT 
										SUM(ISNULL(TSL.numQty,0))
									FROM 
										WareHouseItmsDTL WID
									INNER JOIN
										@TEMPSerialLot TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE 
										WID.numWareHouseItemID=@numWarehouseItemID
										AND TSL.numWarehouseItemID = @numWarehouseItemID
										AND ISNULL(TSL.numQty,0) > 0
										AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
										

						BEGIN
							SET @x = 1
							SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
							SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

							WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
							BEGIN
								SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

								IF ISNULL(@numSerialLotQty,0) > 0
								BEGIN
									IF @bitSerial = 1
									BEGIN
										IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
										BEGIN
											SELECT TOP 1
												@numWareHouseItmsDTLID =numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo 
												AND numQty>=@numSerialLotQty

											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,numWareHouseItemID
												,@numOppID
												,@numOppItemID
												,@numSerialLotQty
											FROM 
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItmsDTLID=@numWareHouseItmsDTLID

											SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
											UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
											UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
										END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
									ELSE IF @bitLot = 1
									BEGIN
										IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
										BEGIN
											DELETE FROM @TEMPWarehouseDTL

											INSERT INTO @TEMPWarehouseDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo

											SET @y = 1
											SELECT COUNT(*) FROM @TEMPWarehouseDTL

											WHILE @y <= @yCount AND @numSerialLotQty > 0
											BEGIN
												SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

												IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,@numSerialLotQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
													SET @numSerialLotQty = 0
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END
												ELSE
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,numQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = 0  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END

												SET @y = @y + 1
											END

											IF ISNULL(@numSerialLotQty,0) > 0
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
								END
								END

								SET @x = @x + 1
							END

							IF @numSerialLotQtyToPick > 0
							BEGIN
								RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
								RETURN
							END
						END
						ELSE
						BEGIN
							RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
							RETURN
						END

					END

					SET @numQtyLeftToPick = @numQtyLeftToPick - (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
				END

				IF @numQtyLeftToPick > 0
				BEGIN
					DECLARE @k INT = 1
					DECLARE @kCount INT 

					SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLocations

					WHILE @k <= @kCount
					BEGIN
						SELECT 
							@numTempWarehouseItemID = numWarehouseItemID
							,@numWarehousePickedQty = ISNULL(numPickedQty,0)
						FROM 
							@TEMPWarehouseLocations 
						WHERE 
							ID = @k

						UPDATE 
							@TEMPWarehouseLocations
						SET
							numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
						WHERE
							numWarehouseItemID=@numTempWarehouseItemID

						IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
						BEGIN
							IF  ISNULL((SELECT 
											SUM(ISNULL(TSL.numQty,0))
										FROM 
											WareHouseItmsDTL WID
										INNER JOIN
											@TEMPSerialLot TSL
										ON
											WID.vcSerialNo = WID.vcSerialNo
										WHERE 
											WID.numWareHouseItemID=@numTempWarehouseItemID
											AND TSL.numWarehouseItemID = @numTempWarehouseItemID
											AND ISNULL(TSL.numQty,0) > 0
											AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)										
							BEGIN
								SET @x = 1
								SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
								SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

								WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
								BEGIN
									SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

									IF ISNULL(@numSerialLotQty,0) > 0
									BEGIN
										IF @bitSerial = 1
										BEGIN
											IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
											BEGIN
												SELECT TOP 1
													@numWareHouseItmsDTLID =numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo 
													AND numQty>=@numSerialLotQty

												INSERT INTO OppWarehouseSerializedItem
												(
													numWarehouseItmsDTLID
													,numWarehouseItmsID
													,numOppID
													,numOppItemID
													,numQty
												)
												SELECT
													numWareHouseItmsDTLID
													,@numWarehouseItemID
													,@numOppID
													,@numOppItemID
													,@numSerialLotQty
												FROM 
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItmsDTLID=@numWareHouseItmsDTLID

												SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
												UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
												UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty,numWareHouseItemID=@numWarehouseItemID  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
										ELSE IF @bitLot = 1
										BEGIN
											IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
											BEGIN
												DELETE FROM @TEMPWarehouseDTL

												INSERT INTO @TEMPWarehouseDTL
												(
													ID
													,numWareHouseItmsDTLID
												)
												SELECT
													ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
													,numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo

												SET @y = 1
												SELECT COUNT(*) FROM @TEMPWarehouseDTL

												WHILE @y <= @yCount AND @numSerialLotQty > 0
												BEGIN
													SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

													IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,@numSerialLotQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														INSERT INTO WareHouseItmsDTL
														(
															numWareHouseItemID
															,vcSerialNo
															,numQty
														)
														VALUES
														(
															@numWarehouseItemID
															,@vcSerialLotNo
															,@numSerialLotQty
														)

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
														SET @numSerialLotQty = 0
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END
													ELSE
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,numQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numWareHouseItemID=@numWarehouseItemID WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END

													SET @y = @y + 1
												END

												IF ISNULL(@numSerialLotQty,0) > 0
												BEGIN
													RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
													RETURN
												END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
									END
									END

									SET @x = @x + 1
								END

								IF @numSerialLotQtyToPick > 0
								BEGIN
									RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
									RETURN
								END
							END
							ELSE
							BEGIN
								RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
								RETURN
							END
						END

						IF @numWarehousePickedQty > 0 AND (@numWarehouseItemID <> @numTempWarehouseItemID)
						BEGIN
							SELECT
								@numFromOnHand=ISNULL(numOnHand,0)
								,@numFromOnOrder=ISNULL(numOnOrder,0)
								,@numFromAllocation=ISNULL(numAllocation,0)
								,@numFromBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID

							SELECT
								@numToOnHand=ISNULL(numOnHand,0)
								,@numToOnOrder=ISNULL(numOnOrder,0)
								,@numToAllocation=ISNULL(numAllocation,0)
								,@numToBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numWarehouseItemID

							IF @numWarehousePickedQty >= @numQtyLeftToPick
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numQtyLeftToPick,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numQtyLeftToPick
								BEGIN
									IF @numFromOnHand >= @numQtyLeftToPick
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numQtyLeftToPick
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numQtyLeftToPick - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numQtyLeftToPick - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numQtyLeftToPick
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numQtyLeftToPick
									SET @numToAllocation = @numToAllocation + @numQtyLeftToPick
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numQtyLeftToPick - @numToBackOrder)
									SET @numToBackOrder = 0
								END

								SET @numQtyLeftToPick = 0
							END
							ELSE
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numWarehousePickedQty,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numWarehousePickedQty
								BEGIN
									IF @numFromOnHand >= @numWarehousePickedQty
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numWarehousePickedQty
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numWarehousePickedQty - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numWarehousePickedQty - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numWarehousePickedQty
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numWarehousePickedQty
									SET @numToAllocation = @numToAllocation + @numWarehousePickedQty
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numWarehousePickedQty - @numToBackOrder)
									SET @numToBackOrder = 0
								END
								

								SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
							END

							--UPDATE INVENTORY AND WAREHOUSE TRACKING
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numFromOnHand,
							@numOnAllocation=@numFromAllocation,
							@numOnBackOrder=@numFromBackOrder,
							@numOnOrder=@numFromOnOrder,
							@numWarehouseItemID=@numTempWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

							SET @vcDescription = REPLACE(@vcDescription,'picked','receieved')
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numToOnHand,
							@numOnAllocation=@numToAllocation,
							@numOnBackOrder=@numToBackOrder,
							@numOnOrder=@numToOnOrder,
							@numWarehouseItemID=@numWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

						END
						ELSE IF @numWarehousePickedQty > 0
						BEGIN
							IF @numWarehousePickedQty > @numFromAllocation
							BEGIN
								RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
							END
							ELSE
							BEGIN
								IF @numWarehousePickedQty >= @numQtyLeftToPick
								BEGIN
									SET @numQtyLeftToPick = 0
								END
								ELSE
								BEGIN
									SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
								END
							END
						END

						IF @numQtyLeftToPick <= 0
							BREAK

						SET @k = @k + 1
					END
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0 AND @numOppChildItemID=0 AND @numOppKitChildItemID=0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END


		DECLARE @TempPickList TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,vcItems VARCHAR(MAX)
		)

		INSERT INTO @TempPickList
		(
			numOppID
			,vcItems
		)
		SELECT
			numOppID
			,CONCAT('<NewDataSet>',stuff((
					SELECT 
						CONCAT(' <BizDocItems><OppItemID>',numOppItemID,'</OppItemID>','<Quantity>',ISNULL(numQtyPicked,0),'</Quantity>','<Notes></Notes><monPrice>0</monPrice></BizDocItems>')
					FROM 
						@TempOrders TOR
					WHERE 
						TOR.numOppID = TORMain.numOppID
						AND ISNULL(numOppChildItemID,0) = 0
						AND ISNULL(numOppKitChildItemID,0) = 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),'</NewDataSet>')
		FROM
			@TempOrders TORMain
		GROUP BY
			numOppID


		SET @k = 1
		SELECT @kCount=COUNT(*) FROM @TempPickList
		DECLARE @numTempOppID NUMERIC(18,0)
		DECLARE @vcItems VARCHAR(MAX)
		DECLARE @intUsedShippingCompany NUMERIC(18,0)
		DECLARE @dtFromDate DATETIME
		DECLARE @numBizDocTempID NUMERIC(18,0)

		SELECT TOP 1
			@numBizDocTempID=numBizDocTempID
		FROM
			BizDocTemplate
		WHERE
			numDomainID=@numDomainID
			AND numBizDocID= 29397
		ORDER BY
			ISNULL(bitEnabled,0) DESC
			,ISNULL(bitDefault,0) DESC

		WHILE @k <= @kCount
		BEGIN
			SELECT @numTempOppID=numOppID,@vcItems=vcItems FROM @TempPickList WHERE ID=@k
			
			IF ISNULL(@vcItems,'') <> ''
			BEGIN
				SELECT 
					@intUsedShippingCompany = ISNULL(intUsedShippingCompany,0)
					,@dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())
				FROM 
					OpportunityMaster
				WHERE
					numOppId=@numTempOppID

				EXEC USP_CreateBizDocs                                  
					 @numTempOppID,                        
					 29397,                        
					 @numUserCntID,                        
					 0,
					 '',
					 1,
					 @vcItems,
					 @intUsedShippingCompany,
					 '',
					 @dtFromDate,
					 0,
					 0,
					 '',
					 0,
					 0,
					 @ClientTimeZoneOffset,
					 0,
					 0,
					 @numBizDocTempID,
					 '',
					 '',
					 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
					 0,
					 1,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 NULL,
					 0,
					 '',
					 0,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 0
			END

			SET @k = @k + 1
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
	@byteMode as tinyint=null,                        
	@numOppId as numeric(9)=null,                        
	@numOppBizDocsId as numeric(9)=null,
	@ClientTimeZoneOffset AS INT=0,
	@numDomainID AS NUMERIC(18,0) = NULL,
	@numUserCntID AS NUMERIC(18,0) = NULL
)                        
AS      
BEGIN
	DECLARE @ParentBizDoc AS NUMERIC(9)=0
	DECLARE @lngJournalId AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
    
	SELECT 
		ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
		ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
		ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
	FROM 
		OpportunityMaster 
	JOIN 
		DivisionMaster 
	ON 
		DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
	JOIN 
		CompanyInfo
	ON 
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	WHERE 
		numOppId = @numOppId
		                                  
	IF @byteMode= 1                        
	BEGIN 
		DECLARE @numBizDocID AS INT
		DECLARE @bitFulfilled AS BIT
		Declare @tintOppType as tinyint
		DECLARE @tintShipped AS BIT
		DECLARE @bitAuthoritativeBizDoc AS BIT

		SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
		SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

		IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
			RETURN
		END

		IF EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		IF EXISTS (SELECT OBPD.* FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397 AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId) > 0
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT',16,1)
			RETURN
		END

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397
		BEGIN
			-- Revert Allocation
			EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
		END

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocsId NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppBizDocsId
		)
		SELECT
			numOppBizDocsId
		FROM
			OpportunityBizDocs 
		WHERE 
			numSourceBizDocId= @numOppBizDocsId

		BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT

			SELECT @iCount = COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @ParentBizDoc=numOppBizDocsId FROM @TEMP WHERE ID = @i

				IF NOT EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @ParentBizDoc)
				BEGIN
					EXEC USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END

				SET @i = @i + 1
			END

			--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
			IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
			BEGIN
				EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
			END

			IF @numBizDocID = 29397
			BEGIN
				UPDATE
					OI
				SET
					OI.numQtyPicked = (CASE 
											WHEN ISNULL(OI.numQtyPicked,0) > ISNULL(OBDI.numUnitHour,0) 
											THEN (ISNULL(OI.numQtyPicked,0) - ISNULL(OBDI.numUnitHour,0)) 
											ELSE 0 
										END)
				FROM	
					OpportunityBizDocItems OBDI
				INNER JOIN
					OpportunityItems OI
				ON
					OBDI.numOppItemID = OI.numoppitemtCode
				WHERE
					OBDI.numOppBizDocID=@numOppBizDocsId
			END

			DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
			DELETE  FROM OpportunityBizDocItems WHERE numOppBizDocID=@numOppBizDocsId              
			DELETE FROM OpportunityRecurring WHERE numOppBizDocID=@numOppBizDocsId and tintRecurringType=4
			UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId
				--Delete All entries for current bizdoc
			--EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
			DELETE OBPD FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId
			DELETE ECI FROM [EmbeddedCostItems] ECI INNER JOIN EmbeddedCost EC ON ECI.numEmbeddedCostID=EC.numEmbeddedCostID WHERE EC.[numOppBizDocID]=@numOppBizDocsId AND EC.[numDomainID] = @numDomainID
			DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId	AND [numDomainID] = @numDomainID
			DELETE OBDD	FROM OpportunityBizDocsDetails OBDD INNER JOIN EmbeddedCost EC ON OBDD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE	numOppBizDocID = @numOppBizDocsId

		

			--Credit Balance
			DECLARE @monCreditAmount AS DECIMAL(20,5);
			SET @monCreditAmount=0
			SET @numDivisionID=0

			SELECT 
				@monCreditAmount=ISNULL(oppBD.monCreditAmount,0)
				,@numDivisionID=Om.numDivisionID
				,@numOppId=OM.numOppId 
			FROM 
				OpportunityBizDocs oppBD 
			JOIN 
				OpportunityMaster Om 
			ON 
				OM.numOppId=oppBD.numOppId 
			WHERE
				numOppBizDocsId = @numOppBizDocsId

			DELETE FROM [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
			IF @tintOppType=1 --Sales
				UPDATE DivisionMaster SET monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			ELSE IF @tintOppType=2 --Purchase
				UPDATE DivisionMaster SET monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


			DELETE FROM DocumentWorkflow WHERE numDocID=@numOppBizDocsId AND cDocType='B'      
			DELETE BA FROM BizDocAction BA INNER JOIN BizActionDetails BAD ON BA.numBizActionId=BAD.numBizActionId WHERE BAD.numOppBizDocsId =@numOppBizDocsId and btDocType=1
			DELETE FROM BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=1


			DELETE GJD FROM General_Journal_Details GJD INNER JOIN General_Journal_Header GJH ON GJD.numJournalID=GJH.numJournal_Id WHERE GJH.numOppId=@numOppId AND GJH.numOppBizDocsId=@numOppBizDocsId AND GJH.numDomainId=@numDomainID
			DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID

			DELETE FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocsId                        

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH             
	END                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
DECLARE @numCurrencyID NUMERIC(18,0)
DECLARE @numARContactPosition NUMERIC(18,0)

SELECT @numCurrencyID=ISNULL(numCurrencyID,0),@numARContactPosition=ISNULL(numARContactPosition,0) FROM Domain WHERE numDomainId=@numDomainId
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numCurrencyID
																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID
  ,con.numContactid
  ,isnull(con.vcEmail,'') AS vcEmail
  ,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) dtFromDate,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
		,0 AS IsFulfillmentGeneratedOnPickList
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty,bitMappingRequired,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID
				,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty,ISNULL(X.bitMappingRequired,0),X.vcSKU,X.vcASIN
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,bitMappingRequired BIT,vcASIN VARCHAR(100)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate
				,vcChildKitSelectedItems,numWOQty,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount
				,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty,X.vcSKU,X.vcASIN
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,vcSKU VARCHAR(100),vcASIN VARCHAR(100)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1
	BEGIN
		EXEC USP_OpportunityMaster_AddVendor @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_AddVendor')
DROP PROCEDURE dbo.USP_OpportunityMaster_AddVendor
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_AddVendor]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numDivisionId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,monPrice DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numDivisionId
		,numItemCode
		,monPrice
	)
	SELECT 
		numDivisionId
		,numItemCode
		,monPrice
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	WHERE 
		OM.numDomainId=@numDomainID 
		AND OM.numOppId=@numOppID 
		AND OM.tintOppType=2 
		AND OM.tintOppStatus=1

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numDivisionId NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monPrice DECIMAL(20,5)

	SELECT @iCount=COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numDivisionId=numDivisionId
			,@numItemCode=numItemCode
			,@monPrice=ISNULL(monPrice,0)
		FROM 
			@TEMP 
		WHERE ID=@i

		EXEC USP_AddVendor @numDivisionId,@numDomainID,@numItemCode,0,'',0,@monPrice
			 

		SET @i = @i + 1
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetARContact')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetARContact
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetARContact]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT TOP 1 
		ACI.numContactId 
		,ACI.vcEmail
	FROM
		OpportunityMaster OM
	INNER JOIN
		Domain D
	ON
		OM.numDomainId = D.numDomainId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		OM.numDivisionId = ACI.numDivisionId
	WHERE 
		OM.numDomainId = @numDomainID
		AND OM.numOppID = @numOppID		
		AND 1 = (CASE WHEN ISNULL(D.numARContactPosition,0) > 0 AND ACI.vcPosition=D.numARContactPosition THEN 1 ELSE 0 END)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetFieldValue')
DROP PROCEDURE USP_OpportunityMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numOppID NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID
	END
	ELSE IF @vcFieldName = 'vcPOppName'
	BEGIN
		SELECT ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReBuildIndex')
DROP PROCEDURE USP_ReBuildIndex
GO
CREATE PROCEDURE USP_ReBuildIndex
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,vcTableName VARCHAR(100)
	)

	INSERT INTO @TEMP (vcTableName) SELECT Name from sys.tables WHERE type_desc = 'USER_TABLE' AND NAME <>'Internet_Sales_Order_Details' ORDER BY Name

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @vcTableName VARCHAR(100)
	DECLARE @sql NVARCHAR(1000)

	SELECT @iCount = COUNT(*) FROM @TEMP
	
	WHILE @i <= @iCount
	BEGIN
		SELECT @vcTableName=vcTableName FROM @TEMP WHERE ID = @i

		BEGIN TRY
			SET @sql = 'ALTER INDEX ALL ON ' + @vcTableName + ' REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON)'
			Exec(@sql)

			PRINT CONCAT('INDEX REBUILD COMPLETED: ',@vcTableName)
		END TRY
		BEGIN CATCH
			PRINT CONCAT('INDEX REBUILD FAILED: ',@vcTableName)
		END CATCH

		SET @i = @i + 1
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Save]
(
	@ID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintAction TINYINT
	,@dtActionTime DATETIME
	,@numProcessedQty FLOAT
	,@numReasonForPause NUMERIC(18,0)
	,@vcNotes VARCHAR(1000)
	,@bitManualEntry BIT
)
AS 
BEGIN

	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF @tintAction=1 AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=1 AND ID <> ISNULL(@ID,0))
		BEGIN
			RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID=@ID)
			BEGIN
				UPDATE
					StagePercentageDetailsTaskTimeLog
				SET
					tintAction = @tintAction
					,dtActionTime =  DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
					,numProcessedQty=@numProcessedQty
					,vcNotes = @vcNotes
					,bitManualEntry=@bitManualEntry
					,numModifiedBy = @numUserCntID
					,numReasonForPause = @numReasonForPause
					,dtModifiedDate = GETUTCDATE()
				WHERE
					ID = @ID			
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID)
				BEGIN
					DECLARE @numTaskAssignee AS NUMERIC(18,0)
					SELECT @numTaskAssignee=numAssignTo FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

					INSERT INTO StagePercentageDetailsTaskTimeLog
					(
						numDomainID
						,numUserCntID
						,numTaskID
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry
					)
					VALUES
					(
						@numDomainID
						,@numTaskAssignee
						,@numTaskID
						,@tintAction
						,DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
						,@numProcessedQty
						,@numReasonForPause
						,@vcNotes
						,@bitManualEntry
					)
				END
				ELSE
				BEGIN
					RAISERROR('TASK_DOES_NOT_EXISTS',16,1)
				END
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	DECLARE @numProjectId NUMERIC=0
	SET @numProjectId = ISNULL((SELECT TOP 1 numProjectId FROM StagePercentageDetailsTask where numTaskId=@numTaskID),0)
	IF(@numProjectId>0)
	BEGIN
		DECLARE @dtmActualStartDate AS DATETIME
		DECLARE @dtmActualFinishDate AS DATETIME
		SET @dtmActualStartDate = (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
		
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID)
		SET @intTotalTaskClosed=(SELECT COUNT(distinct numTaskID) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID) AND tintAction=4)
		IF(@intTotalTaskCount=@intTotalTaskClosed)
		BEGIN
			SET @dtmActualFinishDate = (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
			UPDATE ProjectsMaster SET dtCompletionDate=@dtmActualFinishDate WHERE numProId=@numProjectId
		END
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress(
				numProId,
				numOppId,
				numDomainId,
				intTotalProgress,
				numWorkOrderId
			)VALUES(
				@numProjectId,
				NULL,
				@numDomainID,
				@intTotalProgress,
				NULL
			)
		END

		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContractID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @balanceContractTime INT

		-- REMOVE EXISTING CONTRACT LOG ENTRY
		IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				WHERE
					CL.numDivisionID = @numDomainID
					AND CL.numReferenceId = @numTaskID
					AND vcDetails='1')
		BEGIN
			SELECT 
				@numContractsLogId=numContractsLogId
				,@numContractID = CL.numContractId
				,@numUsedTime = numUsedTime
			FROM
				ContractsLog CL 
			INNER JOIN
				Contracts C
			ON
				CL.numContractId = C.numContractId
			WHERE
				C.numDomainId = @numDomainID
				AND CL.numReferenceId = @numTaskID
				AND vcDetails='1'

			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END

		IF EXISTS (SELECT 
						C.numContractId 
					FROM 
						ProjectsMaster PM 
					INNER JOIN 
						Contracts C 
					ON 
						PM.numDivisionId=C.numDivisonId 
					WHERE 
						PM.numDomainID = @numDomainID
						AND PM.numProID = @numProjectId
						AND C.numDomainId=@numDomainID
						AND C.numDivisonId=PM.numDivisionId 
						AND C.intType=1)
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = @numDomainID AND numTaskId = @numTaskID AND tintAction = 4)
			BEGIN
				SELECT TOP 1
					@numContractID =  C.numContractId
				FROM
					ProjectsMaster PM
				INNER JOIN 
					Contracts C 
				ON 
					PM.numDivisionId=C.numDivisonId 
				WHERE 
					PM.numDomainID = @numDomainID
					AND PM.numProID = @numProjectId
					AND C.numDomainId=@numDomainID
					AND C.numDivisonId=PM.numDivisionId 
					AND C.intType=1
				ORDER BY
					C.numContractId DESC

				-- GET UPDATED TIME
				SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)

				-- CREATE NEW ENTRY FOR
				UPDATE Contracts SET timeUsed=timeUsed+(ISNULL(@numUsedTime,0) * 60),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
				SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numContractId=@numContractID)
				INSERT INTO ContractsLog 
				(
					intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
				)
				SELECT 
					1,Contracts.numDivisonId,@numTaskID,GETUTCDATE(),0,1,(ISNULL(@numUsedTime,0) * 60),@balanceContractTime,numContractId,1
				FROM 
					Contracts
				WHERE 
					numContractId = @numContractID
			END
		END
	END

	SELECT 
		ISNULL(numWorkOrderId,0) AS numWorkOrderId 
	FROM 
		StagePercentageDetailsTask 
	WHERE 
		numDomainID=@numDomainID 
		AND numTaskId=@numTaskID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@bitRemoveGlobalLocation BIT = 0,
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0,
@bitDisplayCustomField BIT = 0,
@bitFollowupAnytime BIT = 0,
@bitpartycalendarTitle BIT = 0,
@bitpartycalendarLocation BIT = 0,
@bitpartycalendarDescription BIT = 0,
@bitREQPOApproval BIT = 0,
@bitARInvoiceDue BIT = 0,
@bitAPBillsDue BIT = 0,
@bitItemsToPickPackShip BIT = 0,
@bitItemsToInvoice BIT = 0,
@bitSalesOrderToClose BIT = 0,
@bitItemsToPutAway BIT = 0,
@bitItemsToBill BIT = 0,
@bitPosToClose BIT = 0,
@bitPOToClose BIT = 0,
@bitBOMSToPick BIT = 0,
@vchREQPOApprovalEmp  VARCHAR(500)='',
@vchARInvoiceDue VARCHAR(500)='',
@vchAPBillsDue VARCHAR(500)='',
@vchItemsToPickPackShip VARCHAR(500)='',
@vchItemsToInvoice VARCHAR(500)='',
@vchPriceMarginApproval VARCHAR(500)='',
@vchSalesOrderToClose VARCHAR(500)='',
@vchItemsToPutAway VARCHAR(500)='',
@vchItemsToBill VARCHAR(500)='',
@vchPosToClose VARCHAR(500)='',
@vchPOToClose VARCHAR(500)='',
@vchBOMSToPick VARCHAR(500)='',
@decReqPOMinValue VARCHAR(500)='',
@bitUseOnlyActionItems BIT = 0,
@bitDisplayContractElement BIT,
@vcEmployeeForContractTimeElement VARCHAR(500)='',
@numARContactPosition NUMERIC(18,0) = 0,
@bitShowCardConnectLink BIT = 0,
@vcBluePayFormName VARCHAR(500)='',
@vcBluePaySuccessURL VARCHAR(500)='',
@vcBluePayDeclineURL VARCHAR(500)='',
@bitUseDeluxeCheckStock BIT = 1,
@bitEnableSmartyStreets BIT=0,
@vcSmartyStreetsAPIKeys VARCHAR(500)='',
@bitUsePreviousEmailBizDoc BIT = 0
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID,
		bitDisplayCustomField=@bitDisplayCustomField,
		bitFollowupAnytime=@bitFollowupAnytime,
		bitpartycalendarTitle=@bitpartycalendarTitle,
		bitpartycalendarLocation=@bitpartycalendarLocation,
		bitpartycalendarDescription=@bitpartycalendarDescription,
		bitREQPOApproval=@bitREQPOApproval,
		bitARInvoiceDue=@bitARInvoiceDue,
		bitAPBillsDue=@bitAPBillsDue,
		bitItemsToPickPackShip=@bitItemsToPickPackShip,
		bitItemsToInvoice=@bitItemsToInvoice,
		bitSalesOrderToClose=@bitSalesOrderToClose,
		bitItemsToPutAway=@bitItemsToPutAway,
		bitItemsToBill=@bitItemsToBill,
		bitPosToClose=@bitPosToClose,
		bitPOToClose=@bitPOToClose,
		bitBOMSToPick=@bitBOMSToPick,
		vchREQPOApprovalEmp=@vchREQPOApprovalEmp,
		vchARInvoiceDue=@vchARInvoiceDue,
		vchAPBillsDue=@vchAPBillsDue,
		vchItemsToPickPackShip=@vchItemsToPickPackShip,
		vchItemsToInvoice=@vchItemsToInvoice,
		vchSalesOrderToClose=@vchSalesOrderToClose,
		vchItemsToPutAway=@vchItemsToPutAway,
		vchItemsToBill=@vchItemsToBill,
		vchPosToClose=@vchPosToClose,
		vchPOToClose=@vchPOToClose,
		vchBOMSToPick=@vchBOMSToPick,
		decReqPOMinValue=@decReqPOMinValue,
		bitUseOnlyActionItems = @bitUseOnlyActionItems,
		bitDisplayContractElement = @bitDisplayContractElement,
		vcEmployeeForContractTimeElement=@vcEmployeeForContractTimeElement,
		numARContactPosition= @numARContactPosition,
		bitShowCardConnectLink = @bitShowCardConnectLink,
		vcBluePayFormName=@vcBluePayFormName,
		vcBluePaySuccessURL=@vcBluePaySuccessURL,
		vcBluePayDeclineURL=@vcBluePayDeclineURL,
		bitUseDeluxeCheckStock=@bitUseDeluxeCheckStock,
		bitEnableSmartyStreets = @bitEnableSmartyStreets,
		vcSmartyStreetsAPIKeys = @vcSmartyStreetsAPIKeys,
		bitUsePreviousEmailBizDoc=@bitUsePreviousEmailBizDoc
	WHERE 
		numDomainId=@numDomainID

	IF(LEN(@vchPriceMarginApproval)>0)
	BEGIN
		DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

		INSERT INTO UnitPriceApprover
			(
				numDomainID,
				numUserID
			)SELECT @numDomainID,Items FROM Split(@vchPriceMarginApproval,',') WHERE Items<>''
	END
	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME,
	@fltReorderQty FLOAT,
	@fltReorderPoint FLOAT,
	@bitAutomateReorderPoint BIT
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	UPDATE Item SET fltReorderQty=@fltReorderQty,bitAutomateReorderPoint=@bitAutomateReorderPoint WHERE numItemCode=@numItemCode
	UPDATE WareHouseItems SET numReorder=@fltReorderPoint WHERE numItemID=@numItemCode


	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDoc INT    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
		SELECT 
			* 
		INTO 
			#temp 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Item',2)    
		WITH 
		( 
			numWareHouseItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,intAdjust FLOAT
			,monAverageCost DECIMAL(20,5)
		) 
		
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
		INTO 
			#tempSerialLotNO 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
		WITH 
			(
				numItemCode NUMERIC(18,0)
				,numWareHouseID NUMERIC(18,0)
				,numWareHouseItemID NUMERIC(18,0)
				,vcSerialNo VARCHAR(3000)
				,byteMode TINYINT
			) 
		
		EXEC sp_xml_removedocument @hDoc  

		DECLARE @numOnHand FLOAT
		DECLARE @numAllocation FLOAT
		DECLARE @numBackOrder FLOAT
		DECLARE @numWareHouseItemID AS NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
			
		SELECT 
			@numWareHouseItemID = MIN(numWareHouseItemID) 
		FROM 
			#temp
			
		DECLARE @bitLotNo AS BIT = 0  
		DECLARE @bitSerialized AS BIT = 0  
		DECLARE @monItemAvgCost DECIMAL(20,5)
		DECLARE @monUnitCost DECIMAL(20,5)
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		DECLARE @TotalOnHand AS FLOAT;
		DECLARE @numNewQtyReceived AS FLOAT

		WHILE @numWareHouseItemID>0    
		BEGIN		  
			SET @monAvgCost = 0
			SET @TotalOnHand = 0
			SET @numNewQtyReceived = 0

			SELECT TOP 1
				@bitLotNo=ISNULL(Item.bitLotNo,0)
				,@bitSerialized=ISNULL(Item.bitSerialized,0)
				,@monItemAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost, 0) END)
				,@monUnitCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(#temp.monAverageCost, 0) END)
				,@numNewQtyReceived = ISNULL(#temp.intAdjust,0)
			FROM 
				Item 
			INNER JOIN 
				#temp 
			ON 
				item.numItemCode=#temp.numItemCode 
			INNER JOIN
				WareHouseItems
			ON
				#temp.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			WHERE 
				#temp.numWareHouseItemID=@numWareHouseItemID 
				AND Item.numDomainID=@numDomainID
				AND WareHouseItems.numDomainID=@numDomainID

			IF @bitLotNo=0 AND @bitSerialized=0
			BEGIN		
				IF (SELECT 
						ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
					FROM 
						WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID 
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
				BEGIN
					SELECT @numTempItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
					--Updating the Average Cost
					SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numTempItemCode
					IF @TotalOnHand + @numNewQtyReceived > 0
					BEGIN
						SET @monAvgCost = ((@TotalOnHand * @monItemAvgCost) + (@numNewQtyReceived * @monUnitCost)) / ( @TotalOnHand + @numNewQtyReceived )
					END
					ELSE
					BEGIN
						SET @monAvgCost = @monItemAvgCost
					END

					UPDATE  
						item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
						,bintModifiedDate=GETUTCDATE()
						,numModifiedBy=@numUserCntID
					WHERE 
						numItemCode = @numTempItemCode

					IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END)
							,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END
					ELSE
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END	  
								
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numTempItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = 'Inventory Adjustment', --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate = @dtAdjustmentDate,
					@numDomainId = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
				END
			END	  
				    
			SELECT TOP 1 
				@numWareHouseItemID = numWareHouseItemID 
			FROM 
				#temp 
			WHERE 
				numWareHouseItemID >@numWareHouseItemID 
			ORDER BY 
				numWareHouseItemID  
				    
			IF @@rowcount=0 
				SET @numWareHouseItemID =0    
		END    
    

		DROP TABLE #temp
    
    
		-------------Serial/Lot #s----------------
		DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
		SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
		DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
				@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
		DECLARE @posComma int, @strKeyVal varchar(20)
		    
		WHILE @minRowNo <= @maxRowNo
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
				@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
			SET @posComma=0
		
			SET @vcSerialNo=RTRIM(@vcSerialNo)
			IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

			SET @posComma=PatIndex('%,%', @vcSerialNo)
			WHILE @posComma>1
				BEGIN
					SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
					DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END		
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END
	  
				SET @numWareHouseItmsDTLID=0
				SET @OldQty=0
				SET @vcComments=''
			
				select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
						from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
						vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
				IF @byteMode=0 --Add
					SET @numQty=@OldQty + @strQty
				ELSE --Deduct
					SET @numQty=@OldQty - @strQty
			
				EXEC dbo.USP_AddUpdateWareHouseForItems
						@numItemCode = @numTempItemCode, --  numeric(9, 0)
						@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
						@numWareHouseItemID =@numWareHouseItemID,
						@numDomainID = @numDomainId, --  numeric(9, 0)
						@vcSerialNo = @strName, --  varchar(100)
						@vcComments = @vcComments, -- varchar(1000)
						@numQty = @numQty, --  numeric(18, 0)
						@byteMode = 5, --  tinyint
						@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
						@numUserCntID = @numUserCntID, --  numeric(9, 0)
						@dtAdjustmentDate = @dtAdjustmentDate
	  
				SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
				SET @posComma=PatIndex('%,%',@vcSerialNo)
			END

			SET @minRowNo=@minRowNo+1
		END
	
		DROP TABLE #tempSerialLotNO

		

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode
	END   
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateReorderPointAndQuantity')
DROP PROCEDURE dbo.USP_UpdateReorderPointAndQuantity
GO
CREATE PROCEDURE [dbo].[USP_UpdateReorderPointAndQuantity]

AS 
BEGIN
	IF DATENAME(WEEKDAY,GETDATE()) = 'Sunday' AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numDomainID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numVendorID NUMERIC(18,0)
			,numLeadTimeDays NUMERIC(18,0)
			,tintReorderPointBasedOn TINYINT
			,intReorderPointDays INT
			,intReorderPointPercent INT
		)

		INSERT INTO @TEMP
		(
			numDomainID
			,numItemCode
			,numVendorID
			,tintReorderPointBasedOn
			,intReorderPointDays
			,intReorderPointPercent
		)
		SELECT TOP 500
			Item.numDomainID
			,Item.numItemCode
			,Item.numVendorID
			,ISNULL(tintReorderPointBasedOn,1)
			,ISNULL(intReorderPointDays,30)
			,ISNULL(intReorderPointPercent,0)
		FROM
			Item
		INNER JOIN
			Domain
		ON
			Item.numDomainID = Domain.numDomainId
		WHERE
			ISNULL(bitAutomateReorderPoint,0) = 1
			AND (dtLastAutomateReorderPoint IS NULL OR dtLastAutomateReorderPoint <> CAST(GETDATE() AS DATE))

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT 
		DECLARE @numDomainID NUMERIC(18,0)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltUnitSold FLOAT
		DECLARE @numLeadTimeDays NUMERIC(18,0)
		DECLARE @tintReorderPointBasedOn TINYINT
		DECLARE @intReorderPointDays INT
		DECLARE @intReorderPointPercent INT

		SELECT @iCount = COUNT(*) FROM @TEMP

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numDomainID=numDomainID
				,@numItemCode=numItemCode
				,@numVendorID=numVendorID
				,@tintReorderPointBasedOn=tintReorderPointBasedOn
				,@intReorderPointDays=intReorderPointDays
				,@intReorderPointPercent=intReorderPointPercent
			FROM
				@TEMP
			WHERE
				ID=@i

			SET @numLeadTimeDays = ISNULL((SELECT TOP 1 
												numListValue 
											FROM 
												VendorShipmentMethod
											INNER JOIN
												AddressDetails 
											ON
												AddressDetails.numRecordID = VendorShipmentMethod.numVendorID
												AND tintAddressOf=2 
												AND tintAddressType=2 
												AND ISNULL(bitIsPrimary,0)=1
											WHERE
												VendorShipmentMethod.numVendorID = @numVendorID),0)

			SET @fltUnitSold = ISNULL((SELECT 
											SUM(OI.numUnitHour)
										FROM 
											OpportunityMaster OM
										INNER JOIN 
											OpportunityItems OI
										ON
											OM.numOppId = OI.numOppId
										WHERE
											OM.numDomainId=@numDomainID
											AND OM.tintOppType=1
											AND OM.tintOppStatus=1
											AND OI.numItemCode = @numItemCode
											AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(DATEADD(DAY,-@intReorderPointDays,(CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END)) AS DATE) AND CAST((CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END) AS DATE)),0)
			
			PRINT CAST(DATEADD(DAY,-@intReorderPointDays,(CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END)) AS DATE)
			PRINT @fltUnitSold
			
			UPDATE WareHouseItems SET numReorder = CEILING(ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0)) WHERE numDomainID=@numDomainID AND numItemID=@numItemCode

			UPDATE
				Item
			SET
				fltReorderQty = CEILING(ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0) + (ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0) * @intReorderPointPercent / 100))  
				,dtLastAutomateReorderPoint = GETDATE()
			WHERE
				numItemCode=@numItemCode
	

			SET @i = @i + 1
		END
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 47
		BEGIN
			UPDATE dbo.Domain SET bitCloneLocationWithItem = @numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 48
		BEGIN
			IF ISNULL(CAST(@vcText AS VARCHAR),'') = ''
			BEGIN
				DELETE FROM DomainMarketplace WHERE numDomainID=@numDomainID
			END
			ELSE
			BEGIN
				DELETE FROM DomainMarketplace WHERE numDomainID=@numDomainID AND numMarketplaceID NOT IN (SELECT Id FROM SplitIDs(@vcText,','))
				INSERT INTO DomainMarketplace (numDomainID,numMarketplaceID) SELECT @numDomainID,Id FROM SplitIDs(@vcText,',') WHERE Id NOT IN (SELECT numMarketplaceID FROM DomainMarketplace WHERE numDomainID=@numDomainID)
			END
		END

		IF @tintMode = 49
		BEGIN
			UPDATE dbo.OpportunityMaster SET intUsedShippingCompany=@numUpdateValueID,numShippingService=-1 WHERE numDomainId = @numDomainID AND numOppId=@numUpdateRecordID
		END

		IF @tintMode = 50
		BEGIN
			UPDATE dbo.StagePercentageDetailsTask SET vcTaskName=@vcText WHERE numDomainId = @numDomainID AND numTaskId=@numUpdateRecordID
		END

		IF @tintMode = 51
		BEGIN
			UPDATE dbo.Domain SET bitReceiveOrderWithNonMappedItem=CAST(@numUpdateValueID AS BIT) WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 52
		BEGIN
			UPDATE dbo.Domain SET numItemToUseForNonMappedItem=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 53
		BEGIN
			UPDATE dbo.Domain SET bitUsePredefinedCustomer=CAST(@numUpdateValueID AS BIT) WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 54
		BEGIN
			UPDATE dbo.Domain SET tintReorderPointBasedOn=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 55
		BEGIN
			UPDATE dbo.Domain SET intReorderPointDays=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 56
		BEGIN
			UPDATE dbo.Domain SET intReorderPointPercent=@numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItem')
DROP PROCEDURE USP_WarehouseItems_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItem]
(   
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUOMFactor AS FLOAT
	DECLARE @numPurchaseUOMFactor AS FLOAT
	DECLARE @bitKitParent BIT

	SELECT 
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@bitKitParent = ISNULL(bitKitParent,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode   

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)

	DECLARE @TEMPChildItems TABLE
	(
		numItemKitID NUMERIC(18,0), numItemCode NUMERIC(18,0), numQtyItemsReq FLOAT, numCalculatedQty FLOAT
	)

	IF @bitKitParent = 1
	BEGIN
		;WITH CTE(numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) AS 
		( 
			SELECT   
				CONVERT(NUMERIC(18,0),0),
				numItemCode,
				DTL.numQtyItemsReq,
				CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
			FROM
				Item
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID = numItemCode
			WHERE 
				numItemKitID = @numItemCode
			UNION ALL
			SELECT 
				Dtl.numItemKitID,
				i.numItemCode,
				DTL.numQtyItemsReq,
				CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
			FROM 
				Item i
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				Dtl.numChildItemID = i.numItemCode
			INNER JOIN 
				CTE c 
			ON 
				Dtl.numItemKitID = c.numItemCode
			WHERE    
				Dtl.numChildItemID != @numItemCode
		)

		INSERT INTO @TEMPChildItems (numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) SELECT numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty FROM CTE
	END

	SELECT
		WarehouseItems.numWareHouseItemID
		,WarehouseItems.numWarehouseID
		,WarehouseItems.numWLocationID
		,ISNULL(Warehouses.vcWareHouse,'') vcExternalLocation
		,numItemID
		,(CASE WHEN ISNULL(Item.bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(WarehouseItems.numItemID,WarehouseItems.numWareHouseID) ELSE 0 END) numBuildableQty
		,Item.numAssetChartAcntId
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 
			THEN ISNULL((SELECT CAST(FLOOR(MIN(CASE 
									WHEN ISNULL(WIINNER.numOnHand, 0) = 0 THEN 0
                                    WHEN ISNULL(WIINNER.numOnHand, 0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN ISNULL(WIINNER.numOnHand, 0) / TCI.numQtyItemsReq
                                    ELSE 0
                                    END)
						) AS FLOAT) FROM @TEMPChildItems TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode=WIINNER.numItemID AND WIINNER.numWareHouseID=WareHouseItems.numWareHouseID),0)
			ELSE ISNULL(numOnHand,0) + ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
		END AS [OnHand]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 OR ISNULL(Item.bitAssembly,0)=1 
			THEN 0 
			ELSE CAST(ISNULL((SELECT 
									SUM(numUnitHour) 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppID=OM.numOppID 
								WHERE 
									OM.numDomainID=@numDomainID 
									AND tintOppType=1 
									AND tintOppStatus=0 
									AND OI.numItemCode=WareHouseItems.numItemID 
									AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) 
		END AS [Requisitions]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder]
		,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) + ISNULL((SELECT SUM(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0) AS TotalOnHand
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,((ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)) * (CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(ISNULL(WareHouseItems.monWListPrice,0),2) Price
		,ISNULL(WareHouseItems.vcWHSKU,'') as SKU
		,ISNULL(WareHouseItems.vcBarCode,'') as BarCode
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
			ELSE ''
		END AS vcAttribute
		,dbo.fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,dbo.fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,dbo.fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,CAST((@numSaleUOMFactor * CASE 
										WHEN ISNULL(Item.bitKitParent,0)=1 
										THEN ISNULL((SELECT CAST(FLOOR(MIN(CASE 
																			WHEN ISNULL(WIINNER.numOnHand, 0) = 0 THEN 0
																			WHEN ISNULL(WIINNER.numOnHand, 0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN ISNULL(WIINNER.numOnHand, 0) / TCI.numQtyItemsReq
																			ELSE 0
																		END)) AS FLOAT) 
													FROM @TEMPChildItems TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode=WIINNER.numItemID AND WIINNER.numWareHouseID=WareHouseItems.numWareHouseID),0)
										ELSE ISNULL(numOnHand,0) 
									END) AS FLOAT) as OnHandUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,',"WarehouseLocationID":',WIInner.numWLocationID,',"Warehouse":"',ISNULL(W.vcWareHouse,''),'", "OnHand":',ISNULL(WIInner.numOnHand,0),', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							INNER JOIN
								Warehouses W
							ON
								WIInner.numWareHouseID = W.numWareHouseID
							INNER JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=@numItemCode
								AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']') vcInternalLocations
		,(CASE WHEN EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=WareHouseItems.numWareHouseItemID AND ISNULL(numQty,0) > 0) THEN 1 ELSE 0 END) bitSerialLotExists
	FROM
		WareHouseItems
	INNER JOIN
		Item
	ON
		WareHouseItems.numItemID = Item.numItemCode
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@numWLocationID,0) = 0 OR EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=@numWarehouseID AND ISNULL(WI.numWLocationID,0) = @numWLocationID))
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_UpdateQtyBuilt')
DROP PROCEDURE USP_WorkOrder_UpdateQtyBuilt
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_UpdateQtyBuilt]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numWOID NUMERIC(18,0)
AS
BEGIN
	-- CODE LEVEL TRANSACTION IS USED

	DECLARE @numQtyBuilt FLOAT
	DECLARE @numQtyItemsReq FLOAT 
	DECLARE @numProcessedQty FLOAT = 0
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	SELECT 
		@numQtyItemsReq = ISNULL(numQtyItemsReq,0)
		,@numQtyBuilt = ISNULL(numQtyBuilt,0)
		,@numWareHouseItemID=numWareHouseItemId
	FROM 
		WorkOrder 
	WHERE 
		WorkOrder.numDomainID = @numDomainID 
		AND WorkOrder.numWOId = @numWOID

	SET @numProcessedQty = ISNULL((SELECT
										MIN(numQtyBuilt) numQtyBuilt
									FROM
									(SELECT	
										numTaskId
										,(CASE 
											WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction = 4) 
											THEN ISNULL(WorkOrder.numQtyItemsReq,0)
											ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
										END) numQtyBuilt
									FROM
										StagePercentageDetailsTask SPDT
									INNER JOIN
										WorkOrder
									ON
										SPDT.numWorkOrderId = WorkOrder.numWOId
									WHERE
										numWorkOrderId=@numWOID) TEMP
									),0)

	SELECT
		Item.numItemCode
		,ISNULL(Item.numAssetChartAcntId,0) numAssetChartAcntId
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,(CASE 
			WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq  THEN ISNULL(@numProcessedQty,0) - @numQtyBuilt
			ELSE 0 
		END) numQtyBuilt
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID

	UPDATE
		WorkOrder
	SET 
		numQtyBuilt = ISNULL(numQtyBuilt,0) + (CASE 
													WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq THEN (ISNULL(@numProcessedQty,0) - @numQtyBuilt)
													ELSE 0 
												END)
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@vcFirstName VARCHAR(300)
	,@vcLastName VARCHAR(300)
	,@vcEmail VARCHAR(300)
	,@vcMarketplace VARCHAR(300)
AS 
BEGIN
	--1: SKU
	--2: UPC
	--3: ItemName
	--4: BizItemID
	--5: ASIN
	--6: CustomerPart
	--7: VendorPart

	DECLARE @numMarketplaceID NUMERIC(18,0) = 0

	IF EXISTS (SELECT ID FROM eChannelHub WHERE vcMarketplace=ISNULL(@vcMarketplace,''))
	BEGIN
		SELECT 
			@numMarketplaceID=ID
		FROM 
			eChannelHub 
		WHERE 
			vcMarketplace=ISNULL(@vcMarketplace,'')
	END

	DECLARE @bitUsePredefinedCustomer BIT

	SELECT @bitUsePredefinedCustomer = ISNULL(bitUsePredefinedCustomer,0) FROM Domain WHERE numDomainId=@numDomainID

	IF @bitUsePredefinedCustomer = 1
	BEGIN
		SET @numDivisionID = ISNULL((SELECT numDivisionID FROM DomainMarketplace WHERE numDomainID = @numDomainID AND numMarketplaceID = @numMarketplaceID),0)
	END
	ELSE 
	BEGIN
		IF ISNULL(@vcEmail,'') = ''
		BEGIN
			SET @numDivisionID = 0
		END
		ELSE
		BEGIN
			SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND vcEmail=ISNULL(@vcEmail,'')),0)
		END
	END

	SELECT 
		TEMP.numContactId AS numContactID
		,TEMP.numAssignedTo AS numAssignedTo
		,TEMP.numDivisionID
		,(CASE WHEN Domain.numDomainID=209 AND @numMarketplaceID IN (1,2,3,4,59,60,61,62) THEN 5 ELSE ISNULL(DMEmployer.tintInbound850PickItem,0) END) AS tintInbound850PickItem
		,TEMP.numCompanyId
		,TEMP.vcCompanyName
		,@numMarketplaceID numMarketplaceID
		,ISNULL(bitUsePredefinedCustomer,0) bitUsePredefinedCustomer
		,ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem
		,ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem
	FROM
		Domain
	INNER JOIN
		DivisionMaster DMEmployer
	ON
		Domain.numDivisionID = DMEmployer.numDivisionID
	OUTER APPLY
	(
		SELECT TOP 1
			DM.numDivisionID
			,CI.numCompanyId
			,CI.vcCompanyName
			,DM.numAssignedTo
			,ACI.numContactId
		FROM
			DivisionMaster DM
		INNER JOIN 
			CompanyInfo CI
		ON 
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			AdditionalContactsInformation ACI 
		ON
			DM.numDivisionID = ACI.numDivisionId
		
		WHERE
			DM.numDomainID = @numDomainID
			AND DM.numDivisionID = @numDivisionId
			AND ISNULL(ACI.bitPrimaryContact,0)=1
	) TEMP
	WHERE
		Domain.numDomainID = @numDomainID
END
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectSummary')
DROP PROCEDURE USP_GetProjectSummary
GO
CREATE PROCEDURE [dbo].[USP_GetProjectSummary]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
SELECT P.vcProjectID,C.vcCompanyName,DT.vcDomainName AS vcByCompanyName,ST.vcTaskName,
CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS dtFinishDate,
ltrim(right(convert(varchar(25),CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE ''
			END, 100), 7)) AS StartTiime
			,ltrim(right(convert(varchar(25),CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE ''
			END, 100), 7)) AS FinishTime,
			
			DATEDIFF(Minute,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE '' END),(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE '' END)) As TaskDurationComplete,
				ADC.vcFirstName+' '+ADC.vcLastName AS AssignedTo,
				(((ST.numHours*60)+ST.numMinutes)-(DATEDIFF(Minute,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE '' END),(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE '' END)) )) AS BalanceTime
				,dbo.GetTotalProgress(@numDomainID,@numProId,2,3,'',ST.numStageDetailsId) numTotalProgress,
				P.txtComments

FROM StagePercentageDetailsTask AS ST 
LEFT JOIN 
	ProjectsMaster AS P 
ON  ST.numProjectId=P.numProId 
LEFT JOIN DivisionMaster AS D 
ON P.numDivisionId=D.numDivisionID 
LEFT JOIN CompanyInfo AS C
ON D.numCompanyID=C.numCompanyId
LEFT JOIN Domain DT ON DT.numDomainId=P.numDomainId
LEFT JOIN AdditionalContactsInformation AS ADC ON ST.numAssignTo=ADC.numContactId
WHERE 
ST.numProjectId=@numProId AND ST.numDomainID=@numDomainID AND
(SELECT ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId ORDER BY dtActionTime DESC,ID DESC),0))=4
END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetStateCountryIDs')
DROP PROCEDURE dbo.USP_GetStateCountryIDs
GO

CREATE PROCEDURE [dbo].[USP_GetStateCountryIDs]
    @vcState VARCHAR(50),
	--@vcCountryCode VARCHAR(50),
	@vcCountryName Varchar(50),
    @numDomainID NUMERIC    

AS 
BEGIN
		--DECLARE @vcCountryName Varchar(50)
		DECLARE @numCountryID NUMERIC	
		DECLARE @numStateID NUMERIC

		--SET @vcCountryName = (SELECT TOP 1 vcCountryName from ShippingCountryMaster where vcCountryCode = @vcCountryCode)

		SET @numCountryID = (SELECT TOP 1 numListItemID FROM ListDetails WHERE numListID=40 AND (constFlag=1 OR numDomainID=@numDomainID) AND vcData = @vcCountryName)

		IF ISNULL(@numCountryID,0) = 0
		BEGIN

			INSERT INTO ListDetails
				(
					numListID
					,vcData
					,numCreatedBY
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,bitDelete
					,numDomainId
					,constFlag
					,sintOrder
				)
				VALUES
				(
					40
					,@vcCountryName
					,1
					,GETDATE()
					,1
					,GETDATE()
					,0
					,@numDomainID
					,0
					,0
				)

				SET @numCountryID = SCOPE_IDENTITY()			

		END


		SET @numStateID = (SELECT TOP 1 numStateID FROM [State] WHERE numDomainID=@numDomainID AND (vcState = @vcState OR vcAbbreviations = @vcState))

		IF ISNULL(@numStateID,0) = 0
		BEGIN
				
			INSERT INTO [STATE]
			(
				 numCountryID
				,vcState
				,numCreatedBy
				,bintCreatedDate
				,numModifiedBy
				,bintModifiedDate
				,numDomainID
				,constFlag
				,vcAbbreviations
				,numShippingZone

			)
			VALUES
			(
				@numCountryID
				,@vcState
				,1
				,GETDATE()
				,1
				,GETDATE()
				,@numDomainID
				,0
				,NULL
				,NULL
			)

            SET @numStateID = SCOPE_IDENTITY()		
        END

	SELECT @numCountryID AS 'CountryId', @numStateID AS 'StateId'
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitemidfor850')
DROP PROCEDURE usp_getwarehouseitemidfor850
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseItemIdFor850]        
	@numDomainID NUMERIC(18,0),        
	@numItemCode NUMERIC(18,0),
	@numQty FLOAT,
	@numOrderSource NUMERIC(18,0),
	@tintSourceType TINYINT,
	@numShipToCountry NUMERIC(18,0),
	@numShipToState NUMERIC(18,0)
AS
BEGIN         
	DECLARE @TEMP TABLE
	(	
		ID INT IDENTITY(1,1)
		,numWareHouseItemID NUMERIC(18,0)
		,bitWarehouseMapped BIT
	)

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)= 1)
	BEGIN
		DECLARE @bitKitParent BIT
		SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		DECLARE @TEMPWarehouse TABLE
		(
			numWarehouseItemID NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numOnHand FLOAT
		)

		INSERT INTO @TEMPWarehouse
		(
			numWarehouseItemID
			,numWarehouseID
			,numOnHand
		)
		SELECT
			WareHouseItems.numWarehouseItemID
			,WareHouseItems.numWareHouseID
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID),0)
		FROM
			WareHouseItems
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		WHERE
			WareHouseItems.numItemID = @numItemCode
			AND WarehouseLocation.numWLocationID IS NULL

		INSERT INTO @TEMP
		(
			numWareHouseItemID
			,bitWarehouseMapped
		)
		SELECT
			numWareHouseItemID
			,1
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
				,*
			FROM
			(
				SELECT
					TW.numWareHouseItemID
					,(CASE 
						WHEN @bitKitParent = 1 
						THEN dbo.fn_IsKitCanBeBuild(@numDomainID,@numItemCode,@numQty,TW.numWarehouseID,'')
						ELSE (CASE WHEN TW.numOnHand >= @numQty THEN 1 ELSE 0 END) 
					END) titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
				FROM
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
				INNER JOIN
					MassSalesFulfillmentWP
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
				INNER JOIN
					@TEMPWarehouse TW
				ON
					MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
				WHERE
					MassSalesFulfillmentWM.numDomainID = @numDomainID
					AND MassSalesFulfillmentWM.numOrderSource = @numOrderSource
					AND MassSalesFulfillmentWM.tintSourceType = @tintSourceType
					AND MassSalesFulfillmentWM.numCountryID = @numShipToCountry
					AND MassSalesFulfillmentWMState.numStateID = @numShipToState
					
			) TEMP
		) T1
		ORDER BY
			titOnHandOrder DESC, intOrder ASC	
	END
	
	IF EXISTS (SELECT ID FROM @TEMP)
	BEGIN
		SELECT TOP 1 numWareHouseItemID FROM @TEMP ORDER BY ID
	END
	ELSE
	BEGIN
		IF @numDomainID=209 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=1279 AND ISNULL(numWLocationID,0) = 0)
		BEGIN
			SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=1279 AND ISNULL(numWLocationID,0) = 0
		END
		ELSE
		BEGIN
			SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numDomainID = @numDomainID AND WarehouseItems.numItemID= @numItemCode
		END
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MigrationDataFromDomainToDomain')
DROP PROCEDURE USP_MigrationDataFromDomainToDomain
GO
CREATE PROCEDURE [dbo].[USP_MigrationDataFromDomainToDomain]    
@sourceDomainId AS NUMERIC(18,0)=0, -- 153 Kirk & Matz
@destinationDomainId AS NUMERIC(18,0)=0, --172 Boneta, Inc
@sourceGroupId AS NUMERIC(18,0)=0,--710
@destinationGroupId AS NUMERIC(18,0)=0,--858
@taskToPerform AS VARCHAR(100)=NULL--'1,2,3'
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION;  

-----------------------------------------------------------Dropdown List Sync----------------------------------------------------------------------
IF(1 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
PRINT 'Start Dropdown sync'
CREATE TABLE #tempSrcListMaster(
	numExistListID NUMERIC(18,0),
	vcListName VARCHAR(MAX), 
	bitDeleted Bit, 
	bitFixed Bit, 
	bitFlag Bit, 
	vcDataType VARCHAR(MAX), 
	numModuleID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)

INSERT INTO #tempSrcListMaster
SELECT numListID,vcListName,bitDeleted,bitFixed,bitFlag,vcDataType,numModuleID FROM listmaster WHERE numDomainID=@sourceDomainId OR bitFlag=1

DECLARE @totalListRecords AS INT=0
SET @totalListRecords =(SELECT COUNT(*) FROM #tempSrcListMaster)
DECLARE @i AS INT =1
DECLARE @vcListName AS  VARCHAR(MAX)
DECLARE @bitDeleted AS BIT
DECLARE @bitFixed AS BIT
DECLARE @bitFlag AS BIT
DECLARE @vcDataType AS VARCHAR(MAX)
DECLARE @numModuleID AS NUMERIC(18,0)
DECLARE @numListID AS NUMERIC(18,0)
DECLARE @numExistListID AS NUMERIC(18,0)

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numExistListID=numExistListID,@vcListName=vcListName,@bitDeleted=bitDeleted,@bitFixed=bitFixed,@bitFlag=bitFlag,@vcDataType=vcDataType,@numModuleID=numModuleID FROM #tempSrcListMaster WHERE ID=@i
	IF(@bitFlag=1)
	BEGIN
		SET @numListID =@numExistListID
	END
	ELSE IF NOT EXISTS(SELECT * FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	BEGIN
		INSERT INTO ListMaster(vcListName, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitDeleted, bitFixed, numDomainID, bitFlag, vcDataType, numModuleID)
		VALUES(@vcListName,1,GETDATE(),1,GETDATE(),@bitDeleted,@bitFixed,@destinationDomainId,@bitFlag,@vcDataType,@numModuleID)
		SET @numListID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SET @numListID =(SELECT TOP 1 numListID FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	END

	INSERT INTO ListDetails(numListID, vcData, bitDelete, numDomainID, constFlag, sintOrder, numListType, tintOppOrOrder,numListItemGroupId)
	SELECT @numListID,vcData, bitDelete, @destinationDomainId, constFlag, sintOrder, numListType, tintOppOrOrder,
	CASE WHEN ISNULL(numListItemGroupId,0)>0 THEN 
	(SELECT TOP 1 numListItemID FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID AND 
	vcData=(SELECT TOP 1 vcData FROM ListDetails As L where L.numListItemID=numListItemGroupId AND numDomainID=@sourceDomainId)) ELSE 0 END 
	FROM ListDetails WHERE 
	numDomainID=@sourceDomainId AND  numListID=@numExistListID AND 
	vcData NOT IN(SELECT vcData FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID)


	INSERT INTO ListOrder(numListId,numListItemId,numDomainId,intSortOrder)
	SELECT @numListID,(SELECT TOP 1 numListItemId FROM ListDetails WHERE numListId=@numListID AND vcData=LD.vcData),@destinationDomainId,intSortOrder FROM 
	ListOrder AS LO LEFT JOIN ListDetails AS LD ON Lo.numListItemId=LD.numListItemID where LO.numListId=@numExistListID AND LO.numDomainId=@sourceDomainId
	SET @i = @i+1
END
DROP TABLE #tempSrcListMaster
END
-----------------------------------------------------------Biz Form Wizard Configuration Sync----------------------------------------------------------------------
IF(2 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempFormFieldGroupConfigurarionMaster(
	numOldFormFieldGroupId NUMERIC(18,0),
	numFormFieldGroupId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempFormFieldGroupConfigurarionMaster(numOldFormFieldGroupId,numFormFieldGroupId)
SELECT numFormFieldGroupId,0 FROM FormFieldGroupConfigurarion WHERE numDomainId=@sourceDomainId 

SET @i=1;
SELECT @totalListRecords = COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster
DECLARE @numFormFieldGroupId AS NUMERIC(18,0)=0
DECLARE @numOldFormFieldGroupId AS NUMERIC(18,0)=0

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFormFieldGroupId = numOldFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE ID=@i
	IF EXISTS(SELECT numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numOldFormFieldGroupId) AND numDomainId=@destinationDomainId)
	BEGIN
		SELECT @numFormFieldGroupId = numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numOldFormFieldGroupId) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		INSERT INTO FormFieldGroupConfigurarion(numFormId, numGroupId, vcGroupName, numDomainId, numOrder)
		SELECT F.numFormId,F.numGroupId,F.vcGroupName,@destinationDomainId,F.numOrder FROM #tempFormFieldGroupConfigurarionMaster AS T LEFT JOIN FormFieldGroupConfigurarion AS F ON T.numOldFormFieldGroupId=F.numFormFieldGroupId WHERE F.numDomainId=@sourceDomainId AND ID=@i 
		SET @numFormFieldGroupId=SCOPE_IDENTITY();
	END
	UPDATE #tempFormFieldGroupConfigurarionMaster SET numFormFieldGroupId=@numFormFieldGroupId WHERE ID=@i
	SET @i=@i+1;
END
--ADD Custom Text Field to DycFieldMaster
CREATE TABLE #tempDycFieldMaster(
	numModuleId NUMERIC(18,0),
	numOldFieldId NUMERIC(18,0),
	numFieldId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)

INSERT INTO #tempDycFieldMaster(numOldFieldId,numFieldId,numModuleId)
SELECT  numFieldId,0,numModuleId FROM DycFieldMaster where numDomainID>0 AND numDomainID=@sourceDomainId
SET @i=1;
DECLARE @numFieldId AS NUMERIC(18,0)=0
DECLARE @numOldFieldId AS NUMERIC(18,0)=0
DECLARE @numOldModuleId AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycFieldMaster
WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFieldId = numOldFieldId,@numOldModuleId=numModuleId FROM #tempDycFieldMaster WHERE ID=@i
	IF EXISTS(SELECT numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM DycFieldMaster WHERE numFieldId=@numOldFieldId AND numModuleID=@numOldModuleId) AND numDomainId=@destinationDomainId AND numModuleID=@numOldModuleId)
	BEGIN
		SELECT @numFieldId = numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM DycFieldMaster WHERE numFieldId=@numOldFieldId AND numModuleID=@numOldModuleId) AND numDomainId=@destinationDomainId  AND numModuleID=@numOldModuleId
	END
	ELSE
	BEGIN
		INSERT INTO DycFieldMaster(numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField)
		SELECT D.numModuleID, @destinationDomainId, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField
		FROM #tempDycFieldMaster AS T LEFT JOIN DycFieldMaster AS D ON T.numOldFieldId=D.numFieldId WHERE D.numDomainId=@sourceDomainId AND D.numModuleID=@numOldModuleId AND ID=@i 
		SET @numFieldId=SCOPE_IDENTITY();
	END
	UPDATE #tempDycFieldMaster SET numFieldId=@numFieldId WHERE ID=@i
	SET @i=@i+1;
END

DELETE FROM DycFormField_Mapping WHERE  numDomainID>0 AND numDomainID=@destinationDomainId

INSERT INTO DycFormField_Mapping(numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
SELECT numModuleID, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE 0 END, @destinationDomainId, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, 
CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) ELSE 0 END
, intSectionID, bitAllowGridColor
FROM DycFormField_Mapping AS D WHERE numDomainID=@sourceDomainId AND numDomainID>0 AND 
(SELECT COUNT(*) FROM DycFormField_Mapping As DM WHERE DM.numModuleID=D.numModuleID AND numDomainID>0 AND numDomainID=@destinationDomainId AND DM.numFormID=D.numFormID AND DM.numFieldID=CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END)=0
AND (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldID) > 0


DELETE FROM DynamicFormField_Validation WHERE  numDomainID>0 AND  numDomainId=@destinationDomainId
INSERT INTO DynamicFormField_Validation(numFormId, numFormFieldId, numDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip)
SELECT numFormId, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) ELSE D.numFormFieldId END, @destinationDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip FROM DynamicFormField_Validation AS D WHERE numDomainID=@sourceDomainId

--Dynamic Configuration For User
DELETE FROM BizFormWizardMasterConfiguration  WHERE  numDomainId=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0

INSERT INTO BizFormWizardMasterConfiguration
		(
			numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId
		)
SELECT  numFormID,
			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END
			FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@sourceDomainId AND numGroupID=@sourceGroupId  AND bitCustom=0



CREATE TABLE #tempDycUserMaster(
	numUserCntID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempDycUserMaster(numUserCntID) SELECT numUserDetailId FROM UserMaster WHERE numGroupID=@destinationGroupId AND numDomainID=@destinationDomainId
SET @i=1;
DECLARE @numUserCntID AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycUserMaster

WHILE(@i<=@totalListRecords)
BEGIN
	
	SELECT @numUserCntID = numUserCntID FROM #tempDycUserMaster WHERE ID=@i
	DELETE FROM DycFormConfigurationDetails WHERE numDomainId=@destinationDomainId AND numUserCntID=@numUserCntID  AND bitCustom=0
	INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
	SELECT  numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			@numUserCntID
			bitGridConfiguration,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
			1
FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0 
	

	SET @i = @i+1
END

delete  DycFormConfigurationDetails where ISNULL(numUserCntId,0)=0 AND numAuthGroupID=@destinationGroupId
			and numFormId>0
			 AND numDomainId =@destinationDomainId
INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
SELECT	    numFormID,
			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			NULL,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
			bitDefaultByAdmin
FROM DycFormConfigurationDetails AS D WHERE ISNULL(numUserCntId,0)=0  AND numDomainId=@sourceDomainId  AND numAuthGroupID=@sourceGroupId and numFormId>0

--delete  DycFormConfigurationDetails where ISNULL(numUserCntId,0)=0  AND numAuthGroupID=@destinationGroupId
--			and numFormId>0
--			AND tintPageType=2 AND numDomainId =@destinationDomainId
--INSERT INTO DycFormConfigurationDetails(numFormID,
--				numFieldID,
--				intColumnNum,
--				intRowNum,
--				numDomainID,
--				numAuthGroupID,
--				numRelCntType,
--				tintPageType,
--				bitCustom,
--				numUserCntID,
--				numFormFieldGroupId,
--				bitDefaultByAdmin)
--SELECT	    numFormID,
--			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
--			intColumnNum,
--			intRowNum,
--			@destinationDomainId,
--			NULL,
--			numRelCntType,
--			tintPageType,
--			bitCustom,
--			0,
--			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
--			bitDefaultByAdmin
--FROM DycFormConfigurationDetails AS D WHERE numUserCntId=0 AND numDomainId=@sourceDomainId  AND numAuthGroupID=@sourceGroupId

DROP TABLE #tempFormFieldGroupConfigurarionMaster
DROP TABLE #tempDycFieldMaster
DROP TABLE #tempDycUserMaster
END
-----------------------------------------------------------Global Setting Configuration----------------------------------------------------------------------
--ADD Custom Text Field to DycFieldMaster
IF(3 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempTabMaster(
	numOldTabId NUMERIC(18,0),
	numTabId NUMERIC(18,0),
	bitFixed BIT,
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempTabMaster(numOldTabId, numTabId,bitFixed)
SELECT numTabId,0,bitFixed FROM TabMaster WHERE numDomainID=@sourceDomainId OR bitFixed=1

SET @i=1;
DECLARE @numOldTabId AS NUMERIC(18,0)=0
DECLARE @numTabId AS NUMERIC(18,0)=0
DECLARE @bitTabFixed AS BIT=0
SELECT @totalListRecords = COUNT(*) FROM #tempTabMaster

WHILE(@i<=@totalListRecords)
BEGIN
	SET @numTabId=0
	SET @numOldTabId=0
	SELECT @numOldTabId=numOldTabId,@bitTabFixed=bitFixed FROM #tempTabMaster WHERE ID=@i
	PRINT @numOldTabId
	IF(@bitTabFixed=1)
	BEGIN
		PRINT 'FIXED'
		SELECT @numTabId =@numOldTabId
	END
	ELSE IF EXISTS(SELECT numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId)
	BEGIN
		PRINT 'EXIST'
		SELECT @numTabId = numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		PRINT 'NEW'
		INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
		SELECT numTabName, Remarks, tintTabType, vcURL, bitFixed, @destinationDomainId, vcImage, vcAddURL, bitAddIsPopUp FROM TabMaster WHERE numTabId=@numOldTabId
		SET @numTabId = SCOPE_IDENTITY()
	END
	
	UPDATE #tempTabMaster SET numTabId=@numTabId WHERE ID=@i
	IF EXISTS(SELECT  * FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId)
	BEGIN
		IF EXISTS(SELECT  * FROM TabDefault WHERE numTabId=@numTabId AND numDomainId=@destinationDomainId)
		BEGIN
			UPDATE TabDefault SET numTabName=(SELECT  TOP 1 numTabName FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId)  WHERE numTabId=@numTabId AND numDomainId=@destinationDomainId
		END
		ELSE
		BEGIN
			INSERT INTO TabDefault(numTabId, numTabName, tintTabType, numDomainId)
			SELECT  @numTabId,numTabName,tintTabType,@destinationDomainId FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId
		END
	END
	SET @i=@i+1;
END

delete from GroupTabDetails where numGroupID=@destinationGroupId  AND tintType<>1
insert into GroupTabDetails(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
SELECT @destinationGroupId, CASE WHEN (SELECT COUNT(*) FROM #tempTabMaster WHERE numTabId=TB.numTabID) > 0 THEN 
(SELECT TOP 1  numTabID FROM #tempTabMaster WHERE numTabId=TB.numTabID) ELSE TB.numTabID END,
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM 
GroupTabDetails AS TB WHERE numGroupId=@sourceGroupId  AND tintType<>1


delete from GroupTabDetails where numGroupID=@destinationGroupId  AND tintType=1
insert into GroupTabDetails(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
SELECT @destinationGroupId,(SELECT TOP 1 Grp_id FROM [CFw_Grp_Master] CTG WHERE CTG.numDomainID=@destinationDomainId AND CTG.Grp_Name=CFW.Grp_Name AND CTG.Loc_Id=CFW.Loc_Id),
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM 
GroupTabDetails AS TB LEFT JOIN [CFw_Grp_Master] AS CFW ON TB.numTabId=CFW.Grp_id WHERE numGroupId=@sourceGroupId  AND TB.tintType=1

DELETE FROM TreeNavigationAuthorization WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId

INSERT INTO TreeNavigationAuthorization (numGroupID, numTabID, numPageNavID, bitVisible, numDomainID, tintType)
SELECT  @destinationGroupId, 
CASE WHEN (SELECT COUNT(*) FROM #tempTabMaster WHERE numOldTabId=T.numTabID) > 0 THEN (SELECT TOP 1  numTabID FROM #tempTabMaster WHERE numOldTabId=T.numTabID) ELSE T.numTabID END,
numPageNavID, bitVisible, @destinationDomainId, tintType FROM 
TreeNavigationAuthorization AS T WHERE T.numDomainID=@sourceDomainId AND T.numGroupID=@sourceGroupId
DROP TABLE #tempTabMaster



END

--DELETE FROM [AccountTypeDetail] WHERE numDomainID=@destinationDomainId
--INSERT INTO 
--UPDATE Domain SET 
--	numARContactPosition=(SELECT TOP 1 numListID FROM ListDetails WHERE numDomainId=@destinationDomainId AND numListID=41 AND vcData = (SELECT TOP 1 L.vcData FROM Domain AS D LEFT JOIN 
--						 ListDetails AS L ON L.numListItemID=numARContactPosition AND L.numDomainID=@sourceDomainId WHERE D.numDomainId=@sourceDomainId)),
--	bitShowCardConnectLink = (SELECT TOP 1 bitShowCardConnectLink FROM Domain WHERE numDomainId=@sourceDomainId),
--	tintReceivePaymentTo =  (SELECT TOP 1 tintReceivePaymentTo FROM Domain WHERE numDomainId=@sourceDomainId),

--WHERE 
--	numDomainId=@destinationDomainId
COMMIT;  
END TRY 
BEGIN CATCH 
	SELECT ERROR_MESSAGE()
	ROLLBACK TRANSACTION
END CATCH
END
