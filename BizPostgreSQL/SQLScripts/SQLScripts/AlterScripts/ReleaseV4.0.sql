/******************************************************************
Project: Release 4.0 Date: 08.DEC.2014
Comments: 
*******************************************************************/

---------------------------------------------- MANISH -----------------------------

------/******************************************************************
------Project: BACRMUI   Date: 4.DEC.2014
------Comments: Add Item Custom Fields to Opportunity Items Module within custom reports
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[ReportFieldGroupMaster]( [vcFieldGroupName] ,[bitActive] ,[bitCustomFieldGroup] ,[numGroupID] ,[vcCustomTableName])
SELECT 'Items Custom Field',1,	1,	5,	'CFW_FLD_Values_Item'

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
SELECT 10,(SELECT MAX([numReportFieldGroupID]) FROM ReportFieldGroupMaster)

SELECT * FROM ReportModuleGroupFieldMappingMaster 
JOIN ReportFieldGroupMaster ON [ReportModuleGroupFieldMappingMaster].[numReportFieldGroupID] = [ReportFieldGroupMaster].numReportFieldGroupID
WHERE [ReportModuleGroupFieldMappingMaster].[numReportModuleGroupID] IN (10)

ROLLBACK
--=======================================================================


------/******************************************************************
------Project: BACRMUI   Date: 2.DEC.2014
------Comments: Add new columns to E-Commerce Details
------*******************************************************************/

ALTER TABLE [dbo].[eCommerceDTL] ADD bitPreSellUp BIT, bitPostSellUp BIT, dcPostSellDiscount DECIMAL

--=======================================================================
