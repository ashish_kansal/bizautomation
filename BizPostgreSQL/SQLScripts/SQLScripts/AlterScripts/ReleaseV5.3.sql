/******************************************************************
Project: Release 5.3 Date: 13.Jan.2016
Comments: ALTER SCRIPTS
*******************************************************************/

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numListItemID AS INT

INSERT INTO ListDetails 
(
	numListID,
	vcData,
	numCreatedBY,
	bitDelete,
	numDomainID,
	constFlag,
	sintOrder
)
VALUES
(
	27,
	'Pick List',
	1,
	0,
	1,
	1,
	1
)


SELECT @numListItemID = SCOPE_IDENTITY()


DECLARE @TEMP TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID INT
)


INSERT INTO @TEMP (numDomainID) SELECT numDomainID FROM Domain

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT 
DECLARE @numDomainID AS INT

SELECT @COUNT=COUNT(*) FROM @TEMP

WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID = numDomainID FROM  @TEMP WHERE ID = @i

	INSERT INTO BizDocTemplate
	(
		numDomainID,numBizDocID,numOppType,txtBizDocTemplate,txtCSS,bitEnabled,tintTemplateType,vcTemplateName,bitDefault
	)
	VALUES
	(
		@numDomainID,@numListItemID,1,'','',1,0,'Pick List',1
	)

	SET @i = @i + 1
END


UPDATE BizDocTemplate SET txtBizDocTemplate='<table width="100%" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td valign="top" align="left">#Logo#
            </td>
            <td align="right" style="white-space: nowrap;">#BizDocType#
            </td>
        </tr>
    </tbody>
</table>
<table width="100%" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td align="right">
            <table width="100%">
                <tbody>
                    <tr>
                        <td class="RowHeader">Order
                        </td>
                        <td class="RowHeader">Date
                        </td>
                        <td class="RowHeader">Assigned To
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">#OrderID#
                        </td>
                        <td class="normal1">#BizDocCreatedDate#
                        </td>
                        <td class="normal1">#AssigneeName#
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <table width="100%" height="100%">
                <tbody>
                    <tr>
                        <td class="RowHeader">Sold To
                        </td>
                        <td class="RowHeader">Ship To
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">#Customer/VendorOrganizationName#<br />
                        #Customer/VendorBillToAddress#
                        </td>
                        <td class="normal1">#Customer/VendorOrganizationName#<br />
                        #Customer/VendorShipToAddress#
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr class="normal1">
            <td>#Products#
            </td>
        </tr>
    </tbody>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td>#FooterImage#
            </td>
        </tr>
    </tbody>
</table>' WHERE numBizDocID = @numListItemID


UPDATE BizDocTemplate SET txtCSS='.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
.RowHeader.hyperlink{color: #333;}
.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
.AltItemStyle td{padding: 8px;}
.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
.WordWrapSerialNo{width: 30%;word-break: break-all;}' WHERE numBizDocID = @numListItemID
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=====================================

/* Made ContactID Editable in cases form */
UPDATE DycFieldMaster SET vcFieldName='Contact', vcPropertyName='ContactID', vcDbColumnName='numContactID',vcOrigDbColumnName='numContactID', vcFieldDataType = 'N', vcAssociatedControlType = 'SelectBox', vcListItemType='', bitSettingField = 1 WHERE numFieldID=128
UPDATE DycFormField_Mapping SET vcFieldName='Contact', vcPropertyName='ContactID' , bitSettingField=1,vcAssociatedControlType = 'SelectBox' WHERE numFormID=12 AND numFieldID = 128
UPDATE DynamicFormFieldMaster SET vcFieldType='N',vcDbColumnName='numContactID',vcAssociatedControlType = 'SelectBox' WHERE numFormFieldID = 1630

=========================================

/* Made ContactID Editable in Opp/Order Detail form */
UPDATE DycFieldMaster SET vcFieldName='Contact', vcPropertyName='ContactID', vcDbColumnName='numContactID',vcOrigDbColumnName='numContactID', vcFieldDataType = 'N', vcAssociatedControlType = 'SelectBox', vcListItemType='', bitSettingField = 1 WHERE numFieldID=102
UPDATE DycFormField_Mapping SET vcFieldName='Contact', vcPropertyName='ContactID' , bitSettingField=1,vcAssociatedControlType = 'SelectBox' WHERE numFormID IN (38,39,40,41) AND numFieldID = 102
UPDATE DynamicFormFieldMaster SET vcFieldType='N',vcDbColumnName='numContactID',vcAssociatedControlType = 'SelectBox' WHERE numFormFieldID IN (1855,1882,1798,1826)

============================================

/* Made Customer Side Project Manager Editable in Project Detail form */
UPDATE DycFieldMaster SET bitInlineEdit=1,bitAllowEdit=1  WHERE numFieldId=152
UPDATE DycFormField_Mapping SET bitInlineEdit=1,bitAllowEdit=1 WHERE numFormID = 13 AND numFieldID=152

=============================================
GO

/****** Object:  Table [dbo].[CurrencyConversionStatus]    Script Date: 19-Dec-15 11:04:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CurrencyConversionStatus](
	[Id] [int] NOT NULL,
	[dtLastExecuted] [date] NOT NULL,
	[bitSucceed] [bit] NOT NULL,
	[intNoOfTimesTried] [int] NOT NULL,
	[bitFailureNotificationSent] [bit] NULL,
 CONSTRAINT [PK_CurrencyConversion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

======================================

INSERT INTO CurrencyConversionStatus (Id, dtLastExecuted, bitSucceed, intNoOfTimesTried, bitFailureNotificationSent) VALUES (1,'12/18/2015',0,0,0)

=============================================

ALTER TABLE PricingTable ADD
vcName VARCHAR(300)

===================================================

<!-- Add following lines to BizService App.Config -->
<!-- We will check every hour whether currency conversion rate is fetched and if not fetched today then we eill fetch rates from yahoo api -->
--<add key="CurrencyConversionRate" value="60" />
--<add key="CurrencyConversionRateFailed" value="sandeep@bizautomation.com,rajesh170889@gmail.com" />


============================================================

ALTER TABLE TaxDetails ADD
vcTaxUniqueName VARCHAR(300)

ALTER TABLE ItemTax ADD
numTaxID NUMERIC(18,0)

ALTER TABLE OpportunityMasterTaxItems ADD
numTaxID NUMERIC(18,0)

ALTER TABLE OpportunityItemsTaxItems ADD
numTaxID NUMERIC(18,0)
============================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldname,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitAllowFiltering
)
VALUES
(
	3,'CRV','TotalCRVTax','TotalCRVTax','TaxItems','T','R','Label','',0,0,1,0,0,0,1
)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(
	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,[order],bitInResults
)
VALUES
(
	16,'CRV','R','Label','TotalCRVTax','',0,0,'T','TotalCRVTax',0,'TaxItems',0,0,1
)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID
)
VALUES
(
	3,@numFieldID,16,0,0,'CRV','Label',0,1,0,0,1,@numFormFieldID
)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH