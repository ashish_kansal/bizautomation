/******************************************************************
Project: Release 10.6 Date: 11.DEC.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE UserMaster ADD vcEmailAlias VARCHAR(100)
ALTER TABLE UserMaster ADD vcEmailAliasPassword VARCHAR(100)

USE [Production.2014]
GO

/****** Object:  Table [dbo].[UniversalSMTPIMAP]    Script Date: 04-Dec-18 1:17:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UniversalSMTPIMAP](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcSMTPServer] [varchar](100) NOT NULL,
	[numSMTPPort] [numeric](18, 0) NOT NULL,
	[bitSMTPSSL] [bit] NOT NULL,
	[bitSMTPAuth] [bit] NOT NULL,
	[vcIMAPServer] [varchar](100) NOT NULL,
	[numIMAPPort] [numeric](18, 0) NOT NULL,
	[bitIMAPSSL] [bit] NOT NULL,
	[bitIMAPAuth] [bit] NOT NULL,
 CONSTRAINT [PK_UniversalSMTPIMAP] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


INSERT INTO ErrorMaster
(
	vcErrorCode,vcErrorDesc,tintApplication
)
VALUES
(
	'ERR079','We�re unable to generate a rate. Your option is to click the get rate from the check-out page, or contact us.',3
)

--POWERSHELL SCRIPT TO COPY NEW IMAGE ADDED TO BIZCART PROJECT TO ALL CURRENT WEBSITE FOLDER. CHANGE PATH ON PRODUCTION
Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Images'))) { Copy-Item -Path "D:\Biz2016\Vss\BizCart\Default\images\ShippingBox.png" -Destination "$($_.FullName)\Images" }}
Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Js'))) { Copy-Item -Path "D:\wwwroot\Shopping\Js\jquery.guestcheckout.js" -Destination "$($_.FullName)\Js" }}
Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Js'))) { Copy-Item -Path "D:\wwwroot\Shopping\Js\jquery.newonepagecheckout.js" -Destination "$($_.FullName)\Js" }}


/******************************************** PRIYA *********************************************/

------------------------------------Order Based Promotion---------------

ALTER TABLE OpportunityMaster
ADD numDiscountID NUMERIC(18)
-----
USE [Production.2014]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DiscountCodes](
	[numDiscountId] [NUMERIC](18, 0) IDENTITY(1,1) NOT NULL,
	[numPromotionID] [NUMERIC](18, 0) NOT NULL,
	[vcDiscountCode] [VARCHAR](20) NULL,
	[CodeUsageLimit] [INT] NULL	

 CONSTRAINT [PK_DiscountCodes] PRIMARY KEY CLUSTERED 
(
	[numDiscountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

--------------
USE [Production.2014]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DiscountCodeUsage](
	[numDiscountCodeUsageId] [NUMERIC](18, 0) IDENTITY(1,1) NOT NULL,
	[numDiscountId] [NUMERIC](18, 0) NOT NULL,
	[numDivisionId] [NUMERIC](18, 0) NULL,
	[intCodeUsed] [INT] NULL

 CONSTRAINT [PK_DiscountCodeUsage] PRIMARY KEY CLUSTERED 
(
	[numDiscountCodeUsageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO
------------------
USE [Production.2014]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromotionOfferOrder](
	[numProOfferOrderID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numPromotionID] [numeric](18, 0) NOT NULL,
	[numOrderAmount] [float] NULL,
	[fltDiscountValue] [float] NULL,
	[numDomainId] [numeric](18, 0) NULL

 CONSTRAINT [PK_PromotionOfferOrder] PRIMARY KEY CLUSTERED 
(
	[numProOfferOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-----------------------Shipping Promotions ---------------

ALTER TABLE ShippingRules
ADD bitFreeShipping BIT,
	FreeShippingOrderAmt INT,
	numRelationship NUMERIC(18,0),
	numProfile NUMERIC(18,0),
	numWareHouseID VARCHAR(1000)

ALTER TABLE ShippingRuleStateList
ADD vcZipPostal Varchar(10)

UPDATE PageNavigationDTL
SET vcPageNavName = 'Real-Time Shipping Rates & Ship Vias'
WHERE vcPageNavName = 'Shipping Carrier Setup'

UPDATE PageNavigationDTL
SET vcPageNavName = 'Static Shipping Rate Tables & Promotions'
WHERE vcPageNavName = 'Shipping Rules'

--UPDATE PageNavigationDTL
--SET bitVisible = 0
--WHERE vcPageNavName = 'Shipping Promotions'

delete from PageNavigationDTL WHERE vcPageNavName = 'Shipping Promotions'

ALTER TABLE Domain
ADD minShippingCost FLOAT NULL,
	bitEnableStaticShippingRule BIT

	--------------Web Store-----------

	
INSERT INTO PageElementMaster
(
	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
)
VALUES
(
	59,'ShippingPromotion','~/UserControls/ShippingPromotion.ascx','{#ShippingPromotion#}',1,0,0,0
)

INSERT INTO PageElementAttributes
(
	numElementID,vcAttributeName,vcControlType,bitEditor
)
VALUES
(
	59,'Html Customize','HtmlEditor',1
)

------------------------
USE [Production.2014]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShippingExceptions](
	[numShippingExceptionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,	
	[numDomainID] [numeric](18, 0) NULL,
	[numClassificationID] [numeric](18, 0) NULL,
	[PercentAbove] [float] NULL,
	[FlatAmt] [float] NULL
 CONSTRAINT [PK_ShippingExceptions] PRIMARY KEY CLUSTERED 
(
	[numShippingExceptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO