/******************************************************************
Project: Release 5.8 Date: 20.JUNE.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRASANT ****************************/

ALTER TABLE OpportunityItems ADD numSortOrder NUMERIC(18,0)

ALTER TABLE OpportunityItems
ALTER COLUMN numCost decimal(30, 16)


/*************************** SANDEEP ****************************/

ALTER TABLE BankReconcileMaster ADD
vcFileName VARCHAR(300)

===================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[BankReconcileFileData]    Script Date: 18-Jun-16 9:04:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BankReconcileFileData](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numReconcileID] [numeric](18, 0) NOT NULL,
	[dtEntryDate] [date] NOT NULL,
	[vcReference] [varchar](500) NULL,
	[fltAmount] [float] NULL,
	[vcPayee] [varchar](1000) NULL,
	[vcDescription] [varchar](1000) NULL,
	[bitCleared] [bit] NULL,
	[bitReconcile] [bit] NULL CONSTRAINT [DF_BankReconcileFileData_bitReconcile]  DEFAULT ((0)),
	[numTransactionId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BankReconcileFileData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[BankReconcileFileData]  WITH CHECK ADD  CONSTRAINT [FK_BankReconcileFileData_BankReconcileMaster] FOREIGN KEY([numReconcileID])
REFERENCES [dbo].[BankReconcileMaster] ([numReconcileID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BankReconcileFileData] CHECK CONSTRAINT [FK_BankReconcileFileData_BankReconcileMaster]
GO

