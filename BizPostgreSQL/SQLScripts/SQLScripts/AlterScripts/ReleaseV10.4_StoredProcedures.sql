/******************************************************************
Project: Release 10.4 Date: 29.OCT.2018
Comments: STORE PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfAssemblyKitChildItemsIsOnBackorder')
DROP FUNCTION CheckIfAssemblyKitChildItemsIsOnBackorder
GO
CREATE FUNCTION [dbo].[CheckIfAssemblyKitChildItemsIsOnBackorder] 
(
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@numOppKitChildItemID AS NUMERIC(18,0),
	@bitKitParent BIT,
	@bitWorkOrder BIT,
	@numQuantity AS FLOAT
)
RETURNS BIT
AS
BEGIN

    DECLARE @bitBackOrder FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numWOStatus NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID 

	IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) = 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitItems WHERE numOppChildItemID = @numOppChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID
	END
	ELSE IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) > 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitChildItems WHERE numOppKitChildItemID = @numOppKitChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID
	END

	SELECT @numQuantity = @numQuantity - @numQtyShipped
	
	IF @bitWorkOrder = 1
	BEGIN
		IF @numWOStatus = 23184
		BEGIN
			IF @numQuantity > 0
			BEGIN
				IF (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID AND numAllocation >= @numQuantity) > 0
				BEGIN
					SET @bitBackOrder = 0
				END
				ELSE
				BEGIN
					SET @bitBackOrder = 1
				END
			END	
			ELSE 
			BEGIN
				SET @bitBackOrder = 0
			END
		END
		ELSE
		BEGIN
			DECLARE @TEMP TABLE
			(
				ItemLevel INT
				,numParentWOID NUMERIC(18,0)
				,numWOID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numQty FLOAT
				,numQtyRequiredToBuild FLOAT
				,numWarehouseItemID NUMERIC(18,0)
				,bitWorkOrder BIT
				,bitReadyToBuild BIT
			)

			;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemCode,numWarehouseItemID,bitWorkOrder,numQty,numQtyRequiredToBuild) AS
			(
				SELECT
					1,
					numParentWOID,
					numWOId,
					numItemCode,
					numWareHouseItemId,
					1 AS bitWorkOrder,
					@numQuantity,
					CAST(1 AS FLOAT)
				FROM
					WorkOrder
				WHERE
					numOppId=@numOppID 
					AND numOppItemID=@numOppItemID
					AND numOppChildItemID=@numOppChildItemID
					AND ISNULL(numOppKitChildItemID,0)=@numOppKitChildItemID
				UNION ALL
				SELECT 
					c.ItemLevel+2,
					c.numWOID,
					WorkOrder.numWOId,
					WorkOrder.numItemCode,
					WorkOrder.numWareHouseItemId,
					1 AS bitWorkOrder,
					c.numQty * WOD.numQtyItemsReq_Orig,
					WOD.numQtyItemsReq_Orig
				FROM 
					WorkOrder
				INNER JOIN 
					CTEWorkOrder c 
				ON 
					WorkOrder.numParentWOID = c.numWOID
				INNER JOIN
					WorkOrderDetails WOD
				ON
					WOD.numWOId = c.numWOID
					AND WOD.numChildItemID = WorkOrder.numItemCode
					AND ISNULL(WOD.numWareHouseItemId,0) = ISNULL(WorkOrder.numWareHouseItemId,0)
			)

			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numQtyRequiredToBuild
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				ItemLevel,
				numParentWOID,
				numWOID,
				numItemCode,
				numQty,
				numQtyRequiredToBuild,
				CTEWorkOrder.numWarehouseItemID,
				bitWorkOrder,
				0
			FROM 
				CTEWorkOrder
		
			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				t1.ItemLevel + 1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				t1.numQty * WorkOrderDetails.numQtyItemsReq_Orig,
				WorkOrderDetails.numWarehouseItemID,
				0 AS bitWorkOrder,
				CASE 
					WHEN Item.charItemType='P'
					THEN
						CASE 
							WHEN @tintCommitAllocation=2 
							THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
							ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
						END
					ELSE 1
				END
			FROM
				WorkOrderDetails
			INNER JOIN
				@TEMP t1
			ON
				WorkOrderDetails.numWOId = t1.numWOID
			INNER JOIN 
				Item
			ON
				WorkOrderDetails.numChildItemID = Item.numItemCode
				AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
			LEFT JOIN
				WareHouseItems
			ON
				WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
			LEFT JOIN
				Warehouses
			ON
				WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
			LEFT JOIN
				OpportunityMaster 
			ON
				WorkOrderDetails.numPOID = OpportunityMaster.numOppId

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempWOID NUMERIC(18,0)

			DECLARE @TEMPWO TABLE
			(
				ID INT IDENTITY(1,1)
				,numWOID NUMERIC(18,0)
			)
			INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

			SELECT @iCount=COUNT(*) FROM @TEMPWO

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

				UPDATE
					T1
				SET
					bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
				FROM
					@TEMP T1
				WHERE
					numWOID=@numTempWOID

				SET @i = @i + 1
			END

			IF (SELECT COUNT(*) FROM @TEMP WHERE ISNULL(bitReadyToBuild,0)=0) > 0
			BEGIN
				SET @bitBackOrder = 1
			END
			ELSE
			BEGIN
				SET @bitBackOrder = 0
			END
		END
	END
	ELSE IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			numOppKitChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitAssembly BIT,
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numPrentItemBackOrder INT
		)

		IF ISNULL(@numOppChildItemID,0) = 0
		BEGIN
			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				0,
				I.vcItemName,
				I.bitAssembly,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * @numQuantity,
				OKI.numQtyItemsReq_Orig,
				ISNULL(WI.numOnHand,0),
				ISNULL(WI.numAllocation,0),
				0
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OI.numoppitemtCode = @numOppItemID
				AND ISNULL(I.bitKitParent,0) = 0
		END

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			I.vcItemName,
			I.bitAssembly,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
			AND ISNULL(I.bitKitParent,0) = 0
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF EXISTS (
					SELECT
						numItemCode
					FROM
						@TEMPTABLE
					WHERE
						1 = CASE 
								WHEN ISNULL(bitAssembly,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,0,1,numQtyItemsReq)
								WHEN ISNULL(bitKitParent,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,1,0,numQtyItemsReq)
								ELSE
									CASE 
										WHEN @tintCommitAllocation=2 
										THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= numOnHand THEN 0 ELSE 1 END)
										ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= numAllocation THEN 0 ELSE 1 END) 
									END
							END
					)
		BEGIN
			SET @bitBackOrder = 1
		END
		ELSE
		BEGIN
			SET @bitBackOrder = 0
		END
	END
	ELSE
	BEGIN
		SELECT
			@numOnHand = ISNULL(numOnHand,0)
			,@numAllocation = ISNULL(numAllocation,0)
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF @tintCommitAllocation=2 
		BEGIn
			IF (@numQuantity - @numQtyShipped) > @numOnHand
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
		ELSE
		BEGIN
			IF (@numQuantity - @numQtyShipped) > @numAllocation
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
	END

	RETURN @bitBackOrder

END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderItemInventoryStatus')
DROP FUNCTION CheckOrderItemInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderItemInventoryStatus] 
(
	@numItemCode NUMERIC(18,0) = NULL,
	@numQuantity AS FLOAT = 0,
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT
)
RETURNS FLOAT
AS
BEGIN
    DECLARE @bitBackOrder FLOAT = 0
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @tintCommitAllocation TINYINT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Item INNER JOIN Domain ON Item.numDomainID=Domain.numDomainId WHERE numItemCode=@numItemCode
	SELECT @numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numAllocation FLOAT,
			numOnHand FLOAT,
			numPrentItemBackOrder INT,
			numWOID NUMERIC(18,0)
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			ISNULL(WI.numOnHand,0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKi.numOppId AND numOppItemID=OKi.numOppItemID AND numOppChildItemID=OKI.numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			ISNULL(WI.numOnHand,0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKCI.numOppId AND numOppItemID=OKCI.numOppItemID AND numOppChildItemID=OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		UPDATE 
			@TEMPTABLE 
		SET 
			numPrentItemBackOrder = (CASE
										WHEN ISNULL(numWOID,0) > 0
										THEN
											(CASE 
												WHEN dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq) >= numQtyItemsReq
												THEN 0 
												ELSE
													(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/numQtyItemReq_Orig AS INT) ELSE 0 END)
											END)
										ELSE
											(CASE 
												WHEN @tintCommitAllocation = 2 
												THEN
													(CASE 
														WHEN numOnHand >= numQtyItemsReq
														THEN 0 
														ELSE
															(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numOnHand/numQtyItemReq_Orig AS INT) ELSE 0 END)
													END)
												ELSE
													(CASE 
														WHEN numAllocation >= numQtyItemsReq
														THEN 0 
														ELSE
															(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numAllocation/numQtyItemReq_Orig AS INT) ELSE 0 END)
													END)
											END)
									END)
		WHERE
			bitKitParent = 0

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numPrentItemBackOrder = (CASE WHEN t2.numQtyItemReq_Orig > 0 THEN CEILING((SELECT MAX(numPrentItemBackOrder) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig) ELSE 0 END)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SELECT @bitBackOrder = MAX(numPrentItemBackOrder) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = numAllocation
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF (@numQuantity - @numQtyShipped) > @numAllocation
            SET @bitBackOrder = (@numQuantity - @numQtyShipped) - @numAllocation
		ELSE
			SET @bitBackOrder = 0
	END

	RETURN @bitBackOrder

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_DemandForecastDaysDisplay')
DROP FUNCTION fn_DemandForecastDaysDisplay
GO
CREATE FUNCTION [dbo].[fn_DemandForecastDaysDisplay]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@dtReleaseDate DATE
	,@numForecastDays NUMERIC(18,0)
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @fltDemandBasedOnReleaseDate FLOAT = 0
	DECLARE @fltDemandBasedOnHistoricalSales FLOAT = 0
	DECLARE @fltDemandBasedOnOpportunity FLOAT = 0
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @numAnalysisDays INT = 0

	SET @fltDemandBasedOnReleaseDate = ISNULL((SELECT
												SUM(numUnitHour - ISNULL(numQtyShipped,0))
											FROM
												OpportunityItems OI
											INNER JOIN
												OpportunityMaster OM
											ON
												OI.numOppId=OM.numOppId
											WHERE
												OM.numDomainId=@numDomainID
												AND OM.tintOppType=1
												AND OM.tintOppStatus=1
												AND ISNULL(OM.tintshipped,0)=0
												AND OI.numItemCode=@numItemCode
												AND OI.numWarehouseItmsID=@numWarehouseItemID
												AND ISNULL(bitDropship,0) = 0
												AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
												AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
												AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)
	

	-- KIT CHILD ITEMS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0))
												FROM
													OpportunityKitItems OKI
												INNER JOIN
													OpportunityItems OI
												ON
													OKI.numOppItemID=OI.numoppitemtCode
												INNER JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKI.numChildItemID=@numItemCode
													AND OKI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	-- KIT CHILD ITEMS OF CHILD KITS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0))
												FROM
													OpportunityKitChildItems OKCI
												INNER JOIN
													OpportunityItems OI
												ON
													OKCI.numOppItemID=OI.numoppitemtCode
												INNER JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKCI.numItemID=@numItemCode
													AND OKCI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	--ITEMS USED IN WORK ORDER
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(WOD.numQtyItemsReq)
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrder WO
												ON
													WOD.numWOId=WO.numWOId
												LEFT JOIN
													OpportunityItems OI
												ON
													WO.numOppItemID = OI.numoppitemtCode
												LEFT JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													WO.numDomainID=@numDomainID
													AND WO.numWOStatus <> 23184 -- NOT COMPLETED
													AND WOD.numChildItemID = @numItemCode
													AND WOD.numWareHouseItemId=@numWarehouseItemID
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
													AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
													AND CAST((CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END) AS DATE) = @dtReleaseDate
													AND 1 = (CASE 
															WHEN OI.numoppitemtCode IS NOT NULL 
															THEN (CASE 
																	WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																	THEN 1 
																	ELSE 0 
																END)
															ELSE 
																(CASE 
																WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																THEN 1 
																ELSE 0 
																END)
															END)),0)


	IF ISNULL(@bitShowHistoricSales,0) = 1
	BEGIN
		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numHistoricalAnalysisPattern

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		SET @fltDemandBasedOnHistoricalSales = ISNULL((SELECT
															SUM(numUnitHour)
														FROM
															OpportunityItems OI
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND numItemCode=@numItemCode
															AND numWarehouseItmsID=@numWarehouseItemID
															AND ISNULL(bitDropship,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKI.numQtyItemsReq)
														FROM
															OpportunityKitItems OKI
														INNER JOIN
															OpportunityItems OI
														ON
															OKI.numOppItemID=OI.numoppitemtCode
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKI.numChildItemID=@numItemCode
															AND OKI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKCI.numQtyItemsReq)
														FROM
															OpportunityKitChildItems OKCI
														INNER JOIN
															OpportunityItems OI
														ON
															OKCI.numOppItemID=OI.numoppitemtCode
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKCI.numItemID=@numItemCode
															AND OKCI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(WOD.numQtyItemsReq)
														FROM
															WorkOrderDetails WOD
														INNER JOIN
															WorkOrder WO
														ON
															WOD.numWOId=WO.numWOId
														LEFT JOIN
															OpportunityItems OI
														ON
															WO.numOppItemID = OI.numoppitemtCode
														LEFT JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															WO.numDomainID=@numDomainID
															AND WOD.numChildItemID = @numItemCode
															AND WOD.numWareHouseItemId=@numWarehouseItemID
															AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		SET @fltDemandBasedOnHistoricalSales = (@fltDemandBasedOnHistoricalSales/@numAnalysisDays) * @numForecastDays
	END

	IF ISNULL(@bitIncludeOpportunity,0) = 1
	BEGIN
		SET @fltDemandBasedOnOpportunity = ISNULL((SELECT
														SUM(OI.numUnitHour * (PP.intTotalProgress/100))
													FROM
														OpportunityItems OI
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND numItemCode=@numItemCode
														AND numWarehouseItmsID=@numWarehouseItemID
														AND ISNULL(bitDropship,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >=  @numOpportunityPercentComplete
												),0)

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityKitItems OKI
													INNER JOIN
														OpportunityItems OI
													ON
														OKI.numOppItemID=OI.numoppitemtCode
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKI.numChildItemID=@numItemCode
														AND OKI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKCI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityKitChildItems OKCI
													INNER JOIN
														OpportunityItems OI
													ON
														OKCI.numOppItemID=OI.numoppitemtCode
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKCI.numItemID=@numItemCode
														AND OKCI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(WOD.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														WorkOrder WO
													ON
														OI.numoppitemtCode = WO.numOppItemID
													INNER JOIN
														WorkOrderDetails WOD
													ON
														WO.numWOId=WOD.numWOId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND WO.numDomainID=@numDomainID
														AND WO.numWOStatus <> 23184 -- NOT COMPLETED
														AND WOD.numChildItemID = @numItemCode
														AND WOD.numWareHouseItemId=@numWarehouseItemID
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
	END


	RETURN CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0,''',@dtReleaseDate,''')"><span class="badge bg-light-blue">',@fltDemandBasedOnReleaseDate,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0,''',@dtReleaseDate,''')"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,',''',@dtReleaseDate,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
	)

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0)
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO 
			@TEMPSelectedKitChilds
		SELECT 
			OKI.numChildItemID,
			OKI.numWareHouseItemId,
			OKCI.numItemID,
			OKCI.numWareHouseItemId
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKI.numOppChildItemID = OKCI.numOppChildItemID
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
	END
	ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
	BEGIN
		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
		)
		SELECT 
			SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
			SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))
		FROM 
			dbo.SplitString(@vcSelectedKitChildItems,',')

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)
	END


	;WITH CTE (numItemCode, vcItemName, bitKitParent, numQtyItemsReq,numUOMId) AS
	(
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(numUOMId,0)
		FROM 
			[ItemDetails] ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE   
			[numItemKitID] = @numItemCode
		UNION ALL
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(ID.numUOMId,0)
		FROM 
			CTE As Temp1
		INNER JOIN
			[ItemDetails] ID
		ON
			ID.numItemKitID = Temp1.numItemCode
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE
			Temp1.bitKitParent = 1
			AND (LEN(ISNULL(@vcSelectedKitChildItems,'')) = 0 OR EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID))
	)

	INSERT INTO @TEMPitems
	(
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	)
	SELECT
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	FROM
		CTE

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		VALUES
		(
			0
			,0
		)
	END
	ELSE
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			1
			,SUM(CalculatedPrice)
		FROM
		(
			SELECT  
				ISNULL(CASE 
						WHEN I.[charItemType]='P' 
						THEN 
							CASE 
							WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
							THEN
								ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
							ELSE
								(CASE @tintKitAssemblyPriceBasedOn 
										WHEN 2 THEN ISNULL(I.monAverageCost,0) 
										WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
										ELSE WI.[monWListPrice]
								END) 
							END
						ELSE (CASE @tintKitAssemblyPriceBasedOn WHEN 2 THEN ISNULL(I.monAverageCost,0) ELSE I.[monListPrice] END)
					END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
			FROM 
				@TEMPitems ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.numItemCode = I.[numItemCode]
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode = V.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1 
					*
				FROM
					WareHouseItems WI
				WHERE 
					WI.numItemID=I.numItemCode 
					AND WI.numWareHouseID = @numWarehouseID
				ORDER BY
					WI.numWareHouseItemID
			) AS WI
			WHERE
				ISNULL(ID.bitKitParent,0) = 0
		) T1
	END

	RETURN
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0),
	  @tintCommitAllocation TINYINT,
	  @bitFromBizDoc NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @bitAssembly BIT
	DECLARE @bitKitParent BIT
	DECLARE @bitWorkOrder BIT

	SELECT 
		@bitAssembly=ISNULL(bitAssembly,0)
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitWorkOrder=ISNULL(bitWorkOrder,0) 
	FROM 
		OpportunityItems 
	INNER JOIN 
		Item 
	ON 
		OpportunityItems.numItemCode=Item.numItemCode 
	WHERE 
		numOppId=@numOppID AND numoppitemtCode=@numOppItemID

	IF @bitKitParent = 1
	BEGIN
		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,bitDropship,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKI.numOppItemID,
			OI.bitDropShip,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1,
			(CASE 
				WHEN OKI.numWareHouseItemId > 0 AND ISNULL(OI.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,0,1,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,1,0,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId=OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		LEFT JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
		WHERE
			OM.numOppId = @numOppID
			AND OI.numoppitemtCode=@numOppItemID

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2,
			(CASE 
				WHEN OKCI.numWareHouseItemId > 0 AND ISNULL(c.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,0,1,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,1,0,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityKitChildItems OKCI
		LEFT JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppID
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID


		INSERT INTO @TEMPTABLEFINAL
		(
			numOppItemCode,
			bitDropship,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		)
		SELECT
			numOppItemCode,
			bitDropship,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		FROM 
			@TEMPTABLE 
		WHERE 
		tintLevel=1
		
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))

			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
			BEGIN
				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,' (')

				SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails + ', ','') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)

				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,' )')
			END

			SET @i = @i + 1
		END
	END
	ELSE IF @bitAssembly=1 AND @bitWorkOrder=1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPFINAL TABLE
		(
			ID INT IDENTITY(1,1)
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		SET @i  = 1

		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END



		INSERT INTO @TEMPFINAL
		(
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		)
		SELECT
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		FROM 
			@TEMP
		WHERE 
			ItemLevel=2
		
		SET @i = 1

		SELECT @iCount = COUNT(*) FROM @TEMPFINAL

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT
																																		(CASE 
																																			WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
																																			THEN 
																																				CONCAT(I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
																																			ELSE
																																				CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
																																		END)
																																	FROM
																																		@TEMPFINAL T1
																																	INNER JOIN
																																		Item I
																																	ON
																																		T1.numItemCode = I.numItemCode
																																	LEFT JOIN
																																		WareHouseItems WI
																																	ON
																																		T1.numWarehouseItemID = WI.numWareHouseItemID
																																	LEFT JOIN
																																		UOM
																																	ON
																																		UOM.numUOMId = (CASE WHEN ISNULL(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
																																	WHERE 
																																		ID = @i))

			SET @i = @i + 1
		END
	END
	

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemAvailableQtyToShip')
DROP FUNCTION GetOrderItemAvailableQtyToShip
GO
CREATE FUNCTION [dbo].[GetOrderItemAvailableQtyToShip] 
(	
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT,
	@tintCommitAllocation TINYINT,
	@bitAllocateInventoryOnPickList BIT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @numQuantity AS FLOAT = 0
    DECLARE @numAvailableQtyToShip FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0

	SELECT @numQuantity=ISNULL(numUnitHour,0),@numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numAvailableQtyToShip INT,
			numWOID NUMERIC(18,0)
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKi.numOppId AND numOppItemID=OKi.numOppItemID AND numOppChildItemID=OKI.numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKCI.numOppId AND numOppItemID=OKCI.numOppItemID AND numOppChildItemID=OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/numQtyItemReq_Orig)
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  numOnHand>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
													ELSE FLOOR(ISNULL(numOnHand,0)/numQtyItemReq_Orig)
												END)
										END)
			WHERE
				bitKitParent = 0
		END
		ELSE
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/numQtyItemReq_Orig)
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numAllocation,0) = 0 THEN 0
													WHEN  numAllocation>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
													ELSE FLOOR(ISNULL(numAllocation,0)/numQtyItemReq_Orig)
												END)
										END)
			WHERE
				bitKitParent = 0
		END

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numAvailableQtyToShip = CEILING((SELECT MAX(numAvailableQtyToShip) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SELECT @numAvailableQtyToShip = MIN(numAvailableQtyToShip) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
	END
	ELSE
	BEGIN
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			SELECT
				@numOnHand = numOnHand
			FROM
				WareHouseItems 
			WHERE
				numWareHouseItemID = @numWarehouseItemID

			IF (@numQuantity - @numQtyShipped) >= @numOnHand
				SET @numAvailableQtyToShip = @numOnHand
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
		ELSE
		BEGIN
			SELECT
				@numAllocation = numAllocation
			FROM
				WareHouseItems 
			WHERE
				numWareHouseItemID = @numWarehouseItemID

			IF (@numQuantity - @numQtyShipped) >= @numAllocation
				SET @numAvailableQtyToShip = @numAllocation
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
	END

	RETURN @numAvailableQtyToShip
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetStoredCustFldValueOppItems')
DROP FUNCTION GetStoredCustFldValueOppItems
GO
CREATE FUNCTION GetStoredCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
       
	IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_OppItems
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numRecordId
	END
	ELSE
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_Item 
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numItemCode
	END

	RETURN @vcValue
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderQtyReadyToBuild')
DROP FUNCTION GetWorkOrderQtyReadyToBuild
GO
CREATE FUNCTION [dbo].[GetWorkOrderQtyReadyToBuild] 
(
	@numWOID NUMERIC(18,0)
	,@numQty FLOAT
)
RETURNS FLOAT
AS 
BEGIN
	DECLARE @fltQtyReadyToBuild FLOAT = @numQty

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numWOId = @numWOID AND numWOStatus <> 23184)
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numOppChildItemID NUMERIC(18,0)
			,numOppKitChildItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,vcItemName VARCHAR(MAX)
			,numQty FLOAT
			,numQtyReq_Orig FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
			,numQtyReadyToBuild FLOAT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numOppID,numOppItemID,numOppChildItemID,numOppKitChildItemID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus,numQtyReadyToBuild) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numOppKitChildItemID,
				numItemCode,
				@numQty,
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM
				WorkOrder
			WHERE
				numWOId = @numWOID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numOppID,
				WorkOrder.numOppItemID,
				WorkOrder.numOppChildItemID,
				WorkOrder.numOppKitChildItemID,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(WorkOrder.numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			numItemCode,
			vcItemName,
			CTEWorkOrder.numQtyItemsReq,
			0,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus,
			0
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		UPDATE
			T1
		SET
			numQtyReq_Orig = (SELECT WorkOrderDetails.numQtyItemsReq_Orig FROM WorkOrderDetails INNER JOIN WorkOrder ON WorkOrderDetails.numWOId=WorkOrder.numWOId WHERE WorkOrder.numWOId = T1.numParentWOID AND WorkOrderDetails.numChildItemID=T1.numItemCode AND WorkOrderDetails.numWareHouseItemId=T1.numWarehouseItemID AND WorkOrder.numOppId=T1.numOppID AND WorkOrder.numOppItemID=T1.numOppItemID AND WorkOrder.numOppChildItemID=T1.numOppChildItemID AND WorkOrder.numOppKitChildItemID=T1.numOppKitChildItemID)
		FROM
			@TEMP T1
		WHERE	
			numParentWOID > 0

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			WorkOrderDetails.numChildItemID,
			Item.vcItemName,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numQtyItemsReq_Orig,
			WorkOrderDetails.numWarehouseItemID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END),
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN WorkOrderDetails.numQtyItemsReq
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numOnHand THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numOnHand,0)/numQtyItemsReq_Orig AS INT) END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numAllocation THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numAllocation,0)/numQtyItemsReq_Orig AS INT) END) 
							END
						ELSE (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig)
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				numQtyReadyToBuild = (CASE WHEN ISNULL(numQty,0) <= (SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID) THEN (numQty/numQtyReq_Orig) ELSE CAST(ISNULL((SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID),0)/numQtyReq_Orig AS INT) END) 
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND numParentWOID > 0

			SET @i = @i + 1
		END

		SET @fltQtyReadyToBuild = (SELECT MIN(numQtyReadyToBuild) FROM @TEMP WHERE numParentWOID=@numWOID)
	END

	RETURN @fltQtyReadyToBuild
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderStatus')
DROP FUNCTION GetWorkOrderStatus
GO
CREATE FUNCTION [dbo].[GetWorkOrderStatus] 
(
	@numDomainID NUMERIC(18,0)
    ,@tintCommitAllocation TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcWoStatus VARCHAR(500) = ''

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID) > 0
	BEGIN
		SET @vcWoStatus = CONCAT('<a href="#" onClick="return OpenWorkOrder(',@numOppID,',',@numOppItemID,');"><img src="../images/workorderstatus.png" /></a> ')

		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numDomainID=@numDomainID
				AND numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END

		IF (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1 AND tintBuildStatus=2) = (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1)
		BEGIN
			SET @vcWoStatus = CONCAT(@vcWoStatus,'Build completed')
		END
		ELSE IF (SELECT COUNT(*) FROM @TEMP WHERE tintBuildStatus=0) > 0
		BEGIN
			SET @vcWoStatus = CONCAT(@vcWoStatus,'Not ready to build')
		END
		ELSE
		BEGIN
			SET @vcWoStatus = CONCAT(@vcWoStatus,'Ready to build')
		END
	END

	RETURN @vcWoStatus
END

GO
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as DECIMAL(20,5) =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT = 0	
	DECLARE @bitSerialized AS BIT  = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup AS NUMERIC(18,0) =0
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @bitRemoveGlobalWarehouse BIT

	SELECT @bitRemoveGlobalWarehouse=ISNULL(bitRemoveGlobalLocation,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@numItemGroup = ISNULL(@numItemGroup,0),
		@vcSKU = ISNULL(vcSKU,0),
		@vcUPC = ISNULL(numBarCodeId,0),
		@monListPrice = ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID)
				BEGIN
					RAISERROR('WAREHOUSE_DETAIL_NOT_FOUND',16,1)
				END
				
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode) AND NOT EXISTS(SELECT numChildItemID FROM dbo.OpportunityKitItems WHERE numChildItemID = @numItemCode) AND NOT EXISTS(SELECT numItemID FROM dbo.OpportunityKitChildItems WHERE numItemID = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
				BEGIN
					RAISERROR('INVALID_ITEM_CODE',16,1)
				END

				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = SCOPE_IDENTITY()


				IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0 AND @bitRemoveGlobalWarehouse = 0
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						-1,
						0,
						0,
						@monWListPrice,
						'',
						@vcWHSKU,
						@vcBarCode,
						@numDomainID,
						GETDATE()
					)  

					SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()
				END



				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		DECLARE @bitOppOrderExists AS BIT = 0

		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		IF @bitMatrix = 1 AND ISNULL(@bitOppOrderExists,0) <> 1 -- ITEM LEVEL ATTRIBUTES
		BEGIN
			IF ISNULL(@numWareHouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numWareHouseItemID
			
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numWareHouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END

			IF ISNULL(@numGlobalWarehouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numGlobalWarehouseItemID
				
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END   
		END
		ELSE IF DATALENGTH(@strFieldList) > 2 AND ISNULL(@bitOppOrderExists,0) <> 1
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

				declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
				Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
				insert into #tempTable (Fld_ID,Fld_Value)                                        
				SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
				WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
				delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C inner join #tempTable T on C.Fld_ID=T.Fld_ID where C.RecId=@numWareHouseItemID                              
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,Fld_Value,RecId,bitSerialized
				) 

				select Fld_ID,isnull(Fld_Value,'') as Fld_Value,@numWareHouseItemID,0 from #tempTable 

				drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 

		IF ISNULL(@numGlobalWarehouseItemID,0) > 0
		BEGIN        
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numGlobalWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = @dtAdjustmentDate,
			@numDomainID = @numDomainID 
		END

		-- KEEP IT LAST IN SECTION
		IF ISNULL(@numItemGroup,0) > 0 OR ISNULL(@bitMatrix,0) = 1 -- IF IT's MATRIX ITEM THEN 
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = (CASE WHEN @monListPrice > 0 THEN @monListPrice ELSE monWListPrice END), vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
		END
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numChildItemID,OKI.numWarehouseItemID,OKI.numQtyItemsReq,OKI.numQtyItemsReq,OKI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitItems OKI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numCHildItemID=I.numItemCode
	WHERE 
		OKI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID  

	INSERT INTO OpportunityKitChildItems                                                                          
	(
		numOppId,numOppItemID,numOppChildItemID,numItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numOppChildItemID,OKCI.numItemID,OKCI.numWarehouseItemID,OKCI.numQtyItemsReq,OKCI.numQtyItemsReq,OKCI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitChildItems OKCI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKCI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		OpportuntiyKitItems OKI
	ON
		OKI.numOppItemID=OI.numOppItemtCOde
	INNER JOIN
		Item I
	ON
		OKCI.numItemID=I.numItemCode
	WHERE 
		OKCI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID 
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numBillingContact,
		@bitAltContact=bitAltBillingContact,
		@vcAltContact=vcAltBillingContact
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numShippingContact,
		@bitAltContact=bitAltShippingContact,
		@vcAltContact=vcAltShippingContact
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN  
	INSERT INTO dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId 
		AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivisionID	
	UNION 
	SELECT
		@numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numNewOppID,1,NULL)	
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Contacts_CT')
DROP PROCEDURE USP_Contacts_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23rdJuly2014>
-- Description:	<Description,, to Track Row changes in Contacts Table>
-- =============================================
Create PROCEDURE [dbo].[USP_Contacts_CT]
	-- Add the parameters for the stored procedure here
-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 -- Insert statements for procedure here
	DECLARE @tblFields TABLE 
	(
		FieldName VARCHAR(200),
		FieldValue VARCHAR(200)
	)

	DECLARE @Type char(1)
	DECLARE @ChangeVersion bigint
	DECLARE @CreationVersion bigint
	DECLARE @last_synchronization_version bigInt=null
	DECLARE @Columns_Updated VARCHAR(1000)

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version
		,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, 0) AS CT
	WHERE 
		numContactId=@numRecordID


	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, 0) AS CT
		WHERE  
			CT.numContactId=@numRecordID
	END
	ELSE
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     
		
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, @last_synchronization_version) AS CT
		WHERE  
			CT.numContactId=@numRecordID
	
		--Fields Up date & for Type :Insert or Update:
		Declare @UFFields as table 
		(
			Address varchar(70),
			numAge varchar(70),
			vcAsstEmail varchar(70),
			vcAsstFirstName varchar(70),
			vcAsstLastName varchar(70),
			numAsstPhone varchar(70),
			vcAltEmail varchar(70),
			vcCategory varchar(70),
			numCell varchar(70),
			vcEmail varchar(70),
			numPhone varchar(70),
			vcPosition varchar(70),
			bitPrimaryContact varchar(70),
			bitOptOut varchar(70),
			numManagerID varchar(70),
			vcFirstName varchar(70),
			charSex varchar(70),
			vcLastName varchar(70),
			bintDOB varchar(70),
			numContactType varchar(70),
			numTeam varchar(70),
			txtNotes varchar(70),
			bintCreatedDate varchar(70)
		)

		INSERT into @UFFields(Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'Address', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS Address ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numAge', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAge, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstEmail, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstFirstName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstFirstName, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstLastName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstLastName	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numAsstPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numAsstPhone ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAltEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAltEmail, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcCategory', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcCategory,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numCell', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCell,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcEmail,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPhone,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcPosition', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPosition,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bitPrimaryContact', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPrimaryContact,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bitOptOut', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitOptOut,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numManagerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numManagerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcFirstName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcFirstName,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'charSex', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS charSex,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcLastName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcLastName,

		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bintDOB', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintDOB,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numContactType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numContactType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numTeam', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTeam,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'txtNotes', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtNotes,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate
		FROM 
		CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, @last_synchronization_version) AS CT
		WHERE 
		ct.numContactId=@numRecordID
    --sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,69
		INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate)
		) AS upv
		where FieldValue<>0
 
		 SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
		 SET @Columns_Updated=ISNULL(@Columns_Updated,'')
		 select @Columns_Updated
	END

	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END
	
--DateField WorkFlow Logic
	
	Declare @TempnumContactId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation_TempDateFields WHERE numContactId=@numRecordID)
		BEGIN
			SET @TempnumContactId = (SELECT ACI.numContactId FROM [AdditionalContactsInformation] ACI WHERE  (			
				(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND ACI.numContactId=@numRecordID)

			IF(@TempnumContactId IS NOT NULL)
			BEGIN	
				UPDATE ACIT 
				SET 
						ACIT.bintCreatedDate = ACI.bintCreatedDate
						,ACIT.bintDOB = ACI.bintDOB
					
				FROM
					[AdditionalContactsInformation_TempDateFields] AS ACIT
					INNER JOIN [AdditionalContactsInformation] AS ACI
						ON ACIT.numContactId = ACI.numContactId
				WHERE  (
						(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR
						(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
					  )			
					AND 
						ACIT.numContactId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
			END	
		END
		ELSE
		BEGIN
		INSERT INTO [AdditionalContactsInformation_TempDateFields] (numContactId, numDomainID, bintCreatedDate, bintDOB) 
		SELECT numContactId,numDomainID, bintCreatedDate,bintDOB	
		FROM AdditionalContactsInformation
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		      )	
			AND numContactId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumContactId = (SELECT ACI.numContactId FROM [AdditionalContactsInformation] ACI WHERE  (			
				(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND ACI.numContactId=@numRecordID)

		IF(@TempnumContactId IS NOT NULL)
		BEGIN	
			UPDATE ACIT 
			SET 
					ACIT.bintCreatedDate = ACI.bintCreatedDate
					,ACIT.bintDOB = ACI.bintDOB
					
			FROM
				[AdditionalContactsInformation_TempDateFields] AS ACIT
				INNER JOIN [AdditionalContactsInformation] AS ACI
					ON ACIT.numContactId = ACI.numContactId
			WHERE  (
					(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND 
					ACIT.numContactId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
	END	

	--DateField WorkFlow Logic

	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 69, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @bitMatrix AS NUMERIC(18,0)
	DECLARE @vcItemName AS VARCHAR(300)

	SELECT @numItemGroup=numItemGroup,@bitMatrix=bitMatrix,@vcItemName=vcItemName FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = SCOPE_IDENTITY()


	IF ISNULL(@bitMatrix,0) = 1
	BEGIN
		DECLARE @vcProductVariation VARCHAR(300)

		WITH data (ProductId, ProductOptionGroupId, ProductOptionId) AS 
		(
			SELECT
				@numItemCode
				,CFW_Fld_Master.Fld_id
				,ListDetails.numListItemID
			FROM
				ItemGroupsDTL
			INNER JOIN
				CFW_Fld_Master
			ON
				ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
			INNER JOIN
				ListDetails
			ON
				CFW_Fld_Master.numlistid = ListDetails.numListID
			WHERE
				numItemGroupID = @numItemGroup
		),
		ranked AS (
		/* ranking the group IDs */
			SELECT
				ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				GroupRank = DENSE_RANK() OVER (PARTITION BY ProductId ORDER BY ProductOptionGroupId)
			FROM 
				data
		),
		crossjoined AS (
			/* obtaining all possible combinations */
			SELECT
				ProductId,
				ProductOptionGroupId,
				GroupRank,
				ProductVariant = CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS varchar(250))
			FROM 
				ranked
			WHERE 
				GroupRank = 1
			UNION ALL
			SELECT
				r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				ProductVariant = CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250))
			FROM 
				ranked r
			INNER JOIN 
				crossjoined c 
			ON 
				r.ProductId = c.ProductId
			AND 
				r.GroupRank = c.GroupRank + 1
		  ),
		  maxranks AS (
			/* getting the maximum group rank value for every product */
			SELECT
				ProductId,
				MaxRank = MAX(GroupRank)
			FROM 
				ranked
			GROUP BY 
				ProductId
		  )
		
		SELECT TOP 1  
			@vcProductVariation = c.ProductVariant 
		FROM 
			crossjoined c 
		INNER JOIN 
			maxranks m 
		ON 
			c.ProductId = m.ProductId
			AND c.GroupRank = m.MaxRank
			AND c.ProductVariant NOT IN (SELECT 
											STUFF((SELECT ',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=IR.numItemCode FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,1,'') AS NameValues
										FROM 
											Item
										INNER JOIN
											ItemAttributes IR
										ON
											IR.numDomainID=@numDomainID
											AND Item.numItemCode =IR.numItemCode
										WHERE 
											Item.numDomainID=@numDomainID
											AND bitMatrix = 1
											AND numItemGroup = @numItemGroup
											AND vcItemName = @vcItemName
										GROUP BY
											IR.numItemCode)

		IF LEN(ISNULL(@vcProductVariation,'')) = 0
		BEGIN
			RAISERROR('ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ItemAttributes
			(
				numDomainID
				,numItemCode
				,FLD_ID
				,FLD_Value
			)
			SELECT
				@numDomainID
				,@numNewItemCode
				,SUBSTRING(OutParam,0,CHARINDEX(':',OutParam,0))
				,SUBSTRING(OutParam,CHARINDEX(':',OutParam,0) + 1,LEN(OutParam))
			FROM
				dbo.SplitString(@vcProductVariation,',')
		END
	END
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT 
		@numNewItemCode
		,CONCAT(SUBSTRING(vcPathForImage, 0, CHARINDEX('.',vcPathForImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForImage, charindex('.',vcPathForImage,0), LEN(vcPathForImage)))
		,CONCAT(SUBSTRING(vcPathForTImage, 0, CHARINDEX('.',vcPathForTImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForTImage, charindex('.',vcPathForTImage,0), LEN(vcPathForTImage)))
		,bitDefault
		,intDisplayOrder
		,numDomainId
		,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID

	IF ISNULL(@bitMatrix,0) = 1 AND (SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numNewItemCode) > 0
	BEGIN
		-- INSERT NEW WAREHOUSE ATTRIBUTES
		INSERT INTO CFW_Fld_Values_Serialized_Items
		(
			Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized
		)                                                  
		SELECT 
			Fld_ID,
			ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			0 
		FROM 
			ItemAttributes  
		OUTER APPLY
		(
			SELECT
				numWarehouseItemID
			FROM 
				WarehouseItems 
			WHERE 
				numDomainID=@numDomainID 
				AND numItemID=@numNewItemCode
		) AS TEMPWarehouse
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numNewItemCode
	END
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 


	SET @numItemCode = @numNewItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityItems.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OI.numWarehouseItmsID
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OI.numWarehouseItmsID
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END) 
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)


		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,dtReleaseDate
			,numUnitHour
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,dtReleaseDate
			,SUM(numUnitHour)
		FROM
			@TEMP
		GROUP BY
			numItemCode
			,numWarehouseItemID
			,dtReleaseDate

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDate,(CASE 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + ISNULL((SELECT 
																				SUM(numUnitHour) 
																			FROM 
																				OpportunityItems 
																			INNER JOIN 
																				OpportunityMaster 
																			ON 
																				OpportunityItems.numOppID=OpportunityMaster.numOppId 
																			WHERE 
																				OpportunityMaster.numDomainId=',@numDomainID,'
																				AND OpportunityMaster.tintOppType=2 
																				AND OpportunityMaster.tintOppStatus=0 
																				AND OpportunityItems.numItemCode=Item.numItemCode 
																				AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,WI.numWarehouseItemID,T1.dtReleaseDate,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',' + ' dbo.FormatedDateFromDate(T1.dtReleaseDate,Item.numDomainID) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN 
														T1.numUnitHour > 0 
													THEN 1 
													ELSE 0 
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByItemVendor')
DROP PROCEDURE dbo.USP_DemandForecast_GetByItemVendor
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByItemVendor]
	 @numDomainID AS NUMERIC(9),
	 @numItemCode AS NUMERIC(9),
	 @numVendorID AS NUMERIC(18,0)
AS 
BEGIN
	

	SELECT 
		ISNULL((SELECT TOP 1 monCost FROM Vendor WHERE numVendorID = @numVendorID AND numItemCode = @numItemCode),0) AS monCost
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID

	SELECT 
		LD.numListItemID
		,LD.vcData
		,ISNULL(numListValue,0) AS numListValue
		,CONCAT('(',ISNULL(numListValue,'-'),' Days)') AS vcLeadDays
	FROM 
		ListDetails  LD
	OUTER APPLY
	(
		SELECT TOP 1
			*
		FROM
			VendorShipmentMethod VSM
		WHERE
			VSM.numListItemID = LD.numListItemID
			AND VSM.numVendorID = @numVendorID
		ORDER BY
			ISNULL(bitPrimary,0) DESC
	) VSMO
	WHERE 
		(LD.numDomainID=@numDomainID OR ISNULL(LD.constFlag,0) = 1)
		AND LD.numListID = 338
	ORDER BY
		bitPreferredMethod DESC,numListItemID ASC
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetRecordDetails')
DROP PROCEDURE dbo.USP_DemandForecast_GetRecordDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetRecordDetails]
	@numDomainID NUMERIC(18,0)
	,@tintType TINYINT
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@dtReleaseDate DATE
	,@numForecastDays INT
	,@numAnalysisDays INT
	,@bitBasedOnLastYear BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,numOpportunityForecastQty FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	IF @tintType = 1 -- Release Dates
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OI.numItemCode=@numItemCode
			AND OI.numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND CAST(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) AS DATE) = @dtReleaseDate
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		
		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq 
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND CAST((CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END) AS DATE) = @dtReleaseDate
			AND 1 = (CASE 
					WHEN OI.numoppitemtCode IS NOT NULL 
					THEN (CASE 
							WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
							THEN 1 
							ELSE 0 
						END)
					ELSE 
						(CASE 
						WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
						THEN 1 
						ELSE 0 
						END)
					END)
	END
	ELSE IF @tintType = 2 -- Historical Sales
	BEGIN
		DECLARE @dtFromDate DATETIME
		DECLARE @dtToDate DATETIME

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,(CASE WHEN WO.numWOStatus = 23184 THEN 0 ELSE WOD.numQtyItemsReq END)
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)
	END
	ELSE IF @tintType = 3 -- Sales Opportunity
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour
			,OI.numUnitHour * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq * (PP.intTotalProgress/100)
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WorkOrder WO
		ON
			OI.numoppitemtCode = WO.numOppItemID
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WO.numWOId=WOD.numWOId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete
	END

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya(10 Oct 2018)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbackorderpermission')
DROP PROCEDURE usp_getbackorderpermission
GO
CREATE PROCEDURE [dbo].[USP_GetBackOrderPermission]    
@numDomainID AS NUMERIC(9)=0,
@UserAuthGroupId AS NUMERIC(9),
@numitemcode AS NUMERIC(9),
@numQuantity AS NUMERIC(9),
@numWareHouseItemid AS NUMERIC(9),
@kitChildItem AS VARCHAR(MAX)


AS
BEGIN        

DECLARE @UserGroupId AS NUMERIC
DECLARE @numBackOrder AS NUMERIC

SET @UserGroupId = (SELECT COUNT(*) FROM AuthenticationGroupBackOrder WHERE numGroupID = @UserAuthGroupId AND numDomainID = @numDomainID)

IF @UserGroupId > 0
BEGIN
SET @numBackOrder =	(SELECT COUNT(*) FROM WareHouseItems						
						WHERE numItemID = @numitemcode
							AND numOnHand < @numQuantity
							AND numWareHouseItemID = @numWareHouseItemid						
					)

	IF @numBackOrder > 0
		SELECT 0
	ELSE
		SELECT 1
END

ELSE
BEGIN
	SELECT 1
END


END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbackorderselectedauthgroup')
DROP PROCEDURE usp_getbackorderselectedauthgroup
GO
CREATE PROCEDURE [dbo].[USP_GetBackOrderSelectedAuthGroup]    
@numDomainID as numeric(9)=0


AS
BEGIN        

SELECT AGB.numGroupID,AGM.vcGroupName
 
 FROM AuthenticationGroupBackOrder AGB
 JOIN AuthenticationGroupMaster AGM
 ON AGB.numGroupID = AGM.numGroupID
 WHERE AGB.numDomainID=@numDomainID 

END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillPaymentDetails' ) 
    DROP PROCEDURE USP_GetBillPaymentDetails
GO
-- exec USP_GetBillPaymentDetails 0,1,0,0
CREATE PROCEDURE [dbo].[USP_GetBillPaymentDetails]
    @numBillPaymentID NUMERIC(18, 0) ,
    @numDomainID NUMERIC ,
    @ClientTimeZoneOffset INT,
    @numDivisionID NUMERIC=0,
    @numCurrencyID NUMERIC=0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0,
	@SortColumn AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
	@strCondition AS VARCHAR(MAX)=''
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
		
				  
		IF @numBillPaymentID>0
		BEGIN
			SELECT @numDivisionID=numDivisionID FROM [dbo].[BillPaymentHeader] BPH WHERE BPH.numBillPaymentID=@numBillPaymentID
		END

		IF LEN(@strCondition) = 0
		BEGIN
			SET @strCondition = '1=1'
		END

		DECLARE @strSQLTable1 AS NVARCHAR(MAX)
		DECLARE @strSQLTable2 AS NVARCHAR(MAX)
		DECLARE @strFinal AS NVARCHAR(MAX)

SET @strSQLTable1 = CONCAT(' SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
						,ISNULL(BPH.monPaymentAmount,0) AS monPaymentAmount,ISNULL(BPH.monAppliedAmount,0) AS monAppliedAmount,
					 ISNULL(CH.numCheckHeaderID, 0) numCheckHeaderID,
					 ISNULL(CH.numCheckNo,0) AS numCheckNo,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
					 BPH.numCurrencyID,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
					,ISNULL(C.varCurrSymbol,'''') varCurrSymbol
					,''',ISNULL(@BaseCurrencySymbol,''),''' AS BaseCurrencySymbol					
					,ISNULL(monRefundAmount,0) AS monRefundAmount,ISNULL(BPH.numAccountClass,0) AS numAccountClass
					  FROM  [dbo].[BillPaymentHeader] BPH
							LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID
							WHERE BPH.numBillPaymentID =',ISNULL(@numBillPaymentID,0),' ')
		
		IF LEN(@SortColumn) = 0 AND LEN(@columnSortOrder) = 0
		BEGIN
			SET @SortColumn = 'dtDueDate'
			SET	@columnSortOrder = 'ASC'
		END 

print @strSQLTable1
print @SortColumn
print @columnSortOrder

		--For Edit Bill Payment Get Details
SET @strSQLTable2 = ' (
						SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else OBD.numOppId end AS numoppid,
							  BPH.numDivisionID,BPD.[numOppBizDocsID],BPD.[numBillID],
							  CASE WHEN(BPD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN ''Laned Cost-'' ELSE ''Bill-'' END + CONVERT(VARCHAR(10),BH.numBillID)
							  else OBD.vcbizdocid END AS [Name],
							  CASE BPD.[tintBillType] WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
							  CASE BPD.[tintBillType] WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.[monAmount]) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.[monAmount] END monAmountDue,
							  CASE BPD.[tintBillType] WHEN 1 THEN '''' WHEN 2 THEN BH.vcReference END Reference,
							  CASE BPD.[tintBillType] WHEN 1 THEN dbo.fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN dbo.fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
							  BPD.[tintBillType],
							  CASE BPD.[tintBillType]
								  WHEN 1
								  THEN CASE ISNULL(OM.bitBillingTerms, 0)
										 WHEN 1
										 
										 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),'+ CONVERT(VARCHAR(10),@numDomainId) +')
										 WHEN 0
										 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																		 '+ CONVERT(VARCHAR(10), @numDomainId) +')
									   END
								  WHEN 2
								  THEN [dbo].[FormatedDateFromDate](BH.dtDueDate, '+ CONVERT(VARCHAR(10),@numDomainID) +') END AS DueDate,
								  BPD.[monAmount] AS monAmountPaid,1 bitIsPaid
								  ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
								  ,BH.dtDueDate 
						FROM    [dbo].[BillPaymentHeader] BPH
								INNER JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
								LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
								LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
								LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
																AND CH.tintReferenceType = 8
						                                                 
						WHERE   BPH.[numBillPaymentID] = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +'
						AND BPH.numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +'
						AND (OM.numCurrencyID = '+ CONVERT(VARCHAR(10),@numCurrencyID) +' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0 OR ISNULL(BPD.numOppBizDocsID,0) =0)
						AND (ISNULL(BPH.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass) +' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
	            
						UNION
				
						SELECT  opp.numoppid AS numoppid ,
								opp.numdivisionid AS numDivisionID ,
								OBD.numOppBizDocsId AS numOppBizDocsId ,
								0 AS numBillID ,
								OBD.vcbizdocid AS [name] ,
								OBD.monDealAmount AS monOriginalAmount ,
								( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
			
								OBD.vcRefOrderNo AS reference ,
			
								c.vccompanyname AS vccompanyname ,
								1 AS [tintBillType] , 
								CASE ISNULL(Opp.bitBillingTerms, 0)
								  WHEN 1
								  
								  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OBD.dtFromDate),'+ CONVERT(VARCHAR(10),@numDomainId) +')
								  WHEN 0
								  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																	'+ CONVERT(VARCHAR(10),@numDomainId) +')
								END AS DueDate ,
								0 AS monAmountPaid,
								0 bitIsPaid
								,ISNULL(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
								,OBD.[dtFromDate] AS dtDueDate
						FROM    OpportunityBizDocs OBD
								INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
								INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
								INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
																 AND ADC.numDivisionId = Div.numDivisionID
								INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
						WHERE   Opp.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) +'
								AND OBD.bitAuthoritativeBizDocs = 1
								AND Opp.tintOppType = 2
								AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
								AND (Opp.numDivisionId = '+ CONVERT(VARCHAR(10),@numDivisionID) +' OR ISNULL('+ CONVERT(VARCHAR(10),@numDivisionID) +',0)=0)
								AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +')
								AND (Opp.numCurrencyID = '+ CONVERT(VARCHAR(10),@numCurrencyID)+' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0)
								AND (ISNULL(Opp.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass)+' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
						 UNION       
						
						SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numoppid ,
								numDivisionID AS numDivisionID ,
								0 AS numOppBizDocsId ,
								BH.numBillID ,
								Case When isnull(BH.bitLandedCost,0)=1 THEN ''Laned Cost-'' ELSE ''Bill-'' END + CONVERT(VARCHAR(10),BH.numBillID) AS [name] ,
								BH.monAmountDue AS monOriginalAmount ,
								(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
			
								BH.vcReference AS reference ,
			
								dbo.fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
								2 AS [tintBillType] ,
								[dbo].[FormatedDateFromDate](BH.dtDueDate,'+ CONVERT(VARCHAR(10),@numDomainId) +') AS DueDate ,
								0 AS monAmountPaid,
								0 bitIsPaid
								,ISNULL(BH.fltExchangeRate,1) fltExchangeRateOfOrder
								,BH.dtDueDate
						FROM    dbo.BillHeader BH
						WHERE   BH.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) +'
								AND BH.bitIsPaid = 0
								AND (BH.numDivisionId = '+ CONVERT(VARCHAR(10),@numDivisionID) +' OR ISNULL('+ CONVERT(VARCHAR(10),@numDivisionID) +',0)=0) 
								AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +')      
								AND (ISNULL(BH.numCurrencyID,'+ CONVERT(VARCHAR(10),@numBaseCurrencyID) +') = '+ CONVERT(VARCHAR(10),@numCurrencyID) +' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0)
								AND (ISNULL((SELECT TOP 1 numClassID FROM BillDetails BD WHERE BD.numBillID=BH.numBillID),0)='+ CONVERT(VARCHAR(10),@numAccountClass) +' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
					) [BillPayment]  WHERE '+ convert(varchar(MAX),@strCondition) 

		SET @strFinal = CONCAT(@strSQLTable1,'SELECT Row_number() OVER ( ORDER BY ',@SortColumn,' ',@columnSortOrder,') AS row,* INTO #tempBillPayment FROM ', @strSQLTable2,'; SELECT @TotalRecords = COUNT(*) FROM #tempBillPayment; SELECT * FROM #tempBillPayment  WHERE row > ',@firstRec,' and row <',@lastRec,'; DROP TABLE #tempBillPayment ;')   

PRINT @strFinal

print  @TotRecs
exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT


GO


/****** Object:  StoredProcedure [dbo].[USP_GetChildItems]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [dbo].[USP_GetChildItems]  173031   
--created by Anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchilditems')
DROP PROCEDURE usp_getchilditems
GO
CREATE PROCEDURE [dbo].[USP_GetChildItems]                             
@numKitId as numeric(9)                            
as                            
select numItemCode,vcItemName,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
Item.monListPrice monListPrice, 
convert(varchar(30),Item.monListPrice)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType
from item                                
join ItemDetails Dtl                 
on numChildItemID=numItemCode                    
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit
where  numItemKitID=@numKitId
GO
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
	@numKitId AS NUMERIC(18,0)                            
	,@numWarehouseItemID NUMERIC(18,0)
AS                            
BEGIN

	WITH CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPrice
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST(0 AS NUMERIC(18,0))
			,convert(NUMERIC(18,0),0)
			,numItemCode
			,vcItemName
			,vcSKU
			,ISNULL(Item.bitKitParent,0)
			,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN ISNULL(item.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(item.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(item.numItemGroup,0) > 0 AND (ISNULL(item.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,ISNULL(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
			,ISNULL(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.numItemDetailID
			,Dtl.numItemKitID
			,i.numItemCode
			,i.vcItemName
			,i.vcSKU
			,ISNULL(i.bitKitParent,0)
			,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN ISNULL(i.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(i.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(i.numItemGroup,0) > 0 AND (ISNULL(i.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
			,ISNULL(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=@numKitId
	)

	SELECT * INTO #temp FROM CTE

	;WITH Final AS 
	(
		SELECT 
			*
			,1 AS RStageLevel1 
		FROM 
			#temp 
		WHERE 
			numitemcode NOT IN (SELECT numItemKitID FROM #temp)
		UNION ALL
		SELECT 
			t.*
			,c.RStageLevel1 + 1 AS RStageLevel1 
		FROM 
			#temp t 
		JOIN 
			Final c 
		ON 
			t.numitemcode=c.numItemKitID
	)


	UPDATE 
		t 
	SET 
		t.RStageLevel=f.RStageLevel 
	FROM 
		#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
	WHERE 
		t.numitemcode=f.numitemcode 
		AND t.numItemKitID=f.numItemKitID 


	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		DECLARE @numWarehouseID NUMERIC(18,0)
		SELECT @numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

		IF EXISTS 
		(
			SELECT
				c.numItemCode
			FROM
				#temp c 
			WHERE
				c.ItemType='P'
				AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID=@numWarehouseID) = 0
		)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				numWareHouseItemID
		) WI
		ORDER BY 
			c.StageLevel
	END
	ELSE
	BEGIN
		SELECT DISTINCT 
			c.*
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		ORDER BY 
			c.StageLevel
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDetailForImport' ) 
    DROP PROCEDURE USP_GetDetailForImport
GO
CREATE PROCEDURE USP_GetDetailForImport
    (
      @numDomainID NUMERIC(18),
      @numMasterID NUMERIC(9)
    )
AS 
    BEGIN
    
		DECLARE @strSQL AS NVARCHAR(MAX)
		DECLARE @vcColumns AS VARCHAR(MAX)
                
        IF ISNULL(@numMasterID, 0) = 31 --AND ISNULL(@vcColumns,'') <> '' 
            BEGIN
				
				SELECT  @vcColumns = COALESCE(@vcColumns + ',', '')
                + (vcLookBackTableName + '.' + vcDbColumnName + ' AS [' + MAPPING.vcFieldName + ']')
				FROM    dycFormField_Mapping MAPPING
						JOIN dbo.DycFieldMaster MAST ON MAPPING.numFieldID = MAST.numFieldId
				WHERE   numFormID = ISNULL(@numMasterID, 0)
				ORDER BY MAPPING.[order]
				
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numItemDetailID AS [Item Detail ID],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numItemKitID AS [Item Name],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numChildItemID AS [Child Item Name],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numWareHouseItemID','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numUOMId','(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = dbo.ItemDetails.numUOMId)')
				
				PRINT @vcColumns
        
                -- SELECT  * FROM dycFormField_Mapping WHERE numFormID = 31
                SET @strSQL = ''
                SET @strSQL = ' SELECT numItemDetailID AS [Item Detail ID],Item.numItemCode AS [Item Code],Item.vcItemName AS [Item Name],Item1.vcItemName AS [Child Item Name],
								' + @vcColumns + ' FROM dbo.ItemDetails 
								LEFT JOIN dbo.Item ON dbo.ItemDetails.numItemKitID = dbo.Item.numItemCode
								LEFT JOIN dbo.Item Item1 ON dbo.ItemDetails.numChildItemID = Item1.numItemCode
							    WHERE Item.numDomainID = ' + CONVERT(VARCHAR(10), @numDomainID)
							    + ' AND ISNULL(dbo.Item.bitAssembly,0) = 1 
							        AND ISNULL(dbo.Item.bitKitParent,0) = 1 
							        ORDER BY Item.numItemCode '
                PRINT @strSQL
                EXEC SP_EXECUTESQL @strSQL
            END
	
        IF ISNULL(@numMasterID, 0) = 54 --AND ISNULL(@vcColumns,'')  <> '' 
            BEGIN
            
				SELECT  @vcColumns = COALESCE(@vcColumns + ',', '') + (vcLookBackTableName + '.' + vcDbColumnName + ' AS [' + MAPPING.vcFieldName + ']')
				FROM    dycFormField_Mapping MAPPING
						JOIN dbo.DycFieldMaster MAST ON MAPPING.numFieldID = MAST.numFieldId
				WHERE   numFormID = ISNULL(@numMasterID, 0)
				ORDER BY MAPPING.[order]
				PRINT @vcColumns
				
				-- SELECT * FROM dycFormField_Mapping WHERE numFormID = 54
				SET @strSQL = ''
                SET @strSQL = 'SELECT Item.vcItemName AS [Item Name],' + @vcColumns + ' FROM Item 
							   LEFT JOIN dbo.ItemImages ON Item.numItemCode = ItemImages.numItemCode AND Item.numDomainID = ItemImages.numDomainID
							   WHERE Item.numDomainID = ' + CONVERT(VARCHAR(10), @numDomainID)
							   + ' ORDER BY Item.numItemCode'
                PRINT @strSQL
                
                EXEC SP_EXECUTESQL @strSQL
				
            END

    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetKitSubItemDetails')
DROP PROCEDURE USP_GetKitSubItemDetails
GO
CREATE PROCEDURE USP_GetKitSubItemDetails
(
	@numItemDetailID		BIGINT,
	@numDomainID			NUMERIC(18)
)
AS 
BEGIN
	
	SELECT  ISNULL(ID.numItemDetailID,0) AS [numItemDetailID],
			ISNULL(ID.numItemKitID,0) AS [numItemKitID],
			ISNULL(ID.numChildItemID,'') AS [numChildItemID],
			ISNULL(ID.numQtyItemsReq,'') AS [numQtyItemsReq],
			ISNULL(ID.numUOMId,0) AS [numUOMId],
			ISNULL(ID.vcItemDesc,0) AS [vcItemDesc],
			ISNULL(ID.sintOrder,0) AS [sintOrder]
	FROM  dbo.ItemDetails ID	
	JOIN dbo.Item I ON ID.numItemKitID = I.numItemCode
	WHERE ID.numItemDetailID = @numItemDetailID 
	AND I.numDomainID = @numDomainID
	
END
/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetailsDynamicForm]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                    
--Purpose: Returns the available form fields from the database                                                        
--Created Date: 07/09/2005                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistdetailsdynamicform')
DROP PROCEDURE usp_getmasterlistdetailsdynamicform
GO
CREATE PROCEDURE [dbo].[usp_GetMasterListDetailsDynamicForm]                      
 @numListID NUMERIC(9),                        
 @vcItemType CHAR(3),                      
 @numDomainID NUMERIC(9)                      
AS     
 
 IF @vcItemType = 'SYS'
	BEGIN
		SELECT 0 AS numItemID,'Lead' AS vcItemName
		UNION ALL
		SELECT 1 AS numItemID,'Prospect' AS vcItemName
		UNION ALL
		SELECT 2 AS numItemID,'Account' AS vcItemName
	END
	
 IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	 BEGIN
		EXEC USP_GetOpportunitySource @numDomainID
	 END                 
 ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END      
	ELSE IF @vcItemType = 'L' AND ISNULL(@numListID,0) > 0    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END                                          
 ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN    
		SELECT vcData AS vcItemName, numListItemID AS numItemID, 'T' As vcItemType, 0 As flagConst FROM ListDetails WHERE numListID = 78 AND numDomainID = @numDomainID                                        
	END                        
 ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN                        
		SELECT vcGrpName AS vcItemName, numGrpId AS numItemID, 'G' As vcItemType, 0 As flagConst FROM Groups                     
	END                         
 ELSE IF @vcItemType = 'S'    --States                    
	BEGIN                        
		SELECT vcState AS vcItemName, numStateID AS numItemID, 'S' As vcItemType, numCountryID As flagConst FROM State WHERE (numDomainID = @numDomainID OR constFlag = 1) AND 1 = 2                    
	END         
 ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN                        
		SELECT A.numContactID AS numItemID,A.vcFirstName+' '+A.vcLastName AS vcItemName              
		from UserMaster UM             
		join AdditionalContactsInformation A            
		on UM.numUserDetailId=A.numContactID              
		where UM.numDomainID=@numDomainID        
		union        
		select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName        
		from AdditionalContactsInformation A         
		join DivisionMaster D        
		on D.numDivisionID=A.numDivisionID        
		join ExtarnetAccounts E         
		on E.numDivisionID=D.numDivisionID        
		join ExtranetAccountsDtl DTL        
		on DTL.numExtranetID=E.numExtranetID        
		join CompanyInfo C        
		on C.numCompanyID=D.numCompanyID        
		where A.numDomainID=@numDomainID and bitPartnerAccess=1        
		and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)                      
	END
  ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
	   SELECT  numCampaignID AS numItemID,
                vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcItemName
       FROM    CampaignMaster
       WHERE   numDomainID = @numDomainID
	END               
  ELSE IF @vcItemType = 'IG' 
	BEGIN
		SELECT vcItemGroup AS vcItemName, numItemGroupID AS numItemID, 'I' As vcItemType, 0 As flagConst FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
	END
 ELSE IF @vcItemType = 'PP' 
   BEGIN
		SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst 
   END
 ELSE IF @vcItemType = 'V' 
   BEGIN
		SELECT DISTINCT ISNULL(C.vcCompanyName,'') AS vcItemName,  numVendorID AS numItemID ,'V' As vcItemType, 0 As flagConst   FROM dbo.Vendor V INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID= V.numVendorID 
		INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE V.numDomainID =@numDomainID
   END
 ELSE IF @vcItemType = 'OC' 
   BEGIN
		SELECT DISTINCT C.vcCurrencyDesc AS vcItemName,C.numCurrencyID AS numItemID FROM dbo.OpportunityMaster OM INNER JOIN dbo.Currency C ON C.numCurrencyID = OM.numCurrencyID
		WHERE OM.numDomainId=@numDomainID
   END
 ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
	   
	   
	   --SELECT  numUOMId AS numItemID,
    --            vcUnitName AS vcItemName
    --   FROM    UOM
    --   WHERE   numDomainID = @numDomainID

	--Modified by Sachin Sadhu ||Date:7thAug2014
	--Purpose :Boneta facing problem in updating units 
	   
			SELECT u.numUOMId as numItemID , u.vcUnitName as vcItemName FROM 
			UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
			WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
			u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)


    END   
 ELSE IF @vcItemType = 'O'    --Opp Type                   
	 BEGIN
		SELECT  1 AS numItemID,'Sales' AS vcItemName
		UNion ALL
		SELECT  2 AS numItemID,'Purchase' AS vcItemName
	 END  
 ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT  1 AS numItemID,'Yes' AS vcItemName
		UNion ALL
		SELECT  0 AS numItemID,'No' AS vcItemName
	END  
 ELSE IF @vcItemType='COA'
	BEGIN
		SELECT  C.[numAccountId] AS numItemID,ISNULL(C.[vcAccountName],'') AS vcItemName
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		ORDER BY C.[vcAccountCode]
	END
	ELSE IF @vcItemType = 'PSS'
	BEGIN
		SELECT 
			numShippingServiceID AS numItemID
			,vcShipmentService AS vcItemName 
		FROM 
			ShippingService 
		WHERE 
			(numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0)
		ORDER BY 
			vcShipmentService
	END
       
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,CPN.CustomerPartNo
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode 
						AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice DECIMAL(20,5),numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost DECIMAL(20,5),numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500),
					ItemRequiredDate DATETIME
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate 
						END )
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)
				ORDER BY
					OI.numSortOrder


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitItems OKI
				INNER JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitChildItems OKCI
				INNER JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues,
					(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE  OI.ItemReleaseDate
						END ) AS ItemRequiredDate
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
					,ISNULL(CPN.CustomerPartNo,'') CustomerPartNo
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate
						END ) AS ItemRequiredDate
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,OI.numWarehouseItmsID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcItemName'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable ORDER BY [row]; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItems')
DROP PROCEDURE USP_GetWareHouseItems
GO



CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL,
@numWarehouseID numeric(18,0)=0,
@numWLocationID AS NUMERIC(18,0)=0 --05052018 Change: Warehouse Location added

                         
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
DECLARE @bitAssembly BIT
DECLARE @bitMatrix BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END )
	,@bitMatrix=bitMatrix
	,@bitAssembly=ISNULL(bitAssembly,0)
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


DECLARE @ID AS NUMERIC(18,0)  = 0                       
DECLARE @numCusFlDItemID AS VARCHAR(20)                        
DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        
DECLARE @Fld_ValueMatrix NUMERIC(18,0)
DECLARE @Fld_ValueNameMatrix VARCHAR(300)

IF @bitMatrix = 1
BEGIN
	SET @ColName='I.numItemCode,1' 

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.fld_id) ID
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value
		,CASE 
			WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
			WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
			ELSE CAST(Fld_Value AS VARCHAR)
		END AS FLD_ValueName
	INTO 
		#tempTableMatrix
	FROM
		CFW_Fld_Master 
	INNER JOIN
		ItemGroupsDTL 
	ON 
		CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
		AND ItemGroupsDTL.tintType = 2
	LEFT JOIN
		ItemAttributes
	ON
		CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
		AND ItemAttributes.numItemCode = @numItemCode
	LEFT JOIN
		ListDetails LD
	ON
		CFW_Fld_Master.numlistid = LD.numListID
	WHERE
		CFW_Fld_Master.numDomainID = @numDomainId
		AND ItemGroupsDTL.numItemGroupID = @numItemGroupID
	GROUP BY
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.bitAutocomplete
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 

	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM #tempTableMatrix
                         
	WHILE @i <= @iCount
	 BEGIN                        
		SELECT @fld_label=Fld_label,@Fld_ValueMatrix=FLD_Value,@Fld_ValueNameMatrix=FLD_ValueName FROM #tempTableMatrix WHERE ID=@i
                          
		SET @str = @str+ CONCAT (',',@Fld_ValueMatrix,' as [',@fld_label,']')
	
		IF @byteMode=1                                        
			SET @str = @str+ CONCAT (',''',@Fld_ValueNameMatrix,''' as [',@fld_label,'Value]')                                   
                          
		SET @i = @i + 1
	 END
END
ELSE
BEGIN
	SET @ColName='WareHouseItems.numWareHouseItemID,0' 

	--Create a Temporary table to hold data                                                            
	create table #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                                                                      
		numCusFlDItemID NUMERIC(9),
		Fld_Value VARCHAR(20)                                                         
	)

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		numOppAccAttrID
		,''
	FROM 
		ItemGroupsDTL 
	WHERE 
		numItemGroupID=@numItemGroupID 
		AND tintType=2    

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END          
                   
                          
                 
                      
                         
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) AS TotalOnHand,
(ISNULL(WareHouseItems.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
CAST(ISNULL(numReorder,0) AS FLOAT) as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 OR @bitAssembly=1 THEN 0 ELSE CAST(ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppID=OM.numOppID WHERE OM.numDomainID=' + CAST(@numDomainID AS VARCHAR) + ' AND tintOppType=1 AND tintOppStatus=0 AND OI.numItemCode=WareHouseItems.numItemID AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) END as [Requisitions]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',ISNULL(@bitLot,0),')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute,
I.numItemcode,
ISNULL(I.bitKitParent,0) AS bitKitParent,
ISNULL(I.bitSerialized,0) AS bitSerialized,
ISNULL(I.bitLotNo,0) as bitLotNo,
ISNULL(I.bitAssembly,0) as bitAssembly,
ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) [IsDeleteKitWarehouse],
CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END AS bitChildItemWarehouse,
I.numAssetChartAcntId,
(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'    
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'        
   
print CAST(@str1 AS NTEXT)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@bitAssembly bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @bitAssembly, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID
,WDTL.numWareHouseItemID
,vcSerialNo
,WDTL.vcComments as Comments
,WDTL.numQty
,WDTL.numQty as OldQty
,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )' 
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'   
                         
print CAST(@str1 AS NTEXT)                      
exec (@str1)                       
                      
IF @bitMatrix = 1
BEGIN
	SELECT
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTableMatrix

	DROP TABLE #tempTableMatrix
END
ELSE
BEGIN                      
	SELECT 
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master
	ON 
		numCusFlDItemID=Fld_ID                      

	DROP TABLE #tempTable
END

                      


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else'' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID NUMERIC(18,0),
@ClientTimeZoneOffset  INT,
@numWOStatus NUMERIC(18,0),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000),
@tintFilter TINYINT = 0,
@numOppID NUMERIC(18,0),
@numOppItemID NUMERIC(18,0)                                                                
AS                            
BEGIN
	SET @vcWOId=ISNULL(@vcWOId,'')

	DECLARE @firstRec AS INTEGER
	DECLARE @lastRec AS INTEGER
	SET @firstRec= ((@CurrentPage-1) * @PageSize) + 1                                                                           
	SET @lastRec= (@CurrentPage * @PageSize)
	
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID   
    
	DECLARE @TEMP TABLE
	(
		ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
		numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
		bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
		numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
	)

	DECLARE @TEMPWO TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @TEMPWORKORDER TABLE
	(
		ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
		bitWorkOrder BIT, bitReadyToBuild BIT
	) 

	INSERT INTO 
		@TEMPWORKORDER
	SELECT 
		1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS FLOAT),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		1 AS bitWorkOrder,
		0 AS bitReadyToBuild
	FROM 
		WorkOrder
	WHERE  
		numDomainID=@numDomainID
		AND (numOppID=@numOppID OR ISNULL(@numOppID,0)=0)
		AND (numOppItemID=@numOppItemID OR ISNULL(@numOppItemID,0)=0)
		AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
		AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
		AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
		AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
	ORDER BY
		bintCreatedDate DESC,
		numWOId ASC

	;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
						bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
	(
		SELECT
			ItemLevel,
			numParentWOID,
			numWOId,
			ID,
			numItemCode,
			numQty,
			numWareHouseItemId,
			vcInstruction,
			numAssignedTo,
			bintCreatedDate,
			numCreatedBy,
			bintCompliationDate,
			numWOStatus,
			numOppId,
			1 AS bitWorkOrder,
			0 AS bitReadyToBuild
		FROM
			@TEMPWORKORDER t
		WHERE
			t.RowNumber BETWEEN @firstRec AND @lastRec
		UNION ALL
		SELECT 
			c.ItemLevel+2,
			c.numWOID,
			WorkOrder.numWOId,
			CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
			WorkOrder.numItemCode,
			CAST(WorkOrder.numQtyItemsReq AS FLOAT),
			WorkOrder.numWareHouseItemId,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
			WorkOrder.numAssignedTo,
			WorkOrder.bintCreatedDate,
			WorkOrder.numCreatedBy,
			WorkOrder.bintCompliationDate,
			WorkOrder.numWOStatus,
			WorkOrder.numOppId,
			1 AS bitWorkOrder,
			0 AS bitReadyToBuild
		FROM 
			WorkOrder
		INNER JOIN 
			CTEWorkOrder c 
		ON 
			WorkOrder.numParentWOID = c.numWOID
	)

	INSERT 
		@TEMP
	SELECT
		ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		OpportunityMaster.vcPOppName,
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
	FROM 
		CTEWorkOrder
	INNER JOIN 
		Item
	ON
		CTEWorkOrder.numItemKitID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN 
		OpportunityMaster
	ON
		CTEWorkOrder.numOppID = OpportunityMaster.numOppId

	INSERT INTO 
		@TEMP
	SELECT
		t1.ItemLevel + 1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		OpportunityMaster.vcPOppName,
		0 AS bitWorkOrder,
		CASE 
			WHEN Item.charItemType='P'
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(t1.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		WorkOrderDetails
	INNER JOIN
		@TEMP t1
	ON
		WorkOrderDetails.numWOId = t1.numWOID
	INNER JOIN 
		Item
	ON
		WorkOrderDetails.numChildItemID = Item.numItemCode
		AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster 
	ON
		WorkOrderDetails.numPOID = OpportunityMaster.numOppId

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempWOID NUMERIC(18,0)
	INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

	SELECT @iCount=COUNT(*) FROM @TEMPWO

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

		UPDATE
			T1
		SET
			bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
		FROM
			@TEMP T1
		WHERE
			numWOID=@numTempWOID

		SET @i = @i + 1
	END

	If @tintFilter = 2 --Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 0 ELSE 1 END) AND bitWorkOrder=1
	END
	ELSE IF @tintFilter = 3 --Un-Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 1 ELSE 0 END) AND bitWorkOrder=1
	END

	SELECT * FROM @TEMP ORDER BY numParentWOID,ItemLevel

	DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

	SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMPWORKORDER WHERE numWOID IN (SELECT numWOID FROM @TEMP)

	SELECT @TOTALROWCOUNT AS TotalRowCount 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,@numIncomeChartAcntId
			,@numAssetChartAcntId
			,@numCOGsChartAcntId
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertAssemblyItems')
DROP PROCEDURE USP_InsertAssemblyItems
GO

/*
BEGIN TRANSACTION
declare @p8 bigint
set @p8=0
exec USP_InsertAssemblyItems @numItemKitID=197596,@numChildItemID=198001,@numQtyItemsReq=10,@numUOMId=3,@numWareHouseId=58,@vcItemDesc='Adding 1',@sintOrder=1,@numItemDetailID=@p8 output
select @p8

ROLLBACK
*/

CREATE PROCEDURE [dbo].[USP_InsertAssemblyItems]
@numItemKitID AS numeric(9)=0,
@numChildItemID AS numeric(9)=0,
@numQtyItemsReq AS numeric(9)=0,
@numUOMId AS numeric(9)=0,
@numWareHouseId AS numeric(9)=0,
@vcItemDesc AS TEXT,
@sintOrder AS INT=0,
@numItemDetailID	NUMERIC(18,0) OUTPUT
AS
BEGIN
 
IF (SELECT COUNT(*) FROM ItemDetails WHERE numItemKitID = @numItemKitID AND numChildItemID = @numChildItemID) = 0
BEGIN
	insert into ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder)
	values(@numItemKitID,@numChildItemID,@numQtyItemsReq,@numUOMId,@vcItemDesc,@sintOrder)
	
	SET @numItemDetailID = SCOPE_IDENTITY()
	PRINT @numItemDetailID
END 
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		ROW_NUMBER() OVER (ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,                      
		WI.numOnOrder,                      
		WI.numReorder,                      
		WI.numAllocation,                      
		WI.numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	INTO
		#Temp
	FROM
		Item
	OUTER APPLY
	(
		SELECT
			SUM(numOnHand) as numOnHand,                      
			SUM(numOnOrder) as numOnOrder,                      
			SUM(numReorder)  as numReorder,                      
			SUM(numAllocation)  as numAllocation,                      
			SUM(numBackOrder)  as numBackOrder
		FROM 
			WarehouseItems
		WHERE
			numItemID=Item.numItemCode
	) WI
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	ORDER BY
		Item.numItemCode
	OFFSET (CASE WHEN ISNULL(@numPageIndex,0) > 0 THEN (@numPageIndex - 1) ELSE 0 END) * @numPageSize ROWS FETCH NEXT (CASE WHEN ISNULL(@numPageSize,0) > 0 THEN @numPageSize ELSE 999999999 END) ROWS ONLY

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master
		LEFT JOIN
			CFW_FLD_Values_Item 
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)

	DROp TABLE #Temp
END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_Search_BIZAPI' ) 
    DROP PROCEDURE USP_Item_Search_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_Search_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID 
		AND	Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE Item.monAverageCost END) monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID AND
		Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.bitVirtualInventory,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master
		LEFT JOIN
			CFW_FLD_Values_Item 
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)

	DROp TABLE #Temp
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT,
	@bitTransferTo AS BIT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strSQLCount NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
DECLARE @tintDecimalPoints AS TINYINT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
	,@tintDecimalPoints=ISNULL(tintDecimalPoints,4)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CAST(CAST(CASE WHEN I.[charItemType] = ''P''
					THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0)
					ELSE monListPrice
					END AS DECIMAL(20,'+ CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CAST(CAST(CASE WHEN I.[charItemType] = ''P''
					THEN ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0)
					ELSE monListPrice
					END AS DEIMCAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END

				DECLARE @numCompany AS NUMERIC(18)
				SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId
						 
                IF @tintOppType = 1 OR (@bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN    
					SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
											COUNT(I.numItemCode) 
										FROM 
											Item  I WITH (NOLOCK)
										LEFT JOIN 
											DivisionMaster D              
										ON 
											I.numVendorID=D.numDivisionID  
										LEFT JOIN 
											CompanyInfo C  
										ON 
											C.numCompanyID=D.numCompanyID 
										LEFT JOIN 
											CustomerPartNumber  CPN  
										ON 
											I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  '
										LEFT JOIN
											dbo.WareHouseItems WHT
										ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND (numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
					IF @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2
                            SET @strSQLCount = @strSQLCount + ' AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                    IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                    BEGIN
                        SET @strSQLCount = @strSQLCount
                            + ' AND I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                            + CONVERT(VARCHAR(20), @numDomainID)
                            + ' and oppM.numDivisionId='
                            + CONVERT(VARCHAR(20), @numDivisionId)
                            + ')'
                    END

					SET @strSQLCount = @strSQLCount + '),0);'

                        SET @strSQL = 'select I.numItemCode, ISNULL(CPN.CustomerPartNo,'''') AS CustomerPartNo,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CAST(CAST(CASE 
									  WHEN I.[charItemType]=''P''
									  THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0) 
									  ELSE 
										monListPrice 
									  END AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) +')) AS VARCHAR) AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup '
									  + @strSQL
									  + ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									 LEFT JOIN CustomerPartNumber  CPN  
									 ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  '
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND (numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2
						BEGIN
                            SET @strSQL = @strSQL + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

							IF ISNULL(@bitTransferTo,0) = 1
							BEGIN
								SET @strSQL = @strSQL + ' and ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.bitSerialized,0) = 0 AND I.charItemType = ''P'' '
							END
						END

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

						IF @numDomainID <> 204
						BEGIN
							SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')
						END
						ELSE
						BEGIN
							SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE 
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'

						END

						SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @str + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'

						IF @numDomainID <> 204
						BEGIN
							SET @strSQL = @strSQL + @strSQLCount

							 SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						END
						ELSE
						BEGIn
							SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal;'

							 SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						END
						
						--PRINT @strSQL
						EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                END      
                ELSE IF @tintOppType = 2
                BEGIN     
					SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
																	COUNT(I.numItemCode) 
																FROM 
																	Item I  WITH (NOLOCK)
																INNER JOIN
																	dbo.Vendor V
																ON
																	V.numItemCode = I.numItemCode
																LEFT JOIN 
																	DivisionMaster D              
																ON 
																	V.numVendorID=D.numDivisionID  
																LEFT join 
																	CompanyInfo C  
																ON 
																	C.numCompanyID=D.numCompanyID
																LEFT JOIN 
																	CustomerPartNumber  CPN  
																ON 
																	I.numItemCode=CPN.numItemCode 
																	AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  ' 
																LEFT JOIN
																	dbo.WareHouseItems WHT
																ON
																	WHT.numItemID = I.numItemCode AND
																	WHT.numWareHouseItemID = (' +
																								CASE LEN(@vcWareHouseSearch)
																								WHEN 0 
																								THEN 
																									'SELECT 
																										TOP 1 numWareHouseItemID 
																									FROM 
																										dbo.WareHouseItems 
																									WHERE 
																										dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																										AND dbo.WareHouseItems.numItemID = I.numItemCode 
																										AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																								ELSE
																									'CASE 
																										WHEN (SELECT 
																													COUNT(*) 
																												FROM 
																													dbo.WareHouseItems 
																												WHERE 
																													dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																													AND dbo.WareHouseItems.numItemID = I.numItemCode 
																													AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																													' AND (' + @vcWareHouseSearch + ')) > 0 
																										THEN
																											(SELECT 
																												TOP 1 numWareHouseItemID 
																											FROM 
																												dbo.WareHouseItems 
																											WHERE 
																												dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																												AND dbo.WareHouseItems.numItemID = I.numItemCode 
																												AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																										ELSE
																											(SELECT 
																												TOP 1 numWareHouseItemID 
																											FROM 
																												dbo.WareHouseItems 
																											WHERE 
																												dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																												AND dbo.WareHouseItems.numItemID = I.numItemCode 
																												AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																										END' 
																								END
																							+
																						')
										       
																		WHERE (
																				(I.charItemType <> ''P'') OR 
																				((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
																				+ CONVERT(VARCHAR(20), @numDomainID)
																				+ ' AND (numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
																				+ CONVERT(VARCHAR(20), @numDivisionID)
																				+ ' and ('+ CASE LEN(@vcVendorSearch)
																								WHEN 0 THEN ''
																								ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																								END + @strSearch + ') ' 

					--- added Asset validation  by sojan
                    IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                    BEGIN
                            SET @strSQLCount = @strSQLCount
                                + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                + CONVERT(VARCHAR(20), @numDomainID)
                                + ' and oppM.numDivisionId='
                                + CONVERT(VARCHAR(20), @numDivisionId)
                                + ')'
                    END
				 
					SET @strSQLCount = @strSQLCount + '),0);'

                    SET @strSQL = 'SELECT I.numItemCode,
									ISNULL(CPN.CustomerPartNo,'''') AS CustomerPartNo,
									(SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									(SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									ISNULL(vcItemName,'''') vcItemName,
									CAST(CAST(monListPrice AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) as monListPrice,
									isnull(vcSKU,'''') vcSKU,
									isnull(numBarCodeId,0) numBarCodeId,
									isnull(vcModelID,'''') vcModelID,
									isnull(txtItemDesc,'''') as txtItemDesc,
									isnull(C.vcCompanyName,'''') as vcCompanyName,
									WHT.numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									(CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup'
									+ @strSQL
									+ ' INTO #TEMPItems from item I  WITH (NOLOCK)
									INNER JOIN
									dbo.Vendor V
									ON
									V.numItemCode = I.numItemCode
									Left join 
									DivisionMaster D              
									ON 
									V.numVendorID=D.numDivisionID  
									LEFT join 
									CompanyInfo C  
									ON 
									C.numCompanyID=D.numCompanyID
									LEFT JOIN 
									CustomerPartNumber  CPN  
									ON 
									I.numItemCode=CPN.numItemCode 
									AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  ' 
									LEFT JOIN
									dbo.WareHouseItems WHT
									ON
									WHT.numItemID = I.numItemCode AND
									WHT.numWareHouseItemID = (' +
																	CASE LEN(@vcWareHouseSearch)
																	WHEN 0 
																	THEN 
																		'SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																	ELSE
																		'CASE 
																			WHEN (SELECT 
																						COUNT(*) 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																						' AND (' + @vcWareHouseSearch + ')) > 0 
																			THEN
																				(SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																			ELSE
																				(SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																			END' 
																	END
																+
															')
										       
									WHERE (
											(I.charItemType <> ''P'') OR 
											((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID)
									+ ' AND (numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
									+ CONVERT(VARCHAR(20), @numDivisionID)
									+ ' and ('+ CASE LEN(@vcVendorSearch)
																	WHEN 0 THEN ''
																	ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																	END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END

							
							IF ISNULL(@bitTransferTo,0) = 1
							BEGIN
								SET @strSQL = @strSQL + ' and ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.bitSerialized,0) = 0 AND I.charItemType = ''P'' '
							END

							IF @numDomainID <> 204
							BEGIN
								SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')
							END
							ELSE
							BEGIN
								SET @strSQL = @strSQL + 'DELETE FROM 
															#TEMPItems
														WHERE 
															numItemCode IN (
																				SELECT 
																					F.numItemCode
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 AND
																					EXISTS (
																								SELECT 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																								FROM 
																									#TEMPItems t1
																								WHERE 
																									t1.vcItemName = F.vcItemName
																									AND t1.bitMatrix = 1
																									AND t1.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																								HAVING 
																									Count(t1.numItemCode) > 1
																							)
																			)
															AND numItemCode NOT IN (
																									SELECT 
																										Min(numItemCode)
																									FROM 
																										#TEMPItems AS F
																									WHERE
																										ISNULL(F.bitMatrix,0) = 1 AND
																										Exists (
																													SELECT 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																													FROM 
																														#TEMPItems t2
																													WHERE 
																														t2.vcItemName = F.vcItemName
																													   AND t2.bitMatrix = 1
																													   AND t2.numItemGroup = F.numItemGroup
																													GROUP BY 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																													HAVING 
																														Count(t2.numItemCode) > 1
																												)
																									GROUP BY 
																										vcItemName, bitMatrix, numItemGroup
																								);'
							END

							SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @str + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


							IF @numDomainID <> 204
							BEGIN
								SET @strSQL = @strSQL + @strSQLCount

								SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
							END
							ELSE
							BEGIn
								SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal;'

								SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal where SRNO> '
								+ CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
								+ ' and SRNO < '
								+ CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
								+ ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
							END

							
                        
							EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   
   PRINT(CAST (@strSQL AS NTEXT))

IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		sum(numReorder)  as numReorder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory,                   
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
   
 @numDomainID NUMERIC(18,0),  
 @numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	SELECT
		Item.numItemCode,
		Item.vcItemName,
		ISNULL((SELECT bitKitSingleSelect FROM Item IInner WHERe IInner.numItemCode=@numItemCode),0) AS bitKitSingleSelect
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
   WHERE
		ItemDetails.numItemKitID = @numItemCode
END  


GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(numCompanyType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		--KEEP THIS BELOW ABOVE IF CONDITION
		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
			,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
			,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
		DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
		SET @searchPositionColumnGroupBy = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
					SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )
				END

			END

		       SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
												( 
													 SELECT
														I.numDomainID
														,I.numItemCode
														,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
														,I.bitMatrix
														,I.bitKitParent
														,I.numItemGroup
														,vcItemName
														,I.bintCreatedDate
														,vcManufacturer
														,ISNULL(txtItemDesc,'''') txtItemDesc
														,vcSKU
														,fltHeight
														,fltWidth
														,fltLength
														,vcModelID
														,fltWeight
														,numBaseUnit
														,numSaleUnit
														,numPurchaseUnit
														,bitFreeShipping
														,C.vcCategoryName
														,C.vcDescription as CategoryDesc
														,charItemType
														,monListPrice
														,bitAllowBackOrder
														,bitShowInStock
														,numVendorID
														,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
														,SUM(numOnHand) numOnHand
														,SUM(numAllocation) numAllocation
													 FROM      
														Item AS I
													 INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
													 LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
													 INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
													 INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
													 INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
													  ',@Join,' ',@DefaultSortJoin)

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterItemAttributes
			END

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			SET @strSQL = @strSQL + @Where
	
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         

            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = CONCAT(@strSQL,' GROUP BY
												I.numDomainID
												,I.numItemCode
												,I.bitMatrix
												,I.bitKitParent
												,I.numItemGroup
												,vcItemName
												,I.bintCreatedDate
												,vcManufacturer
												,txtItemDesc
												,vcSKU
												,fltHeight
												,fltWidth
												,fltLength
												,vcModelID
												,fltWeight
												,numBaseUnit
												,numSaleUnit
												,numPurchaseUnit
												,bitFreeShipping
												,C.vcCategoryName
												,C.vcDescription
												,charItemType
												,monListPrice
												,bitAllowBackOrder
												,bitShowInStock
												,numVendorID
												,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
			IF @numDomainID = 204
			BEGIN
				SET @strSQL = @strSQL + 'DELETE FROM 
											#TEMPItems
										WHERE 
											numItemCode IN (
																SELECT 
																	F.numItemCode
																FROM 
																	#TEMPItems AS F
																WHERE 
																	ISNULL(F.bitMatrix,0) = 1 
																	AND EXISTS (
																				SELECT 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																				FROM 
																					#TEMPItems t1
																				WHERE 
																					t1.vcItemName = F.vcItemName
																					AND t1.bitMatrix = 1
																					AND t1.numItemGroup = F.numItemGroup
																				GROUP BY 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																				HAVING 
																					Count(t1.numItemCode) > 1
																			)
															)
											AND numItemCode NOT IN (
																					SELECT 
																						Min(numItemCode)
																					FROM 
																						#TEMPItems AS F
																					WHERE
																						ISNULL(F.bitMatrix,0) = 1 
																						AND Exists (
																									SELECT 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																									FROM 
																										#TEMPItems t2
																									WHERE 
																										t2.vcItemName = F.vcItemName
																									   AND t2.bitMatrix = 1
																									   AND t2.numItemGroup = F.numItemGroup
																									GROUP BY 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																									HAVING 
																										Count(t2.numItemCode) > 1
																								)
																					GROUP BY 
																						vcItemName, bitMatrix, numItemGroup
																				);'

			END
									  
									  
			SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  

			SET @strSQL = CONCAT(@strSQL,'SELECT  
										Rownumber
										,I.numItemCode
										,vcCategoryName
										,CategoryDesc
										,vcItemName
										,txtItemDesc
										,vcManufacturer
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,bitFreeShipping
										,', 
											(CASE 
											WHEN @tintPreLoginPriceLevel > 0 
											THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
											ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
											END)
											,' AS monListPrice,
											ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
											',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
													WHEN tintRuleType = 1 AND tintDiscountType = 1
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 1 AND tintDiscountType = 2
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
													WHEN tintRuleType = 2 AND tintDiscountType = 1
													THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 2 AND tintDiscountType = 2
													THEN ISNULL(Vendor.monCost,0) + decDiscount
													WHEN tintRuleType = 3
													THEN decDiscount
												END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
												WHEN tintRuleType = 1 AND tintDiscountType = 1 
												THEN decDiscount 
												WHEN tintRuleType = 1 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
												WHEN tintRuleType = 2 AND tintDiscountType = 1
												THEN decDiscount
												WHEN tintRuleType = 2 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
												WHEN tintRuleType = 3
												THEN 0
											END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
										,vcPathForImage
										,vcpathForTImage
										,UOM AS UOMConversionFactor
										,UOMPurchase AS UOMPurchaseConversionFactor
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													THEN (CASE 
															WHEN bitAllowBackOrder = 1 THEN 1
															WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
															ELSE 1
														END)
													ELSE 1
										END) AS bitInStock
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													THEN ( CASE WHEN bitShowInStock = 1
																THEN (CASE 
																		WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																		WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																		WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																		ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																	END)
																ELSE ''''
															END )
													ELSE ''''
												END ) AS InStock
										, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
										,(SELECT 
												COUNT(numProId) 
											FROM 
												PromotionOffer PO
											WHERE 
												numDomainId=',CAST(@numDomainID AS VARCHAR(20)),' 
												AND ISNULL(bitEnabled,0)=1
												AND ISNULL(bitAppliesToSite,0)=1 
												AND ISNULL(bitRequireCouponCode,0)=0
												AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
												AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
												AND (1 = (CASE 
															WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													OR
													1 = (CASE 
															WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													)
											)  IsOnSale',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                                    FROM 
										#tmpPagedItems I
									LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1',
									(CASE 
										WHEN @tintPreLoginPriceLevel > 0 
										THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
										ELSE '' 
									END),(CASE 
										WHEN @numDomainID = 172
										THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
										ELSE '' 
									END),' 
									OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													WHERE  W.numItemID = I.numItemCode 
													AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
													AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
			PRINT @strSQL

			IF LEN(ISNULL(@fldList,'')) > 0
			BEGIN
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
			END

			IF LEN(ISNULL(@fldList1,'')) > 0
			BEGIN
					
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
			END
			ELSE
			BEGIN
				SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
			END

			SET  @strSQL = @strSQL +' order by Rownumber';
              
                                       
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
	
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS DECIMAL(20,5),
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS DECIMAL(20,5),
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS DECIMAL(20,5) 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC 
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
				ELSE
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

					SET @numUnits = @numUnits - @QtyShipped
                                                        
					IF @onHand >= @numUnits 
					BEGIN                                    
						SET @onHand = @onHand - @numUnits                            
						SET @onAllocation = @onAllocation + @numUnits                                    
					END                                    
					ELSE IF @onHand < @numUnits 
					BEGIN                                    
						SET @onAllocation = @onAllocation + @onHand                                    
						SET @onBackOrder = @onBackOrder + @numUnits
							- @onHand                                    
						SET @onHand = 0                                    
					END    
			                                 
					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID          
					END
				END
				
			END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits
                        - @onHand                                    
                    SET @onHand = 0                                    
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END  
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,2
			END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits 
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					RAISERROR('INVALID ON ORDER',16,1)
					RETURN
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,3
			END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			DECLARE @numDeletedReceievedQty AS NUMERIC(18,0)
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE numoppitemtcode=@numoppitemtCode
			UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtcode=@numoppitemtCode

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numOIRLID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT
			DECLARE @numTempDeletedReceievedQty FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numOIRLID = numOIRLID,
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
					@numTempDeletedReceievedQty = numDeletedReceievedQty
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description='PO Re-Open (Qty:' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - @numTempDeletedReceievedQty),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID=@numOIRLID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR) + ')'
             
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand - (@numUnits-@numDeletedReceievedQty)
                ,numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) <> -1    
				
			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) = -1    
        END
    END
            
            
            
    IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numOppChildItemID NUMERIC(18,0),
	@numOppKitChildItemID NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS FLOAT,
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty FLOAT 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID 
		AND numOppItemID=@numOppItemID 
		AND ISNULL(numOppChildItemID,0) = @numOppChildItemID
		AND ISNULL(numOppKitChildItemID,0) = @numOppKitChildItemID
		AND numItemCode=@numItemCode 
		AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty FLOAT,
			numOrgQtyRequired FLOAT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO 
			@TEMPITEM 
		SELECT 
			numChildItemID,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numWareHouseItemId,
			Item.bitAssembly 
		FROM 
			WorkOrderDetails 
		INNER JOIN 
			Item 
		ON 
			WorkOrderDetails.numChildItemID=Item.numItemCode 
		WHERE 
			numWOId = @numWOID 
			AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS FLOAT
			DECLARE @numChildOrgQtyRequired AS FLOAT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS FLOAT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS FLOAT
			DECLARE @numOnOrder AS FLOAT
			DECLARE @numOnAllocation AS FLOAT
			DECLARE @numOnBackOrder AS FLOAT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT/EDIT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF @tintMode = 4 --WO DELETE
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						--SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED, EDITED (NON COMPLETE WORK ORDER) OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1 
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS FLOAT
					DECLARE @numQtyShipped AS FLOAT

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						IF @numOppKitChildItemID > 0
						BEGIN
							SET @Description='Items Allocated For SO KIT-CHILD-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
						ELSE IF @numOppChildItemID > 0
						BEGIN
							SET @Description='Items Allocated For SO KIT-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
						ELSE
						BEGIN
							SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
					END
					ELSE
					BEGIN
						IF @numOppKitChildItemID > 0
						BEGIN
							SET @Description='Items Allocated For SO KIT-CHILD-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
						ELSE IF @numOppChildItemID > 0
						BEGIN
							SET @Description='Items Allocated For SO KIT-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
						ELSE
						BEGIN
							SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
						END
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description


							-- AUTO CREATE BACK ORDER PO IF GLOBAL SETTINGS IS CONFIGURED FOR IT
							DECLARE @bitReOrderPoint AS BIT = 0
							DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT  = 0
							DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT = 0
							DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
							DECLARE @tintOppStatus TINYINT

							SELECT 
								@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
								,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
								,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
								,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,0)
							FROM 
								Domain 
							WHERE 
								numDomainID = @numDomainID

							IF @bitReOrderPoint=1 AND (@tintOppStautsForAutoPOBackOrder = 2 OR @tintOppStautsForAutoPOBackOrder=3) --2:Purchase Opportunity, 3: Purchase Order
							BEGIN
								SET @tintOppStatus = (CASE WHEN @tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END)

								--CREATE PURCHASE ORDER/OPPORTUNITY FOR QUANTITY WHICH GOES ON BACKORDER
								EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,@tintOppStatus,0

								UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID 
							END							                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END

		UPDATE WorkOrder SET bitInventoryImpacted=1 WHERE numWOId=@numWOID
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO   
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryPickList')
DROP PROCEDURE USP_ManageInventoryPickList
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryPickList]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @bitWorkorder AS BIT
	DECLARE @numUnitHour FLOAT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @dtItemReceivedDate AS DATETIME=NULL

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numChildBackOrder AS FLOAT
	DECLARE @numChildOnHand AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numKitChildBackOrder AS FLOAT
	DECLARE @numKitChildOnHand AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempPickListItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItemID NUMERIC(18,0)
		,bitWorkorder BIT
		,bitAsset BIT
		,bitKitParent BIT
	)

	INSERT INTO @TempPickListItems
	(
		numOppItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItemID
		,bitWorkorder
		,bitAsset
		,bitKitParent
	)
	SELECT
		OI.numoppitemtCode
		,I.numItemCode
		,OBDI.numUnitHour
		,OI.numWarehouseItmsID
		,ISNULL(OI.bitWorkorder,0)
		,ISNULL(I.bitAsset,0)
		,ISNULL(I.bitKitParent,0)
	FROM
		OpportunityBizDocItems OBDI
	INNER JOIN
		OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
	INNER JOIN 
		WareHouseItems WI 
	ON 
		OI.numWarehouseItmsID=WI.numWareHouseItemID
    INNER JOIN 
		Item I 
	ON 
		OI.numItemCode = I.numItemCode
    WHERE  
		charitemtype='P'
		AND OBDI.numOppBizDocID=@numOppBizDocID
        AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OBDI.numUnitHour,0) > 0 
    ORDER BY 
		OI.numoppitemtCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TempPickListItems

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numOppItemID=numOppItemID
			,@numItemCode = numItemCode
			,@numUnitHour = numUnitHour
			,@numWareHouseItemID=numWarehouseItemID
			,@bitKitParent=bitKitParent
			,@bitWorkOrder=bitWorkorder
			,@bitAsset=bitAsset
		FROM
			@TempPickListItems
		WHERE
			ID=@i

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0),
			@onReOrder = ISNULL(numReorder,0)
		FROM 
			WareHouseItems
		WHERE
			 numWareHouseItemID = @numWareHouseItemID

		/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
		SET @description='SO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
				
		IF @bitKitParent = 1 
		BEGIN
			-- CLEAR DATA OF PREVIOUS ITERATION
			DELETE FROM @TempKitSubItems

			-- GET KIT SUB ITEMS DETAIL
			INSERT INTO @TempKitSubItems
			(
				ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				ISNULL(numOppChildItemID,0),
				ISNULL(I.numItemCode,0),
				ISNULL(I.bitKitParent,0),
				ISNULL(numWareHouseItemId,0),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numUnitHour),
				ISNULL(numQtyShipped,0)
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				Item I 
			ON 
				OKI.numChildItemID=I.numItemCode    
			WHERE 
				charitemtype='P' 
				AND ISNULL(numWareHouseItemId,0) > 0 
				AND OKI.numOppID=@numOppID 
				AND OKI.numOppItemID=@numOppItemID 

			SET @j = 1
			SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
			WHILE @j <= @ChildCount
			BEGIN
				SELECT
					@numOppChildItemID=numOppChildItemID,
					@numChildItemCode=numChildItemCode,
					@bitChildIsKit=bitChildIsKit,
					@numChildWarehouseItemID=numChildWarehouseItemID,
					@numChildQty=numChildQty,
					@numChildQtyShipped=numChildQtyShipped
				FROM
					@TempKitSubItems
				WHERE 
					ID = @j

				SELECT @numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
				IF @bitChildIsKit = 1
				BEGIN
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
					IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
					BEGIN
						-- CLEAR DATA OF PREVIOUS ITERATION
						DELETE FROM @TempKitChildKitSubItems

						-- GET SUB KIT SUB ITEMS DETAIL
						INSERT INTO @TempKitChildKitSubItems
						(
							ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							ISNULL(numOppKitChildItemID,0),
							ISNULL(OKCI.numItemID,0),
							ISNULL(OKCI.numWareHouseItemId,0),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
							ISNULL(numQtyShipped,0)
						FROM 
							OpportunityKitChildItems OKCI 
						JOIN 
							Item I 
						ON 
							OKCI.numItemID=I.numItemCode    
						WHERE 
							charitemtype='P' 
							AND ISNULL(numWareHouseItemId,0) > 0 
							AND OKCI.numOppID=@numOppID 
							AND OKCI.numOppItemID=@numOppItemID 
							AND OKCI.numOppChildItemID = @numOppChildItemID

						SET @k = 1
						SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

						WHILE @k <= @CountKitChildItems
						BEGIN
							SELECT
								@numOppKitChildItemID=numOppKitChildItemID,
								@numKitChildItemCode=numKitChildItemCode,
								@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
								@numKitChildQty=numKitChildQty,
								@numKitChildQtyShipped=numKitChildQtyShipped
							FROM
								@TempKitChildKitSubItems
							WHERE 
								ID = @k

							UPDATE
								WarehouseItems
							SET 
								numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numOnHand,0) - @numKitChildQty ELSE 0 END)
								,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numAllocation,0) + @numKitChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
								,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numKitChildQty - ISNULL(numOnHand,0)) END)
								,dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numKitChildWarehouseItemID

							SET @vcKitChildDescription = 'SO Pick List Child Kit insert/edit (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:0)'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numKitChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcKitChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 

							SET @k = @k + 1
						END
					END

					SET @description='SO Pick List Kit insert/edit (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:0)'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = 3, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomainID
				END
				ELSE
				BEGIN
					UPDATE
						WarehouseItems
					SET 
						numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numOnHand,0) - @numChildQty ELSE 0 END)
						,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numAllocation,0) + @numChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
						,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numChildQty - ISNULL(numOnHand,0)) END)
						,dtModified = GETDATE()
					WHERE
						numWareHouseItemID=@numChildWarehouseItemID

					SET @vcChildDescription = 'SO Pick List Kit insert/edit (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:0)'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 3,
						@vcDescription = @vcChildDescription,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END

				SET @j = @j + 1
			END
		END 
		ELSE IF @bitWorkOrder = 1
		BEGIN
			IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
			BEGIN
				SET @description='SO-WO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
				-- MANAGE INVENTOY OF ASSEMBLY ITEMS
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,0,0,@numItemCode,@numWareHouseItemID,0,1
			END
			ELSE
			BEGIN
				SET @description='SO-WO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
                                      
				IF @onHand >= @numUnitHour 
				BEGIN                                    
					SET @onHand = @onHand - @numUnitHour                            
					SET @onAllocation = @onAllocation + @numUnitHour                                    
				END                                    
				ELSE IF @onHand < @numUnitHour 
				BEGIN                                    
					SET @onAllocation = @onAllocation + @onHand                                    
					SET @onBackOrder = @onBackOrder + @numUnitHour - @onHand                                    
					SET @onHand = 0                                    
				END    
			                                 
				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END
		END
		ELSE
		BEGIN
			IF @onHand >= @numUnitHour 
			BEGIN                                    
				SET @onHand = @onHand - @numUnitHour                            
				SET @onAllocation = @onAllocation + @numUnitHour                                    
			END                                    
			ELSE IF @onHand < @numUnitHour 
			BEGIN                                    
				SET @onAllocation = @onAllocation + @onHand                                    
				SET @onBackOrder = @onBackOrder + @numUnitHour - @onHand                                    
				SET @onHand = 0                                    
			END    
			                                 
			IF @bitAsset=0--Not Asset
			BEGIN 
				UPDATE
					WareHouseItems
				SET
					numOnHand = @onHand
					,numAllocation = @onAllocation
					,numBackOrder = @onBackOrder
					,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID          
			END
		END  
            
		IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID
		END

		SET @i = @i + 1
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryWorkOrder')
DROP PROCEDURE USP_ManageInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryWorkOrder]
@numWOId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
		
	DECLARE @TEMPITEM AS TABLE
	(
		RowNo INT NOT NULL identity(1,1),
		numItemCode INT,
		numQty FLOAT,
		numOrgQtyRequired FLOAT,
		numWarehouseItemID NUMERIC(18,0),
		bitAssembly BIT
	)

	INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT

	SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
	IF ISNULL(@Count,0) > 0
	BEGIN
		DECLARE @Description AS VARCHAR(1000)
		DECLARE @numChildItemCode AS NUMERIC(18,0)
		DECLARE @numChildItemQty AS FLOAT
		DECLARE @numChildOrgQtyRequired AS FLOAT
		DECLARE @bitChildIsAssembly AS BIT
		DECLARE @numQuantityToBuild AS FLOAT
		DECLARE @numChildItemWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numOnHand AS FLOAT
		DECLARE @numOnOrder AS FLOAT
		DECLARE @numOnAllocation AS FLOAT
		DECLARE @numOnBackOrder AS FLOAT

		WHILE @i <= @Count
		BEGIN
			DECLARE @numNewOppID AS NUMERIC(18,0) = 0
			DECLARE @QtyToBuild AS FLOAT
			DECLARE @numQtyShipped AS FLOAT

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
			SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
			SELECT  
				@numOnHand = ISNULL(numOnHand, 0),
				@numOnAllocation = ISNULL(numAllocation, 0),
				@numOnOrder = ISNULL(numOnOrder, 0),
				@numOnBackOrder = ISNULL(numBackOrder, 0)
			FROM    
				WareHouseItems
			WHERE   
				numWareHouseItemID = @numChildItemWarehouseItemID	

			SET @Description='Items Allocated For Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ')'
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
			IF ISNULL(@bitChildIsAssembly,0) = 1
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN      
					SET @numOnHand = @numOnHand - @numChildItemQty
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                                                         
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN      
					SET @numOnAllocation = @numOnAllocation + @numOnHand
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
					SET @QtyToBuild = (@numChildItemQty - @numOnHand)
					SET @numQtyShipped = 0
					SET @numOnHand = 0   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					EXEC USP_WorkOrder_InsertRecursive 0,0,0,0,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,1
				END 
			END
			ELSE
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN                                    
					SET @numOnHand = @numOnHand - @numChildItemQty                            
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                             
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN     
								
					SET @numOnAllocation = @numOnAllocation + @numOnHand  
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
					SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
					SET @numOnHand = 0 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					-- AUTO CREATE BACK ORDER PO IF GLOBAL SETTINGS IS CONFIGURED FOR IT
					DECLARE @bitReOrderPoint AS BIT = 0
					DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT  = 0
					DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT = 0
					DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
					DECLARE @tintOppStatus TINYINT

					SELECT 
						@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
						,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
						,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
						,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,0)
					FROM 
						Domain 
					WHERE 
						numDomainID = @numDomainID

					IF @bitReOrderPoint=1 AND (@tintOppStautsForAutoPOBackOrder = 2 OR @tintOppStautsForAutoPOBackOrder=3) --2:Purchase Opportunity, 3: Purchase Order
					BEGIN
						SET @tintOppStatus = (CASE WHEN @tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END)

						--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
						EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,@tintOppStatus,@numReOrderPointOrderStatus

						UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID
					END
				END 
			END
					
			SET @i = @i + 1
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P')
			  BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
				
			  END

				-- This is common check for CharItemType P and Other
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_COGS_ACCOUNT',16,1)
					RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
					RETURN
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(18,0)=0,
@numOppID AS numeric(18,0)=0,
@strItems AS TEXT,
@numUserCntID AS numeric(9)=0
AS
BEGIN
	DECLARE @hDocItem int                                                                                                                            

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppId)=0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems  
  
		DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
  
		DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(18,0), @numWarehouseID NUMERIC(18,0)

		DECLARE @numoppitemtCode NUMERIC(18,0)
		DECLARE @numOppChildItemID NUMERIC(18,0)
		DECLARE @numOppKitChildItemID NUMERIC(18,0)
  
		DECLARE WOCursor CURSOR FOR 
		SELECT 
			numoppitemtCode
			,0 AS numOppChildItemID
			,0 AS numOppKitChildItemID
			,numItemCode
			,numUnitHour
			,numWarehouseItmsID
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM 
			OpportunityItems                                                                         
		WHERE 
			numOppID=@numOppID
			AND ISNULL(bitWorkOrder,0)=1
		UNION ALL
		SELECT
			numOppItemID
			,numOppChildItemID
			,0 AS numOppKitChildItemID
			,Item.numItemCode
			,OKI.numQtyItemsReq
			,OKI.numWareHouseItemId
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item 
		ON
			OKI.numChildItemID = Item.numItemCode
		WHERE 
			OKI.numOppID=@numOppID
			AND ISNULL(Item.bitAssembly,0)=1
			AND ISNULL(OKI.numWareHouseItemId,0) > 0
		UNION ALL
		SELECT
			numOppItemID
			,OKCI.numOppChildItemID
			,OKCI.numOppKitChildItemID
			,Item.numItemCode
			,OKCI.numQtyItemsReq
			,OKCI.numWareHouseItemId
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item 
		ON
			OKCI.numItemID = Item.numItemCode
		WHERE 
			OKCI.numOppID=@numOppID
			AND ISNULL(Item.bitAssembly,0)=1
			AND ISNULL(OKCI.numWareHouseItemId,0) > 0	

		OPEN WOCursor;

		FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

		WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

			IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND ISNULL((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0) = 0)
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				RETURN
			END


			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@bintCompliationDate
				,@numWOAssignedTo
				,@numoppitemtCode
				,@numOppChildItemID
				,@numOppKitChildItemID
			)
	
			SET @numWOId = SCOPE_IDENTITY()
	

			IF @numWOAssignedTo>0
			BEGIN
				DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
				SELECT 
					@vcItemName=ISNULL(vcItemName,'') 
				FROM 
					Item 
				WHERE 
					numItemCode=@numItemCode

				SET @vcItemName= 'Work Order for Item : ' + @vcItemName

				SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

				EXEC dbo.usp_InsertCommunication
					@numCommId = 0, --  numeric(18, 0)
					@bitTask = 972, --  numeric(18, 0)
					@numContactId = @numUserCntID, --  numeric(18, 0)
					@numDivisionId = @numDivisionId, --  numeric(18, 0)
					@txtDetails = @vcItemName, --  text
					@numOppId = 0, --  numeric(18, 0)
					@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(18, 0)
					@numDomainId = @numDomainID, --  numeric(18, 0)
					@bitClosed = 0, --  tinyint
					@vcCalendarName = '', --  varchar(100)
					@dtStartTime = @bintCompliationDate, --  datetime
					@dtEndtime = @bintCompliationDate, --  datetime
					@numActivity = 0, --  numeric(9, 0)
					@numStatus = 0, --  numeric(9, 0)
					@intSnoozeMins = 0, --  int
					@tintSnoozeStatus = 0, --  tinyint
					@intRemainderMins = 0, --  int
					@tintRemStaus = 0, --  tinyint
					@ClientTimeZoneOffset = 0, --  int
					@bitOutLook = 0, --  tinyint
					@bitSendEmailTemplate = 0, --  bit
					@bitAlert = 0, --  bit
					@numEmailTemplate = 0, --  numeric(9, 0)
					@tintHours = 0, --  tinyint
					@CaseID = 0, --  numeric(18, 0)
					@CaseTimeId = 0, --  numeric(18, 0)
					@CaseExpId = 0, --  numeric(18, 0)
					@ActivityId = 0, --  numeric(18, 0)
					@bitFollowUpAnyTime = 0, --  bit
					@strAttendee=''
			END

			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
	
			FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numOppChildItemID,@numOppKitChildItemID,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
		END

		CLOSE WOCursor;
		DEALLOCATE WOCursor;

		EXEC sp_xml_removedocument @hDocItem     
	END 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo 
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @strSQL AS VARCHAR(MAX)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)

                
        SELECT  @DivisionID = numDivisionID,
                @tintOppType = tintOpptype
        FROM    OpportunityMaster
        WHERE   numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
			vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
			numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
			bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int
			,vcFieldDataType CHAR(1)
		)

		IF
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN	

		INSERT INTO #tempForm
		select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
		,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
		 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
		 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
		 UNION
    
			select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
		 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
		,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		 from View_DynamicCustomColumns
		 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
		 AND ISNULL(bitCustom,0)=1

		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END


SET @strSQL = ''

DECLARE @avgCost INT
DECLARE @tintCommitAllocation TINYINT
SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(OM.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	   
	   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails')
	   BEGIN
			SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
	   END     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueOppItems('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numoppitemtCode,Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm
WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CT')
DROP PROCEDURE USP_OpportunityBizDocs_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thMay2014>
-- Description:	<Description,,Get only Order type :Sales /or Purchase>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityBizDocs_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
 -- Insert statements for procedure here
	declare @tblFields as table 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	Declare @Type char(1)
	Declare @ChangeVersion bigint
	Declare @CreationVersion bigint
	Declare @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.OpportunityBizDocs, 0) AS CT
	WHERE 
		numOppBizDocsId=@numRecordID

	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID
	END
	ELSE--UPDATE
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     
	
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    vcBizDocID varchar(70),
				monAmountPaid varchar(70),
				numBizDocStatus varchar(70),
				numBizDocId varchar(70),
				monDealAmount varchar(70),
				numModifiedBy varchar(70),
				numCreatedBy varchar(70),
				bitRecurred varchar(70)
			)

		INSERT INTO @UFFields
		(
			vcBizDocID
			,monAmountPaid
			,numBizDocStatus
			,numBizDocId
			,monDealAmount
			,numModifiedBy
			,numCreatedBy
			,bitRecurred
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'vcBizDocID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcBizDocID ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monAmountPaid', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monAmountPaid, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocId, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount	,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numModifiedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred 
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppBizDocsId=@numRecordID


		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName
			,FieldValue
		FROM
		(
			SELECT  vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred
			FROM @UFFields	
		) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred)
		) AS upv
		WHERE 
			FieldValue<>0
	END

 select @Type
DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

SELECT @Columns_Updated

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 71, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetItemsToReturn')
DROP PROCEDURE USP_OpportunityBizDocs_GetItemsToReturn
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityBizDocs_GetItemsToReturn]                                                                                                                                                
(           
	@numDomainID NUMERIC(18,0),                                                                                                                                     
	@numOppID NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)                                                              
)                                                                                                                                        
AS                           
BEGIN
	SELECT 
		OBDI.numOppBizDocItemID,
		OI.numoppitemtCode,
        OI.numOppId,
		OI.vcNotes as txtNotes,
        OI.numItemCode,
		OI.bitItemPriceApprovalRequired,
		OBDI.numUnitHour AS numOrigUnitHour,
        (OBDI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID=OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND 
		numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
		WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
		AS numUnitHour,
        OI.monPrice,
        OI.monTotAmount,
        OI.numSourceID,
        OI.vcItemDesc,
		OI.numCost,
        OI.numWarehouseItmsID,
        OI.vcType ItemType,
        OI.vcAttributes Attributes,
        OI.bitDropShip DropShip,
        OI.numUnitHourReceived,
        OI.bitDiscountType,
        OI.fltDiscount,
		OI.bitMarkupDiscount,
        OI.monTotAmtBefDiscount,
        OI.numQtyShipped,
        OI.vcItemName,
        OI.vcModelID,
        OI.vcPathForTImage,
        OI.vcManufacturer,
        OI.monVendorCost,
        ISNULL(OI.numUOMId, 0) AS numUOM,
        ISNULL(U.vcUnitName, '') vcUOMName,
        ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                OM.numDomainId, NULL),1) AS UOMConversionFactor,
        OI.bitWorkOrder,
        isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
        OI.numShipmentMethod,
        OI.numSOVendorId,
        OI.numProjectID,
        OI.numClassID,
        OI.numProjectStageID,
        OI.numToWarehouseItemID,
        UPPER(I.charItemType) charItemType,
        WItems.numWarehouseID,
        W.vcWareHouse AS Warehouse,
        0 as Op_Flag,
		CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                        OM.numDomainId, ISNULL(OI.numUOMId, 0))
			* OBDI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
		isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
		CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
		AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
		ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
		ISNULL(I.fltLength,0) AS [fltLength],
		ISNULL(I.fltWidth,0) AS [fltWidth],
		ISNULL(I.fltHeight,0) AS [fltHeight],
		ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
		(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
		ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
		ISNULL(I.txtItemDesc,'') txtItemDesc,
		ISNULL(I.vcModelID,'') vcModelID,
		ISNULL(I.vcManufacturer,'') vcManufacturer,
		ISNULL(I.numBarCodeId,'') numBarCodeId,
		ISNULL(I.bitSerialized,0) bitSerialized,
		ISNULL(I.bitKitParent,0) bitKitParent,
		ISNULL(I.bitAssembly,0) bitAssembly,
		ISNULL(I.IsArchieve,0) IsArchieve,
		ISNULL(I.bitLotNo,0) bitLotNo,
		ISNULL(WItems.monWListPrice,0) monWListPrice,
		ISNULL(I.bitTaxable, 0) bitTaxable,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
		case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
		ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
		ISNULL(OI.vcAttrValues,'') AS AttributeIDs
		,(CASE WHEN OM.tintOppType=2 THEN ISNULL(OI.vcNotes,'') ELSE '' END) AS vcVendorNotes
    FROM
		OpportunityBizDocItems OBDI
	INNER JOIN   
		dbo.OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE
		OI.numOppId = @numOppID
		AND OBDI.numOppBizDocID=@numOppBizDocID
		AND OM.numDomainId = @numDomainId
		AND (OBDI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID=OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
	SELECT  vcSerialNo,
			vcComments AS Comments,
			numOppItemID AS numoppitemtCode,
			W.numWarehouseItmsDTLID,
			numWarehouseItmsID AS numWItmsID,
			dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
	FROM    WareHouseItmsDTL W
			JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
	WHERE   
		numOppID = @numOppID 
		AND numOppItemID IN (SELECT 
								OI.numoppitemtCode 
							FROM 
								OpportunityBizDocItems OBDI
							INNER JOIN   
								dbo.OpportunityItems OI
							ON
								OBDI.numOppItemID = OI.numoppitemtCode 
							WHERE 
								OI.numOppId = @numOppID 
								AND (OBDI.numUnitHour-ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																ReturnItems 
															WHERE 
																numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId=@numOppID) 
																AND numOppItemID=OI.numoppitemtCode),0))>0
							)
END
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CT')
DROP PROCEDURE USP_OpportunityMaster_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityMaster_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.OpportunityMaster, 0) AS CT
WHERE numOppId=@numRecordID

IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

End
Else--Update
Begin

SET @last_synchronization_version=@ChangeVersion-1
        

        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

		
		
		--Fields Update:
			Declare @UFFields as table 
			(
					tintActive varchar(70),
					monPAmount varchar(70),
					numAssignedBy varchar(70),
					numAssignedTo varchar(70),
					numCampainID varchar(70),
					bintAccountClosingDate varchar(70),
					txtComments varchar(70),
					lngPConclAnalysis varchar(70),
					bintCreatedDate varchar(70),
					vcOppRefOrderNo varchar(70),
					tintOppStatus varchar(70),
					intpEstimatedCloseDate varchar(70),
					--monDealAmount varchar(70),
					vcMarketplaceOrderID varchar(70),
					tintOppType varchar(70),
					numStatus varchar(70),
					monDealAmount varchar(70),
					numRecOwner varchar(70),
					vcPoppName varchar(70),
					numSalesOrPurType varchar(70),
					tintSource varchar(70),
					bitRecurred varchar(70),
					numPartner varchar(70),
					numPartenerContact varchar(70),
					dtReleaseDate varchar(70),
					numReleaseStatus varchar(70),
					intUsedShippingCompany varchar(70),
					numShipmentMethod varchar(70),
					numShippingService varchar(70),
					numPercentageComplete varchar(70),
					bintClosedDate varchar(70)
			)

		INSERT into @UFFields
		(
			tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
			,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,
			numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete,bintClosedDate
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintActive, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monPAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monPAmount, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numCampainID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampainID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintAccountClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintAccountClosingDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'lngPConclAnalysis', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS lngPConclAnalysis, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcOppRefOrderNo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcOppRefOrderNo, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intpEstimatedCloseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intpEstimatedCloseDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcMarketplaceOrderID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcMarketplaceOrderID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPoppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPoppName, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numSalesOrPurType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numSalesOrPurType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintSource', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintSource ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartner,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartenerContact', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartenerContact,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'dtReleaseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtReleaseDate,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numReleaseStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numReleaseStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intUsedShippingCompany', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intUsedShippingCompany,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numShipmentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numShipmentMethod,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numShippingService', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numShippingService,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPercentageComplete', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPercentageComplete,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintClosedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintClosedDate
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppId=@numRecordID

		--exec USP_GetWorkFlowFormFieldMaster 1,70
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT 
					tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
					,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred
					,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete)
		) AS upv
		where FieldValue<>0
 

End

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END


--DateField WorkFlow Logic
	
	Declare @TempnumOppId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID)
		BEGIN
			SET @TempnumOppId = (SELECT numOppId FROM [OpportunityMaster] OM WHERE  (
					(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND numOppId=@numRecordID)

			IF(@TempnumOppId IS NOT NULL)
			BEGIN	
				UPDATE OMT 
				SET 
						OMT.bintCreatedDate = OM.bintCreatedDate
						,OMT.bintClosedDate = OM.bintClosedDate
						,OMT.intPEstimatedCloseDate = OM.intPEstimatedCloseDate
						,OMT.dtReleaseDate = OM.dtReleaseDate
				FROM
					OpportunityMaster_TempDateFields AS OMT
					INNER JOIN OpportunityMaster AS OM
						ON OMT.numOppId = OM.numOppId
				WHERE  (
					(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND 
					OMT.numOppId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [OpportunityMaster_TempDateFields] (numOppId, numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate) 
			SELECT numOppId,numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate
			FROM OpportunityMaster
			WHERE (
					(bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND numOppId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumOppId = (SELECT numOppId FROM [OpportunityMaster] OM WHERE  (
				(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				 OR
				(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )			
			AND numOppId=@numRecordID)

		IF(@TempnumOppId IS NOT NULL)
		BEGIN	
			UPDATE OMT 
			SET 
					OMT.bintCreatedDate = OM.bintCreatedDate
					,OMT.bintClosedDate = OM.bintClosedDate
					,OMT.intPEstimatedCloseDate = OM.intPEstimatedCloseDate
					,OMT.dtReleaseDate = OM.dtReleaseDate
			FROM
				OpportunityMaster_TempDateFields AS OMT
				INNER JOIN OpportunityMaster AS OM
					ON OMT.numOppId = OM.numOppId
			WHERE  (
				(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				 OR
				(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )			
			AND 
				OMT.numOppId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
	END	

	--DateField WorkFlow Logic


--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
IF(	@Columns_Updated='monDealAmount')
BEGIN

set @tintWFTriggerOn=1
END

--select  @Columns_Updated
--select @tintWFTriggerOn
--select * from @UFFields
--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 70, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,Opp.vcType ItemType
									,Opp.vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,I.fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Organization_CT')
DROP PROCEDURE USP_Organization_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_Organization_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	-- Insert statements for procedure here
	DECLARE @tblFields TABLE 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	DECLARE @Type char(1)
	DECLARE @ChangeVersion bigint
	DECLARE @CreationVersion bigint
	DECLARE @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version
		,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, 0) AS CT
	WHERE 
		numDivisionID=@numRecordID


	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
			SELECT 
				@Type = CT.SYS_CHANGE_OPERATION
			FROM 
				CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
			WHERE  
				CT.numDivisionID=@numRecordID
	END
	ELSE --Update
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
			CT.numDivisionID=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    bitActiveInActive varchar(70),
				numAssignedBy varchar(70),
				numAssignedTo varchar(70),
				numCampaignID varchar(70),
				numCompanyDiff varchar(70),
				vcCompanyDiff varchar(70), 
				numCurrencyID varchar(70),
				numFollowUpStatus1 varchar(70),
				numFollowUpStatus varchar(70),
				numGrpID varchar(70),
				bintCreatedDate varchar(70),
				vcComFax varchar(70),
				bitPublicFlag varchar(70),
				tintCRMType varchar(70),
				bitNoTax varchar(70),
				numTerID varchar(70),
				numStatusID varchar(70),
				vcComPhone varchar(70),
				numAccountClassID varchar(70),
				numBillingDays VARCHAR(70),
				tintPriceLevel VARCHAR(70),
				numDefaultPaymentMethod VARCHAR(70),
				intShippingCompany VARCHAR(70),
				numDefaultShippingServiceID VARCHAR(70)
			)

		INSERT into @UFFields
		(
			bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
			,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany,
			numDefaultShippingServiceID
		)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitActiveInActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitActiveInActive ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCampaignID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampaignID, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCompanyDiff	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  vcCompanyDiff ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCurrencyID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCurrencyID, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus1', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus1,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numGrpID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numGrpID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComFax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComFax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitPublicFlag', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPublicFlag,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintCRMType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintCRMType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitNoTax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitNoTax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numTerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numStatusID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatusID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComPhone,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAccountClassID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAccountClassID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numBillingDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBillingDays,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintPriceLevel', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPriceLevel,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultPaymentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultPaymentMethod,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'intShippingCompany', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intShippingCompany,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultShippingServiceID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultShippingServiceID
		FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numDivisionID=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,68
		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName,
			FieldValue
		FROM
			(
				SELECT 
					bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
					,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany
					,numDefaultShippingServiceID
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus
			,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel
			,numDefaultPaymentMethod,intShippingCompany,numDefaultShippingServiceID)
		) AS upv
		WHERE 
			FieldValue<>0
	END

	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

	--DateField WorkFlow Logic

	Declare @TempbintCreatedDate AS VARCHAR(70)
	SET @TempbintCreatedDate = (SELECT bintCreatedDate FROM DivisionMaster WHERE numDivisionID=@numRecordID)	
	
	Declare @TempnumDivisionId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numDivisionID FROM DivisionMaster_TempDateFields WHERE numDivisionID=@numRecordID)
		BEGIN
			IF (@TempbintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND @TempbintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
			BEGIN
				UPDATE DivisionMaster_TempDateFields
				SET bintCreatedDate = @TempbintCreatedDate
				WHERE numDivisionID = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
			END
		END
		ELSE
		BEGIN
			INSERT INTO [DivisionMaster_TempDateFields] (numDivisionID, numDomainID, bintCreatedDate) 
			SELECT numDivisionID,numDomainID, bintCreatedDate
			FROM DivisionMaster
			WHERE (
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )
				AND numDivisionID=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumDivisionId = (SELECT numDivisionID FROM DivisionMaster_TempDateFields WHERE numDivisionID=@numRecordID)

		IF(@TempnumDivisionId IS NOT NULL)
		BEGIN
			IF (@TempbintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND @TempbintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
			BEGIN
				UPDATE DivisionMaster_TempDateFields
				SET bintCreatedDate = @TempbintCreatedDate
				WHERE numDivisionID = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
			END
		END
	END

	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
	END	

	--DateField WorkFlow Logic

	Declare @numAssignedTo numeric(18,0)
	Declare @numAssignedBy numeric(18,0)
	Declare @numWebLead numeric(18,0)
	Declare @bitLeadBoxFlg bit

	Select @numAssignedTo=numAssignedTo,@numAssignedBy=numAssignedBy,@numWebLead=numGrpId,@bitLeadBoxFlg=bitLeadBoxFlg from DivisionMaster where numDivisionID=@numRecordID

	--For Fields Update Exection Point
	DECLARE @Columns_Updated VARCHAR(1000)
	SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
	SET @Columns_Updated=ISNULL(@Columns_Updated,'')

	select @Columns_Updated,@numAssignedTo,@numAssignedBy,@numWebLead
	IF (@Columns_Updated='numAssignedBy,numAssignedTo')
	BEGIN	
		IF (@numWebLead=1) --web Leads
		BEGIN
			IF	(@bitLeadBoxFlg=1) --(through System)
			BEGIN
				IF(@numAssignedTo=0 AND @numAssignedBy=0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
			ELSE		--(through Web)			
			BEGIN
				IF(@numAssignedTo>0 AND @numAssignedBy>0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
		END

		ELSE    --Normal Leads
		BEGIN
			IF	(@numAssignedTo=0 AND @numAssignedBy=0)
			BEGIN
				SET @tintWFTriggerOn=1
			END
			ELSE
			BEGIN
				SET @tintWFTriggerOn=2
			END
		END
	END

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 68, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END


GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
-- exec USP_ProjectOpp @byteMode=1,@domainId=72,@numDivisionID=15914,@numProjectId=197,@numCaseId=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectopp')
DROP PROCEDURE usp_projectopp
GO
CREATE PROCEDURE [dbo].[USP_ProjectOpp]        
	@byteMode as tinyint=0,      
	@domainId as numeric(9)=0,    
	@numDivisionID as numeric(9)=0 ,       
	@numProjectId  as numeric(9)=0  ,
	@numCaseId  as numeric(9)=0 ,
	@numStageID	AS NUMERIC(9)=0,
	@tintOppType as tinyint=0
as        
BEGIN        
	IF @byteMode=0       
	BEGIN      
		SELECT 
			numOppId
			,vcPOppName         
		FROM 
			OpportunityMaster 
		WHERE 
			tintActive=1 
			AND tintShipped=0 
			AND numDivisionID=@numDivisionID 
			AND numDomainId=@domainId 
		ORDER BY 
			numOppId DESC        
	END           
      
	IF @byteMode=1      
	BEGIN 
		If (@numProjectId = 0 and @numCaseId<>0)
		BEGIN
			SELECT 
				numOppId
				,vcPOppName      
			FROM 
				OpportunityMaster     
			WHERE 
				numDomainId=@domainId 
				AND numDivisionID=@numDivisionID  
				AND ((tintActive=1 AND tintShipped=0) OR numOppId IN (SELECT numOppId from CaseOpportunities where numCaseId=@numCaseId and numDomainId=@domainId))  
			ORDER BY 
				numOppId DESC
		END
		ELSE
		BEGIN
			SELECT 
				OM.numOppId
				,0 as numBillID
				,0 as numProjOppID
				,OM.vcPOppName
				,I.[vcItemName]
				,'Purchase' AS [Type]
				,(OPP.[monPrice] * OPP.[numUnitHour]) AS ExpenseAmount
				,0 as bitIntegratedToAcnt
				,0 as numBizDocsPaymentDetailsId
				,COA.vcAccountName as ExpenseAccount
			FROM 
				[OpportunityMaster] OM 
			INNER JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
            LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
			WHERE 
				OM.numDomainId=@domainId  
					AND OPP.numProjectID = @numProjectId
					AND (OPP.numProjectStageID = @numStageID OR @numStageID =0)
					AND OM.[tintOppType] = 2
			UNION ALL
			SELECT 
			 0 as numOppId,PO.numBillID,PO.numProjOppID,'','','Bill' as [Type],
			 BH.monAmountDue AS ExpenseAmount,
			ISNULL((SELECT bitIntegrated FROM dbo.OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId),0) AS bitIntegratedToAcnt,
			(SELECT numBizDocsPaymentDetailsId FROM dbo.OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId) AS numBizDocsPaymentDetailsId,
			'' as ExpenseAccount
			 from  ProjectsOpportunities PO 
				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
			 where  PO.numDomainId=@domainId  
					AND PO.numProID = @numProjectId
					AND (PO.numStageID = @numStageID OR @numStageID =0)
					and isnull(PO.numBillId,0)>0
			 order by numOppId desc
		END     
	END

	IF @byteMode= 2
	BEGIN
	 select  OB.[vcBizDocID],OB.[numOppBizDocsId],CASE WHEN OB.vcBizDocName IS NULL THEN OB.vcBizDocID WHEN LEN(OB.vcBizDocID)>0 THEN OB.vcBizDocID ELSE OB.vcBizDocName END AS vcBizDocName
	 from OpportunityMaster OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId] where 
	 OM.tintOppType =1 AND 
	 OB.[bitAuthoritativeBizDocs]=1 AND tintActive=1 and  tintShipped=0 and numDivisionID=@numDivisionID and numDomainId=@domainId order by OB.numOppId desc

	END 

	IF @byteMode= 3 --Select only sales/purchase opportunity 
	BEGIN
	   select   numOppId,
				vcPOppName
	   from     OpportunityMaster
	   where    tintActive = 1
				and tintShipped = 0
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
	   order by numOppId desc        

	END 

	IF @byteMode= 4 --Select only closed orders which are not returned yet.
	BEGIN
	   select   numOppId,
				vcPOppName
	   from     OpportunityMaster
	   where    tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
				--AND numOppId NOT IN (SELECT numOppId FROM dbo.ReturnHeader WHERE numDomainId=@domainId)
	   order by numOppId desc        
	END 

	IF @byteMode= 5 --Select Fulfillment BizDocs FROM closed orders which are not returned yet.
	BEGIN
		IF @tintOppType = 2
		BEGIN
			SELECT
				CONCAT(OpportunityMaster.numOppID,'-0') numOppBizDocsId
				,vcPOppName
			FROM
				OpportunityMaster
			WHERE
				tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
			ORDER BY 
				OpportunityMaster.numOppId desc
		END
		ELSE
		BEGIN
			SELECT
				CONCAT(OpportunityMaster.numOppID,'-',numOppBizDocsId) numOppBizDocsId
				,CONCAT(vcBizDocID,' (',vcPOppName,')') vcPOppName
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityBizDocs
			ON
				OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			WHERE
				tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
				AND numBizDocId = 296
		   ORDER BY 
				OpportunityMaster.numOppId desc
		END
	END 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Projects_CT')
DROP PROCEDURE USP_Projects_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,29thJuly2014>
-- Description:	<Description,, to Track Row changes in Projects Table>
-- =============================================
Create PROCEDURE [dbo].[USP_Projects_CT]
-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null
DECLARE @Columns_Updated VARCHAR(1000)

		SELECT 			
			@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE numProId=@numRecordID

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE  CT.numProId=@numRecordID


IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE  CT.numProId=@numRecordID
END
ELSE
BEGIN




SET @last_synchronization_version=@ChangeVersion-1     
		
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, @last_synchronization_version) AS CT
		WHERE  CT.numProId=@numRecordID
	
		--Fields Up date & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			numAssignedBy varchar(70),
			numAssignedTo varchar(70),
			bintProClosingDate varchar(70),
			txtComments varchar(70),
			numContractId varchar(70),
			numCreatedBy varchar(70),
			bintCreatedDate varchar(70),
			numCustPrjMgr varchar(70),
			intDueDate varchar(70),
			numintPrjMgr varchar(70),
			numOppId varchar(70),
			vcProjectID varchar(70),
			vcProjectName varchar(70),
			numProjectStatus varchar(70),
			numProjectType varchar(70),
			numRecOwner varchar(70),
			numAccountID varchar(70)
			
			)

	INSERT into @UFFields(numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'bintProClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintProClosingDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numContractId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numContractId	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numCreatedBy ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numCustPrjMgr', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCustPrjMgr,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'intDueDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intDueDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numintPrjMgr', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numintPrjMgr,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numOppId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppId,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'vcProjectID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcProjectID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'vcProjectName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcProjectName,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numProjectStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numProjectStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numProjectType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numProjectType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAccountID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAccountID
		
		FROM 
		CHANGETABLE(CHANGES dbo.ProjectsMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numProId=@numRecordID
    --sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,73
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID)
		) AS upv
		where FieldValue<>0
 
END


SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
select @Columns_Updated

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--DateField WorkFlow Logic
	
	Declare @TempnumProId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN		
		IF EXISTS (SELECT numProId FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID)
		BEGIN
			SET @TempnumProId = (SELECT PM.numProId FROM ProjectsMaster PM WHERE  (			
				(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND PM.numProId=@numRecordID)

			IF(@TempnumProId IS NOT NULL)
			BEGIN	
				UPDATE PMT 
				SET 
						PMT.bintCreatedDate = PM.bintCreatedDate 
						,PMT.bintProClosingDate =PM.bintProClosingDate
						,PMT.intDueDate = PM.intDueDate
					
				FROM
					[ProjectsMaster_TempDateFields] AS PMT
					INNER JOIN ProjectsMaster AS PM
						ON PMT.numProId = PM.numProId
				WHERE  (
						(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR	
						(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR
						(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
						)			
					AND 
						PMT.numProId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [ProjectsMaster_TempDateFields] (numProId, numDomainID, bintCreatedDate, bintProClosingDate,intDueDate) 
			SELECT numProId,numDomainID, bintCreatedDate, bintProClosingDate, intDueDate
			FROM ProjectsMaster
			WHERE (
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )

				AND numProId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumProId = (SELECT PM.numProId FROM ProjectsMaster PM WHERE  (			
				(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND PM.numProId=@numRecordID)

		IF(@TempnumProId IS NOT NULL)
		BEGIN	
			UPDATE PMT 
			SET 
					PMT.bintCreatedDate = PM.bintCreatedDate 
					,PMT.bintProClosingDate =PM.bintProClosingDate
					,PMT.intDueDate = PM.intDueDate
					
			FROM
				[ProjectsMaster_TempDateFields] AS PMT
				INNER JOIN ProjectsMaster AS PM
					ON PMT.numProId = PM.numProId
			WHERE  (
					(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
					)			
				AND 
					PMT.numProId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
	END	

	--DateField WorkFlow Logic
	
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 73, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @ClientTimeZoneOffset as int, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF ISNULL(@numCurrentPage,0) > 0 AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY;')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savebackorderauthgroup')
DROP PROCEDURE usp_savebackorderauthgroup
GO
CREATE PROCEDURE [dbo].[USP_SaveBackOrderAuthGroup] 

@numDomainID AS NUMERIC(9)=0,
@strAuthGroupID AS text=''

AS


DELETE from AuthenticationGroupBackOrder WHERE numDomainID=@numDomainID 

CREATE TABLE Tempdata
(
    numDomainID NUMERIC,
    strGroupID VARCHAR(MAX)
)

INSERT Tempdata SELECT @numDomainID, @strAuthGroupID

;WITH tmp(numDomainID, numGroupID, GroupID) AS
(
    SELECT
        numDomainID,
        LEFT(strGroupID, CHARINDEX(',', strGroupID + ',') - 1),
        STUFF(strGroupID, 1, CHARINDEX(',', strGroupID + ','), '')
    FROM Tempdata
    UNION all

    SELECT
        numDomainID,
        LEFT(GroupID, CHARINDEX(',', GroupID + ',') - 1),
        STUFF(GroupID, 1, CHARINDEX(',', GroupID + ','), '')
    FROM tmp
    WHERE
        GroupID > ''
)


Insert into AuthenticationGroupBackOrder
SELECT
    numDomainID,    
    numGroupID
FROM tmp

	DROP TABLE Tempdata
                                
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_CT')
DROP PROCEDURE USP_StagePercentageDetails_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thMay2014>
-- Description:	<Description,,Get Changes in Stage Percentage details.used in WFA>
-- =============================================
Create PROCEDURE [dbo].[USP_StagePercentageDetails_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.StagePercentageDetails, 0) AS CT
WHERE numStageDetailsId=@numRecordID

IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE  CT.numStageDetailsId=@numRecordID

End

Else
Begin

SET @last_synchronization_version=@ChangeVersion-1     
	
	  SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE  CT.numStageDetailsId=@numRecordID


		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			   	numStageDetailsId varchar(70),
				dtStartDate varchar(70),
				dtEndDate varchar(70),
				tintPercentage varchar(70),
				tinProgressPercentage varchar(70),
				numAssignTo varchar(70),
				intDueDays varchar(70),
				numModifiedBy varchar(70),
				numCreatedBy varchar(70)

			)


	INSERT into @UFFields(numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numStageDetailsId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStageDetailsId ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtStartDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtStartDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtEndDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtEndDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tintPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPercentage, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tinProgressPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tinProgressPercentage	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numAssignTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numAssignTo ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'intDueDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intDueDays,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numModifiedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy  
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE 
		ct.numStageDetailsId=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,90
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
		) AS upv
		where FieldValue<>0
 

End

--select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
-- If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
-- Begin
-- set @Type='I'
-- End
-- Else
-- Begin
-- set @Type='U'
-- End
-- select @Type
DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--DateField WorkFlow Logic
	
	Declare @TempnumStageDetailsId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN		
		IF EXISTS (SELECT numStageDetailsId FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId=@numRecordID)
		BEGIN
			SET @TempnumStageDetailsId = (SELECT SP.numStageDetailsId FROM StagePercentageDetails SP WHERE  (			
				(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND SP.numStageDetailsId=@numRecordID)

			IF(@TempnumStageDetailsId IS NOT NULL)
			BEGIN	
				UPDATE SPT 
				SET 
						SPT.dtEndDate  = SP.dtEndDate 
						,SPT.dtStartDate  =SP.dtStartDate 					
				FROM
					[StagePercentageDetails_TempDateFields] AS SPT
					INNER JOIN StagePercentageDetails AS SP
						ON SPT.numStageDetailsId = SP.numStageDetailsId
				WHERE  (
						(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR	
						(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
						)			
					AND 
						SPT.numStageDetailsId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [StagePercentageDetails_TempDateFields] (numStageDetailsId, numDomainID, dtEndDate, dtStartDate) 
			SELECT numStageDetailsId,numDomainID, dtEndDate, dtStartDate
			FROM StagePercentageDetails
			WHERE (
					(dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )
				AND numStageDetailsId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumStageDetailsId = (SELECT SP.numStageDetailsId FROM StagePercentageDetails SP WHERE  (			
				(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND SP.numStageDetailsId=@numRecordID)

		IF(@TempnumStageDetailsId IS NOT NULL)
		BEGIN	
			UPDATE SPT 
			SET 
					SPT.dtEndDate  = SP.dtEndDate 
					,SPT.dtStartDate  =SP.dtStartDate 					
			FROM
				[StagePercentageDetails_TempDateFields] AS SPT
				INNER JOIN StagePercentageDetails AS SP
					ON SPT.numStageDetailsId = SP.numStageDetailsId
			WHERE  (
					(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
					)			
				AND 
					SPT.numStageDetailsId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
	END	

	--DateField WorkFlow Logic


--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

SELECT @Columns_Updated
select @tintWFTriggerOn

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 94, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END
	





/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs,
 numAuthorizePercentage=@numAuthorizePercentage,
 bitEDI=ISNULL(@bitEDI,0),
 bit3PL=ISNULL(@bit3PL,0),
 bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
 tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
 tintInvoicing = ISNULL(@tintInvoicing,1),
 tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
 tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID AS NUMERIC(18,0),
	@Units AS FLOAT,
	@Mode AS TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit,1:Delete,2: DEMOTED TO OPPORTUNITY
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintOppType AS TINYINT = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		I.bitAssembly,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @onReOrder FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @QtyShipped FLOAT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
			IF @tintMode=2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY KIT (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			END
		ELSE IF @Mode=2
				SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
				SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType
		END
		ELSE
		BEGIN
			SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID

			IF @bitAssembly = 1
			BEGIN
				IF @Mode=0 --insert/edit
    			BEGIN
					IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
					BEGIN
						SET @description='SO KIT-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						-- MANAGE INVENTOY OF ASSEMBLY ITEMS
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,1
					END
					ELSE
					BEGIN
						SET @description='SO KIT-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

						SET @numUnits = @numUnits - @QtyShipped
                                                        
						IF @onHand >= @numUnits 
						BEGIN                                    
							SET @onHand = @onHand - @numUnits                            
							SET @onAllocation = @onAllocation + @numUnits                                    
						END                                    
						ELSE IF @onHand < @numUnits 
						BEGIN                                    
							SET @onAllocation = @onAllocation + @onHand                                    
							SET @onBackOrder = @onBackOrder + @numUnits
								- @onHand                                    
							SET @onHand = 0                                    
						END    
			                                 
						UPDATE 
							WareHouseItems
						SET  
							numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE 
							numWareHouseItemID = @numWareHouseItemID
					END
				END
				ELSE IF @Mode=1 -- Revert
				BEGIN
					DECLARE @numWOID AS NUMERIC(18,0)
					DECLARE @numWOStatus AS NUMERIC(18,0)
					SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID

					IF  @tintMode=2
					BEGIN
						SET @description='SO KIT-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @description='SO KIT-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					END


					IF @QtyShipped>0
					BEGIN
						IF @tintMode=0
							SET @numUnits = @numUnits - @QtyShipped
						ELSE IF @tintmode=1 OR @tintMode=2
							SET @onAllocation = @onAllocation + @QtyShipped 
					END 


					--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
					IF @numWOStatus <> 23184
					BEGIN
						IF @onOrder >= @numUnits
							SET @onOrder = @onOrder - @numUnits
						ELSE
							SET @onOrder = 0
					END

					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numUnits < @onBackOrder 
					BEGIN                  
						SET @onBackOrder = @onBackOrder - @numUnits
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@onAllocation - @numUnits) >= 0
							SET @onAllocation = @onAllocation - @numUnits
						
						--ADD QTY TO ONHAND
						SET @onHand = @onHand + @numUnits
					END

					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand ,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID   

					--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
					IF @numWOStatus <> 23184
					BEGIN
						EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID,1
					END
				END
				ELSE IF @Mode=2 -- Close
				BEGIN
					SET @description='SO KIT-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,2
				END
				ELSE IF @Mode = 3 --Re-Open
				BEGIN
					SET @description='SO KIT-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,3
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
			END
			ELSE
			BEGIN
    			IF @Mode=0 --insert/edit
    			BEGIN
    				SET @numUnits = @numUnits - @QtyShipped
    	
					IF @onHand>=@numUnits                                    
					BEGIN                                    
						SET @onHand=@onHand-@numUnits                            
						SET @onAllocation=@onAllocation+@numUnits                                    
					END                                    
					ELSE IF @onHand<@numUnits                                    
					BEGIN                                    
						SET @onAllocation=@onAllocation+@onHand                                    
						SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
						SET @onHand=0                                    
					END                        
    			END
				ELSE IF @Mode=1 --Revert
				BEGIN
					IF @QtyShipped>0
					BEGIN
						IF @tintMode=0
							SET @numUnits = @numUnits - @QtyShipped
						ELSE IF @tintmode=1 OR @tintMode=2
							SET @onAllocation = @onAllocation + @QtyShipped 
					END 
								                    
					IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                            
						IF (@onAllocation - @numUnits >= 0)
							SET @onAllocation = @onAllocation - @numUnits
						SET @onHand = @onHand + @numUnits                                            
					END                                            
					ELSE IF @numUnits < @onBackOrder 
					BEGIN                  
						IF (@onBackOrder - @numUnits >0)
							SET @onBackOrder = @onBackOrder - @numUnits
					END 
				END
				ELSE IF @Mode=2 --Close
				BEGIN
					SET @numUnits = @numUnits - @QtyShipped  
					SET @onAllocation = @onAllocation - @numUnits            
				END
				ELSE IF @Mode=3 --Re-Open
				BEGIN
					SET @numUnits = @numUnits - @QtyShipped
					SET @onAllocation = @onAllocation + @numUnits            
				END

				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=@onHand,
					numAllocation=@onAllocation,                                    
					numBackOrder=@onBackOrder,
					dtModified = GETDATE()  
				WHERE 
					numWareHouseItemID=@numWareHouseItemID 
	
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
			END
		END
		
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ManageInventoryKitWithinKit')
DROP PROCEDURE USP_WareHouseItems_ManageInventoryKitWithinKit
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ManageInventoryKitWithinKit]    
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@Mode as TINYINT,
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@tintOppType AS TINYINT
AS
BEGIN	
	DECLARE @TableKitChildItem TABLE
	(
		RowNo INT,
		numOppKitChildItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		bitAssembly BIT,
		numWarehouseItemID NUMERIC(18,0),
		numUnits FLOAT,
		numQtyShipped FLOAT
	)

	INSERT INTO @TableKitChildItem
	(
		RowNo,
		numOppKitChildItemID,
		numItemCode,
		bitAssembly,
		numWarehouseItemID,
		numUnits,
		numQtyShipped
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
		numOppKitChildItemID,
		numItemID,
		ISNULL(bitAssembly,0),
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped
	FROM
		OpportunityKitChildItems
	JOIN 
		Item I 
	ON 
		OpportunityKitChildItems.numItemID=I.numItemCode    
	WHERE 
		charitemtype = 'P' 
		AND ISNULL(numWareHouseItemId,0) > 0 
		AND numOppID = @numOppID
		AND numOppItemID = @numOppItemID
		AND numOppChildItemID = @numOppChildItemID
  
	DECLARE @numOppKitChildItemID NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onReOrder AS FLOAT
	DECLARE @numUnits as FLOAT
	DECLARE @QtyShipped AS FLOAT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	SELECT @COUNT = COUNT(*) FROM @TableKitChildItem

	WHILE @i <= @COUNT
	BEGIN
		SELECT 
			@numOppKitChildItemID=numOppKitChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWarehouseItemID,
			@numUnits=ISNULL(numUnits,0),
			@QtyShipped=ISNULL(numQtyShipped,0)
		FROM 
			@TableKitChildItem 
		WHERE 
			RowNo=@i
		
		DECLARE @description AS VARCHAR(100)

		IF @Mode=0
			SET @description='SO CHILD KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
			SET @description='SO CHILD KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=2
			SET @description='SO CHILD KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
			SET @description='SO CHILD KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			
    	SELECT 
			@onHand=ISNULL(numOnHand,0),
			@onAllocation=ISNULL(numAllocation,0),                                    
			@onOrder=ISNULL(numOnOrder,0),
			@onBackOrder=ISNULL(numBackOrder,0),
			@onReOrder = ISNULL(numReorder,0)                                     
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
		
		IF @bitAssembly = 1
		BEGIN
			IF @Mode=0 --insert/edit
    		BEGIN
				PRINT @numOppKitChildItemID
				IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
				BEGIN
					SET @description='SO KIT-CHILD-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS

					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numWareHouseItemID,@QtyShipped,1
				END
				ELSE
				BEGIN
					SET @description='SO KIT-CHILD-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

					SET @numUnits = @numUnits - @QtyShipped
                                                        
					IF @onHand >= @numUnits 
					BEGIN                                    
						SET @onHand = @onHand - @numUnits                            
						SET @onAllocation = @onAllocation + @numUnits                                    
					END                                    
					ELSE IF @onHand < @numUnits 
					BEGIN                                    
						SET @onAllocation = @onAllocation + @onHand                                    
						SET @onBackOrder = @onBackOrder + @numUnits
							- @onHand                                    
						SET @onHand = 0                                    
					END    
			                                 
					UPDATE 
						WareHouseItems
					SET  
						numOnHand = @onHand,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE 
						numWareHouseItemID = @numWareHouseItemID
				END
			END
			ELSE IF @Mode=1 -- Revert
			BEGIN
				DECLARE @numWOID AS NUMERIC(18,0)
				DECLARE @numWOStatus AS NUMERIC(18,0)
				SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID

				IF  @tintMode=2
				BEGIN
					SET @description='SO KIT-CHILD-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				END
				ELSE
				BEGIN
					SET @description='SO KIT-CHILD-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				END


				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 


				--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
				IF @numWOStatus <> 23184
				BEGIN
					IF @onOrder >= @numUnits
						SET @onOrder = @onOrder - @numUnits
					ELSE
						SET @onOrder = 0
				END

				-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
				-- DECREASE BACKORDER QTY BY QTY TO REVERT
				IF @numUnits < @onBackOrder 
				BEGIN                  
					SET @onBackOrder = @onBackOrder - @numUnits
				END 
				-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
				-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
				-- SET BACKORDER QTY TO 0
				ELSE IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                        
					--REMOVE ITEM FROM ALLOCATION 
					IF (@onAllocation - @numUnits) >= 0
						SET @onAllocation = @onAllocation - @numUnits
						
					--ADD QTY TO ONHAND
					SET @onHand = @onHand + @numUnits
				END

				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   

				--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
				IF @numWOStatus <> 23184
				BEGIN
					EXEC USP_DeleteAssemblyWOAndInventory  @numDomainID,@numUserCntID,@numWOID,@numOppID,1
				END
			END
			ELSE IF @Mode=2 -- Close
			BEGIN
				SET @description='SO KIT-CHILD-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numWareHouseItemID,@QtyShipped,2
			END
			ELSE IF @Mode = 3 --Re-Open
			BEGIN
				SET @description='SO KIT-CHILD-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numWareHouseItemID,@QtyShipped,3
			END

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomainID
		END
		ELSE
		BEGIN
    		IF @Mode=0 --insert/edit
    		BEGIN
    			SET @numUnits = @numUnits - @QtyShipped
    	
				IF @onHand >= @numUnits                                    
				BEGIN                                    
					SET @onHand=@onHand-@numUnits                            
					SET @onAllocation=@onAllocation+@numUnits                                    
				END                                    
				ELSE IF @onHand < @numUnits                                    
				BEGIN                                    
					SET @onAllocation=@onAllocation+@onHand                                    
					SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
					SET @onHand=0                                    
				END                         
    		END
			ELSE IF @Mode=1 --Revert
			BEGIN
				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 
								                    
				IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                            
					IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					SET @onHand = @onHand + @numUnits                                            
				END                                            
				ELSE IF @numUnits < @onBackOrder 
				BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits					
				END 
			END       
			ELSE IF @Mode=2 --Close
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation - @numUnits            
			END
			ELSE IF @Mode=3 --Re-Open
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation + @numUnits            
			END

			UPDATE 
				WareHouseItems 
			SET 
				numOnHand=@onHand,
				numAllocation=@onAllocation,                                    
				numBackOrder=@onBackOrder,
				dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
	
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomainID
		END

		SET @i = @i + 1
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID NUMERIC(18,0),
	@numOppKitChildItemID NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@numQty AS FLOAT,
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS FLOAT,
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItemID

	SELECT @vcInstruction=vcInstruction,@numAssignedTo=numAssignedTo,@bintCompletionDate=bintCompliationDate FROM WorkOrder WHERE numWOId=@numParentWOID

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,numOppItemID,numOppChildItemID,numOppKitChildItemID,numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate
	)
	VALUES
	(
		@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate
	)

	SELECT @numWOID = SCOPE_IDENTITY()


	IF (SELECT 
			COUNT(*)
		FROM 
			item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numItemCode
			AND item.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=Dtl.numChildItemID AND numWareHouseID=@numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		RETURN
	END

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMId,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numQty)AS FLOAT),
		isnull((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	DECLARE @Description AS VARCHAR(1000)

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @numQty,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 

	IF ISNULL(@bitFromWorkOrderScreen,0) = 0
	BEGIN
		IF @numOppChildItemID > 0 
		BEGIN
			SET @Description='SO KIT Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'
		END
		ELSE IF @numOppKitChildItemID > 0
		BEGIN
			SET @Description='SO KIT CHILD Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'
		END
		ELSE
		BEGIN
			SET @Description='SO-WO Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'
		END

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppID, --  numeric(9, 0)
		@tintRefType = 3, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numWarehouseItemID,@numQtyShipped,1
	END
	ELSE IF ISNULL(@bitFromWorkOrderScreen,0) = 1
	BEGIN
		SET @Description='Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO
-- USP_GetItemsForInventoryAdjustment 1 , 58 , 0 , ''
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsForInventoryAdjustment')
DROP PROCEDURE USP_GetItemsForInventoryAdjustment
GO

CREATE PROCEDURE [dbo].[USP_GetItemsForInventoryAdjustment]
@numDomainID numeric(18,0),
@numWarehouseID numeric(18,0),
@numItemGroupID NUMERIC = 0,
@KeyWord AS VARCHAR(1000) = '', 
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=0,
@numTotalPage as numeric(9) OUT,    
@SortChar CHAR(1) = '0',
@numUserCntID NUMERIC(18,0),
@numItemCode AS NUMERIC(18,0),
@numWarehouseItemID AS NUMERIC(18,0),
@numWLocationID AS NUMERIC(18,0) --05052018 Change: Warehouse Location added
as

declare @strsql as varchar(8000)
declare @strRowCount as varchar(8000)
    /*paging logic*/
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@numCurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@numCurrentPage
                      * @PageSize
                      + 1);

DECLARE @strColumns VARCHAR(MAX) = ''
DECLARE @strJoin VARCHAR(MAX) = ''
DECLARE @numFormID NUMERIC(18,0) = 126

DECLARE @TempForm TABLE  
(
	ID INT IDENTITY(1,1),tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),
	vcAssociatedControlType nvarchar(50), vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),
	bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
	bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,
	vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)


DECLARE @Nocolumns AS TINYINT;
SET @Nocolumns = 0

SELECT 
	@Nocolumns=ISNULL(SUM(TotalRow),0) 
FROM
	(            
		SELECT	
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

IF @Nocolumns=0
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,0,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=@numFormId AND bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
	ORDER BY 
		tintOrder ASC 
END


INSERT INTO @TempForm
SELECT 
	tintRow+1 as tintOrder,	vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
	vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit, bitIsRequired,bitIsEmail,
	bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
	ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
FROM 
	View_DynamicColumns 
WHERE 
	numFormId=@numFormId 
	AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1 
	AND ISNULL(bitSettingField,0)=1 
	AND ISNULL(bitCustom,0)=0  
UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,
		'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,
		bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,
		ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  

DECLARE @bitCustom AS BIT
DECLARE @tintOrder AS TINYINT
DECLARE @numFieldId AS NUMERIC(18,0)
DECLARE @vcFieldName AS VARCHAR(200)
DECLARE @vcAssociatedControlType AS VARCHAR(200)
DECLARE @vcDbColumnName AS VARCHAR(200)

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT = 0
SELECT @COUNT=COUNT(*) FROM @TempForm

WHILE @i <= @COUNT
BEGIN
	SELECT  
		@bitCustom = bitCustomField,
		@tintOrder = tintOrder,
		@vcFieldName = vcFieldName,
		@numFieldId=numFieldId,
        @vcAssociatedControlType = vcAssociatedControlType,
        @vcDbColumnName = vcDbColumnName
    FROM    
		@TempForm
    WHERE   
		ID = @i        

	IF @bitCustom = 1 
    BEGIN      
        IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
        BEGIN                    
			SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=I.numItemCode '                                                         
        END   
        ELSE IF @vcAssociatedControlType = 'CheckBox' 
        BEGIN            
            SET @strColumns = @strColumns + ',CASE WHEN ISNULL(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,0)=0 then ''No'' when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcDbColumnName
                                    + ']'              
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=I.numItemCode   '                                                     
        END                
        ELSE IF @vcAssociatedControlType = 'DateField' 
        BEGIN              
            SET @strColumns = @strColumns + ',dbo.FormatedDateFromDate(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,' + CONVERT(VARCHAR(10), @numDomainId) 
									+ ')  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
										+ '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=I.numItemCode   '                                                         
        END                
        ELSE IF @vcAssociatedControlType = 'SelectBox' 
        BEGIN                
            SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)                
            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcDbColumnName + ']'                                                          
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
									+ '.Fld_Id=' + CONVERT(VARCHAR(10), @numFieldId) + ' and CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.RecId=I.numItemCode    '                                                         
            SET @strJoin = @strJoin + ' LEFT JOIN ListDetails L'+ CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
  + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
        END                 
    END       


	SET @i = @i + 1
END


SET @strsql = ''
SET @strRowCount = ''

SET @strsql = @strsql +
'WITH WarehouseItems AS (
	SELECT 
		ROW_NUMBER() OVER(ORDER by WI.numWareHouseItemID) AS RowNumber,
		WI.numWareHouseItemID,
		ISNULL(W.vcWarehouse,'''') + (CASE WHEN WI.numWLocationID = -1 THEN '' (Global)'' ELSE (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) END) vcWarehouse,
		ISNULL(WL.numWLocationID,0) AS numWLocationID,
		Case WHEN I.bitKitParent=1 THEN 0 ELSE ISNULL(numOnHand,0) END as [numOnHand],
		ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0) AS TotalOnHand,
		Case WHEN I.bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END as [Reorder],
		Case WHEN I.bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END as [OnOrder],
		Case WHEN I.bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END as [Allocation],
		Case WHEN I.bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END as [BackOrder],
		(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost,
		(ISNULL(WI.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
		CASE WHEN I.bitSerialized =1 THEN 1 WHEN I.bitLotNo =1 THEN 1 ELSE 0 END AS IsLotSerializedItem,
		I.numAssetChartAcntId,
		CASE 
			WHEN ISNULL(I.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WI.numWareHouseItemID,0)
			ELSE ''''
		END AS vcAttribute,
		ISNULL(I.bitKitParent,0) AS bitKitParent,
		ISNULL(I.bitSerialized,0) AS bitSerialized,
		ISNULL(I.bitLotNo,0) as bitLotNo,
		ISNULL(I.bitAssembly,0) as bitAssembly,
		I.numItemcode,
		I.vcItemName,
		ISNULL(CompanyInfo.vcCompanyName,'''') AS vcCompanyName,
		ISNULL(WI.monWListPrice,0) AS monListPrice,
		ISNULL(I.txtItemDesc,'''') AS txtItemDesc,
		ISNULL(I.vcModelID,'''') AS vcModelID,
		ISNULL(I.vcManufacturer,'''') AS vcManufacturer,
		ISNULL(I.fltHeight,'''') AS fltHeight,
		ISNULL(I.fltLength,'''') AS fltLength,
		ISNULL(I.fltWeight,'''') AS fltWeight,
		ISNULL(I.fltWidth,'''') AS fltWidth,
		ISNULL(I.numItemGroup,0) AS ItemGroup,
		ISNULL(ItemGroups.vcItemGroup,'''') AS numItemGroup,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numPurchaseUnit) ,'''') AS numPurchaseUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numSaleUnit) ,'''') AS numSaleUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numBaseUnit) ,'''') AS numBaseUnit,
		ISNULL(WI.vcWHSKU,'''') AS vcSKU,
		ISNULL(WI.vcBarCode,'''') AS numBarCodeId,
		ISNULL(TEMPVendor.monCost,0) AS monCost,
		ISNULL(TEMPVendor.intMinQty,0) AS intMinQty,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) [IsDeleteKitWarehouse],
		(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WI.numItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse '
		+
		ISNULL(@strColumns,'')
		+ ' FROM 
		dbo.WareHouseItems WI 
		INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
		INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID 
		OUTER APPLY
		(
			SELECT
				numVendorID,
				monCost,
				intMinQty
			FROM
				Vendor
			WHERE
				Vendor.numItemCode = I.numItemCode AND Vendor.numVendorID = I.numVendorID
		) AS TEMPVendor
		LEFT JOIN DivisionMaster ON TEMPVendor.numVendorID = DivisionMaster.numDivisionID
		LEFT JOIN CompanyInfo ON DivisionMaster.numCompanyID  = CompanyInfo.numCompanyId
		LEFT JOIN ItemGroups ON I.numItemGroup = ItemGroups.numItemGroupID '
		+ 
		ISNULL(@strJoin,'')
		+ ' WHERE (I.numItemCode = ' + CAST(@numItemCode AS VARCHAR) + ' OR ' + CAST(@numItemCode AS VARCHAR) + ' = 0) AND
		(WI.numWareHouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR) + ' OR ' + CAST(@numWarehouseItemID AS VARCHAR) + ' = 0) AND
		I.charItemType = ''P'' AND 
		I.numDomainID = '+ convert(varchar(20), @numDomainID) + 
		' AND  WI.numDomainID=' + convert(varchar(20), @numDomainID) +
		' AND (WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' +
		' AND (WI.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)' +
		' AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+' OR '+CONVERT(VARCHAR(20), @numItemGroupID)+' = 0)'

SET @strRowCount = @strRowCount + 
' SELECT 
COUNT(*) AS TotalRowCount
FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
WHERE (I.numItemCode = ' + CAST(@numItemCode AS VARCHAR) + ' OR ' + CAST(@numItemCode AS VARCHAR) + ' = 0) AND
(WI.numWareHouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR) + ' OR ' + CAST(@numWarehouseItemID AS VARCHAR) + ' = 0) AND 
I.charItemType = ''P''  
AND I.numDomainID = '+ convert(varchar(20), @numDomainID)  +' 
AND  WI.numDomainID=' + convert(varchar(20), @numDomainID)+'  
AND (WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' + 
' AND (WI.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)' +
'AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+' OR '+CONVERT(VARCHAR(20), @numItemGroupID)+' = 0) '


 IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strSql = @strSql
                        + ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')'
                         
					SET @strRowCount =  @strRowCount  +	 ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')' 		
                END
            ELSE 
            BEGIN
            	SET @strSql = @strSql + ' and  1 = 1 ' + @KeyWord 
                SET @strRowCount =  @strRowCount  + ' and  1 = 1 ' + @KeyWord 
            END
                
        END


if @SortChar<>'0'
	BEGIN
	  set @strSql=@strSql + ' AND vcItemName like '''+@SortChar+'%'')'  
	  set @strRowCount=@strRowCount + ' AND vcItemName like '''+@SortChar+'%'''  
	END
ELSE
   BEGIN
      set @strSql=@strSql + ' ) '  
   END


 





   set @strSql=@strSql + ' SELECT * FROM WarehouseItems WHERE 
                            RowNumber > '+CONVERT(VARCHAR(20),@firstRec) +' AND RowNumber < '+CONVERT(VARCHAR(20),@lastRec) + @strRowCount
 
   
  
PRINT(@strSql)
exec(@strSql)

SELECT * FROM @TempForm


GO
--exec USP_GetItemsForInventoryAdjustment @numDomainID=156,@numWarehouseID=1039,@numItemGroupID=0,@KeyWord='',@numCurrentPage=1,@PageSize=5,@numTotalPage=0,@SortChar='0'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numItemCode,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
