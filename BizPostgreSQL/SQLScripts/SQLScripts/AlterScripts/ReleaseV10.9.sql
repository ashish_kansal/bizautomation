/******************************************************************
Project: Release 10.9 Date: 21.JAN.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

INSERT INTO ReportListMaster
(
	vcReportName,vcReportDescription,numDomainID,tintReportType,bitDefault,intDefaultReportID
)
VALUES
(
	'Sales Opportunity Won/Lost Report','Sales Opportunity Won/Lost Report',0,1,1,34
),
(
	'Sales Opportunity Pipeline Report','Sales Opportunity Pipeline Report',0,1,1,35
)

ALTER TABLE ReportDashboard ADD
vcTimeLine VARCHAR(50)
,vcGroupBy VARCHAR(50)
,vcTeritorry VARCHAR(MAX)
,vcFilterBy TINYINT
,vcFilterValue VARCHAR(MAX)
,dtFromDate DATE
,dtToDate DATE


ALTER TABLE OpportunityMaster ADD dtDealLost DATETIME


DECLARE @numFieldID NUMERIC(18,0)

SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'vcItemReleaseDate'


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID
	,numFieldID
	,vcFieldName
	,vcAssociatedControlType
	,vcFieldDataType
	,bitAllowSorting
	,bitAllowGrouping
	,bitAllowAggregate
	,bitAllowFiltering
)
VALUES
(
	4
	,@numFieldID
	,'Item Release Date'
	,'DateField'
	,'D'
	,1
	,1
	,0
	,1
)