/******************************************************************
Project: Release 8.3 Date: 25.OCTOBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitWorkFlowField,vcGroup,bitInResults,bitDeleted,bitAllowEdit,bitInlineEdit,bitDefault
)
VALUES
(
	3,'PO from SO / SO from PO','bitPOFromSOOrSOFromPO','bitPOFromSOOrSOFromPO','POFromSOOrSOFromPO','OpportunityMaster','Y','R','CheckBox','',0,1,'Order Fields',0,0,0,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,70,0,0,'PO from SO / SO from PO','CheckBox','POFromSOOrSOFromPO',1,1,2,1,1
)


ALTER TABLE GroupTabDetails ADD bitInitialTab BIT