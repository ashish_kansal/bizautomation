/******************************************************************
Project: Release 8.9 Date: 27.JANUARY.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************   SANDEEP  *****************************/

ALTER TABLE OpportunityMaster ADD tintEDIStatus TINYINT

-----------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,'EDI Status','tintEDIStatus','tintEDIStatus','EDIStatus','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,1,1
	)

	SET @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,39,0,0,'EDI Status','SelectBox','EDIStatus',1,1,1,0,0,1,0,1,1,1
	)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

ALTER TABLE NameTemplate ADD dtCreatedDate DATETIME

------------------------------------------------

SET IDENTITY_INSERT dbo.ListDetails ON;
INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
)
VALUES
(
	15445,176,'Send Shipment Request (940)',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
)

INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
)
VALUES
(
	15446,176,'Shipment Request (940) Failed',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
)

INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
)
VALUES
(
	15447,176,'Send 856',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
)

INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
)
VALUES
(
	15448,176,'Send 856 Failed',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
)

SET IDENTITY_INSERT dbo.ListDetails OFF;

------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION
	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId ASC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId > 0
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		EXEC USP_DomainSFTPDetail_Generate @numDomainId

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


ALTER TABLE Domain ADD bitEDI BIT DEFAULT 0
ALTER TABLE ShippingReport ADD bitSendToTP2 BIT

-----------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[EDIHistory]    Script Date: 27-Jan-18 12:26:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDIHistory](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[tintEDIStatus] [tinyint] NOT NULL,
	[dtDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EDIHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[EDIQueue]    Script Date: 27-Jan-18 12:26:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDIQueue](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[bitExecuted] [bit] NOT NULL,
	[bitSuccess] [bit] NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[dtExecutionDate] [datetime] NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[EDIType] [int] NOT NULL,
 CONSTRAINT [PK_EDIQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[EDIQueue]  WITH CHECK ADD  CONSTRAINT [FK_EDIQueue_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EDIQueue] CHECK CONSTRAINT [FK_EDIQueue_Domain]
GO

ALTER TABLE [dbo].[EDIQueue]  WITH CHECK ADD  CONSTRAINT [FK_EDIQueue_OpportunityMaster] FOREIGN KEY([numOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EDIQueue] CHECK CONSTRAINT [FK_EDIQueue_OpportunityMaster]
GO


----------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[EDIQueueLog]    Script Date: 27-Jan-18 12:28:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EDIQueueLog](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numEDIQueueID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[vcLog] [ntext] NOT NULL,
	[bitSuccess] [bit] NOT NULL,
	[vcExceptionMessage] [ntext] NULL,
	[dtDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EDIQueueLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


-------------------------


USE [Production.2014]
GO

/****** Object:  Table [dbo].[DomainSFTPDetail]    Script Date: 27-Jan-18 12:29:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DomainSFTPDetail](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcUsername] [varchar](20) NOT NULL,
	[vcPassword] [varchar](20) NOT NULL,
	[tintType] [tinyint] NOT NULL,
 CONSTRAINT [PK_DomainSFTPDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


