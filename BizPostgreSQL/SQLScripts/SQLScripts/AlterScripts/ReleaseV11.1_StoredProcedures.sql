/******************************************************************
Project: Release 11.1 Date: 20.FEB.2019
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_businessprocesslist')
DROP PROCEDURE usp_businessprocesslist
GO
CREATE PROCEDURE [dbo].[usp_BusinessProcessList] 
@numDomainId as numeric(9),
@Mode as int
as 
IF(@Mode=-1)
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 order by slp_name
END
ELSE
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where Pro_Type=@Mode and numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 order by slp_name
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelPromotionOffer')
DROP PROCEDURE USP_DelPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_DelPromotionOffer]
	@numDomainID AS NUMERIC(18,0),
	@numProId AS NUMERIC(18,0)
AS
BEGIN TRY
	IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainID AND numOrderPromotionID=@numProId)
	BEGIN
		RAISERROR('ITEM_PROMO_USING_ORDER_PROMO',16,1)
		RETURN
	END

	BEGIN TRANSACTION
		DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId 
		DELETE FROM PromotionOfferItems WHERE numProId = @numProId
		DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId		
		DELETE FROM PromotionOfferOrder WHERE numPromotionID=@numProId	
		DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId
	COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DiscountCodes_Validate')
DROP PROCEDURE USP_DiscountCodes_Validate
GO
CREATE PROCEDURE [dbo].[USP_DiscountCodes_Validate]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionId NUMERIC(18,0)
	,@txtCouponCode VARCHAR(100)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END

	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @bitOrderBasedPromotion AS BIT = 0
	DECLARE @bitPromotionNeverExpires BIT
	DECLARE @dtPromotionStartDate DATETIME
	DECLARE @dtPromotionEndDate DATETIME
	DECLARE @intUsageLimit INT
	DECLARE @numDisocuntID NUMERIC(18,0)

	IF @numDomainID <> 204
	BEGIN
		-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
		SELECT 
			@numPromotionID=PO.numProId
			,@intUsageLimit=ISNULL(DC.CodeUsageLimit,0)
			,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
			,@dtPromotionStartDate=PO.dtValidFrom
			,@dtPromotionEndDate=PO.dtValidTo
			,@numDisocuntID=numDiscountId
			,@bitOrderBasedPromotion=ISNULL(PO.IsOrderBasedPromotion,0)
		FROM 
			PromotionOffer PO
		INNER JOIN 
			PromotionOfferOrganizations PORG
		ON 
			PO.numProId = PORG.numProId
		INNER JOIN 
			DiscountCodes DC
		ON 
			PO.numProId = DC.numPromotionID
		WHERE
			PO.numDomainId = @numDomainID
			AND 1 =(CASE WHEN numRelationship=@numRelationship AND numProfile=@numProfile THEN 1 ELSE 0 END)
			AND DC.vcDiscountCode = @txtCouponCode

		IF ISNULL(@numPromotionID,0) > 0
		BEGIN
			-- NOW CHECK IF PROMOTION EXPIRES OR NOT
			IF ISNULL(@bitPromotionNeverExpires,0) = 0
			BEGIN
				IF (@dtPromotionStartDate <= GETUTCDATE() AND @dtPromotionEndDate >= GETUTCDATE())
				BEGIN
					IF ISNULL(@intUsageLimit,0) <> 0
					BEGIN
						IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numDisocuntID AND numDivisionId=@numDivisionId),0) >= @intUsageLimit
						BEGIN
							RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_EXPIRED',16,1)
					RETURN
				END
			END
		END
		ELSE
		BEGIN
			RAISERROR('INVALID_COUPON_CODE',16,1)
			RETURN
		END
	END

	SELECT 
		@numPromotionID AS numPromotionID
		,@numDisocuntID AS numDisocuntID
		,@txtCouponCode AS vcCouponCode
		,@bitOrderBasedPromotion AS bitOrderBasedPromotion
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
	@numUserCntId numeric(18,0),
	@numDomainId numeric(18,0),
	@vcCookieId varchar(100),
	@bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   ,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,I.txtItemDesc AS txtItemDesc,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection,
			   (CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
				
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection
			   ,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId	
				
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0)
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO 
		#View_Journal 
	SELECT 
		numAccountId
		,AccountTypeCode
		,AccountCode
		,datEntry_Date
		,Debit
		,Credit 
	FROM 
		view_journal_master 
	WHERE 
		numDomainId = @numDomainID 
		AND (numClassIDDetail=@numAccountClass OR @numAccountClass=0)
		AND (numDivisionId=@numDivisionID OR @numDivisionID=0)

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
	AS
	(
		SELECT 
			ParentId,
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(CONCAT(d.Struc ,'#',[COA].[numAccountId]) AS VARCHAR(300)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc 
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
	UNION ALL
	SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL,'-2'
	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc 
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'


	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-1
	WHERE 
		[vcAccountCode] NOT IN ('010302','010402')
		AND [LEVEL] = 1

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.numAccountId = COA.numAccountId
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			DATENAME(mm, @dtFromDate) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN		
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = ' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%''
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2
				UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS DECIMAL(20,5)
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                FORMAT(monPrice,'0.################') monPrice,
				CONVERT(DECIMAL(18, 4),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') vcBaseUOM,
				ISNULL(dbo.fn_GetUOMName(numUOMId),'') vcUOM,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID=RI.numOppBizDocItemID),isnull(i.monAverageCost,'0')) END) as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount]
				,ISNULL(I.bitKitParent,0) AS bitKitParent
				,ISNULL(I.bitAssembly,0) AS bitAssembly,
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0)
AS                                                          
BEGIN 

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END
 

;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,tintSortOrder)
AS
(
  -- anchor
	SELECT 
		CAST('-1' AS VARCHAR) AS ParentId,
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		[ATD].[vcAccountType],
		[ATD].[vcAccountCode],
		1 AS LEVEL, 
		CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc,
		(CASE [ATD].[vcAccountCode] WHEN '0103' THEN 100 WHEN '0106' THEN 500 WHEN '0104' THEN 1500 END)
	FROM 
		[dbo].[AccountTypeDetail] AS ATD
	WHERE 
		[ATD].[numDomainID]=@numDomainId AND  [ATD].[vcAccountCode] IN('0103','0104','0106')
	UNION ALL
	-- recursive
	SELECT 
		D.vcCompundParentKey AS ParentId, 
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		[ATD].[vcAccountType],
		[ATD].[vcAccountCode],
		LEVEL + 1, 
		CAST(d.Struc + '#' + CAST([ATD].[vcAccountCode] AS VARCHAR) AS varchar(300))  AS Struc,
		(D.tintSortOrder + 1)
	FROM 
		[dbo].[AccountTypeDetail] AS ATD 
	JOIN 
		[DirectReport] D 
	ON 
		D.[numAccountTypeID] = [ATD].[numParentID]
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
),
DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,tintSortOrder)
AS
(
	-- anchor
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType,
		vcAccountCode, 
		LEVEL,
		CAST(numAccountID AS NUMERIC(18)),
		Struc,
		tintSortOrder
	FROM 
		DirectReport 
	UNION ALL
	-- recursive
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)), 
		COA.[vcAccountName],
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST([COA].[numAccountId] AS VARCHAR) AS VARCHAR(300))  AS Struc,
		(D.tintSortOrder + 1)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.[numAccountTypeID] = COA.[numParntAcntTypeId]
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND COA.bitActive = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 0
	UNION ALL
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)),
		[vcAccountName],
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
		(D.tintSortOrder + 1)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.numAccountId = COA.numParentAccId
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND ISNULL(COA.bitActive,0) = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 1
)

  
SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode,numAccountId,Struc,tintSortOrder INTO #tempDirectReport FROM DirectReport1 ORDER BY tintSortOrder

INSERT INTO #tempDirectReport
SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL,'-1',1
UNION ALL
SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL,'-2',2000


INSERT INTO #tempDirectReport SELECT '-1','-4',-4,'Gross Profit',0,NULL,NULL,'-1#-4',1000

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc, tintSortOrder=2001 WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


SELECT 
	COA.ParentId
	,COA.vcCompundParentKey
	,COA.numAccountTypeID
	,COA.vcAccountType
	,COA.LEVEL
	,COA.vcAccountCode
	,COA.numAccountId
	,COA.Struc
	,(CASE 
		WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
		THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
		ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
	END) AS Amount
	,V.datEntry_Date
INTO 
	#tempViewData
FROM 
	#tempDirectReport COA 
JOIN 
	View_Journal_Master V 
ON  
	V.numAccountId = COA.numAccountId
	AND V.numDomainID=@numDomainId 
	AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (V.numClassIDDetail=@numAccountClass OR @numAccountClass=0)
	AND (V.numDivisionId=@numDivisionID OR @numDivisionID=0)
WHERE 
	COA.[numAccountId] IS NOT NULL

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
		MONTH(DATEADD(d,1,new_date)) AS 'mm',
		DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE DATEADD(d,1,new_date) < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY tintSortOrder, Struc'

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY tintSortOrder, Struc'
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = ' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.tintSortOrder) AS t
				ORDER BY tintSortOrder, Struc'
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDetails')
DROP PROCEDURE USP_GetItemPromotionDetails
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDetails]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@numUnitHour FLOAT,
	@monTotalAmount DECIMAL(20,5)
AS
BEGIN
	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		DECLARE @vcCurrency VARCHAR(10)
		SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=@numDomainID

		SELECT TOP 1
			numProId 
			,ISNULL(vcLongDesc,'-') vcLongDesc
			,CONCAT(ISNULL(vcShortDesc,'-'),(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) vcShortDesc
			,ISNULL(tintOfferTriggerValueTypeRange,1) tintOfferTriggerValueTypeRange
			,ISNULL(tintOfferTriggerValueType,1) tintOfferTriggerValueType
			,ISNULL(fltOfferTriggerValue,0) fltOfferTriggerValue
			,ISNULL(fltOfferTriggerValueRange,0) fltOfferTriggerValueRange
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				END) canUsePromotion
			,CONCAT((CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					(CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to between ',ISNULL(fltOfferTriggerValue,0),' & ',ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to ',ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END)
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					(CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase amount to between ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' & ',@vcCurrency,ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency) 
								ELSE CONCAT('Increase amount to ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END)
			END),(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) vcPromotionStatus
			,tintOfferBasedOn
			,(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN 1 ELSE 0 END) bitRequireCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID	
	END
	ELSE
	BEGIN
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID 

		DECLARE @numItemClassification AS NUMERIC(18,0)
		SELECT @numItemClassification = numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode

		SELECT TOP 1
			numProId 
			,ISNULL(vcShortDesc,'-') vcShortDesc
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
			END) canUsePromotion
			,ISNULL(bitRequireCouponCode,0) bitRequireCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(IsOrderBasedPromotion,0) = 0
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE 
						WHEN ISNULL(numOrderPromotionID,0) > 0
						THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
						ELSE
							(CASE tintCustomersBasedOn 
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN 1
								ELSE 0
							END)
					END)
			AND 1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 4 THEN 1 
						ELSE 0
					END)
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC
	END
END 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPriceOfItem]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetPriceOfItem @numItemID=173028,@intQuantity=1,@numDomainID=72,@numDivisionID=7056

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpriceofitem')
DROP PROCEDURE usp_getpriceofitem
GO
CREATE PROCEDURE [dbo].[USP_GetPriceOfItem]
@numItemID AS NUMERIC(18,0)=0,
@intQuantity AS INTEGER=0,
@numDomainID AS NUMERIC(18,0)=0,
@numDivisionID as numeric(18,0)=0,
@numWarehouseID AS NUMERIC(18,0)=0,
@numWarehouseItemID AS NUMERIC(18,0) = 0,
@vcSelectedKitChildItems AS VARCHAR(MAX) = ''
AS
BEGIN
	IF ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		SELECT TOP 1 @numWareHouseItemID=[numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID]=@numItemID AND [numWareHouseID]=@numWarehouseID
	END

	EXEC USP_ItemPricingRecomm 
		@numItemCode=@numItemID
		,@units=@intQuantity
		,@numOppID=0
		,@numDivisionID=@numDivisionID
		,@numDomainID=@numDomainID
		,@tintOppType=1
		,@CalPrice=0.0
		,@numWareHouseItemID=@numWareHouseItemID
		,@bitMode=2
		,@vcSelectedKitChildItems=@vcSelectedKitChildItems
		,@numOppItemID=0
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotiondrulefororder')
DROP PROCEDURE usp_getpromotiondrulefororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotiondRuleForOrder]
	 @numDomainID NUMERIC(18,0)
	,@numSubTotal FLOAT = 0
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numPromotionID NUMERIC(18,0)
AS
BEGIN	
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END



	DECLARE @numProID AS NUMERIC

	SET @numProID = (SELECT TOP 1 
						PO.numProId		 
					FROM 
						PromotionOffer PO	
					INNER JOIN 
						PromotionOfferOrganizations PORG
					ON 
						PO.numProId = PORG.numProId
					WHERE 
						PO.numDomainId = @numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 1 
						AND (PO.numProID=@numPromotionID OR ISNULL(@numPromotionID,0)=0)
						AND ISNULL(bitEnabled,0) = 1
						AND numRelationship=@numRelationship
						AND ISNULL(bitUseForCouponManagement,0) = 0
						AND numProfile=@numProfile
						AND  1 = (CASE 
									WHEN ISNULL(PO.bitNeverExpires,0) = 1
									THEN 1
									WHEN ISNULL(PO.bitNeverExpires,0) = 0 
									THEN 
										CASE WHEN (PO.dtValidFrom <= GETUTCDATE() AND PO.dtValidTo >= GETUTCDATE())
									THEN 1	
									ELSE 0 END
								ELSE 0 END )
				)


	SELECT * FROM PromotionOffer WHERE numProId = @numProID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numProID

END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0,
	@ClientTimeZoneOffset AS INT
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT
			[numProId]
			,[vcProName]
			,[numDomainId]
			,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
			,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
			,[bitNeverExpires]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
			,[vcShortDesc]
			,[vcLongDesc]
			,[tintItemCalDiscount]
			,ISNULL(bitUseOrderPromotion,0) bitUseOrderPromotion
			,ISNULL(numOrderPromotionID,0) numOrderPromotionID
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId AND ISNULL(IsOrderBasedPromotion,0) = 0
    END  
	ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			PO.numProId,
            PO.vcProName,
			(CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN 'Order based' ELSE 'Item based' END) vcPromotionType,
			(CASE 
				WHEN ISNULL(PO.numOrderPromotionID,0) > 0
				THEN
					(CASE WHEN ISNULL(POOrder.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidFrom) ELSE POOrder.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidTo) ELSE POOrder.dtValidTo END),@numDomainID)) END)
				ELSE
					(CASE WHEN ISNULL(PO.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidFrom) ELSE PO.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidTo) ELSE PO.dtValidTo END),@numDomainID)) END)
			END) AS vcDateValidation
			,(CASE 
				WHEN ISNULL(PO.bitRequireCouponCode,0) = 1 OR ISNULL(PO.numOrderPromotionID,0) > 0 THEN
			       STUFF((SELECT CONCAT (', ' , vcDiscountCode , ' (', ISNULL(SUM(DCU.intCodeUsed),0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = (CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN PO.numOrderPromotionID ELSE PO.numProId END) GROUP BY D.vcDiscountCode, DCU.numDiscountId
									   FOR XML PATH ('')), 1, 1, '')
				ELSE '' 
			END) AS vcCouponCode
			,(CASE WHEN ISNULL(PO.bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(PO.bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN
						CONCAT('Buy ', 
							CASE PO.tintDiscountType WHEN 1 THEN
										STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
										   FROM PromotionOfferOrder POO					   
										   WHERE POO.numPromotionID =  PO.numProId
										   FOR XML PATH ('')), 1, 1, '')  
									WHEN 2 THEN 
										STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
												   FROM PromotionOfferOrder POO					   
												   WHERE POO.numPromotionID =  PO.numProId
												   FOR XML PATH ('')), 1, 1, '')
							ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN PO.tintOfferTriggerValueType=1 THEN CAST(PO.fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',PO.fltOfferTriggerValue) END
					,CASE PO.tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE  
						WHEN (PO.tintDiscountType = 1 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT(PO.fltDiscountValue,'% off on') 
						WHEN (PO.tintDiscountType = 2 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT('$',PO.fltDiscountValue, ' off on')
						WHEN (PO.tintDiscountType = 3 AND PO.tintDiscoutBaseOn <> 4) THEN 
						CONCAT(PO.fltDiscountValue,' of') END
					, CASE 
						WHEN PO.tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 3 THEN ' related items.'
						WHEN PO.tintDiscoutBaseOn = 4 THEN CONCAT(PO.fltDiscountValue,' quantity free for any item of equal or lesser value')
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,PO.bitEnabled, PO.tintCustomersBasedOn, PO.tintOfferTriggerValueTypeRange, PO.fltOfferTriggerValueRange, PO.[IsOrderBasedPromotion]
        FROM  
			PromotionOffer PO
		LEFT JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		WHERE 
			PO.numDomainID = @numDomainID
		ORDER BY
			PO.dtCreated DESC
    END
	  
   	
END      
  
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotionofferdetailsfororder')
DROP PROCEDURE usp_getpromotionofferdetailsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferDetailsForOrder] 

@numPromotionID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(9)=0,
@ClientTimeZoneOffset AS INT

AS
BEGIN
	SELECT
		[numProId]
		,[vcProName]
		,[numDomainId]
		,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
		,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
		,[bitNeverExpires]
		,[bitApplyToInternalOrders]
		,[bitAppliesToSite]
		,[bitRequireCouponCode]
		,STUFF((SELECT ',' + vcDiscountCode FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId FOR XML PATH ('')), 1, 1, '') AS vcDiscountCode
		,ISNULL((SELECT TOP 1 CodeUsageLimit FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId),0) CodeUsageLimit
		,[numCreatedBy]
		,[dtCreated]
		,[numModifiedBy]
		,[dtModified]
		,[tintDiscountType]
		,[IsOrderBasedPromotion]
		,ISNULL(bitUseForCouponManagement,0) bitUseForCouponManagement
    FROM
		PromotionOffer
    WHERE
		numProId = @numPromotionID AND numDomainId = @numDomainID

	SELECT numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numPromotionID AND numDomainId = @numDomainID
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetStageItemDetails]    Script Date: 09/23/2010 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetStageItemDetails              
              
--This procedure will return the values from the stagepercentagedetails table. If the stage number is passed then              
--the details pertaining to that stge will be passed else all the records will be passed              
--EXEC usp_GetStageItemDetails 1,133,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstageitemdetails')
DROP PROCEDURE usp_getstageitemdetails
GO
CREATE PROCEDURE [dbo].[usp_GetStageItemDetails]
--    @numStageNo NUMERIC = 0,
    @numDomainid NUMERIC = 0,
    @slpid NUMERIC = 0 ,
    @tintMode TINYINT=0,
	@numOppId NUMERIC=0
--              
AS 
IF @tintMode = 0 
BEGIN              
        SELECT  numStageDetailsId,
                numStagePercentageId,
                vcStageName,
                sp.numDomainId,
                sp.numCreatedBy,
                sp.bintCreatedDate,
                sp.numModifiedBy,
                sp.bintModifiedDate,
                slp_id,
                sp.numAssignTo,
                vcMilestoneName,
				ISNULL((select
				   DISTINCT
					stuff((
						select ',' + ST.vcTaskName+'('+ISNULL((SELECT TOP 1 (AU.vcFirstName+' '+AU.vcLastName) FROM AdditionalContactsInformation AS AU WHERE AU.numContactID=ST.numAssignTo),'-')+')'
						from StagePercentageDetailsTask ST
						where ST.numStageDetailsId = STU.numStageDetailsId AND ISNULL(ST.numOppId,0)=0 AND  ISNULL(ST.numProjectId,0)=0
						order by ST.numTaskId
						for xml path('')
					),1,1,'') as TaskName
				from StagePercentageDetailsTask AS STU  WHERE STU.numStageDetailsID=sp.numStageDetailsId AND ISNULL(STU.numOppId,0)=0 AND  ISNULL(STU.numProjectId,0)=0
				group by numStageDetailsId),'-') As TaskName,
				(SELECT ISNULL(SUM(numHours),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId) As numHours,
				(SELECT ISNULL(SUM(numMinutes),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId)  As numMinutes,
                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
                     ELSE A.vcFirstName + ' ' + A.vcLastName
                END AS vcAssignTo,
                ISNULL(tintPercentage, '') tintPercentage,
                ISNULL(bitClose, 0) bitClose,
                tintConfiguration,
                ISNULL(vcDescription,'') vcDescription,
                ISNULL(numParentStageID,0) numParentStageID,
				ISNULL(bitIsDueDaysUsed,0) AS bitIsDueDaysUsed,
				ISNULL(numTeamId,0) AS numTeamId,
				ISNULL(bitRunningDynamicMode,0) AS bitRunningDynamicMode,
                ISNULL(intDueDays,0) AS intDueDays,(select count(*) from StageDependency where numStageDetailID=sp.numStageDetailsId) numDependencyCount,
				(convert(varchar(10),sp.dtStartDate, 101) + right(convert(varchar(32),sp.dtStartDate,100),8)) AS StageStartDate,
				(select dbo.fn_calculateTotalHoursMinutesByStageDetails(sp.numStageDetailsId,@numOppId)) as dayshoursTaskDuration,
				ISNULL(SP.numStageOrder,0) As numStageOrder
        FROM    stagepercentagedetails sp
                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
        WHERE  
                sp.numdomainid = @numDomainid
                AND slp_id = @slpid
                AND ISNULL(sp.numProjectID,0)= 0
                AND ISNULL(sp.numOppID,0)= @numOppId
        ORDER BY numstagepercentageid,
                numStageOrder ASC              
    END              
ELSE IF @tintMode = 1 -- Fill dropdown
SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1

ELSE IF  @tintMode = 2
SELECT [numStageDetailsId],[numStagePercentageId],tintConfiguration
      ,[vcStageName],[numDomainId],[numCreatedBy],
	(Case bitFromTemplate when 0 then isnull(dbo.fn_GetContactName([numCreatedBy]),'-') else 'Template' end) as vcCreatedBy,dbo.[FormatedDateFromDate](ISNULL(bintCreatedDate,GETDATE()),@numDomainId) bintCreatedDate
      ,[numModifiedBy],isnull(dbo.fn_GetContactName([numModifiedBy]),'-') as vcModifiedBy
      ,dbo.[FormatedDateTimeFromDate](ISNULL(bintModifiedDate,GETDATE()),@numDomainId) bintModifiedDate,[slp_id]
      ,[numAssignTo],isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo
      ,[vcMileStoneName],[tintPercentage],isnull([tinProgressPercentage],0) tinProgressPercentage,[bitClose]
      ,dbo.[FormatedDateFromDate](ISNULL(dtStartDate,GETDATE()),@numDomainId) dtStartDate,dbo.[FormatedDateFromDate](ISNULL(dtEndDate,GETDATE()),@numDomainId) dtEndDate,[numParentStageID],[intDueDays]
      ,[numProjectID],[numOppID],[vcDescription], 
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(numProjectID,numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(numProjectID,numStageDetailsId,1),'Billable Time (0)  Non Billable Time (0)') as numTime,
(select count(*) from dbo.GenericDocuments where numRecID=numStageDetailsId and vcDocumentSection='PS') numDocuments,
(select count(*) from [StagePercentageDetails] where numParentStageId=p.numStageDetailsId and numProjectID=p.numProjectID) as numChildCount,
isnull(bitTimeBudget,0) as bitTimeBudget,isnull(bitExpenseBudget,0) as bitExpenseBudget,isnull(monTimeBudget,0) as monTimeBudget,isnull(monExpenseBudget,0) as monExpenseBudget
  FROM [StagePercentageDetails] p where [numStageDetailsId]=@slpid

ELSE IF  @tintMode = 3
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numProjectID=@slpid and numDomainid=@numDomainid

ELSE IF  @tintMode =4
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numOppID=@slpid and numDomainid=@numDomainid

ELSE
BEGIN
	SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1
END
--    BEGIN              
--        SELECT  numStageDetailsId,
--                numStagePercentageId,
--                numStageNumber,
--                vcStageDetail,
--                sp.numDomainId,
--                sp.numCreatedBy,
--                sp.bintCreatedDate,
--                sp.numModifiedBy,
--                sp.bintModifiedDate,
--                slp_id,
--                sp.numAssignTo,
--                vcStagePercentageDtl,
--                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
--                     ELSE A.vcFirstName + ' ' + A.vcLastName
--                END AS vcAssignTo,
--                numTemplateId,
--                ISNULL(tintPercentage, '') tintPercentage,
--                ISNULL(bitClose, 0) bitClose
--        FROM    stagepercentagedetails sp
--                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
--        WHERE   sp.numdomainid = @numDomainid
--                AND slp_id = @slpid
--        ORDER BY numstagepercentageid,
--                numStageDetailsId ASC              
            
            
   --SELECT * FROM stagepercentagedetails             
            
--    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertStagePercentageDetails]    Script Date: 09/17/2010 17:41:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertstagepercentagedetails')
DROP PROCEDURE usp_insertstagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_InsertStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME, 
    @numParentStageID NUMERIC,
	@numProjectID NUMERIC,
	@numOppID NUMERIC
AS 
DECLARE @numStageDetailId NUMERIC               

DECLARE @sortOrder AS NUMERIC(18,0)=0
SET @sortOrder=(SELECT ISNULL(MAX(numStageOrder),0) FROM StagePercentageDetails WHERE numStagePercentageId=@numStagePercentageId)
SET @sortOrder=@sortOrder+1;
INSERT  INTO [StagePercentageDetails] ( [numStagePercentageId],
                                        [tintConfiguration],
                                        [vcStageName],
                                        [numDomainId],
                                        [numCreatedBy],
                                        [bintCreatedDate],
                                        [numModifiedBy],
                                        [bintModifiedDate],
                                        [slp_id],
                                        [numAssignTo],
                                        [vcMileStoneName],
                                        [tintPercentage],
                                        [vcDescription],dtStartDate,dtEndDate,numParentStageID,numProjectID,numOppID,bitclose,numStageOrder
 )
VALUES  (
          @numStagePercentageId,
          @tintConfiguration,
          @vcStageName,
          @numDomainId,
          @numUserCntID,
          GETUTCDATE(),
          @numUserCntID,
          GETUTCDATE(),
          @slpid,
          @numAssignTo,
          @vcMileStoneName,
          @tintPercentage,
          @vcDescription,@dtStartDate,@dtEndDate,@numParentStageID,@numProjectID,@numOppID,0,@sortOrder
  )

  DECLARE @numTempStagePercentageId AS NUMERIC(18,0) 
  SET @numTempStagePercentageId = SCOPE_IDENTITY()

if((select bitClose from StagePercentageDetails where numStageDetailsId=@numParentStageID)=1)
 update StagePercentageDetails set bitclose=1,tinProgressPercentage=100 where @numParentStageID=numParentStageID and numStageDetailsId=@numTempStagePercentageId

exec usp_ManageStageAccessDetail @numProjectID,@numOppID,@numTempStagePercentageId,@numUserCntID,0

--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint

  
SELECT  @numTempStagePercentageId
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATETIME,
    @dtValidTo AS DATETIME,
	@bitNeverExpires BIT,
	@bitUseForCouponManagement BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,0
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
		IF ISNULL(@bitRequireCouponCode,0) = 1 AND (SELECT 
															COUNT(*) 
														FROM 
															PromotionOffer POInner 
														INNER JOIN 
															DiscountCodes DCInner 
														ON 
															POInner.numProId=DCInner.numPromotionID 
														WHERE 
															POInner.numDomainId=@numDomainId 
															AND ISNULL(POInner.IsOrderBasedPromotion,0) = 1
															AND DCInner.numPromotionID <> @numProId 
															AND DCInner.vcDiscountCode IN (SELECT OutParam FROM dbo.SplitString(@txtCouponCode,','))) > 0
		BEGIN
			RAISERROR ( 'DUPLICATE_COUPON_CODE',16, 1 )
 			RETURN;
		END
		
		IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn 
				AND ISNULL(bitEnabled,0) = 1			 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 AND ISNULL(@bitRequireCouponCode,0) = 0
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
			RAISERROR('RELATIONSHIPPROFILE ALREADY EXISTS',16,1)
			RETURN
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitRequireCouponCode = @bitRequireCouponCode
			,bitUseForCouponManagement=@bitUseForCouponManagement			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		IF ISNULL(@bitUseForCouponManagement,0) = 0
		BEGIN
			DECLARE @Tempdata TABLE
			(
				numPromotionID NUMERIC,
				numDomainID NUMERIC,
				numOrderAmount VARCHAR(MAX),
				fltDiscountValue VARCHAR(MAX)
			)

			INSERT @Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

			;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
			(
				SELECT
					numPromotionID,
					numDomainID,
					LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
					STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
					LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
					STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
				FROM @Tempdata
				UNION all

				SELECT
					numPromotionID,
					numDomainID,
					LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
					STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
					LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
					STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
				FROM tmp
				WHERE
					OrderAmount > '' AND fltDiscountValue > ''
			)

			INSERT INTO PromotionOfferOrder
			SELECT
				numPromotionID,				
				numOrderAmount,
				numfltDiscountValue,
				numDomainID

			FROM tmp
		END
				
		IF @bitRequireCouponCode = 1
		BEGIN
			DECLARE @DiscountTempdata TABLE
			(
				numPromotionID NUMERIC,
				CodeUsageLimit TINYINT,
				vcDiscountCode VARCHAR(MAX)			
			)

			INSERT INTO @DiscountTempdata
			(
				numPromotionID,
				CodeUsageLimit,
				vcDiscountCode
			)
			SELECT 
				@numProId
				,@tintUsageLimit
				,OutParam 
			FROM 
				dbo.SplitString(@txtCouponCode,',')


			DELETE FROM 
				DiscountCodes 
			WHERE 
				numPromotionID = @numProId 
				AND vcDiscountCode NOT IN (SELECT vcDiscountCode FROM @DiscountTempdata)
				AND ISNULL((SELECT COUNT(*) FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DiscountCodes.numDiscountId),0) = 0
			 
			INSERT INTO DiscountCodes
			(
				numPromotionID
				,vcDiscountCode
				,CodeUsageLimit
			)
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM 
				@DiscountTempdata tmp1
			WHERE 
				tmp1.vcDiscountCode NOT IN (SELECT vcDiscountCode FROM DiscountCodes WHERE numPromotionID = @numProId)

			UPDATE DiscountCodes SET CodeUsageLimit = @tintUsageLimit WHERE numPromotionID = @numProId
		END	

        SELECT  @numProId
	END
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageCheckHeader' )
    DROP PROCEDURE USP_ManageCheckHeader
GO
CREATE PROCEDURE USP_ManageCheckHeader
    @tintMode AS TINYINT ,
    @numDomainID NUMERIC(18, 0) ,
    @numCheckHeaderID NUMERIC(18, 0) ,
    @numChartAcntId NUMERIC(18, 0) ,
    @numDivisionID NUMERIC(18, 0) ,
    @monAmount DECIMAL(20,5) ,
    @numCheckNo NUMERIC(18, 0) ,
    @tintReferenceType TINYINT ,
    @numReferenceID NUMERIC(18, 0) ,
    @dtCheckDate DATETIME ,
    @bitIsPrint BIT ,
    @vcMemo VARCHAR(1000) ,
    @numUserCntID NUMERIC(18, 0)
AS --Validation of closed financial year
BEGIN
    EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID, @dtCheckDate
	
    IF @tintMode = 1 --SELECT Particulat Check Header
    BEGIN
        SELECT 
			CH.numCheckHeaderID ,
            CH.numChartAcntId ,
            CH.numDomainID ,
            CH.numDivisionID ,
            CH.monAmount ,
            CH.numCheckNo ,
            CH.tintReferenceType ,
            CH.numReferenceID ,
            CH.dtCheckDate ,
            CH.bitIsPrint ,
            CH.vcMemo ,
            CH.numCreatedBy ,
            CH.dtCreatedDate ,
            CH.numModifiedBy ,
            CH.dtModifiedDate ,
            ISNULL(GJD.numTransactionId, 0) AS numTransactionId
        FROM 
			dbo.CheckHeader CH
        LEFT OUTER JOIN 
			General_Journal_Details GJD 
		ON 
			GJD.tintReferenceType = 1
            AND GJD.numReferenceID = CH.numCheckHeaderID
        WHERE 
			numCheckHeaderID = @numCheckHeaderID
            AND CH.numDomainID = @numDomainID
    END
    ELSE
    IF @tintMode = 2 --Insert/Update Check Header
    BEGIN
        IF @numCheckHeaderID > 0 --Update
        BEGIN
            UPDATE 
				dbo.CheckHeader
            SET 
				numChartAcntId = @numChartAcntId ,
                numDivisionID = @numDivisionID ,
                monAmount = @monAmount ,
                numCheckNo = @numCheckNo ,
                tintReferenceType = @tintReferenceType ,
                numReferenceID = @numReferenceID ,
                dtCheckDate = @dtCheckDate ,
                bitIsPrint = @bitIsPrint ,
                vcMemo = @vcMemo ,
                numModifiedBy = @numUserCntID ,
                dtModifiedDate = GETUTCDATE()
            WHERE 
				numCheckHeaderID = @numCheckHeaderID
                AND numDomainID = @numDomainID
        END
        ELSE --Insert
        BEGIN
			--Set Default Class If enable User Level Class Accountng 
            DECLARE @numAccountClass AS NUMERIC(18);
            SET @numAccountClass = 0
					  
            IF @tintReferenceType = 1 --Only for Direct Check
            BEGIN
                DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            END
					  
            INSERT  INTO dbo.CheckHeader
            ( 
				numChartAcntId ,
                numDomainID ,
                numDivisionID ,
                monAmount ,
                numCheckNo ,
                tintReferenceType ,
                numReferenceID ,
                dtCheckDate ,
                bitIsPrint ,
                vcMemo ,
                numCreatedBy ,
                dtCreatedDate ,
                numAccountClass
            )
            SELECT 
				@numChartAcntId ,
				@numDomainID ,
				@numDivisionID ,
				@monAmount ,
				@numCheckNo ,
				@tintReferenceType ,
				@numReferenceID ,
				@dtCheckDate ,
				@bitIsPrint ,
				@vcMemo ,
				@numUserCntID ,
				GETUTCDATE() ,
				@numAccountClass

			SET @numCheckHeaderID = SCOPE_IDENTITY()
		END	
			
        SELECT  
			numCheckHeaderID ,
            numChartAcntId ,
            numDomainID ,
            numDivisionID ,
            monAmount ,
            numCheckNo ,
            tintReferenceType ,
            numReferenceID ,
            dtCheckDate ,
            bitIsPrint ,
            vcMemo ,
            numCreatedBy ,
            dtCreatedDate ,
            numModifiedBy ,
            dtModifiedDate
        FROM 
			dbo.CheckHeader
        WHERE 
			numCheckHeaderID = @numCheckHeaderID
            AND numDomainID = @numDomainID	
    END
    ELSE IF @tintMode = 3 --Print Check List
    BEGIN
        SELECT  CH.numCheckHeaderID ,
                CH.numChartAcntId ,
                CH.numDomainID ,
                CH.numDivisionID ,
                CH.monAmount ,
                CH.numCheckNo ,
                CH.tintReferenceType ,
                CH.numReferenceID ,
                CH.dtCheckDate ,
                CH.bitIsPrint ,
                CH.vcMemo ,
                CH.numCreatedBy ,
                CH.dtCreatedDate ,
                CH.numModifiedBy ,
                CH.dtModifiedDate ,
                CASE CH.tintReferenceType
                    WHEN 11
                    THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
                                                    0))
                    ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
                                                    0))
                END AS vcCompanyName ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN 'Checks'
                    WHEN 8 THEN 'Bill Payment'
                    WHEN 10 THEN 'RMA'
                    WHEN 11 THEN 'Payroll'
                END AS vcReferenceType ,
                dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN ''
                    WHEN 8
                    THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                    ','
                                                    + CASE
                                                    WHEN ISNULL(BPD.numBillID,
                                                    0) > 0
                                                    THEN 'Bill-'
                                                    + CAST(BPD.numBillID AS VARCHAR(18))
                                                    ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
                                                    END
                                                    FROM
                                                    BillPaymentHeader BPH
                                                    JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                    LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                    WHERE
                                                    BPH.numDomainID = CH.numDomainID
                                                    AND BPH.numBillPaymentID = CH.numReferenceID
                                                    FOR
                                                    XML
                                                    PATH('')
                                                    ), 2, 200000)
                                        ), '')
                        )
                END AS vcBizDoc ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN ''
                    WHEN 8
                    THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                    ','
                                                    + CASE
                                                    WHEN BPD.numOppBizDocsID > 0
                                                    THEN CAST(ISNULL(OBD.vcRefOrderNo,
                                                    '') AS VARCHAR(18))
                                                    WHEN BPD.numBillID > 0
                                                    THEN CAST(ISNULL(BH.vcReference,
                                                    '') AS VARCHAR(500))
                                                    END
                                                    FROM
                                                    BillPaymentHeader BPH
                                                    JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                    LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                    LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
                                                    WHERE
                                                    BPH.numDomainID = CH.numDomainID
                                                    AND BPH.numBillPaymentID = CH.numReferenceID
                                                    FOR
                                                    XML
                                                    PATH('')
                                                    ), 2, 200000)
                                        ), '')
                        )
                    ELSE ''
                END AS vcRefOrderNo
        FROM    dbo.CheckHeader CH
                LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID
                                                AND CH.tintReferenceType = 11
        WHERE   CH.numDomainID = @numDomainID
                AND ( numCheckHeaderID = @numCheckHeaderID
                        OR @numCheckHeaderID = 0
                    )
                AND ( CH.tintReferenceType = @tintReferenceType
                        OR @tintReferenceType = 0
                    )
                AND ( CH.numReferenceID = @numReferenceID
                        OR @numReferenceID = 0
                    )
                AND CH.numChartAcntId = @numChartAcntId
                AND ISNULL(bitIsPrint, 0) = 0	 
    END
	ELSE IF @tintMode = 4 --New Print Check List
	BEGIN
		SELECT  
			CH.numCheckHeaderID ,
			CH.numChartAcntId ,
			CH.numDomainID ,
			CH.numDivisionID ,
			(CASE WHEN tintReferenceType=10 OR tintReferenceType=1 THEN ISNULL(CH.monAmount,0) ELSE  ISNULL([BPD].[monAmount],0) END) monAmount,
			CH.numCheckNo ,
			CH.tintReferenceType ,
			CH.numReferenceID ,
			CH.dtCheckDate ,
			CH.bitIsPrint ,
			ISNULL(CH.vcMemo,'') vcMemo,
			CH.numCreatedBy ,
			CH.dtCreatedDate ,
			CH.numModifiedBy ,
			CH.dtModifiedDate ,
			CASE CH.tintReferenceType
				WHEN 11
				THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
											0))
				ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
											0))
			END AS vcCompanyName ,
			CASE CH.tintReferenceType
				WHEN 1 THEN 'Checks'
				WHEN 8 THEN 'Bill Payment'
				WHEN 10 THEN 'RMA'
				WHEN 11 THEN 'Payroll'
			END AS vcReferenceType ,
			dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
			CASE CH.tintReferenceType
				WHEN 1 THEN ''
				WHEN 8
				THEN CASE WHEN ISNULL(BPD.numBillID, 0) > 0
						THEN 'Bill-'
								+ CAST(BPD.numBillID AS VARCHAR(18))
						ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
					END
			END AS vcBizDoc ,
			CASE CH.tintReferenceType
				WHEN 1 THEN ''
				WHEN 8
				THEN CASE WHEN BPD.numOppBizDocsID > 0
						THEN CAST(ISNULL(OBD.vcRefOrderNo,
											'') AS VARCHAR(18))
						WHEN BPD.numBillID > 0
						THEN CAST(ISNULL(BH.vcReference,
											'') AS VARCHAR(500))
					END
				ELSE ''
			END AS vcRefOrderNo
		FROM  
			dbo.CheckHeader CH
			LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID AND CH.tintReferenceType = 11
			LEFT JOIN BillPaymentHeader BPH ON BPH.numBillPaymentID = CH.numReferenceID AND BPH.numDomainID = CH.numDomainID
			LEFT JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
			LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
			LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
		WHERE   CH.numDomainID = @numDomainID
				AND ([CH].[numDivisionID] = @numDivisionID OR @numDivisionID = 0)
				AND ( numCheckHeaderID = @numCheckHeaderID
						OR @numCheckHeaderID = 0
					)
				AND ( CH.tintReferenceType = @tintReferenceType
						OR @tintReferenceType = 0
					)
				AND ( CH.numReferenceID = @numReferenceID
						OR @numReferenceID = 0
					)
				AND CH.numChartAcntId = @numChartAcntId
				AND ISNULL(bitIsPrint, 0) = 0	
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOffer')
DROP PROCEDURE USP_ManagePromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitUseOrderPromotion BIT,
	@numOrderPromotionID NUMERIC(18,0),
	@tintOfferTriggerValueType TINYINT,
	@fltOfferTriggerValue INT,
	@tintOfferBasedOn TINYINT,
	@tintDiscountType TINYINT,
	@fltDiscountValue FLOAT,
	@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@tintOfferTriggerValueTypeRange TINYINT,
	@fltOfferTriggerValueRange FLOAT,
	@vcShortDesc VARCHAR(100),
	@vcLongDesc VARCHAR(500),
	@tintItemCalDiscount TINYINT
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,0
		   ,0
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			IF @tintOfferBasedOn <> 4
			BEGIN
				IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
				BEGIN
					RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 					RETURN;
				END
			END

			IF (ISNULL(@tintDiscoutBaseOn,0) <> 3 AND ISNULL(@tintDiscoutBaseOn,0) <> 4) AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintDiscoutBaseOn,0) AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END

			IF ISNULL(@bitUseOrderPromotion,0) = 0
			BEGIN
				/*Check For Duplicate Customer-Item relationship 
					1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
				IF @tintCustomersBasedOn = 1
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
					BEGIN
						RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 						RETURN;
					END
				END
			
					/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
				ELSE IF @tintCustomersBasedOn = 2
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
					BEGIN
						RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 						RETURN;
					END
				END

				ELSE IF @tintCustomersBasedOn = 0
				BEGIN
					RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 					RETURN;
				END

				IF @tintCustomersBasedOn = 3
				BEGIN
					DELETE FROM PromotionOfferorganizations 
						WHERE tintType IN (1, 2) 
							AND numProId = @numProId
				END

				IF (
					SELECT
						COUNT(*)
					FROM
						PromotionOffer
					LEFT JOIN
						PromotionOfferOrganizations
					ON
						PromotionOffer.numProId = PromotionOfferOrganizations.numProId
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
					LEFT JOIN
						PromotionOfferItems
					ON
						PromotionOffer.numProId = PromotionOfferItems.numProId
						AND 1 = (CASE 
									WHEN @tintOfferBasedOn=1 THEN (CASE WHEN PromotionOfferItems.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintOfferBasedOn=2 THEN (CASE WHEN PromotionOfferItems.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItems.tintRecordType = 5
					LEFT JOIN
						PromotionOfferItems PromotionOfferItemsDiscount
					ON
						PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn=1 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintDiscoutBaseOn=2 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItemsDiscount.tintRecordType = 6
					WHERE
						PromotionOffer.numDomainId=@numDomainID
						AND PromotionOffer.numProId <> @numProId
						AND tintCustomersBasedOn = @tintCustomersBasedOn
						AND tintDiscoutBaseOn = @tintDiscoutBaseOn
						AND tintOfferBasedOn = @tintOfferBasedOn
						AND tintOfferTriggerValueType = @tintOfferTriggerValueType
						AND 1 = (CASE 
									WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN fltOfferTriggerValue = @fltOfferTriggerValue THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE WHEN fltOfferTriggerValue BETWEEN @fltOfferTriggerValue AND @fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN @fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE 
													WHEN (@fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) OR (@fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									ELSE 0
								END)
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN 3
								END)
					) > 0
				BEGIN
					DECLARE @ERRORMESSAGEVALIDATION AS VARCHAR(50)
					SET @ERRORMESSAGEVALIDATION = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 THEN 'ORGANIZATION-ITEM'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 THEN 'ORGANIZATION-ALLITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 THEN 'RELATIONSHIPPROFILE-ITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 THEN 'ALLORGANIZATIONS-ITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 THEN 'ALLORGANIZATIONS-ALLITEMS'
							END)
					RAISERROR(@ERRORMESSAGEVALIDATION,16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				DELETE FROM PromotionOfferorganizations WHERE numProId=@numProId
			END

			UPDATE 
				PromotionOffer
			SET 
				vcProName = @vcProName
				,numDomainId = @numDomainId
				,dtValidFrom = @dtValidFrom
				,dtValidTo = @dtValidTo
				,bitNeverExpires = @bitNeverExpires
				,bitUseOrderPromotion=@bitUseOrderPromotion
				,numOrderPromotionID=(CASE WHEN ISNULL(@bitUseOrderPromotion,0)=1 THEN @numOrderPromotionID ELSE 0 END)
				,tintOfferTriggerValueType = @tintOfferTriggerValueType
				,fltOfferTriggerValue = @fltOfferTriggerValue
				,tintOfferBasedOn = @tintOfferBasedOn
				,tintDiscountType = @tintDiscountType
				,fltDiscountValue = @fltDiscountValue
				,tintDiscoutBaseOn = @tintDiscoutBaseOn
				,numModifiedBy = @numUserCntID
				,dtModified = GETUTCDATE()
				,tintCustomersBasedOn = @tintCustomersBasedOn
				,tintOfferTriggerValueTypeRange = @tintOfferTriggerValueTypeRange
				,fltOfferTriggerValueRange = @fltOfferTriggerValueRange
				,vcShortDesc = @vcShortDesc
				,vcLongDesc = @vcLongDesc
				,tintItemCalDiscount = @tintItemCalDiscount
				,bitError=0
			WHERE 
				numProId=@numProId
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			UPDATE PromotionOffer SET bitEnabled=0,bitError=1 WHERE numProId=@numProId 

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
        
		SELECT  @numProId
	END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN

	DECLARE @numOppItemID NUMERIC(18,0)
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @numWLocationID AS NUMERIC(18,0)
	DECLARE @monAmount as DECIMAL(20,5) 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @TotalOnHand FLOAT
	DECLARE @monItemAverageCost DECIMAL(20,5)
	DECLARE @monReturnAverageCost DECIMAL(20,5)
	DECLARE @monNewAverageCost DECIMAL(20,5)
	DECLARE @description AS VARCHAR(100)
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
  	
  	SELECT TOP 1 
		@numReturnItemID=numReturnItemID,
		@itemcode=RI.numItemCode,
		@numUnits=RI.numUnitHourReceived,
  		@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
		@monAmount=RI.monTotAmount,
		@numOppItemID=RI.numOppItemID
		,@monItemAverageCost=ISNULL(I.monAverageCost,0)
		,@monReturnAverageCost=ISNULL(OBDI.monAverageCost,RI.monAverageCost)
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON	
		RI.numOppBizDocItemID = OBDI.numOppBizDocItemID               
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID
				                                   
	WHILE @numReturnItemID>0                                        
	BEGIN   
		IF @numWareHouseItemID>0
		BEGIN        
			SELECT @TotalOnHand=SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
		             
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@numWLocationID=ISNULL(numWLocationID,0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			--Receive : SalesReturn 
			IF (@tintFlag=1 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                IF @onBackOrder >= @numUnits 
                BEGIN            
                    SET @onBackOrder = @onBackOrder - @numUnits            
                    SET @onAllocation = @onAllocation + @numUnits            
                END            
                ELSE 
                BEGIN            
                    SET @onAllocation = @onAllocation + @onBackOrder            
                    SET @numUnits = @numUnits - @onBackOrder            
                    SET @onBackOrder = 0            
                    SET @onHand = @onHand + @numUnits            
                END      
				
				IF @TotalOnHand + @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
				END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
		                            
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode   
			END
			--Revert : SalesReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
				IF @onHand  - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - @numUnits
				END
				ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
				BEGIN						
					SET @numUnits = @numUnits - @onHand
                    SET @onHand = 0 
                            
                    SET @onBackOrder = @onBackOrder + @numUnits
                    SET @onAllocation = @onAllocation - @numUnits  
				END	
				ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					SET @numUnits = @numUnits - @onHand
                    SET @onHand = 0 
                            
                    SET @numUnits = @numUnits - @onAllocation  
                    SET @onAllocation = 0 
                            
					SET @onBackOrder = @onBackOrder + @numUnits
				END

				IF @TotalOnHand - @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				                    
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END
			--Revert : PurchaseReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
			BEGIN
				DECLARE @numQtyReturnRemainingToDelete FLOAT
				SET @numQtyReturnRemainingToDelete = @numUnits
				
				IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID)
				BEGIN
					-- DELETE RETURN QTY
					DECLARE @TEMPReturn TABLE
					(
						ID INT,
						numWarehouseItemID NUMERIC(18,0),
						numReturnedQty FLOAT
					)

					INSERT INTO @TEMPReturn
					(
						ID,
						numWarehouseItemID,
						numReturnedQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
						,numWarehouseItemID
						,SUM(numReturnedQty)
					FROM
						OpportunityItemsReceievedLocationReturn OITLR
					INNER JOIN
						OpportunityItemsReceievedLocation OIRL
					ON
						OITLR.numOIRLID = OIRL.ID
					WHERE
						numReturnID=@numReturnHeaderID
						AND numReturnItemID = @numReturnItemID
					GROUP BY
						numWarehouseItemID

					DECLARE @i AS INT = 1
					DECLARE @iCOUNT AS INT
					DECLARE @numQtyReturned FLOAT
					DECLARE @numTempReturnWarehouseItemID NUMERIC(18,0)

					SELECT @iCOUNT = COUNT(*) FROM @TEMPReturn
							
					WHILE @i <= @iCOUNT
					BEGIN
						SELECT 
							@numTempReturnWarehouseItemID=numWarehouseItemID,
							@numQtyReturned=numReturnedQty
						FROM 
							@TEMPReturn
						WHERE
							ID = @i
						
						-- INCREASE THE OnHand Of Destination Location
						UPDATE
							WareHouseItems
						SET
							numBackOrder = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numBackOrder,0) - @numQtyReturned ELSE 0 END),         
							numAllocation = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numAllocation,0) + @numQtyReturned ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
							numOnHand = (CASE WHEN numBackOrder >= @numQtyReturned THEN numOnHand ELSE ISNULL(numOnHand,0) + @numQtyReturned - ISNULL(numBackOrder,0) END),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempReturnWarehouseItemID

						SET @numQtyReturnRemainingToDelete = ISNULL(@numQtyReturnRemainingToDelete,0) - ISNULL(@numQtyReturned,0)

						DECLARE @descriptionInner VARCHAR(MAX)='Purchase Return Delete (Qty:' + CAST(@numQtyReturned AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempReturnWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @descriptionInner, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId

						SET @i = @i + 1
					END

					DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID
				END

				IF ISNULL(@numQtyReturnRemainingToDelete,0) > 0
				BEGIN
					SET @description='Purchase Return Delete (Qty:' + CAST(@numQtyReturnRemainingToDelete AS VARCHAR(10)) + ')'

					IF @onBackOrder >= @numQtyReturnRemainingToDelete 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numQtyReturnRemainingToDelete            
						SET @onAllocation = @onAllocation + @numQtyReturnRemainingToDelete            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numQtyReturnRemainingToDelete = @numQtyReturnRemainingToDelete - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numQtyReturnRemainingToDelete            
					END
				END

				IF @TotalOnHand + @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				               
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END
			--Receive : PurchaseReturn 
			ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
			BEGIN
				DECLARE @numRemainingQtyToReturn AS FLOAT = @numUnits

				-- FIRST TRY TO RETURN QTY FROM OTHER WAREHOUSES WHERE PURCHASE ORDER IS RECIEVED
				IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation OIRL WHERE numOppItemID=@numOppItemID AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= 0)
				BEGIN
					DECLARE @TEMP TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numReturnedQty FLOAT
					)

					INSERT INTO 
						@TEMP
					SELECT 
						ROW_NUMBER() OVER(ORDER BY OIRL.ID),
						OIRL.ID,
						OIRL.numWarehouseItemID,
						OIRL.numUnitReceieved,
						ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
					FROM
						OpportunityItemsReceievedLocation OIRL
					WHERE 
						numOppItemID=@numOppItemID
						AND (OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0
					ORDER BY
						(OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) DESC

					DECLARE @numTempOnHand AS FLOAT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempWarehouseItemID	NUMERIC(18,0)
					DECLARE @numRemaingUnits FLOAT

					DECLARE @j AS INT = 1
					DECLARE @jCOUNT AS INT

					SELECT @jCOUNT = COUNT(*) FROM @TEMP
							
					WHILE @j <= @jCOUNT AND @numRemainingQtyToReturn > 0
					BEGIN
						SELECT 
							@numTempOIRLID=numOIRLID,
							@numTempWarehouseItemID=T1.numWarehouseItemID,
							@numTempOnHand = ISNULL(WI.numOnHand,0),
							@numRemaingUnits=ISNULL(numUnitReceieved,0)-ISNULL(numReturnedQty,0)
						FROM
							@TEMP T1
						INNER JOIN
							WareHouseItems WI
						ON
							T1.numWarehouseItemID=WI.numWareHouseItemID
						WHERE
							ID = @j

						IF @numTempOnHand >= @numRemaingUnits
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = ISNULL(numOnHand,0) - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
							)

							SET @descriptionInner = 'Purchase Return Receive (Qty:' + CAST((CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END) AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
						END
						ELSE IF @numTempOnHand > 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = numOnHand - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
							)

							SET @descriptionInner ='Purchase Return Receive (Qty:' + CAST((CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END) AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
						END

						SET @j = @j + 1
					END
				END

				IF @numRemainingQtyToReturn > 0
				BEGIN
					SET @description='Purchase Return Receive (Qty:' + CAST(@numRemainingQtyToReturn AS VARCHAR(10)) + ')'

					IF @onHand  - @numRemainingQtyToReturn >= 0
					BEGIN						
						SET @onHand = @onHand - @numRemainingQtyToReturn
					END
					ELSE
					BEGIN
						RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						RETURN ;
					END
				END

				IF @TotalOnHand - @numUnits > 0
				BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
				ELSE
				BEGIN
					SET @monNewAverageCost = @monItemAverageCost
				END
				              
				UPDATE 
					Item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
				WHERE 
					numItemCode = @itemcode
			END 

			IF @tintReturnType=1 OR (@tintReturnType=2 AND @numWareHouseItemID > 0 AND (ISNULL(@numRemainingQtyToReturn,0) > 0 OR ISNULL(@numQtyReturnRemainingToDelete,0) > 0))
			BEGIN
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 

				DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainId = @numDomainId
			END         
		END
		  
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=RI.numItemCode,
			@numUnits=RI.numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=RI.monTotAmount,
			@numOppItemID=RI.numOppItemID
			,@monItemAverageCost=ISNULL(I.monAverageCost,0)
			,@monReturnAverageCost=ISNULL(OBDI.monAverageCost,RI.monAverageCost)
		FROM 
			ReturnItems RI 
		JOIN 
			Item I 
		ON 
			RI.numItemCode=I.numItemCode
		LEFT JOIN
			OpportunityBizDocItems OBDI
		ON	
			RI.numOppBizDocItemID = OBDI.numOppBizDocItemID                                
		WHERE 
			numReturnHeaderID=@numReturnHeaderID 
			AND (charitemtype='P' OR 1=(CASE 
										WHEN @tintReturnType=1 THEN 
											CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
												WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
												ELSE 0 
											END 
										ELSE 0 END)) 
			AND RI.numReturnItemID>@numReturnItemID 
		ORDER BY 
			RI.numReturnItemID
							
		IF @@ROWCOUNT=0 SET @numReturnItemID=0         
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSalesprocessList]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managesalesprocesslist')
DROP PROCEDURE usp_managesalesprocesslist
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesprocessList]
    @numSalesProsID AS NUMERIC(9) = 0,
    @vcSlpName AS VARCHAR(50) = '',
    @numUserCntID AS NUMERIC(9) = 0,
    @tintProType AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0,
    @numStageNumber AS NUMERIC = 0,
	@bitAssigntoTeams AS BIT=0,
	@bitAutomaticStartTimer AS BIT=0,
	@numOppId AS NUMERIC(18,0)=0,
	@numTaskValidatorId AS NUMERIC(18,0)=0
AS 
IF @numSalesProsID = 0 
    BEGIN        
        INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId)
        VALUES  (
                  @vcSlpName,
                  @numDomainID,
                  @tintProType,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE(),@numStageNumber,@bitAutomaticStartTimer,@bitAssigntoTeams,@numOppId,@numTaskValidatorId
                )        
        SET @numSalesProsID = SCOPE_IDENTITY()        
 
        SELECT  @numSalesProsID     
    END        
ELSE 
    BEGIN   
		IF(@numOppId>0)
		BEGIN
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
		ELSE
		BEGIN  
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName,
					numModifedby = @numUserCntID,
					dtModifiedOn = GETUTCDATE(),
					bitAssigntoTeams=@bitAssigntoTeams,
					bitAutomaticStartTimer=@bitAutomaticStartTimer,
					numTaskValidatorId=@numTaskValidatorId
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageWarehouse]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managewarehouse')
DROP PROCEDURE usp_managewarehouse
GO
CREATE PROCEDURE [dbo].[USP_ManageWarehouse]      
@byteMode as tinyint=0,      
@numWareHouseID as numeric(9)=0,      
@vcWarehouse as varchar(50),          
@numDomainID as numeric(9)=0,
@numAddressID As numeric(18,0) = 0,
@vcPrintNodeAPIKey AS VARCHAR(100) = '',
@vcPrintNodePrinterID AS VARCHAR(100) = ''
AS      
BEGIN      
	IF @byteMode=0      
	BEGIN      
		IF @numWareHouseID=0      
		BEGIN
			INSERT INTO Warehouses 
			(
				vcWarehouse
				,numDomainID
				,numAddressID
				,vcPrintNodeAPIKey
				,vcPrintNodePrinterID	
			)      
			VALUES 
			(
				@vcWarehouse
				,@numDomainID
				,@numAddressID
				,@vcPrintNodeAPIKey
				,@vcPrintNodePrinterID
			)      
        
			DECLARE @numNewWarehouseID NUMERIC(18,0)
			SET @numNewWarehouseID = SCOPE_IDENTITY()


			INSERT INTO WareHouseItems 
			(
				numItemID, 
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified
			) 
			SELECT
				numItemCode
				,@numNewWarehouseID
				,NULL
				,0
				,0
				,monListPrice
				,''
				,vcSKU
				,numBarCodeId
				,@numDomainID
				,GETDATE()
			FROM
				Item
			WHERE
				numDomainID=@numDomainID
				AND charItemType='P'
				AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)

			INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  
				WHI.numWareHouseItemID
				,ISNULL(WHI.numOnHand,0)
				,ISNULL(WHI.numOnOrder,0)
				,ISNULL(WHI.numReorder,0)
				,ISNULL(WHI.numAllocation,0)
				,ISNULL(WHI.numBackOrder,0)
				,WHI.numDomainID
				,'INSERT WareHouse'
				,1
				,I.numItemCode
				,GETUTCDATE()
				,0
				,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)
				,(SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode)
				,GETUTCDATE()
            FROM 
				Item I 
			INNER JOIN 
				WareHouseItems WHI 
			ON 
				I.numItemCode=WHI.numItemID
			WHERE 
				WHI.numWareHouseID = @numNewWarehouseID
				AND [WHI].[numDomainID] = @numDomainID


			SELECT @numNewWarehouseID     
		END      
		ELSE IF @numWareHouseID>0      
		BEGIN
			UPDATE 
				Warehouses 
			SET    
				vcWarehouse= @vcWarehouse
				,numAddressID=@numAddressID
				,vcPrintNodeAPIKey=@vcPrintNodeAPIKey
				,vcPrintNodePrinterID=@vcPrintNodePrinterID
			WHERE 
				numWareHouseID=@numWareHouseID      
		
			SELECT @numWareHouseID      
		END      
	END
	ELSE IF @byteMode=1      
	BEGIN     
		IF (SELECT COUNT(*) FROM [WareHouseItems] WHERE numWarehouseID = @numWareHouseID  AND (ISNULL(numOnHand,0) > 0 OR ISNULL(numOnOrder,0) > 0 OR ISNULL(numAllocation,0) > 0 OR ISNULL(numBackOrder,0) > 0)) > 0
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE IF EXISTS (SELECT OI.numoppitemtCode FROM WareHouseItems WI INNER JOIN OpportunityItems OI ON WI.numWareHouseItemID=OI.numWarehouseItmsID WHERE WI.numWareHouseID=@numWareHouseID)
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE
		BEGIN
			DELETE FROM WareHouseItems WHERE ISNULL(numWareHouseID,0)=ISNULL(@numWareHouseID,0)
			DELETE FROM Warehouses WHERE numWareHouseID=@numWareHouseID	
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name AS vcProcessName
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID=SLP.Slp_Id
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE PO.tintCustomersBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									WHEN 3 THEN 1
									ELSE 0
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN							
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				KitChildItems VARCHAR(MAX)
			)

			INSERT INTO 
				@TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0

			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			           		        
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems OI
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @j INT = 1
		DECLARE @jCount INT

		--DECLARE @numCouponPromotionID NUMERIC(18,0)
		--DECLARE @tintOfferTriggerValueType TINYINT
		--DECLARE @tintOfferTriggerValueTypeRange TINYINT
		--DECLARE @fltOfferTriggerValue FLOAT
		--DECLARE @fltOfferTriggerValueRange FLOAT

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			-- IF USER SELECTED PROMOTION HE/SHE WANT TO USE THEN FIRST WE HAVE TO VALIDAE IT AND MARK IT AS TRIGGER
			UPDATE
				T1
			SET
				bitChanged = 1
				,numPromotionID=PO.numProId
				,bitPromotionTriggered=1
				,vcPromotionDescription=ISNULL(PO.vcShortDesc,'')
			FROM
				@TEMP T1
			INNER JOIN
				PromotionOffer PO 
			ON
				T1.numSelectedPromotionID = PO.numProId
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				T1.numSelectedPromotionID > 0
				AND ISNULL(T1.numPromotionID,0)  = 0
				AND PO.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
				AND 1 = (CASE 
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
						END)

			UPDATE
				T1
			SET
				bitChanged = 1
				,numPromotionID=T2.numProId
				,bitPromotionTriggered=1
				,vcPromotionDescription=vcShortDesc
			FROM
				@TEMP T1
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				INNER JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN 
					DiscountCodes DC
				ON 
					POOrder.numProId = DC.numPromotionID
				WHERE
					PO.numDomainId=@numDomainID 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
					AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					AND DC.vcDiscountCode = @vcDiscountCode
					AND 1 = (CASE 
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(T1.numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(T1.numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(T1.monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(T1.monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
						END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(numPromotionID,0)  = 0
		END
		
		INSERT INTO @TEMPPromotion
		(
			numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			
			SET @numItemClassification = ISNULL((SELECT numItemClassification FROM Item WHERE Item.numItemCode = @numItemClassification),0)

			-- FIRST CHECK WHETHER PROMOTION IS STILL VALID
			IF EXISTS (SELECT 
							numProId 
						FROM 
							PromotionOffer PO
						LEFT JOIN	
							DiscountCodes DC
						ON
							PO.numOrderPromotionID = DC.numPromotionID
						WHERE 
							numDomainId=@numDomainID 
							AND numProId=@numTempPromotionID
							AND ISNULL(bitEnabled,0)=1 
							AND ISNULL(IsOrderBasedPromotion,0) = 0
							AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
							AND 1 = (CASE 
										WHEN ISNULL(numOrderPromotionID,0) > 0 
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										ELSE
											(CASE tintCustomersBasedOn 
												WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
												WHEN 3 THEN 1
												ELSE 0
											END)
									END)
							AND 1 = (CASE 
										WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
										WHEN tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
							AND 1 = (CASE 
										WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@numUnits,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
										WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
									END)
							AND 1 = (CASE 
										WHEN ISNULL(numOrderPromotionID,0) > 0 
										THEN 
											(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
										ELSE 1 
									END)
					)
			BEGIN
				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN T1.numUnits >= @numRemainingPromotion 
																THEN (@numRemainingPromotion * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
																ELSE (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=0
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR ISNULL(bitPromotionTriggered,0) = 1)
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END
			END
			ELSE
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END
			
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ClearCouponCodeItemPromotion')
DROP PROCEDURE USP_PromotionOffer_ClearCouponCodeItemPromotion
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ClearCouponCodeItemPromotion]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX)
AS
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
		)

		EXEC sp_xml_removedocument @hDocItem

		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
		WHERE 
			ISNULL(numPromotionID,0)=@numPromotionID
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetCouponBasedOrderPromotion')
DROP PROCEDURE USP_PromotionOffer_GetCouponBasedOrderPromotion
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetCouponBasedOrderPromotion]
    @numDomainId AS NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numProId
		,vcProName
	FROM
		PromotionOffer
	WHERE
		numDomainId=@numDomainId
		AND ISNULL(bitEnabled,0) = 1
		AND ISNULL(IsOrderBasedPromotion,0) = 1
		AND ISNULL(bitRequireCouponCode,0) = 1
	ORDER BY
		vcProName
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetPromotionApplicableToItem')
DROP PROCEDURE USP_PromotionOffer_GetPromotionApplicableToItem
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetPromotionApplicableToItem]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification NUMERIC(18,0)

	SELECT 
		@numItemClassification=ISNULL(numItemClassification,0) 
	FROM 
		Item 
	WHERE 
		numDomainID=@numDomainID 
		AND numItemCode=@numItemCode

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID

	SELECT
		PO.numProId
		,PO.vcProName
		,CONCAT(ISNULL(PO.vcShortDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcShortDesc
		,CONCAT(ISNULL(PO.vcLongDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcLongDesc
	FROM 
		PromotionOffer PO
	LEFT JOIN
		PromotionOffer POOrder
	ON
		PO.numOrderPromotionID = POOrder.numProId
	WHERE 
		PO.numDomainId=@numDomainID 
		AND ISNULL(PO.bitEnabled,0)=1 
		AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				END)
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN
						(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					ELSE
						(CASE PO.tintCustomersBasedOn 
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN 1
							ELSE 0
						END)
				END)
		AND 1 = (CASE 
					WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ReturnItems_GetForReturnJournal' ) 
    DROP PROCEDURE USP_ReturnItems_GetForReturnJournal
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_ReturnItems_GetForReturnJournal]
(
    @numReturnHeaderID NUMERIC(18,0),
    @numDomainId NUMERIC(18,0)
)
AS 
BEGIN
    SELECT  
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID=RI.numOppBizDocItemID),isnull(i.monAverageCost,'0')) END) as AverageCost
		,CONVERT(DECIMAL(20,5),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,0 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
	UNION ALL
	SELECT 
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0) numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(OBDKI.monAverageCost,isnull(i.monAverageCost,0)) END) as AverageCost
		,0 AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,1 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		INNER JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN dbo.OpportunityBizDocKitItems OBDKI ON RI.numOppBizDocItemID = OBDKI.numOppBizDocItemID
		INNER JOIN dbo.OpportunityKitItems OKI ON OBDKI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		INNER JOIN Item I ON I.numItemCode = OBDKI.numChildItemID
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
		AND ISNULL(I.bitKitParent,0) = 0
	UNION ALL
	SELECT 
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived * ISNULL(OKCI.numQtyItemsReq_Orig,0) numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(OBDKCI.monAverageCost,isnull(i.monAverageCost,0)) END) as AverageCost
		,0 AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,2 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		INNER JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN dbo.OpportunityBizDocKitChildItems OBDKCI ON RI.numOppBizDocItemID = OBDKCI.numOppBizDocItemID
		INNER JOIN dbo.OpportunityKitChildItems OKCI ON OBDKCI.numOppKitChildItemID = OKCI.numOppKitChildItemID
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		INNER JOIN Item I ON I.numItemCode = OBDKCI.numChildItemID
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
		AND ISNULL(I.bitKitParent,0) = 0
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingPackageType_GetByShippingCompany')
DROP PROCEDURE USP_ShippingPackageType_GetByShippingCompany
GO
CREATE PROCEDURE [dbo].[USP_ShippingPackageType_GetByShippingCompany]  
	@numShippingCompanyID NUMERIC(18,0)
	,@bitEndicia BIT
AS  
BEGIN  
	SELECT
		ID
		,numNsoftwarePackageTypeID
		,vcPackageName
	FROM
		ShippingPackageType
	WHERE
		(numShippingCompanyID=ISNULL(@numShippingCompanyID,0) OR @numShippingCompanyID = 0)
		AND 1 = (CASE 
					WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
					ELSE (CASE WHEN ISNULL(bitEndicia,0) = @bitEndicia THEN 1 ELSE 0 END) 
				END)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatestagepercentagedetails')
DROP PROCEDURE usp_updatestagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_UpdateStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
    @numStageDetailsIds AS NUMERIC(9),
    @bitClose AS BIT,
    @numParentStageID NUMERIC,
    @intDueDays int,
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME,
	@bitTimeBudget as bit,
	@bitExpenseBudget as bit,
	@monTimeBudget as DECIMAL(20,5),
	@monExpenseBudget as DECIMAL(20,5),
	@numSetPrevStageProgress AS NUMERIC = 0,
	@bitIsDueDaysUsed AS BIT,
	@numTeamId AS NUMERIC,
	@bitRunningDynamicMode AS BIT,
	@numStageOrder AS NUMERIC(18,0)=0
AS 

IF @numSetPrevStageProgress > 0
BEGIN
	UPDATE [StagePercentageDetails] 
		SET tinProgressPercentage = 100
		WHERE   numdomainid = @numDomainid
                AND numStageDetailsId < @numSetPrevStageProgress
                AND slp_id = @slpid
				AND tinProgressPercentage < 100

END


UPDATE  [StagePercentageDetails]
SET     [numStagePercentageId] = @numStagePercentageId,
        [tintConfiguration] = @tintConfiguration,
        [vcStageName] = @vcStageName,
        [numModifiedBy] = @numUserCntID,
        [bintModifiedDate] = GETUTCDATE(),
        [numAssignTo] = (case @numAssignTo when -1 then numAssignTo else @numAssignTo end),
		[tintPercentage] = (case @numAssignTo when -1 then tintPercentage else @tintPercentage end),
		[tinProgressPercentage] = (case @numAssignTo when -1 then @tintPercentage else tinProgressPercentage end), 
        [vcMileStoneName] = @vcMileStoneName,
        [vcDescription] = @vcDescription,
        bitClose = @bitClose,
        numParentStageID = @numParentStageID,
        intDueDays = @intDueDays,
		dtStartDate = @dtStartDate,
		dtEndDate = @dtEndDate,bitTimeBudget=@bitTimeBudget,bitExpenseBudget=@bitExpenseBudget,
		monTimeBudget=@monTimeBudget,monExpenseBudget=@monExpenseBudget,bitIsDueDaysUsed=@bitIsDueDaysUsed,
		numTeamId=@numTeamId,bitRunningDynamicMode=@bitRunningDynamicMode,numStageOrder=@numStageOrder
WHERE   numStageDetailsId = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId

if @bitClose=1
BEGIN
 update  [StagePercentageDetails] set bitClose = @bitClose,tinProgressPercentage=@tintPercentage where numParentStageID  = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId
END 


if @numParentStageID>0
BEGIN
Declare @TotalR  numeric,@TotalP  numeric

  select @TotalR = count(*) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
  select @TotalP = sum(tinProgressPercentage) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
	
  if @TotalP = @TotalR*100
	Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numStageDetailsId=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
END


DECLARE @numProjectID NUMERIC
DECLARE @numOppID NUMERIC
SELECT @numProjectID=numProjectID,@numOppID=numOppID FROM dbo.StagePercentageDetails WHERE [numStageDetailsId]=@numStageDetailsIds


--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint

GO
--created by Prasanta Pradhan
--select dbo.fn_calculateTotalHoursMinutesByStageDetails(62055,717)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calculateTotalHoursMinutesByStageDetails')
DROP FUNCTION fn_calculateTotalHoursMinutesByStageDetails
GO
CREATE FUNCTION [dbo].[fn_calculateTotalHoursMinutesByStageDetails]
(
@numStageDetailsId As NUMERIC=0,
@numOppId As NUMERIC(18,0)=0
)
Returns VARCHAR(500) 
as
begin
DECLARE @totalDays AS NUMERIC(18,2)=0
DECLARE @totalHours AS NUMERIC(18,2)=0
DECLARE @totalMinutes AS NUMERIC(18,2)=0

SELECT @totalDays=MAX([Days]) FROM 
(SELECT ISNULL(numAssignTo,0) AS numAssignTo,
FLOOR(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0))/480) AS [Days],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 480)/60 AS [Hours],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 60) AS [Minutes]
FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numStageDetailsId AND bitSavedTask=1 AND numOppId=@numOppId
GROUP BY numAssignTo) AS T

SELECT @totalHours=FLOOR(SUM([Hours])),@totalMinutes=FLOOR(SUM([Minutes])) FROM 
(SELECT ISNULL(numAssignTo,0) AS numAssignTo,
FLOOR(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0))/480) AS [Days],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 480)/60 AS [Hours],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 60) AS [Minutes]
FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numStageDetailsId AND bitSavedTask=1 AND numOppId=@numOppId
GROUP BY numAssignTo) AS T

RETURN CAST(@totalDays AS varchar)+'_'+CAST(@totalHours AS varchar)+'_'+CAST(@totalMinutes AS varchar)
end
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteProcessFromOpportunity')
DROP PROCEDURE USP_DeleteProcessFromOpportunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteProcessFromOpportunity] 
@numOppId AS NUMERIC(18,0)=0
as    
BEGIN   
	IF(@numOppId>0)
	BEGIN
		UPDATE dbo.OpportunityMaster SET numBusinessProcessID=NULL WHERE numOppID=@numOppId
	END
	DELETE FROM StagePercentageDetails WHERE numOppId=@numOppId
	DELETE FROM ProjectProcessStageDetails WHERE numOppId=@numOppId
	
	DELETE FROM StagePercentageDetailsTask WHERE numOppId=@numOppId
	DELETE FROM Sales_process_List_Master WHERE numOppId=@numOppId
	
END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @bitHasChildKits BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=ISNULL(tintPriceBookDiscount,0) FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1),
		@bitKit=ISNULL(bitKitParent,0),
		@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numItemCode,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);						
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscount = 1 -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
			ELSE -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitKit AS bitKitParent,
		@bitHasChildKits AS bitHasChildKits,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes
END 
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskByStageDetailsId')
DROP PROCEDURE USP_GetTaskByStageDetailsId
GO
CREATE PROCEDURE [dbo].[USP_GetTaskByStageDetailsId]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@vcSearchText AS VARCHAR(MAX)='',
@bitAlphabetical AS BIT=0
as    
BEGIN   
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=NULL
	SET @dynamicQuery = 'SELECT 
		ST.numTaskId,
		ST.vcTaskName,
		ST.numHours,
		ST.numMinutes,
		ST.numAssignTo,
		ISNULL(AC.vcFirstName+'' ''+AC.vcLastName,''-'') As vcContactName,
		ISNULL(ST.bitDefaultTask,0) AS bitDefaultTask,
		ISNULL(ST.bitTaskClosed,0) AS bitTaskClosed,
		ISNULL(ST.bitSavedTask,0) AS bitSavedTask,
		ISNULL(ST.numParentTaskId,0) AS numParentTaskId,
		ISNULL(AU.vcFirstName+'' ''+AU.vcLastName,''-'') As vcUpdatedByContactName,
		convert(varchar(10),ST.dtmUpdatedOn, 105) + right(convert(varchar(32),ST.dtmUpdatedOn,100),8) As dtmUpdatedOn,
		 convert(varchar(10),PPD.dtmStartDate, 101) + right(convert(varchar(32),PPD.dtmStartDate,100),8) AS StageStartDate,
		ST.numStageDetailsId,
		AC.numTeam
	FROM
		StagePercentageDetailsTask AS ST
	LEFT JOIN 
		AdditionalContactsInformation AS AC
	ON
		ST.numAssignTo=AC.numContactId
	LEFT JOIN 
		AdditionalContactsInformation AS AU
	ON
		ST.numCreatedBy=AU.numContactId
	LEFT JOIN
		ProjectProcessStageDetails AS PPD
	ON
		ST.numStageDetailsId=PPD.numStageDetailsId AND ST.numOppId=PPD.numOppId
	WHERE
		ST.numDomainID='+CAST(@numDomainID AS VARCHAR(100))+' '
	IF(@numStageDetailsId>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.numStageDetailsId='+CAST(@numStageDetailsId AS VARCHAR(100))+' '
	END
	SET @dynamicQuery=@dynamicQuery+' AND ST.numOppId='+CAST(@numOppId AS VARCHAR(100))+' '
	SET @dynamicQuery=@dynamicQuery+' AND ST.numProjectId='+CAST(@numProjectId AS VARCHAR(100))+' '
	
	IF(LEN(@vcSearchText)>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.vcTaskName LIKE ''%'+CAST(@vcSearchText AS VARCHAR(100))+'%'' '
	END
	IF(@bitAlphabetical=1)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.vcTaskName ASC '
	END
	ELSE
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.numOrder ASC '
	END
	EXEC(@dynamicQuery)
END 
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskDataForGanttChart')
DROP PROCEDURE USP_GetTaskDataForGanttChart
GO
CREATE PROCEDURE [dbo].[USP_GetTaskDataForGanttChart]
@numDomainID as numeric(9)=0,    
@numOppId AS NUMERIC(18,0)=0
as    
BEGIN   
SELECT 
	S.vcMileStoneName AS MileStoneName,
	S.vcStageName,
	T.vcTaskName,
	S.numStageDetailsId,
	T.numTaskId,
	CAST(S.tintPercentage As INT) AS tinProgressPercentage,
	T.bitSavedTask,
	T.bitTaskClosed,
	S.bitIsDueDaysUsed,
	T.numHours,
	CONVERT(VARCHAR, T.numHours / 8) AS TaskDays,
	CASE WHEN T.numAssignTo>0 THEN (SELECT TOP 1 vcFirstName+' '+vcLastName FROM AdditionalContactsInformation WHERE numContactId=T.numAssignTo) ELSE '-' END AS vcContactName,
	S.dtStartDate As StageStartDate,
	CAST(CASE WHEN S.bitIsDueDaysUsed=1 THEN DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE))
		ELSE DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId),'_')),CAST(S.dtStartDate AS DATE)) END AS DATETIME) AS StageEndDate
FROM 
	StagePercentageDetails AS S,
	StagePercentageDetailsTask AS T
	LEFT JOIN ProjectProcessStageDetails AS PPD
	ON T.numStageDetailsId=PPD.numStageDetailsId AND PPD.numOppId=@numOppId
WHERE
	T.numStageDetailsId=S.numStageDetailsId AND
	S.numOppID=@numOppId AND
	T.numOppId=@numOppId AND
	T.bitSavedTask=1 AND
	S.numDomainId=@numDomainID AND
	T.numDomainID=@numDomainID
ORDER BY
	S.vcMileStoneName,
	S.numStageOrder,
	T.numOrder
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetTaskForUserActivity]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskForUserActivity')
DROP PROCEDURE USP_GetTaskForUserActivity
GO
CREATE PROCEDURE [dbo].[USP_GetTaskForUserActivity]
@numUserCntId as numeric(9)=0,    
@numDomainID as numeric(18)=0
as    
BEGIN   
	SELECT T.dtmDueDate,T.vcMileStoneName,T.OrderType,T.vcStageName,T.vcPOppName,T.vcTaskName,T.numOppID,
	T.numStageDetailsId,T.numOppID,T.numStagePercentageId,T.numStagePercentage AS tintPercentage,T.tintConfiguration FROM (
	SELECT 
		T.vcTaskName,
		CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 103)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId),'_')),CAST(S.dtStartDate AS DATE)), 103) END AS dtmDueDate,
		CASE WHEN OP.tintOppType=0 THEN 'Sales Opportunity' 
			 WHEN OP.tintOppType=1 THEN 'Sales Order' WHEN OP.tintOppType=2 THEN 'Purchase Order' ELSE '' END AS OrderType,
			 OP.vcPOppName,
			 S.vcMileStoneName,
			 S.vcStageName,
			 T.numStageDetailsId,
			 S.numOppID,
			 S.numStagePercentageId,
			 SM.numStagePercentage,
			 S.tintConfiguration
	FROM StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId
	LEFT JOIN ProjectProcessStageDetails AS PPD
	ON T.numStageDetailsId=PPD.numStageDetailsId AND T.numOppId=PPD.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId
	WHERE T.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND T.numDomainID=@numDomainID AND
	1=(CASE WHEN T.numAssignTo=@numUserCntId OR (S.numTeamId>0 
	AND @numUserCntId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam=S.numTeamId)) THEN 1 ELSE 0 END)) AS T 
END

SELECT * FROM StagePercentageMaster
SELECT * FROM ProjectProcessStageDetails

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProcesstoOpportunity')
DROP PROCEDURE USP_ManageProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_ManageProcesstoOpportunity]
@numDomainID as numeric(9)=0,    
@numProcessId as numeric(18)=0,      
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN
 INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId)
     SELECT Slp_Name,
            numdomainid,
            pro_type,
            numCreatedby,
            dtCreatedon,
            numModifedby,
            dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,@numOppId,numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=@numProcessId    
	DECLARE @numNewProcessId AS NUMERIC(18,0)=0
	SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
	IF(@numOppId>0)
	BEGIN
		UPDATE OpportunityMaster SET numBusinessProcessID=@numNewProcessId WHERE numOppId=@numOppId
	END
	
	INSERT INTO StagePercentageDetails(
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		numCreatedBy, 
		bintCreatedDate, 
		numModifiedBy, 
		bintModifiedDate, 
		slp_id, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		dtStartDate, 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		numOppID, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder
	)
	SELECT
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		@numCreatedBy, 
		GETDATE(), 
		@numCreatedBy, 
		GETDATE(), 
		@numNewProcessId, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		GETDATE(), 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		@numOppId, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder
	FROM
		StagePercentageDetails
	WHERE
		slp_id=@numProcessId	

	INSERT INTO StagePercentageDetailsTask(
		numStageDetailsId, 
		vcTaskName, 
		numHours, 
		numMinutes, 
		numAssignTo, 
		numDomainID, 
		numCreatedBy, 
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask,
		numOrder
	)
	SELECT 
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numOppID=@numOppId),
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		@numDomainID,
		@numCreatedBy,
		GETDATE(),
		@numOppId,
		@numProjectId,
		0,
		1,
		CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
		numOrder
	FROM 
		StagePercentageDetailsTask AS ST
	LEFT JOIN
		StagePercentageDetails As SP
	ON
		ST.numStageDetailsId=SP.numStageDetailsId
	WHERE
		ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
		ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId)
	ORDER BY ST.numOrder

END 

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0,
@bitAutoClosedTaskConfirmed As BIT=0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask
		)
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		DECLARE @numTaskValidatorId AS NUMERIC(18,0)=0
		DECLARE @numTaskSlNo AS NUMERIC(18,0)=0
		DECLARE @numStageOrder AS NUMERIC(18,0)=0
		DECLARE @numStagePercentageId AS NUMERIC(18,0)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask,@numTaskSlNo=numOrder
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
		SELECT @numTaskValidatorId=numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=(SELECT TOP 1 slp_id FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)

		DECLARE @bitRunningDynamicMode AS BIT=0
		SELECT TOP 1 
			@bitRunningDynamicMode=bitRunningDynamicMode,
			@numStageOrder=numStageOrder ,
			@numStagePercentageId=numStagePercentageId
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numStageDetailsId
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND @numOppId>0)
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask
			)
			SELECT 
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1 
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=(SELECT TOP 1 numTaskId FROM StagePercentageDetailsTask WHERE numOppId=0 AND numStageDetailsId=@numStageDetailsId AND vcTaskName=@vcRunningTaskName) AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) 					
			END

	
					
		END
		IF((@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=2 AND @bitTaskClosed=1) OR (@numOppId>0 AND @numTaskValidatorId=1 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
				bitTaskClosed=1
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
				bitTaskClosed=1
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
		END
		IF((@numOppId>0 AND @numTaskValidatorId=3 AND @bitTaskClosed=1) OR (@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=4 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
				bitTaskClosed=1
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
				bitTaskClosed=1
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
			IF(ISNULL(@numStageOrder,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder<@numStageOrder AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId<@numStageDetailsId AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId<@numStagePercentageId AND numOppId=@numOppId)
		END
	
		IF(@numAssignTo>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numHours>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numHours=@numHours
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numMinutes>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numMinutes=@numMinutes
			WHERE
				numTaskId=@numTaskId
		END
		IF(LEN(@vcTaskName)>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				vcTaskName=@vcTaskName
			WHERE
				numTaskId=@numTaskId
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_EnableDisable')
DROP PROCEDURE USP_PromotionOffer_EnableDisable
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_EnableDisable]
	@numDomainID NUMERIC(18,0),
	@numProId NUMERIC(18,0)
AS
BEGIN
	IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId AND ISNULL(bitEnabled,0) = 0 AND ISNULL(bitError,0) = 1)
	BEGIN
		RAISERROR('PROMOTION_CONFIGURATION_INVALID',16,1)
		RETURN
	END
	ELSE
	BEGIN
		UPDATE
			PromotionOffer
		SET
			bitEnabled = (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 0 ELSE 1 END)
		WHERE	
			numDomainId=@numDomainID
			AND numProId=@numProId
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOpportunityDealStatusConclusion')
DROP PROCEDURE USP_UpdateOpportunityDealStatusConclusion
GO
CREATE PROCEDURE [dbo].[USP_UpdateOpportunityDealStatusConclusion] 
( 
 @numDomainID NUMERIC(9),
 @numOppId NUMERIC(9),
 @tintOppStatus AS INT = 0,
 @lngPConclAnalysis NUMERIC(18,0)=0,
 @numUserCntID NUMERIC(18,0)=0
)
AS 
BEGIN
	IF(@tintOppStatus>0)
	BEGIN
		DECLARE @bitViolatePrice BIT
		DECLARE @intPendingApprovePending INT
		DECLARE @intExecuteDiv INT=0
		DECLARE @numDivisionID NUMERIC=0
		
		IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
		BEGIN
			SET @bitViolatePrice = 1
		END
		SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

		IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @tintOppStatus='1')
		BEGIN
			SET @intExecuteDiv=1
		END

		IF ISNULL(@intExecuteDiv,0)=0
		BEGIN
			DECLARE @tintOldOppStatus AS TINYINT
			SELECT @tintOldOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

			IF @tintOldOppStatus=0 AND @tintOppStatus='1' --Open to Won
			BEGIN
				SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
				DECLARE @tintCRMType AS TINYINT      
				SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

				-- Promote Lead to Account when Sales/Purchase Order is created against it
				IF @tintCRMType=0 --Lead & Order
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintLeadProm=GETUTCDATE()
						,bintLeadPromBy=@numUserCntID
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END
				--Promote Prospect to Account
				ELSE IF @tintCRMType=1 
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END    

		 		EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

				UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
			END
			ELSE IF (@tintOppStatus='2') -- Win to Lost
			BEGIN
				UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
			END
				
			IF (@tintOldOppStatus=1 and @tintOppStatus='2') or (@tintOldOppStatus=1 and @tintOppStatus='0') --Won to Lost or Won to Open
					EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

			UPDATE OpportunityMaster SET tintOppStatus = @tintOppStatus WHERE numOppId=@numOppID
				
		END
		--UPDATE 
		--	OpportunityMaster
		--SET
		--	tintOppStatus=@tintOppStatus
		--WHERE
		--	numDomainId=@numDomainID AND
		--	numOppId=@numOppId
	END
	IF(@lngPConclAnalysis>0)
	BEGIN
		UPDATE 
			OpportunityMaster
		SET
			lngPConclAnalysis=@lngPConclAnalysis
		WHERE
			numDomainId=@numDomainID AND
			numOppId=@numOppId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateStartDateProcesstoOpportunity')
DROP PROCEDURE USP_UpdateStartDateProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_UpdateStartDateProcesstoOpportunity]
@numStageDetailsId as numeric(18)=0,   
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@dtmStartDate AS DATETIME=NULL
as    
BEGIN   
	UPDATE 
		StagePercentageDetails
	SET
		dtStartDate=@dtmStartDate
	WHERE
		numOppId=@numOppId AND numStageDetailsId=@numStageDetailsId
	
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateTaskOrder')
DROP PROCEDURE USP_UpdateTaskOrder
GO
CREATE PROCEDURE [dbo].[USP_UpdateTaskOrder]
@numDomainID as numeric(9)=0,    
@vcSortedTask as VARCHAR(500)=''
as    
BEGIN
UPDATE ST SET ST.numOrder=Row# FROM StagePercentageDetailsTask AS ST
LEFT JOIN
(SELECT Items,ROW_NUMBER()  OVER(ORDER BY (SELECT 1)) AS Row# FROM Split(@vcSortedTask,',')) AS T
ON ST.numTaskId=T.Items
WHERE ST.numDomainID=@numDomainID AND ST.numTaskId=T.Items

RETURN 1
END 
DROP INDEX VIEW_Journal_Master_Index ON VIEW_Journal_Master

GO 
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_Journal_Master')
DROP VIEW VIEW_Journal_Master
GO
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO 
CREATE VIEW VIEW_Journal_Master WITH SCHEMABINDING
AS 

/*indexed with with inner joins http://www.sqlservercentral.com/articles/Indexing/indexedviewswithouterjoins/1884/ */ 
    SELECT  GJD.numTransactionId,
			COA.numAccountId,
            COA.vcAccountName AS AccountName,
            COA.vcAccountCode AS AccountCode,
            ATD.numAccountTypeID,
            ATD.vcAccountType AS AccountTypeName,
            ATD.vcAccountCode AS AccountTypeCode,
            ISNULL(GJD.numDebitAmt, 0) AS Debit,
            ISNULL(GJD.numCreditAmt, 0) AS Credit,
            GJH.datEntry_Date,
            GJH.numDomainId,
            GJD.numItemID,
            NULLIF(I.numItemClassification,0) numItemClassification,
            GJD.numcontactid,
			CASE WHEN GJD.numProjectID >0 THEN GJD.numProjectID 
			WHEN GJH.numProjectID>0 THEN GJH.numProjectID END AS numProjectID,
            GJH.numOppId,
            GJH.numOppBizDocsId,
            (CASE WHEN ISNULL(RH.numReturnHeaderID,0) > 0 THEN  RH.numDivisionId ELSE OM.numDivisionId END) numDivisionId,
            OM.numContactId numOrderContactID,
            OM.numAssignedTo,
            GJH.numCategoryHDRID,
            GJH.numClassID,
            GJD.numClassID AS numClassIDDetail,
            CI.numCompanyType RelationshipID,
            CI.vcProfile ProfileID,
			DATEPART(year,GJH.datEntry_Date) YEAR,
			DATEPART(DayOfYear,GJH.datEntry_Date) DayOfYear
    FROM
            dbo.General_Journal_Details AS GJD 
            INNER JOIN dbo.General_Journal_Header AS GJH ON GJH.numJournal_Id = GJD.numJournalId
                                                         AND GJH.numDomainId = GJD.numDomainId 
			INNER JOIN dbo.Chart_Of_Accounts AS COA ON COA.numAccountId = GJD.numChartAcntId
            INNER JOIN dbo.AccountTypeDetail AS ATD ON COA.numParntAcntTypeId = ATD.numAccountTypeID
			INNER JOIN dbo.Item I ON I.numItemCode = ISNULL(GJD.numItemID,-255)
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = ISNULL(GJH.numOppId,-255)
			INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
			INNER JOIN dbo.CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
			LEFT JOIN dbo.ReturnHeader RH ON GJH.numReturnID=RH.numReturnHeaderID
GO
