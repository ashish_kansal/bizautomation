/******************************************************************
Project: Release 12.5 Date: 03.SEP.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,476,135,0,0,'Billing Date','DateField',37,1,37,1,0,0,1,0,0
)


------ Following update on production


DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Profit','monProfit','monProfit','Profit','OpportunityBizDocItems','M','R','Label','',0,0,0,0,0,0,0,0,1,1
)


SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
)
VALUES
(
	6,@numFieldID,'Profit','TextBox','M',1,1,0,0
)

---------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Profit Percent','numProfitPercent','numProfitPercent','ProfitPercent','OpportunityBizDocItems','N','R','Label','',0,0,0,0,0,0,0,0,1,1
)


SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
)
VALUES
(
	6,@numFieldID,'Profit Percent','TextBox','N',1,1,0,0
)

--------------------------
DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Average Cost','monAverageCost','monAverageCost','AverageCost','OpportunityBizDocItems','M','R','TextBox','',0,1,0,0,0,1,0,0,1,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
)
VALUES
(
	6,@numFieldID,'Average Cost','TextBox','M',1,1,0,0
)