     
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpeningBalance]    Script Date: 10/05/2009 17:46:28 ******/

GO

GO
--DROP FUNCTION [dbo].[fn_GetOpeningBalance]
-- SELECT * FROM dbo.fn_GetOpeningBalance(',74,666,658,77,263,668,1156,1162,1166,1167,1170,1172,1174',72,'2009-01-01 00:00:00:000')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getopeningbalance')
DROP FUNCTION fn_getopeningbalance
GO
CREATE FUNCTION [dbo].[fn_GetOpeningBalance]
(@vcChartAcntId as varchar(500),
@numDomainId as numeric(9),
@dtFromDate as DATETIME,
@ClientTimeZoneOffset Int  --Added by Chintan to enable calculation of date according to client machine
)                                 
RETURNS 

@COAOpeningBalance table
(
	numAccountId int,
	vcAccountName varchar(100),
	numParntAcntTypeID int,
	vcAccountDescription varchar(100),
	vcAccountCode varchar(50),
	dtAsOnDate datetime,
	mnOpeningBalance money
)                               
As                                  
Begin          

DECLARE @numFinYear INT;
DECLARE @dtFinFromDate datetime;

set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

set @dtFinFromDate= (SELECT dtPeriodFrom  FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);


SET @dtFromDate = DateAdd(minute, -@ClientTimeZoneOffset, @dtFromDate);

INSERT INTO @COAOpeningBalance

SELECT  COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
 @dtFromDate,

--isnull((SELECT TOP 1 isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE
--numFinYearId=@numFinYear and numDomainID=@numDomainId and
--CAO.numAccountId=COA.numAccountId),0) 
--+
--CASE WHEN  COA.dtOpeningDate<= @dtFromDate THEN COA.numOriginalOpeningBal 
--ELSE 0 END
--+ 
(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD INNER JOIN 
General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id AND 
GJD.numDomainId=COA.numDomainId AND 
GJD.numChartAcntId=COA.numAccountId AND 
GJH.datEntry_Date <=@dtFromDate /*BETWEEN @dtFinFromDate AND @dtFromDate*/ ) /*DATEADD(DAY,-1,@dtFromDate) removed as Date we are passing is 12AM which is 1 second more, compare to opening balance of pervious date */

 FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND
	COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcChartAcntId,','));

RETURN 

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='SplitWordWithPosition')
DROP FUNCTION SplitWordWithPosition
GO
CREATE FUNCTION [dbo].[SplitWordWithPosition]
(
	@IdList varchar(8000),
	@Delimiter char(1) = '/'
)
RETURNS 
@ParsedList table
(
	RowNumber int,
	strItem VARCHAR(1000)
)
AS
BEGIN
	DECLARE @Id varchar(1000), @Pos INT, @rownumber INT 

	SET @IdList = LTRIM(RTRIM(@IdList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)
	SET @rownumber = 0;
	IF REPLACE(@IdList, @Delimiter, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @Id = LTRIM(RTRIM(LEFT(@IdList, @Pos - 1)))
			IF @Id <> ''
			BEGIN
				SET @rownumber = @rownumber +1;
				INSERT INTO @ParsedList (RowNumber,strItem) 
				VALUES (@rownumber ,@Id) --Use Appropriate conversion
			END
			SET @IdList = RIGHT(@IdList, LEN(@IdList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)

		END
	END	
	RETURN
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AccountsReports')
DROP PROCEDURE USP_AccountsReports
GO
CREATE PROCEDURE [dbo].[USP_AccountsReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintReportType TINYINT
as                 

IF @tintReportType=1 --Report Name: Check Register
BEGIN
	SELECT CH.numCheckNo,CH.dtCheckDate,CH.monAmount,CH.numDivisionID,C.vcCompanyName,COA.vcAccountName
	FROM dbo.CheckHeader CH 
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	JOIN dbo.Chart_Of_Accounts COA ON CH.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE CH.numDomainID=@numDomainID --AND ISNULL(bitIsPrint,0)=0 
	AND CH.dtCheckDate BETWEEN  @dtFromDate AND @dtToDate
	order by dtCheckDate,numCheckNo
END

ELSE IF @tintReportType=2 --Report Name: Sales Journal Detail (By GL Account)
BEGIN
	SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt 
	FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId 
	AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL 
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.ReturnHeader RH ON RH.numDomainID=@numDomainID AND RH.numReturnHeaderID=GJH.numReturnID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID AND RH.tintReceiveType=2 AND RH.tintReturnType IN(1,3) AND  RH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=3 --Report Name: Purchase Journal
BEGIN
SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt
FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=2 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	
	UNION ALL
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.BillHeader BH ON BH.numBillID=GJH.numBillID AND BH.numDomainId=@numDomainID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=4 --Report Name: Invoice Register
BEGIN

	SELECT OM.vcPOppName,OBD.vcBizDocID,OBD.dtFromDate AS FromDate,C.vcCompanyName,OM.monDealAMount,OBD.monDealAMount AS monBizDocAmount,OBD.monAmountPaid
	FROM OpportunityMaster OM
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.bitAuthoritativeBizDocs=1 
	JOIN dbo.DivisionMaster D ON OM.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate

	UNION ALL

	SELECT RH.vcRMA,RH.vcBizDocName,RH.dtCreatedDate AS FromDate,C.vcCompanyName,DM.monDepositAmount * -1 AS monDealAMount,0 AS monBizDocAmount,0 AS monAmountPaid 
	FROM dbo.ReturnHeader RH
	JOIN dbo.DepositMaster DM ON RH.numReturnHeaderID=DM.numReturnHeaderID AND DM.numDomainID=@numDomainID
	JOIN dbo.DivisionMaster D ON RH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE RH.tintReceiveType=2 AND  RH.numDomainId=@numDomainID
	AND RH.dtCreatedDate BETWEEN  @dtFromDate AND @dtToDate
	ORDER BY FromDate

END

ELSE IF @tintReportType=5 --Report Name: Receipt Journal (Summary format)
BEGIN
	
	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE ISNULL(GJH.numDepositId,0)>0 AND GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName

END

ELSE IF @tintReportType=6 --Report Name: Total Receipts (Detailed)
BEGIN

	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,GJD.varDescription AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=6
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=7
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	LEFT JOIN dbo.DivisionMaster D ON GJD.numCustomerId=D.numDivisionID
	LEFT JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(DD.monAmountPaid AS VARCHAR(20)) +')',NULL,NULL,
	DM.numDepositId AS numReference,8 AS tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId = DD.numDepositID 
	JOIN dbo.OpportunityBizDocs OBD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId AND DD.numOppID=OM.numOppId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	ORDER BY datEntry_Date,numReference,tintReferenceType	
	
END

ELSE IF @tintReportType=7 --Report Name: Total Disbursement Journal (Summary)
BEGIN

	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE (ISNULL(GJH.numBillPaymentID,0)>0 OR ISNULL(GJH.numCheckHeaderID,0)>0) AND GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName
	
END

ELSE IF @tintReportType=8 --Report Name: Cash disbursement Journal (Detailed)
BEGIN

	SELECT ISNULL(GJH.numCheckHeaderID,0) AS numCheckHeaderID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
	GJH.datEntry_Date,COA.vcAccountName,numDebitAmt,numCreditAmt,GJD.tintReferenceType INTO #tempGJ 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainID=@numDomainID AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (ISNULL(GJH.numCheckHeaderID,0)>0 OR ISNULL(GJH.numBillPaymentID,0)>0)
	
	SELECT GJH.datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=1
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
		
	UNION ALL
	SELECT GJH.datEntry_Date AS datEntry_Date,NULL,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=2
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=8
	LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.tintReferenceType=8
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL AS numCheckNo,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=9
	JOIN dbo.DivisionMaster D ON BPH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE BPH.numDomainID=@numDomainID
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(BPD.monAmount AS VARCHAR(20)) +')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,COA.vcAccountName + '(' + CAST(GJD.numDebitAmt AS VARCHAR(20)) + ' : Bill-' + CONVERT(VARCHAR(10),BH.numBillID)+')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
	JOIN dbo.General_Journal_Header GJH ON BH.numBillID=GJH.numBillID JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE BPH.numDomainID=@numDomainID AND GJD.numDebitAmt>0
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	
	ORDER BY datEntry_Date,numReference,tintReferenceType
	
	DROP TABLE #tempGJ
END
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0'
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID 
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID 
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID                      
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode,I.vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN

DECLARE @bParentCatrgory BIT;

SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

DECLARE @tintDisplayCategory AS TINYINT
SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
PRINT @tintDisplayCategory 
--IF @tintDisplayCategory > 0
--	BEGIN
--	SET @strSQL = @strSQL + ' AND ISNULL((SELECT SUM(numOnHand) FROM WareHouseItems W WHERE W.numItemID = I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '),0) > 0 '	                   	
--	END
	PRINT @bParentCatrgory
	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND (
					SC.numCategoryID IN (
										SELECT numCategoryID FROM dbo.ItemCategory 
										WHERE numItemID IN (
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numOnHand) > 0
															UNION ALL
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numAllocation) > 0
														   )
									   )
					OR tintLevel = 1
					)				   
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory FROM Category  WHERE numCategoryID = @numCatergoryId
END
ELSE IF @byteMode = 18
BEGIN
	     --used for hierarchical category list
	     SELECT 
	C1.numCategoryID,C1.vcCategoryName,NULLIF(numDepCategory,0) numDepCategory,ISNULL(numDepCategory,0) numDepCategory1,0 AS tintLevel,
	ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
	ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] in (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
	from Category C1 WHERE numDomainID =@numDomainID
--	AND C1.numCategoryID <> ISNULL(@numCatergoryId,0)
--	ORDER BY ISNULL(numDepCategory,0)
   

END

ELSE IF @byteMode=19
BEGIN
	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID 
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID 
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN
    
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate); 
PRINT @dtFromDate
PRINT @dtToDate
	 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC, dbo.General_Journal_Details.numTransactionId asc 
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetItems_API]    Script Date: 05/07/2009 17:44:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by Chintan prajapati
--EXEC USP_GetItems_API 1,1,'',''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItems_API')
DROP PROCEDURE USP_GetItems_API
GO
CREATE PROCEDURE [dbo].[USP_GetItems_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @CreatedDate AS VARCHAR(50) ='',
          @ModifiedDate AS VARCHAR(50)='' 
AS
     
IF(@ModifiedDate='' and @CreatedDate='')
BEGIN
	SELECT   numItemCode,
			   vcItemName,
			    ISNULL((SELECT TOP 1([monWListPrice])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                        AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                        AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU]

	  FROM     Item I 
	  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
	  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
	  WHERE    I.numDomainID = @numDomainID  
		--AND LEN(vcModelID) >0
		AND  LEN(vcItemName) >0
		AND I.charItemType ='P'
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
END
ELSE
IF(@ModifiedDate ='')
BEGIN
	SELECT   numItemCode,
			   vcItemName,
			    ISNULL((SELECT TOP 1([monWListPrice])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                        AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                        AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU]

	  FROM     Item I 
	  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
	  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
	  WHERE    I.numDomainID = @numDomainID AND 
		bintCreatedDate > @CreatedDate
			--AND LEN(vcModelID) >0
		AND  LEN(vcItemName) >0
		AND I.charItemType ='P'
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
END
ELSE IF(@CreatedDate ='')
BEGIN
		SELECT  numItemCode,
			    vcItemName,
			    ISNULL((SELECT TOP 1([monWListPrice])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU]
	  FROM     Item I 
	  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
	  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
	  WHERE    I.numDomainID = @numDomainID AND 
		bintModifiedDate > @ModifiedDate
			--AND LEN(vcModelID) >0
		AND  LEN(vcItemName) >0
		AND I.charItemType ='P'
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
END	
  
  

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--SELECT * FROM [Chart_Of_Accounts] WHERE [numDomainId]=72

--SELECT SUBSTRING(CONVERT(VARCHAR(20),GETUTCDATE()),0,4)
-- EXEC USP_GetMonthlySummary 72, '2008-09-26 00:00:00.000', '2009-09-26 00:00:00.000','71'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMonthlySummary')
DROP PROCEDURE USP_GetMonthlySummary
GO
CREATE PROCEDURE [dbo].[USP_GetMonthlySummary](
               @numDomainId   AS INT,
               @dtFromDate    AS DATETIME,
               @dtToDate      AS DATETIME,
               @vcChartAcntId AS VARCHAR(500),
               @ClientTimeZoneOffset INT --Added by Chintan to enable calculation of date according to client machine
               )
AS
  BEGIN
  
	SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
	SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

	PRINT @dtFromDate 
	PRINT @dtToDate
	 
	DECLARE @mnOpeningBalance AS MONEY
	SELECT @mnOpeningBalance = mnOpeningBalance FROM dbo.fn_GetOpeningBalance(@vcChartAcntId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)




		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcChartAcntId, ','))
				AND COA1.numDomainId = @numDomainID
				AND (COA2.vcAccountCode LIKE COA1.vcAccountCode OR (COA2.vcAccountCode LIKE COA1.vcAccountCode + '%' AND COA2.numParentAccID>0))
				
		SELECT @vcChartAcntId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
  
  
    SELECT   numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date)) AS MONTH,
             MONTH(datEntry_Date) AS MonthNumber,
             YEAR(datEntry_Date) AS YEAR,
             SUM(Debit) AS Debit,
             SUM(Credit) AS Credit,
             @mnOpeningBalance AS Opening, 
             0.00 AS Closing
             
             INTO #Temp1
    FROM     VIEW_JOURNAL
    WHERE    datEntry_Date BETWEEN @dtFromDate AND @dtToDate
             AND numDomainId = @numDomainID
             AND numAccountId IN (SELECT *
                                  FROM   dbo.SplitIds(@vcChartAcntId,','))
    GROUP BY numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             MONTH(datEntry_Date),
             YEAR(datEntry_Date),
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date))
    ORDER BY YEAR(datEntry_Date),MONTH(datEntry_Date)
    
    
--    SELECT DATEPART(MONTH,dtPeriodFrom) FY_Opening_Month,DATEPART(year,dtPeriodFrom) FY_Opening_year, * FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
--Following logic applieds to only income, expense and cogs account
IF EXISTS( SELECT * FROM #Temp1 where SUBSTRING(COAvcAccountCode,0,5) IN ('0103','0104','0106') )
BEGIN
	 alter table #Temp1 alter column MONTH VARCHAR(50);
    
    DECLARE @numMaxFinYearID NUMERIC
    DECLARE @Month INT
    DECLARE @year  INT
    
    SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId 
--    PRINT @numMaxFinYearID
    WHILE @numMaxFinYearID > 0
    BEGIN
			SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
    
--    PRINT @Month
--    PRINT @year
	/*Rule:make first month of each FY zero as opening balance and show report respectively */
			UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start'
			WHERE MonthNumber = @Month AND YEAR= @year
			
			IF @@ROWCOUNT=0 -- when No data avail for First month of FY
			BEGIN
				SELECT TOP 1  @Month = MonthNumber FROM #Temp1 WHERE MonthNumber>@Month AND YEAR=@year
				UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start'
				WHERE MonthNumber = @Month AND YEAR= @year
			END
			
    	
    	
    	 SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId < @numMaxFinYearID
    	IF @@ROWCOUNT = 0 
    	SET @numMaxFinYearID = 0
    END
END
   

    
    SELECT * FROM #Temp1
    
     DROP TABLE #Temp
     DROP TABLE #Temp1
  END
  
--  exec USP_GetMonthlySummary @numDomainId=89,@dtFromDate='2011-12-01 00:00:00:000',@dtToDate='2013-03-01 23:59:59:000',@vcChartAcntId='1444',@ClientTimeZoneOffset=-330

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetScalerValue')
DROP PROCEDURE USP_GetScalerValue
GO
CREATE PROCEDURE USP_GetScalerValue 
@numDomainID AS NUMERIC,--Will receive SiteID when mode =2,12,9
@tintMode AS TINYINT,
@vcInput AS VARCHAR(1000),
@numUserCntId AS NUMERIC(9)=0
AS
BEGIN
	IF @tintMode = 1
	BEGIN
		SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId=@numDomainID and numBizDocsPaymentDetId= CONVERT(NUMERIC, @vcInput)
	END
	IF @tintMode = 2 
	BEGIN
		SELECT COUNT(*) FROM dbo.Sites WHERE vcHostName=@vcInput AND numSiteID <> @numDomainID 
	END
	IF @tintMode = 3 
	BEGIN
		SELECT COUNT(*) FROM dbo.Domain WHERE vcPortalName=@vcInput AND numDomainId <> @numDomainID
	END
	IF @tintMode = 4 --Item Name
	BEGIN
		SELECT dbo.GetItemName(@vcInput)
	END
	IF @tintMode = 5 --Company Name
	BEGIN
		SELECT dbo.fn_GetComapnyName(@vcInput)
	END
	IF @tintMode = 6 --Vendor Cost
	BEGIN
		SELECT dbo.fn_GetVendorCost(@vcInput)
	END
	IF @tintMode = 7 -- tintRuleFor
	BEGIN
		SELECT tintRuleFor FROM PriceBookRules WHERE numPricRuleID=@vcInput AND numDomainId =@numDomainId
	End
	IF @tintMode = 8 -- monSCreditBalance
	BEGIN
		SELECT isnull(monSCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	End
    IF @tintMode = 9 -- Check Site Page Exists
	BEGIN
		if exists(SELECT [vcPageName] FROM   SitePages WHERE  [numSiteID] = @numDomainId AND LOWER([vcPageURL]) = LOWER(@vcInput))
			SELECT 1 as SitePageExists
		else
			SELECT 0 as SitePageExists
	END
	IF @tintMode = 10 -- Category Name
	BEGIN
		SELECT ISNULL(vcCategoryName,'NA') FROM dbo.Category WHERE numCategoryID=CAST(@vcInput AS NUMERIC(9)) AND numDomainId =@numDomainId
	End
	IF @tintMode = 11 -- monPCreditBalance
	BEGIN
		SELECT isnull(monPCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	END
	 IF @tintMode = 12 -- Get Style ID from filename
	BEGIN
		SELECT TOP 1 ISNULL([numCssID],0) FROM  dbo.StyleSheets WHERE  [numSiteID] = @numDomainId AND LOWER([styleFileName]) = LOWER(@vcInput)
	END
	IF @tintMode = 13 -- numItemClassification
	BEGIN
		SELECT ISNULL(numItemClassification,0) as numItemClassification FROM  Item WHERE  numDomainID = @numDomainId AND numItemCode = @vcInput
	END
	IF @tintMode = 14 -- numAssignedTo
	BEGIN
		SELECT ISNULL(numAssignedTo,0) as numAssignedTo FROM  DivisionMaster WHERE  numDomainID = @numDomainId AND numDivisionID = @vcInput
	END
	IF @tintMode = 15 -- Get Currency ID 
	BEGIN
		SELECT numCurrencyID FROM Currency WHERE numDomainID = @numDomainID AND chrCurrency = @vcInput
	END
	IF @tintMode = 16 -- Total Order of Items where Warehouse exists
	BEGIN
		SELECT count(*) as TotalRecords FROM OpportunityMaster OM join OpportunityItems OI on OM.numOppId=OI.numOppId
			 WHERE OI.numItemCode = @vcInput AND OM.numDomainID = @numDomainID and OI.numWarehouseItmsID>0
	END
	IF @tintMode = 17 -- Get numBizDocID
	BEGIN
		SELECT numBizDocTempID FROM dbo.BizDocTemplate 
								WHERE numDomainID = @numDomainID
								  AND tintTemplateType = 0
								  AND bitDefault = 1
								  AND bitEnabled = 1
								  AND numBizDocID = @vcInput
	END
	IF @tintMode = 18 -- Get Domain DivisionID
	BEGIN
		SELECT numDivisionID FROM Domain WHERE numDomainID = @numDomainID
	END
	IF @tintMode = 19
	BEGIN
		SELECT numItemID FROM ItemAPI WHERE numDomainID = @numDomainID AND  vcAPIItemID = @vcInput
	END
	IF @tintMode = 20
	BEGIN
		SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput
	END
	IF @tintMode = 21
	BEGIN
		SELECT numStateId FROM dbo.State S inner JOIN webApiStateMapping SM
		ON S.vcState =  SM.vcStateName 
		AND S.numDomainId = @numDomainId 
		AND ( LOWER(SM.vcAmazonStateCode) = LOWER(@vcInput) OR LOWER(SM.vcStateName) = LOWER(@vcInput))
	END
	IF @tintMode = 22
	BEGIN
		SELECT LD.numListItemId FROM ListDetails LD INNER Join WebApiCountryMapping CM ON 
		LD.vcData = CM.vcCountryName WHERE LD.numListId = 40 AND LD.numDomainId = @numDomainId AND
		CM.vcAmazonCountryCode = @vcInput
	END
   IF @tintMode = 23
   BEGIN
       select isnull(txtSignature,'') from userMaster where numUserDetailId = convert(numeric , @vcInput) and numDomainId = @numDomainId 
   END
   
   IF @tintMode = 24
   BEGIN
         DECLARE @dtEmailStartDate AS varchar(20),
                 @dtEmailEndDate AS varchar(20) ,
                 @intNoOfEmail AS numeric
            
        select @dtEmailStartDate =convert(varchar(20) , dtEmailStartDate) ,@dtEmailEndDate =convert(varchar(20), dtEmailEndDate), @intNoOfEmail = intNoOfEmail from Subscribers where numTargetDomainID=@numDomainID
       
        IF  Convert(datetime, Convert(int, GetDate())) >=  @dtEmailStartDate AND Convert(datetime, Convert(int, GetDate())) <=  @dtEmailEndDate  
            BEGIN
		          select  (convert(varchar(10) , count(*)) + '-' +(select top 1 convert(varchar(10), intNoOfEmail) from Subscribers where numTargetDomainID = @numDomainID)) as EmailData
                  from broadcastDTLs BD inner join BroadCast B on B.numBroadCastID = BD.numBroadCastID 
                  where B.numDomainId = @numDomainId AND bintBroadCastDate between @dtEmailStartDate and @dtEmailEndDate and bitBroadcasted = 1 and tintSucessfull = 1     		
			END       
   END  
   IF @tintMode = 25
	BEGIN
		SELECT IsNull(vcMsg,'') AS vcMsg FROM dbo.fn_GetEmailAlert(@numDomainId ,@vcInput,@numUserCntId)
	END
	IF @tintMode = 26
	BEGIN
		SELECT COUNT(*) AS Total FROM dbo.Communication WHERE numDomainID = @numDomainID and numContactId = @vcInput
	END 
	IF @tintMode = 27 -- Check Order Closed
	BEGIN
		SELECT ISNULL(tintshipped,0) AS tintshipped FROM OpportunityMaster  
			 WHERE numOppId = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 28 -- General Journal Header for Bank Recon.
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 29 -- General Journal Header for Write Check
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numCheckHeaderID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	
	IF @tintMode = 30 -- Get Units for sample item to be created
	BEGIN
	 SELECT ISNULL(u.numUOMId,0) FROM 
        UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID 
        AND d.numDomainID=@numDomainID 
        AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
        AND u.vcUnitName ='Units'
	END
	
	IF @tintMode = 31 -- Get Magento Attribute Set for Current Domain
	BEGIN
		SELECT vcEighthFldValue FROM dbo.WebAPIDetail WHERE numDomainId = @numDomainID  AND webApiId = @vcInput
	END
	
	IF @tintMode = 32 -- Get WareHouseItemID for ItemID is existis for Current Domain
	BEGIN
		 SELECT ISNULL((SELECT TOP 1 numWareHouseItemID FROM  dbo.WareHouseItems WHERE numDomainId = @numDomainID  AND numItemID = @vcInput),0)AS  numWareHouseItemID
	END
	
	IF @tintMode = 33
	BEGIN
	SELECT ISNULL(MAX(CAST(ISNULL(numSequenceId,0) AS BigInt)),0) + 1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@vcInput
	END	
	
	IF @tintMode = 34
	BEGIN
	SELECT ISNULL(vcOppRefOrderNo,'') FROM dbo.OpportunityMaster OM 
			 WHERE OM.numDomainId=@numDomainID AND OM.numOppID=@vcInput
	END
	
	IF @tintMode = 35
	BEGIN 
	SELECT ISNULL(bitAutolinkUnappliedPayment,0) FROM dbo.Domain  WHERE numDomainId=@numDomainID 
	END		 
	
	IF @tintMode = 36
	BEGIN 
		SELECT ISNULL(numJournalId,0) FROM dbo.General_Journal_Details WHERE numDomainId=@numDomainID AND numChartAcntId=@vcInput AND chBizDocItems='OE'
	END	
	
	IF @tintMode = 37
	BEGIN 
		SELECT ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID
	END	
	IF @tintMode = 38
	BEGIN
		SELECT CONVERT(VARCHAR(50),numItemCode)+'~'+CharItemType as numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput
	END	
	IF @tintMode = 39 -- Get WareHouseItemID for SKU is exists for Current Domain
	BEGIN
	IF CHARINDEX('~',@vcInput) > 0
		BEGIN
			DECLARE @numItemID NUMERIC(18,0)
			DECLARE @vcSKU VARCHAR(50)
			SELECT @numItemID = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 1
			SELECT @vcSKU = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 2

			
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND vcWHSKU = @vcSKU
                            AND numItemID = @numItemID
                   ), 0) AS numWareHouseItemID
		END
	ELSE
		BEGIN
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND numItemID = @vcInput
                   ), 0) AS numWareHouseItemID
		END
	END 
END
--exec USP_GetScalerValue @numDomainID=1,@tintMode=24,@vcInput='0'
 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetTrackingDetails' ) 
    DROP PROCEDURE usp_GetTrackingDetails
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTrackingDetails]
 @numDomainId AS NUMERIC(9), 
 @numWebApiId AS NUMERIC(9)
AS 
    BEGIN
	
		SELECT AOID.WebApiOrderItemId ,AOID.numDomainId,AOID.numWebApiId,AOID.numOppId,OBI.numItemCode,OBI.numOppBizDocItemID,
		AOID.numOppItemId,vcapiOppId,vcApiOppItemId,OB.numOppBizDocsId,OB.vcTrackingNo ,OB.monShipCost,OB.vcShippingMethod,LD.vcData AS ShippingCarrier,
		(SELECT vcAPIItemID FROM dbo.ItemAPI 
		WHERE numItemID = OBI.numItemCode 
		AND WebApiId = AOID.numWebApiId
		AND 1 = (CASE ISNULL(vcSKU,'') WHEN (SELECT vcSKU FROM Item WHERE numItemCode = numItemID AND numDomainID = @numDomainId ) THEN 1  
									   WHEN (SELECT vcWHSKU FROM dbo.WareHouseItems WI WHERE OBI.numWarehouseItmsID  = WI.numWareHouseItemID ) THEN 1
									   ELSE 0
				 END)  
		) AS vcAPIItemId,
		(SELECT COUNT(*) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID = OB.numOppBizDocsId AND tintTrackingStatus = 1) AS numTrackingNoUpdated,OBI.numOppBizDocID
		FROM WebApiOppItemDetails AOID 
		INNER JOIN dbo.OpportunityBizDocs OB ON AOID.numOppId  = OB.numOppId AND OB.bitAuthoritativeBizDocs = 1
		INNER JOIN dbo.OpportunityBizDocItems OBI  ON OBI.numOppBizDocID = OB.numOppBizDocsId
		INNER JOIN dbo.ListDetails LD ON LD.numlistItemID = OB.numShipVia

		WHERE  AOID.numWebApiId = @numWebApiId AND AOID.numDomainID = @numDomainId 
		AND len(AOID.vcAPIOppId)>2 AND len(OB.vcTrackingNo)>1
		AND (OBI.tintTrackingStatus = 0 or OBI.tintTrackingStatus IS NULL) 
		
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(W.vcWHSKU,ISNULL(vcSKU,'')) AS vcSKU, isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem]
from Item I                    
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode   
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
where I.numItemCode=@numItemCode  group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable, W.vcWHSKU,vcSKU, bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
	,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem 


--exec USP_ItemDetails @numItemCode = 197605 ,@ClientTimeZoneOffset =330

--Created By Anoop Jayaraj          
        
--exec USP_ItemDetailsForEcomm 173028,63,72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE usp_itemdetailsforecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9)                                              
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)

set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0)--,@tintColumns=isnull(tintItemColumns,1)        
from eCommerceDTL where numDomainID=@numDomainID
IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode                                
 
 
declare @strSql as varchar(5000)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType,
ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN CASE WHEN I.bitSerialized = 1 
					  THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
					  ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice] 
				 END 
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName

from Item I                  
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) + '             
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit 

)'

--,
--


set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        
        IF @numWareHouseID = 0 
            BEGIN
                SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,
                                                        0)
                FROM    [eCommerceDTL]
                WHERE   [numDomainID] = @numDomainID	
            END
        
		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(10)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(ISNULL(@SearchText,'')) > 0
			BEGIN
				--PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END
		ELSE
			BEGIN
				--PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'
							
				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END
			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN CASE WHEN I.bitSerialized = 1
																	  THEN ( UOM * monListPrice )
																	  ELSE ( UOM * ISNULL(W1.[monWListPrice], 0) )
																 END
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN ''<font color=red>Out Of Stock</font>''
																			   WHEN bitAllowBackOrder = 1 THEN ''In Stock''
																			   ELSE ''In Stock'' 
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' 
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' ) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' 
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
			 PRINT  @strSQL     
             EXECUTE sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	
		
		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageAPIOppItem' ) 
    DROP PROCEDURE USP_ManageAPIOppItem
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE PROCEDURE [dbo].[USP_ManageAPIOppItem]
@WebApiOrderItemId numeric(18, 0),
@numDomainId	numeric(18, 0),
@numWebApiId	int,
@numOppId	numeric(18, 0),
@numOppItemId numeric(18, 0),
@vcAPIOppId	varchar(50),
@vcApiOppItemId varchar(50),
@tintStatus tinyint = 0,
@numCreatedby	numeric(18, 0)
AS 
BEGIN
  DECLARE @Check as integer
  IF @WebApiOrderItemId =0
	  BEGIN
		SELECT @Check = COUNT(*) FROM   [WebApiOppItemDetails] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId 
		AND vcAPIOppId = @vcAPIOppId AND vcApiOppItemId = @vcApiOppItemId
  		IF @Check =0    
		 BEGIN
			INSERT INTO [WebApiOppItemDetails]
				   (
				   [numDomainId]
				   ,[numWebApiId]
				   ,[numOppId]
				   ,[numOppItemId]
				   ,[vcAPIOppId]
				   ,[vcApiOppItemId]
				   ,[tintStatus]
				   ,[numCreatedby]
				   ,[dtCreated])
			 VALUES
				   (
					@numDomainId,
				   @numWebApiId,
				   @numOppId,
				   @numOppItemId,
				   @vcAPIOppId,
				   @vcApiOppItemId,
				   @tintStatus,
				   @numCreatedby,
				   GETUTCDATE())
		END
END
	ELSE 
		IF @WebApiOrderItemId <> 0
			BEGIN
				SELECT @Check = COUNT(*) FROM   [WebApiOppItemDetails] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId 
				AND vcAPIOppId = @vcAPIOppId AND vcApiOppItemId = @vcApiOppItemId
				IF @Check =0    
					BEGIN
						INSERT INTO [WebApiOppItemDetails]
							   (
							   [numDomainId]
							   ,[numWebApiId]
							   ,[numOppId]
							   ,[numOppItemId]
							   ,[vcAPIOppId]
							   ,[vcApiOppItemId]
							   ,[tintStatus]
							   ,[numCreatedby]
							   ,[dtCreated],
							   [dtModified])
						 VALUES
							   (
								@numDomainId,
							   @numWebApiId,
							   @numOppId,
							   @numOppItemId,
							   @vcAPIOppId,
							   @vcApiOppItemId,
							   @tintStatus,
							   @numCreatedby,
							   GETUTCDATE(),
							   GETUTCDATE())
					END
				ELSE
					BEGIN
						UPDATE [WebApiOppItemDetails]
						SET    [tintStatus] = @tintStatus,[dtModified] = GETUTCDATE()
						WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId
						AND [vcAPIOppId] = @vcAPIOppId AND [vcApiOppItemId] = @vcApiOppItemId
					END		
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0)	                    
AS   

                                                     
 -- Added By Manish Anjara As On : 21st Jam,2013
 -- Check whether GroupID Or tintCRMType is not passed then we need to add record as prospect (means numGroupID = 0, tintCRMType = 1)
 IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
 BEGIN
 	SET @numGrpId = 0
 	SET @tintCRMType = 1
 END
                                                                                                               
 IF @numDivisionId is null OR @numDivisionId=0                                                 
 BEGIN    
 if @UpdateDefaultTax=1 set @bitNoTax=0
                                                                                  
    INSERT INTO DivisionMaster                                    
     (                      
      numCompanyID,                      
      vcDivisionName,                      
      numGrpId,                      
      numFollowUpStatus,               
      bitPublicFlag,                      
      numCreatedBy,                      
      bintCreatedDate,                                    
      numModifiedBy,                      
      bintModifiedDate,                      
      tintCRMType,                      
      numDomainID,                                    
      bitLeadBoxFlg,                      
      numTerID,                     
      numStatusID,                                    
      numRecOwner,                              
      tintBillingTerms,                                    
      numBillingDays,                      
      tintInterestType,                      
      fltInterest,                      
      numCampaignID,            
      vcComPhone,            
      vcComFax,    
   bitNoTax,numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID                
 )                                    
     VALUES                      
    (                      
    @numCompanyID,                       
    @vcDivisionName,                       
                      
    @numGrpId,                       
    @numFollowUpStatus,                               
    @bitPublicFlag,                       
    @numUserCntID,                       
    getutcdate(),                       
    @numUserCntID,                       
    getutcdate(),                       
    @tintCRMType,                       
    @numDomainID,                       
    @bitLeadBoxFlg,                       
    @numTerID,                       
    @numStatusID,                                                        
    @numUserCntID,                                
    0,                      
    @numBillingDays,                      
    0,                      
    0,                      
    @numCampaignID,            
    @vcComPhone,            
    @vcComFax,    
    @bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID                           
  )                                  
	SELECT @numDivisionID = SCOPE_IDENTITY()
	  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
	  IF	EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
         BEGIN
				INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
         END
         
     ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END

       --End of Sachin Script                   
     



  if @UpdateDefaultTax=1 
  begin
   insert into DivisionTaxTypes
   select @numDivisionID,numTaxItemID,1 from TaxItems
   where numDomainID=@numDomainID
   union
   select @numDivisionID,0,1 
  end                                                


	                             
	DECLARE @numGroupID NUMERIC
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0 
	insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
      values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                      
                                                                          
     SELECT @numDivisionID                                                  
                                                   
 END                                                        
 ELSE if @numDivisionId>0                                                      
 BEGIN                                                                    
   --Inserting into Follow up History                                       
    declare @numFollow as varchar(10)                                        
    declare @binAdded as varchar(20)                                        
    declare @PreFollow as varchar(10)                                        
    declare @RowCount as varchar(2)                                        
    set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID)                                        
    set @RowCount=@@rowcount                                        
    select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID                                         
    if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                                        
     begin                                        
   select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc                                        
                                         
  if @PreFollow<>0                                        
  begin                                        
                                         
   if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
      begin                                        
                                          
            insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                        
          end                                        
  end                              
  else                                        
  begin                                        
                                         
  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                   
  end                                        
                                     
  end                                        
    -----                                                      
                                                        
   UPDATE DivisionMaster                       
  SET                        
   numCompanyID = @numCompanyID ,                     
   vcDivisionName = @vcDivisionName,                                                        
     numGrpId = @numGrpId,                                                                       
     numFollowUpStatus =@numFollowUpStatus,                                                        
     numTerID = @numTerID,                                                   
     bitPublicFlag =@bitPublicFlag,                                                        
     numModifiedBy = @numUserCntID,                                                  
     bintModifiedDate = getutcdate(),                                                     
     tintCRMType = @tintCRMType,                                                      
     numStatusID = @numStatusID,                                                 
     tintBillingTerms = @tintBillingTerms,                                                      
     numBillingDays = @numBillingDays,                                                     
     tintInterestType = @tintInterestType,                                                       
     fltInterest =@fltInterest,                          
     vcComPhone=@vcComPhone,                          
     vcComFax= @vcComFax,                      
     numCampaignID=@numCampaignID,    
     bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,numCurrencyID = @numCurrencyID                                                                
   WHERE numDivisionID = @numDivisionID       

    Update dbo.AddressDetails set
       vcStreet=@vcBillStreet,                                    
       vcCity =@vcBillCity,                                    
       vcPostalCode=@vcBillPostCode,                                     
       numState=@vcBillState,                                    
       numCountry=@vcBillCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=1         
	   Update dbo.AddressDetails set
       vcStreet=@vcShipStreet,                                    
       vcCity =@vcShipCity,                                    
       vcPostalCode=@vcShipPostCode,                                     
       numState=@vcShipState,                                    
       numCountry=@vcShipCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=2         
	  
             
  end 

    ---Updating if organization is assigned to someone                
  declare @tempAssignedTo as numeric(9)              
  set @tempAssignedTo=null               
  select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID                
print @tempAssignedTo              
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
  begin                
    update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID                
  end               
  else if  (@numAssignedTo =0)              
  begin              
   update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID              
                  
                
                                                                      
   SELECT @numDivisionID                                                   
 END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemAPILinking]    Script Date: 05/07/2009 17:52:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemAPILinking')
DROP PROCEDURE USP_ManageItemAPILinking
GO
CREATE PROCEDURE [dbo].[USP_ManageItemAPILinking]
                @numDomainID  AS NUMERIC(9),
                @intWebApiId  INT  = NULL,
                @numItemCode  AS NUMERIC(9)  = 0,
                @vcApiItemId  AS VARCHAR(50),
                @numUserCntID AS VARCHAR(50),
                @vcSKU  AS VARCHAR(100)
AS
  BEGIN
    IF NOT EXISTS (SELECT *
                   FROM   [ItemAPI]
                   WHERE  
--                   [vcAPIItemID] = @vcApiItemId
                          [numItemID] = @numItemCode
                          AND [WebApiId] = @intWebApiId
                          AND [numDomainId] = @numDomainID)
      BEGIN
        INSERT INTO [ItemAPI]
                   ([WebApiId],
                    [numDomainId],
                    [numItemID],
                    [vcAPIItemID],
                    [numCreatedby],
                    [dtCreated],
                    [numModifiedby],
                    [dtModified],[vcSKU])
        VALUES     (@intWebApiId,
                    @numDomainID,
                    @numItemCode,
                    @vcApiItemId,
                    @numUserCntID,
                    GETUTCDATE(),
                    @numUserCntID,
                    GETUTCDATE(),@vcSKU)
      END
      ELSE
      BEGIN
	  	 UPDATE [ItemAPI]
         SET    vcApiItemId = @vcApiItemId,
				[dtModified] = GETUTCDATE()
         WHERE  [numItemID] = @numItemCode
                AND [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
         
	  END
  END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
SET @ParentSKU = @vcSKU
--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId > 1
    AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        PRINT 1
        DECLARE @ItemID AS NUMERIC(9)
        DECLARE @cnt AS INT
    
       -- check wether this id already exist in Domain
       
        SELECT  @cnt = COUNT([numItemCode])
        FROM    [Item]
        WHERE   [numDomainId] = @numDomainID
                AND ( vcSKU = @vcSKU
                      OR @vcSKU IN ( SELECT  vcWHSKU
                                    FROM    dbo.WareHouseItems
                                    WHERE   numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                    )
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemCode], @ParentSKU = vcSKU
                FROM    [Item]
                WHERE   [numDomainId] = @numDomainID
                        AND ( vcSKU = @vcSKU
                              OR @vcSKU IN (
                              SELECT    vcWHSKU
                              FROM      dbo.WareHouseItems
                              WHERE     numItemID = Item.[numItemCode]
                                        AND vcWHSKU = @vcSKU )
                            )
                SET @numItemCode = @ItemID
            END
        ELSE 
            BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                SELECT  @cnt = COUNT([numItemID])
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
                IF @cnt > 0 
                    BEGIN
                        SELECT  @ItemID = [numItemID]
                        FROM    [ItemAPI]
                        WHERE   [WebApiId] = @intWebApiId
                                AND [numDomainId] = @numDomainID
                                AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                        SET @numItemCode = @ItemID
                    END
     
            END
    END

                                                           
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
 
 --insert warehouse for inventory item
-- EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList      
-- insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--    
--update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID 
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode
	
--	--validate average cost, do not update average cost if item has sales/purchase orders
--	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
--	BEGIN
--		SELECT @monAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode=@numItemCode
--	END

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode 
	                                          
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice=@monListPrice,                                             
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU=@ParentSKU,                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID --  numeric(9, 0)
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END

--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--  SELECT * FROM dbo.ItemCategory
--  Insert into ItemCategory(numItemID,numCategoryID)                                            
--  (SELECT numItemID,numCategoryID
--  FROM OPENXML(@hDoc,'/ItemCategories/',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--   ))
   
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END


--  Insert into WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)                                            
--  (SELECT numWareHouseItemID,vcSerialNo,Comments,numQty                                                                        
--  FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))
--                                                                                
--  update WareHouseItmsDTL set                                                                         
--   numWareHouseItemID=X.numWareHouseItemID,                                                                       
--   vcSerialNo=X.vcSerialNo,          
--    vcComments=X.Comments,numQty=X.numQty                                                                                          
--    From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty                              
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                          
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))X                                                                         
--  where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                                            
--                                             
--  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                   
--    with(numWareHouseItmsDTLID numeric(9),                              
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--  Op_Flag tinyint)) 


--  SELECT X.*,ROW_NUMBER() OVER( order by X.numQty) AS ROWNUMBER
--INTO #TempTableWareHouseItmsDTLID
--FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),numQty NUMERIC(9) )
--                ) X


--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT,@Op_Flag AS int
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--DECLARE @numWareHouseItmsDTLID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTableWareHouseItmsDTLID
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = numQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag,@numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM #TempTableWareHouseItmsDTLID WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Op_Flag=2 
--   	    BEGIN
--		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.numQty 
--                FROM #TempTableWareHouseItmsDTLID X INNER JOIN WareHouseItmsDTL W
--                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Op_Flag=1 
--   	    BEGIN
--   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.numQty  FROM #TempTableWareHouseItmsDTLID X 
--		   	   WHERE X.ROWNUMBER=@minROWNUMBER
--		END
--   	    
--   	    ELSE IF @Op_Flag=3 
--   	    BEGIN
--   	       	  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
--		END
--		
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTableWareHouseItmsDTLID WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)
	delete from WareHouseItems where numItemID=@numItemCode
END

--	insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--SELECT @@IDENTITY	

--  update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID                                   
                                   
                                   
--    delete from  WareHouseItmsDTL where numWareHouseItemID in (SELECT numWareHouseItemID                                                                 
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                            
--    Op_Flag tinyint))                                             
	
	/*Enforce valiation for child records*/
--	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID] IN (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),
--    Op_Flag TINYINT)) )
--	BEGIN
--		raiserror('CHILD_WAREHOUSE',16,1);
--		RETURN ;
--	END

--  delete from  WareHouseItems where numWareHouseItemID in (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),                                                                 
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                  
    /*commented by chintan, Obsolete, Reason: While inserting new Row numWareHouseItemID will be 0,1,2 incrementally. */
--  delete from  WareHouseItems where numItemID=@numItemCode and  numWareHouseItemID not in (SELECT numWareHouseItemID                                                                    
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                    
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                        

/*If Item is Matrix item and changed to non matrix item then remove matrix attributes associated with- by chintan*/
--IF @OldGroupID>0 AND @numItemGroup = 0 
--BEGIN
--	DELETE FROM [CFW_Fld_Values_Serialized_Items] WHERE [bitSerialized] =0 AND 
--	[RecId] IN ( SELECT numWareHouseItemID FROM  OPENXML(@hDoc,'/NewDataSet/WareHouse',2) with(numWareHouseItemID numeric(9)) )
--END
                                         
                                             
                                                       
                                             
                                             
-- delete from WareHouseItmsDTL where numWareHouseItemID not in (select numWareHouseItemID from WareHouseItems)                                            
 
-- if @bitSerialized=0 AND @bitLotNo=0 delete from WareHouseItmsDTL where  numWareHouseItemID  in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)                                             
    
--    DELETE FROM  ItemUOM WHERE numItemCode=@numItemCode AND numDomainId=@numDomainId
    
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))                                        
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              
/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetabsincusfields')
DROP PROCEDURE usp_managetabsincusfields
GO
CREATE PROCEDURE [dbo].[USP_ManageTabsInCuSFields]      
@byteMode as tinyint=0,      
@LocID as numeric(9)=0,      
@TabName as varchar(50)='',      
@TabID as numeric(9)=0  ,    
--@vcURL as varchar(1000)='',  
@numDomainID as numeric(9),
@vcURL as varchar(100)=''    
as      
      
      
if @byteMode=1      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType)       
values(@TabName,@LocID,@numDomainID,0)      
      
end      
      
if @byteMode=2      
begin      
update  CFw_Grp_Master set Grp_Name=@TabName       
where Grp_id=@TabID      
      
end      
      
if @byteMode=3      
begin      
--Check if any custom fields associated with subtab 
IF (SELECT COUNT(*) FROM cfw_fld_master WHERE [subgrp]= @TabID)>0
BEGIN
	  RAISERROR ('CHILD_RECORD_FOUND',16,1);
	RETURN 
END
--delete permission
--[tintType]=1=subtab 
DELETE FROM [GroupTabDetails] WHERE [numTabId]=@TabID AND [tintType]=1
--delete subtabs
delete from  CFw_Grp_Master        
where Grp_id=@TabID  
    
      
end     
   
if @byteMode=4      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,1,@vcURL)      
      
END
--Added by chintan, this mode will add existing subtabs with tintType=2 which can not be deleted  
if @byteMode=5 
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,2,@vcURL)      
      
END
--Added by chintan, this mode will add Default subtabs and give permission to all Roles by default
-- Only required Parameter is DomainID
IF @byteMode = 6 
    BEGIN      
        IF ( SELECT COUNT(*) FROM   domain WHERE  numDomainID = @numDomainID ) > 0 
        BEGIN
			DECLARE @i NUMERIC(9)	

			--ContactDetails
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Contact Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Opportunities',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--SO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--PO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Case details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Case Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Project details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Milestones And Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Income & Expense',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Leads locid has been changed from 1 to 14
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Lead Detail',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Prospects
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Prospect Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Accounting',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Accounts
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Account Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Accounting',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contracts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Sachin ||Issue raised by Customer:EasternBikes
			--Reason:To Make default entry for any new subscription
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Items',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of code		

			/*********Get Auth groups */
			---- Give permission by default
				DECLARE  @minGroupID NUMERIC(9)
				DECLARE  @maxGroupID NUMERIC(9)
				SELECT @minGroupID=MIN(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				SELECT @maxGroupID=MAX(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				WHILE  @minGroupID <= @maxGroupID
				  BEGIN
					PRINT 'GroupID=' + CONVERT(VARCHAR(20),@minGroupID)
							Set @i=0
							DECLARE  @minTabID NUMERIC(9)
							DECLARE  @maxTabID NUMERIC(9)
							SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							SELECT @maxTabID=max([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							WHILE @minTabID<=@maxTabID
							BEGIN
								PRINT 'TabID=' + CONVERT(VARCHAR(20),@minTabID)
								---------------
								insert into GroupTabDetails
								(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType)
								VALUES (@minGroupID,@minTabID,1,@i+1,0,1)

								---------------
								SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID AND [Grp_id]>@minTabID
							END
					
					SELECT @minGroupID = min(numGroupID)
					FROM AuthenticationGroupMaster WHERE [numDomainID]= @numDomainID AND [numGroupID] > @minGroupID
				  END		


        END
      
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9)
)                                                                          
                                                                          
as  

 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode) from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100)
   ))X    
    
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END)
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes 
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200)
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems (
	numOppId,
	numTaxItemID,
	fltPercentage
) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
   union 
  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID

-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode) from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),                              
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200)
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,                 
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200)                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
		                                                             
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt money,numCreditAmt money,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = SUM(numDebitAmt)-SUM(numCreditAmt) FROM #temp
--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		                                                            
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0
                        
                        --Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId
						END
                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
						
                    drop table #temp
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO
