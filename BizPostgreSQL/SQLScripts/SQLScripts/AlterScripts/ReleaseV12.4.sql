/******************************************************************
Project: Release 12.4 Date: 20.AUG.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),10,122,'Purchase Fulfillment 2.0','../opportunity/frmMassPurchaseFulfillment.aspx',1,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		1,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

-------------------------------

ALTER TABLE OpportunityItems ADD numQtyReceived FLOAT
--ALTER TABLE OpportunityItems ADD dtReceivedDate DATE
ALTER TABLE MassSalesFulfillmentConfiguration ADD bitGeneratePickListByOrder BIT DEFAULT 0

----------------------------

UPDATE DycFormField_Mapping SET vcFieldName='Qty Received & Put-Away' WHERE numFormID=135 AND numFieldID=493

----------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO [dbo].[DycFieldMaster]
(
	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],[vcPropertyName],[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired]
)
SELECT
    [numModuleID],[numDomainID],'Qty Received Only','numQtyReceived','numQtyReceived','QtyReceivedOnly',[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],'Received Only Quantity',[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],0,0,1,0,0,1,0,0,0,0,0,0
FROM 
	DycFieldMaster 
WHERE 
	numFieldId=493

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting
)
VALUES
(
	3,@numFieldID,135,0,0,'Qty Received','Label',1,0,0,1,1
)


----------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassPurchaseFulfillmentConfiguration]    Script Date: 12-AUG-19 9:10:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassPurchaseFulfillmentConfiguration](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[bitGroupByOrderForReceive] [bit] NOT NULL,
	[bitGroupByOrderForPutAway] [bit] NOT NULL,
	[bitGroupByOrderForBill] [bit] NOT NULL,
	[bitGroupByOrderForClose] [bit] NOT NULL,
	[tintScanValue] [TINYINT] NOT NULL DEFAULT 1,
 CONSTRAINT [PK_MassPurchaseFulfillmentConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

----------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,119,135,0,0,'Created','DateField',32,1,32,1,0,0,1,1,1
)



INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,101,135,0,0,'Order status','SelectBox',17,1,17,1,0,0,1,1,0
),
(
	3,100,135,0,0,'Assigned To','SelectBox',35,1,35,1,0,0,1,1,1
),
(
	3,111,135,0,0,'Rec Owner','SelectBox',36,1,36,1,0,0,1,1,1
)

UPDATE DycFormField_Mapping SET bitAllowSorting=1 WHERE numFormID=135 AND numFieldID=189
UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=135 AND numFieldID IN (3,96,189) -- Include SKU field on production

-----------------------

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	143,'Mass Purchase Fulfillment Right Configuration','Y','N',0,0
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES

----------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,351,143,0,0,'Image','TextBox',1,1,1,1,0,1,1,0,0
),
(
	4,251,143,0,0,'Item Name','TextBox',2,1,2,1,0,1,1,1,1
),
(
	3,96,143,0,0,'Order Name','TextBox',3,1,3,1,0,0,1,1,1
),
(
	3,253,143,0,0,'Description','TextBox',4,1,4,1,0,0,1,0,0
),
(
	3,350,143,0,0,'Attributes','Label',5,1,5,1,0,0,1,0,0
),
(
	3,281,143,0,0,'SKU','Label',6,1,6,1,0,1,1,1,1
),
(
	3,203,143,0,0,'UPC','Label',7,1,7,1,0,0,1,1,1
),
(
	3,233,143,0,0,'Put-Away Location','SelectBox',10,1,10,1,0,0,1,1,0
)


------------------------

DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numQtyToShipReceive')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
)
VALUES
(
	3,@numFieldID,143,0,0,'Put-Away','TextBox',8,1,8,1,0,1,1,1,0,1
)

--------------------------------------------------


DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
)
VALUES
(
	3,@numFieldID,143,0,0,'Remaining','Label',9,1,9,1,0,1,1,1,0,1
)

-------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='SerialLotNo' AND vcLookBackTableName='OpportunityItems'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
)
VALUES
(
	3,@numFieldID,143,0,0,'Serial/Lot #s','TextBox',10,1,10,0,1,0,0,1
)


-------------------------------------------------------

-- Following updated on production

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Received Date','dtItemReceivedDate','dtItemReceivedDate','','OpportunityMaster','V','R','DateField',0,1,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,141,0,0,'Received Date','DateField',37,1,37,1,0,0,1,1,1
)

SELECT * FROM DycFieldMaster WHERE vcOrigDbColumnName='dtItemReceivedDate'


-----------------------------

insert into dycformfield_mapping
(
	nummoduleid,numfieldid,numformid,bitallowedit,bitinlineedit,vcfieldname,vcassociatedcontroltype,tintrow,tintcolumn,[order],bitinresults,bitdeleted,bitdefault,bitsettingfield,bitallowsorting,bitallowfiltering
)
values
(
	4,110,135,0,0,'Invoice #','TextBox',11,1,11,1,0,1,1,0,0
)

-----------------------------------------------

ALTER TABLE MassSalesFulfillmentConfiguration ADD
numOrderStatusPicked NUMERIC(18,0) DEFAULT 0
,numOrderStatusPacked1 NUMERIC(18,0) DEFAULT 0
,numOrderStatusPacked2 NUMERIC(18,0) DEFAULT 0
,numOrderStatusPacked3 NUMERIC(18,0) DEFAULT 0
,numOrderStatusPacked4 NUMERIC(18,0) DEFAULT 0
,numOrderStatusInvoiced NUMERIC(18,0) DEFAULT 0
,numOrderStatusPaid NUMERIC(18,0) DEFAULT 0
,numOrderStatusClosed NUMERIC(18,0) DEFAULT 0