/******************************************************************
Project: Release 11.5 Date: 28.MAR.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_DemandForecastDaysDisplay')
DROP FUNCTION fn_DemandForecastDaysDisplay
GO
CREATE FUNCTION [dbo].[fn_DemandForecastDaysDisplay]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numItemCode NUMERIC(18,0)
	,@numLeadDays NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays NUMERIC(18,0)
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @fltDemandBasedOnReleaseDate FLOAT = 0
	DECLARE @fltDemandBasedOnHistoricalSales FLOAT = 0
	DECLARE @fltDemandBasedOnOpportunity FLOAT = 0
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @numAnalysisDays INT = 0

	SET @fltDemandBasedOnReleaseDate = ISNULL((SELECT
												SUM(numUnitHour - ISNULL(numQtyShipped,0))
											FROM
												OpportunityItems OI
											INNER JOIN
												OpportunityMaster OM
											ON
												OI.numOppId=OM.numOppId
											WHERE
												OM.numDomainId=@numDomainID
												AND OM.tintOppType=1
												AND OM.tintOppStatus=1
												AND ISNULL(OM.tintshipped,0)=0
												AND OI.numItemCode=@numItemCode
												AND OI.numWarehouseItmsID=@numWarehouseItemID
												AND ISNULL(bitDropship,0) = 0
												AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
												AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)
	

	-- KIT CHILD ITEMS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0))
												FROM
													OpportunityKitItems OKI
												INNER JOIN
													OpportunityItems OI
												ON
													OKI.numOppItemID=OI.numoppitemtCode
												INNER JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKI.numChildItemID=@numItemCode
													AND OKI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	-- KIT CHILD ITEMS OF CHILD KITS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0))
												FROM
													OpportunityKitChildItems OKCI
												INNER JOIN
													OpportunityItems OI
												ON
													OKCI.numOppItemID=OI.numoppitemtCode
												INNER JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKCI.numItemID=@numItemCode
													AND OKCI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	--ITEMS USED IN WORK ORDER
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(WOD.numQtyItemsReq)
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrder WO
												ON
													WOD.numWOId=WO.numWOId
												LEFT JOIN
													OpportunityItems OI
												ON
													WO.numOppItemID = OI.numoppitemtCode
												LEFT JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													WO.numDomainID=@numDomainID
													AND WO.numWOStatus <> 23184 -- NOT COMPLETED
													AND WOD.numChildItemID = @numItemCode
													AND WOD.numWareHouseItemId=@numWarehouseItemID
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
													AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
													AND 1 = (CASE 
															WHEN OI.numoppitemtCode IS NOT NULL 
															THEN (CASE 
																	WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																	THEN 1 
																	ELSE 0 
																END)
															ELSE 
																(CASE 
																WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																THEN 1 
																ELSE 0 
																END)
															END)),0)


	IF ISNULL(@bitShowHistoricSales,0) = 1
	BEGIN
		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numHistoricalAnalysisPattern

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		SET @fltDemandBasedOnHistoricalSales = ISNULL((SELECT
															SUM(numUnitHour)
														FROM
															OpportunityItems OI
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND numItemCode=@numItemCode
															AND numWarehouseItmsID=@numWarehouseItemID
															AND ISNULL(bitDropship,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKI.numQtyItemsReq)
														FROM
															OpportunityKitItems OKI
														INNER JOIN
															OpportunityItems OI
														ON
															OKI.numOppItemID=OI.numoppitemtCode
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKI.numChildItemID=@numItemCode
															AND OKI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKCI.numQtyItemsReq)
														FROM
															OpportunityKitChildItems OKCI
														INNER JOIN
															OpportunityItems OI
														ON
															OKCI.numOppItemID=OI.numoppitemtCode
														INNER JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKCI.numItemID=@numItemCode
															AND OKCI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(WOD.numQtyItemsReq)
														FROM
															WorkOrderDetails WOD
														INNER JOIN
															WorkOrder WO
														ON
															WOD.numWOId=WO.numWOId
														LEFT JOIN
															OpportunityItems OI
														ON
															WO.numOppItemID = OI.numoppitemtCode
														LEFT JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															WO.numDomainID=@numDomainID
															AND WOD.numChildItemID = @numItemCode
															AND WOD.numWareHouseItemId=@numWarehouseItemID
															AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		SET @fltDemandBasedOnHistoricalSales = (@fltDemandBasedOnHistoricalSales/@numAnalysisDays) * @numForecastDays
	END

	IF ISNULL(@bitIncludeOpportunity,0) = 1
	BEGIN
		SET @fltDemandBasedOnOpportunity = ISNULL((SELECT
														SUM(OI.numUnitHour * (PP.intTotalProgress/100))
													FROM
														OpportunityItems OI
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND numItemCode=@numItemCode
														AND numWarehouseItmsID=@numWarehouseItemID
														AND ISNULL(bitDropship,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >=  @numOpportunityPercentComplete
												),0)

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityKitItems OKI
													INNER JOIN
														OpportunityItems OI
													ON
														OKI.numOppItemID=OI.numoppitemtCode
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKI.numChildItemID=@numItemCode
														AND OKI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKCI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityKitChildItems OKCI
													INNER JOIN
														OpportunityItems OI
													ON
														OKCI.numOppItemID=OI.numoppitemtCode
													INNER JOIN
														OpportunityMaster OM
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKCI.numItemID=@numItemCode
														AND OKCI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(WOD.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														WorkOrder WO
													ON
														OI.numoppitemtCode = WO.numOppItemID
													INNER JOIN
														WorkOrderDetails WOD
													ON
														WO.numWOId=WOD.numWOId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND WO.numDomainID=@numDomainID
														AND WO.numWOStatus <> 23184 -- NOT COMPLETED
														AND WOD.numChildItemID = @numItemCode
														AND WOD.numWareHouseItemId=@numWarehouseItemID
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
	END


	RETURN CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge ',CASE WHEN @numLeadDays > @numForecastDays THEN 'bg-red'  ELSE 'bg-light-blue' END,'">',CASE WHEN @numLeadDays > @numForecastDays THEN @fltDemandBasedOnReleaseDate * -1 ELSE @fltDemandBasedOnReleaseDate END,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKI.numChildItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1))
			,OKI.numUOMId
			,ISNULL(I.bitKitParent,0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1
						THEN (CASE 
								WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
								THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
								ELSE 1 
							END)
						ELSE 1
					END)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKCI.numItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,IOKI.numBaseUnit),1)) * (OKCI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,IOKCI.numBaseUnit),1))
			,OKCI.numUOMId
			,ISNULL(IOKCI.bitKitParent,0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item IOKCI
		ON
			OKCI.numItemID = IOKCI.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IOKI
		ON
			OKI.numChildItemID = IOKI.numItemCode
		WHERE
			OKCI.numOppId = @numOppID
			AND OKCI.numOppItemID = @numOppItemID		
			AND ISNULL(IOKI.bitKitParent,0) = 1 
			AND ISNULL(IOKI.bitCalAmtBasedonDepItems,0) = 1	
	END
	ELSE
	BEGIN
		DECLARE @TempExistingItems TABLE
		(
			vcItem VARCHAR(100)
		)

		INSERT INTO @TempExistingItems
		(
			vcItem
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcSelectedKitChildItems,',')

		DECLARE @TEMPSelectedKitChilds TABLE
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0),
			ChildQty FLOAT
		)

		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
			,ChildQty
		)
		SELECT 
			Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
		FROM  
		(
			SELECT 
				vcItem 
			FROM 
				@TempExistingItems
		) As [x]

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)


		IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
		BEGIN
			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				I.numItemCode
				,ChildQty
				,I.numBaseUnit
				,0
			FROM
				@TEMPSelectedKitChilds T1
			INNER JOIN
				Item I
			ON
				T1.ChildKitChildItemID = I.numItemCOde
			WHERE
				ISNULL(ChildKitItemID,0)=0
		END
		ELSE
		BEGIN
			;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE (CASE 
										WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
										THEN	
											(CASE 
												WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
												THEN 1 
												ELSE 0 
											END) 
										ELSE 1
									END)
							END)
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE
					Temp1.bitKitParent = 1
					AND Temp1.bitCalAmtBasedonDepItems = 1
					AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
			)

			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			FROM
				CTE
		END

		

		--IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ChildKitItemID = 0) > 0
		--BEGIN
		--	INSERT INTO @TEMPitems
		--	(
		--		numItemCode
		--		,numQtyItemsReq
		--		,numUOMId
		--		,bitKitParent
		--		,bitFirst
		--	)
		--	SELECT 
		--		ID.numChildItemID
		--		,CAST((ISNULL(T1.ChildQty,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
		--		,ISNULL(numUOMId,0)
		--		,ISNULL(I.bitKitParent,0)
		--		,1
		--	FROM
		--		[ItemDetails] ID
		--	INNER JOIN 
		--		[Item] I 
		--	ON 
		--		ID.[numChildItemID] = I.[numItemCode]
		--	INNER JOIN
		--		@TEMPSelectedKitChilds T1
		--	ON
		--		I.numItemCode=T1.ChildKitChildItemID
		--	WHERE   
		--		[numItemKitID] = @numItemCode
		--		AND 1 = (CASE 
		--					WHEN ISNULL(I.bitKitParent,0) = 1
		--					THEN (CASE 
		--							WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
		--							THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
		--							ELSE 1 
		--						END)
		--					ELSE 1
		--				END) 
		--END
		--ELSE
		--BEGIN
		--	INSERT INTO @TEMPitems
		--	(
		--		numItemCode
		--		,numQtyItemsReq
		--		,numUOMId
		--		,bitKitParent
		--		,bitFirst
		--	)
		--	SELECT 
		--		ID.numChildItemID
		--		,CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
		--		,ISNULL(numUOMId,0)
		--		,ISNULL(I.bitKitParent,0)
		--		,1
		--	FROM
		--		[ItemDetails] ID
		--	INNER JOIN 
		--		[Item] I 
		--	ON 
		--		ID.[numChildItemID] = I.[numItemCode]
		--	WHERE   
		--		[numItemKitID] = @numItemCode
		--		AND 1 = (CASE 
		--					WHEN ISNULL(I.bitKitParent,0) = 1 OR  ISNULL(I.bitAssembly,0) = 1
		--					THEN (CASE 
		--							WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
		--							THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
		--							ELSE 1 
		--						END)
		--					ELSE 1
		--				END) 
				
		--END

		--;WITH CTE (numItemCode, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
		--(
		--	SELECT
		--		I.numItemCode
		--		,ISNULL(I.bitKitParent,0)
		--		,ISNULL(I.bitCalAmtBasedonDepItems,0)
		--		,T1.numQtyItemsReq
		--		,T1.numUOMId
		--	FROM
		--		@TEMPitems T1
		--	INNER JOIN
		--		Item I
		--	ON
		--		T1.numItemCode = I.numItemCode
		--	UNION ALL
		--	SELECT
		--		ID.numChildItemID,
		--		ISNULL(I.bitKitParent,0),
		--		ISNULL(I.bitCalAmtBasedonDepItems,0),
		--		CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
		--		ISNULL(ID.numUOMId,0)
		--	FROM 
		--		CTE As Temp1
		--	INNER JOIN
		--		[ItemDetails] ID
		--	ON
		--		ID.numItemKitID = Temp1.numItemCode
		--	INNER JOIN 
		--		[Item] I 
		--	ON 
		--		ID.[numChildItemID] = I.[numItemCode]
		--	WHERE
		--		Temp1.bitKitParent = 1
		--		AND Temp1.bitCalAmtBasedonDepItems = 1
		--		AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID)
		--)

		--INSERT INTO @TEMPitems
		--(
		--	numItemCode
		--	,numQtyItemsReq
		--	,numUOMId
		--	,bitKitParent
		--)
		--SELECT
		--	numItemCode
		--	,numQtyItemsReq
		--	,numUOMId
		--	,bitKitParent
		--FROM
		--	CTE

		---- DELETING PREVIOUSLY ADDED ROWS TO AVOID DUPLICATION
		--DELETE FROM @TEMPitems WHERE bitFirst=1
	END

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		VALUES
		(
			0
			,0
		)
	END
	ELSE
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			1
			,(CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
		FROM
		(
			SELECT  
				ISNULL(CASE 
						WHEN I.[charItemType]='P' 
						THEN 
							CASE 
							WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
							THEN
								ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
							WHEN ISNULL(I.bitKitParent,0) = 1  AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
							THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,0) = 4 THEN WI.[monWListPrice] ELSE 0 END)
							ELSE
								(CASE @tintKitAssemblyPriceBasedOn
										WHEN 2 THEN ISNULL(I.monAverageCost,0) 
										WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
										ELSE WI.[monWListPrice]
								END) 
							END
						ELSE (CASE @tintKitAssemblyPriceBasedOn 
								WHEN 4 THEN I.[monListPrice]
								WHEN 2 THEN ISNULL(I.monAverageCost,0)
								WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
								ELSE I.[monListPrice] 
							END)
					END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
			FROM 
				@TEMPitems ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.numItemCode = I.[numItemCode]
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode = V.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1 
					*
				FROM
					WareHouseItems WI
				WHERE 
					WI.numItemID=I.numItemCode 
					AND WI.numWareHouseID = @numWarehouseID
				ORDER BY
					WI.numWareHouseItemID
			) AS WI
		) T1
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCalculatedPriceForKitAssembly')
DROP FUNCTION GetCalculatedPriceForKitAssembly
GO
CREATE FUNCTION [dbo].[GetCalculatedPriceForKitAssembly]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKI.numChildItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1))
			,OKI.numUOMId
			,ISNULL(I.bitKitParent,0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1
						THEN (CASE 
								WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
								THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
								ELSE 1 
							END)
						ELSE 1
					END)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKCI.numItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,IOKI.numBaseUnit),1)) * (OKCI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,IOKCI.numBaseUnit),1))
			,OKCI.numUOMId
			,ISNULL(IOKCI.bitKitParent,0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item IOKCI
		ON
			OKCI.numItemID = IOKCI.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IOKI
		ON
			OKI.numChildItemID = IOKI.numItemCode
		WHERE
			OKCI.numOppId = @numOppID
			AND OKCI.numOppItemID = @numOppItemID		
			AND ISNULL(IOKI.bitKitParent,0) = 1 
			AND ISNULL(IOKI.bitCalAmtBasedonDepItems,0) = 1	
	END
	ELSE
	BEGIN
		DECLARE @TempExistingItems TABLE
		(
			vcItem VARCHAR(100)
		)

		INSERT INTO @TempExistingItems
		(
			vcItem
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcSelectedKitChildItems,',')

		DECLARE @TEMPSelectedKitChilds TABLE
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0),
			ChildQty FLOAT
		)

		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
			,ChildQty
		)
		SELECT 
			Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
		FROM  
		(
			SELECT 
				vcItem 
			FROM 
				@TempExistingItems
		) As [x]

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)

		IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
		BEGIN
			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				I.numItemCode
				,ChildQty
				,I.numBaseUnit
				,0
			FROM
				@TEMPSelectedKitChilds T1
			INNER JOIN
				Item I
			ON
				T1.ChildKitChildItemID = I.numItemCOde
			WHERE
				ISNULL(ChildKitItemID,0)=0
		END
		ELSE
		BEGIN
			;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE (CASE 
										WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
										THEN	
											(CASE 
												WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
												THEN 1 
												ELSE 0 
											END) 
										ELSE 1
									END)
							END)
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE
					Temp1.bitKitParent = 1
					AND Temp1.bitCalAmtBasedonDepItems = 1
					AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
			)

			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			FROM
				CTE
		END
	END
	
	SELECT
		@monCalculatedPrice = (CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
	FROM
	(
		SELECT  
			ISNULL(CASE 
					WHEN I.[charItemType]='P' 
					THEN 
						CASE 
						WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
						THEN
							ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
						WHEN ISNULL(I.bitKitParent,0) = 1  AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
						THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,0) = 4 THEN WI.[monWListPrice] ELSE 0 END)
						ELSE
							(CASE @tintKitAssemblyPriceBasedOn
									WHEN 2 THEN ISNULL(I.monAverageCost,0) 
									WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
									ELSE WI.[monWListPrice]
							END) 
						END
					ELSE (CASE @tintKitAssemblyPriceBasedOn 
							WHEN 4 THEN I.[monListPrice]
							WHEN 2 THEN ISNULL(I.monAverageCost,0)
							WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
							ELSE I.[monListPrice] 
						END)
				END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
		FROM 
			@TEMPitems ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.numItemCode = I.[numItemCode]
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1 
				*
			FROM
				WareHouseItems WI
			WHERE 
				WI.numItemID=I.numItemCode 
				AND WI.numWareHouseID = @numWarehouseID
			ORDER BY
				WI.numWareHouseItemID
		) AS WI
	) T1

	RETURN @monCalculatedPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetKitWeightBasedOnItemSelection')
DROP FUNCTION GetKitWeightBasedOnItemSelection
GO
CREATE FUNCTION [dbo].[GetKitWeightBasedOnItemSelection]
(
	@numItemCode NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS FLOAT  
AS    
BEGIN   
	DECLARE @TempExistingItems TABLE
	(
		vcItem VARCHAR(100)
	)

	INSERT INTO @TempExistingItems
	(
		vcItem
	)
	SELECT 
		OutParam 
	FROM 
		SplitString(@vcSelectedKitChildItems,',')

	DECLARE @fltWeight AS FLOAT

	SET @fltWeight = ISNULL((SELECT
								SUM(numQty * ISNULL(I.fltWeight,0))
							FROM
							(
								SELECT 
									Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2)) As numItemCode
									,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3)) As numQty
								FROM  
								(
									SELECT 
										vcItem 
									FROM 
										@TempExistingItems
								) As [x] 
							) T2
							INNER JOIN
								Item I 
							ON
								T2.numItemCode = I.numItemCode),0)
	
	RETURN @fltWeight
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0),
	  @tintCommitAllocation TINYINT,
	  @bitFromBizDoc NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		bitDropship BIT,
		bitKitParent BIT,
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	DECLARE @bitAssembly BIT
	DECLARE @bitKitParent BIT
	DECLARE @bitWorkOrder BIT

	SELECT 
		@bitAssembly=ISNULL(bitAssembly,0)
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitWorkOrder=ISNULL(bitWorkOrder,0) 
	FROM 
		OpportunityItems 
	INNER JOIN 
		Item 
	ON 
		OpportunityItems.numItemCode=Item.numItemCode 
	WHERE 
		numOppId=@numOppID AND numoppitemtCode=@numOppItemID

	IF @bitKitParent = 1
	BEGIN
		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,bitDropship,bitKitParent,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKI.numOppItemID,
			OI.bitDropShip,
			ISNULL(I.bitKitParent,0),
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1,
			(CASE 
				WHEN OKI.numWareHouseItemId > 0 AND ISNULL(OI.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,0,1,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
							ELSE
								CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainID,@numOppID,@numOppItemID,OKI.numOppChildItemID,0,1,0,@numQty * OKI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<li><b><span style="color:red">',I.vcItemName, ':</b> ','</span>')
							ELSE
								CONCAT('<li><b>',I.vcItemName, ':</b> ') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(OKI.numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(OKI.numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>') 
										ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span></li>')
									END) 
							END
					END 
				ELSE 
					(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN 
							CONCAT('<li><b>',I.vcItemName, ':</b> ')
						ELSE
							CONCAT('<li>',I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
					END)
			END)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId=OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		LEFT JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		LEFT JOIN
			ItemDetails ID
		ON
			ID.numItemKitID = OI.numItemCode
			AND ID.numChildItemID=I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
		WHERE
			OM.numOppId = @numOppID
			AND OI.numoppitemtCode=@numOppItemID
		ORDER BY
			OKI.numOppChildItemID

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2,
			(CASE 
				WHEN OKCI.numWareHouseItemId > 0 AND ISNULL(c.bitDropship,0) = 0 AND ISNULL(@bitFromBizDoc,0) = 0
				THEN 
					CASE 
						WHEN ISNULL(I.bitAssembly,0)=1
						THEN 
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,0,1,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						WHEN ISNULL(I.bitKitParent,0)=1
						THEN
							CASE WHEN dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,@numOppID,@numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,1,0,c.numTotalQty * OKCI.numQtyItemsReq_Orig) = 1
							THEN
								CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
							ELSE
								CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
							END
						ELSE 
							CASE 
								WHEN @tintCommitAllocation=2 
								THEN (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numOnHand 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>') 
									END)
								ELSE (CASE 
										WHEN ISNULL(numQtyItemsReq,0) <= WI.numAllocation 
										THEN CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
										ELSE CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
									END) 
							END
					END 
				ELSE CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')') 
			END)
		FROM 
			OpportunityKitChildItems OKCI
		LEFT JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		LEFT JOIN
			UOM
		ON
			UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppID
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID


		INSERT INTO @TEMPTABLEFINAL
		(
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		)
		SELECT
			numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID, 
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
		FROM 
			@TEMPTABLE 
		WHERE 
		tintLevel=1
		
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

		WHILE @i <= @iCount
		BEGIN
			IF (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 0 OR (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 1 AND (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0))
			BEGIN
				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))
			END

			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
			BEGIN
				--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

				SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLEFINAL WHERE ID=@i)

				SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</li>')
			END

			SET @i = @i + 1
		END

		SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	END
	ELSE IF @bitAssembly=1 AND @bitWorkOrder=1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPFINAL TABLE
		(
			ID INT IDENTITY(1,1)
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,numUOMID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN @tintCommitAllocation=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


		SET @i  = 1

		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND T1.tintBuildStatus <> 2

			SET @i = @i + 1
		END



		INSERT INTO @TEMPFINAL
		(
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		)
		SELECT
			numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus
		FROM 
			@TEMP
		WHERE 
			ItemLevel=2
		
		SET @i = 1

		SELECT @iCount = COUNT(*) FROM @TEMPFINAL

		WHILE @i <= @iCount
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT
																																		(CASE 
																																			WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
																																			THEN 
																																				CONCAT(I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
																																			ELSE
																																				CONCAT('<span style="color:red">',I.vcItemName, ' (', CAST(T1.numQty AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')','</span>')
																																		END)
																																	FROM
																																		@TEMPFINAL T1
																																	INNER JOIN
																																		Item I
																																	ON
																																		T1.numItemCode = I.numItemCode
																																	LEFT JOIN
																																		WareHouseItems WI
																																	ON
																																		T1.numWarehouseItemID = WI.numWareHouseItemID
																																	LEFT JOIN
																																		UOM
																																	ON
																																		UOM.numUOMId = (CASE WHEN ISNULL(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
																																	WHERE 
																																		ID = @i))

			SET @i = @i + 1
		END
	END
	

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityItems.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', ',dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID),' (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'') AS NameValues
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDate,(CASE 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + ISNULL((SELECT 
																				SUM(numUnitHour) 
																			FROM 
																				OpportunityItems 
																			INNER JOIN 
																				OpportunityMaster 
																			ON 
																				OpportunityItems.numOppID=OpportunityMaster.numOppId 
																			WHERE 
																				OpportunityMaster.numDomainId=',@numDomainID,'
																				AND OpportunityMaster.tintOppType=2 
																				AND OpportunityMaster.tintOppStatus=0 
																				AND OpportunityItems.numItemCode=Item.numItemCode 
																				AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN 
														T1.numUnitHour > 0 
													THEN 1 
													ELSE 0 
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByItemVendor')
DROP PROCEDURE dbo.USP_DemandForecast_GetByItemVendor
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByItemVendor]
	 @numDomainID AS NUMERIC(9),
	 @numItemCode AS NUMERIC(9),
	 @numVendorID AS NUMERIC(18,0)
AS 
BEGIN
	

	SELECT 
		ISNULL((SELECT TOP 1 monCost FROM Vendor WHERE numVendorID = @numVendorID AND numItemCode = @numItemCode),0) AS monCost
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID

	SELECT 
		LD.numListItemID
		,LD.vcData
		,ISNULL(numListValue,0) AS numListValue
		,CONCAT('(',ISNULL(numListValue,'-'),' Days)') AS vcLeadDays
	FROM 
		ListDetails  LD
	OUTER APPLY
	(
		SELECT TOP 1
			*
		FROM
			VendorShipmentMethod VSM
		WHERE
			VSM.numListItemID = LD.numListItemID
			AND VSM.numVendorID = @numVendorID
		ORDER BY
			ISNULL(bitPrimary,0) DESC
	) VSMO
	WHERE 
		(LD.numDomainID=@numDomainID OR ISNULL(LD.constFlag,0) = 1)
		AND LD.numListID = 338
	ORDER BY
		bitPreferredMethod DESC,numListItemID ASC
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetRecordDetails')
DROP PROCEDURE dbo.USP_DemandForecast_GetRecordDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetRecordDetails]
	@numDomainID NUMERIC(18,0)
	,@tintType TINYINT
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	--,@dtReleaseDate DATE
	,@numForecastDays INT
	,@numAnalysisDays INT
	,@bitBasedOnLastYear BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,numOpportunityForecastQty FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	IF @tintType = 1 -- Release Dates
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OI.numItemCode=@numItemCode
			AND OI.numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		
		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq 
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE 
					WHEN OI.numoppitemtCode IS NOT NULL 
					THEN (CASE 
							WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
							THEN 1 
							ELSE 0 
						END)
					ELSE 
						(CASE 
						WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
						THEN 1 
						ELSE 0 
						END)
					END)
	END
	ELSE IF @tintType = 2 -- Historical Sales
	BEGIN
		DECLARE @dtFromDate DATETIME
		DECLARE @dtToDate DATETIME

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,(CASE WHEN WO.numWOStatus = 23184 THEN 0 ELSE WOD.numQtyItemsReq END)
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)
	END
	ELSE IF @tintType = 3 -- Sales Opportunity
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour
			,OI.numUnitHour * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq * (PP.intTotalProgress/100)
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WorkOrder WO
		ON
			OI.numoppitemtCode = WO.numOppItemID
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WO.numWOId=WOD.numWOId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete
	END

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya(10 Oct 2018)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbackorderpermission')
DROP PROCEDURE usp_getbackorderpermission
GO
CREATE PROCEDURE [dbo].[USP_GetBackOrderPermission]    
	@numDomainID AS NUMERIC(9)=0,
	@UserAuthGroupId AS NUMERIC(9),
	@numitemcode AS NUMERIC(9),
	@numQuantity AS FLOAT,
	@numWareHouseItemid AS NUMERIC(9),
	@kitChildItem AS VARCHAR(MAX)
AS
BEGIN
	DECLARE @UserGroupId AS NUMERIC(18,0)
	DECLARE @numBackOrder AS NUMERIC(18,0)

	SET @UserGroupId = (SELECT COUNT(*) FROM AuthenticationGroupBackOrder WHERE numGroupID = @UserAuthGroupId AND numDomainID = @numDomainID)

	IF @UserGroupId > 0
	BEGIN
		DECLARE @bitKitParent BIT
		SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numItemCode=@numitemcode

		IF @bitKitParent = 1
		BEGIN
			DECLARE @numWarehouseID NUMERIC(18,0)
			SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWareHouseItemid

			DECLARE @TEMPitems TABLE
			(
				numItemCode NUMERIC(18,0)
				,numWarehouseItemID NUMERIC(18,0)
				,numQtyItemsReq FLOAT
			)

			DECLARE @TempExistingItems TABLE
			(
				vcItem VARCHAR(100)
			)

			INSERT INTO @TempExistingItems
			(
				vcItem
			)
			SELECT 
				OutParam 
			FROM 
				SplitString(@kitChildItem,',')

			DECLARE @TEMPSelectedKitChilds TABLE
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0),
				ChildQty FLOAT
			)

			INSERT INTO @TEMPSelectedKitChilds
			(
				ChildKitItemID
				,ChildKitChildItemID
				,ChildQty
			)
			SELECT 
				Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
				,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
				,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
			FROM  
			(
				SELECT 
					vcItem 
				FROM 
					@TempExistingItems
			) As [x]

			UPDATE 
				@TEMPSelectedKitChilds 
			SET 
				ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
				,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)

			IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
			BEGIN
				INSERT INTO @TEMPitems
				(
					numItemCode
					,numWarehouseItemID
					,numQtyItemsReq
				)
				SELECT
					I.numItemCode
					,T1.ChildKitChildWarehouseItemID
					,@numQuantity * ChildQty
				FROM
					@TEMPSelectedKitChilds T1
				INNER JOIN
					Item I
				ON
					T1.ChildKitChildItemID = I.numItemCOde
				WHERE
					ISNULL(ChildKitItemID,0)=0
					AND ISNULL(bitKitParent,0) = 0
			END
			ELSE
			BEGIN
				;WITH CTE (numItemCode, bitKitParent, numQtyItemsReq) AS
				(
					SELECT
						ID.numChildItemID,
						ISNULL(I.bitKitParent,0),
						CAST(@numQuantity * (ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
					FROM 
						[ItemDetails] ID
					INNER JOIN 
						[Item] I 
					ON 
						ID.[numChildItemID] = I.[numItemCode]
					WHERE   
						[numItemKitID] = @numItemCode
						AND 1 = (CASE 
									WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@kitChildItem,'')) > 0 THEN 
										(CASE 
											WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
											THEN 1 
											ELSE 0 
										END) 
									ELSE (CASE 
											WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
											THEN	
												(CASE 
													WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
													THEN 1 
													ELSE 0 
												END) 
											ELSE 1
										END)
								END)
					UNION ALL
					SELECT
						ID.numChildItemID,
						ISNULL(I.bitKitParent,0),
						CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
					FROM 
						CTE As Temp1
					INNER JOIN
						[ItemDetails] ID
					ON
						ID.numItemKitID = Temp1.numItemCode
					INNER JOIN 
						[Item] I 
					ON 
						ID.[numChildItemID] = I.[numItemCode]
					WHERE
						Temp1.bitKitParent = 1
						AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
				)

				INSERT INTO @TEMPitems
				(
					numItemCode
					,numWarehouseItemID
					,numQtyItemsReq
				)
				SELECT
					numItemCode
					,(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID)
					,numQtyItemsReq
				FROM
					CTE
				WHERE
					ISNULL(bitKitParent,0) = 0
			END

		
			IF EXISTS (SELECT 
							T1.numItemCode 
						FROM 
							@TEMPitems T1 
						INNER JOIN 
							WareHouseItems WI 
						ON 
							T1.numWarehouseItemID=WI.numWareHouseItemID 
							WHERE ISNULL(numOnHand,0) < numQtyItemsReq)
			BEGIN
				SELECT 0
			END
			ELSE
			BEGIN
				SELECT 1
			END
		END
		ELSE
		BEGIN
			SET @numBackOrder =	(SELECT 
									COUNT(*) 
								FROM 
									WareHouseItems						
								WHERE 
									numItemID = @numitemcode
									AND ISNULL(numOnHand,0) < @numQuantity
									AND numWareHouseItemID = @numWareHouseItemid)

			IF @numBackOrder > 0
				SELECT 0
			ELSE
				SELECT 1
		END
	END
	ELSE
	BEGIN
		SELECT 1
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetKitChildsFirstLevel')
DROP PROCEDURE USP_GetKitChildsFirstLevel
GO
CREATE PROCEDURE [dbo].[USP_GetKitChildsFirstLevel]        
	@numDomainID as numeric(9)=0,
	@strItem as varchar(1000),
	@numItemCode AS NUMERIC(18,0) = 0,
	@numPageIndex AS INT,
	@numPageSize AS INT,     
	@TotalCount AS INT OUTPUT
AS
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @vcWareHouseSearch VARCHAR(1000) = ''
	DECLARE @vcVendorSearch VARCHAR(1000) = ''
	DECLARE @TEMPitems TABLE 
	( 
		numItemCode NUMERIC(18,0)
	)
	DECLARE @tintDecimalPoints AS TINYINT

	SELECT  
		@tintDecimalPoints=ISNULL(tintDecimalPoints,4)
	FROM    
		domain
	WHERE  
		numDomainID = @numDomainID
               
	SELECT 
		*
	INTO 
		#Temp1
	FROM
	( 
		SELECT    
			numFieldId ,
			vcDbColumnName ,
			ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
			tintRow AS tintOrder ,
			0 AS Custom
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 0
			AND ISNULL(bitSettingField, 0) = 1
			AND numRelCntType = 0
		UNION
		SELECT 
			numFieldId ,
			vcFieldName ,
			vcFieldName ,
			tintRow AS tintOrder ,
			1 AS Custom
		FROM
			View_DynamicCustomColumns
		WHERE
			Grp_id = 5
			AND numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 1
			AND numRelCntType = 0
	) X 
  
	IF NOT EXISTS (SELECT * FROM #Temp1)
	BEGIN
		INSERT INTO 
			#Temp1
		SELECT 
			numFieldId
			,vcDbColumnName
			,ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName
			,tintorder
			,0
		FROM
			View_DynamicDefaultColumns
		WHERE 
			numFormId = 22
			AND bitDefault = 1
			AND ISNULL(bitSettingField, 0) = 1
			AND numDomainID = @numDomainID 
	END

	DECLARE @tintOrder AS INT
    DECLARE @Fld_id AS NVARCHAR(20)
    DECLARE @Fld_Name AS VARCHAR(20)
        
		

    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_id = numFieldId ,
        @Fld_Name = vcFieldName
    FROM
		#Temp1
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > 0
    BEGIN
        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('+ @Fld_id + ', I.numItemCode) as [' + @Fld_Name + ']'

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_id = numFieldId ,
            @Fld_Name = vcFieldName
        FROM 
			#Temp1
        WHERE 
			Custom = 1
			AND tintOrder >= @tintOrder
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
            SET @tintOrder = 0
    END

	--Temp table for Item Search Configuration

    SELECT 
		*
    INTO
		#tempSearch
    FROM
	(
		SELECT
			numFieldId,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintRow AS tintOrder ,
            0 AS Custom
        FROM
			View_DynamicColumns
        WHERE
			numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND numRelCntType = 1
        UNION
        SELECT
			numFieldId,
            vcFieldName ,
            vcFieldName ,
            tintRow AS tintOrder ,
            1 AS Custom
		FROM
			View_DynamicCustomColumns
        WHERE
			Grp_id = 5
            AND numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 1
            AND numRelCntType = 1
    ) X 
  
    IF NOT EXISTS(SELECT * FROM #tempSearch)
    BEGIN
		INSERT INTO 
			#tempSearch
        SELECT 
			numFieldId ,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintorder ,
            0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	DECLARE @strSearch AS NVARCHAR(MAX)
	,@CustomSearch AS NVARCHAR(MAX)
	,@numFieldId AS NUMERIC(18,0)

	SET @strSearch = ''
        
    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM
		#tempSearch
    WHERE
		Custom = 0
    ORDER BY
		tintOrder
        
	WHILE @tintOrder > -1
    BEGIN
        IF @Fld_Name = 'vcPartNo'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Vendor
                    JOIN Item ON Item.numItemCode = Vendor.numItemCode
            WHERE   Item.numDomainID = @numDomainID
                    AND Vendor.numDomainID = @numDomainID
                    AND Vendor.vcPartNo IS NOT NULL
                    AND LEN(Vendor.vcPartNo) > 0
                    AND vcPartNo LIKE '%' + @strItem + '%'
							
			SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcBarCode'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                    AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                    AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcBarCode IS NOT NULL
                    AND LEN(WI.vcBarCode) > 0
                    AND WI.vcBarCode LIKE '%' + @strItem + '%'

			SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcWHSKU'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcWHSKU IS NOT NULL
                    AND LEN(WI.vcWHSKU) > 0
                    AND WI.vcWHSKU LIKE '%'
                    + @strItem + '%'

			IF LEN(@vcWareHouseSearch) > 0
				SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
			ELSE
				SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
		END
        ELSE
		BEGIN
            SET @strSearch = @strSearch + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @strItem + '%'''
		END

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM
			#tempSearch
        WHERE
			tintOrder >= @tintOrder
			AND Custom = 0
        ORDER BY 
			tintOrder
                        
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
		BEGIN
            IF @Fld_Name != 'vcPartNo' AND @Fld_Name != 'vcBarCode' AND @Fld_Name != 'vcWHSKU'
            BEGIN
                SET @strSearch = @strSearch + ' or '
            END
		END
    END

    IF (SELECT COUNT(*) FROM @TEMPitems) > 0
	BEGIN
        SET @strSearch = @strSearch + ' OR  I.numItemCode IN (select numItemCode from #tempItemCode)' 
	END

	--Custom Search
    SET @CustomSearch = ''
    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM 
		#tempSearch
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > -1
    BEGIN
        SET @CustomSearch = @CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM 
			#tempSearch
        WHERE 
			tintOrder >= @tintOrder
			AND Custom = 1
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
        BEGIN
            SET @CustomSearch = @CustomSearch + ' , '
        END
    END
	
    IF LEN(@CustomSearch) > 0
    BEGIN
        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
            + @strItem + '%'')'

        IF LEN(@strSearch) > 0
            SET @strSearch = @strSearch + ' OR ' + @CustomSearch
        ELSE
            SET @strSearch = @CustomSearch  
    END

	DECLARE @strSQLCount NVARCHAR(MAX) = ''

	SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
										COUNT(I.numItemCode) 
									FROM 
										Item  I WITH (NOLOCK)
									LEFT JOIN 
										DivisionMaster D              
									ON 
										I.numVendorID=D.numDivisionID  
									LEFT JOIN 
										CompanyInfo C  
									ON 
										C.numCompanyID=D.numCompanyID
									LEFT JOIN 
										CustomerPartNumber  CPN  
									ON 
										I.numItemCode=CPN.numItemCode 
										AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									WHERE ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID)
									+ ') > 0)) AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN (''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + ' AND ' + @strSearch  
						    

					SET @strSQLCount = @strSQLCount + '),0);'

                    SET @strSQL = 'select I.numItemCode, ISNULL(CPN.CustomerPartNo,'''')  AS CustomerPartNo,
									(SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									(SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									isnull(vcItemName,'''') vcItemName,
									CAST(CAST(CASE 
									WHEN I.[charItemType]=''P''
									THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0) 
									ELSE 
									monListPrice 
									END AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) +')) AS VARCHAR) AS monListPrice,
									isnull(vcSKU,'''') vcSKU,
									isnull(numBarCodeId,0) numBarCodeId,
									isnull(vcModelID,'''') vcModelID,
									isnull(txtItemDesc,'''') as txtItemDesc,
									isnull(C.vcCompanyName,'''') as vcCompanyName,
									D.numDivisionID,
									WHT.numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									(CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) bitHasKitAsChild
									,ISNULL(I.bitMatrix,0) AS bitMatrix
									,ISNULL(I.numItemGroup,0) AS numItemGroup
									,I.charItemType
									,ISNULL(I.bitKitParent,0) bitKitParent
									,ISNULL(I.bitAssembly,0) bitAssembly '
									+ @strSQL
									+ ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									Left join DivisionMaster D              
									on I.numVendorID=D.numDivisionID  
									left join CompanyInfo C  
									on C.numCompanyID=D.numCompanyID
									LEFT JOIN CustomerPartNumber  CPN  
									ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID) + ') > 0)) AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + ' AND ' + @strSearch   
						     
						

                       

	SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')


	SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @strItem + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'

	SET @strSQL = @strSQL + @strSQLCount

	SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						
	PRINT @strSQL
	EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT

	SELECT  *
	FROM    #Temp1
	WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 

	IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
	BEGIN
		DROP TABLE #Temp1
	END

	IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
	BEGIN
		DROP TABLE #tempSearch
	END

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_SelectedChildItemDetails')
DROP PROCEDURE USP_Item_SelectedChildItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_SelectedChildItemDetails]        
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numChildItemCode NUMERIC(18,0),
	@vcKitChilds VARCHAR(MAX),
	@bitEdit BIT 
AS
BEGIN
	DECLARE @bitCalAmtBasedonDepItems BIT
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@monListPrice DECIMAL(20,5)
	,@bitKit BIT
	,@bitHasChildKits BIT

	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
		,@monListPrice=ISNULL(monListPrice,0)
		,@bitKit=ISNULL(bitKitParent,0)
		,@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	WHERE
		numItemCode=@numItemCode

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,vcType VARCHAR(20)
		,numQty FLOAT
		,monListPrice DECIMAL(20,5)
		,monVendorCost DECIMAL(20,5)
		,monAverageCost DECIMAL(20,5)
		,monPrice DECIMAL(20,5)
		,numSequence INT
	)

	IF ISNULL(@bitKit,0) = 1
	BEGIN
		IF ISNULL(@bitEdit,0) = 1 AND LEN(ISNULL(@vcKitChilds,'')) = 0
		BEGIN
			SELECT 
				@vcKitChilds = COALESCE(@vcKitChilds + ', ', '') + CONCAT('0-',numChildItemID,'-',numQtyItemsReq * dbo.fn_UOMConversion(numUOMID,I.numItemCode,I.numDomainID,I.numPurchaseUnit),'-',ISNULL(sintOrder,1)) 
			FROM 
				ItemDetails
			INNER JOIN
				Item I
			ON
				numChildItemID=numItemCode
			WHERE
				numItemKitID=@numItemCode
		END

		IF LEN(ISNULL(@vcKitChilds,'')) > 0
		BEGIN
			DECLARE @TempExistingItems TABLE
			(
				vcItem VARCHAR(100)
			)

			INSERT INTO @TempExistingItems
			(
				vcItem
			)
			SELECT 
				OutParam 
			FROM 
				SplitString(@vcKitChilds,',')

			INSERT INTO @TEMP
			(
				numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence
			)
			SELECT
				I.numItemCode
				,ISNULL(I.vcItemName,'') vcItemName
				,ISNULL(I.vcSKU,'') vcSKU
				,(CASE 
					WHEN charItemType='P' 
					THEN CONCAT('Inventory',CASE 
												WHEN ISNULL(I.bitKitParent,0)=1 THEN ' (Kit)' 
												WHEN ISNULL(I.bitAssembly,0)=1 THEN ' (Assembly)' 
												WHEN ISNULL(I.numItemGroup,0) > 0 AND (ISNULL(I.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=I.numItemGroup AND tintType=2)) THEN ' (Matrix)' 
												ELSE '' 
											END) 
					WHEN charItemType='S' THEN 'Service' 
					WHEN charItemType='A' THEN 'Accessory' 
					WHEN charItemType='N' THEN 'Non-Inventory' 
				END) AS vcType
				,T1.numQty AS numQty
				,ISNULL(monListPrice,0) monListPrice
				,ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) monVendorCost
				,ISNULL(monAverageCost,0) monAverageCost
				,(CASE 
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=4 
					THEN ISNULL(T1.numQty,0) * ISNULL(monListPrice,0)
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=3 
					THEN ISNULL(T1.numQty,0) * ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=2 
					THEN ISNULL(T1.numQty,0) * ISNULL(monAverageCost,0)
					ELSE ISNULL(T1.numQty,0) * ISNULL(monListPrice,0)
				END)
				,T1.numSequence
			FROM
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						vcItem 
					FROM 
						@TempExistingItems
				) As [x] 
			) T1
			INNER JOIN
				Item I
			ON
				T1.numChildItemID = I.numItemCode
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode=V.numItemCode
		END

		IF (SELECT COUNT(*) FROM @TEMP WHERE numItemCode=@numChildItemCode) = 0
		BEGIN
			INSERT INTO @TEMP
			(
				numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence
			)
			SELECT
				I.numItemCode
				,ISNULL(I.vcItemName,'') vcItemName
				,ISNULL(I.vcSKU,'') vcSKU
				,(CASE 
					WHEN charItemType='P' 
					THEN CONCAT('Inventory',CASE 
												WHEN ISNULL(I.bitKitParent,0)=1 THEN ' (Kit)' 
												WHEN ISNULL(I.bitAssembly,0)=1 THEN ' (Assembly)' 
												WHEN ISNULL(I.numItemGroup,0) > 0 AND (ISNULL(I.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=I.numItemGroup AND tintType=2)) THEN ' (Matrix)' 
												ELSE '' 
											END) 
					WHEN charItemType='S' THEN 'Service' 
					WHEN charItemType='A' THEN 'Accessory' 
					WHEN charItemType='N' THEN 'Non-Inventory' 
				END) AS vcType
				,1 AS numQty
				,ISNULL(monListPrice,0) monListPrice
				,ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) monVendorCost
				,ISNULL(monAverageCost,0) monAverageCost
				,(CASE 
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=4 
						THEN 1 * ISNULL(monListPrice,0)
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=3 
						THEN 1 * ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=2 
						THEN 1 * ISNULL(monAverageCost,0)
						WHEN @bitCalAmtBasedonDepItems=1
						THEN 1 * ISNULL(monListPrice,0)
						ELSE 0
				END)
				,ISNULL((SELECT MAX(numSequence) FROM @TEMP),0) + 1
			FROM
				Item I
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode=V.numItemCode 
			WHERE
				I.numItemCode=@numChildItemCode
		END		
	END
	
	

	SELECT
		 @bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems
		 ,@tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
		 ,@monListPrice AS monListPrice
		 ,@bitKit AS bitKit
		 ,@bitHasChildKits AS bitHasChildKits
		 ,(CASE WHEN @tintKitAssemblyPriceBasedOn = 4 AND @bitCalAmtBasedonDepItems=1 THEN @monListPrice ELSE 0 END) + ISNULL((SELECT SUM(monPrice) FROM @TEMP),0) AS monPrice

	SELECT * FROM @TEMP
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @intNoOfPartners AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(1000),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
    @tintLogin TINYINT,
    @intNoofPartialSubscribed AS INTEGER,
    @intNoofMinimalSubscribed AS INTEGER,
	@dtEmailStartDate DATETIME,
	@dtEmailEndDate DATETIME,
	@intNoOfEmail INT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT
AS 
    IF @numSubscriberID = 0 
        BEGIN                                 
            IF NOT EXISTS ( SELECT  *
                            FROM    Subscribers
                            WHERE   numDivisionID = @numDivisionID
                                    AND numDomainID = @numDomainID ) 
                BEGIN              
                    SET @bitExists = 0                              
                    DECLARE @vcCompanyName AS VARCHAR(100)                             
                    DECLARE @numCompanyID AS NUMERIC(9)                                   
                    SELECT  @vcCompanyName = vcCompanyName,
                            @numCompanyID = C.numCompanyID
                    FROM    CompanyInfo C
                            JOIN DivisionMaster D ON D.numCompanyID = C.numCompanyID
                    WHERE   D.numDivisionID = @numDivisionID                                    
                                
                    DECLARE @numNewCompanyID AS NUMERIC(9)                                   
                    DECLARE @numNewDivisionID AS NUMERIC(9)                                    
                    DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
                    INSERT  INTO Domain
                            (
                              vcDomainName,
                              vcDomainDesc,
                              numDivisionID,
                              numAdminID,
                              tintLogin,tintDecimalPoints,tintChrForComSearch,tintChrForItemSearch, bitIsShowBalance,tintComAppliesTo,tintCommissionType, charUnitSystem, bitExpenseNonInventoryItem
                            )
                    VALUES  (
                              @vcCompanyName,
                              @vcCompanyName,
                              @numDivisionID,
                              @numAdminContactID,
                              @tintLogin,2,1,1,0,3,1,'E',1
                            )                                    
                    SET @numTargetDomainID = SCOPE_IDENTITY()                                       
                                       
                    INSERT  INTO CompanyInfo
                            (
                              vcCompanyName,
                              numCompanyType,
                              numCompanyRating,
                              numCompanyIndustry,
                              numCompanyCredit,
                              vcWebSite,
                              vcWebLabel1,
                              vcWebLink1,
                              vcWebLabel2,
                              vcWebLink2,
                              vcWebLabel3,
                              vcWebLink3,
                              vcWeblabel4,
                              vcWebLink4,
                              numAnnualRevID,
                              numNoOfEmployeesId,
                              vcHow,
                              vcProfile,
                              bitPublicFlag,
                              numDomainID
                            )
                            SELECT  vcCompanyName,
                                    93,--i.e employer --numCompanyType,
                                    numCompanyRating,
                                    numCompanyIndustry,
                                    numCompanyCredit,
                                    vcWebSite,
                                    vcWebLabel1,
                                    vcWebLink1,
                                    vcWebLabel2,
                                    vcWebLink2,
                                    vcWebLabel3,
                                    vcWebLink3,
                                    vcWeblabel4,
                                    vcWebLink4,
                                    numAnnualRevID,
                                    numNoOfEmployeesId,
                                    vcHow,
                                    vcProfile,
                                    bitPublicFlag,
                                    numDomainID
                            FROM    CompanyInfo
                            WHERE   numCompanyID = @numCompanyID                            
                    SET @numNewCompanyID = SCOPE_IDENTITY()                            
                                     
                    INSERT  INTO DivisionMaster
                            (
                              numCompanyID,
                              vcDivisionName,
                              numGrpId,
                              bitPublicFlag,
                              tintCRMType,
                              numStatusID,
                              tintBillingTerms,
                              numBillingDays,
                              tintInterestType,
                              fltInterest,
                              vcComPhone,
                              vcComFax,
                              numDomainID
                            )
                            SELECT  @numNewCompanyID,
                                    vcDivisionName,
                                    numGrpId,
                                    bitPublicFlag,
                                    2,
                                    numStatusID,
                                    tintBillingTerms,
                                    numBillingDays,
                                    tintInterestType,
                                    fltInterest,
                                    vcComPhone,
                                    vcComFax,
                                    numDomainID
                            FROM    DivisionMaster
                            WHERE   numDivisionID = @numDivisionID                     
                    SET @numNewDivisionID = SCOPE_IDENTITY()     
                                            
                            INSERT INTO dbo.AddressDetails (
								vcAddressName,
								vcStreet,
								vcCity,
								vcPostalCode,
								numState,
								numCountry,
								bitIsPrimary,
								tintAddressOf,
								tintAddressType,
								numRecordID,
								numDomainID
							) SELECT 
									 vcAddressName,
									 vcStreet,
									 vcCity,
									 vcPostalCode,
									 numState,
									 numCountry,
									 bitIsPrimary,
									 tintAddressOf,
									 tintAddressType,
									 @numNewDivisionID,
									 numDomainID FROM dbo.AddressDetails WHERE numRecordID= @numDivisionID AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = SCOPE_IDENTITY()                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              SCOPE_IDENTITY(),
                              @numNewContactID,
                              1
                            )                  
                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'System Administrator'
                       ) = 0 
                        BEGIN                       
                         
                            DECLARE @numGroupId1 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'System Administrator',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId1 = SCOPE_IDENTITY()                        
                            SET @TargetGroupId = @numGroupId1                     
                       
INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId1,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster
--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId1,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


--Added by Anoop for Dashboard +++++++++++++++++++

                            IF ( SELECT COUNT(*)
                                 FROM   customreport
                                 WHERE  numDomainId = @numTargetDomainID
                               ) = 0 
                                BEGIN                  
                                    EXEC CreateCustomReportsForNewDomain @numTargetDomainID,
                                        @numNewContactID              
                                END 

                            DECLARE @numGroupId2 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Executive',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 





                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Sales Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 






                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Warehouse Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY() 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                           
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           


                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 258
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Executive'
                                                            )

                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 259
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Sales Staff'
                                                            )


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 260
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Warehouse Staff'
                                                            )



------ Added by Anoop for Dashboard-----------       
                        END                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'Default Extranet'
                       ) = 0 
                        BEGIN                       
                                             
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Default Extranet',
                                      @numTargetDomainID,
                                      2,
                                      0
                                    )                      
                            SET @numGroupId2 = SCOPE_IDENTITY()                        
                      
                       
                            INSERT  INTO GroupAuthorization
                                    (
                                      numGroupID,
                                      numModuleID,
                                      numPageID,
                                      intExportAllowed,
                                      intPrintAllowed,
                                      intViewAllowed,
                                      intAddAllowed,
                                      intUpdateAllowed,
                                      intDeleteAllowed,
                                      numDomainID                      
                       
                                    )
                                    SELECT  @numGroupId2,
                                            x.numModuleID,
                                            x.numPageID,
                                            x.intExportAllowed,
                                            x.intPrintAllowed,
                                            x.intViewAllowed,
                                            x.intAddAllowed,
                                            x.intUpdateAllowed,
                                            x.intDeleteAllowed,
                                            @numTargetDomainID
                                    FROM    ( SELECT    *
                                              FROM      GroupAuthorization
                                              WHERE     numGroupid = 2
                                            ) x           
        
                          
--MainTab From TabMaster for Customer:46
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            46,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Customer:46
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            46,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


                          
--MainTab From TabMaster  for Employer:93
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                           93,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Employer:93
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            93,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 
      
                   
                        END                      
----                    IF ( SELECT COUNT(*)
----                         FROM   AuthenticationGroupMaster
----                         WHERE  numDomainId = @numTargetDomainID
----                                AND vcGroupName = 'Default PRM'
----                       ) = 0 
----                        BEGIN                       
----                         
----                            DECLARE @numGroupId3 AS NUMERIC(9)                      
----                            INSERT  INTO AuthenticationGroupMaster
----                                    (
----                                      vcGroupName,
----                                      numDomainID,
----                                      tintGroupType,
----                                      bitConsFlag 
----                                    )
----                            VALUES  (
----                                      'Default PRM',
----                                      @numTargetDomainID,
----                                      3,
----                                      0
----                                    )                      
----                            SET @numGroupId3 = SCOPE_IDENTITY()                        
----                      
----                       
----                            INSERT  INTO GroupAuthorization
----                                    (
----                                      numGroupID,
----                                      numModuleID,
----                                      numPageID,
----                                      intExportAllowed,
----                                      intPrintAllowed,
----                                      intViewAllowed,
----                                      intAddAllowed,
----                                      intUpdateAllowed,
----                                      intDeleteAllowed,
----                                      numDomainID                   
----                   
----                                    )
----                                    SELECT  @numGroupId3,
----                                            x.numModuleID,
----                                            x.numPageID,
----                                            x.intExportAllowed,
----                                            x.intPrintAllowed,
----                                            x.intViewAllowed,
----                                            x.intAddAllowed,
----                                            x.intUpdateAllowed,
----                                            x.intDeleteAllowed,
----                                            @numTargetDomainID
----                                    FROM    ( SELECT    *
----                                              FROM      GroupAuthorization
----                                              WHERE     numGroupid = 46
----                                            ) x             
----        
------                            INSERT  INTO GroupTabDetails
------                                    (
------                                      numGroupId,
------                                      numTabId,
------                                      numRelationShip,
------                                      bitallowed,
------                                      numOrder
------                                    )
------                                    SELECT  @numGroupId3,
------                                            x.numTabId,
------                                            x.numRelationShip,
------                                            x.bitallowed,
------                                            x.numOrder
------                                    FROM    ( SELECT    *
------                                              FROM      GroupTabDetails
------                                              WHERE     numGroupid = 46
------                                            ) x         
----                 
----                        END                      
                    
       

      
--- Set default Chart of Accounts 
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = SCOPE_IDENTITY()                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag,
									  numShippingZone,
									  vcAbbreviations
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag,
											numShippingZone,
											vcAbbreviations
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,ISNULL(fltExchangeRate,1),1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1, fltExchangeRate=1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = SCOPE_IDENTITY()                        
                      
                      
                              
                END                                
            ELSE 
                BEGIN                        
                    SELECT  @numSubscriberID = numSubscriberID
                    FROM    Subscribers
                    WHERE   numDivisionID = @numDivisionID
                            AND numDomainID = @numDomainID                                
                    UPDATE  Subscribers
                    SET     bitDeleted = 0
                    WHERE   numSubscriberID = @numSubscriberID                                
                END                                 
                                   
                     
        END                                    
    ELSE 
        BEGIN                                    
                                  
            DECLARE @bitOldStatus AS BIT                                  
            SET @bitExists = 1                                 
            SELECT  @bitOldStatus = bitActive,
                    @numTargetDomainID = numTargetDomainID
            FROM    Subscribers
            WHERE   numSubscriberID = @numSubscriberID                                    
                                  
            IF ( @bitOldStatus = 1
                 AND @bitActive = 0
               ) 
                UPDATE  Subscribers
                SET     dtSuspendedDate = GETUTCDATE()
                WHERE   numSubscriberID = @numSubscriberID                                  
                                  
            UPDATE  Subscribers
            SET     intNoofUsersSubscribed = @intNoofUsersSubscribed,
                    intNoOfPartners = @intNoOfPartners,
                    dtSubStartDate = @dtSubStartDate,
                    dtSubEndDate = @dtSubEndDate,
                    bitTrial = @bitTrial,
                    numAdminContactID = @numAdminContactID,
                    bitActive = @bitActive,
                    vcSuspendedReason = @vcSuspendedReason,
                    numModifiedBy = @numUserContactID,
                    dtModifiedDate = GETUTCDATE(),
                    intNoofPartialSubscribed = @intNoofPartialSubscribed,
                    intNoofMinimalSubscribed = @intNoofMinimalSubscribed,
					dtEmailStartDate= @dtEmailStartDate,
					dtEmailEndDate= @dtEmailEndDate,
					intNoOfEmail=@intNoOfEmail,
					bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
					bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin
            WHERE   numSubscriberID = @numSubscriberID                 
                       
                         UPDATE  Domain
                    SET     tintLogin = @tintLogin,
							numAdminID = @numAdminContactID
                    WHERE   numdomainID = @numTargetDomainID   
                           
            DECLARE @hDoc1 INT                                                              
            EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
            INSERT  INTO UserMaster
                    (
                      vcUserName,
                      vcMailNickName,
                      vcUserDesc,
                      numGroupID,
                      numDomainID,
                      numCreatedBy,
                      bintCreatedDate,
                      numModifiedBy,
                      bintModifiedDate,
                      bitActivateFlag,
                      numUserDetailId,
                      vcEmailID,
                      vcPassword
                    )
                    SELECT  UserName,
                            UserName,
                            UserName,
                            0,
                            @numTargetDomainID,
                            @numUserContactID,
                            GETUTCDATE(),
                            @numUserContactID,
                            GETUTCDATE(),
                            Active,
                            numContactID,
                            Email,
                            vcPassword
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                                        WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                            ) X                                
                
                              
            UPDATE  UserMaster
            SET     vcUserName = X.UserName,
                    numModifiedBy = @numUserContactID,
                    bintModifiedDate = GETUTCDATE(),
                    vcEmailID = X.Email,
                    vcPassword = X.vcPassword,
                    bitActivateFlag = Active
            FROM    ( SELECT    numUserID AS UserID,
                                numContactID,
                                Email,
                                vcPassword,
                                UserName,
                                Active
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                                WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                    ) X
            WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
            EXEC sp_xml_removedocument @hDoc1                               
                                   
        END                                    
    SELECT  @numSubscriberID
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				LEFT JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				LEFT JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,ISNULL(Opp.vcType,'''') ItemType
									,ISNULL(Opp.vcType,'''') vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,(CASE 
										WHEN ISNULL(I.bitKitParent,0)=1 
												AND LEN(ISNULL(Opp.vcChildKitSelectedItems,'''')) > 0 
												AND (CASE 
														WHEN ISNULL(bitKitParent,0) = 1 
														THEN 
															(CASE 
																WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
																THEN 1
																ELSE 0
															END)
														ELSE 0 
													END) = 0
										THEN 
											dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,Opp.vcChildKitSelectedItems)
										ELSE 
											ISNULL(I.fltWeight,0) 
									END) fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_PromotionOffer_ApplyItemPromotionToECommerce') 
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToECommerce
GO
CREATE  PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToECommerce]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)	
)
AS
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

	IF @numDivisionID > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numCartID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
		,numItemClassification NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
		,monInsertUnitPrice DECIMAL(20,5)
        ,fltInsertDiscount DECIMAL(20,5)
        ,bitInsertDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	INSERT INTO @TEMP
	(
		numCartID
        ,numItemCode
		,numItemClassification
        ,numUnits
		,numWarehouseItemID
        ,monUnitPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertUnitPrice
        ,fltInsertDiscount
        ,bitInsertDiscountType
        ,monTotalAmount
        ,numPromotionID
        ,bitPromotionTriggered
		,bitPromotionDiscount
		,vcKitChildItems
		,numSelectedPromotionID
		,bitChanged
	)
	SELECT
		numCartID
        ,CI.numItemCode
		,ISNULL(I.numItemClassification,0)
        ,numUnitHour * ISNULL(decUOMConversionFactor,1)
		,numWarehouseItmsID
        ,monPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
        ,monTotAmount
        ,PromotionID
        ,bitPromotionTrigerred
		,bitPromotionDiscount
		,vcChildKitItemSelection
		,numPreferredPromotionID
		,0
	FROM
		CartItems CI
	INNER JOIN
		Item I
	ON
		CI.numItemCode = I.numItemCode
	WHERE
		CI.numDomainId=@numDomainId
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @l INT = 1
	DECLARE @lCount INT
	SET @lCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @numTempPromotionID NUMERIC(18,0)
	DECLARE @tintOfferTriggerValueType TINYINT
	DECLARE @tintOfferBasedOn TINYINT
	DECLARE @tintOfferTriggerValueTypeRange TINYINT
	DECLARE @fltOfferTriggerValue FLOAT
	DECLARE @fltOfferTriggerValueRange FLOAT
	DECLARE @vcShortDesc VARCHAR(500)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @monTotalAmount DECIMAL(20,5)

	DECLARE @bitRemainingCheckRquired AS BIT
	DECLARE @numRemainingPromotion FLOAT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT

	DECLARE @TEMPUsedPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPUsedPromotion
	(
		ID
		,numPromotionID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
		,numPromotionID
		,SUM(numUnits)
		,SUM(monTotalAmount)
	FROM
		@TEMP T1
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
	GROUP BY
		numPromotionID


	DECLARE @j INT = 1
	DECLARE @jCount INT
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

	-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
	UPDATE 
		T1
	SET 
		numPromotionID=0
		,bitPromotionTriggered=0
		,bitPromotionDiscount=0
		,vcPromotionDescription=''
		,bitChanged=1
		,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
		,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
		,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
	FROM
		@TEMP T1
	WHERE 
		numPromotionID > 0
		AND ISNULL(bitPromotionTriggered,0) = 0
		AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(T2.bitPromotionTriggered,0) = 1) = 0

	-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPUsedPromotion 
		WHERE 
			ID = @j

		IF NOT EXISTS (SELECT 
						PO.numProId 
					FROM 
						PromotionOffer PO
					LEFT JOIN	
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					LEFT JOIN	
						DiscountCodes DC
					ON
						PO.numOrderPromotionID = DC.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND PO.numProId=@numTempPromotionID
						AND ISNULL(PO.bitEnabled,0)=1 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN 
										(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
									ELSE 1 
								END)
				)
		BEGIN
			-- IF Promotion is not valid than revert line item price to based on default pricing
			UPDATE 
				T1
			SET 
				numPromotionID=0
				,bitPromotionTriggered=0
				,bitPromotionDiscount=0
				,vcPromotionDescription=''
				,bitChanged=1
				,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
				,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
				,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
			FROM
				@TEMP T1
			WHERE 
				numPromotionID=@numTempPromotionID
		END

		SET @j = @j + 1
	END

	IF ISNULL(@bitBasedOnDiscountCode,0) = 1
	BEGIN
		DECLARE @TableItemCouponPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,tintOfferTriggerValueType TINYINT
			,tintOfferTriggerValueTypeRange TINYINT
			,fltOfferTriggerValue FLOAT
			,fltOfferTriggerValueRange FLOAT
			,tintOfferBasedOn TINYINT
			,vcShortDesc VARCHAR(300)
		)

		INSERT INTO @TableItemCouponPromotion
		(
			numPromotionID
			,tintOfferTriggerValueType
			,tintOfferTriggerValueTypeRange
			,fltOfferTriggerValue
			,fltOfferTriggerValueRange
			,tintOfferBasedOn
			,vcShortDesc
		)
		SELECT
			PO.numProId
			,PO.tintOfferTriggerValueType
			,PO.tintOfferTriggerValueTypeRange
			,PO.fltOfferTriggerValue
			,PO.fltOfferTriggerValueRange
			,PO.tintOfferBasedOn
			,ISNULL(PO.vcShortDesc,'-')
		FROM
			PromotionOffer PO
		INNER JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		INNER JOIN 
			DiscountCodes DC
		ON 
			POOrder.numProId = DC.numPromotionID
		WHERE
			PO.numDomainId = @numDomainID
			AND POOrder.numDomainId=@numDomainID 
			AND ISNULL(PO.bitEnabled,0)=1 
			AND ISNULL(POOrder.bitEnabled,0)=1 
			AND ISNULL(PO.bitUseOrderPromotion,0)=1 
			AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
			AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
			AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
			AND DC.vcDiscountCode = @vcDiscountCode
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC
			
		SET @l = 1
		DECLARE @k INT = 1
		DECLARE @kCount INT 
		SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

		WHILE @k <= @kCount
		BEGIN
			SELECT
				@numTempPromotionID=numPromotionID
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TableItemCouponPromotion
			WHERE
				ID=@k

			IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @i = 1

					WHILE @i <= @iCount
					BEGIN

						IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @i
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @i = @i + 1
					END

				END
			END

			SET @k = @k + 1
		END
	END

	-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
	SET @i = 1
	WHILE @i <= @iCount
	BEGIN
		SET @numTempPromotionID = NULL

		SELECT
			@numTempPromotionID=numProId
			,@tintOfferTriggerValueType=tintOfferTriggerValueType
			,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
			,@fltOfferTriggerValue=fltOfferTriggerValue
			,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
			,@tintOfferBasedOn=tintOfferBasedOn
			,@vcShortDesc=vcShortDesc
		FROM
			@TEMP T1
		INNER JOIN
			Item I
		ON
			T1.numItemCode=I.numItemCode
		CROSS APPLY
		(
			SELECT TOP 1
				PO.numProId
				,PO.tintOfferBasedOn
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,ISNULL(PO.vcShortDesc,'') vcShortDesc
			FROM 
				PromotionOffer PO
			WHERE
				PO.numDomainId=@numDomainID
				AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
				AND ISNULL(PO.bitUseOrderPromotion,0) = 0
				AND ISNULL(PO.bitRequireCouponCode,0) = 0
				AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE 
							WHEN ISNULL(numOrderPromotionID,0) > 0
							THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							ELSE
								(CASE tintCustomersBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									WHEN 3 THEN 1
									ELSE 0
								END)
						END)
				AND 1 = (CASE 
							WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 4 THEN 1 
							ELSE 0
						END)
			ORDER BY
				(CASE 
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
				END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

		) T2
		WHERE
			ISNULL(T1.numPromotionID,0)  = 0
			AND T1.ID = @i
			AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

		IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
		BEGIN
			IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
			BEGIN
				SET @l = 1

				WHILE @l <= @lCount
				BEGIN
						
					IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						UPDATE
							T1
						SET
							bitChanged = 1
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=1
							,vcPromotionDescription=ISNULL(@vcShortDesc,'')
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							T1.ID = @l
							AND ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
									END)
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
					END

					SET @l = @l + 1
				END

			END
		END

		SET @i = @i + 1
	END
		

	DECLARE @TEMPPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,vcShortDesc VARCHAR(500)
		,numTriggerItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPPromotion
	(
		ID
		,numPromotionID
		,numTriggerItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numCartID ASC)
		,numPromotionID
		,numItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID = PO.numProId
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
		AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

	SET @j = 1
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numItemCode=numTriggerItemCode
			,@numTempWarehouseItemID=numWarehouseItemID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPPromotion 
		WHERE 
			ID = @j

			
		SELECT
			@fltDiscountValue=ISNULL(fltDiscountValue,0)
			,@tintDiscountType=ISNULL(tintDiscountType,0)
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@vcShortDesc=ISNULL(vcShortDesc,'')
			,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
		FROM
			PromotionOffer
		WHERE
			numProId=@numTempPromotionID

		-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
		IF @tintDiscountType = 2 OR @tintDiscountType = 3
		BEGIN
			SET @bitRemainingCheckRquired = 1
		END
		ELSE
		BEGIN
			SET @bitRemainingCheckRquired = 0
			SET @numRemainingPromotion = 0
		END

		-- If promotion is valid than check whether any item left to apply promotion
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			IF @bitRemainingCheckRquired=1
			BEGIN
				IF @tintDiscountType = 2 -- Discount by amount
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
				END
				ELSE IF @tintDiscountType = 3 -- Discount by quantity
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
				END
			END

			IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
			BEGIN
				UPDATE
					T1
				SET
					monUnitPrice= (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
					,fltDiscount = (CASE 
										WHEN @bitRemainingCheckRquired=0 
										THEN 
											@fltDiscountValue
										ELSE 
											(CASE 
												WHEN @tintDiscountType = 2 -- Discount by amount
												THEN
													(CASE 
														WHEN (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) >= @numRemainingPromotion 
														THEN @numRemainingPromotion 
														ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
													END)
												WHEN @tintDiscountType = 3 -- Discount by quantity
												THEN
													(CASE 
														WHEN T1.numUnits >= @numRemainingPromotion 
														THEN (@numRemainingPromotion * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
														ELSE (T1.numUnits * (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																				ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																			END)) 
													END)
												ELSE 0
											END)
										END
									)
					,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
					,numPromotionID=@numTempPromotionID
					,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
					,vcPromotionDescription=@vcShortDesc
					,bitChanged=1
					,bitPromotionDiscount=1
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				WHERE
					ID=@i
					AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
					AND 1 = (CASE 
								WHEN @tintDiscoutBaseOn = 1 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
								THEN 
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 3 -- Related Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
								THEN
									(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
								ELSE 0
							END)
			END

			SET @i = @i + 1
		END

		SET @j = @j + 1
	END

	-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
	IF (SELECT 
			COUNT(*) 
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
	BEGIN
		UPDATE
			T1
		SET
			T1.numPromotionID = 0
			,T1.bitPromotionTriggered=0
			,T1.bitPromotionDiscount=0
			,T1.vcPromotionDescription=''
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
			,T1.fltDiscount=ISNULL(T1.fltInsertDiscount,0)
			,T1.bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			
			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1
			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN T1.numUnits >= @numRemainingPromotion 
															THEN (@numRemainingPromotion * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
															ELSE (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																				END)) 
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END
	END


	UPDATE
		CI
	SET
		monPrice=T1.monUnitPrice
		,fltDiscount=T1.fltDiscount
		,bitDiscountType=T1.bitDiscountType
		,monTotAmtBefDiscount = ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(T1.fltDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(T1.bitDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount >= 0
											THEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount
											ELSE 0
										END)
									ELSE ISNULL(CI.numUnitHour,0) * (ISNULL(T1.monUnitPrice,0) - (ISNULL(T1.monUnitPrice,0) * (T1.fltDiscount/100)))
								END)
							ELSE ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
						END)
		,PromotionID=T1.numPromotionID
		,bitPromotionTrigerred=T1.bitPromotionTriggered
		,PromotionDesc=T1.vcPromotionDescription
		,bitPromotionDiscount=T1.bitPromotionDiscount
	FROM
		CartItems CI
	INNER JOIN
		@Temp T1
	ON
		CI.numCartID = T1.numCartID
	WHERE
		T1.bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @l INT = 1
		DECLARE @lCount INT
		SET @lCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @tintOfferTriggerValueTypeRange TINYINT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @fltOfferTriggerValueRange FLOAT
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPUsedPromotion
		(
			ID
			,numPromotionID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
		FROM
			@TEMP T1
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
		GROUP BY
			numPromotionID

		DECLARE @j INT = 1
		DECLARE @jCount INT
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitPromotionDiscount=0
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPUsedPromotion 
			WHERE 
				ID = @j

			IF NOT EXISTS (SELECT 
								PO.numProId 
							FROM 
								PromotionOffer PO
							LEFT JOIN	
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							LEFT JOIN	
								DiscountCodes DC
							ON
								PO.numOrderPromotionID = DC.numPromotionID
							WHERE 
								PO.numDomainId=@numDomainID 
								AND PO.numProId=@numTempPromotionID
								AND ISNULL(PO.bitEnabled,0)=1 
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
											ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN
												(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											ELSE
												(CASE PO.tintCustomersBasedOn 
													WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
													WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
													WHEN 3 THEN 1
													ELSE 0
												END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN 
												(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
											ELSE 1 
										END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitPromotionDiscount=0
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			DECLARE @TableItemCouponPromotion TABLE
			(
				ID INT IDENTITY(1,1)
				,numPromotionID NUMERIC(18,0)
				,tintOfferTriggerValueType TINYINT
				,tintOfferTriggerValueTypeRange TINYINT
				,fltOfferTriggerValue FLOAT
				,fltOfferTriggerValueRange FLOAT
				,tintOfferBasedOn TINYINT
				,vcShortDesc VARCHAR(300)
			)

			INSERT INTO @TableItemCouponPromotion
			(
				numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc
			)
			SELECT
				PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,ISNULL(PO.vcShortDesc,'-')
			FROM
				PromotionOffer PO
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				PO.numDomainId = @numDomainID
				AND POOrder.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(POOrder.bitEnabled,0)=1 
				AND ISNULL(PO.bitUseOrderPromotion,0)=1 
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
			
			SET @l = 1
			DECLARE @k INT = 1
			DECLARE @kCount INT 
			SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

			WHILE @k <= @kCount
			BEGIN
				SELECT
					@numTempPromotionID=numPromotionID
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
					,@tintOfferBasedOn=tintOfferBasedOn
					,@vcShortDesc=vcShortDesc
				FROM
					@TableItemCouponPromotion
				WHERE
					ID=@k

				IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
				BEGIN
					IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						SET @i = 1

						WHILE @i <= @iCount
						BEGIN

							IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
							BEGIN
								UPDATE
									T1
								SET
									bitChanged = 1
									,numPromotionID=@numTempPromotionID
									,bitPromotionTriggered=1
									,vcPromotionDescription=ISNULL(@vcShortDesc,'')
								FROM
									@TEMP T1
								INNER JOIN
									Item I
								ON
									T1.numItemCode = I.numItemCode
								WHERE
									T1.ID = @i
									AND ISNULL(numPromotionID,0) = 0
									AND 1 = (CASE 
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
											END)
									AND 1 = (CASE 
												WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 4 THEN 1 
												ELSE 0
											END)
							END

							SET @i = @i + 1
						END

					END
				END

				SET @k = @k + 1
			END
		END

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			SET @numTempPromotionID = NULL

			SELECT
				@numTempPromotionID=numProId
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				WHERE
					PO.numDomainId=@numDomainID
					AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitUseOrderPromotion,0) = 0
					AND ISNULL(PO.bitRequireCouponCode,0) = 0
					AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

			IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @l = 1

					WHILE @l <= @lCount
					BEGIN
						
						IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @l
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @l = @l + 1
					END

				END
			END

			SET @i = @i + 1
		END
		

		DECLARE @TEMPPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN T1.numUnits >= @numRemainingPromotion 
															THEN (@numRemainingPromotion * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
															ELSE (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																				END)) 
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
		IF (SELECT 
				COUNT(*) 
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
		BEGIN
			UPDATE
				T1
			SET
				T1.numPromotionID = 0
				,T1.bitPromotionTriggered=0
				,T1.bitPromotionDiscount=0
				,T1.vcPromotionDescription=''
				,T1.bitChanged=1
				,T1.monUnitPrice=ISNULL(T2.monPrice,0)
				,T1.fltDiscount=ISNULL(T2.decDiscount,0)
				,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			CROSS APPLY
				dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

			SET @j = 1
			SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numTempPromotionID=numPromotionID
					,@numItemCode=numTriggerItemCode
					,@numTempWarehouseItemID=numWarehouseItemID
					,@numUnits = numUnits
					,@monTotalAmount=monTotalAmount
				FROM 
					@TEMPPromotion 
				WHERE 
					ID = @j

				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN T1.numUnits >= @numRemainingPromotion 
																THEN (@numRemainingPromotion * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
																ELSE (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
							,bitPromotionDiscount=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END

				SET @j = @j + 1
			END
		END
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
