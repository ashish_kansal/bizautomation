/******************************************************************
Project: Release 5.0 Date: 23.Nov.2015
Comments: STORED PROCEDURES
*******************************************************************/

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalBizDocTaxAmt')
DROP FUNCTION fn_CalBizDocTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalBizDocTaxAmt]
(
	@numDomainID as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppID AS NUMERIC(9),
	@numOppItemID as numeric(9),
	@tintMode AS TINYINT,
	@monAmount AS MONEY
) 
RETURNS MONEY
AS
BEGIN

	DECLARE @monTaxAmount AS MONEY = 0

	DECLARE @tintTaxOperator AS TINYINT;
	SET @tintTaxOperator=0
	
	DECLARE @fltPercentage FLOAT
	SET @fltPercentage=0

	DECLARE @tintTaxType AS TINYINT
	SET @tintTaxType = 1
   
	IF @tintMode=1 --Order
	BEGIN
		SELECT @tintTaxOperator=ISNULL([tintTaxOperator],0) FROM [OpportunityMaster] WHERE numOppID=@numOppID and numDomainID=@numDomainID

		SELECT @fltPercentage=ISNULL(fltPercentage,0), @tintTaxType=ISNULL(tintTaxType,1) FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID and numTaxItemID=@numTaxItemID    

		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			IF ISNULL(@tintTaxType,1) = 2
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0)
			END
			ELSE
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0) * ISNULL(@monAmount,0) / 100
			END
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @monTaxAmount = 0
		END
		ELSE IF (select COUNT(*) from OpportunityItemsTaxItems where numOppID=@numOppID and numOppItemId=@numOppItemID and numTaxItemID=@numTaxItemID)>0
		BEGIN
			IF ISNULL(@tintTaxType,1) = 2
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0)
			END
			ELSE
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0) * ISNULL(@monAmount,0) / 100
			END
		END 
	END

	IF @tintMode=2 --Return RMA
	BEGIN
		SELECT @fltPercentage=ISNULL(fltPercentage,0), @tintTaxType=ISNULL(tintTaxType,1) FROM OpportunityMasterTaxItems WHERE numReturnHeaderID=@numOppID and numTaxItemID=@numTaxItemID    

		IF (select COUNT(*) from OpportunityItemsTaxItems where numReturnItemID=@numOppItemID and numTaxItemID=@numTaxItemID)>0
		BEGIN
			IF ISNULL(@tintTaxType,1) = 2
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0)
			END
			ELSE
			BEGIN
				SET @monTaxAmount = ISNULL(@fltPercentage,0) * ISNULL(@monAmount,0) / 100
			END
		END 
	END

	RETURN ISNULL(@monTaxAmount,0)
END


--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_calitemtaxamt')
DROP FUNCTION fn_calitemtaxamt
GO
CREATE FUNCTION [dbo].[fn_CalItemTaxAmt]
(
	@numDomainID as numeric(9),
	@numDivisionID as numeric(9),
	@numItemCode as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppId as numeric(9),
	@bitAlwaysTaxApply AS BIT,
	@numReturnHeaderID AS NUMERIC(9)
) 
RETURNS @TEMP TABLE
(
	decTaxValue FLOAT, 
	tintTaxType TINYINT
)
AS
BEGIN

	SET @numOppId=NULLIF(@numOppId,0)
	SET @numReturnHeaderID=NULLIF(@numReturnHeaderID,0)

	declare @tintBaseTaxCalcOn as tinyint;
	declare @tintBaseTaxOnArea as tinyint;

	SELECT  @tintBaseTaxCalcOn = [tintBaseTax],@tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

	declare @TaxPercentage as float
	DECLARE @tintTaxType AS TINYINT = 1 -- 1 = PERCENTAGE, 2 = FLAT AMOUNT
	declare @bitTaxApplicable as bit;set @bitTaxApplicable=0 ---Check Tax is applied for the Item

	if @numTaxItemID=0 and @numItemCode<>0
		select @bitTaxApplicable=bitTaxable from Item where numItemCode=@numItemCode
	else if @numItemCode<>0
		select @bitTaxApplicable=bitApplicable from ItemTax where numItemCode=@numItemCode and numTaxItemID=@numTaxItemID
	else if @numItemCode=0 
		set @bitTaxApplicable=1


	---Check Tax is applied for Company
	if @bitTaxApplicable=1 AND @bitAlwaysTaxApply=0
	begin
		set @bitTaxApplicable=0

		if @numTaxItemID=0 
			select @bitTaxApplicable=(case when bitNoTax=1 then 0 else 1 end) from DivisionMaster where numDivisionID=@numDivisionID
		else
			select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=@numDivisionID and numTaxItemID=@numTaxItemID
	end


	if @bitTaxApplicable=1
	begin

		declare @numCountry as numeric(9),@numState as numeric(9)
		declare @vcCity as varchar(100),@vcZipPostal as varchar(20)

		IF @numOppId>0
		BEGIN
		  DECLARE  @tintOppType  AS TINYINT,@tintBillType  AS TINYINT,@tintShipType  AS TINYINT
	 
			SELECT  @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
			  FROM   OpportunityMaster WHERE  numOppId = @numOppId
	 
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				IF @tintBillType IS NULL or (@tintBillType = 1 AND @tintOppType = 1) or (@tintBillType = 1 AND @tintOppType = 2)
					BEGIN
							Select @numState = ISNULL(AD.numState, 0),
							@numCountry = ISNULL(AD.numCountry, 0),
							@vcCity=isnull(AD.VcCity,''),@vcZipPostal=isnull(AD.vcPostalCode,'')
							from AddressDetails AD where AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
					END
				ELSE IF @tintBillType = 0
					BEGIN
						Select @numState = ISNULL(AD1.numState, 0),
							@numCountry = ISNULL(AD1.numCountry, 0),
							@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')
							 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								   JOIN Domain D1  ON D1.numDivisionID = div1.numDivisionID
								   JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
								   WHERE  D1.numDomainID = @numDomainID
					END
				 ELSE IF @tintBillType = 2 or @tintBillType = 3
					BEGIN
							Select @numState = ISNULL(numBillState, 0),
							@numCountry = ISNULL(numBillCountry, 0),
							@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
							FROM   OpportunityAddress WHERE  numOppID = @numOppId
					END
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
				 IF @tintShipType IS NULL or (@tintShipType = 1 AND @tintOppType = 1) or (@tintShipType = 1 AND @tintOppType = 2) 
					BEGIN
							Select @numState = ISNULL(AD2.numState, 0),
							@numCountry = ISNULL(AD2.numCountry, 0),
							@vcCity=isnull(AD2.VcCity,''),@vcZipPostal=isnull(AD2.vcPostalCode,'')				
							from AddressDetails AD2 where AD2.numDomainID=@numDomainID AND AD2.numRecordID=@numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1
					END
	
				 ELSE IF @tintShipType = 0 
					BEGIN
							Select @numState = ISNULL(AD1.numState, 0),
							@numCountry = ISNULL(AD1.numCountry, 0),
							@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')	
							FROM companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								 JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
								 JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
								 WHERE  D1.numDomainID = @numDomainID
					END
		
				 ELSE IF @tintShipType = 2 or @tintShipType = 3  
					BEGIN
						Select @numState = ISNULL(numShipState, 0),
							@numCountry = ISNULL(numShipCountry, 0),
							@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
							FROM  OpportunityAddress WHERE  numOppID = @numOppId
					 END
			END
		END
		ELSE IF @numReturnHeaderID>0
		BEGIN
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
							Select @numState = ISNULL(numBillState, 0),
							@numCountry = ISNULL(numBillCountry, 0),
							@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
							FROM   OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
		 
						Select @numState = ISNULL(numShipState, 0),
							@numCountry = ISNULL(numShipCountry, 0),
							@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
							FROM  OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
         
			END
		END
	ELSE
	BEGIN
	   IF @tintBaseTaxCalcOn = 2 --Shipping Address(Default)
			   SELECT   @numState = ISNULL(numState, 0),
						@numCountry = ISNULL(numCountry, 0),
						@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @numDivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 2
						AND bitIsPrimary=1
		ELSE --Billing Address
				SELECT   @numState = ISNULL(numState, 0),
						@numCountry = ISNULL(numCountry, 0),
						@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @numDivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
	END 


	--Take from TaxCountryConfi If not then use default from domain
	select @tintBaseTaxOnArea=tintBaseTaxOnArea from TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numCountry 
             
	set @TaxPercentage=0 
             
	if @numCountry >0 and @numState>0
	begin
		if @numState>0        
		begin  
				if exists(select * from TaxDetails where numCountryID=@numCountry and numStateID=@numState and 
							(1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=@numState and 
								(1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numCountry and numStateID=@numState 
								and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=@numState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''      
						END
				else             
					select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID                      
		end                  
	end
	else if @numCountry >0              
		select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end
	else
		set @TaxPercentage=0


	INSERT INTO @TEMP 
	(
		decTaxValue,
		tintTaxType
	)
	VALUES
	(
		ISNULL(@TaxPercentage,0), 
		ISNULL(@tintTaxType,1)
	) 

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalOppItemTotalTaxAmt')
DROP FUNCTION fn_CalOppItemTotalTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalOppItemTotalTaxAmt]
(
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9), 
@numOppBizDocID as numeric(9)    
) 
RETURNS MONEY
AS
BEGIN

	DECLARE @numOppItemID AS NUMERIC(9) 
	DECLARE @numOppBizDocItemID AS NUMERIC(9) 
	DECLARE @numItemCode AS NUMERIC(9)  
	DECLARE @ItemAmount AS MONEY    
	DECLARE @TotalTaxAmt AS MONEY = 0 
	DECLARE @tintTaxOperator AS TINYINT = 0
	DECLARE @fltPercentage FLOAT = 0
	DECLARE @tintTaxType AS TINYINT = 1
	
	SELECT 
		@tintTaxOperator= ISNULL([tintTaxOperator],0) 
	FROM 
		[OpportunityMaster] 
	WHERE 
		numOppID=@numOppID

	SELECT 
		@fltPercentage=fltPercentage,
		@tintTaxType=ISNULL(tintTaxType,1) 
	FROM 
		OpportunityMasterTaxItems 
	WHERE 
		numOppId=@numOppID and numTaxItemID=@numTaxItemID    

	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)
	SELECT @numShippingServiceItemID = numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainID

	IF @numOppBizDocID=0
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = (CASE WHEN @tintTaxType = 2 THEN (ISNULL(@fltPercentage,0) * COUNT(*)) ELSE ISNULL(SUM(monTotAmount),0) * @fltPercentage / 100  END)
			FROM 
				OpportunityItems
			WHERE 
				numOppID=@numOppID  
				AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
				  
			RETURN ISNULL(@TotalTaxAmt,0)
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			RETURN  ISNULL(@TotalTaxAmt,0)
		END
		ELSE IF (ISNULL(@fltPercentage,0) > 0 AND 
					(SELECT [DivisionMaster].[bitNoTax] FROM [dbo].[DivisionMaster] 
					 WHERE [DivisionMaster].[numDomainID] = @numDomainID 
					  AND  [DivisionMaster].[numDivisionID] = (SELECT numdivisionID FROM [dbo].[OpportunityMaster] 
															   WHERE [OpportunityMaster].[numOppId] = @numOppID) ) = 0)
		BEGIN  
			SET @numOppItemID=0
					
			SELECT TOP 1 
				@numOppItemID=numoppitemtCode,
				@ItemAmount=monTotAmount 
			FROM 
				OpportunityItems
			WHERE 
				numOppID=@numOppID   
				AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
			ORDER BY 
				numoppitemtCode 
				   
				    
			WHILE @numOppItemID>0    
			BEGIN
				IF (select COUNT(*) from OpportunityItemsTaxItems where numOppItemId=@numOppItemID and numTaxItemID=@numTaxItemID)>0
				BEGIN
					SET @TotalTaxAmt= @TotalTaxAmt + (CASE WHEN @tintTaxType = 2 THEN ISNULL(@fltPercentage,0) ELSE @fltPercentage * @ItemAmount / 100 END)
				END    
				    
				SELECT TOP 1 
					@numOppItemID=numoppitemtCode,
					@ItemAmount=monTotAmount 
				FROM 
					OpportunityItems 
				WHERE 
					numOppID=@numOppID  
					AND numoppitemtCode>@numOppItemID
					AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)   
				ORDER BY 
					numoppitemtCode     
				    
				 IF @@rowcount=0 
					SET @numOppItemID=0    
			END    
		END    
		ELSE    
			SET @TotalTaxAmt=0  
	END
	ELSE --IF BIZDoc
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = (CASE WHEN @tintTaxType = 2 THEN (ISNULL(@fltPercentage,0) * COUNT(*)) ELSE (ISNULL(SUM(monTotAmount),0) * @fltPercentage / 100) END)
			FROM 
				OpportunityBizDocItems
			WHERE 
				numOppBizDocID=@numOppBizDocID  
				AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
				  
			RETURN ISNULL(@TotalTaxAmt,0)
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			RETURN  ISNULL(@TotalTaxAmt,0)
		END
		ELSE if (ISNULL(@fltPercentage,0)>0 AND 
				(SELECT [DivisionMaster].[bitNoTax] FROM [dbo].[DivisionMaster] 
					WHERE [DivisionMaster].[numDomainID] = @numDomainID 
					AND  [DivisionMaster].[numDivisionID] = (SELECT numdivisionID FROM [dbo].[OpportunityMaster] 
															WHERE [OpportunityMaster].[numOppId] = @numOppID) ) = 0)
		BEGIN  
			SET @numOppBizDocItemID=0
					
			SELECT TOP 1 
				@numOppBizDocItemID=numOppBizDocItemID,
				@numOppItemID=numOppItemID,
				@numItemCode=numItemCode,
				@ItemAmount=monTotAmount 
			FROM 
				OpportunityBizDocItems
			WHERE 
				numOppBizDocID=@numOppBizDocID
				AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
			ORDER BY 
				numOppBizDocItemID 
				   
				    
			WHILE @numOppBizDocItemID>0    
			BEGIN
				IF (select COUNT(*) from OpportunityItemsTaxItems where numOppItemId=@numOppItemID and numTaxItemID=@numTaxItemID)>0
				BEGIN
					set @TotalTaxAmt= @TotalTaxAmt + (CASE WHEN @tintTaxType = 2 THEN ISNULL(@fltPercentage,0) ELSE @fltPercentage * @ItemAmount / 100 END)
				END    
				    
				SELECT TOP 1 
					@numOppBizDocItemID=numOppBizDocItemID,
					@numOppItemID=numOppItemID,
					@numItemCode=numItemCode,
					@ItemAmount=monTotAmount 
				FROM 
					OpportunityBizDocItems 
				WHERE 
					numOppBizDocID=@numOppBizDocID 
					AND numOppBizDocItemID>@numOppBizDocItemID
					AND ISNULL(numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)   
				ORDER BY 
					numOppBizDocItemID     
				    
				IF @@rowcount=0 
					SET @numOppBizDocItemID=0    
			END    
		end    
		else    
			set @TotalTaxAmt=0  
END

return @TotalTaxAmt
end

--This function is specific to sales tax. generic function for calculating tax is fn_CalItemTaxAmt
--SELECT * FROM [OpportunityMaster] WHERE [numOppId]=12275
--SELECT * FROM [OpportunityBizDocs] WHERE [numOppBizDocsId]=12360
--SELECT * FROM [OpportunityAddress] WHERE [numOppID]=12340
-- select dbo.[fn_CalSalesTaxAmt](11946,110)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calsalestaxamt')
DROP FUNCTION fn_calsalestaxamt
GO
CREATE FUNCTION [fn_CalSalesTaxAmt]
    (
      @numOppBizDocID AS NUMERIC,
      @numDomainID AS NUMERIC 
    )
RETURNS FLOAT
BEGIN
    DECLARE @numDivisionID AS NUMERIC(9)
    DECLARE @numOppID AS NUMERIC(9)
    DECLARE @numCountry AS NUMERIC(9)
    DECLARE @numState AS NUMERIC(9)
    DECLARE @TaxPercentage AS MONEY
    
SELECT  @numOppID = [numOppId]  FROM    [OpportunityBizDocs]    WHERE   [numOppBizDocsId] = @numOppBizDocID

SELECT TOP 1 @numState=numState,@numCountry=numCountry FROM  dbo.[fn_getTaxableStateCountry](@numDomainID,@numOppID)
--             RETURN @numState
--              RETURN @numCountry
    SET @TaxPercentage = 0              
    IF @numCountry > 0
        AND @numState > 0 
        BEGIN
            SELECT  @TaxPercentage = decTaxPercentage
            FROM    TaxDetails
            WHERE   numCountryID = @numCountry
                    AND numStateID = @numState
                    AND numDomainID = @numDomainID
					and numTaxItemID = 0 -- for sales tax
        END 
    IF ISNULL(@TaxPercentage,0) = 0 
    BEGIN
		SELECT  @TaxPercentage = decTaxPercentage
            FROM    TaxDetails
            WHERE   numCountryID = @numCountry
                    AND numStateID = 0
                    AND numDomainID = @numDomainID
					and numTaxItemID = 0 -- for sales tax
	END    
	
    RETURN ISNULL(@TaxPercentage,0)
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CheckIfItemUOMConversionExists')
DROP FUNCTION fn_CheckIfItemUOMConversionExists
GO
CREATE FUNCTION [dbo].[fn_CheckIfItemUOMConversionExists]
(
	@numDomainID NUMERIC,
	@numItemCode NUMERIC,
	@numSourceUnit NUMERIC
)
RETURNS BIT
AS
BEGIN
	DECLARE @bitExists BIT  = 0
	DECLARE @numBaseUnit AS NUMERIC(18,0) = 0

	SELECT @numBaseUnit=ISNULL(numBaseUnit,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	
	IF @numSourceUnit=@numBaseUnit
		SET @bitExists = 1
	ELSE
	BEGIN
		DECLARE @intConversionsFount AS INT = 0

		;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
		(
			SELECT  
				IUOM.numSourceUOM,
				0 AS LevelNum,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				IUOM.numTargetUnit
			FROM 
				ItemUOMConversion IUOM
			WHERE   
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
				AND IUOM.numSourceUOM = @numSourceUnit
			UNION ALL
			SELECT  
				cte.FROMUnit,
				LevelNum + 1,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				(IUOM.numTargetUnit * cte.numTargetUnit)
			FROM 
				ItemUOMConversion IUOM
			INNER JOIN 
				CTE cte 
			ON 
				IUOM.numSourceUOM = cte.numTargetUOM 
			WHERE 
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
		)

		SELECT @intConversionsFount=COUNT(*) FROM CTE WHERE FROMUnit = @numSourceUnit AND numTargetUOM = @numBaseUnit

		IF ISNULL(@intConversionsFount,0) > 0
		BEGIN
			SET @bitExists = 1
		END
	END

	RETURN @bitExists
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_ItemUOMConversion')
DROP FUNCTION fn_ItemUOMConversion
GO
CREATE FUNCTION [dbo].[fn_ItemUOMConversion]
(
	@numFromUnit NUMERIC,
	@numItemCode NUMERIC,
	@numDomainID NUMERIC
)
RETURNS DECIMAL(18,5)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @MultiplicationFactor DECIMAL(18,5)
	
	SELECT @numBaseUnit=ISNULL(numBaseUnit,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	
	IF ISNULL(@numBaseUnit,0) = 0
		RETURN 1
 
	IF(@numFromUnit=@numBaseUnit)
		RETURN 1
	
	;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
	(
		SELECT  
			IUOM.numSourceUOM,
			0 AS LevelNum,
			IUOM.numSourceUOM,
			IUOM.numTargetUOM,
			IUOM.numTargetUnit
		FROM 
			ItemUOMConversion IUOM
		WHERE   
			IUOM.numDomainID=@numDomainID
			AND IUOM.numItemCode = @numItemCode
			AND IUOM.numSourceUOM = @numFromUnit
		UNION ALL
		SELECT  
			cte.FROMUnit,
			LevelNum + 1,
			IUOM.numSourceUOM,
			IUOM.numTargetUOM,
			(IUOM.numTargetUnit * cte.numTargetUnit)
		FROM 
			ItemUOMConversion IUOM
		INNER JOIN 
			CTE cte 
		ON 
			IUOM.numSourceUOM = cte.numTargetUOM 
		WHERE 
			IUOM.numDomainID=@numDomainID
			AND IUOM.numItemCode = @numItemCode
	)

	SELECT TOP 1 @MultiplicationFactor=numTargetUnit FROM CTE WHERE FROMUnit = @numFromUnit AND numTargetUOM = @numBaseUnit

	IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT NULL)
	BEGIN
		RETURN @MultiplicationFactor	
	END 

RETURN 1
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_UOMConversion')
DROP FUNCTION fn_UOMConversion
GO
CREATE FUNCTION [dbo].[fn_UOMConversion]
(
	@numFromUnit NUMERIC,
	@numItemCode NUMERIC,
	@numDomainID NUMERIC,
	@numToUnit NUMERIC=null
)
RETURNS DECIMAL(28,14)
AS
BEGIN
	DECLARE @bitEnableItemLevelUOM AS BIT = 0
	DECLARE @MultiplicationFactor DECIMAL(28,14)

	SELECT @bitEnableItemLevelUOM=ISNULL(bitEnableItemLevelUOM,0) FROM Domain WHERE numDomainID = @numDomainID
 
	IF ISNULL(@numToUnit,0) = 0
		SELECT @numToUnit=numBaseUnit FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

	IF EXISTS (SELECT @numToUnit WHERE @numToUnit IS null)
		RETURN 1
 
	IF(@numFromUnit=@numToUnit)
		RETURN 1

	IF @bitEnableItemLevelUOM = 0
	BEGIN
		;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
		(
			select  numUOM1,0 as LevelNum
					,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
					,CONVERT(DECIMAL(18,5),ct.decConv2 / ct.decConv1) as multiplicationFactor
			from UOMConversion ct
			where   ct.numUOM1 = @numFromUnit AND ct.numDomainID=@numDomainID

			union all

			select  FROMUnit,LevelNum + 1
					,rt.numUOM2,rt.decConv2,ct.numUOM2,ct.decConv2
					,CONVERT(DECIMAL(18,5),rt.multiplicationFactor * CONVERT(DECIMAL(18,5),(ct.decConv2 / ct.decConv1)))
			from UOMConversion ct
			inner join recursiveTable rt on rt.numUOM2 = ct.numUOM1 WHERE ct.numDomainID=@numDomainID
		)

		select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM2 = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
		BEGIN
			RETURN @MultiplicationFactor	
		END 
		ELSE
		BEGIN
			;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
			(
				select  numUOM2,0 as LevelNum
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(DECIMAL(18,5),ct.decConv1 / ct.decConv2) as multiplicationFactor
				from UOMConversion ct
				where   ct.numUOM2 = @numFromUnit AND ct.numDomainID=@numDomainID

				union all

				select  FROMUnit,LevelNum + 1
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(DECIMAL(18,5),rt.multiplicationFactor * CONVERT(DECIMAL(18,5),(ct.decConv1 / ct.decConv2)))
				from UOMConversion ct
				inner join recursiveTable rt on rt.numUOM1 = ct.numUOM2 WHERE ct.numDomainID=@numDomainID
			)

			select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM1 = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END
	ELSE
	BEGIN
		;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
		(
			SELECT  
				IUOM.numSourceUOM,
				0 AS LevelNum,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				IUOM.numTargetUnit
			FROM 
				ItemUOMConversion IUOM
			WHERE   
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
				AND IUOM.numSourceUOM = @numFromUnit
			UNION ALL
			SELECT  
				cte.FROMUnit,
				LevelNum + 1,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				(IUOM.numTargetUnit * cte.numTargetUnit)
			FROM 
				ItemUOMConversion IUOM
			INNER JOIN 
				CTE cte 
			ON 
				IUOM.numSourceUOM = cte.numTargetUOM 
			WHERE 
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
		)

		SELECT TOP 1 @MultiplicationFactor=numTargetUnit FROM CTE WHERE FROMUnit = @numFromUnit AND numTargetUOM = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT NULL)
		BEGIN
			RETURN @MultiplicationFactor	
		END
		ELSE
		BEGIN
			;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
			(
				SELECT  
					IUOM.numTargetUOM,
					0 AS LevelNum,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					(1 / IUOM.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				WHERE   
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
					AND IUOM.numTargetUOM = @numFromUnit
				UNION ALL
				SELECT  
					cte.FROMUnit,
					LevelNum + 1,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					((1 / IUOM.numTargetUnit) * cte.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				INNER JOIN 
					CTE cte 
				ON 
					IUOM.numTargetUOM = cte.numSourceUOM 
				WHERE 
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
			)

			select top 1 @MultiplicationFactor=numTargetUnit from cte where FROMUnit = @numFromUnit and numSourceUOM = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END

RETURN 1
END
GO
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdealamount')
DROP FUNCTION getdealamount
GO
CREATE FUNCTION [dbo].[GetDealAmount]
(@numOppID numeric,@dt Datetime,@numOppBizDocsId numeric=0)    
returns money    
as    
begin   
  
  
declare @DivisionID as numeric(9)                         
                                  
declare @TaxPercentage as float                    
  
declare @shippingAmt as money    
declare @TotalTaxAmt as money     
declare @decDisc as float 
declare @bitDiscType as bit ;set @bitDiscType=0     
declare @decInterest as float    
    
set @decInterest=0    
set @decDisc=0    
declare @bitBillingTerms as bit     
declare @intBillingDays as integer    
declare @bitInterestType as bit    
declare @fltInterest as float    
declare @dtFromDate as varchar(20)    
declare @strDate as datetime    
declare @TotalAmt as money    
declare @TotalAmtAfterDisc as money    
declare @fltDiscount as float  
declare @tintOppStatus as tinyint  
declare @tintshipped as tinyint 
declare @dtshipped as datetime 
declare @numBillCountry as numeric(9)
declare @numBillState as numeric(9)
declare @numDomainID as numeric(9)
declare @tintOppType tinyint
declare @bitTaxApplicable BIT
set @TotalAmt=0


--declare @monCreditAmount as money;set @monCreditAmount=0    
DECLARE @tintTaxOperator AS TINYINT;SET @tintTaxOperator=0

select    
@DivisionID=numDivisionID,@shippingAmt=0,    
@bitBillingTerms=0,    
@intBillingDays=0,@bitInterestType=0,    
@fltInterest=0,@fltDiscount=0,
@tintOppStatus=tintOppStatus ,@tintshipped=tintshipped,@numDomainID=numDomainID,@tintOppType=tintOpptype,
@tintTaxOperator=ISNULL([tintTaxOperator],0)
from OpportunityMaster where numOppId=@numOppId          
   

IF @numOppBizDocsId=0
BEGIN
	select @TotalAmt=isnull(sum(X.amount),0)    
			from (SELECT monTotAmount as Amount from OpportunityItems opp                   
					join item i on opp.numItemCode=i.numItemCode where numOppId=@numOppId  
			 union ALL --timeandexpense (expense)  
			  SELECT monAmount as amount from timeandexpense   
			   where (numOppId=@numOppId or numOppBizDocsId in (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId ) )  
			   and numCategory = 2 and numtype=1  
			)x  
END
ELSE
BEGIN
SELECT @bitBillingTerms=isnull(bitBillingTerms,0),    
	@intBillingDays=isnull(intBillingDays,0),@bitInterestType=isnull(bitInterestType,0),    
	@fltInterest=isnull(fltInterest,0),@fltDiscount=isnull(fltDiscount,0),
	@bitDiscType=isnull(bitDiscountType,0)
FROM dbo.OpportunityMaster WHERE numOppId=@numOppID

	select    
	@shippingAmt=isnull(monShipCost,0),    
	@dtshipped=dtShippedDate,
--	@monCreditAmount=isnull(monCreditAmount,0),
	@dtFromDate=dtFromDate
	from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId

select @TotalAmt=isnull(sum(X.amount),0)
from (SELECT OBD.monTotAmount as Amount from OpportunityItems opp                   
		join OpportunityBizDocItems OBD on OBD.numOppItemID=Opp.numoppitemtCode    
		where numOppId=@numOppId  and OBD.numOppBizDocID=@numOppBizDocsId
 
	union ALL  --timeandexpense (expense)  
	 select monAmount as amount from timeandexpense  where (numOppId=@numOppId or numOppBizDocsId =@numOppBizDocsId )  
		and numCategory = 2 and numtype=1  
)x  

END

	SET @TotalTaxAmt=isnull(dbo.[GetDealTaxAmount](@numOppId,@numOppBizDocsId,@numDOmainId),0)


set @TotalAmtAfterDisc= @TotalAmt 
if @tintOppStatus=1 
begin  
   
    if @tintshipped=1 set @dt=isnull(@dtshipped,@dt)
	if  @bitBillingTerms=1    
	begin    
		--set @strDate = DateAdd(Day, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(@intBillingDays,0)),0)),@dtFromDate)    
		set @strDate = DateAdd(Day, convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(@intBillingDays,0)),0)),@dtFromDate)    
		--(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	     
		if @bitInterestType=0     
		begin    
			if @strDate>@dt    
			begin    
				set @decDisc=0    
			end    
			else    
			begin    
				set @decDisc=@fltInterest    
	       
			end    
		end    
		ELSE    
		begin    
			if @strDate>@dt    
			begin    
				set @decInterest=0    
			end    
			else    
			begin    
				set @decInterest=@fltInterest    
			end    
		end    
	end    
	 
	if @decDisc>0  
	begin      
          if @bitDiscType=0 set @TotalAmtAfterDisc=@TotalAmt-((@TotalAmt*@fltDiscount/100)+((@TotalAmt*(100-@fltDiscount)/100)*@decDisc/100))+@shippingAmt+@TotalTaxAmt  
          else if @bitDiscType=1  set @TotalAmtAfterDisc=@TotalAmt-(@fltDiscount+(@TotalAmt-@fltDiscount)*@decDisc/100)+@shippingAmt+@TotalTaxAmt  
	end    
	else  
	begin 
		if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt*(100-@fltDiscount)/100)/100)-(@TotalAmt*@fltDiscount/100)+@shippingAmt+@TotalTaxAmt    
		else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt-@fltDiscount)/100)-(@fltDiscount)+@shippingAmt+@TotalTaxAmt    
	 end  
end 
else
begin
	if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -(@TotalAmt*@fltDiscount/100)
	else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -@fltDiscount
end

 --SET @TotalAmtAfterDisc = @TotalAmtAfterDisc - isnull(@monCreditAmount,0)
SET @TotalAmtAfterDisc = ROUND(@TotalAmtAfterDisc,2)

return CAST(@TotalAmtAfterDisc AS MONEY) -- Set Accuracy of Two precision
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetDealTaxAmount')
DROP FUNCTION GetDealTaxAmount
GO
CREATE FUNCTION [dbo].[GetDealTaxAmount]
(
	@numOppID NUMERIC(18,0),
	@numOppBizDocsId NUMERIC(18,0) = 0,
	@numDomainId NUMERIC(18,0)
)    
RETURNS MONEY
AS    
BEGIN   
	DECLARE @TotalTaxAmt AS MONEY = 0		
	DECLARE @numTaxItemID AS NUMERIC(9)
		
	SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId ORDER BY numTaxItemID 
 
	WHILE @numTaxItemID>-1    
	BEGIN
		SELECT @TotalTaxAmt = @TotalTaxAmt + ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocsId),0)
    
		SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID>@numTaxItemID ORDER BY numTaxItemID
    
		IF @@rowcount=0 
			SET @numTaxItemID=-1    
	END   
	    
	RETURN CAST(ISNULL(@TotalTaxAmt,0) AS MONEY) -- Set Accuracy of Two precision
END
GO
GO
--select dbo.GetReturnDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetReturnDealAmount')
DROP FUNCTION GetReturnDealAmount
GO
CREATE FUNCTION [dbo].[GetReturnDealAmount]
(
	@numReturnHeaderID NUMERIC(18,0),
	@tintMode TINYINT
)    
RETURNS @Result TABLE     
(    
	monAmount MONEY,
	monTotalTax MONEY,
	monTotalDiscount MONEY  
)    
AS    
BEGIN   
 
	DECLARE @monTotalDiscount MONEY,@monAmount MONEY,@monTotalTax MONEY,@tintReturnType TINYINT 
 
	SELECT 
		@monTotalDiscount=ISNULL(monTotalDiscount,0),
		@monAmount=ISNULL(monAmount,0),
		@tintReturnType=tintReturnType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
 
	 IF @tintMode=5 OR @tintMode=6 OR @tintMode=7 OR @tintMode=8 OR (@tintReturnType=1 AND @tintMode=9)
	 BEGIN
 	
		SET @monTotalTax=0
 	
 		DECLARE @numTaxItemID AS NUMERIC(9)
		DECLARE @fltPercentage FLOAT = 0
		DECLARE @tintTaxType TINYINT = 1
		DECLARE @ItemAmount MONEY = 0

		SELECT @monAmount=SUM(
								(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END)  
								* (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(monTotAmount,0) / numUnitHour) END)
							) 
		FROM 
			ReturnItems 
		WHERE 
			numReturnHeaderID=@numReturnHeaderID   
		 
		SELECT TOP 1 
			@numTaxItemID=numTaxItemID,
			@fltPercentage=fltPercentage,
			@tintTaxType=ISNULL(tintTaxType,1)
		FROM 
			OpportunityMasterTaxItems 
		WHERE 
			numReturnHeaderID=@numReturnHeaderID  
		ORDER BY 
			numTaxItemID 
 
		WHILE @numTaxItemID>-1    
		BEGIN
			IF ISNULL(@fltPercentage,0)>0
			BEGIN  
				DECLARE @numReturnItemID AS NUMERIC
				SET @numReturnItemID=0
				SET @ItemAmount=0	
					
				SELECT TOP 1 
					@numReturnItemID=numReturnItemID,
					@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) 
								* (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(monTotAmount,0) / numUnitHour) END)
				FROM 
					ReturnItems
				WHERE 
					numReturnHeaderID=@numReturnHeaderID   
				ORDER BY 
					numReturnItemID 
				     
				WHILE @numReturnItemID>0    
				BEGIN
					IF (SELECT COUNT(*) from OpportunityItemsTaxItems where numReturnItemID=@numReturnItemID and numTaxItemID=@numTaxItemID)>0
					BEGIN
						SET @monTotalTax= @monTotalTax + (CASE WHEN @tintTaxType = 2 THEN ISNULL(@fltPercentage,0) ELSE @fltPercentage * @ItemAmount / 100 END)
					END    
				    
					SELECT TOP 1 
						@numReturnItemID=numReturnItemID,
						@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(monTotAmount,0) / numUnitHour) END)
					FROM 
						ReturnItems 
					WHERE 
						numReturnHeaderID=@numReturnHeaderID  and numReturnItemID>@numReturnItemID   
					ORDER BY 
						numReturnItemID     
				    
					IF @@rowcount=0 SET @numReturnItemID=0    
				END 
			END
			
			SELECT TOP 1 
				@numTaxItemID=numTaxItemID,
				@fltPercentage=fltPercentage,
				@tintTaxType=ISNULL(tintTaxType,1) 
			FROM 
				OpportunityMasterTaxItems 
			WHERE 
				numReturnHeaderID = @numReturnHeaderID 
				AND numTaxItemID>@numTaxItemID  
			ORDER BY 
				numTaxItemID     
	    
			IF @@rowcount=0 SET @numTaxItemID=-1    
		END  
	 END
 
 
 INSERT INTO @result
 SELECT ISNULL(@monAmount,0),ISNULL(@monTotalTax,0),ISNULL(@monTotalDiscount,0)
 
 RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTotalQtyByUOM')
DROP FUNCTION GetTotalQtyByUOM
GO
CREATE FUNCTION [dbo].[GetTotalQtyByUOM]
(
	@numOppBizDocID NUMERIC(18,0)
)
RETURNS VARCHAR(3000)
AS
BEGIN
	DECLARE @vcTotalQtybyUOM AS VARCHAR(3000) = ''

	
	SELECT
		@vcTotalQtybyUOM = COALESCE(@vcTotalQtybyUOM,'') + (CASE WHEN LEN(@vcTotalQtybyUOM) > 0 THEN ', ' ELSE '' END) + CONCAT(TEMP.vcUOMName,' - ', SUM(numQty))
	FROM
		(
			SELECT 
				OI.numUOMId,
				dbo.fn_GetUOMName(OI.numUOMId) vcUOMName,
				CAST(ISNULL((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode, I.numDomainId, ISNULL(OI.numUOMId, 0))* SUM(OBI.numUnitHour)),0) AS NUMERIC(18, 2)) AS numQty
			FROM
				OpportunityBizDocItems OBI
			INNER JOIN
				OpportunityItems OI
			ON
				OBI.numOppItemID = OI.numOppItemtCode
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID = @numOppBizDocID
				AND ISNULL(OI.numUOMId,0) > 0
			GROUP BY
				I.numItemCode,
				I.numDomainID,
				I.numBaseUnit,
				OI.numUOMId
		) AS TEMP
		GROUP BY
			TEMP.numUOMId,
			TEMP.vcUOMName
		ORDER BY
			TEMP.vcUOMName


	RETURN ISNULL(@vcTotalQtybyUOM,'')
END
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/26/2008 18:13:14 ******/
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='SplitString')
DROP FUNCTION SplitString
GO
CREATE FUNCTION [dbo].[SplitString]
(
	@List TEXT,
	@Delimiter CHAR(1)
)
RETURNS @ReturnTbl TABLE 
(
	OutParam VARCHAR(500)
)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @LeftSplit VARCHAR(7998)
	DECLARE @SplitStart INT SET @SplitStart = 0
	DECLARE @SplitEnd INT
	SET @SplitEnd = 7997

	SELECT 
		@SplitEnd = MAX(Number)
	FROM dbo.Numbers
	WHERE 
		(
			REPLACE(SUBSTRING(@List, Number, 1), ' ', CHAR(255)) = 
				REPLACE(@Delimiter, ' ', CHAR(255))
			OR Number = DATALENGTH(@List) + 1
		)
		AND Number BETWEEN @SplitStart AND @SplitEnd

	WHILE @SplitStart < @SplitEnd
	BEGIN
		SET @LeftSplit = 
			@Delimiter + 
			SUBSTRING(@List, @SplitStart, @SplitEnd - @SplitStart) + 
			@Delimiter

		INSERT @ReturnTbl 
		(
			OutParam
		)
		SELECT 
			LTRIM
			(
				RTRIM
				(
					SUBSTRING
					(
						@LeftSplit, 
						Number + 1,
	                    CHARINDEX(@Delimiter, @LeftSplit, Number + 1) - Number - 1
					)
				)
			) AS Value
		FROM dbo.Numbers
		WHERE  
			Number <= LEN(@LeftSplit) - 1
			AND REPLACE(SUBSTRING(@LeftSplit, Number, 1), ' ', CHAR(255)) = 
				REPLACE(@Delimiter, ' ', CHAR(255))
			AND '' <>
				SUBSTRING
				(
					@LeftSplit, 
					Number + 1, 
					CHARINDEX(@Delimiter, @LeftSplit, Number + 1) - Number - 1
				)

		SET @SplitStart = @SplitEnd + 1
		SET @SplitEnd = @SplitEnd + 7997

		SELECT 
			@SplitEnd = MAX(Number) + @SplitStart
		FROM dbo.Numbers
		WHERE 
			(
				REPLACE(SUBSTRING(@List, Number + @SplitStart, 1), ' ', CHAR(255)) = 
					REPLACE(@Delimiter, ' ', CHAR(255))
				OR Number + @SplitStart = DATALENGTH(@List) + 1
			)
			AND Number BETWEEN 1 AND @SplitEnd - @SplitStart
	END

	RETURN
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPI_GetItemCustomFieldsValue' ) 
    DROP PROCEDURE USP_BizAPI_GetItemCustomFieldsValue
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 OCT 2015
-- Description:	Gets custom fields value for item
-- =============================================
CREATE PROCEDURE USP_BizAPI_GetItemCustomFieldsValue
	@numDomainID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		 CFW_Fld_Master.Fld_id AS Id,
		 CFW_Fld_Master.Fld_label AS Name,
		 CFW_FLD_Values_Item.Fld_Value AS ValueId,
		 dbo.GetCustFldValueItem(CFW_Fld_Master.Fld_id,Item.numItemCode) AS Value
	FROM 
		Item
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		Item.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		Item.numDomainID = @numDomainID
		AND Item.numItemCode = @numItemCode
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPI_GetSecretKey' ) 
    DROP PROCEDURE USP_BizAPI_GetSecretKey
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets domain user based on api key
-- =============================================
CREATE PROCEDURE USP_BizAPI_GetSecretKey
	@BizAPIPublicKey VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

                                                                
 SELECT TOP 1
        U.numUserID ,
        numUserDetailId ,
        ISNULL(vcEmailID, '') vcEmailID ,
        ISNULL(numGroupID, 0) numGroupID ,
        bitActivateFlag ,
        ISNULL(vcMailNickName, '') vcMailNickName ,
        ISNULL(txtSignature, '') txtSignature ,
        ISNULL(U.numDomainID, 0) numDomainID ,
        ISNULL(Div.numDivisionID, 0) numDivisionID ,
        ISNULL(vcCompanyName, '') vcCompanyName ,
        ISNULL(E.bitExchangeIntegration, 0) bitExchangeIntegration ,
        ISNULL(E.bitAccessExchange, 0) bitAccessExchange ,
        ISNULL(E.vcExchPath, '') vcExchPath ,
        ISNULL(E.vcExchDomain, '') vcExchDomain ,
        ISNULL(D.vcExchUserName, '') vcExchUserName ,
        ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS ContactName ,
        CASE WHEN E.bitAccessExchange = 0 THEN E.vcExchPassword
             WHEN E.bitAccessExchange = 1 THEN D.vcExchPassword
        END AS vcExchPassword ,
        tintCustomPagingRows ,
        vcDateFormat ,
        numDefCountry ,
        tintComposeWindow ,
        DATEADD(day, -sintStartDate, GETUTCDATE()) AS StartDate ,
        DATEADD(day, sintNoofDaysInterval,
                DATEADD(day, -sintStartDate, GETUTCDATE())) AS EndDate ,
        tintAssignToCriteria ,
        bitIntmedPage ,
        tintFiscalStartMonth ,
        numAdminID ,
        ISNULL(A.numTeam, 0) numTeam ,
        ISNULL(D.vcCurrency, '') AS vcCurrency ,
        ISNULL(D.numCurrencyID, 0) AS numCurrencyID ,
        ISNULL(D.bitMultiCurrency, 0) AS bitMultiCurrency ,
        bitTrial ,
        ISNULL(tintChrForComSearch, 0) AS tintChrForComSearch ,
        ISNULL(tintChrForItemSearch, 0) AS tintChrForItemSearch ,
        CASE WHEN bitSMTPServer = 1 THEN vcSMTPServer
             ELSE ''
        END AS vcSMTPServer ,
        CASE WHEN bitSMTPServer = 1 THEN ISNULL(numSMTPPort, 0)
             ELSE 0
        END AS numSMTPPort ,
        ISNULL(bitSMTPAuth, 0) bitSMTPAuth ,
        ISNULL([vcSmtpPassword], '') vcSmtpPassword ,
        ISNULL(bitSMTPSSL, 0) bitSMTPSSL ,
        ISNULL(bitSMTPServer, 0) bitSMTPServer ,
        ISNULL(IM.bitImap, 0) bitImapIntegration ,
        ISNULL(charUnitSystem, 'E') UnitSystem ,
        DATEDIFF(day, GETUTCDATE(), dtSubEndDate) AS NoOfDaysLeft ,
        ISNULL(bitCreateInvoice, 0) bitCreateInvoice ,
        ISNULL([numDefaultSalesBizDocId], 0) numDefaultSalesBizDocId ,
        ISNULL([numDefaultPurchaseBizDocId], 0) numDefaultPurchaseBizDocId ,
        vcDomainName ,
        S.numSubscriberID ,
        D.[tintBaseTax] ,
        ISNULL(D.[numShipCompany], 0) numShipCompany ,
        ISNULL(D.[bitEmbeddedCost], 0) bitEmbeddedCost ,
        ( SELECT    ISNULL(numAuthoritativeSales, 0)
          FROM      AuthoritativeBizDocs
          WHERE     numDomainId = D.numDomainID
        ) numAuthoritativeSales ,
        ( SELECT    ISNULL(numAuthoritativePurchase, 0)
          FROM      AuthoritativeBizDocs
          WHERE     numDomainId = D.numDomainID
        ) numAuthoritativePurchase ,
        ISNULL(D.[numDefaultSalesOppBizDocId], 0) numDefaultSalesOppBizDocId ,
        ISNULL(tintDecimalPoints, 2) tintDecimalPoints ,
        ISNULL(D.tintBillToForPO, 0) tintBillToForPO ,
        ISNULL(D.tintShipToForPO, 0) tintShipToForPO ,
        ISNULL(D.tintOrganizationSearchCriteria, 0) tintOrganizationSearchCriteria ,
        ISNULL(D.tintLogin, 0) tintLogin ,
        ISNULL(bitDocumentRepositary, 0) bitDocumentRepositary ,
        ISNULL(D.tintComAppliesTo, 0) tintComAppliesTo ,
        ISNULL(D.vcPortalName, '') vcPortalName ,
        ( SELECT    COUNT(*)
          FROM      dbo.Subscribers
          WHERE     GETUTCDATE() BETWEEN dtSubStartDate
                                 AND     dtSubEndDate
                    AND bitActive = 1
        ) ,
        ISNULL(D.bitGtoBContact, 0) bitGtoBContact ,
        ISNULL(D.bitBtoGContact, 0) bitBtoGContact ,
        ISNULL(D.bitGtoBCalendar, 0) bitGtoBCalendar ,
        ISNULL(D.bitBtoGCalendar, 0) bitBtoGCalendar ,
        ISNULL(bitExpenseNonInventoryItem, 0) bitExpenseNonInventoryItem ,
        ISNULL(D.bitInlineEdit, 0) bitInlineEdit ,
        ISNULL(U.numDefaultClass, 0) numDefaultClass ,
        ISNULL(D.bitRemoveVendorPOValidation, 0) bitRemoveVendorPOValidation ,
        ISNULL(D.bitTrakDirtyForm, 1) bitTrakDirtyForm ,
        ISNULL(D.tintBaseTaxOnArea, 0) tintBaseTaxOnArea ,
        ISNULL(D.bitAmountPastDue, 0) bitAmountPastDue ,
        CONVERT(DECIMAL(18, 2), ISNULL(D.monAmountPastDue, 0)) monAmountPastDue ,
        ISNULL(D.bitSearchOrderCustomerHistory, 0) bitSearchOrderCustomerHistory ,
        ISNULL(U.tintTabEvent, 0) AS tintTabEvent ,
        ISNULL(D.bitRentalItem, 0) AS bitRentalItem ,
        ISNULL(D.numRentalItemClass, 0) AS numRentalItemClass ,
        ISNULL(D.numRentalHourlyUOM, 0) AS numRentalHourlyUOM ,
        ISNULL(D.numRentalDailyUOM, 0) AS numRentalDailyUOM ,
        ISNULL(D.tintRentalPriceBasedOn, 0) AS tintRentalPriceBasedOn ,
        ISNULL(D.tintCalendarTimeFormat, 24) AS tintCalendarTimeFormat ,
        ISNULL(D.tintDefaultClassType, 0) AS tintDefaultClassType ,
        ISNULL(U.numDefaultWarehouse, 0) numDefaultWarehouse ,
        ISNULL(D.tintPriceBookDiscount, 0) tintPriceBookDiscount ,
        ISNULL(D.bitAutoSerialNoAssign, 0) bitAutoSerialNoAssign ,
        ISNULL(IsEnableProjectTracking, 0) AS [IsEnableProjectTracking] ,
        ISNULL(IsEnableCampaignTracking, 0) AS [IsEnableCampaignTracking] ,
        ISNULL(IsEnableResourceScheduling, 0) AS [IsEnableResourceScheduling] ,
        ISNULL(D.numSOBizDocStatus, 0) AS numSOBizDocStatus ,
        ISNULL(D.bitAutolinkUnappliedPayment, 0) AS bitAutolinkUnappliedPayment ,
        ISNULL(numShippingServiceItemID, 0) AS [numShippingServiceItemID] ,
        ISNULL(D.numDiscountServiceItemID, 0) AS numDiscountServiceItemID ,
        ISNULL(D.tintCommissionType, 1) AS tintCommissionType ,
        ISNULL(D.bitDiscountOnUnitPrice, 0) AS bitDiscountOnUnitPrice ,
        ISNULL(D.intShippingImageWidth, 0) AS intShippingImageWidth ,
        ISNULL(D.intShippingImageHeight, 0) AS intShippingImageHeight ,
        ISNULL(D.numTotalInsuredValue, 0) AS numTotalInsuredValue ,
        ISNULL(D.numTotalCustomsValue, 0) AS numTotalCustomsValue ,
        ISNULL(D.bitUseBizdocAmount, 1) AS [bitUseBizdocAmount] ,
        ISNULL(D.bitDefaultRateType, 1) AS [bitDefaultRateType] ,
        ISNULL(D.bitIncludeTaxAndShippingInCommission, 1) AS [bitIncludeTaxAndShippingInCommission],
        U.vcBizAPISecretKey
 FROM   UserMaster U
        LEFT JOIN ExchangeUserDetails E ON E.numUserID = U.numUserID
        LEFT JOIN ImapUserDetails IM ON IM.numUserCntID = U.numUserDetailID
        JOIN Domain D ON D.numDomainID = U.numDomainID
        LEFT JOIN DivisionMaster Div ON D.numDivisionID = Div.numDivisionID
        LEFT JOIN CompanyInfo C ON C.numCompanyID = Div.numDivisionID
        LEFT JOIN AdditionalContactsInformation A ON A.numContactID = U.numUserDetailId
        JOIN Subscribers S ON S.numTargetDomainID = D.numDomainID
 WHERE  vcBizAPIPublicKey = @BizAPIPublicKey COLLATE Latin1_General_CS_AS
        AND bitActive IN ( 1, 2 )
        AND U.bitBizAPIAccessEnabled = 1
        AND GETUTCDATE() BETWEEN dtSubStartDate
                         AND     dtSubEndDate
 
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	FROM 
		OpportunityItems OI 
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	WHERE 
		OI.numOppId=@numNewOppID AND 
		OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numNewOppID)
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Insert Tax for Division   

IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId, numTaxItemID,	fltPercentage,tintTaxType
	) 
	SELECT 
		@numNewOppID,TI.numTaxItemID,TEMPTax.decTaxValue,TEMPTax.tintTaxType
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL) 
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId AND 
		DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,0,TEMPTax.decTaxValue,TEMPTax.tintTaxType
	FROM 
		dbo.DivisionMaster 
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)  
	) AS TEMPTax
	WHERE 
		bitNoTax=0 AND 
		numDivisionID=@numDivisionID
END


--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9) 
DECLARE @bitIsPrimary BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour INT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0))
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	CROSS APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour INT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS INT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour INT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0))
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour INT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS INT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              

DECLARE @listIds VARCHAR(MAX)
SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcEmailID=@UserName and vcPassword=@vcPassword

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
/****** Object:  StoredProcedure [dbo].[usp_CompanyDivision]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companydivision')
DROP PROCEDURE usp_companydivision
GO
CREATE PROCEDURE [dbo].[usp_CompanyDivision]                                         
 @numDivisionID  numeric=0,                                           
 @numCompanyID  numeric=0,                                           
 @vcDivisionName  varchar (100)='',                                                                
 @numGrpId   numeric=0,                                                                       
 @numTerID   numeric=0,                                          
 @bitPublicFlag  bit=0,                                          
 @tintCRMType  tinyint=0,                                          
 @numUserCntID  numeric=0,                                                                                                                                     
 @numDomainID  numeric=0,                                          
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                          
 @numStatusID  numeric=0,                                        
 @numCampaignID numeric=0,                            
 @numFollowUpStatus numeric(9)=0,                                                           
 @tintBillingTerms as tinyint,                                          
 @numBillingDays as numeric(9),                                         
 @tintInterestType as tinyint,                                          
 @fltInterest as float,                          
 @vcComPhone as varchar(50),                
 @vcComFax as varchar(50),        
 @numAssignedTo as numeric(9)=0,
 @bitNoTax as BIT,
 @bitSelectiveUpdate BIT=0,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@bitActiveInActive as bit=1,
@numCurrencyID AS numeric(9)=0,
@vcShipperAccountNo	VARCHAR(100),
@intShippingCompany INT = 0,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0,
@numAccountClass NUMERIC(18,0) = 0        
AS                                                                       
BEGIN                                          
                                         
                                         
 IF @numDivisionId is null OR @numDivisionId=0                                          
 BEGIN                                                                       
  INSERT INTO DivisionMaster                      
(numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,                      
    bitPublicFlag,numCreatedBy,bintCreatedDate,                      
     numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                      
     bitLeadBoxFlg,numTerID,numStatusID,bintLeadProm,bintLeadPromBy,bintProsProm,bintProsPromBy,                      
     numRecOwner,tintBillingTerms,                      
     numBillingDays,tintInterestType,fltInterest,vcComPhone,vcComFax,numCampaignID,bitNoTax,numAssignedTo,numAssignedBy,numCompanyDiff,vcCompanyDiff,numCurrencyID,vcShippersAccountNo,intShippingCompany,numDefaultExpenseAccountID,numAccountClassID)                     
   VALUES(@numCompanyID, @vcDivisionName, @numGrpId, 0,                                                                         
   @bitPublicFlag, @numUserCntID, getutcdate(), @numUserCntID,getutcdate(), @tintCRMType, @numDomainID, @bitLeadBoxFlg, @numTerID, @numStatusID,                                   
   NULL, NULL, NULL, NULL, @numUserCntID,0,0,0,0,@vcComPhone,@vcComFax,@numCampaignID,@bitNoTax,@numAssignedTo,@numUserCntID,@numCompanyDiff,@vcCompanyDiff,@numCurrencyID,@vcShipperAccountNo,@intShippingCompany,@numDefaultExpenseAccountID,ISNULL(@numAccountClass,0))                                       
                                    
  --Return the ID auto generated by the above INSERT.                                          
  SELECT @numDivisionId = @@IDENTITY
	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0          
                                    
   insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                  
     values(@numCompanyID,@numDivisionId,@numGroupID,@numDomainID)                                  
                              
                                         
                                       
       SELECT @numDivisionId                                    
                                     
 END                                          
 ELSE                                         
 BEGIN                                          


	IF @bitSelectiveUpdate = 0 
	BEGIN
			declare @numFollow as varchar(10)                          
			declare @binAdded as varchar(20)                          
			declare @PreFollow as varchar(10)                          
			declare @RowCount as varchar(2)                          
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount                          
			select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                          
			begin                          
			select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc                          
			                    
			if @PreFollow<>0                          
			begin                          
			                   
			if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
			begin                          
			                      
				  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
				end                          
			end                          
			else                          
			begin                          
			                     
			insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
			end                          
			                         
			end                          
			              
			                                      
			UPDATE DivisionMaster SET  numCompanyID = @numCompanyID ,vcDivisionName = @vcDivisionName,                                      
			numGrpId = @numGrpId,                                                           
			numFollowUpStatus =@numFollowUpStatus,                                                                 
			numTerID = @numTerID,                                       
			bitPublicFlag =@bitPublicFlag,                                            
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = getutcdate(),                                         
			tintCRMType = @tintCRMType,                                          
			numStatusID = @numStatusID,                                                                            
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			vcComPhone=@vcComPhone,                
			vcComFax=@vcComFax,            
			numCampaignID=@numCampaignID,
			bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,bitActiveInActive=@bitActiveInActive,numCurrencyID=@numCurrencyID,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID                                                  
			WHERE numDivisionID = @numDivisionID   and numDomainId= @numDomainID     
			    
			    
			---Updating if organization is assigned to someone            
			declare @tempAssignedTo as numeric(9)          
			set @tempAssignedTo=null           
			select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			print @tempAssignedTo          
			if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')          
			begin            
			update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
			end           
			else if  (@numAssignedTo =0)          
			begin          
			update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
			end                                            
			                      
			                                 
			SELECT @numDivisionID
 
 
 
		END                     
	END
	
	IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update DivisionMaster Set '

		IF(LEN(@vcComPhone)>0)
		BEGIN
			SET @sql =@sql + ' vcComPhone=''' + @vcComPhone + ''', '
		END
		IF(LEN(@vcComFax)>0)
		BEGIN
			SET @sql =@sql + ' vcComFax=''' + @vcComFax + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numDivisionID= '+CONVERT(VARCHAR(10),@numDivisionID)

		PRINT @sql
		EXEC(@sql)
	END
	
	

end
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0 AND vcDbColumnName <> 'numCompanyID'
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

--SELECT @strSQL
EXEC (@strSQL)

END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_SearchWithClass' ) 
    DROP PROCEDURE USP_CompanyInfo_SearchWithClass
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 26 October 2015
-- Description:	Gets company info based on class
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_SearchWithClass
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX),
	@numAccountClass NUMERIC(18,0) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @tintDefaultClassType AS TINYINT 
SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain	WHERE numDomainID = @numDomainID


SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0 AND vcDbColumnName <> 'numCompanyID'
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + 
	' AND 1 = (CASE WHEN ' + CAST(ISNULL(@tintDefaultClassType,0) AS VARCHAR(20)) + ' = 2 THEN (CASE WHEN ISNULL(d.numAccountClassID,0) = ' + CAST(@numAccountClass AS VARCHAR(20)) + ' THEN 1 ELSE 0 END) ELSE 1 END) '  + 
	' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) 
	+ ' AND 1 = (CASE WHEN ' + CAST(ISNULL(@tintDefaultClassType,0) AS VARCHAR(20)) + ' = 2 THEN (CASE WHEN ISNULL(d.numAccountClassID,0) = ' + CAST(@numAccountClass AS VARCHAR(20)) + ' THEN 1 ELSE 0 END) ELSE 1 END) '  + 
	' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) 
	+ ' AND 1 = (CASE WHEN ' + CAST(ISNULL(@tintDefaultClassType,0) AS VARCHAR(20)) + ' = 2 THEN (CASE WHEN ISNULL(d.numAccountClassID,0) = ' + CAST(@numAccountClass AS VARCHAR(20)) + ' THEN 1 ELSE 0 END) ELSE 1 END) '  + 
	+ ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

--SELECT @strSQL
EXEC (@strSQL)

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId >0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			
			IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
			BEGIN
				INSERT INTO OpportunityBizDocs
					   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
						[bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID])
				SELECT @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
						@bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId
				FROM OpportunityBizDocs OBD WHERE numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId     
			END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID])
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
		BEGIN
			IF DATALENGTH(@strBizDocItems)>2
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END
		END
		ELSE
		BEGIN
			IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
			BEGIN
				DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

				SELECT TOP 1
					@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
				FROM 
					OpportunityBizDocs 
				WHERE 
					numOppId=@numOppId 
					AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
					AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
				ORDER BY
					numOppBizDocsId

				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				INNER JOIN
					(
						SELECT 
							OpportunityBizDocItems.numOppItemID,
							OpportunityBizDocItems.numUnitHour
						FROM 
							OpportunityBizDocs 
						JOIN 
							OpportunityBizDocItems 
						ON 
							OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
						WHERE 
							numOppId=@numOppId 
							AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
					) TempBizDoc
				ON
					TempBizDoc.numOppItemID = OI.numoppitemtCode
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


				UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
			END
			ELSE
			BEGIN
		   		INSERT INTO OpportunityBizDocItems                                                                          
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc 
						WHEN 1 
						THEN 1 
						ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
					END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				OUTER APPLY
					(
						SELECT 
							SUM(OBDI.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs OBD 
						JOIN 
							dbo.OpportunityBizDocItems OBDI 
						ON 
							OBDI.numOppBizDocId  = OBD.numOppBizDocsId
							AND OBDI.numOppItemID = OI.numoppitemtCode
						WHERE 
							numOppId=@numOppId 
							AND 1 = (CASE 
										WHEN @bitAuthBizdoc = 1 
										THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
										WHEN @numBizDocId = 304 --Deferred Income
										THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
									END)
					) TempBizDoc
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
			END
		END

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = Getdate(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity numeric(18,10),
			Notes varchar(500),
			monPrice MONEY,
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity numeric(18,10),
			Notes varchar(500),
			monPrice MONEY
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost money,
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
	BEGIN
		RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as money;Set @monCreditAmount=0
   Declare @monCreditBalance as money;Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0	SELECT 		@isOrderItemsAvailable = COUNT(*) 	FROM    		OpportunityItems OI	JOIN 		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId	JOIN 		Item I ON OI.numItemCode = I.numItemCode	WHERE   		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1									 ELSE 0 END 								ELSE 0 END)) AND OI.numOppId = @numOppId							   AND ( bitDropShip = 0									 OR bitDropShip IS NULL								   )   if (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1                 begin        exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                

	IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
		UPDATE WHIDL
			SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
		FROM 
			WareHouseItmsDTL WHIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		INNER JOIN
			WareHouseItems
		ON
			WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			OWSI.numOppID = @numOppId
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		IF (SELECT
				COUNT(*)
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WHIDL
			ON
				OWSI.numWarehouseItmsDTLID = WHIDL.numWareHouseItmsDTLID 
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID 
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
				AND 1 = (CASE 
							WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
							WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
							ELSE 0 
						END)
			) > 0
		BEGIN
			RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
		END
		ELSE
		BEGIN
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
		END
	END
									        
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
	delete from OpportunityMaster where numOppId=@numOppId and numDomainID= @numDomainID             
  ELSE
	RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_FeaturedItem' ) 
    DROP PROCEDURE USP_FeaturedItem
GO
CREATE PROCEDURE USP_FeaturedItem
    @numNewDomainID NUMERIC(9) ,
    @numReportId NUMERIC(9) ,
    @numSiteId NUMERIC(9)
AS 
    BEGIN

        DECLARE @strsql VARCHAR(5000) 
        DECLARE @DefaultWarehouseID NUMERIC(9)
       
        SELECT  @DefaultWarehouseID = numDefaultWareHouseID
        FROM    dbo.eCommerceDTL
        WHERE   numDomainId = @numNewDomainID
        SET @strsql = 'DECLARE @numDomainId NUMERIC;
       SET @numDomainid =' + CONVERT(VARCHAR, @numNewDomainID)
            + ';
     select X.##Items## from ( '
        DECLARE @query AS VARCHAR(2000)
        SELECT  @query = textQuery
        FROM    dbo.CustomReport
        WHERE   dbo.CustomReport.numCustomReportID = @numReportId 
        SET @strsql = @strsql + ' ' + @query + ') X'
        PRINT @strSql

 
        CREATE TABLE #temptable ( ItemCode NUMERIC(9) ) 
        INSERT  INTO #temptable
                EXEC ( @strsql)
SELECT X.*, ( SELECT TOP 1
                            vcCategoryName
                  FROM      dbo.Category
                  WHERE     numCategoryID = X.numCategoryID
                ) AS vcCategoryName
                
  FROM (        
  SELECT  I.numItemCode ,
                I.vcItemName ,
                I.txtItemDesc ,
                ( SELECT TOP 1
                            numCategoryID
                  FROM      ItemCategory
                  WHERE     numItemID = I.numItemCode
                ) AS numCategoryID ,
                CASE WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.bitDefault = 1
                                            AND II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.bitDefault = 1
                                    AND II.numItemCode = I.numItemCode
                          )
                     WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.numItemCode = I.numItemCode
                          )
                     ELSE ''
                END AS vcPathForTImage ,
                ISNULL(CASE WHEN I.[charItemType] = 'P'
                            THEN CASE WHEN I.bitSerialized = 1
                                      THEN 1.00000 * monListPrice
                                      ELSE 1.00000
                                           * ISNULL(( SELECT TOP 1
                                                              W.[monWListPrice]
                                                      FROM    WarehouseItems W
                                                      WHERE   W.numDomainID = @numNewDomainID
															  AND W.numItemID = I.numItemCode
                                                              AND W.numWarehouseID = @DefaultWarehouseID
                                                    ), NULL)
                                 END
                            ELSE 1.00000 * monListPrice
                       END, 0) AS monListPrice ,
                dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0), I.numItemCode,
                                     I.numDomainID, ISNULL(I.numBaseUnit, 0)) AS UOMConversionFactor ,
                I.vcSKU ,
                I.vcManufacturer 
               
        FROM    Item AS I
        WHERE   I.numItemCode IN ( SELECT   ItemCode
                                   FROM     #tempTable )
                AND I.numDomainId = @numNewDomainID
                
) AS X
    DROP TABLE #tempTable
    END
--EXEC USP_FeaturedItem @numNewDomainID= 1 , @numReportId = 0 ,@numSiteId = 18
--exec USP_FeaturedItem @numNewDomainID=1,@numReportId=6774,@numSiteId=106

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountClass')
DROP PROCEDURE dbo.USP_GetAccountClass
GO
CREATE PROCEDURE [dbo].[USP_GetAccountClass]
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
    @numDivisionID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @numAccountClass AS NUMERIC(18,0) = 0
	DECLARE @tintDefaultClassType AS INT = 0
	SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType = 1 --USER
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numUserCntID
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivisionID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SELECT @numAccountClass
END
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN                                                                

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	DECLARE @PLCHARTAccountCode AS VARCHAR(100) = ''
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS MONEY
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL

	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ #tempDirectReport.vcAccountCode +''#%'' THEN Period.ProfitLossAmount ELSE 0 END) +
							ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
							ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#tempYearMonth
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		
		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ #tempDirectReport.vcAccountCode +''#%'' THEN Period.ProfitLossAmount ELSE 0 END) +
							ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
							ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#TempYearMonthQuarter
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS MONEY = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT 
			#tempDirectReport.ParentId,
			#tempDirectReport.numAccountId,
			#tempDirectReport.numAccountTypeID,
			#tempDirectReport.vcAccountType,
			#tempDirectReport.LEVEL,
			#tempDirectReport.vcAccountCode,
			('#' + #tempDirectReport.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE WHEN '#' + @PLCHARTAccountCode + '#' LIKE '%#'+ #tempDirectReport.vcAccountCode +'#%' THEN @ProifitLossAmount ELSE 0 END) +
			ISNULL((SELECT ISNULL(sum(Debit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) -
			ISNULL((SELECT ISNULL(sum(Credit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) AS Amount
		FROM 
			#tempDirectReport
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #View_Journal SELECT numAccountId,vcAccountCode,COAvcAccountCode, datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(-1 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), COA.
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(CONCAT(d.Struc ,'#',[COA].[numAccountId]) AS VARCHAR(300)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND COA.bitActive = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc 
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	INSERT INTO #tempDirectReport
	SELECT 0,-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
	UNION ALL
	SELECT 0,-2,'Other Income and Expenses',0,NULL,NULL,'-2'
	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc 
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			DATENAME(mm, @dtFromDate) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = ' FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2
				UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, DM.numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                  
  DM.numRecOwner,    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID],
 ISNULL(numDefaultShippingServiceID,0) AS [numDefaultShippingServiceID],
 ISNULL(numAccountClassID,0) As numAccountClassID
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
 (select vcdata from listdetails where numListItemID = numFollowUpStatus) as numFollowUpStatusName,
 numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 tintBillingTerms,              
 numBillingDays,              
 tintInterestType,              
 fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
 numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(vcComPhone,'') as vcComPhone, 
 vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 numCampaignID,              
 numAssignedBy, isnull(bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod                                                         
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END
GO




/****** Object:  StoredProcedure [dbo].[USP_GetDepositDetails]    Script Date: 07/26/2008 16:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDepositDetailsWithClass' ) 
    DROP PROCEDURE USP_GetDepositDetailsWithClass
GO
CREATE PROCEDURE [dbo].[USP_GetDepositDetailsWithClass]
    @numDepositId AS NUMERIC(9) = 0 ,
    @numDomainId AS NUMERIC(9) = 0 ,
    @tintMode AS TINYINT = 0, -- 1= Get Undeposited Payments 
    @numCurrencyID numeric(9) =0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    BEGIN  
      				
	    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
      
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
			  
        IF @tintMode = 0 
            BEGIN 
    	
    
                SELECT  numDepositId ,
                        dbo.fn_GetComapnyName(numDivisionID) vcCompanyName ,
                        numDivisionID ,
                        numChartAcntId ,
                        dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
                        dtDepositDate AS [dtOrigDepositDate],
                        monDepositAmount ,
                        numPaymentMethod ,
                        vcReference ,
                        vcMemo ,
                        tintDepositeToType,tintDepositePage,bitDepositedToAcnt
                        ,DM.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount
						,ISNULL(DM.numAccountClass,0) AS numAccountClass
                FROM    dbo.DepositMaster DM
						LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
                WHERE   numDepositId = @numDepositId
                        AND DM.numDomainId = @numDomainId
        
                SELECT  DD.numDepositeDetailID ,
                        DD.numDepositID ,
                        DD.numOppBizDocsID ,
                        DD.numOppID ,
                        DD.monAmountPaid ,
                        DD.numChildDepositID ,
                        DD.numPaymentMethod ,
                        DD.vcMemo ,
                        DD.vcReference ,
                        DD.numClassID ,
                        DD.numProjectID ,
                        DD.numReceivedFrom ,
                        DD.numAccountID,
                        GJD.numTransactionId AS numTransactionID,
                        GJD1.numTransactionId AS numTransactionIDHeader
                        ,ISNULL(DM.numCurrencyID,0) numCurrencyID
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						
                FROM    dbo.DepositeDetails DD
                LEFT JOIN dbo.DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
                LEFT JOIN dbo.General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7 
                LEFT JOIN dbo.General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6 
                WHERE   DD.numDepositID = @numDepositId
            END        
        ELSE 
            IF @tintMode = 1 
                BEGIN
                
                --Get Undeposited payments
                SELECT * INTO #tempDeposits FROM 
				(
				--Get Saved deposit entries for edit mode
				SELECT  Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							DD.numDepositeDetailID ,
							DM.numDepositId ,
							DM.numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid AS monDepositAmount,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numReceivedFrom AS numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							 (SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
							LEFT JOIN dbo.TransactionHistory TH ON TH.numTransHistoryID= DM.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DD.numDepositID = @numDepositId AND ISNULL(DM.numAccountClass,0)=@numAccountClass
					UNION
					SELECT Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							0 numDepositeDetailID ,
							numDepositId ,
							numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							monDepositAmount ,
							numPaymentMethod ,
							vcReference ,
							vcMemo ,
							DM.numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							(SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositMaster DM
							INNER JOIN dbo.Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
							LEFT JOIN dbo.TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DM.numDomainId = @numDomainID
							AND tintDepositeToType = 2
							AND ISNULL(bitDepositedToAcnt,0) = 0 
							AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
							AND ISNULL(DM.numAccountClass,0)=@numAccountClass
			  )  [Deposits]
			
			SELECT  @TotRecs = COUNT(*) FROM #tempDeposits
						
			SELECT  * 
			FROM    #tempDeposits
			WHERE   row > @firstRec
					AND row < @lastRec ;
            
            DROP TABLE #tempDeposits        
					
			-- get new deposite Entries
					SELECT  DD.numDepositeDetailID ,
							DM.numDepositId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid ,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numAccountID,
--							 COA.vcAccountCode numAcntType,
							DD.numClassID,
							DD.numProjectID,
							DD.numReceivedFrom,
							dbo.fn_GetComapnyName(DD.numReceivedFrom) ReceivedFrom,
							bitDepositedToAcnt
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numDepositID = DM.numDepositId
--							LEFT JOIN dbo.Chart_Of_Accounts COA ON DD.numAccountID = COA.numAccountId
					WHERE   DD.numDepositID = @numDepositId AND ISNULL(numChildDepositID,0) = 0
					AND ISNULL(DM.numAccountClass,0)=@numAccountClass
					
						
                END
    END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailMergeData' ) 
    DROP PROCEDURE USP_GetEmailMergeData 
GO
CREATE PROCEDURE USP_GetEmailMergeData
    @numModuleID NUMERIC,
    @vcRecordIDs VARCHAR(8000) = '', -- Seperated by comma
    @numDomainID NUMERIC,
    @tintMode TINYINT = 0,
    @ClientTimeZoneOffset Int  
AS 
BEGIN
	
    IF @numModuleID = 1 
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.vcProfile,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                                 '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                                       '') AS OrgShippingAddress ,								   
                  
				  (select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature]
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					-- LEFT JOIN UserMaster U on ACI.numRecOwner=U.numUserDetailId
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
    IF @numModuleID = 2 
        BEGIN
            SELECT  vcPOppName OpportunityName,
                    dbo.fn_getContactName(numAssignedTo) OpportunityAssigneeName
                    --,ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECTintShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=OM.numOppID FOR XML PATH('')),4,200000)),'') AS  TrackingNo
            FROM    dbo.OpportunityMaster OM
            WHERE   OM.numDomainID = @numDomainID
                    AND numOppId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
    IF @numModuleID = 4 
        BEGIN        
            SELECT  vcProjectID ProjectID,
                    vcProjectName ProjectName,
                    dbo.fn_getContactName(numAssignedTo) ProjectAssigneeName,
                    dbo.FormatedDateFromDate(intDueDate, numDomainId) ProjectDueDate
            FROM    dbo.ProjectsMaster
            WHERE   numDomainID = @numDomainID
                    AND numProID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 5 
        BEGIN        
            SELECT  vcCaseNumber CaseNo,
                    textSubject AS CaseSubject,
                    dbo.fn_getContactName(numAssignedTo) CaseAssigneeName,
                    dbo.FormatedDateFromDate(intTargetResolveDate, numDomainID) CaseResoveDate
            FROM    dbo.Cases
            WHERE   numDomainID = @numDomainID
                    AND numCaseId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 6 
        BEGIN        
            SELECT  textDetails TicklerComment,
                    dbo.GetListIemName(bitTask) TicklerType,
                    dbo.fn_getContactName(numAssign) TicklerAssigneeName,
                    dbo.FormatedDateFromDate(dtStartTime, numDomainID) TicklerDueDate,
                    dbo.GetListIemName(numStatus) TicklerPriority,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ), numDomainID) TicklerStartTime,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ), numDomainID) TicklerEndTime,
                    dbo.GetListIemName(bitTask) + ' - ' + dbo.GetListIemName(numStatus) AS TicklerTitle,
                    dbo.fn_getContactName(numCreatedBy) TicklerCreatedBy 
                   
                    
            FROM    dbo.Communication
            WHERE   numDomainID = @numDomainID
                    AND numCommId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 8 
        BEGIN
            SELECT  dbo.GetListIemName(OB.numBizDocId) BizDoc,
                    OB.vcBizDocID BizDocID,
                    OB.monDealAmount BizDocAmount,
                    OB.monAmountPaid BizDocAmountPaid,
                    ( OB.monDealAmount - OB.monAmountPaid ) BizDocBalanceDue,
                    dbo.GetListIemName(ISNULL(OB.numBizDocStatus, 0)) AS BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)  AS BizDocBillingTermsNetDays,
                    dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+'
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 	
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 	
                                THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms       		
                                    ,
--                          DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 
--											 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) 
--											 ELSE 0 
--										END,OB.dtCreatedDate) AS BizDocDueDate,
						  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 
											 THEN ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)
											 ELSE 0 
										END,OB.dtCreatedDate) AS BizDocDueDate,										
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,OB.numOppBizDocsID,
					isnull(OB.vcTrackingNo,'') AS  vcTrackingNo,
					ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OB.numShipVia  AND vcFieldName='Tracking URL')),'') AS vcTrackingURL,
					OM.vcPOppName OpportunityName,
                    dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName
            FROM    dbo.OpportunityBizDocs OB
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = OB.numOppID
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
            WHERE   OM.numDomainID = @numDomainID
                    AND OB.numOppBizDocsID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 	
        IF @numModuleID = 9 
        BEGIN
            SELECT  
                   ISNULL(TempBizDoc.vcBizDocID,'') BizDocID,
                   ISNULL(TempBizDoc.monDealAmount,0) BizDocAmount,
                   ISNULL(TempBizDoc.monAmountPaid,0) BizDocAmountPaid,
                   ISNULL(TempBizDoc.BizDocBalanceDue,0) BizDocBalanceDue,
				   ISNULL(TempBizDoc.BizDocStatus,'') BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0) AS BizDocBillingTermsNetDays,
                   -- dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+'
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1  		
                                THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms       		
                                    ,
                        --  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) ELSE 0 END,OB.dtCreatedDate) AS BizDocDueDate,
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail
            FROM    dbo.OpportunityMaster OM 
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
					OUTER APPLY
					(
						SELECT TOP 1
							vcBizDocID,
							monDealAmount,
							monAmountPaid,
							(monDealAmount - monAmountPaid) AS BizDocBalanceDue,
							dbo.GetListIemName(ISNULL(numBizDocStatus, 0)) AS BizDocStatus
						FROM
							OpportunityBizDocs 
						WHERE 
							numOppId=OM.numOppID ANd ISNULL(bitAuthoritativeBizDocs,0) = 1
					) AS TempBizDoc
            WHERE   OM.numDomainID = @numDomainID
                    AND OM.numOppID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 
        
        IF @numModuleID = 11 
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.vcProfile,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                                 '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                                       '') AS OrgShippingAddress 
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=1 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
        	
--select * from dbo.OpportunityBizDocs	
--    SELECT TOP 1 bitInterestType,
--            *
--    FROM    dbo.OpportunityBizDocs ORDER BY numoppbizdocsid DESC 
END
--exec USP_GetEmailMergeData @numModuleID=6,@vcRecordIDs='18463',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
--exec USP_GetEmailMergeData @numModuleID=1,@vcRecordIDs='1',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0,
	  @numAccountClass NUMERIC(18,0) = 0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN

--IF @IsReport = 1
--BEGIN
--	DECLARE @vcAccountIDs AS VARCHAR(MAX)
--	SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
--	FROM [dbo].[Chart_Of_Accounts] AS COA 
--	WHERE [COA].[numDomainId] = @numDomainID
--	PRINT @vcAccountIDs

--	SET @vcAccountId= @vcAccountIDs
--END    

DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT @Month
PRINT @year
		 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC, dbo.General_Journal_Details.numTransactionId asc 
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS money
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                monPrice,
				CONVERT(DECIMAL(18, 4),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') vcBaseUOM,
				ISNULL(dbo.fn_GetUOMName(numUOMId),'') vcUOM,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,isnull(i.monAverageCost,'0') as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount],
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)

AS                                                          
BEGIN 

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END
 

;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
AS
(
  -- anchor
  SELECT CAST(-1 AS NUMERIC(18)) AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   1 AS LEVEL, cast(cast([ATD].[vcAccountCode] AS varchar) AS varchar (300))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD
  WHERE [ATD].[numDomainID]=@numDomainId AND  [ATD].[vcAccountCode] IN('0103','0104','0106')
  UNION ALL
  -- recursive
  SELECT ATD.[numParentID] AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   LEVEL + 1, cast(d.Struc + '#' + cast([ATD].[vcAccountCode] AS varchar) AS varchar(300))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD JOIN [DirectReport] D ON D.[numAccountTypeID] = [ATD].[numParentID]
  WHERE [ATD].[numDomainID]=@numDomainId 
  ),
  DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
AS
(
  -- anchor
  SELECT ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL ,CAST(NULL AS NUMERIC(18)),Struc
  FROM DirectReport 
  UNION ALL
  -- recursive
  SELECT COA.numParntAcntTypeId AS ParentId,CAST(NULL AS NUMERIC(18)), COA.[vcAccountName],COA.[vcAccountCode],
   LEVEL + 1,[COA].[numAccountId], cast(d.Struc + '#' + cast([COA].[numAccountId] AS varchar) AS varchar(300))  AS Struc
  FROM [dbo].[Chart_of_Accounts] AS COA JOIN [DirectReport1] D ON D.[numAccountTypeID] = COA.[numParntAcntTypeId]
  WHERE [COA].[numDomainID]=@numDomainId AND COA.bitActive = 1
  )

  
SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode,numAccountId,Struc INTO #tempDirectReport
FROM DirectReport1 

INSERT INTO #tempDirectReport
SELECT 0,-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
UNION ALL
SELECT 0,-2,'Other Income and Expenses',0,NULL,NULL,'-2'

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId, COA.Struc,
CASE WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
     ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) END AS Amount,V.datEntry_Date
INTO #tempViewData
FROM #tempDirectReport COA JOIN View_Journal V ON  /*V.[numAccountId] = COA.[numAccountId]*/ V.COAvcAccountCode like COA.vcAccountCode + '%' 
AND V.numDomainID=@numDomainId AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
AND (V.numAccountClass=@numAccountClass OR @numAccountClass=0) 
WHERE COA.[numAccountId] IS NOT NULL

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
		MONTH(DATEADD(d,1,new_date)) AS 'mm',
		DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE DATEADD(d,1,new_date) < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = ' FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				ORDER BY Struc'
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemUOMConversion')
DROP PROCEDURE USP_GetItemUOMConversion
GO
Create PROCEDURE [dbo].[USP_GetItemUOMConversion]                                                                          
 @numDomainID as numeric(9),    
 @numItemCode as numeric(9),
 @numUOMId as numeric(9)
AS       
   
   
   SELECT dbo.fn_UOMConversion(@numUOMId,@numItemCode,@numDomainId,numBaseUnit) AS BaseUOMConversion,
   dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) AS PurchaseUOMConversion,
   dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numSaleUnit,0)>0 THEN numSaleUnit ELSE numBaseUnit END) AS SaleUOMConversion,
   dbo.fn_GetUOMName(numBaseUnit) AS vcBaseUOMName 
   FROM item WHERE numDomainID=@numDomainID and numItemCode=@numItemCode
   
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
	 
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '-' AS ShipVia,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocItems' ) 
    DROP PROCEDURE USP_GetMirrorBizDocItems
GO
--- EXEC USP_GetMirrorBizDocItems 53,7,1
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocItems]
    (
      @numReferenceID NUMERIC(9) = NULL,
      @numReferenceType TINYINT = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 

  IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
    			EXEC USP_MirrorOPPBizDocItems @numReferenceID,@numDomainID
END
ELSE
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    DECLARE @tintReturnType AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    
    SELECT  @DivisionID = numDivisionID,@tintReturnType=tintReturnType
    FROM    dbo.ReturnHeader
    WHERE   numReturnHeaderID = @numReferenceID                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
    SET @numBizDocTempID = 0
        
    IF @numReferenceType = 5 OR @numReferenceType = 6 
        BEGIN
            SELECT  @numBizDocTempID = ISNULL(numRMATempID, 0)
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReferenceID                       
        END 
    ELSE IF @numReferenceType = 7 OR @numReferenceType = 8  OR @numReferenceType = 9  OR @numReferenceType = 10 
            BEGIN
                SELECT  @numBizDocTempID = ISNULL(numBizdocTempID, 0)
                FROM    dbo.ReturnHeader
                WHERE   numReturnHeaderID = @numReferenceID
            END  
	
    PRINT 'numBizdocTempID : ' + CONVERT(VARCHAR(10), @numBizdocTempID)
    SELECT  @numBizDocId = numBizDocId
    FROM    dbo.BizDocTemplate
    WHERE   numBizDocTempID = @numBizDocTempID
            AND numDomainID = @numDomainID
      
     IF @numReferenceType = 5 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 6 
	 BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		
		SET @tintType = 8   
	 END
	 ELSE IF @numReferenceType = 7 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 8
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		
		SET @tintType = 8 
	 END
	 ELSE IF @numReferenceType = 9
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 10
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
														  

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT    numReturnItemID,I.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        RI.vcItemDesc AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
--                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
--                                             i.numItemCode, @numDomainID,
--                                             ISNULL(RI.numUOMId, 0))
--                        * RI.numUnitHour AS numUnitHour,
						CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour,
                       RI.monPrice,
					   (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END)  As monUnitSalePrice,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS  Amount,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                         * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS monTotAmount/*Fo calculating sum*/,
                        RH.monTotalTax AS [Tax],
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        RI.numItemCode AS [numoppitemtCode],
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(i.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(RI.numWarehouseItemID,
                                              bitSerialized) AS vcAttributes,
                        '' AS vcPartNo,
                        ( ISNULL(RH.monTotalDiscount, RI.monTotAmount)
                          - RI.monTotAmount ) AS DiscAmt,
                        '' AS vcNotes,
                        '' AS vcTrackingNo,
                        '' AS vcShippingMethod,
                        0 AS monShipCost,
                        [dbo].[FormatedDateFromDate](RH.dtCreatedDate,
                                                     @numDomainID) dtDeliveryDate,
                        RI.numWarehouseItemID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(RI.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        '' AS vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        0 AS DropShip,
                        ISNULL(RI.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(RI.numUOMId, RI.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        i.numShipClass,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalReturnDate,
                       		SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
              FROM      dbo.ReturnHeader RH
                        JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                        LEFT JOIN dbo.Item i ON RI.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = RI.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
                        LEFT JOIN dbo.WareHouseItems WI ON RI.numWarehouseItemID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     RH.numReturnHeaderID = @numReferenceID
            ) X


	--SELECT * FROM #Temp1
	
    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000)
    DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SET @strSQLUpdate = ''
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID

    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintReturnType = 1
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2,Amount)'
    ELSE 
        IF @tintReturnType = 1
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
        ELSE 
            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2,Amount)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'

    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numReferenceID) + ',numReturnItemID,2,Amount)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END
	
    PRINT 'QUERY  :' + @strSQLUpdate
	
    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN  
            PRINT @vcDbColumnName  
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END 

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
    
    END
GO
--- EXEC USP_GetMirrorBizDocItems 30,5,1
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--SELECT * FROM [Chart_Of_Accounts] WHERE [numDomainId]=72

--SELECT SUBSTRING(CONVERT(VARCHAR(20),GETUTCDATE()),0,4)
-- EXEC USP_GetMonthlySummary 72, '2008-09-26 00:00:00.000', '2009-09-26 00:00:00.000','71'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMonthlySummary')
DROP PROCEDURE USP_GetMonthlySummary
GO
CREATE PROCEDURE [dbo].[USP_GetMonthlySummary](
               @numDomainId   AS INT,
               @dtFromDate    AS DATETIME,
               @dtToDate      AS DATETIME,
               @vcChartAcntId AS VARCHAR(500),
               @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine,
			   @numAccountClass NUMERIC(18,0) = 0
               )
AS
  BEGIN
  
	--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
	--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

	PRINT @dtFromDate 
	PRINT @dtToDate

	DECLARE @mnOpeningBalance AS MONEY
	SELECT @mnOpeningBalance = ISNULL(mnOpeningBalance,0)
	FROM dbo.fn_GetOpeningBalance(@vcChartAcntId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
	

		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcChartAcntId, ','))
				AND COA1.numDomainId = @numDomainID
				AND (COA2.vcAccountCode LIKE COA1.vcAccountCode OR (COA2.vcAccountCode LIKE COA1.vcAccountCode + '%' AND COA2.numParentAccID>0))
				
		SELECT @vcChartAcntId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
  
  
    SELECT   numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date)) AS MONTH,
             MONTH(datEntry_Date) AS MonthNumber,
             YEAR(datEntry_Date) AS YEAR,
             SUM(ISNULL(Debit,0)) AS Debit,
             SUM(ISNULL(Credit,0)) AS Credit,
             ISNULL(@mnOpeningBalance,0) AS Opening, 
             0.00 AS Closing
             
             INTO #Temp1
    FROM     VIEW_JOURNAL
    WHERE    datEntry_Date BETWEEN @dtFromDate AND @dtToDate
             AND numDomainId = @numDomainID
             AND numAccountId IN (SELECT * FROM   dbo.SplitIds(@vcChartAcntId,','))
			 AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
    GROUP BY numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             MONTH(datEntry_Date),
             YEAR(datEntry_Date),
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date))
    ORDER BY YEAR(datEntry_Date),MONTH(datEntry_Date)
    
    
--    SELECT DATEPART(MONTH,dtPeriodFrom) FY_Opening_Month,DATEPART(year,dtPeriodFrom) FY_Opening_year, * FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
--Following logic applieds to only income, expense and cogs account
IF EXISTS( SELECT * FROM #Temp1 where SUBSTRING(COAvcAccountCode,0,5) IN ('0103','0104','0106') )
BEGIN
	 alter table #Temp1 alter column MONTH VARCHAR(50);
	 ALTER TABLE [#Temp1] ADD IsFinancialMonth BIT;
	 UPDATE [#Temp1] SET IsFinancialMonth = 0

    DECLARE @numMaxFinYearID NUMERIC
    DECLARE @Month INT
    DECLARE @year  INT
    
    SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId 
--    PRINT @numMaxFinYearID
    WHILE @numMaxFinYearID > 0
    BEGIN
			SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
    
--    PRINT @Month
--    PRINT @year
	/*Rule:make first month of each FY zero as opening balance and show report respectively */
			UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
			WHERE MonthNumber = @Month AND YEAR= @year
			
			IF @@ROWCOUNT=0 -- when No data avail for First month of FY
			BEGIN
				SELECT TOP 1  @Month = MonthNumber FROM #Temp1 WHERE MonthNumber>@Month AND YEAR=@year
				UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
				WHERE MonthNumber = @Month AND YEAR= @year
			END
			
    	
    	
    	 SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId < @numMaxFinYearID
    	IF @@ROWCOUNT = 0 
    	SET @numMaxFinYearID = 0
    END
END
   

    
    SELECT * FROM #Temp1
    
     DROP TABLE #Temp
     DROP TABLE #Temp1
  END
  
--  exec USP_GetMonthlySummary @numDomainId=89,@dtFromDate='2011-12-01 00:00:00:000',@dtToDate='2013-03-01 23:59:59:000',@vcChartAcntId='1444',@ClientTimeZoneOffset=-330

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(300),bitDropShip BIT,numUnitHour INT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					0,OI.numUnitHour,ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END

            
					--AND 1 = (CASE 
					--			WHEN @tintOppType = 1 
					--			THEN (CASE WHEN ISNULL(I.bitKitParent,0) = 0 THEN 1 ELSE 0 END)
					--			ELSE 1
					--		END)
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath  
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	SELECT TOP 1 @numFldID = numFormfieldID,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DynamicFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFormFieldID                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	  AND vcFieldType = 'C' 
	ORDER BY intRowNum

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount)'
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		ELSE 
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount)'

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL
	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFormfieldID,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DynamicFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFormFieldID                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND vcFieldType = 'C' 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END
--Created By Anoop Jayaraj    
    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxAmountOppForTaxItem')
DROP PROCEDURE USP_GetTaxAmountOppForTaxItem
GO
CREATE PROCEDURE [dbo].[USP_GetTaxAmountOppForTaxItem]    
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9),    
@numDivisionID as numeric(9),
@numOppBizDocID as numeric(9),
@tintMode AS TINYINT=0    
AS    
IF @tintMode=0 --Order
BEGIN
	SELECT ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocID),0)
END
ELSE IF @tintMode=5 OR @tintMode=7 OR @tintMode=9 --RMA Return
BEGIN
	DECLARE @fltPercentage FLOAT = 0
	DECLARE @TotalTaxAmt AS MONEY = 0    
	DECLARE @ItemAmount AS MONEY
	DECLARE @tintTaxType TINYINT = 1

	SELECT 
		@fltPercentage=fltPercentage,
		@tintTaxType=ISNULL(tintTaxType,1)
	FROM 
		OpportunityMasterTaxItems 
	WHERE 
		numReturnHeaderID=@numOppID 
		AND numTaxItemID=@numTaxItemID    

	DECLARE @numReturnItemID AS NUMERIC(18) = 0
		
	SELECT TOP 1 
		@numReturnItemID=numReturnItemID,
		@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * monPrice from ReturnItems
	WHERE 
		numReturnHeaderID=@numOppID   
	ORDER BY 
		numReturnItemID 
				   

	WHILE @numReturnItemID>0    
	BEGIN
		IF (select COUNT(*) from OpportunityItemsTaxItems where numReturnItemID=@numReturnItemID and numTaxItemID=@numTaxItemID)>0
		BEGIN
			set @TotalTaxAmt = @TotalTaxAmt + (CASE WHEN @tintTaxType = 2 THEN ISNULL(@fltPercentage,0) ELSE ISNULL(@fltPercentage * @ItemAmount / 100,0) END)
		END    
				    
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * monPrice 
		FROM 
			ReturnItems 
		WHERE 
			numReturnHeaderID=@numOppID  and numReturnItemID>@numReturnItemID   
		ORDER BY 
			numReturnItemID     
				    
		IF @@rowcount=0 SET @numReturnItemID=0    
	END    
				 
	SELECT @TotalTaxAmt AS TotalTaxAmt				 
END  
    
/****** Object:  StoredProcedure [dbo].[usp_GetTaxDetails]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxdetails')
DROP PROCEDURE usp_gettaxdetails
GO
CREATE PROCEDURE [dbo].[usp_GetTaxDetails]  
@numDomainId as numeric(9),
@numTaxItemID as numeric(9)=0,
@numCountry as numeric(9)=0,  
@numState as numeric(9)=0  
as   
  
select  
numTaxID,  
[dbo].[fn_GetListName](numCountryID,0) as country,  
case when numStateID = 0 then 'All' else [dbo].[fn_GetState](numStateID) end as [state] ,  
decTaxPercentage, Case when TaxDetails.numTaxItemID=0 or TaxDetails.numTaxItemID is null  then 'Sales Tax(Default)' else isnull(vcTaxName,'') end as vcTaxName
,isnull(vcCity,'') as vcCity,isnull(vcZipPostal,'') as vcZipPostal,
CASE WHEN tintTaxType = 2 THEN CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' (Flat Amount)') ELSE CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' %') END AS vcTax
from TaxDetails
left join TaxItems
on TaxItems.numTaxItemID=TaxDetails.numTaxItemID
where TaxDetails.numdomainid = @numDomainId 
and (TaxDetails.numTaxItemID=@numTaxItemID or @numTaxItemID=0)
and (TaxDetails.numCountryID=@numCountry or @numCountry=0)
and (TaxDetails.numStateID=@numState or @numState=0)
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTaxForCompany]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxforcompany')
DROP PROCEDURE usp_gettaxforcompany
GO
CREATE PROCEDURE [dbo].[USP_GetTaxForCompany]
@numDivID as numeric(9)
as
-- THIS PROCEDURE IS NOT USED AND LOGIC IS NOT RELEVANT TO CURRENT IMPLEMENTATION 
-- declare @numBillCountry as numeric(9)
--declare @numBillState as numeric(9) 
--declare @numDomainID as numeric(9) 
--declare @TaxPercentage as decimal                                                                                                                                                                                                                                                                                                                 
-- select @numBillState=isnull(numState,0), @numBillCountry=isnull(numCountry,0),@numDomainID=DM.numDomainID  
-- from DivisionMaster DM   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
--   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 where numDivisionID=@numDivID 

--set @TaxPercentage=0
--if @numBillCountry >0 and @numBillState>0
--begin
--    if @numBillState>0
--    begin
--			if exists(select count(*) from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and numDomainID= @numDomainID)
--				select @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and numDomainID= @numDomainID
--			else 
--				select @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and numDomainID= @numDomainID
--			end

--end
--else if @numBillCountry >0
--	select @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and numDomainID= @numDomainID
    

--select @TaxPercentage
--GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxOfCartItems')
DROP PROCEDURE USP_GetTaxOfCartItems
GO
CREATE PROCEDURE [dbo].[USP_GetTaxOfCartItems]
(
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDivisionId AS NUMERIC(9)=0,
    @BaseTaxOn AS Int ,
    @CookieId AS VARCHAR(100),
    @numCountryID AS NUMERIC(9,0),
    @numStateID AS NUMERIC(9,0),
    @vcCity AS VARCHAR(100),
    @vcZipPostalCode AS VARCHAR(20),
    @fltTotalTaxAmount AS FLOAT OUTPUT ,
    @numUserCntId AS NUMERIC(9,0)
)
AS
BEGIN
	Declare  @sqlQuery AS NVARCHAR(MAX) 
	SET @sqlQuery = ''
 
	CREATE TABLE #CartItems
	(
		numItemCode   NUMERIC(9,0) null,
		numTaxItemID NUMERIC(9,0) NULL ,
		monTotAmount MONEY NULL ,
		numCartId NUMERIC(18,0),
		bitApplicable BIT 
	)
  
	CREATE TABLE #TaxDetail  
	(
		numTaxID   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0),
		decTaxPercentage DECIMAL,
		tintTaxType TINYINT,
		numDomainId NUMERIC(9,0),
		vcCity VARCHAR(100) ,
		vcZipPostal VARCHAR(20)
	)

	CREATE TABLE #ApplicableTaxesForDivision
	( 
		vcTaxName   VARCHAR(100) null,
		decTaxPercentage DECIMAL  NULL,
		tintTaxType TINYINT,
		numTaxItemID NUMERIC(9,0) ,
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0) ,
		numDivisionID NUMERIC(9,0),
		bitApplicable BIT
	)
  
	CREATE TABLE #TaxAmount  
	(
		numItemCode   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		bitApplicable BIT ,
		decTaxPercentage DECIMAL ,
		vcTaxName VARCHAR(100),
		monTotAmount MONEY ,
		bitTaxable Bit ,
		fltTaxAmount FLOAT 
	)

	-- here union  is used to remove dublicate rows 
	INSERT INTO 
		#CartItems 
    SELECT 
		IT.numItemCode , 
		IT.numTaxItemID , 
		CI.monTotAmount, 
		CI.numCartId, 
		IT.bitApplicable 
    FROM 
		dbo.ItemTax IT 
	INNER JOIN 
		dbo.CartItems CI 
	ON 
		IT.numItemCode =  CI.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0))
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
    UNION 
    SELECT 
		CI.numItemCode,
		0,
		CI.monTotAmount, 
		CI.numCartId, 
		1 
    FROM 
		dbo.CartItems CI 
	INNER JOIN 
		Item I 
	ON 
		CI.numItemCode = I.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0)) 
		AND  I.bitTaxable = 1          
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
         
 
	SELECT * FROM #CartItems
 
	DECLARE @TaxPercentage AS NUMERIC(9,0)
 

	DECLARE @Temp1 TABLE
	(
		row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
		TaxItemID NUMERIC
	)

	DECLARE @RowNo as INT
	
	INSERT INTO @Temp1 
    (   
        TaxItemID
    )
	SELECT DISTINCT 
		numTaxItemID 
	FROM 
		TaxDetails 
	WHERE 
		numDomainID= @numDomainID

	SELECT * FROM @Temp1

	DECLARE @RowsToProcess int
	DECLARE @CurrentRow int

	SELECT @RowsToProcess = COUNT(*) FROM @Temp1
	SET @CurrentRow = 1
 
	WHILE @CurrentRow <= @RowsToProcess
	BEGIN   
		IF @numCountryID >0 and @numStateID >0
		BEGIN
			IF @numStateID>0        
			BEGIN 
				
			    IF EXISTS(SELECT * FROM TaxDetails TD inner JOIN @Temp1 T ON T.TaxItemID = TD.numTaxItemID WHERE numCountryID=@numCountryID and ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case when isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								and numDomainID= @numDomainID and T.row_id = @CurrentRow)
				BEGIN
					INSERT INTO 
						#TaxDetail 
					SELECT 
						numTaxID, 
						numTaxItemID,  
						numCountryID, 
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails 
					WHERE 
						numCountryID=@numCountryID 
						AND ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case when isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								
								and numDomainID= @numDomainID  
								and numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE IF exists(select * from TaxDetails TD inner JOIN @Temp1 T on T.TaxItemID = TD.numTaxItemID 
							where numCountryID=@numCountryID and (numStateID=@numStateID )
							and numDomainID= @numDomainID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and T.row_id = @CurrentRow)            
				BEGIN
					INSERT INTO 
						#TaxDetail 
					SELECT 
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId, 
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND (numStateID = @numStateID)
						AND numDomainID = @numDomainID 
						AND ISNULL(vcCity,'') = '' AND ISNULL(vcZipPostal,'') = '' AND numTaxItemID = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE    
				BEGIN         
					INSERT INTO 
						#TaxDetail 
					SELECT 
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND numStateID=0 
						AND isnull(vcCity,'')='' 
						AND isnull(vcZipPostal,'')='' 
						AND numDomainID= @numDomainID 
						AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END  
			END                   
		END 
		ELSE IF @numCountryID >0              
		BEGIN 
			INSERT INTO 
				#TaxDetail 
			SELECT 
				numTaxID, 
				numTaxItemID,
				numCountryID,
				numStateID,
				decTaxPercentage,
				tintTaxType,
				numDomainId,
				vcCity,
				vcZipPostal 
			FROM 
				TaxDetails
			WHERE 
				numCountryID=@numCountryID 
				AND numStateID=0 
				AND ISNULL(vcCity,'')='' 
				AND ISNULL(vcZipPostal,'')='' 
				AND numDomainID= @numDomainID 
				AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
		END 

		SET @CurrentRow = @CurrentRow + 1  
	END

	SELECT * FROM #TaxDetail
 
	SET @sqlQuery = @sqlQuery + 'INSERT INTO #ApplicableTaxesForDivision
         SELECT CASE WHEN DTD.numTaxItemID = 0 THEN ''Sales Tax'' ELSE TI.vcTaxName END AS vcTaxName ,TD.decTaxPercentage, TD.tintTaxType, DTD.numTaxItemID , TD.numCountryID ,TD.numStateID , DTD.numDivisionID , DTD.bitApplicable
         FROM dbo.DivisionTaxTypes DTD LEFT JOIN #TaxDetail TD ON DTD.numTaxItemID = TD.numTaxItemID
         LEFT JOIN dbo.TaxItems TI ON TD.numDomainId = TI.numDomainID AND TD.numTaxItemID = TI.numTaxItemID
         WHERE isnull(DTD.bitApplicable,0)=1 and numDivisionID=' + Convert(varchar(100) , @numDivisionID)  +' AND TD.numDomainId= ' + Convert(varchar(100) , @numDomainID) 

	EXEC(@sqlQuery)

	SELECT * FROM  #ApplicableTaxesForDivision
  
	INSERT INTO 
		#TaxAmount
    SELECT 
		I.numItemCode, 
		TT.numTaxItemID,
		TT.bitApplicable, 
		TT1.decTaxPercentage,
		TT1.vcTaxName,
		TT.monTotAmount,
		I.bitTaxable, 
		CASE 
			WHEN TT1.tintTaxType = 2 --FLAT AMOUNT
			THEN CONVERT(FLOAT, TT1.decTaxPercentage)
			ELSE --PERCENT
				((CONVERT(FLOAT, TT.monTotAmount)   * CONVERT(FLOAT, TT1.decTaxPercentage))/100) 
		END AS fltTaxAmount  
	FROM 
		#CartItems TT 
	INNER JOIN 
		#ApplicableTaxesForDivision TT1 
	ON 
		TT.numTaxItemID = TT1.numTaxItemID 
    INNER JOIN 
		Item I 
	ON 
		I.numItemCode = TT.numItemCode  
	WHERE 
		I.bitTaxable = 1 ORDER BY numItemCode
  
	SELECT numItemCode,numTaxItemID ,bitApplicable  ,fltTaxAmount AS fltTaxAmount  FROM  #TaxAmount
	SELECT numItemCode ,SUM(fltTaxAmount) AS fltTaxAmount  FROM  #TaxAmount GROUP BY numItemCode 
  
  
	IF EXISTS(SELECT * FROM #TaxAmount )
	BEGIN
		SELECT @fltTotalTaxAmount =  SUM(fltTaxAmount)  FROM  #TaxAmount  WHERE bitTaxable = 1 
	END

	DROP TABLE #CartItems
	DROP TABLE #ApplicableTaxesForDivision
	DROP TABLE #TaxDetail
	DROP TABLE #TaxAmount
 
 END
GO

--exec USP_GetTaxOfCartItems @numDomainId=1,@numDivisionId=1,@BaseTaxOn=2,@CookieId='bbe5f0f4-2791-461f-a5fa-d7d33cf2f7f0',@numCountryID=600,@numStateID=112,@vcCity='Ahmedabad',@vcZipPostalCode='361305',@fltTotalTaxAmount=0,@numUserCntId=1
/****** Object:  StoredProcedure [dbo].[USP_GetTaxPercentage]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxpercentage')
DROP PROCEDURE usp_gettaxpercentage
GO
CREATE PROCEDURE [dbo].[USP_GetTaxPercentage]
@DivisionID as numeric(9)=0,
@numBillCountry as numeric(9)=0,
@numBillState  as numeric(9)=0,
@numDomainID as numeric(9),
@numTaxItemID as numeric(9),
@tintBaseTaxCalcOn TINYINT=2, -- Base tax calculation on Shipping Address(2) or Billing Address(1)
@tintBaseTaxOnArea TINYINT=0,
@vcCity varchar(100),
@vcZipPostal varchar(20)
as

  
DECLARE @TaxPercentage as float 
DECLARE @tintTaxType AS TINYINT

declare @bitTaxApplicable as bit 
set @TaxPercentage=0 
 
if @DivisionID>0 --Existing Customer
BEGIN
	IF @tintBaseTaxCalcOn = 1 --Billing Address
			select @numBillState=isnull(numState,0), @numBillCountry=isnull(numCountry,0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 where numDivisionID=@DivisionID
	ELSE --Shipping Address
			select @numBillState=isnull([numState],0), @numBillCountry=isnull([numCountry],0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
			AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 where numDivisionID=@DivisionID
             
	select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=  @DivisionID and numTaxItemID=@numTaxItemID

end 
else --New Customer
begin
	SET @bitTaxApplicable=1
END

--Take from TaxCountryConfi If not then use default from domain
select @tintBaseTaxOnArea=tintBaseTaxOnArea from TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numBillCountry 

if  @bitTaxApplicable=1 --If Tax Applicable
	begin
		if @numBillCountry >0 and @numBillState>0            
		begin            
			if @numBillState>0            
			begin            
				if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
						(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity + '%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
								(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState 
							and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''         
						END
				 else             
					select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID          
		   end                
		end            
		else if @numBillCountry >0            
			 select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end

select CONCAT(ISNULL(@TaxPercentage,0),'#',ISNULL(@tintTaxType,1))
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTopAPIOrderReport' ) 
    DROP PROCEDURE USP_GetTopAPIOrderReport
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROC [dbo].[USP_GetTopAPIOrderReport]
          @numDomainID AS NUMERIC(9),
          @numWebApiId AS NUMERIC(9),
		@Mode AS TINYINT = 0
AS
  BEGIN
IF @Mode = 0
BEGIN

SELECT TOP 1 ISNULL(vcWebApiOrderReportId,0) as ReportId ,ISNULL(tintReportTypeId,0) as tintReportTypeId FROM WebApiOrderReports 
WHERE numDomainId = @numDomainID AND WebApiId = @numWebApiId AND tintStatus = 0

END

IF @Mode = 1
BEGIN

SELECT TOP 1 IAO.numImportApiOrderReqId as RequestId,ISNULL(IAO.tintRequestType,0) AS tintRequestType,ISNULL(IAO.vcApiOrderId,0) AS vcApiOrderId,
ISNULL(IAO.dtFromDate,'') as dtFromDate,ISNULL(IAO.dtToDate,'') AS dtToDate,ISNULL(IAO.dtCreatedDate,'') AS dtCreatedDate,IAO.numOppId,OM.vcMarketplaceOrderReportId,IAO.numOppId
FROM ImportApiOrder IAO JOIN OpportunityMaster OM on IAO.numOppId = OM.numOppId
WHERE IAO.numDomainId = @numDomainID AND IAO.numWebApiId = @numWebApiId AND IAO.bitIsActive = 1
ORDER BY IAO.numImportApiOrderReqId DESC

END


END 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0
	DECLARE @PLCHARTAccountCode AS VARCHAR(100) = ''
	SELECT @PLCHARTID=COA.numAccountId,@PLCHARTAccountCode=vcAccountCode FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ COA.vcAccountCode +''#%'' AND COA.bitProfitLoss <> 1 THEN -(ProfitLossAmount) + ISNULL(SUM(Amount),0)
								WHEN COA.bitProfitLoss = 1 THEN -(ProfitLossAmount) 
								ELSE ISNULL(SUM(Amount),0) 
							END) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),
							MonthYear
						FROM
							#TempYearMonth
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ COA.vcAccountCode +''#%'' AND COA.bitProfitLoss <> 1 THEN -(ProfitLossAmount) + ISNULL(SUM(Amount),0)
								WHEN COA.bitProfitLoss = 1 THEN -(ProfitLossAmount) 
								ELSE ISNULL(SUM(Amount),0) 
							END) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS MONEY = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2

		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		Select 
			COA.ParentId, 
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE 
				WHEN '#' + @PLCHARTAccountCode + '#' LIKE '%#'+ COA.vcAccountCode +'#%' AND COA.bitProfitLoss <> 1 THEN -(@monProfitLossAmount) + ISNULL(SUM(Amount),0)
				WHEN COA.bitProfitLoss = 1 THEN -(@monProfitLossAmount) 
				ELSE ISNULL(SUM(Amount),0) 
			END) AS Amount
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like COA.Struc + '%' 
		GROUP BY 
			COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss
		UNION
		SELECT 
			0, 
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			'-1' AS Struc,
			2,
			(SELECT 
				SUM(Debit) - SUM(Credit)
			FROM 
				VIEW_JOURNAL 
			WHERE 
				numDomainID = 187
				AND (vcAccountCode LIKE '0101%' 
				OR vcAccountCode LIKE '0102%'
				OR vcAccountCode LIKE '0103%'
				OR vcAccountCode LIKE '0104%'
				OR vcAccountCode LIKE '0105%'
				OR vcAccountCode LIKE '0106%')
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOMConversion')
DROP PROCEDURE USP_GetUOMConversion
GO
CREATE PROCEDURE [dbo].[USP_GetUOMConversion]                                                                          
 @numDomainID as numeric(9)                  
AS       
      
   SELECT c.numUOMConv,c.numUOM1,c.decConv1,c.numUOM2,c.decConv2
   FROM UOMConversion c INNER JOIN UOM u ON c.numUOM1=u.numUOMId INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE c.numDomainID=@numDomainID AND u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
        u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
         
/****** Object:  StoredProcedure [dbo].[usp_GetUserListForUserDetails]    Script Date: 07/26/2008 16:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserlistforuserdetails')
DROP PROCEDURE usp_getuserlistforuserdetails
GO
CREATE PROCEDURE [dbo].[usp_GetUserListForUserDetails]         
@numUserID NUMERIC(9) = 0,    
@numDomainID numeric(9)=0        
     
AS                 
          
  
select A.numContactId, ISNULL(A.vcfirstname,'') +' '+ ISNULL(A.vclastname,'') as vcGivenName       
From AdditionalContactsInformation A   
left join Domain D  
on D.numDomainID=A.numDomainID       
where A.numDivisionID=D.numDivisionID and A.numDomainID=@numDomainID 
and A.numContactId not in (select distinct(numUserDetailId) from UserMaster where numDomainID=@numDomainID and numUserID<>@numUserID)

/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL                             
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


SET @ColName='WareHouseItems.numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
ISNULL(WL.vcLocation,'''') AS vcInternalLocation,
W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) end) as numeric(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) end) as numeric(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) end) as numeric(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) end) as numeric(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',@bitLot,')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'              
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeader]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertjournalentryheader' ) 
    DROP PROCEDURE usp_insertjournalentryheader
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeader]
	@numJournal_Id AS NUMERIC(9) = 0 OUTPUT,
	@numRecurringId AS NUMERIC(9) = 0,
    @datEntry_Date AS DATETIME,
    @vcDescription AS VARCHAR(1000),
    @numAmount AS MONEY,
    @numCheckId NUMERIC=NULL,
    @numCashCreditCardId NUMERIC= NULL,
    @numChartAcntId NUMERIC=NULL,
    @numOppId NUMERIC=NULL,
    @numOppBizDocsId NUMERIC=NULL,
    @numDepositId NUMERIC=NULL,
    @numBizDocsPaymentDetId NUMERIC=NULL,
    @bitOpeningBalance BIT=NULL,
    @numCategoryHDRID NUMERIC=NULL,
    @numReturnID NUMERIC=NULL,
    @numCheckHeaderID numeric(18, 0),
	@numBillID numeric(18, 0),
	@numBillPaymentID numeric(18, 0),
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numReconcileID AS NUMERIC(9) = NULL,
    @numJournalReferenceNo AS NUMERIC(9)=0,
    @numPayrollDetailID NUMERIC=NULL
AS 
BEGIN    
	--Validation of closed financial year
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date
	
    IF @numRecurringId = 0 SET @numRecurringId = NULL  
--    IF @numProjectID = 0 SET @numProjectID = NULL
    IF @numCheckId = 0 SET @numCheckId = NULL
    IF @numCashCreditCardId = 0 SET @numCashCreditCardId = NULL
    IF @numChartAcntId = 0 SET @numChartAcntId = NULL
    IF @numOppId = 0 SET @numOppId = NULL
    IF @numOppBizDocsId = 0 SET @numOppBizDocsId = NULL
    IF @numDepositId = 0 SET @numDepositId = NULL
    IF @numBizDocsPaymentDetId = 0 SET @numBizDocsPaymentDetId = NULL
    IF @bitOpeningBalance = 0 SET @bitOpeningBalance = NULL
    IF @numCategoryHDRID = 0 SET @numCategoryHDRID = NULL
--    IF @numClassID = 0 SET @numClassID = NULL
    IF @numReturnID=0 SET @numReturnID=NULL
    IF @numReconcileID=0 SET @numReconcileID=NULL
    IF @numJournalReferenceNo=0 SET @numJournalReferenceNo=NULL
    IF @numPayrollDetailID=0 SET @numPayrollDetailID=NULL
    
DECLARE @numEntryDateSortOrder AS NUMERIC

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

    IF @numJournal_Id = 0
        BEGIN
           --Set Default Class If enable User Level Class Accountng 
		  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
		  
		  IF ISNULL(@numDepositID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM DepositMaster 
				WHERE numDomainID=@numDomainID AND numDepositID=@numDepositID
		  END
		  ELSE IF ISNULL(@numBillID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM BillHeader 
				WHERE numDomainID=@numDomainID AND numBillID=@numBillID
		  END
		  ELSE IF ISNULL(@numBillPaymentID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM BillPaymentHeader 
				WHERE numDomainID=@numDomainID AND numBillPaymentID=@numBillPaymentID
		  END
		  ELSE IF ISNULL(@numCheckHeaderID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM CheckHeader 
				WHERE numDomainID=@numDomainID AND numCheckHeaderID=@numCheckHeaderID
		  END
		  ELSE IF ISNULL(@numReturnID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM ReturnHeader 
				WHERE numDomainID=@numDomainID AND numReturnHeaderID=@numReturnID
		  END
		  ELSE IF ISNULL(@numOppId,0)>0
		  BEGIN
		     SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM OpportunityMaster 
				WHERE numDomainID=@numDomainID AND numOppId=@numOppId
		  END
		  ELSE
		  BEGIN
		  	  -- GET ACCOUNT CLASS IF ENABLED
			DECLARE @tintDefaultClassType AS INT = 0
			SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

			IF @tintDefaultClassType = 1 --USER
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numDefaultClass,0) 
				FROM 
					dbo.UserMaster UM 
				JOIN 
					dbo.Domain D 
				ON 
					UM.numDomainID=D.numDomainId
				WHERE 
					D.numDomainId=@numDomainId 
					AND UM.numUserDetailId=@numUserCntID
			END
			ELSE
			BEGIN
				SET @numAccountClass = 0
			END
		  END
		
           INSERT INTO dbo.General_Journal_Header (
				numRecurringId,
				datEntry_Date,
				varDescription,
				numAmount,
				numDomainId,
				numCheckId,
				numCashCreditCardId,
				numChartAcntId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numBizDocsPaymentDetId,
				bitOpeningBalance,
				dtLastRecurringDate,
				numNoTransactions,
				numCreatedBy,
				datCreatedDate,
				numCategoryHDRID,
--				numProjectID,
--				numClassID,
				numModifiedBy,
				dtModifiedDate,
				numReturnID,
				[numCheckHeaderID],
				[numBillID],
				[numBillPaymentID],numReconcileID,numJournalReferenceNo,numPayrollDetailID,
				[numEntryDateSortOrder],numAccountClass
			) VALUES ( 
				 @numRecurringId,
				 CAST(@datEntry_Date AS SMALLDATETIME),
				 @vcDescription,
				 @numAmount,
				 @numDomainId,
				 @numCheckId,
				 @numCashCreditCardId,
				 @numChartAcntId,
				 @numOppId,
				 @numOppBizDocsId,
				 @numDepositId,
				 @numBizDocsPaymentDetId,
				 @bitOpeningBalance,
				 null,
				 null,
				 @numUserCntID,
				 GETUTCDATE(),
				 @numCategoryHDRID,
--				 @numProjectID,
--				 @numClassID,
				 @numUserCntID,
				 GETUTCDATE(),
				 @numReturnID,
				 @numCheckHeaderID,
				 @numBillID,
				 @numBillPaymentID,@numReconcileID,@numJournalReferenceNo,@numPayrollDetailID,
				 @numEntryDateSortOrder,@numAccountClass)
            SET @numJournal_Id = @@IDENTITY                              
            SELECT  @numJournal_Id 
        END                        
        ELSE
        BEGIN
			UPDATE  [General_Journal_Header]
			SET     [numRecurringId] = @numRecurringId,
					[datEntry_Date] = CAST(@datEntry_Date AS SMALLDATETIME),
					[varDescription] = @vcDescription,
					[numAmount] = @numAmount,
					[numDomainId] = @numDomainId,
					[numCheckId] = @numCheckId,
					[numCashCreditCardId] = @numCashCreditCardId,
		--      ,[numChartAcntId] = @numChartAcntId
					[numOppId] = @numOppId,
					[numOppBizDocsId] = @numOppBizDocsId,
					[numDepositId] = @numDepositId,
					[numBizDocsPaymentDetId] = @numBizDocsPaymentDetId,
					[bitOpeningBalance] = @bitOpeningBalance,
					[numCategoryHDRID] = @numCategoryHDRID,
--					[numProjectID] = @numProjectID,
--					[numClassID] = @numClassID,
					numModifiedBy = @numUserCntID,
					dtModifiedDate =GETUTCDATE(),
					numReturnID = @numReturnID,
					[numCheckHeaderID] = @numCheckHeaderID,
					[numBillID] = @numBillID,
					[numBillPaymentID] = @numBillPaymentID,
					numReconcileID=@numReconcileID,
					numJournalReferenceNo=@numJournalReferenceNo,
					numPayrollDetailID=@numPayrollDetailID,
					numEntryDateSortOrder = @numEntryDateSortOrder
			WHERE   numJournal_Id = @numJournal_Id
					AND numDomainId = @numDomainID
			SELECT  @numJournal_Id 
		END

SELECT GJH1.numJournal_ID,GJH1.numEntryDateSortOrder,Row_number() OVER(ORDER BY GJH1.numJournal_ID) AS NewSortId  INTO #Temp1 FROM General_Journal_Header GJH1
WHERE GJH1.numDomainId = @numDomainID and  GJH1.numEntryDateSortOrder  = @numEntryDateSortOrder

UPDATE  GJH2 SET GJH2.numEntryDateSortOrder = (Temp1.numEntryDateSortOrder + Temp1.NewSortId) 
FROM General_Journal_Header GJH2 INNER JOIN #Temp1 AS Temp1 ON GJH2.numJournal_ID = Temp1.numJournal_ID

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1	


END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		ISNULL(Item.monAverageCost,0.00) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate
		) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.Fld_label AS Name,
		CFW_Fld_Master.Fld_type AS [Type],
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		CASE 
			WHEN CFW_Fld_Master.Fld_type='TextBox' or CFW_Fld_Master.Fld_type='TextArea' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='0' OR CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE CFW_FLD_Values_Item.Fld_Value END)
			WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(CFW_FLD_Values_Item.Fld_Value) END)
			WHEN CFW_Fld_Master.Fld_type='CheckBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		I.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)


	DROp TABLE #Temp

END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_Search_BIZAPI' ) 
    DROP PROCEDURE USP_Item_Search_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_Search_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID 
		AND	Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID AND
		Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.Fld_label AS Name,
		CFW_Fld_Master.Fld_type AS [Type],
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		CASE 
			WHEN CFW_Fld_Master.Fld_type='TextBox' or CFW_Fld_Master.Fld_type='TextArea' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='0' OR CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE CFW_FLD_Values_Item.Fld_Value END)
			WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(CFW_FLD_Values_Item.Fld_Value) END)
			WHEN CFW_Fld_Master.Fld_type='CheckBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		I.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)


	DROp TABLE #Temp
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, 
case 
when charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
when charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
when charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
else case when charItemType='P' then 'Inventory' else '' end
end as InventoryItemType,
dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount,
case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numWOQty,
monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, 
ISNULL(vcSKU,'') AS vcItemSKU,
(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
ISNULL(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                   
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
ISNULL(I.bitAsset,0) AS [bitAsset],
ISNULL(I.bitRental,0) AS [bitRental],
ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount
FROM Item I       
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode                
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
WHERE I.numItemCode=@numItemCode  
GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode]


--exec USP_ItemDetails @numItemCode = 822625 ,@ClientTimeZoneOffset =330

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_CheckConversionExists')
DROP PROCEDURE dbo.USP_ItemUOMConversion_CheckConversionExists
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_CheckConversionExists]
(
	@numDomainID NUMERIC,
	@numItemCode NUMERIC,
	@numSourceUnit NUMERIC
)
AS 
BEGIN  
	DECLARE @numBaseUnit AS NUMERIC(18,0) = 0

	SELECT @numBaseUnit=ISNULL(numBaseUnit,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

	IF ISNULL(@numBaseUnit,0) = 0
	BEGIN
		RAISERROR('ITEM_BASE_UOM_MISSING',16,1)
	END
	ELSE 
	BEGIN
		IF @numBaseUnit = @numSourceUnit
		BEGIN 
			SELECT 1 --NO NEED TO CHECK CONVERSION BECAUSE IT IS SAME AS BASE UOM
		END
		ELSE
		BEGIN
			SELECT dbo.fn_CheckIfItemUOMConversionExists(@numDomainID,@numItemCode,@numSourceUnit)
		END
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_Delete')
DROP PROCEDURE dbo.USP_ItemUOMConversion_Delete
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_Delete]
(
	@numItemUOMConvID NUMERIC(18,0)
)
AS 
BEGIN  
	DELETE FROM ItemUOMConversion WHERE numItemUOMConvID = @numItemUOMConvID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_GetAllByItem')
DROP PROCEDURE dbo.USP_ItemUOMConversion_GetAllByItem
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_GetAllByItem]
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)
AS 
BEGIN
	SELECT numBaseUnit FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

	SELECT 
		ItemUOMConversion.numItemUOMConvID,
		ItemUOMConversion.numDomainID,
		ItemUOMConversion.numItemCode,
		ItemUOMConversion.numSourceUOM,
		SourceUOM.vcUnitName AS SourceUOM,
		ItemUOMConversion.numTargetUOM,
		TargetUOM.vcUnitName AS TargetUOM,
		ItemUOMConversion.numTargetUnit
	FROM
		ItemUOMConversion 
	INNER JOIN
		UOM AS SourceUOM
	ON
		ItemUOMConversion.numSourceUOM = SourceUOM.numUOMId
	INNER JOIN
		UOM AS TargetUOM
	ON
		ItemUOMConversion.numTargetUOM = TargetUOM.numUOMId
	WHERE
		ItemUOMConversion.numDomainID = @numDomainID
		AND ItemUOMConversion.numItemCode = @numItemCode
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_Save')
DROP PROCEDURE dbo.USP_ItemUOMConversion_Save
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_Save]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numSourceUOM NUMERIC(18,0),
	@numTargetUOM NUMERIC(18,0),
	@numTargetUnit FLOAT
)
AS 
BEGIN  
	INSERT INTO ItemUOMConversion
	(
		numDomainID,
		numItemCode,
		numSourceUOM,
		numTargetUOM,
		numTargetUnit,
		numCreatedBy,
		dtCreated
	)
	VALUES
	(
		@numDomainID,
		@numItemCode,
		@numSourceUOM,
		@numTargetUOM,
		@numTargetUnit,
		@numUserCntID,
		GETDATE()
	)
END

GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseBasedOnItem]    Script Date: 09/01/2009 19:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseBasedOnItem 14    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehousebasedonitem')
DROP PROCEDURE usp_loadwarehousebasedonitem
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseBasedOnItem]    
@numItemCode as varchar(20)=''  
as    
DECLARE @numDomainID AS NUMERIC(18,0)
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100) 
declare @bitSerialize as bit     
DECLARE @bitKitParent BIT
 
SELECT 
	@numDomainID = numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@bitSerialize=bitSerialized,
	@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode    


SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)


DECLARE @KitOnHand AS NUMERIC(9) = 0

IF @bitKitParent=1
	SELECT @KitOnHand=dbo.fn_GetKitInventory(@numItemCode)          

SELECT 
	DISTINCT(WareHouseItems.numWareHouseItemId),
	vcWareHouse, 
	dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
	WareHouseItems.monWListPrice,
	CASE WHEN @bitKitParent=1 THEN @KitOnHand else ISNULL(numOnHand,0) END numOnHand,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
	Warehouses.numWareHouseID,
	@vcUnitName AS vcUnitName,
	@vcSaleUnitName AS vcSaleUnitName,
	@vcPurchaseUnitName AS vcPurchaseUnitName,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN CAST(CAST(@KitOnHand/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL(numOnHand,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnHandUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnOrderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numReorderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numAllocationUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numBackOrderUOM
FROM 
	WareHouseItems    
JOIN 
	Warehouses 
ON 
	Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
WHERE 
	numItemID=@numItemCode

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0),
	@numOppId NUMERIC(18,0)=0,
	@bitLandedCost BIT=0,
	@numCurrencyID NUMERIC(18,0) = 0
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate

		DECLARE @fltExchangeRate AS FLOAT

		IF ISNULL(@numCurrencyID,0) = 0
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId = @numDomainID
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID,
						numCurrencyID=@numCurrencyID,
						fltExchangeRate = @fltExchangeRate
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN

				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass,[numOppId],[bitLandedCost],[numCurrencyID],[fltExchangeRate]
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass,@numOppId,@bitLandedCost,@numCurrencyID,@fltExchangeRate
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
                 DELETE FROM BillDetails WHERE numBillID = @numBillID 
					AND numBillDetailID NOT IN (SELECT X.numBillDetailID 
			 FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(numBillDetailID numeric(9))X)
			                                                           
			--Update transactions
			Update BillDetails  Set numExpenseAccountID=X.numExpenseAccountID,monAmount=X.monAmount,                                                                                              
			vcDescription=X.vcDescription,numProjectID=X.numProjectID
			,numClassID=X.numClassID,numCampaignID=X.numCampaignID
			From (SELECT * FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(                                                                                       
			numBillDetailID NUMERIC(18,0),[numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0)                                                                                                 
			))X                                                                                                  
			Where BillDetails.numBillDetailID=X.numBillDetailID AND numBillID = @numBillID                                                                                             
			               
			
                INSERT  INTO [dbo].[BillDetails]
                        (
                          [numBillID],
                          [numExpenseAccountID],
                          [monAmount],
                          [vcDescription],
                          [numProjectID],
                          [numClassID],
                          [numCampaignID]
                        )
                        SELECT  @numBillID,
                                X.[numExpenseAccountID],
                                X.[monAmount],
                                X.[vcDescription],
                                X.[numProjectID],
                                X.[numClassID],
                                X.[numCampaignID]
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1 [numBillDetailID=0]', 2)
                                            WITH (numBillDetailID NUMERIC(18,0), [numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0) )
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END

		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageCheckHeader' )
    DROP PROCEDURE USP_ManageCheckHeader
GO
CREATE PROCEDURE USP_ManageCheckHeader
    @tintMode AS TINYINT ,
    @numDomainID NUMERIC(18, 0) ,
    @numCheckHeaderID NUMERIC(18, 0) ,
    @numChartAcntId NUMERIC(18, 0) ,
    @numDivisionID NUMERIC(18, 0) ,
    @monAmount MONEY ,
    @numCheckNo NUMERIC(18, 0) ,
    @tintReferenceType TINYINT ,
    @numReferenceID NUMERIC(18, 0) ,
    @dtCheckDate DATETIME ,
    @bitIsPrint BIT ,
    @vcMemo VARCHAR(1000) ,
    @numUserCntID NUMERIC(18, 0)
AS --Validation of closed financial year
    EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID, @dtCheckDate
	
    IF @tintMode = 1 --SELECT Particulat Check Header
        BEGIN
            SELECT  CH.numCheckHeaderID ,
                    CH.numChartAcntId ,
                    CH.numDomainID ,
                    CH.numDivisionID ,
                    CH.monAmount ,
                    CH.numCheckNo ,
                    CH.tintReferenceType ,
                    CH.numReferenceID ,
                    CH.dtCheckDate ,
                    CH.bitIsPrint ,
                    CH.vcMemo ,
                    CH.numCreatedBy ,
                    CH.dtCreatedDate ,
                    CH.numModifiedBy ,
                    CH.dtModifiedDate ,
                    ISNULL(GJD.numTransactionId, 0) AS numTransactionId
            FROM    dbo.CheckHeader CH
                    LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType = 1
                                                              AND GJD.numReferenceID = CH.numCheckHeaderID
            WHERE   numCheckHeaderID = @numCheckHeaderID
                    AND CH.numDomainID = @numDomainID	

        END
    ELSE
        IF @tintMode = 2 --Insert/Update Check Header
            BEGIN
                IF @numCheckHeaderID > 0 --Update
                    BEGIN
                        UPDATE  dbo.CheckHeader
                        SET     numChartAcntId = @numChartAcntId ,
                                numDivisionID = @numDivisionID ,
                                monAmount = @monAmount ,
                                numCheckNo = @numCheckNo ,
                                tintReferenceType = @tintReferenceType ,
                                numReferenceID = @numReferenceID ,
                                dtCheckDate = @dtCheckDate ,
                                bitIsPrint = @bitIsPrint ,
                                vcMemo = @vcMemo ,
                                numModifiedBy = @numUserCntID ,
                                dtModifiedDate = GETUTCDATE()
                        WHERE   numCheckHeaderID = @numCheckHeaderID
                                AND numDomainID = @numDomainID

                    END
                ELSE --Insert
                    BEGIN
					  --Set Default Class If enable User Level Class Accountng 
                        DECLARE @numAccountClass AS NUMERIC(18);
                        SET @numAccountClass = 0
					  
                        IF @tintReferenceType = 1 --Only for Direct Check
                        BEGIN
                            DECLARE @tintDefaultClassType AS INT = 0
							SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

							IF @tintDefaultClassType = 1 --USER
							BEGIN
								SELECT 
									@numAccountClass=ISNULL(numDefaultClass,0) 
								FROM 
									dbo.UserMaster UM 
								JOIN 
									dbo.Domain D 
								ON 
									UM.numDomainID=D.numDomainId
								WHERE 
									D.numDomainId=@numDomainId 
									AND UM.numUserDetailId=@numUserCntID
							END
							ELSE IF @tintDefaultClassType = 2 --COMPANY
							BEGIN
								SELECT 
									@numAccountClass=ISNULL(numAccountClassID,0) 
								FROM 
									dbo.DivisionMaster DM 
								WHERE 
									DM.numDomainId=@numDomainId 
									AND DM.numDivisionID=@numDivisionID
							END
							ELSE
							BEGIN
								SET @numAccountClass = 0
							END
                        END
					  
                        INSERT  INTO dbo.CheckHeader
                                ( numChartAcntId ,
                                  numDomainID ,
                                  numDivisionID ,
                                  monAmount ,
                                  numCheckNo ,
                                  tintReferenceType ,
                                  numReferenceID ,
                                  dtCheckDate ,
                                  bitIsPrint ,
                                  vcMemo ,
                                  numCreatedBy ,
                                  dtCreatedDate ,
                                  numAccountClass
                                )
                                SELECT  @numChartAcntId ,
                                        @numDomainID ,
                                        @numDivisionID ,
                                        @monAmount ,
                                        @numCheckNo ,
                                        @tintReferenceType ,
                                        @numReferenceID ,
                                        @dtCheckDate ,
                                        @bitIsPrint ,
                                        @vcMemo ,
                                        @numUserCntID ,
                                        GETUTCDATE() ,
                                        @numAccountClass

                        SET @numCheckHeaderID = SCOPE_IDENTITY()
                    END	
			
                SELECT  numCheckHeaderID ,
                        numChartAcntId ,
                        numDomainID ,
                        numDivisionID ,
                        monAmount ,
                        numCheckNo ,
                        tintReferenceType ,
                        numReferenceID ,
                        dtCheckDate ,
                        bitIsPrint ,
                        vcMemo ,
                        numCreatedBy ,
                        dtCreatedDate ,
                        numModifiedBy ,
                        dtModifiedDate
                FROM    dbo.CheckHeader
                WHERE   numCheckHeaderID = @numCheckHeaderID
                        AND numDomainID = @numDomainID	
            END
        ELSE
            IF @tintMode = 3 --Print Check List
                BEGIN
                    SELECT  CH.numCheckHeaderID ,
                            CH.numChartAcntId ,
                            CH.numDomainID ,
                            CH.numDivisionID ,
                            CH.monAmount ,
                            CH.numCheckNo ,
                            CH.tintReferenceType ,
                            CH.numReferenceID ,
                            CH.dtCheckDate ,
                            CH.bitIsPrint ,
                            CH.vcMemo ,
                            CH.numCreatedBy ,
                            CH.dtCreatedDate ,
                            CH.numModifiedBy ,
                            CH.dtModifiedDate ,
                            CASE CH.tintReferenceType
                              WHEN 11
                              THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
                                                             0))
                              ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
                                                              0))
                            END AS vcCompanyName ,
                            CASE CH.tintReferenceType
                              WHEN 1 THEN 'Checks'
                              WHEN 8 THEN 'Bill Payment'
                              WHEN 10 THEN 'RMA'
                              WHEN 11 THEN 'Payroll'
                            END AS vcReferenceType ,
                            dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
                            CASE CH.tintReferenceType
                              WHEN 1 THEN ''
                              WHEN 8
                              THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                              ','
                                                              + CASE
                                                              WHEN ISNULL(BPD.numBillID,
                                                              0) > 0
                                                              THEN 'Bill-'
                                                              + CAST(BPD.numBillID AS VARCHAR(18))
                                                              ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
                                                              END
                                                              FROM
                                                              BillPaymentHeader BPH
                                                              JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                              LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                              WHERE
                                                              BPH.numDomainID = CH.numDomainID
                                                              AND BPH.numBillPaymentID = CH.numReferenceID
                                                              FOR
                                                              XML
                                                              PATH('')
                                                              ), 2, 200000)
                                                   ), '')
                                   )
                            END AS vcBizDoc ,
                            CASE CH.tintReferenceType
                              WHEN 1 THEN ''
                              WHEN 8
                              THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                              ','
                                                              + CASE
                                                              WHEN BPD.numOppBizDocsID > 0
                                                              THEN CAST(ISNULL(OBD.vcRefOrderNo,
                                                              '') AS VARCHAR(18))
                                                              WHEN BPD.numBillID > 0
                                                              THEN CAST(ISNULL(BH.vcReference,
                                                              '') AS VARCHAR(500))
                                                              END
                                                              FROM
                                                              BillPaymentHeader BPH
                                                              JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                              LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                              LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
                                                              WHERE
                                                              BPH.numDomainID = CH.numDomainID
                                                              AND BPH.numBillPaymentID = CH.numReferenceID
                                                              FOR
                                                              XML
                                                              PATH('')
                                                              ), 2, 200000)
                                                   ), '')
                                   )
                              ELSE ''
                            END AS vcRefOrderNo
                    FROM    dbo.CheckHeader CH
                            LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID
                                                          AND CH.tintReferenceType = 11
                    WHERE   CH.numDomainID = @numDomainID
                            AND ( numCheckHeaderID = @numCheckHeaderID
                                  OR @numCheckHeaderID = 0
                                )
                            AND ( CH.tintReferenceType = @tintReferenceType
                                  OR @tintReferenceType = 0
                                )
                            AND ( CH.numReferenceID = @numReferenceID
                                  OR @numReferenceID = 0
                                )
                            AND CH.numChartAcntId = @numChartAcntId
                            AND ISNULL(bitIsPrint, 0) = 0	 
                END
            ELSE
                IF @tintMode = 4 --New Print Check List
                    BEGIN
					PRINT 1
                        SELECT  CH.numCheckHeaderID ,
                                CH.numChartAcntId ,
                                CH.numDomainID ,
                                CH.numDivisionID ,
                                (CASE WHEN tintReferenceType=10 THEN ISNULL(CH.monAmount,0) ELSE  ISNULL([BPD].[monAmount],0) END) monAmount,
                                CH.numCheckNo ,
                                CH.tintReferenceType ,
                                CH.numReferenceID ,
                                CH.dtCheckDate ,
                                CH.bitIsPrint ,
                                CH.vcMemo ,
                                CH.numCreatedBy ,
                                CH.dtCreatedDate ,
                                CH.numModifiedBy ,
                                CH.dtModifiedDate ,
                                CASE CH.tintReferenceType
                                  WHEN 11
                                  THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
                                                              0))
                                  ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
                                                              0))
                                END AS vcCompanyName ,
                                CASE CH.tintReferenceType
                                  WHEN 1 THEN 'Checks'
                                  WHEN 8 THEN 'Bill Payment'
                                  WHEN 10 THEN 'RMA'
                                  WHEN 11 THEN 'Payroll'
                                END AS vcReferenceType ,
                                dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
                                CASE CH.tintReferenceType
                                  WHEN 1 THEN ''
                                  WHEN 8
                                  THEN CASE WHEN ISNULL(BPD.numBillID, 0) > 0
                                            THEN 'Bill-'
                                                 + CAST(BPD.numBillID AS VARCHAR(18))
                                            ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
                                       END
                                END AS vcBizDoc ,
                                CASE CH.tintReferenceType
                                  WHEN 1 THEN ''
                                  WHEN 8
                                  THEN CASE WHEN BPD.numOppBizDocsID > 0
                                            THEN CAST(ISNULL(OBD.vcRefOrderNo,
                                                             '') AS VARCHAR(18))
                                            WHEN BPD.numBillID > 0
                                            THEN CAST(ISNULL(BH.vcReference,
                                                             '') AS VARCHAR(500))
                                       END
                                  ELSE ''
                                END AS vcRefOrderNo
                        FROM    dbo.CheckHeader CH
                                LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID
                                                              AND CH.tintReferenceType = 11
                                LEFT JOIN BillPaymentHeader BPH ON BPH.numBillPaymentID = CH.numReferenceID
                                                              AND BPH.numDomainID = CH.numDomainID
                                LEFT JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
                        WHERE   CH.numDomainID = @numDomainID
                                AND ([CH].[numDivisionID] = @numDivisionID OR @numDivisionID = 0)
                                AND ( numCheckHeaderID = @numCheckHeaderID
                                      OR @numCheckHeaderID = 0
                                    )
                                AND ( CH.tintReferenceType = @tintReferenceType
                                      OR @tintReferenceType = 0
                                    )
                                AND ( CH.numReferenceID = @numReferenceID
                                      OR @numReferenceID = 0
                                    )
                                AND CH.numChartAcntId = @numChartAcntId
                                AND ISNULL(bitIsPrint, 0) = 0	
                    END
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
    BEGIN 
        DECLARE @hDocItem INT                                                                                                                                                                
 
        IF @numReturnHeaderID = 0 
            BEGIN             
				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
                   
                       INSERT  INTO [ReturnHeader]([vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
                                  [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
                                  [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
                                  [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment)
                                SELECT  @vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
                                        @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,
                                        @monTotalTax,@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,
										@numParentID,
										(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
													WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
											  ELSE 0
										END)
                    
                        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
                        SELECT  @numReturnHeaderID

                        --Update DepositMaster if Refund UnApplied Payment
                        IF @numDepositIDRef>0 AND @tintReturnType=4
                        BEGIN
							UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
						END
						
						--Update BillPaymentHeader if Refund UnApplied Payment
						IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						BEGIN
							UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
						END
						
                        DECLARE @tintType TINYINT 
                        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
												WHEN @tintReturnType=3 THEN 6
												WHEN @tintReturnType=4 THEN 5 END
												
                        EXEC dbo.USP_UpdateBizDocNameTemplate
								@tintType =  @tintType, --  tinyint
								@numDomainID = @numDomainID, --  numeric(18, 0)
								@RecordID = @numReturnHeaderID --  numeric(18, 0)

                        DECLARE @numRMATempID AS NUMERIC(18, 0)
                        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

                        IF @tintReturnType=1 
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=2
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
						END			    
                        ELSE IF @tintReturnType=3
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=4
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
                            SET @numBizdocTempID=@numRMATempID    
						END
                                               
                        UPDATE  dbo.ReturnHeader
                        SET     numRMATempID = ISNULL(@numRMATempID,0),
                                numBizdocTempID = ISNULL(@numBizdocTempID,0)
                        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
			                --Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

--Bill Address
        IF @numBillAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numBillAddressId
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 0, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = 0, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
    
  --Ship Address
        IF @numShipAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numShipAddressId
 
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 1, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = @numCompanyId, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
            
							IF @tintReturnType=1
							BEGIN    
								IF @numOppId>0
								BEGIN
									INSERT dbo.OpportunityMasterTaxItems 
									(
										numReturnHeaderID,
										numTaxItemID,
										fltPercentage,
										tintTaxType
									) 
									SELECT 
										@numReturnHeaderID,
										numTaxItemID,
										fltPercentage,
										tintTaxType
									FROM 
										OpportunityMasterTaxItems 
									WHERE 
										numOppID=@numOppID
								END
								ELSE
								BEGIN 
															--Insert Tax for Division                       
										INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage,
											tintTaxType
										) 
										SELECT 
											@numReturnHeaderID,
											TI.numTaxItemID,
											TEMPTax.decTaxValue,
											TEMPTax.tintTaxType
										FROM 
											TaxItems TI 
										JOIN 
											DivisionTaxTypes DTT 
										ON 
											TI.numTaxItemID = DTT.numTaxItemID
										OUTER APPLY
										(
											SELECT
												decTaxValue,
												tintTaxType
											FROM
												dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
										) AS TEMPTax
										WHERE 
											DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
										UNION 
										SELECT 
											@numReturnHeaderID,
											0,
											TEMPTax.decTaxValue,
											TEMPTax.tintTaxType
										FROM 
											dbo.DivisionMaster 
										OUTER APPLY
										(
											SELECT
												decTaxValue,
												tintTaxType
											FROM
												dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
										) AS TEMPTax
										WHERE 
											bitNoTax=0 
											AND numDivisionID=@numDivisionID
								END			  
							END
          
          IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                PRINT 1
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour NUMERIC, numUnitHourReceived NUMERIC, monPrice MONEY, monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT)

                EXEC sp_xml_removedocument @hDocItem 
                
                        INSERT  INTO [ReturnItems]
                                (
                                  [numReturnHeaderID],
                                  [numItemCode],
                                  [numUnitHour],
                                  [numUnitHourReceived],
                                  [monPrice],
                                  [monTotAmount],
                                  [vcItemDesc],
                                  [numWareHouseItemID],
                                  [vcModelID],
                                  [vcManufacturer],
                                  [numUOMId],numOppItemID,
								  bitDiscountType,fltDiscount
                                )
                                SELECT  @numReturnHeaderID,
                                        numItemCode,
                                        numUnitHour,
                                        0,
                                        monPrice,
                                        monTotAmount,
                                        vcItemDesc,
                                        NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
                                        vcModelID,
                                        vcManufacturer,
                                        numUOMId,numOppItemID,
										bitDiscountType,fltDiscount
                                FROM    #temp
                                WHERE   numReturnItemID = 0
					
                     
                DROP TABLE #temp
   
						IF @tintReturnType=1
						BEGIN               
							IF @numOppId>0
								BEGIN
									INSERT INTO dbo.OpportunityItemsTaxItems (
										numReturnHeaderID,numReturnItemID,numTaxItemID
									)  SELECT @numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID 
									FROM dbo.ReturnItems OI JOIN dbo.OpportunityItemsTaxItems IT ON OI.numOppItemID=IT.numOppItemID 
									WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.numOppId=@numOppId
								END
								ELSE
								BEGIN
										--Delete Tax for ReturnItems if item deleted 
						--DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID NOT IN (SELECT numReturnItemID FROM ReturnItems WHERE numReturnHeaderID=@numReturnHeaderID)

						--Insert Tax for ReturnItems
						INSERT INTO dbo.OpportunityItemsTaxItems (
							numReturnHeaderID,
							numReturnItemID,
							numTaxItemID
						) SELECT @numReturnHeaderID,OI.numReturnItemID,TI.numTaxItemID FROM dbo.ReturnItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
						TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.bitApplicable=1  
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
						UNION
						  select @numReturnHeaderID,OI.numReturnItemID,0 FROM dbo.ReturnItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
						   WHERE OI.numReturnHeaderID=@numReturnHeaderID  AND I.bitTaxable=1
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
							END
						END
            END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
            END    
        ELSE IF @numReturnHeaderID <> 0 
                BEGIN    
                   IF ISNULL(@tintMode, 0) = 0
                   BEGIN
				   			UPDATE  [ReturnHeader]
							SET     [vcRMA] = @vcRMA,
									[numReturnReason] = @numReturnReason,
									[numReturnStatus] = @numReturnStatus,
									[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
									[monTotalDiscount] = @monTotalDiscount,
									[tintReceiveType] = @tintReceiveType,
									[vcComments] = @vcComments,
									[numModifiedBy] = @numUserCntID,
									[dtModifiedDate] = GETUTCDATE()
							WHERE   [numDomainId] = @numDomainId
									AND [numReturnHeaderID] = @numReturnHeaderID
				   END                 
                   ELSE IF ISNULL(@tintMode, 0) = 1 
                   BEGIN
                   	UPDATE  ReturnHeader
								SET     numReturnStatus = @numReturnStatus
								WHERE   numReturnHeaderID = @numReturnHeaderID
							
				   	IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
						SELECT  *
						INTO    #temp1
						FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numUnitHourReceived NUMERIC, numWareHouseItemID NUMERIC)
						EXEC sp_xml_removedocument @hDocItem 

								
								UPDATE  [ReturnItems]
								SET     [numUnitHourReceived] = X.numUnitHourReceived,
										[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
								FROM    #temp1 AS X
								WHERE   X.numReturnItemID = ReturnItems.numReturnItemID
										AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
						DROP TABLE #temp1
				END 
			END
       END            
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @intNoOfPartners AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(100),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
    @tintLogin TINYINT,
    @intNoofPartialSubscribed AS INTEGER,
    @intNoofMinimalSubscribed AS INTEGER,
	@dtEmailStartDate DATETIME,
	@dtEmailEndDate DATETIME,
	@intNoOfEmail INT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT
AS 
    IF @numSubscriberID = 0 
        BEGIN                                 
            IF NOT EXISTS ( SELECT  *
                            FROM    Subscribers
                            WHERE   numDivisionID = @numDivisionID
                                    AND numDomainID = @numDomainID ) 
                BEGIN              
                    SET @bitExists = 0                              
                    DECLARE @vcCompanyName AS VARCHAR(100)                             
                    DECLARE @numCompanyID AS NUMERIC(9)                                   
                    SELECT  @vcCompanyName = vcCompanyName,
                            @numCompanyID = C.numCompanyID
                    FROM    CompanyInfo C
                            JOIN DivisionMaster D ON D.numCompanyID = C.numCompanyID
                    WHERE   D.numDivisionID = @numDivisionID                                    
                                
                    DECLARE @numNewCompanyID AS NUMERIC(9)                                   
                    DECLARE @numNewDivisionID AS NUMERIC(9)                                    
                    DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
                    INSERT  INTO Domain
                            (
                              vcDomainName,
                              vcDomainDesc,
                              numDivisionID,
                              numAdminID,
                              tintLogin,tintDecimalPoints,tintChrForComSearch,tintChrForItemSearch, bitIsShowBalance,tintComAppliesTo,tintCommissionType, charUnitSystem
                            )
                    VALUES  (
                              @vcCompanyName,
                              @vcCompanyName,
                              @numDivisionID,
                              @numAdminContactID,
                              @tintLogin,2,1,1,0,3,1,'E'
                            )                                    
                    SET @numTargetDomainID = @@identity                                       
                                       
                    INSERT  INTO CompanyInfo
                            (
                              vcCompanyName,
                              numCompanyType,
                              numCompanyRating,
                              numCompanyIndustry,
                              numCompanyCredit,
                              vcWebSite,
                              vcWebLabel1,
                              vcWebLink1,
                              vcWebLabel2,
                              vcWebLink2,
                              vcWebLabel3,
                              vcWebLink3,
                              vcWeblabel4,
                              vcWebLink4,
                              numAnnualRevID,
                              numNoOfEmployeesId,
                              vcHow,
                              vcProfile,
                              bitPublicFlag,
                              numDomainID
                            )
                            SELECT  vcCompanyName,
                                    93,--i.e employer --numCompanyType,
                                    numCompanyRating,
                                    numCompanyIndustry,
                                    numCompanyCredit,
                                    vcWebSite,
                                    vcWebLabel1,
                                    vcWebLink1,
                                    vcWebLabel2,
                                    vcWebLink2,
                                    vcWebLabel3,
                                    vcWebLink3,
                                    vcWeblabel4,
                                    vcWebLink4,
                                    numAnnualRevID,
                                    numNoOfEmployeesId,
                                    vcHow,
                                    vcProfile,
                                    bitPublicFlag,
                                    numDomainID
                            FROM    CompanyInfo
                            WHERE   numCompanyID = @numCompanyID                            
                    SET @numNewCompanyID = @@identity                            
                                     
                    INSERT  INTO DivisionMaster
                            (
                              numCompanyID,
                              vcDivisionName,
                              numGrpId,
                              bitPublicFlag,
                              tintCRMType,
                              numStatusID,
                              tintBillingTerms,
                              numBillingDays,
                              tintInterestType,
                              fltInterest,
                              vcComPhone,
                              vcComFax,
                              numDomainID
                            )
                            SELECT  @numNewCompanyID,
                                    vcDivisionName,
                                    numGrpId,
                                    bitPublicFlag,
                                    2,
                                    numStatusID,
                                    tintBillingTerms,
                                    numBillingDays,
                                    tintInterestType,
                                    fltInterest,
                                    vcComPhone,
                                    vcComFax,
                                    numDomainID
                            FROM    DivisionMaster
                            WHERE   numDivisionID = @numDivisionID                     
                    SET @numNewDivisionID = @@identity     
                                            
                            INSERT INTO dbo.AddressDetails (
								vcAddressName,
								vcStreet,
								vcCity,
								vcPostalCode,
								numState,
								numCountry,
								bitIsPrimary,
								tintAddressOf,
								tintAddressType,
								numRecordID,
								numDomainID
							) SELECT 
									 vcAddressName,
									 vcStreet,
									 vcCity,
									 vcPostalCode,
									 numState,
									 numCountry,
									 bitIsPrimary,
									 tintAddressOf,
									 tintAddressType,
									 @numNewDivisionID,
									 numDomainID FROM dbo.AddressDetails WHERE numRecordID= @numDivisionID AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = @@identity                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              @@identity,
                              @numNewContactID,
                              1
                            )                  
                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'System Administrator'
                       ) = 0 
                        BEGIN                       
                         
                            DECLARE @numGroupId1 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'System Administrator',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId1 = @@identity                        
                            SET @TargetGroupId = @numGroupId1                     
                       
INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId1,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster
--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId1,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


--Added by Anoop for Dashboard +++++++++++++++++++

                            IF ( SELECT COUNT(*)
                                 FROM   customreport
                                 WHERE  numDomainId = @numTargetDomainID
                               ) = 0 
                                BEGIN                  
                                    EXEC CreateCustomReportsForNewDomain @numTargetDomainID,
                                        @numNewContactID              
                                END 

                            DECLARE @numGroupId2 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Executive',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 





                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Sales Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 






                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Warehouse Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                           
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           


                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 258
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Executive'
                                                            )

                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 259
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Sales Staff'
                                                            )


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 260
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Warehouse Staff'
                                                            )



------ Added by Anoop for Dashboard-----------       
                        END                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'Default Extranet'
                       ) = 0 
                        BEGIN                       
                                             
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Default Extranet',
                                      @numTargetDomainID,
                                      2,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity                        
                      
                       
                            INSERT  INTO GroupAuthorization
                                    (
                                      numGroupID,
                                      numModuleID,
                                      numPageID,
                                      intExportAllowed,
                                      intPrintAllowed,
                                      intViewAllowed,
                                      intAddAllowed,
                                      intUpdateAllowed,
                                      intDeleteAllowed,
                                      numDomainID                      
                       
                                    )
                                    SELECT  @numGroupId2,
                                            x.numModuleID,
                                            x.numPageID,
                                            x.intExportAllowed,
                                            x.intPrintAllowed,
                                            x.intViewAllowed,
                                            x.intAddAllowed,
                                            x.intUpdateAllowed,
                                            x.intDeleteAllowed,
                                            @numTargetDomainID
                                    FROM    ( SELECT    *
                                              FROM      GroupAuthorization
                                              WHERE     numGroupid = 2
                                            ) x           
        
                          
--MainTab From TabMaster for Customer:46
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            46,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Customer:46
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            46,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


                          
--MainTab From TabMaster  for Employer:93
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                           93,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Employer:93
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            93,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 
      
                   
                        END                      
----                    IF ( SELECT COUNT(*)
----                         FROM   AuthenticationGroupMaster
----                         WHERE  numDomainId = @numTargetDomainID
----                                AND vcGroupName = 'Default PRM'
----                       ) = 0 
----                        BEGIN                       
----                         
----                            DECLARE @numGroupId3 AS NUMERIC(9)                      
----                            INSERT  INTO AuthenticationGroupMaster
----                                    (
----                                      vcGroupName,
----                                      numDomainID,
----                                      tintGroupType,
----                                      bitConsFlag 
----                                    )
----                            VALUES  (
----                                      'Default PRM',
----                                      @numTargetDomainID,
----                                      3,
----                                      0
----                                    )                      
----                            SET @numGroupId3 = @@identity                        
----                      
----                       
----                            INSERT  INTO GroupAuthorization
----                                    (
----                                      numGroupID,
----                                      numModuleID,
----                                      numPageID,
----                                      intExportAllowed,
----                                      intPrintAllowed,
----                                      intViewAllowed,
----                                      intAddAllowed,
----                                      intUpdateAllowed,
----                                      intDeleteAllowed,
----                                      numDomainID                   
----                   
----                                    )
----                                    SELECT  @numGroupId3,
----                                            x.numModuleID,
----                                            x.numPageID,
----                                            x.intExportAllowed,
----                                            x.intPrintAllowed,
----                                            x.intViewAllowed,
----                                            x.intAddAllowed,
----                                            x.intUpdateAllowed,
----                                            x.intDeleteAllowed,
----                                            @numTargetDomainID
----                                    FROM    ( SELECT    *
----                                              FROM      GroupAuthorization
----                                              WHERE     numGroupid = 46
----                                            ) x             
----        
------                            INSERT  INTO GroupTabDetails
------                                    (
------                                      numGroupId,
------                                      numTabId,
------                                      numRelationShip,
------                                      bitallowed,
------                                      numOrder
------                                    )
------                                    SELECT  @numGroupId3,
------                                            x.numTabId,
------                                            x.numRelationShip,
------                                            x.bitallowed,
------                                            x.numOrder
------                                    FROM    ( SELECT    *
------                                              FROM      GroupTabDetails
------                                              WHERE     numGroupid = 46
------                                            ) x         
----                 
----                        END                      
                    
       

      
--- Set default Chart of Accounts 
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = @@identity                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = @@identity                        
                      
                      
                              
                END                                
            ELSE 
                BEGIN                        
                    SELECT  @numSubscriberID = numSubscriberID
                    FROM    Subscribers
                    WHERE   numDivisionID = @numDivisionID
                            AND numDomainID = @numDomainID                                
                    UPDATE  Subscribers
                    SET     bitDeleted = 0
                    WHERE   numSubscriberID = @numSubscriberID                                
                END                                 
                                   
                     
        END                                    
    ELSE 
        BEGIN                                    
                                  
            DECLARE @bitOldStatus AS BIT                                  
            SET @bitExists = 1                                 
            SELECT  @bitOldStatus = bitActive,
                    @numTargetDomainID = numTargetDomainID
            FROM    Subscribers
            WHERE   numSubscriberID = @numSubscriberID                                    
                                  
            IF ( @bitOldStatus = 1
                 AND @bitActive = 0
               ) 
                UPDATE  Subscribers
                SET     dtSuspendedDate = GETUTCDATE()
                WHERE   numSubscriberID = @numSubscriberID                                  
                                  
            UPDATE  Subscribers
            SET     intNoofUsersSubscribed = @intNoofUsersSubscribed,
                    intNoOfPartners = @intNoOfPartners,
                    dtSubStartDate = @dtSubStartDate,
                    dtSubEndDate = @dtSubEndDate,
                    bitTrial = @bitTrial,
                    numAdminContactID = @numAdminContactID,
                    bitActive = @bitActive,
                    vcSuspendedReason = @vcSuspendedReason,
                    numModifiedBy = @numUserContactID,
                    dtModifiedDate = GETUTCDATE(),
                    intNoofPartialSubscribed = @intNoofPartialSubscribed,
                    intNoofMinimalSubscribed = @intNoofMinimalSubscribed,
					dtEmailStartDate= @dtEmailStartDate,
					dtEmailEndDate= @dtEmailEndDate,
					intNoOfEmail=@intNoOfEmail,
					bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
					bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin
            WHERE   numSubscriberID = @numSubscriberID                 
                       
                         UPDATE  Domain
                    SET     tintLogin = @tintLogin,
							numAdminID = @numAdminContactID
                    WHERE   numdomainID = @numTargetDomainID   
                           
            DECLARE @hDoc1 INT                                                              
            EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
            INSERT  INTO UserMaster
                    (
                      vcUserName,
                      vcMailNickName,
                      vcUserDesc,
                      numGroupID,
                      numDomainID,
                      numCreatedBy,
                      bintCreatedDate,
                      numModifiedBy,
                      bintModifiedDate,
                      bitActivateFlag,
                      numUserDetailId,
                      vcEmailID,
                      vcPassword
                    )
                    SELECT  UserName,
                            UserName,
                            UserName,
                            0,
                            @numTargetDomainID,
                            @numUserContactID,
                            GETUTCDATE(),
                            @numUserContactID,
                            GETUTCDATE(),
                            Active,
                            numContactID,
                            Email,
                            vcPassword
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                                        WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                            ) X                                
                
                              
            UPDATE  UserMaster
            SET     vcUserName = X.UserName,
                    numModifiedBy = @numUserContactID,
                    bintModifiedDate = GETUTCDATE(),
                    vcEmailID = X.Email,
                    vcPassword = X.vcPassword,
                    bitActivateFlag = Active
            FROM    ( SELECT    numUserID AS UserID,
                                numContactID,
                                Email,
                                vcPassword,
                                UserName,
                                Active
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                                WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                    ) X
            WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
            EXEC sp_xml_removedocument @hDoc1                               
                                   
        END                                    
    SELECT  @numSubscriberID
GO
/****** Object:  StoredProcedure [dbo].[usp_ManageTaxDetails]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetaxdetails')
DROP PROCEDURE usp_managetaxdetails
GO
CREATE PROCEDURE [dbo].[usp_ManageTaxDetails]  
@numCountry as numeric(9),  
@numState as numeric(9),  
@decTax as decimal(10,4),    
@numTaxId as numeric(10),  
@numDomainId  as numeric(9),  
@tintMode as tinyint,
@numTaxItemID as numeric(9),
@vcCity as varchar(100),
@vcZipPostal as varchar(20),
@tintTaxType AS TINYINT  
as   
if @tintMode = 0 --Insert record
begin  
IF @numState = 0
    BEGIN
		SET  @vcCity = ''
		SET  @vcZipPostal = ''  
	END
 if (select count(*) from TaxDetails where numCountryID = @numCountry and numStateID = @numState and numDomainId = @numDomainId and numTaxItemID=@numTaxItemID and vcCity=@vcCity and vcZipPostal=@vcZipPostal) =0  
 begin  
    
    
    INSERT INTO TaxDetails  ( numTaxItemID, [numCountryID]  ,[numStateID]  ,[decTaxPercentage]  ,[numDomainId],vcCity,vcZipPostal,tintTaxType)  
     VALUES  ( isnull(@numTaxItemID,0), @numCountry  ,@numState  ,@decTax  ,@numDomainId,@vcCity,@vcZipPostal,@tintTaxType)  

     SELECT  SCOPE_IDENTITY();   
 end  
 else  
  select 0   
  
end  
else If @tintMode = 1 --Delete particular record
Begin  
 delete TaxDetails WHERE numTaxId = @numTaxId and [numDomainId]=@numDomainId  
 SELECT @numTaxId  
end
else If @tintMode = 2 --Delete All records
Begin  
 delete TaxDetails WHERE [numDomainId]=@numDomainId  
 SELECT 0 
end
GO

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUOMConversion')
DROP PROCEDURE USP_ManageUOMConversion
GO
CREATE PROCEDURE [dbo].[USP_ManageUOMConversion]
	@numDomainID NUMERIC(9),
	@vcUnitConversion NText,
	@bitEnableItemLevelUOM BIT 
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @iDoc int   

	UPDATE Domain SET bitEnableItemLevelUOM = @bitEnableItemLevelUOM WHERE numDomainId = @numDomainID
 
	DELETE FROM UOMConversion WHERE numDomainID=@numDomainID

	EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcUnitConversion

	INSERT INTO UOMConversion 
	(
		[numUOM1],[decConv1],[numUOM2],[decConv2],[numDomainID]
	)             
	SELECT 
		numUOM1,decConv1,numUOM2,decConv2,@numDomainID
    FROM 
		OPENXML(@iDoc, '/NewDataSet/UnitConversion', 2)                        
	WITH                        
	(                
		numUOM1 NUMERIC(9),                  
		decConv1 DECIMAL(18,5),                  
		numUOM2 NUMERIC(9),                
		decConv2 DECIMAL(18,5)               
	)

	EXECUTE sp_xml_removedocument @iDoc 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END 
	



GO
/****** Object:  StoredProcedure [dbo].[USP_MangeAutoPOGeneration]    Script Date: 03/05/2010 20:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mangeautopogeneration')
DROP PROCEDURE usp_mangeautopogeneration
GO
CREATE PROCEDURE [dbo].[USP_MangeAutoPOGeneration]
@numParentOppID numeric(9),
@numOppBizDocID numeric(9),
@numItemCode numeric(9),
@numDomainID numeric(9),
@numOppBizDocItemID numeric(9),
@monDealAmount money,
@monPaidAmount money

AS

BEGIN


DECLARE @numNewOppId numeric(9)
DECLARE @numContactId numeric(9)
DECLARE @numDivisionId numeric(9)
DECLARE @numUserCntID NUMERIC(9)
DECLARE @vcCompanyName varchar(250)
DECLARE @monCost money
DECLARE @CloseDate datetime
DECLARE @vcPOppName VARCHAR(500)
DECLARE @numBizDocId numeric(9)
DECLARE @numCurrencyCode numeric
DECLARE @TotalAmount MONEY
DECLARE @numOppBizDocsId NUMERIC(9)
DECLARE @numOldBizDocId NUMERIC(9)

select @numCurrencyCode=numCurrencyID from Domain where numDomainID=@numDomainID;

select @CloseDate=getdate()

--delete from  temp_AutoOrder;



--insert into temp_AutoOrder
--select
--@numParentOppID ,
--@numOppBizDocID ,
--@numItemCode ,
--@numDomainID ,
--@numOppBizDocItemID ,
--@numNewOPpId ,
--@numDivisionID

------ FOR THE RULE WHERE BIZDOC PAID FULLY

set @numDivisionId=0
--if @monDealAmount=@monPaidAmount
	
				
			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID and f.btActive=1 --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=1 and b.monAMountPaid= @monDealAmount and 
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

					insert into Temp_Cheking select 'Insert Amount Match: Div ' + cast(@numDivisionId as varchar(50));

					set @numNewOppId=0;

					Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
								OpportunityMaster OM on OM.numOppId= OL.numChildOppID
					 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
						   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;
						   
					--update temp_autoOrder set numDivisionID =@numDivisionId;

					set @vcPOppName=''
					if @numDivisionId>0
						begin

							SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());

							if @numNewOppId=0
								begin
									EXEC USP_OppManage
									@numNewOppId OUTPUT,
									@numContactId =@numContactId,                                                                          
									@numDivisionId =@numDivisionId,                                                                          
									@tintSource=0,                                                                         
									@vcPOppName =@vcPOppName,                                                                          
									--@Comments ='',                                                                          
									--@bitPublicFlag =0,                                                                          
									@numUserCntID =@numUserCntID,                                                                                 
									--@monPAmount  =0,                                                 
									--@numAssignedTo =0,                                                                                                        
									@numDomainId =@numDomainID,                                                                                                                                                                                   
									--@strItems =null,                                                                          
									--@strMilestone =null,                                                                          
									@dtEstimatedCloseDate =@CloseDate,                                                                          
									@CampaignID =0,
									@lngPConclAnalysis =0,
									@tintOppType =2,                                                                                                                                             
									@tintActive =1,                                                              
									@numSalesOrPurType =0,             
									@numRecurringId=0,
									@numCurrencyID =@numCurrencyCode,
									--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
									@numStatus =0,
									@vcRefOrderNo =''
									--@numBillAddressId =0,
									--@numShipAddressId =0 


									--update temp_autoOrder set numNewOPpId =@numNewOppId;

									insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
									(@numParentOppID,@numNewOppId,@numOppBizDocID)

									insert into Temp_Cheking select 'Insert Amount Match: New Opp ' + cast(@numNewOppId as varchar(50));
								end
										if @numNewOppId>0 
											begin
												
												if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
													begin	
														insert into Temp_Cheking select 'Insert Amount Match: BizDoc Opp Items - ' + cast(@numOppBizDocItemID as varchar(50));														
														insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
														select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
														from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
													end
											end
								
							
							end

------------------- END OF AMOUTN NOT MATCH

--------------- INSERTING INTO BIZDOC AND BIZDOC ITEMS-----------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;						
						end	
					end
				end
------------------------------------- END OF BIZDOC ITEM INSERT -------------------
------------------------------------- START AMOUNT NOT MATCH -------------------
			insert into Temp_Cheking select 'Insert Amount Not Match'
			set @numDivisionId=0

			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID and f.btActive=1--and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=0 and
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

			insert into Temp_Cheking select 'Insert Amount Not Match ' + cast(@numDivisionId as varchar(100));

			set @numNewOppId=0;

			Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
						OpportunityMaster OM on OM.numOppId= OL.numChildOppID
			 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
				   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;


			


			set @vcPOppName=''
			if @numDivisionId>0
				begin
					SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());
					if @numNewOppId=0
						begin
							EXEC USP_OppManage
							@numNewOppId OUTPUT,
							@numContactId =@numContactId,                                                                          
							@numDivisionId =@numDivisionId,                                                                          
							@tintSource=0,                                                                         
							@vcPOppName =@vcPOppName,                                                                          
							--@Comments ='',                                                                          
							--@bitPublicFlag =0,                                                                          
							@numUserCntID =@numUserCntID,                                                                                 
							--@monPAmount  =0,                                                 
							--@numAssignedTo =0,                                                                                                        
							@numDomainId =@numDomainID,                                                                                                                                                                                   
							--@strItems =null,                                                                          
							--@strMilestone =null,                                                                          
							@dtEstimatedCloseDate =@CloseDate,                                                                          
							@CampaignID =0,
							@lngPConclAnalysis =0,
							@tintOppType =2,                                                                                                                                             
							@tintActive =1,                                                              
							@numSalesOrPurType =0,             
							@numRecurringId=0,
							@numCurrencyID =@numCurrencyCode,
							--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
							@numStatus =0,
							@vcRefOrderNo =''
							--@numBillAddressId =0,
							--@numShipAddressId =0  
							
							--update temp_autoOrder set numNewOPpId =@numNewOppId;
							
							insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
							(@numParentOppID,@numNewOppId,@numOppBizDocID)
						end
							if @numNewOppId>0 
									begin
									if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
										begin															
											insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
											select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
											from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
										end
								end
				end
------------------- END OF AMOUNT NOT MATCH------------------------------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
-------------------------------------------
---======================================================

END -- THE FINAL END

GO
/****** Object:  StoredProcedure [dbo].[USP_MangeAutoPOGenerationRule2]    Script Date: 03/05/2010 20:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mangeautopogenerationrule2')
DROP PROCEDURE usp_mangeautopogenerationrule2
GO
CREATE PROCEDURE [dbo].[USP_MangeAutoPOGenerationRule2]
@numParentOppID numeric(9),
@numOppBizDocID numeric(9),
@numItemCode numeric(9),
@numDomainID numeric(9),
@numOppBizDocItemID numeric(9),
@monDealAmount money,
@monPaidAmount money

AS

BEGIN


DECLARE @numNewOppId numeric(9)
DECLARE @numContactId numeric(9)
DECLARE @numDivisionId numeric(9)
DECLARE @numUserCntID NUMERIC(9)
DECLARE @vcCompanyName varchar(250)
DECLARE @monCost money
DECLARE @CloseDate datetime
DECLARE @vcPOppName VARCHAR(500)
DECLARE @numBizDocId numeric(9)
DECLARE @numCurrencyCode numeric
DECLARE @TotalAmount MONEY
DECLARE @numOppBizDocsId NUMERIC(9)
DECLARE @numOldBizDocId NUMERIC(9)

select @numCurrencyCode=numCurrencyID from Domain where numDomainID=@numDomainID;

select @CloseDate=getdate()

--delete from  temp_AutoOrder;



--insert into temp_AutoOrder
--select
--@numParentOppID ,
--@numOppBizDocID ,
--@numItemCode ,
--@numDomainID ,
--@numOppBizDocItemID ,
--@numNewOPpId ,
--@numDivisionID

------ FOR THE RULE WHERE BIZDOC PAID FULLY

set @numDivisionId=0
--if @monDealAmount=@monPaidAmount
	

				
			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRuleDetails F on f.numItemClassId=D.numItemClassification and  f.btActive=1 and 
			(Case D.charItemType when 'P' then 2 when 'N' then 3 when 'S' then	1 end)= f.numItemTypeId 
			inner join 
			OrderAutoRule OA on OA.numDomainID=A.numDomainID and OA.numRuleId=F.numRuleId
			--and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and 
			F.btFullPaid=1 and b.monAMountPaid= @monDealAmount and 
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

					insert into Temp_Cheking select 'Insert Amount Match: Div ' + cast(@numDivisionId as varchar(50));

					set @numNewOppId=0;

					Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
								OpportunityMaster OM on OM.numOppId= OL.numChildOppID
					 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
						   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;
						   
					--update temp_autoOrder set numDivisionID =@numDivisionId;

					set @vcPOppName=''
					if @numDivisionId>0
						begin

							SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());

							if @numNewOppId=0
								begin
									EXEC USP_OppManage
									@numNewOppId OUTPUT,
									@numContactId =@numContactId,                                                                          
									@numDivisionId =@numDivisionId,                                                                          
									@tintSource=0,                                                                         
									@vcPOppName =@vcPOppName,                                                                          
									--@Comments ='',                                                                          
									--@bitPublicFlag =0,                                                                          
									@numUserCntID =@numUserCntID,                                                                                 
									--@monPAmount  =0,                                                 
									--@numAssignedTo =0,                                                                                                        
									@numDomainId =@numDomainID,                                                                                                                                                                                   
									--@strItems =null,                                                                          
									--@strMilestone =null,                                                                          
									@dtEstimatedCloseDate =@CloseDate,                                                                          
									@CampaignID =0,
									@lngPConclAnalysis =0,
									@tintOppType =2,                                                                                                                                             
									@tintActive =1,                                                              
									@numSalesOrPurType =0,             
									@numRecurringId=0,
									@numCurrencyID =@numCurrencyCode,
									--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
									@numStatus =0,
									@vcRefOrderNo =''
									--@numBillAddressId =0,
									--@numShipAddressId =0  


									--update temp_autoOrder set numNewOPpId =@numNewOppId;

									insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
									(@numParentOppID,@numNewOppId,@numOppBizDocID)

									insert into Temp_Cheking select 'Insert Amount Match: New Opp ' + cast(@numNewOppId as varchar(50));
								end
										if @numNewOppId>0 
											begin
												
												if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
													begin	
														insert into Temp_Cheking select 'Insert Amount Match: BizDoc Opp Items - ' + cast(@numOppBizDocItemID as varchar(50));														
														insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
														select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
														from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
													end
											end
								
							
							end

------------------- END OF AMOUTN NOT MATCH

--------------- INSERTING INTO BIZDOC AND BIZDOC ITEMS-----------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
------------------------------------- END OF BIZDOC ITEM INSERT -------------------
------------------------------------- START AMOUNT NOT MATCH -------------------
			insert into Temp_Cheking select 'Insert Amount Not Match'
			set @numDivisionId=0

			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRuleDetails F on f.numItemClassId=D.numItemClassification and   f.btActive=1 and
			(Case D.charItemType when 'P' then 2 when 'N' then 3 when 'S' then	1 end)= f.numItemTypeId 
			inner join 
			OrderAutoRule OA on OA.numDomainID=A.numDomainID and OA.numRuleId=F.numRuleId
			--and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID  and 
			F.btFullPaid=0 and
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

			insert into Temp_Cheking select 'Insert Amount Not Match ' + cast(@numDivisionId as varchar(100));

			set @numNewOppId=0;

			Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
						OpportunityMaster OM on OM.numOppId= OL.numChildOppID
			 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
				   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;


			


			set @vcPOppName=''
			if @numDivisionId>0
				begin
					SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());
					if @numNewOppId=0
						begin
							EXEC USP_OppManage
							@numNewOppId OUTPUT,
							@numContactId =@numContactId,                                                                          
							@numDivisionId =@numDivisionId,                                                                          
							@tintSource=0,                                                                         
							@vcPOppName =@vcPOppName,                                                                          
							--@Comments ='',                                                                          
							--@bitPublicFlag =0,                                                                          
							@numUserCntID =@numUserCntID,                                                                                 
							--@monPAmount  =0,                                                 
							--@numAssignedTo =0,                                                                                                        
							@numDomainId =@numDomainID,                                                                                                                                                                                   
							--@strItems =null,                                                                          
							--@strMilestone =null,                                                                          
							@dtEstimatedCloseDate =@CloseDate,                                                                          
							@CampaignID =0,
							@lngPConclAnalysis =0,
							@tintOppType =2,                                                                                                                                             
							@tintActive =1,                                                              
							@numSalesOrPurType =0,             
							@numRecurringId=0,
							@numCurrencyID =@numCurrencyCode,
							--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
							@numStatus =0,
							@vcRefOrderNo =''
							--@numBillAddressId =0,
							--@numShipAddressId =0   
							
							--update temp_autoOrder set numNewOPpId =@numNewOppId;
							
							insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
							(@numParentOppID,@numNewOppId,@numOppBizDocID)
						end
							if @numNewOppId>0 
									begin
									if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
										begin															
											insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
											select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
											from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
										end
								end
				end
------------------- END OF AMOUNT NOT MATCH------------------------------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
-------------------------------------------
---======================================================

END -- THE FINAL END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MirrorOPPBizDocItems' ) 
    DROP PROCEDURE USP_MirrorOPPBizDocItems
GO
CREATE PROCEDURE [dbo].[USP_MirrorOPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppStatus AS TINYINT   
    
    DECLARE @tintOppType AS TINYINT   
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
      SET @numBizDocTempID = 0
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]  ,
    @tintType = tintOppType,@tintOppStatus=tintOppStatus,
            @tintOppType = tintOppType   ,@numBizDocTempID = ISNULL(numOppBizDocTempID, 0)      
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId    
                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
                                                   
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
                                          
    
       IF @tintOppType=1 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
       ELSE IF @tintOppType=1 AND @tintOppStatus=1
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=1
	 		SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
                                                                              
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(500), Opp.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, Opp.numUnitHour)
                             ELSE CONVERT(VARCHAR(500), Opp.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * Opp.numUnitHour AS numUnitHour,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * Opp.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), Opp.monTotAmount)) Amount,
                        Opp.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(Opp.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
                          - Opp.monTotAmount ) AS DiscAmt,
                        '' vcNotes,
                        '' vcTrackingNo,
                        '' vcShippingMethod,
                        0 monShipCost,
                        NULL dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        NULL dtRentalStartDate,
                        NULL dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass
              FROM      OpportunityItems opp
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     opp.numOppId = @numOppId
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount)'
    ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType AND bitDefault=1 AND numDomainID=@numDomainID AND vcLookBackTableName = (CASE @numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END      

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
              --                              AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														--ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]						
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 1 --add Sales tax
	
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
    ELSE IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE /*IF @tintOppType = 1*/ 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount)'
    --ELSE 
    --            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            /*IF @tintOppType = 1 */
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount)'
            --ELSE 
            --    SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc,                                    
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            OBD.vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                    ELSE i.vcSKU
            END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                    ELSE i.numBarCodeId
            END vcBarcode,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
       

    ) X

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numUnitHourOrig * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,                                    
            t2.numQty AS numUnitHour,
            (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),(t2.numQty * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            (t2.numQty * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode ELSE I.numBarCodeId END vcBarcode,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
    END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY 
		tintRow
END
GO


/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as                        
                                          
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint

SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  JOIN divisionmaster div ON div.numDivisionId=oppmst.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
			  ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile,
		0 AS intInvoiceForDiferredBizDoc	 
from                         
 ReturnHeader RH                        
  JOIN divisionmaster div ON div.numDivisionId=RH.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = RH.numContactId
where RH.numOppId=@numOppId
)X         
                        
END




GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
--			 case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0))=1 
--				  then ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0) 
--				  else 0 
--			 end AS numBillingDaysName,
--			 CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))) = 1
-- 				   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))
--				   ELSE 0
--			  END AS numBillingDaysName,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,0) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null,                                                                
 @numDomainID as numeric(9)=0 ,                                                      
 @numUserCntID as numeric(9)=0                                                                                                                                               
)                                                                                                                                                
                                                                                                                                            
as                           
begin

declare @numCurrencyID as numeric(9)
declare @fltExchangeRate as float

declare @fltCurrentExchangeRate as float
declare @tintType as tinyint                                                          
declare @vcPOppName as VARCHAR(100)                                                          
DECLARE @bitPPVariance AS BIT
DECLARE @numDivisionID AS NUMERIC(9)

select  @numCurrencyID=numCurrencyID, @fltExchangeRate=fltExchangeRate,@tintType=tintOppType,@vcPOppName=vcPOppName,@bitPPVariance=ISNULL(bitPPVariance,0),@numDivisionID=numDivisionID from  OpportunityMaster
where numOppID=@numOppId 

DECLARE @vcBaseCurrency AS VARCHAR(100);SET  @vcBaseCurrency=''
DECLARE @bitAutolinkUnappliedPayment AS BIT 
SELECT @vcBaseCurrency=ISNULL(C.varCurrSymbol,''),@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) FROM  dbo.Domain D LEFT JOIN Currency C ON D.numCurrencyID=C.numCurrencyID 
WHERE D.numDomainID=@numDomainID

DECLARE @vcForeignCurrency AS VARCHAR(100) ;SET  @vcForeignCurrency=''
SELECT @vcForeignCurrency=ISNULL(C.varCurrSymbol,'') FROM  Currency C WHERE numCurrencyID=@numCurrencyID

DECLARE @fltExchangeRateBizDoc AS FLOAT 
declare @numBizDocId as numeric(9)                                                           

DECLARE @vcBizDocID AS VARCHAR(100)
select @numBizDocId=numBizDocId,@fltExchangeRateBizDoc=fltExchangeRateBizDoc,@vcBizDocID=vcBizDocID  from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                                                         

if @numCurrencyID>0 set @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
else set @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

declare @strSQL as varchar(8000)                                                          
declare @strSQLCusFields varchar(1000)                                                          
declare @strSQLEmpFlds varchar(500)                                                          
set @strSQLCusFields=''                                                          
set @strSQLEmpFlds=''                                                          
declare @intRowNum as int                                                          
declare @numFldID as varchar(15)                                                          
declare @vcFldname as varchar(50)                                                                                                                                          
                                                                
if @tintType=1 set @tintType=7                                                                
else set @tintType=8                                                                                                                      
select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                       
where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId and bitCustom=1 order by tintRow                                                          
 while @intRowNum>0                                               
 begin                                                          
  set @strSQLCusFields=@strSQLCusFields+',  dbo.GetCustFldValueBizdoc('+@numFldID+','+convert(varchar(2),@tintType)+',opp.numoppitemtCode) as ['+ @vcFldname+']'                                                     
  set @strSQLEmpFlds=@strSQLEmpFlds+',''-'' as ['+ @vcFldname+']'         
                                                 
  select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                                               
  where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId                                                          
  and bitCustom=1 and tintRow>@intRowNum order by tintRow                                                          
                                                            
  if @@rowcount=0 set @intRowNum=0                                                          
                                                           
 end                                                           
                                                       
       
         
select I.[vcItemName] as Item,        
charitemType as type,        
OBI.vcitemdesc as [desc],        
OBI.numUnitHour as Unit,        
OBI.monPrice as Price,        
OBI.monTotAmount as Amount,(opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour AS  ItemTotalAmount,       
isnull(monListPrice,0) as listPrice,convert(varchar,i.numItemCode) as ItemCode,numoppitemtCode,        
L.vcdata as vcItemClassification,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable,monListPrice +' '+@strSQLCusFields,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
,isnull(i.numCOGsChartAcntId,0) as itemCoGs,isnull(i.monAverageCost,'0') as AverageCost ,isnull(OBI.bitDropShip,0) as  bitDropShip ,  (OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt,
NULLIF(Opp.numProjectID,0) numProjectID ,NULLIF(Opp.numClassID,0) numClassID,ISNULL(i.bitKitParent,0) AS bitKitParent,ISNULL(i.bitAssembly,0) AS bitAssembly
from  OpportunityItems opp
join  OpportunityBizDocItems OBI
on OBI.numOppItemID=Opp.numoppitemtCode       
left join item i on opp.numItemCode=i.numItemCode        
left join ListDetails L on i.numItemClassification=L.numListItemID        
where Opp.numOppId=@numOppId  and OBI.numOppBizDocID=@numOppBizDocsId
/*Commented by chintan, Reason: Now Adding Time will be based any Service item insted of 'Time' item
Below code commented While working on Project Mgmt Add Time Function.*/
--   union                                                       
-- select case when numcategory=1 then           
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end            
--when numcategory=2 then           
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end            
--end  as item,         
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                    
-- as Type,                    
--convert(varchar(100),txtDesc) as [Desc],                    
----txtDesc as [Desc],                    
--convert(decimal(10,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                    
--convert(varchar(100),monamount) as Price,                    
--case when isnull(te.numcontractid,0) = 0  
--then  
-- case when numCategory =1 then  
--   isnull(convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)  
--   when numCategory =2 then  
--   isnull(monamount,0)  
--   end  
--else                
--  0  
--end as amount,                    
--0.00 as Tax,0 as listPrice,'' as ItemCode,numCategoryHDRID as numoppitemtCode,'' as vcItemClassification,                    
--'No' as Taxable,0 as monListPrice,0 as itemIncomeAccount,                    
--        
--0 as itemInventoryAsset,'TE' as ItemType        
--,0 as itemCoGs, 0 as AverageCost,0 as bitDropShip,0 as DiscAmt                   
--from timeandexpense TE                     
--left join contractmanagement cm on TE.numcontractid= cm.numcontractid                     
--where                     
--(numtype= 1 or numtype=2)  And      
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)                                                         
--numHours + Case when numMins=15 then .25 when numMins=30 then .50 when numMins=45 then .75 when numMins=0 then .0 end as  unit,                                                
--convert(varchar(100),numRate) as Price,                                                          
--convert(varchar(100),numRate*numHours) + Case when numMins=15 then numRate*.25 when numMins=30 then numRate*.50 when numMins=45 then numRate*.75 when numMins=0 then .0 end  as amount,                     
                                                                                                                  
                                                         
  select  isnull(@numCurrencyID,0) as numCurrencyID, isnull(@fltExchangeRate,1) as fltExchangeRate,isnull(@fltCurrentExchangeRate,1) as CurrfltExchangeRate,isnull(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc ,@vcBaseCurrency AS vcBaseCurrency,@vcForeignCurrency AS vcForeignCurrency,ISNULL(@vcPOppName,'') AS vcPOppName,ISNULL(@vcBizDocID,'') AS vcBizDocID,ISNULL(@bitPPVariance,0) AS bitPPVariance,@numDivisionID AS numDivisionID,@bitAutolinkUnappliedPayment AS bitAutolinkUnappliedPayment                  
     
     
                                                  
End
GO

/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(MAX)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END

/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint               

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                                                                       
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,@Comments,@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany
		)                                                                                                                      
		
		SET @numOppID=SCOPE_IDENTITY()                                                
  
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired 
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour NUMERIC(9,2), monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour NUMERIC(9,2),monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		SET @TotalAmount=0                                                           
		SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
		UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType
			FROM 
				dbo.DivisionMaster 
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = @Comments,bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,tintOppStatus = @DealStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired 
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour numeric(9,2),monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount float,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), 
					bitItemPriceApprovalRequired BIT
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder  
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour NUMERIC(9,2),monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(200),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              

declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@tintOppStatus TINYINT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                                     

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			-- GET ACCOUNT CLASS IF ENABLED
			DECLARE @numAccountClass AS NUMERIC(18) = 0

			DECLARE @tintDefaultClassType AS INT = 0
			SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

			IF @tintDefaultClassType = 1 --USER
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numDefaultClass,0) 
				FROM 
					dbo.UserMaster UM 
				JOIN 
					dbo.Domain D 
				ON 
					UM.numDomainID=D.numDomainId
				WHERE 
					D.numDomainId=@numDomainId 
					AND UM.numUserDetailId=@numUserCntID
			END
			ELSE IF @tintDefaultClassType = 2 --COMPANY
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numAccountClassID,0) 
				FROM 
					dbo.DivisionMaster DM 
				WHERE 
					DM.numDomainId=@numDomainId 
					AND DM.numDivisionID=@numDivisionID
			END
			ELSE
			BEGIN
				SET @numAccountClass = 0
			END
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,@tintOppStatus,
				@numOrderStatus,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=Item.numItemCode and VN.numVendorID=IT.numVendorID),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					0,
					NULL,'', (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage,
					tintTaxType
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType
				FROM 
					dbo.DivisionMaster 
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
				) AS TEMPTax
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			DECLARE @DealStatus AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND @tintOppStatus = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND @tintOppStatus = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND @tintOppStatus = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mangeautopogeneration')
DROP PROCEDURE usp_mangeautopogeneration
GO
CREATE PROCEDURE USP_MangeAutoPOGeneration
@numParentOppID numeric(9),
@numOppBizDocID numeric(9),
@numItemCode numeric(9),
@numDomainID numeric(9),
@numOppBizDocItemID numeric(9),
@monDealAmount money,
@monPaidAmount money

AS

BEGIN


DECLARE @numNewOppId numeric(9)
DECLARE @numContactId numeric(9)
DECLARE @numDivisionId numeric(9)
DECLARE @numUserCntID NUMERIC(9)
DECLARE @vcCompanyName varchar(250)
DECLARE @monCost money
DECLARE @CloseDate datetime
DECLARE @vcPOppName VARCHAR(500)
DECLARE @numBizDocId numeric(9)
DECLARE @numCurrencyCode numeric
DECLARE @TotalAmount MONEY
DECLARE @numOppBizDocsId NUMERIC(9)
DECLARE @numOldBizDocId NUMERIC(9)

select @numCurrencyCode=numCurrencyID from Domain where numDomainID=@numDomainID;

select @CloseDate=getdate()

--delete from  temp_AutoOrder;



--insert into temp_AutoOrder
--select
--@numParentOppID ,
--@numOppBizDocID ,
--@numItemCode ,
--@numDomainID ,
--@numOppBizDocItemID ,
--@numNewOPpId ,
--@numDivisionID

------ FOR THE RULE WHERE BIZDOC PAID FULLY

set @numDivisionId=0
--if @monDealAmount=@monPaidAmount
	
				
			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=1 and b.monAMountPaid= @monDealAmount and 
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

					insert into Temp_Cheking select 'Insert Amount Match: Div ' + cast(@numDivisionId as varchar(50));

					set @numNewOppId=0;

					Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
								OpportunityMaster OM on OM.numOppId= OL.numChildOppID
					 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
						   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;
						   
					--update temp_autoOrder set numDivisionID =@numDivisionId;

					set @vcPOppName=''
					if @numDivisionId>0
						begin

							SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());

							if @numNewOppId=0
								begin
									EXEC USP_OppManage
									@numNewOppId OUTPUT,
									@numContactId =@numContactId,                                                                          
									@numDivisionId =@numDivisionId,                                                                          
									@tintSource=0,                                                                         
									@vcPOppName =@vcPOppName,                                                                          
									--@Comments ='',                                                                          
									--@bitPublicFlag =0,                                                                          
									@numUserCntID =@numUserCntID,                                                                                 
									--@monPAmount  =0,                                                 
									--@numAssignedTo =0,                                                                                                        
									@numDomainId =@numDomainID,                                                                                                                                                                                   
									--@strItems =null,                                                                          
									--@strMilestone =null,                                                                          
									@dtEstimatedCloseDate =@CloseDate,                                                                          
									@CampaignID =0,
									@lngPConclAnalysis =0,
									@tintOppType =2,                                                                                                                                             
									@tintActive =1,                                                              
									@numSalesOrPurType =0,             
									@numRecurringId=0,
									@numCurrencyID =@numCurrencyCode,
									@DealCompleted =0 


									--update temp_autoOrder set numNewOPpId =@numNewOppId;

									insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
									(@numParentOppID,@numNewOppId,@numOppBizDocID)

									insert into Temp_Cheking select 'Insert Amount Match: New Opp ' + cast(@numNewOppId as varchar(50));
								end
										if @numNewOppId>0 
											begin
												
												if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
													begin	
														insert into Temp_Cheking select 'Insert Amount Match: BizDoc Opp Items - ' + cast(@numOppBizDocItemID as varchar(50));														
														insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
														select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
														from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
													end
											end
								
							
							end

------------------- END OF AMOUTN NOT MATCH

--------------- INSERTING INTO BIZDOC AND BIZDOC ITEMS-----------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
------------------------------------- END OF BIZDOC ITEM INSERT -------------------
------------------------------------- START AMOUNT NOT MATCH -------------------
			insert into Temp_Cheking select 'Insert Amount Not Match'
			set @numDivisionId=0

			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1 
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=0 and
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

			insert into Temp_Cheking select 'Insert Amount Not Match ' + cast(@numDivisionId as varchar(100));

			set @numNewOppId=0;

			Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
						OpportunityMaster OM on OM.numOppId= OL.numChildOppID
			 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
				   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;


			


			set @vcPOppName=''
			if @numDivisionId>0
				begin
					SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());
					if @numNewOppId=0
						begin
							EXEC USP_OppManage
							@numNewOppId OUTPUT,
							@numContactId =@numContactId,                                                                          
							@numDivisionId =@numDivisionId,                                                                          
							@tintSource=0,                                                                         
							@vcPOppName =@vcPOppName,                                                                          
							--@Comments ='',                                                                          
							--@bitPublicFlag =0,                                                                          
							@numUserCntID =@numUserCntID,                                                                                 
							--@monPAmount  =0,                                                 
							--@numAssignedTo =0,                                                                                                        
							@numDomainId =@numDomainID,                                                                                                                                                                                   
							--@strItems =null,                                                                          
							--@strMilestone =null,                                                                          
							@dtEstimatedCloseDate =@CloseDate,                                                                          
							@CampaignID =0,
							@lngPConclAnalysis =0,
							@tintOppType =2,                                                                                                                                             
							@tintActive =1,                                                              
							@numSalesOrPurType =0,             
							@numRecurringId=0,
							@numCurrencyID =@numCurrencyCode,
							@DealCompleted =0  
							
							--update temp_autoOrder set numNewOPpId =@numNewOppId;
							
							insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
							(@numParentOppID,@numNewOppId,@numOppBizDocID)
						end
							if @numNewOppId>0 
									begin
									if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
										begin															
											insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
											select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
											from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
										end
								end
				end
------------------- END OF AMOUNT NOT MATCH------------------------------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
-------------------------------------------
---======================================================

END -- THE FINAL END
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS TINYINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000)

as                            
declare @CRMType as integer               
declare @numRecOwner as integer                            
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
                                                   
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1                              

DECLARE @numTemplateID NUMERIC
SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

   Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
	 -- GET ACCOUNT CLASS IF ENABLED
	DECLARE @numAccountClass AS NUMERIC(18) = 0

	DECLARE @tintDefaultClassType AS INT = 0
	SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType = 1 --USER
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END
                            
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
insert into OpportunityMaster                              
  (                              
  numContactId,                              
  numDivisionId,                              
  txtComments,                              
  numCampainID,                              
  bitPublicFlag,                              
  tintSource, 
  tintSourceType ,                               
  vcPOppName,                              
  monPAmount,                              
  numCreatedBy,                              
  bintCreatedDate,                               
  numModifiedBy,                              
  bintModifiedDate,                              
  numDomainId,                               
  tintOppType, 
  tintOppStatus,                   
  intPEstimatedCloseDate,              
  numRecOwner,
  bitOrder,
  numCurrencyID,
  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
  monShipCost,
  numOppBizDocTempID,numAccountClass
  )                              
 Values                              
  (                              
  @numContactId,                              
  @numDivID,                              
  @txtComments,                              
  @numCampainID,--  0,
  0,                              
  @tintSource,   
  @tintSourceType,                           
  ISNULL(@vcPOppName,'SO'),                             
  0,                                
  @numContactId,                              
  getutcdate(),                              
  @numContactId,                              
  getutcdate(),        
  @numDomainId,                              
  1,             
  @tintOppStatus,       
  getutcdate(),                                 
  @numRecOwner ,
  1,
  @numCurrencyID,
  @fltExchangeRate,@fltDiscount,@bitDiscountType
  ,@monShipCost,
  @numTemplateID,@numAccountClass
  
  )                               
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                
                
insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount 
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

declare @tintShipped as tinyint      
DECLARE @tintOppType AS TINYINT
      
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                            
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )

IF(@vcSource IS NOT NULL)
BEGIN
	insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
        
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
   	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
	select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
	join divisionMaster Div                            
	on div.numCompanyID=com.numCompanyID                            
	where div.numdivisionID=@numDivID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
   	
END

--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems 
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType
) 
SELECT 
	@numOppID,
	TI.numTaxItemID,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType
FROM 
	TaxItems TI 
JOIN 
	DivisionTaxTypes DTT 
ON 
	TI.numTaxItemID = DTT.numTaxItemID
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
) AS TEMPTax
WHERE 
	DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
UNION 
SELECT 
	@numOppID,
	0,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType
FROM 
	dbo.DivisionMaster 
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
) AS TEMPTax
WHERE 
	bitNoTax=0 
	AND numDivisionID=@numDivID
  
  --Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME
AS                 
	SELECT 
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax 
	FROM 
	(
		SELECT 
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount
		FROM 
			OpportunityMaster OM 
		JOIN 
			OpportunityBizDocs OBD 
		ON 
			OM.numOppId=OBD.numOppId
		JOIN 
			OpportunityMasterTaxItems OMTI 
		ON 
			OMTI.numOppId=OBD.numOppId
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
(
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0,
	 @numDefaultShippingServiceID NUMERIC(18,0),
	 @numAccountClass NUMERIC(18,0) = 0
    )
AS 
	BEGIN

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			bitEmailToCase = @bitEmailToCase,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numDefaultShippingServiceID = @numDefaultShippingServiceID,
			numAccountClassID = ISNULL(@numAccountClass,0)
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 


GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAddress]    Script Date: 05/31/2009 15:44:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppaddress')
DROP PROCEDURE usp_updateoppaddress
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppAddress]      
@numOppID as numeric(9)=0,      
@byteMode as tinyint,      
@vcStreet as varchar(100),      
@vcCity as varchar(50),      
@vcPostalCode as varchar(15),      
@numState as numeric(9),      
@numCountry as numeric(9),  
@vcCompanyName as varchar(100), 
@numCompanyId as numeric(9), -- This in Parameter is added by Ajit Singh on 01/10/2008,
@vcAddressName as varchar(50)='',
@numReturnHeaderID as numeric(9)=0
as      
	declare @numDivisionID as numeric(9)   -- This local variable is added by Ajit Singh on 01/10/2008
	
	SET @numReturnHeaderID = NULLIF(@numReturnHeaderID,0)
	SET @numOppID = NULLIF(@numOppID,0)
	
	DECLARE @numDomainID AS NUMERIC(9)
	SET @numDomainID = 0
	
if @byteMode=0      
	begin      
		IF ISNULL(@numOppID,0) >0
		BEGIN
			 update OpportunityMaster set tintBillToType=2 where numOppID=@numOppID    
	 
			if not exists(select * from OpportunityAddress where numOppID=@numOppID)    
				insert into OpportunityAddress (numOppID,vcBillCompanyName,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcAddressName,numReturnHeaderID,numBillCompanyID)    
				values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numReturnHeaderID,@numCompanyID)    
			else    
				 update OpportunityAddress set  
				  vcBillCompanyName=@vcCompanyName,       
				  vcBillStreet=@vcStreet,      
				  vcBillCity=@vcCity,      
				  numBillState=@numState,      
				  vcBillPostCode=@vcPostalCode,      
				  numBillCountry=@numCountry,vcAddressName=@vcAddressName,
				  numBillCompanyID=@numCompanyId      
				 where numOppID=@numOppID 
		END	 
	   ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			if not exists(select * from OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
				insert into OpportunityAddress (numOppID,vcBillCompanyName,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcAddressName,numReturnHeaderID,numBillCompanyID)    
				values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numReturnHeaderID,@numCompanyID)    
			else
 			 update OpportunityAddress set  
			  vcBillCompanyName=@vcCompanyName,       
			  vcBillStreet=@vcStreet,      
			  vcBillCity=@vcCity,      
			  numBillState=@numState,      
			  vcBillPostCode=@vcPostalCode,      
			  numBillCountry=@numCountry,vcAddressName=@vcAddressName,
			  numBillCompanyID=@numCompanyId      
			 where numReturnHeaderID = @numReturnHeaderID 
		END
	end      
	
else if @byteMode=1      
	begin     
		IF ISNULL(@numOppID,0) >0
		BEGIN
		IF @numCompanyId >0 
		BEGIN
			update OpportunityMaster set tintShipToType=2 where numOppID=@numOppID     
		END 
		
		if not exists(select * from OpportunityAddress where numOppID=@numOppID)    
			insert into OpportunityAddress (numOppID,vcShipCompanyName,vcShipStreet,vcShipCity,numShipState,vcShipPostCode,numShipCountry,vcAddressName,numShipCompanyID)    
			values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numCompanyID)    
		else 
				update OpportunityAddress set  
			  vcShipCompanyName= @vcCompanyName,      
			  vcShipStreet=@vcStreet,      
			  vcShipCity=@vcCity,      
			  numShipState=@numState,      
			  vcShipPostCode=@vcPostalCode,      
			  numShipCountry=@numCountry,vcAddressName=@vcAddressName,
			  numShipCompanyID=@numCompanyId         
			 where numOppID=@numOppID   
		END
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
		if not exists(select * from OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
			insert into OpportunityAddress (numOppID,vcShipCompanyName,vcShipStreet,vcShipCity,numShipState,vcShipPostCode,numShipCountry,vcAddressName,numShipCompanyID)    
			values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numCompanyID)    
		else
				update OpportunityAddress set  
			  vcShipCompanyName= @vcCompanyName,      
			  vcShipStreet=@vcStreet,      
			  vcShipCity=@vcCity,      
			  numShipState=@numState,      
			  vcShipPostCode=@vcPostalCode,      
			  numShipCountry=@numCountry,vcAddressName=@vcAddressName,
			  numShipCompanyID=@numCompanyId         
			 where numReturnHeaderID = @numReturnHeaderID 
		END
	END
	
else if @byteMode=2
	BEGIN
		update OpportunityMaster set tintShipToType=3 where numOppID=@numOppID   
	END

ELSE IF @byteMode= 3     
	BEGIN
		--SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 
		IF NOT EXISTS(SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID AND tintAddressOf = 4)    
			BEGIN
				PRINT 'ADD Mode'
				INSERT INTO dbo.AddressDetails 
				(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
				SELECT @vcAddressName,@vcStreet,@vcCity,@vcPostalCode,@numState,@numCountry,1,4,2,@numOppID,numDomainID	
				FROM dbo.ProjectsMaster WHERE numProId = @numOppID
				
				UPDATE dbo.ProjectsMaster SET numAddressID = @@IDENTITY WHERE numProId = @numOppID
			END
			
		ELSE
			BEGIN
				PRINT 'Update Mode'	 
				UPDATE dbo.AddressDetails SET 
							vcStreet = @vcStreet,
							vcCity = @vcCity,
							numState = @numState,
							vcPostalCode = @vcPostalCode,
							numCountry = @numCountry,
							vcAddressName = @vcAddressName  
				WHERE   numRecordID = @numOppID   	
			END
			
        --SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 			
	END

IF @numOppID>0
BEGIN
select @numDomainID=numDomainID,@numDivisionID=numDivisionID from OpportunityMaster where numOppID=@numOppId

--Update Tax for Division   
UPDATE 
	OMTI 
SET 
	fltPercentage=TEMPTax.decTaxValue,
	tintTaxType = TEMPTax.tintTaxType
FROM 
	OpportunityMasterTaxItems OMTI 
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,OMTI.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE 
	numOppID=@numOppID   

UPDATE OBD SET monDealAmount= [dbo].[GetDealAmount](@numOppId ,getutcdate(),OBD.numOppBizDocsId ) 
FROM dbo.OpportunityBizDocs OBD WHERE numOppId=@numOppID	                 
END 
ELSE IF @numReturnHeaderID>0
BEGIN
	select @numDomainID=numDomainID,@numDivisionID=numDivisionID from ReturnHeader where numReturnHeaderID=@numReturnHeaderID

--Update Tax for Division   
UPDATE 
	OMTI 
SET 
	fltPercentage=TEMPTax.decTaxValue,
	tintTaxType = TEMPTax.tintTaxType
FROM 
	OpportunityMasterTaxItems OMTI 
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,OMTI.numTaxItemID,NULL,1,@numReturnHeaderID)
) AS TEMPTax
WHERE 
	numReturnHeaderID=@numReturnHeaderID   

END

