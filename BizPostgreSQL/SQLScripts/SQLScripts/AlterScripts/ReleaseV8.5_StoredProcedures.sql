/******************************************************************
Project: Release 8.5 Date: 05.DECEMBER.2017
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizDocComission_MarkAsPaidForSelectedDateRange')
DROP PROCEDURE USP_BizDocComission_MarkAsPaidForSelectedDateRange
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_BizDocComission_MarkAsPaidForSelectedDateRange]
(
    @numDomainId AS NUMERIC(9) = 0,
	@dtFromDate AS DATETIME,
	@dtToDate AS DATETIME
)
AS 
BEGIN 
	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	UPDATE
		BDC
	SET 
		bitCommisionPaid = 1
    FROM    
		BizDocComission BDC
    INNER JOIN 
		OpportunityBizDocs OBD 
	ON 
		BDC.numOppBizDocId = OBD.numOppBizDocsId
    INNER JOIN 
		OpportunityMaster Opp 
	ON 
		OPP.numOppId = OBD.numOppId
    WHERE 
		OPP.numDomainID = @numDomainId
        AND BDC.numDomainID = @numDomainId
		AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(BDC.bitCommisionPaid,0)=0
        AND OBD.bitAuthoritativeBizDocs = 1
END
/****** Object:  StoredProcedure [dbo].[USP_cfwSaveCusfld]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwsavecusfld')
DROP PROCEDURE usp_cfwsavecusfld
GO
CREATE PROCEDURE [dbo].[USP_cfwSaveCusfld]          
          
@RecordId as numeric(9)=null,          
@strDetails as text='',          
@PageId as tinyint=null          
AS 
BEGIN        
	IF CHARINDEX('<?xml',@strDetails) = 0
	BEGIN
		SET @strDetails = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strDetails)
	END

 
declare @hDoc int          
          
        
          
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDetails          
          
if @PageId=1          
begin          

Delete from CFW_FLD_Values where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFields as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFields(fld_id) 
SELECT Fld_ID from CFW_FLD_Values where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCnt AS INT
DECLARE @intCntFields AS INT
Declare @numFieldID As numeric(9,0)
Declare @numDomainID as numeric(18,0)
Declare @numUserID as numeric(18,0)
SET @intCnt = 0
SET @intCntFields = (SELECT COUNT(*) FROM @CFFields)
SELECT @numDomainID=numDomainID,@numUserID=numCreatedBy from DivisionMaster where numDivisionID=@RecordId

IF @intCntFields > 0
	BEGIN
		WHILE(@intCnt < @intCntFields)
			BEGIN
			--print 'sahc'
				SET @intCnt = @intCnt + 1
				SELECT @numFieldID = fld_id FROM @CFFields WHERE RowID = @intCnt
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_CT
									@numDomainID =@numDomainID,
									@numUserCntID =0,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldID
			END 

	END
End       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
if @PageId=128 
begin          

insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID

End        
 
          
if @PageId=4          
begin  

Delete from CFW_FLD_Values_Cont where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Cont set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Cont.FldDTLID=X.FldDTLID

--        
--Delete from CFW_FLD_Values_Cont where RecId=@RecordId          
--insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
    
	
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCon as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCon(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Cont where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCon AS INT
DECLARE @intCntFieldsCon AS INT
Declare @numFieldIDCon As numeric(9,0)
Declare @numDomainIDCon as numeric(18,0)
Declare @numUserIDCon as numeric(18,0)
SET @intCntCon = 0
SET @intCntFieldsCon = (SELECT COUNT(*) FROM @CFFieldsCon)
SELECT @numDomainIDCon=numDomainID,@numUserIDCon=numCreatedBy from AdditionalContactsInformation where numContactId=@RecordId

IF @intCntFieldsCon > 0
	BEGIN
		WHILE(@intCntCon < @intCntFieldsCon)
			BEGIN
			
				SET @intCntCon = @intCntCon + 1
				SELECT @numFieldIDCon = fld_id FROM @CFFieldsCon WHERE RowID = @intCntCon
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Cont_CT
									@numDomainID =@numDomainIDCon,
									@numUserCntID =@numUserIDCon,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCon
			END 

	END  
	   
end          
          
if @PageId=3          
BEGIN 
	DECLARE @TEMP TABLE
	(
		fld_id numeric(9),
		FldDTLID numeric(9),
		Value varchar(1000)
	)

	INSERT INTO @TEMP 
	(
		fld_id,
		FldDTLID,
		Value
	)
	SELECT
		fld_id,
		FldDTLID,
		Value
	FROM OPENXML 
		(@hDoc,'/NewDataSet/Table',2)          
	WITH 
		(fld_id NUMERIC(18,0),FldDTLID NUMERIC(18,0),Value VARCHAR(1000)) 

  
	DELETE FROM 
		CFW_FLD_Values_Case 
	WHERE 
		FldDTLID NOT IN (SELECT FldDTLID FROM @TEMP WHERE FldDTLID > 0) 
		AND RecId= @RecordId      
		AND Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)  


	INSERT INTO CFW_FLD_Values_Case 
	(
		Fld_ID,
		Fld_Value,
		RecId
	)          
	SELECT 
		fld_id,
		Value,
		@RecordId  
	FROM
		@TEMP
	WHERE
		FldDTLID=0
		AND fld_id NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)


	UPDATE 
		CFW_FLD_Values_Case 
	SET 
		Fld_Value = X.Value  
	FROM
	   @TEMP X
	WHERE
	   X.FldDTLID > 0
	   AND CFW_FLD_Values_Case.FldDTLID=X.FldDTLID
	   AND  CFW_FLD_Values_Case.Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)           
          
--Delete from CFW_FLD_Values_Case where RecId=@RecordId          
--insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
     
	 --Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCase as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCase(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Case where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCase AS INT
DECLARE @intCntFieldsCase AS INT
Declare @numFieldIDCase As numeric(9,0)
Declare @numDomainIDCase as numeric(18,0)
Declare @numUserIDCase as numeric(18,0)
SET @intCntCase = 0
SET @intCntFieldsCase = (SELECT COUNT(*) FROM @CFFieldsCase)
SELECT @numDomainIDCase=numDomainID,@numUserIDCase=numCreatedBy from Cases where numCaseId=@RecordId

IF @intCntFieldsCase > 0
	BEGIN
		WHILE(@intCntCase < @intCntFieldsCase)
			BEGIN
			--print 'sahc'
				SET @intCntCase = @intCntCase + 1
				SELECT @numFieldIDCase = fld_id FROM @CFFieldsCase WHERE RowID = @intCntCase
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Case_CT
									@numDomainID =@numDomainIDCase,
									@numUserCntID =@numUserIDCase,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCase
			END 

	END
	      
end          
        
if @PageId=5        
begin          


Delete from CFW_FLD_Values_Item where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Item set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Item.FldDTLID=X.FldDTLID  


--Delete from CFW_FLD_Values_Item where RecId=@RecordId          
--insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end         
          
if @PageId=6 or @PageId=2      
begin       

Delete from CFW_Fld_Values_Opp where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Opp set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Opp.FldDTLID=X.FldDTLID 
   
--Delete from CFW_Fld_Values_Opp where RecId=@RecordId          
--insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X     
  
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsOpp as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsOpp(fld_id) 
SELECT Fld_ID from CFW_Fld_Values_Opp where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntOpp AS INT
DECLARE @intCntFieldsOpp AS INT
Declare @numFieldIDOpp As numeric(9,0)
Declare @numDomainIDOpp as numeric(18,0)
Declare @numUserIDOpp as numeric(18,0)
SET @intCntOpp = 0
SET @intCntFieldsOpp = (SELECT COUNT(*) FROM @CFFieldsOpp)
SELECT @numDomainIDOpp=numDomainID,@numUserIDOpp=numCreatedBy from OpportunityMaster where numOppId=@RecordId

IF @intCntFieldsOpp > 0
	BEGIN
		WHILE(@intCntOpp < @intCntFieldsOpp)
			BEGIN
			--print 'sahc'
				SET @intCntOpp = @intCntOpp + 1
				SELECT @numFieldIDOpp = fld_id FROM @CFFieldsOpp WHERE RowID = @intCntOpp
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_Fld_Values_Opp_CT
									@numDomainID =@numDomainIDOpp,
									@numUserCntID =@numUserIDOpp,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDOpp
			END 

	END

   
          
end      
    
if @PageId=7 or    @PageId=8    
begin

Delete from CFW_Fld_Values_Product where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Product set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Product.FldDTLID=X.FldDTLID
          
--Delete from CFW_Fld_Values_Product where RecId=@RecordId          
--insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end     
          
if @PageId=11  
begin   

Delete from CFW_Fld_Values_Pro where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Pro set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Pro.FldDTLID=X.FldDTLID
       
--Delete from CFW_Fld_Values_Pro where RecId=@RecordId          
--insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X    


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsPro as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsPro(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Pro where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntPro AS INT
DECLARE @intCntFieldsPro AS INT
Declare @numFieldIDPro As numeric(9,0)
Declare @numDomainIDPro as numeric(18,0)
Declare @numUserIDPro as numeric(18,0)
SET @intCntPro = 0
SET @intCntFieldsPro = (SELECT COUNT(*) FROM @CFFieldsPro)
SELECT @numDomainIDPro=numDomainID,@numUserIDPro=numCreatedBy from ProjectsMaster where numProId=@RecordId

IF @intCntFieldsPro > 0
	BEGIN
		WHILE(@intCntPro < @intCntFieldsPro)
			BEGIN
			--print 'sahc'
				SET @intCntPro = @intCntPro + 1
				SELECT @numFieldIDPro = fld_id FROM @CFFieldsPro WHERE RowID = @intCntPro
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Pro_CT
									@numDomainID =@numDomainIDPro,
									@numUserCntID =@numUserIDPro,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDPro
			END 

	END      
          
end

        
EXEC sp_xml_removedocument @hDoc
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomerCreditCardInfo_Save')
DROP PROCEDURE USP_CustomerCreditCardInfo_Save

GO
/****** Object:  StoredProcedure [dbo].[USP_AddCustomerCreditCardInfo]    Script Date: 05/07/2009 21:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CustomerCreditCardInfo_Save]
	@numCCInfoID NUMERIC(18,0),
    @numContactId NUMERIC(18,0),
    @vcCardHolder VARCHAR(500),
    @vcCreditCardNo VARCHAR(500),
    @vcCVV2 VARCHAR(200),
    @numCardTypeID NUMERIC(9),
    @tintValidMonth TINYINT,
    @intValidYear INT,
    @numUserCntId NUMERIC(18,0),
    @bitIsDefault BIT=0
AS
BEGIN
    IF @bitIsDefault = 1
    BEGIN
      	UPDATE CustomerCreditCardInfo SET dbo.CustomerCreditCardInfo.bitIsDefault = 0 WHERE numContactId=@numContactId AND bitIsDefault = 1
	END

	IF ISNULL(@numCCInfoID,0) > 0
	BEGIN
		UPDATE 
			CustomerCreditCardInfo
        SET    
            vcCardHolder = @vcCardHolder,
            vcCreditCardNo = @vcCreditCardNo,
            vcCVV2 = @vcCVV2,
            numCardTypeID = @numCardTypeID,
            tintValidMonth = @tintValidMonth,
            intValidYear = @intValidYear,
            numModifiedby = @numUserCntId,
            dtModified = GETUTCDATE(),
            bitIsDefault = @bitIsDefault
		WHERE 
			numCCInfoID = @numCCInfoID
	END
	ELSE 
	BEGIN
		INSERT INTO CustomerCreditCardInfo
        (
			[numContactId],
            [vcCardHolder],
            [vcCreditCardNo],
            [vcCVV2],
            [numCardTypeID],
            [tintValidMonth],
            [intValidYear],
            [numCreatedby],
            [dtCreated],
            [numModifiedby],
            [dtModified],
            bitIsDefault
		)
        VALUES     
		(
			@numContactId,
            @vcCardHolder,
            @vcCreditCardNo,
            @vcCVV2,
            @numCardTypeID,
            @tintValidMonth,
            @intValidYear,
            @numUserCntId,
            GETUTCDATE(),
            @numUserCntId,
            GETUTCDATE(),
            @bitIsDefault
		)
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetContacts')
DROP PROCEDURE dbo.USP_ElasticSearch_GetContacts
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetContacts]
	@numDomainID NUMERIC(18,0)
	,@vcContactIds VARCHAR(MAX)
AS 
BEGIN
	
	SELECT
		numContactId AS id
		,'contact' AS module
		,ISNULL(tintCRMType,0) AS tintOrgCRMType
		,ISNULL(numCompanyType,0) AS numOrgCompanyType
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,ISNULL(AdditionalContactsInformation.numRecOwner,0) numRecOwner
		,CONCAT('<b style="color:#0099cc">Contact:</b> ',(CASE WHEN LEN(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),' ',(CASE WHEN LEN(vcLastName) > 0 THEN vcLastName ELSE '-' END),', ',(CASE WHEN LEN(vcEmail) > 0 THEN vcEmail ELSE '-' END), ', ', vcCompanyName) AS displaytext
		,CONCAT((CASE WHEN LEN(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),' ',(CASE WHEN LEN(vcLastName) > 0 THEN vcLastName ELSE '-' END)) AS [text]
		,CONCAT('/contact/frmContacts.aspx?CntId=',numContactId) AS url
		,ISNULL(vcFirstName,'') AS Search_vcFirstName
		,ISNULL(vcLastName,'') AS Search_vcLastName
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS Search_vcFullName
		,ISNULL(vcEmail,'') AS Search_vcEmail
		,ISNULL(vcCompanyName,'') AS Search_vcCompanyName
		,ISNULL(AdditionalContactsInformation.numCell,'') Search_numCell 
		,ISNULL(AdditionalContactsInformation.numHomePhone,'') Search_numHomePhone
		,ISNULL(AdditionalContactsInformation.numPhone,'') Search_numPhone
	FROM
		AdditionalContactsInformation
	INNER JOIN
		DivisionMaster
	ON
		AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		AdditionalContactsInformation.numDomainId=@numDomainID
		AND (@vcContactIds IS NULL OR AdditionalContactsInformation.numContactId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcContactIds,'0'),',')))
		
END
-- USP_ExportDataBackUp 1,9
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExportDataBackUp')
DROP PROCEDURE USP_ExportDataBackUp
GO
CREATE PROCEDURE [dbo].[USP_ExportDataBackUp]
    @numDomainID NUMERIC(9),
    @tintTable TINYINT,
	@dtFromDate DATETIME,
	@dtToDate DATETIME
AS 
    BEGIN
		
        IF @tintTable = 1 
            BEGIN
                SELECT  CMP.numCompanyID CompanyID,
                        CMP.vcCompanyName CompanyName,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = DM.numStatusID
                        ) AS CompanyStatus,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyRating
                        ) AS CompanyRating,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyType
                        ) AS CompanyType,
                        CASE WHEN ISNULL(DM.bitPublicFlag, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsPrivate,
                        DM.numDivisionID DivisionID,
                        DM.vcDivisionName DivisionName,
                        ISNULL(AD1.vcStreet, '') AS BillStreet,
                        ISNULL(AD1.vcCity, '') AS BillCity,
                        ISNULL(AD1.vcPostalCode, '') AS BillPostCode,
                        dbo.fn_GetListName(AD1.numCountry, 0) BillCountry,
                        ISNULL(dbo.fn_GetState(AD1.numState), '') AS BillState,
                        
                        ISNULL(AD2.vcStreet, '') AS ShipStreet,
                        ISNULL(AD2.vcCity, '') AS ShipCity,
                        ISNULL(AD2.vcPostalCode, '') AS ShipPostCode,
                        dbo.fn_GetListName(AD2.numCountry, 0) ShipCountry,
                        ISNULL(dbo.fn_GetState(AD2.numState), '') AS ShipState,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = numFollowUpStatus
                        ) AS FollowUpStatus,
                        CMP.vcWebLabel1 WebLabel1,
                        CMP.vcWebLink1 WebLink1,
                        CMP.vcWebLabel2 WebLabel2,
                        CMP.vcWebLink2 WebLink2,
                        CMP.vcWebLabel3 WebLabel3,
                        CMP.vcWebLink3 WebLink3,
                        CMP.vcWeblabel4 Weblabel4,
                        CMP.vcWebLink4 WebLink4,
                        CASE WHEN ISNULL(tintBillingTerms, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsBillingTerms,
                        --(SELECT vcData FROM dbo.ListDetails WHERE numListID = 296 AND numDomainID = CMP.numDomainID AND numListItemID = numBillingDays ) AS [BillingDays],
                        (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS [BillingDays],
                        (SELECT vcTerms FROM dbo.BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS [BillingDaysName],
--                        tintInterestType InterestType,
--                        fltInterest Interest,
                      
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = DM.numTerID
                        ) AS Territory,
--                        DM.numTerID TerritoryID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyIndustry
                        ) AS CompanyIndustry,
--                        CMP.numCompanyIndustry,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyCredit
                        ) AS CompanyCredit,
--                        CMP.numCompanyCredit,
                        ISNULL(CMP.txtComments, '') AS Comments,
                        ISNULL(CMP.vcWebSite, '') WebSite,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numAnnualRevID
                        ) AS AnnualRevenue,
--                        CMP.numAnnualRevID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numNoOfEmployeesID
                        ) AS NoOfEmployees,
--                        numNoOfEmployeesID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.vcProfile
                        ) AS [Profile],
--                        CMP.vcProfile,
                        DM.numCreatedBy CreatedBy,
                        dbo.fn_GetContactName(DM.numRecOwner) AS RecordOwner,
                        DM.numRecOwner AS RecordOwnerID,
                        DM.numCreatedBy AS CreatedBy,
                        DM.bintCreatedDate CreateDate,
--                        DM.numRecOwner RecordOwner,
                        DM.numModifiedBy AS ModifiedBy,
                        ISNULL(vcComPhone, '') AS CompanyPhone,
                        vcComFax CompanyFax,
--                        DM.numGrpID,
                        ( SELECT    vcGrpName
                          FROM      Groups
                          WHERE     numGrpId = DM.numGrpID
                        ) AS GroupName,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.vcHow
                        ) AS InformationSource,
--                        CMP.vcHow AS vcInfoSource,
                        CASE DM.tintCRMType
                          WHEN 0 THEN 'Leads'
                          WHEN 1 THEN 'Prospects'
                          WHEN 2 THEN 'Accounts'
                          ELSE 'NA'
                        END [RelationshipType],
                        ( SELECT    vcCampaignName
                          FROM      CampaignMaster
                          WHERE     numCampaignID = DM.numCampaignID
                        ) AS CampaignName,
                        numCampaignID CampaignID,
                        DM.numAssignedBy AssignedByID,
                        ISNULL(ACI2.vcFirstName,'') + ' ' + ISNULL(ACI2.vcLastName,'') AS [AssignedBy],
                        CASE WHEN ISNULL(bitNoTax, 0) = 0 THEN 'False'
							 ELSE 'True'
						END [IsSalesTax],
--                        ( SELECT    A.vcFirstName + ' ' + A.vcLastName
--                          FROM      AdditionalContactsInformation A
--                                    LEFT JOIN DivisionMaster D ON D.numDivisionID = A.numDivisionID
--                          WHERE     A.numContactID = D.numAssignedTo AND D.numDomainId=cmp.numDomainID and cmp.numCompanyId=D.numCompanyId
--                        ) AS AssignedName,
						ISNULL(ACI1.vcFirstName,'') + ' ' + ISNULL(ACI1.vcLastName,'') AS [AssignedTo],
                        DM.numAssignedTo AS AssignedToID,
                        ACI.numContactID ContactID,
                        ACI.vcFirstName FirstName,
                        ACI.vcLastName LastName,
                        ACI.vcEmail ContactEmailAddress,
                        ACI.vcAltEmail AlternetEmailAddress,
                        ACI.numPhone ContactPhone,
                        ACI.numPhoneExtension ContactPhoneExt,
                        L8.vcData ContactStatus,
                        L9.vcData ContactType,
                        ACI.vcGivenName GivenName,
--                        ACI.numTeam,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.numTeam
                        ) AS Team,
                        ACI.vcFax FaxNo,
                        CASE ISNULL(ACI.charSex, '')
                          WHEN 'M' THEN 'Male'
                          WHEN 'F' THEN 'Female'
                          ELSE ''
                        END AS Gender,
                        ACI.bintDOB DateOfBirth,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.vcPosition
                        ) AS Position,
                        ACI.txtNotes Notes,
                        ACI.numCell CellNo,
                        ACI.NumHomePhone HomePhone,
                        ACI.vcAsstFirstName AsstFirstName,
                        ACI.vcAsstLastName AsstLastName,
                        ACI.numAsstPhone AsstPhone,
                        ACI.numAsstExtn AsstExtn,
                        ACI.vcAsstEmail AsstEmail,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.vcDepartment
                        ) AS Department,
                        AD.vcStreet PrimaryStreet,
                        AD.vcCity PrimaryCity,
                        dbo.fn_GetState(AD.numState) AS PrimaryState,
                        dbo.fn_GetListName(AD.numCountry, 0) AS PrimaryCountry,
                        AD.vcPostalCode PrimaryPotalCode,
--                        AddC.vcContactLocation,
--                        AddC.vcStreet,
--                        AddC.vcCity,
--                        AddC.vcState,
--                        AddC.intPostalCode,
--                        AddC.vcCountry,
                        ACI.numManagerID ManagersContactID,
                        ACI.vcCategory,
                        ACI.vcTitle,
                        CASE WHEN ISNULL(ACI.bitOptOut, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END ContactEmailOptOut,
                         CASE WHEN ISNULL(ACI.bitPrimaryContact, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END PrimaryContact,
                        C.vcCurrencyDesc AS [DefaultTradingCurrency],
                        IACR.numNonBizCompanyID AS NonBizCompanyID,
                        IAI.numNonBizContactID AS NonBizContactID
                FROM    CompanyInfo CMP
                        JOIN DivisionMaster DM ON DM.numCompanyID = CMP.numCompanyID
                        LEFT OUTER JOIN AdditionalContactsInformation ACI ON DM.numDivisionID = ACI.numDivisionID
                        LEFT JOIN AdditionalContactsInformation ACI1 ON ACI1.numContactID = DM.numAssignedTo 
                        LEFT JOIN AdditionalContactsInformation ACI2 ON ACI2.numContactID = DM.numAssignedBy 
                        LEFT JOIN ListDetails L8 ON L8.numListItemID = ACI.numEmpStatus
                        LEFT JOIN ListDetails L9 ON L9.numListItemID = ACI.numContactType
                        LEFT JOIN dbo.AddressDetails AD ON AD.numRecordID = ACI.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
                        LEFT JOIN dbo.AddressDetails AD1 ON AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
                        LEFT JOIN dbo.AddressDetails AD2 ON AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                        LEFT JOIN [Currency] C ON C.[numCurrencyID] = DM.[numCurrencyID] AND DM.numDomainID = C.numDomainID
                        LEFT JOIN dbo.ImportActionItemReference IAI ON IAI.numContactID  =ACI.numContactId
                        LEFT JOIN dbo.ImportOrganizationContactReference IACR ON IACR.numDivisionID =DM.numDivisionID
                WHERE   DM.numDomainID = @numDomainID
                ORDER BY DM.numCompanyID   
                
                --Get field name 
				-- 14 for leads,13 for Accounts,12 for Prospects

                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 1
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 12
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 13
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 14
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values] CF
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   DM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
            
        IF @tintTable = 2 
            BEGIN
                SELECT  Opp.numoppid OpportunityID,
                        ( SELECT    vcFirstname + ' ' + vcLastName
                          FROM      additionalContactsinformation
                          WHERE     numContactId = Opp.numContactId
                        ) AS ContactName,
                        Opp.txtComments Comments,
                        Opp.numDivisionID DivisionID,
                        CASE tintOppType
                          WHEN 1 THEN 'Sales'
                          WHEN 2 THEN 'Purchase'
                          ELSE 'NA'
                        END OpportunityType,
--                        ISNULL(bintAccountClosingDate, '') AS bintAccountClosingDate,
                        Opp.numContactID AS ContactID,
                        ( SELECT    vcData
                          FROM      [ListDetails]
                          WHERE     numListItemID = Opp.tintSource
                        ) AS OpportunitySource,
                        C2.vcCompanyName CompanyName, 
--                        D2.tintCRMType,
                        Opp.vcPoppName OpportunityName,
--                        intpEstimatedCloseDate,
                        opp.monDealAmount CalculatedAmount,
--                        lngPConclAnalysis,
                        monPAmount AS OppAmount,
--                        numSalesOrPurType,
                        CASE WHEN ISNULL(Opp.tintActive, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsActive,
                        Opp.numCreatedby Createdby,
                        Opp.numModifiedBy ModifiedBy,
                        dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
                        Opp.numRecOwner RecordOwnerID,
                        CASE WHEN ISNULL(tintshipped, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END AS IsShipped,
--                        ISNULL(tintOppStatus, 0) AS tintOppStatus,
                        CASE ISNULL(tintOppStatus, 0)
                          WHEN 0 THEN 'Open'
                          WHEN 1 THEN 'Closed-Won'
                          WHEN 2 THEN 'Closed-Lost'
                        END OpportunityStatus,
                        Opp.numAssignedTo AssignedTo,
                        Opp.numAssignedBy AssignedBy,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = D2.numTerID
                        ) AS Territory,
                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                        Opp.bintCreatedDate CreatedDate,
                        Opp.bintAccountClosingDate ClosingDate,
                        Opp.intPEstimatedCloseDate DueDate,
                        dbo.fn_GetContactAddedToOpportunity(numOppID) AS AssociatedContactIds
                FROM    OpportunityMaster Opp
                        JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
                        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
                        LEFT JOIN [Currency] C ON C.[numCurrencyID] = Opp.[numCurrencyID]
                WHERE   Opp.numDomainID = @numDomainID
                ORDER BY Opp.numDivisionID 
                
                SELECT  OI.[numoppitemtCode] OppItemID,
                        OI.[numOppId] OpportunityID,
                        OI.[numItemCode] ItemID,
                        OI.[numUnitHour] Quantity,
                        OI.[monPrice] Price,
                        OI.[monTotAmount] TotalAmount,
--					   OI.[numSourceID],
                        OI.[vcItemDesc] ItemDescription,
--					   OI.[numWarehouseItmsID] WarehouseItemID,
                        CASE OI.[vcType]
                          WHEN 'P' THEN 'Inventory Item'
                          WHEN 'S' THEN 'Service'
                          WHEN 'N' THEN 'Non-Inventory Item'
                          WHEN 'A' THEN 'Asset Item'
                          ELSE OI.[vcType]
                        END ItemType,
                        OI.[vcAttributes] Attributes,
                        OI.[bitDropShip] IsDropship,
--					   OI.[numUnitHourReceived],
--					   OI.[bitDiscountType],
--					   OI.[fltDiscount],
                        OI.[monTotAmtBefDiscount] TotalAmtBeforeDiscount
--					   OI.[numQtyShipped]
                FROM    [OpportunityMaster] OM
                        INNER JOIN [OpportunityItems] OI ON OM.numOppID = OI.numOppID
                WHERE   OM.numDomainID = @numDomainID
                
				--Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id IN ( 2, 6 ) -- 2 for Sales Records, 6 for Purchase records
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    CFW_Fld_Values_Opp CF
                        INNER JOIN [OpportunityMaster] OM ON OM.[numOppId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   OM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
        IF @tintTable = 3 
            BEGIN
                SELECT  pro.numProID ProjID,
                        pro.vcProjectName ProjectName,
                        pro.vcProjectID ProjectID,
                        pro.numDivisionID DivisionID,
                        pro.numintPrjMgr IntPrjMgrID,
                        dbo.fn_GetContactName(pro.numintPrjMgr) AS InternalProjectMgr,
                        dbo.fn_GetContactName(pro.numCustPrjMgr) AS CustomerPrjectMgr,
                        pro.numCustPrjMgr CustPrjMgrID,
                        pro.txtComments Comments,
                        [dbo].[fn_GetContactName](pro.numAssignedTo) AS AssignedTo,
                        pro.numAssignedTo AssignedToID,
                        pro.intDueDate DueDate,
                        pro.[bintProClosingDate] ClosingDate,
                        pro.[bintCreatedDate] CreatDate
                FROM    ProjectsMaster pro
                WHERE   pro.numdomainID = @numDomainID 
	   		--Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 11 -- 11 for projects
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values_Pro] CF
                        INNER JOIN [ProjectsMaster] PM ON PM.[numProId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   PM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
        IF @tintTable = 4 
            BEGIN
                SELECT  C.numCaseId CaseID,
                        C.numContactID ContactID,
                        vcCaseNumber CaseNumber,
                        intTargetResolveDate ResolveDate,
                        textSubject Subject,
                        Div.numDivisionID DivisionID,
                        dbo.[GetListIemName](numStatus) Status,
                        dbo.[GetListIemName](numPriority) Priority,
                        textDesc CaseDescription,
--                        tintCRMType ,
                        textInternalComments InternalComments,
                        dbo.[GetListIemName](numReason) Reason,
                        C.numContractID ContractID,
                        dbo.[GetListIemName](numOrigin) CaseOrigin,
                        dbo.[GetListIemName](numType) CaseType,
                        C.bintCreatedDate CreateDate,
                        C.numCreatedby CreatedBy,
                        C.numModifiedBy ModifiedBy,
                        C.bintModifiedDate ModifiedDate,
                        C.numRecOwner RecordOwnerID,
--                        tintSupportKeyType,
                        C.numAssignedTo AssignedTo,
                        C.numAssignedBy AssignedBy,
                        ISNULL(Addc.vcFirstName, '') + ' '
                        + ISNULL(Addc.vcLastName, '') AS ContactName,
                        ISNULL(vcEmail, '') AS ContactEmail,
                        ISNULL(numPhone, '')
                        + CASE WHEN numPhoneExtension IS NULL THEN ''
                               WHEN numPhoneExtension = '' THEN ''
                               ELSE ', ' + numPhoneExtension
                          END AS Phone,
                        Com.numCompanyId CompanyID,
                        Com.vcCompanyName CompanyName
                FROM    Cases C
                        LEFT JOIN AdditionalContactsInformation Addc ON Addc.numContactId = C.numContactID
                        JOIN DivisionMaster Div ON Div.numDivisionId = Addc.numDivisionId
                        JOIN CompanyInfo Com ON Com.numCompanyID = div.numCompanyID
                WHERE   C.numDomainID = @numDomainID
            
            --Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 3 -- 3 for cases
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values_Case] CF
                        INNER JOIN [Cases] C ON C.[numCaseId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   C.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
            END
	
    END
    IF @tintTable = 5 
        BEGIN
            SELECT  numSolnID SolutionID,
                    dbo.[GetListIemName](numCategory) Category,
                    vcSolnTitle SolutionName,
                    txtSolution SolutionDescription,
                    vcLink URL
            FROM    SolutionMaster
            WHERE   [numDomainID] = @numDomainID
            
        END
    IF @tintTable = 6 
        BEGIN
            SELECT  [numContractId] ContractID,
                    [numDivisionID] DivisionID,
                    [vcContractName] ContractName,
                    [bintStartDate] StatDate,
                    [bintExpDate] ExpirationDate,
                    [numIncidents] NoOfIncidents,
                    [numHours] NoOfHours,
                    [numEmailDays] EmailDays,
                    [numEmailIncidents] NoOfEmailIncidents,
                    [numEmailHours] EmailHours,
                    [numUserCntID] ContactID,
                    [vcNotes] Notes,
                    [bintCreatedOn] CreateDate,
                    [numAmount] Amount,
                    [bitAmount] IsAmount,
                    [bitIncidents] IsIncidents,
                    [bitDays] IsDays,
                    [bitHour] IsHour,
                    [bitEDays] IsEMailDays,
                    [bitEIncidents] IsEmailIncedents,
                    [bitEHour],
                    [numTemplateId] EmailTemplateID,
                    [decRate],
                    dbo.fn_GetContactAddedToContract(numDomainID,
                                                     numContractID) AS ContactsAddedToContract
            FROM    [ContractManagement]
            WHERE   [numDomainID] = @numDomainID
			
        END 
        
    IF @tintTable = 7 
        BEGIN
            SELECT  --[numJournal_Id] JournalID,
                    [datEntry_Date] JournalEntryDate,
					[TransactionType],
					[CompanyName],
                    [varDescription] Description,
					[BizDocID] BizDocName,
					[numOppBizDocsId] BizDocID,
                    --[numTransactionId] TransationID,
                    --[numOppId] OpportunityID,
					[numDebitAmt] DebitAmount,
                    [numCreditAmt] CreditAmount,
                    [TranRef] TransactinoReferece,
                    [TranDesc] TransactionDescription,
					[BizPayment],
					[CheqNo] CheckNo
                    --[numAccountId] ChartOfAccountID,
                    --[vcAccountName] AccountName,
                    --[numCheckId] CheckID,
--                    [numCashCreditCardId] ,
                    --[numDepositId] DespositeID,
                    --[numCategoryHDRID] TimeExpenseID,
                    --[tintTEType] TimeExpenseType,
                    --[numCategory] CategoryID,
                    --[dtFromDate] FromDate,
                    --[numUserCntID] ContactID,
                    --[numDivisionID] DivisionID
            FROM    [VIEW_GENERALLEDGER]
            WHERE   [numDomainId] = @numDomainID
            AND [VIEW_GENERALLEDGER].[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
        END 
        
    IF @tintTable = 8 
        BEGIN
			DECLARE @CustomFields AS VARCHAR(1000)

				SELECT @CustomFields = COALESCE (@CustomFields,'') + '[' + Fld_label + '],' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
				-- Removes last , from string
				IF DATALENGTH(@CustomFields) > 0
					SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

				DECLARE @vcSQL VARCHAR(8000)

				SET @vcSQL = CONCAT('SELECT  
										I.numItemCode [Item ID],
										vcItemName [Item Name],
										ISNULL(txtItemDesc, '''') [Item Description],
										CASE charItemType
										  WHEN ''P'' THEN ''Inventory Item''
										  WHEN ''S'' THEN ''Service''
										  WHEN ''N'' THEN ''Non-Inventory Item''
										  WHEN ''A'' THEN ''Asset Item''
										  ELSE charItemType
										END [Item Type],
										vcModelID [Model ID],
										(select vcItemGroup from ItemGroups where numItemGroupID=numItemGroup and numDomainId=',@numDomainID,') as [Item Group],
										vcSKU [SKU(NI)],
										dbo.[GetListIemName](numItemClassification) [Item Classification],
										(select top 1 vcCategoryName from ItemCategory IC Inner Join CATEGORY CT on IC.numCategoryID=CT.numCategoryId
										and CT.numDomainID=',@numDomainID,' and IC.numItemId=i.numItemCode) as [Item Category],
										ISNULL(fltWeight, 0) AS Weight,
										ISNULL(fltHeight, 0) AS Height,
										ISNULL(fltWidth, 0) AS Width,
										ISNULL(fltLength, 0) AS Length,
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numBaseUnit,0)) AS [Base Unit],
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numPurchaseUnit,0)) AS [Purchase Unit],
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numSaleUnit,0)) AS [Sale Unit],
										CASE charItemType WHEN ''P'' THEN ISNULL(monListPrice,0) END AS [List Price (I)],
										CASE charItemType WHEN ''N'' THEN ISNULL(monListPrice,0) END AS [List Price (NI)],
										ISNULL(CAST(numBarCodeId AS VARCHAR(50)), '''') AS BarCodeId,
										ISNULL(vcManufacturer,'''') as Manufacturer,
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where DM.numDivisionId=numVendorID) as Vendor,
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numCOGsChartAcntId and COA.numDomainID=',@numDomainID,') as [COGs/Expense Account],
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numAssetChartAcntId and COA.numDomainID=',@numDomainID,') as [Asset Account],
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numIncomeChartAcntId and COA.numDomainID=',@numDomainID,') as [Income Account],
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where  
										DM.numDivisionId=CA.numDivId) as AssetOwner,
										isnull(numAppvalue,0) as AppreciationValue,
										isnull(numDepValue,0) as DepreciationValue,
										isnull(dtPurchase,'''') as PurchaseDate,
										isnull(dtWarrentyTill,'''') as WarranteeTill,
										(SELECT TOP 1 vcPathForImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1 ) ImagePath,
										(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1) ThumbImagePath,
										CASE WHEN ISNULL(bitTaxable, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Taxable],
										CASE WHEN ISNULL(bitKitParent, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Kit],
										CASE WHEN ISNULL(bitSerialized, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Serialize],                    					
										monAverageCost [Average Cost],
										monCampaignLabourCost CampaignLabourCost,
										numCreatedBy CreatedBy,
										bintCreatedDate CreatedDate,
										numModifiedBy ModifiedBy,
										bintModifiedDate ModifiedDate,                    
										CASE WHEN ISNULL(bitFreeShipping, 0) = 0 THEN ''False''
											 ELSE ''True''
										END IsFreeShipping,
										CASE WHEN ISNULL(bitAllowBackOrder, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Allow Back Orders],
										CASE WHEN ISNULL(bitCalAmtBasedonDepItems, 0) = 0
											 THEN ''False''
											 ELSE ''True''
										END CalAmtBasedonDepItems,
										CASE WHEN ISNULL(bitAssembly, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Assembly] , 
										CASE WHEN ISNULL(IsArchieve, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Archive],       
										(SELECT  ISNULL(vcData, '''') FROM  dbo.ListDetails WHERE numDomainID = 1 AND numListItemID = numItemClass AND numListID = 381) AS [Item Class],
										(SELECT  ISNULL(vcData, '''') FROM  dbo.ListDetails WHERE numDomainID = 1 AND numListItemID = numShipClass AND numListID = 461) AS [Shipping Class]
										' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) + '
								FROM    Item I  left outer join  (select * from CompanyAssets CA where CA.numDomainID=',@numDomainID,') CA
										ON CA.numItemCode=I.numItemCode
									' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN CONCAT('OUTER APPLY
										(
											SELECT 
											* 
											FROM 
											(
												SELECT  
													CFW_Fld_Master.Fld_label,
													ISNULL(dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Item.Fld_ID, CFW_Fld_Values_Item.RecId, CFW_Fld_Values_Item.Fld_Value),'''') Fld_Value 
												FROM 
													CFW_Fld_Master
												LEFT JOIN 
													CFW_Fld_Values_Item
												ON
													CFW_Fld_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
												WHERE 
													RecId = I.numItemCode
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label  IN (',@CustomFields,') ) AS pvt
										) CustomFields') ELSE '' END) + ' WHERE   I.[numDomainiD] = ',@numDomainID)

	
					EXEC (@vcSQL)
            
					SELECT  [numVendorID] VendorID,
							[vcPartNo] [Vendor Part#],
							[monCost] [Vendor Cost],
							[numItemCode] [Item ID],
							[intMinQty] [Vendor Minimum Qty]
					FROM    [Vendor]
					WHERE   [numDomainID] = @numDomainID
			--Get field name 
            SELECT  *
            FROM    [CFW_Fld_Master]
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.[numDomainID] = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
                    AND CFW_Fld_Master.Grp_id = 5 -- 5 for Item details
				
                --Get values
            SELECT  [FldDTLID],
                    CF.[Fld_ID],
                    CM.[Fld_label],
                    CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                         THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                         ELSE CF.[Fld_Value]
                    END Fld_Value,
                    [RecId]
            FROM    [CFW_FLD_Values_Item] CF
                    INNER JOIN [Item] I ON I.[numItemCode] = CF.[RecId]
                    INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
            WHERE   I.[numDomainId] = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
                    AND CM.Grp_id = 5
                    AND CM.numDomainID = @numDomainID
                
        END
			
    IF @tintTable = 9 
        BEGIN
            SELECT  c.numAccountId ChartOfAccountID,
                    AT.[numAccountTypeID] AccountTypeID,
                    c.[numParntAcntTypeId] ParentAcntTypeID,
                    c.[vcAccountCode] AccountCode,
                    c.vcAccountName AS AccountName,
                    AT.[vcAccountType] AccountType,
                    CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
                         ELSE c.numOpeningBal
                    END AS OpeningBal,
                    c.[vcAccountDescription] Description,
                    C.[dtOpeningDate] OpeningDate,
					CASE WHEN ISNULL(c.[bitFixed], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsFixed,
                    CASE WHEN ISNULL(c.[bitDepreciation], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsDepreciation,
                    CASE WHEN ISNULL(c.[bitOpeningBalanceEquity], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsOpeningBalanceEquity,
					c.[monEndingOpeningBal] EndingOpeningBalace,
					c.[monEndingBal] EndingBalance,
					c.[dtEndStatementDate] EndStatementDate,
					CASE WHEN ISNULL(c.[bitProfitLoss], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsProfitLoss,
                    c.[vcNumber] AS [AccountNumber]
					,CASE WHEN ISNULL(c.[bitIsSubAccount], 0) = 0 THEN 'False'
                         ELSE 'True'
                     END [IsSubAccount]
					,CCC.vcAccountName AS [ParentAccountName]
					,CASE WHEN ISNULL(c.[IsBankAccount], 0) = 0 THEN 'False'
                         ELSE 'True'
                     END [IsBankAccount]
					,c.[vcStartingCheckNumber] AS [StartingCheckNumber]
					,c.[vcBankRountingNumber] AS [BankRountingNumber]
            FROM    Chart_Of_Accounts c
                    LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
                    LEFT JOIN Chart_Of_Accounts CCC ON c.numParentAccId = Ccc.numAccountId
            WHERE   c.numDomainId = @numDomainID
            ORDER BY c.numAccountID ASC
            
--            EXEC USP_ChartofChildAccounts @numDomainId = 72,
--                @numUserCntId = 17, @strSortOn = 'ID',
--                @strSortDirection = 'ASCENDING'
			
        END

        
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @numDivisionId AS NUMERIC(9) = NULL,
      @numAccountClass AS NUMERIC(9)=0,
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
AS 
    BEGIN 
  
  		SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
		SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

		PRINT @dtFromDate
		PRINT @dtToDate

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount MONEY NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount MONEY NULL,numTotal MONEY NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numDomainId ;
        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(OB.monAmountPaid
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                --AND dtShippedDate IS NULL
                        AND opp.numDomainID = @numDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate],OB.monAmountPaid
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(OB.monAmountPaid
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT  @numDomainId,
                        0 numOppId,
                        BH.numDivisionId,
                        0 numBizDocsId,
                        ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
                        0 numBizDocId,
                        BH.dtDueDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
                FROM    BillHeader BH
                WHERE   BH.numDomainID = @numDomainId
                        AND ( BH.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                         AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0   
                         AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						 AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY BH.numDivisionId,
                        BH.dtDueDate
                HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND [GJH].[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
                UNION ALL
				--Add Commission Amount
                SELECT  @numDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numDomainId
                        and BDC.numDomainID = @numDomainId
                        AND (D.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                        AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
						AND ISNULL(BDC.bitCommisionPaid,0)=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
					AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numDomainId   
		AND (BPH.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )



--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 0
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  #TempAPRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    #TempAPRecord
WHERE   numUserCntID =@numDomainId 
GROUP BY numDivisionId
) X WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numThirtyDays, 0) + isnull(numThirtyDaysOverDue, 0)
                + isnull(numSixtyDays, 0) + isnull(numSixtyDaysOverDue, 0)
                + isnull(numNinetyDays, 0) + isnull(numNinetyDaysOverDue, 0)
                + isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numDomainId AND (numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

        SET NOCOUNT OFF   
    
    END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
    SET ANSI_NULLS ON

/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].USP_GetAccountPayableAging_Details 1,7281,'90-'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountPayableAging_Details')
DROP PROCEDURE USP_GetAccountPayableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging_Details](
               @numDomainId   AS NUMERIC(9)  = 0,
               @numDivisionID NUMERIC(9),
               @vcFlag        VARCHAR(20))
AS
  BEGIN
	DECLARE @strSqlJournal VARCHAR(8000);
	DECLARE @strSqlOrder VARCHAR(8000);
	Declare @strSqlCommission varchar(8000);
	Declare @strSqlBill varchar(8000);
	Declare @strSqlCheck varchar(8000);
	
    DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
       ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      

     
    --Get Master data
	 
	 SET @strSqlBill = 'SELECT  
						  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numOppId,
						   ''Bill'' vcPOppName,
						   -2 AS [tintOppType],-- flag for bills 
						   0 numOppBizDocsId,
						   CASE WHEN len(BH.vcReference)=0 THEN '''' ELSE BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as TotalAmount,
						   ISNULL(BH.monAmtPaid, 0) as AmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   BH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   BH.dtDueDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,BH.numBillID,0 AS numCheckHeaderID
					FROM    
							BillHeader BH 
					WHERE   
							BH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
							AND BH.numDivisionId = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0 '
			 
		SET @strSqlCheck=' UNION
              SELECT  
						   0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS [tintOppType],-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS [vcBizDocID],
						   ISNULL(CD.monAmount, 0) as TotalAmount,
						   ISNULL(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   [dbo].[FormatedDateFromDate](CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 AS numBillID,CH.numCheckHeaderID
					FROM   CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
					WHERE CH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
                        AND CD.numCustomerId = '+ CONVERT(VARCHAR(15),@numDivisionID)
			SET @strSqlOrder=' UNION 
				SELECT 
               OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               isnull(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.[monPAmount]
               (OB.[monAmountPaid] * OM.fltExchangeRate) AmountPaid,
               isnull(OB.monDealAmount * OM.fltExchangeRate
                        - OB.[monAmountPaid] * OM.fltExchangeRate,0)  BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,0 as numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativePurchaseBizDocId,0)) +' 
               AND DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - OB.[monAmountPaid] * OM.fltExchangeRate,0) > 0 '
		
		SET @strSqlCommission=' UNION 
		 SELECT  OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               BDC.numComissionAmount,
               0 AmountPaid,
               BDC.numComissionAmount BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,BDC.numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM    BizDocComission BDC JOIN  OpportunityBizDocs OB
                ON BDC.numOppBizDocId = OB.numOppBizDocsId
				join OpportunityMaster OM on OM.numOppId=OB.numOppId
				JOIN Domain D on D.numDomainID=BDC.numDomainID
				  LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=isnull(OBDD.numBizDocsPaymentDetId,0)
        WHERE   OM.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' and 
				 BDC.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' 
				AND ISNULL(BDC.bitCommisionPaid,0)=0
				AND OB.bitAuthoritativeBizDocs = 1
				AND isnull(OBDD.bitIntegratedToAcnt,0) = 0
                AND D.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) 
             
							
                
    --Show Impact of AR journal entries in report as well 
    	
SET @strSqlJournal = '    	
		UNION 
		 SELECT 
				GJH.numJournal_Id [numOppId],
				''Journal'' [vcPOppName],
				-1 [tintOppType],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				1 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 numBizDocsPaymentDetId,
				1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01020102%''
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND GJH.numJournal_Id NOT IN (
				--Exclude Bill and Bill payment entries
				SELECT GH.numJournal_Id FROM dbo.General_Journal_Header GH INNER JOIN dbo.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
				WHERE GH.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
				) AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0
				'
    
    
    IF (@vcFlag = '0+30')
      BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlOrder =@strSqlOrder +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                                                                                                     
			SET @strSqlCommission =@strSqlCommission +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                               
          SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
            
            SET @strSqlOrder =@strSqlOrder +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                                          
			SET @strSqlCommission =@strSqlCommission +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                 
           SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
               
            SET @strSqlOrder =@strSqlOrder +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                                            
			SET @strSqlCommission =@strSqlCommission +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                   
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
            SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'

			SET @strSqlOrder =@strSqlOrder +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'                                     
                                                     
            SET @strSqlCommission =@strSqlCommission +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'
                                                     
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
              SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate ,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                    
                    SET @strSqlOrder =@strSqlOrder +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
                                                                     
					SET @strSqlCommission =@strSqlCommission +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
				SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                               
				SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                
                SET @strSqlOrder =@strSqlOrder +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                                           
						SET @strSqlCommission =@strSqlCommission +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                      
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
                  SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                                               
	              SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                        
                        SET @strSqlOrder =@strSqlOrder +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                                     
						SET @strSqlCommission =@strSqlCommission +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                        
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                    SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) > 90 '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) > 90 '
                             
                         SET @strSqlOrder =@strSqlOrder +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                                  
						SET @strSqlCommission =@strSqlCommission +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                          
						SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) > 90 '
                    END
                  ELSE
                    BEGIN
						SET @strSqlBill =@strSqlBill +''
						SET @strSqlCheck =@strSqlCheck +''
						SET @strSqlOrder =@strSqlOrder +''
						SET @strSqlCommission =@strSqlCommission +''
						SET @strSqlJournal =@strSqlJournal +''
                    END
                    
                    
	PRINT (@strSqlBill)
	PRINT (@strSqlCheck)
	PRINT (@strSqlOrder)
	PRINT (@strSqlCommission)
	PRINT (@strSqlJournal)
	
	EXEC(@strSqlBill + @strSqlCheck + @strSqlOrder + @strSqlCommission + @strSqlJournal)
  END
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess                                        
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId     
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetDashboardAllowedReports')
DROP PROCEDURE Usp_GetDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[Usp_GetDashboardAllowedReports]        
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@tintMode AS TINYINT=0
as                                                 
      
IF @tintMode=0 --All Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	LEFT join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	where RLM.numDomainID=@numDomainID
	order by RLM.numReportID
END    
ELSE IF @tintMode=1 --Only Allowed Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID 
	where RLM.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	order by RLM.numReportID 
END   
ELSE IF @tintMode=2 --All KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                 
	LEFT join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	where KPI.numDomainID=@numDomainID
	order by KPI.numReportKPIGroupID
END    
ELSE IF @tintMode=3 --Only Allowed KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                  
	join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID 
	where KPI.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	order by KPI.numReportKPIGroupID 
END   
ELSE IF @tintMode=4 --Default Reports
BEGIN
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
	FROM 
		ReportListMaster RLM 
	INNER JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END
ELSE IF @tintMode=5 -- Default Reports With Permission
BEGIN
	SELECT 
		RLM.intDefaultReportID
		,RLM.vcReportName
		,RLM.vcReportDescription 
		,CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	FROM 
		ReportListMaster RLM 
	LEFT JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.numDomainID,0)=0
		AND ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END   
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(50)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SG' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID   AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SM' --Reminer,snooze time
				BEGIN
					INSERT INTO #temp 
					SELECT '5','5 minutes'
					UNION 
					SELECT '15','15 minutes'
					UNION 
					SELECT '30','30 Minutes'
					UNION 
					SELECT '60','1 Hour'
					UNION 
					SELECT '240','4 Hour'
					UNION 
					SELECT '480','8 Hour'
					UNION 
					SELECT '1440','24 Hour'

				END
			 ELSE IF @vcListItemType = 'PT' --Pin To
				BEGIN
					INSERT INTO #temp 
					SELECT '1','Sales Opportunity'
					UNION 
					SELECT '2','Purchase Opportunity'
					UNION 
					SELECT '3','Sales Order'
					UNION 
					SELECT '4','Purchase Order'
					UNION 
					SELECT '5','Project'
					UNION 
					SELECT '6','Cases'
					UNION 
					SELECT '7','Item'

				END
				ELSE IF @vcListItemType = 'DV' --Customer
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
					SELECT d.numDivisionID,a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ' ' + dbo.fn_getlistitemname(d.numCompanyDiff) + ':' + isnull(d.vcCompanyDiff,'') else '' end as vcCompanyname from   companyinfo AS a                      
					join divisionmaster d                      
					on  a.numCompanyid=d.numCompanyid
					WHERE D.numDomainID=@numDomainID
			
				END
				ELSE IF @vcListItemType = 'BD' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
						SELECT numTermsID,vcTerms	
						FROM dbo.BillingTerms 
						WHERE 
						numDomainID = @numDomainID
						AND ISNULL(bitActive,0) = 1
			
				END
				ELSE IF @vcListItemType = 'BT' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
							SELECT numBizDocTempID,    
							vcTemplateName
							FROM BizDocTemplate  
							WHERE [numDomainID] = @numDomainID and tintTemplateType=0
			
				END
				ELSE IF @vcListItemType = 'PSS' --Parcel Shipping Service
				BEGIN
					INSERT INTO #temp (numID,vcData) VALUES
						(10,'Priority Overnight')
                        ,(11,'Standard Overnight')
                        ,(12,'First Overnight')
                        ,(13,'FedEx 2nd Day')
                        ,(14,'FedEx Express Saver')
                        ,(15,'FedEx Ground')
                        ,(16,'Ground Home Delivery')
                        ,(17,'FedEx 1 Day Freight')
                        ,(18,'FedEx 2 Day Freight')
                        ,(19,'FedEx 3 Day Freight')
                        ,(20,'International Priority')
                        ,(21,'International Priority Distribution')
                        ,(22,'International Economy')
                        ,(23,'International Economy Distribution')
                        ,(24,'International First')
                        ,(25,'International Priority Freight')
                        ,(26,'International Economy Freight')
                        ,(27,'International Distribution Freight')
                        ,(28,'Europe International Priority')
                        ,(40,'UPS Next Day Air')
                        ,(42,'UPS 2nd Day Air')
                        ,(43,'UPS Ground')
                        ,(48,'UPS 3Day Select')
                        ,(49,'UPS Next Day Air Saver')
                        ,(50,'UPS Saver')
                        ,(51,'UPS Next Day Air Early A.M.')
                        ,(55,'UPS 2nd Day Air AM')
                        ,(70,'USPS Express')
                        ,(71,'USPS First Class')
                        ,(72,'USPS Priority')
                        ,(73,'USPS Parcel Post')
                        ,(74,'USPS Bound Printed Matter')
                        ,(75,'USPS Media')
                        ,(76,'USPS Library')

				END
				ELSE IF @vcDbColumnName='charSex'
					BEGIN
							INSERT INTO #temp
							SELECT 'M','Male' UNION
							SELECT 'F','Female'
						END
				ELSE IF @vcDbColumnName='tintOppStatus'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Deal Won' UNION
							SELECT 2,'Deal Lost'
					END
				ELSE IF @vcDbColumnName='tintOppType'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Sales Order' UNION
							SELECT 2,'Purchase Order' UNION
							SELECT 3,'Sales Opportunity' UNION
							SELECT 4,'Purchase Opportunity' 
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						INSERT INTO #temp
						SELECT -1,'Global' UNION
						SELECT
							numWLocationID,
							ISNULL(vcLocation,'')
						FROM
							WarehouseLocation
						WHERE
							numDomainID=@numDomainID
				
					END
					ELSE IF @vcDbColumnName = 'tintPriceLevel'
					BEGIN
						DECLARE @Temp TABLE
						(
							Id INT
						)
						INSERT INTO @Temp
						(
							Id
						)
						SELECT DISTINCT 
							ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
						FROM 
							[PricingTable] pt
						INNER JOIN 
							Item
						ON 
							Item.numItemCode = pt.numItemCode 
							AND Item.numDomainID = @numDomainID
						WHERE 
							tintRuleType = 3
						ORDER BY [Id] 

						INSERT INTO 
							#temp
						SELECT 
							pt.Id
							,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
						FROM 
							@Temp pt
						LEFT JOIN
							PricingNamesTable pnt
						ON 
							pt.Id = pnt.tintPriceLevel
						AND 
							pnt.numDomainID = @numDomainID				
					END
--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 INSERT INTO #temp  SELECT 0,'--None--'				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	select numItemCode,vcItemName from Item where numDomainID =@numDomainID   and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF (SELECT ISNULL(bitAssembly,0) FROM Item  WHERE numItemCode = @numItemCode) = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,CONCAT(vcItemName,CASE WHEN ISNULL(dbo.fn_GetItemAttributes(@numDomainID,numItemCode),'') <> '' THEN CONCAT(' (',dbo.fn_GetItemAttributes(@numDomainID,numItemCode),')') ELSE '' END) vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND (vcItemName like @strItem+'%' OR vcSKU like @strItem+'%' OR vcModelID like @strItem+'%' OR numBarcodeID LIKE @strItem+'%')
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     ((SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0 OR ISNULL(bitAssembly,0) = 1)) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
	END
	ELSE
	BEGIN
		--ONLY NON INVENTORY, SERVICE, INVENTORY ITEMS AND KIT ITEMS WITHOUT HAVING ANOTHER KIT AS SUB ITEM WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY

		SELECT 
			numItemCode,
			vcItemName,
			bitKitParent,
			bitAssembly 
		FROM 
			Item 
		WHERE 
			numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
			AND numItemCode <> @numItemCode
			AND 1= (
					CASE 
						WHEN charitemtype='P' THEN
						CASE 
							WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode)>0 
							THEN 1 
							ELSE 0 
						END 
						ELSE 1 
					END)
			AND 1 = (CASE 
						WHEN Item.bitKitParent = 1 
						THEN 
							(
								CASE 
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item I ON ItemDetails.numChildItemID = I.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(I.bitKitParent,0) = 1) > 0
									THEN 0
									ELSE 1
								END
							)
						ELSE 1 
					END)
	END
END				
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitems')
DROP PROCEDURE usp_getmasterlistitems
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItems]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0          
as          
BEGIN          
SELECT 
	Ld.numListItemID
	,ISNULL(LDN.vcName,ISNULL(vcRenamedListName,vcData)) AS vcData
	,ISNULL(ld.numListType,0) AS numListType  
FROM 
	ListDetails Ld        
LEFT JOIN 
	ListOrder LO 
ON 
	Ld.numListItemID= LO.numListItemID 
	AND Lo.numDomainId = @numDomainID
LEFT JOIN
	ListDetailsName LDN
ON
	LDN.numDomainID = @numDomainID
	AND LDN.numListID = @ListID
	AND LDN.numListItemID = Ld.numListItemID
WHERE 
	1 = (CASE WHEN @ListID=338 THEN (CASE WHEN Ld.numListID=@ListID OR Ld.numListID=82 THEN 1 ELSE 0 END) ELSE (CASE WHEN Ld.numListID=@ListID THEN 1 ELSE 0 END) END) AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
ORDER BY 
	ISNULL(intSortOrder,LD.sintOrder)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsUsingDomainID]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsusingdomainid')
DROP PROCEDURE usp_getmasterlistitemsusingdomainid
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingDomainID]            
@numDomainID as numeric(9)=0,            
@numListID as numeric(9)=0          
AS
BEGIN            
	SELECT 
		Ld.numListItemID
		,ISNULL(LDN.vcName,vcData) AS vcData
		,Ld.constFlag
		,bitDelete
		,ISNULL(intSortOrder,0) intSortOrder
		,ISNULL(numListType,0) as numListType
		,(SELECT vcData FROM ListDetails WHERE numListId=27 and numListItemId=ld.numListType) AS vcListType
		,(CASE WHEN ld.numListType=1 THEN 'Sales' WHEN ld.numListType=2 THEN 'Purchase' ELSE 'All' END) AS vcOrderType
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END) AS vcOppOrOrder
		,numListType
		,tintOppOrOrder
	FROM 
		ListDetails LD          
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID = LO.numListItemID 
		AND lo.numDomainId = @numDomainID
	LEFT JOIN 
		ListDetailsName LDN 
	ON 
		LDN.numDOmainID=@numDomainID 
		AND LDN.numListID=@numListID 
		AND LDN.numListItemID=LD.numListItemID
	WHERE 
		(Ld.numDomainID=@numDomainID OR Ld.constFlag=1) 
		AND Ld.numListID=@numListID           
	ORDER BY 
		intSortOrder
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMasterListItemsUsingListType')
DROP PROCEDURE USP_GetMasterListItemsUsingListType
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingListType]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0,
@numListType numeric(9,0),
@tintOppOrOrder TINYINT     
as          
          
SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData,ISNULL(ld.numListType,0) as numListType FROM listdetails Ld        
left join listorder LO 
on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID )
and ISNULL(ld.numListType,0) in (0,@numListType) AND ISNULL(ld.tintOppOrOrder,0) IN (0,@tintOppOrOrder)
order by ISNULL(intSortOrder,LD.sintOrder)

--select * from ListDetails


--SELECT Ld.numListItemID, vcData FROM listdetails Ld        
--left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID   
--WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID)      
--order by intSortOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
-- USP_GetOpportunitySource 1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySource')
DROP PROCEDURE USP_GetOpportunitySource 
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySource]
          @numDomainID NUMERIC(9)
AS
CREATE TABLE #temp (numItemID/*numListItemID*/ VARCHAR(50),vcItemName VARCHAR(100), constFlag BIT, bitDelete BIT,intSortOrder INT, numListItemID NUMERIC(9),vcData VARCHAR(100), vcListType VARCHAR(1000), vcOrderType VARCHAR(50), vcOppOrOrder VARCHAR(20), numListType NUMERIC(18,0), tintOppOrOrder TINYINT)

INSERT INTO #temp
  SELECT '0~1','Internal Order', 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, -1 AS numListItemID,'Internal Order','Internal Order','','',0,0
  
  UNION
  
  SELECT CAST([numSiteID] AS VARCHAR(250)) +'~2',[vcSiteName], 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, CAST([numSiteID] AS VARCHAR(18)) AS numListItemID,[vcSiteName],[Sites].[vcSiteName],'','',0,0
  FROM   Sites WHERE  [numDomainID] = @numDomainID
		
  UNION
  
  SELECT DISTINCT CAST(WebApiId  AS VARCHAR(250)) +'~3',vcProviderName,1 constFlag, 1 bitDelete, 0 intSortOrder, CAST(WebApiId  AS VARCHAR(18)) AS numListItemID,vcProviderName,[WebAPI].[vcProviderName],'','',0,0
  FROM dbo.WebAPI   

  UNION
  
  SELECT CAST(Ld.numListItemID  AS VARCHAR(250)) +'~1', isnull(vcRenamedListName,vcData) as vcData, Ld.constFlag,bitDelete,isnull(intSortOrder,0) intSortOrder,CAST(Ld.numListItemID  AS VARCHAR(18)),isnull(vcRenamedListName,vcData) as vcData,
		(SELECT vcData FROM ListDetails WHERE numListId=9 and numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID),(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END)
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END),numListType,tintOppOrOrder
		FROM listdetails Ld  left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=9 and (constFlag=1 or Ld.numDomainID=@numDomainID )  
		 
SELECT * FROM #temp	ORDER BY bitDelete DESC
DROP TABLE #temp		 	

GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderType')
DROP PROCEDURE USP_GetOrderType
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thJune2014>
-- Description:	<Description,,Get only Order type :Sales /or Purchase>
-- =============================================
Create PROCEDURE [dbo].[USP_GetOrderType]
	-- Add the parameters for the stored procedure here
	@numOppId numeric (18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT tintOppType,tintOppStatus from OpportunityMaster where numOppId=@numOppId
END

/****** Object:  StoredProcedure [dbo].[usp_GetPageLevelUserRights]    Script Date: 07/26/2008 16:18:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageleveluserrights')
DROP PROCEDURE usp_getpageleveluserrights
GO
CREATE PROCEDURE [dbo].[usp_GetPageLevelUserRights]          
 @numUserID numeric,          
 --@vcFileName varchar (100),          
 @numModuleID numeric=0,          
 @numPageID numeric=0             
--          
As           
declare @numDomainID as numeric(9)  
declare @numAdminID as numeric(9)  
declare @numUserDetailId as numeric(9)

select  @numDomainID=numDomainID,@numUserDetailId=numUserDetailId from UserMaster where numUserID=@numUserID  
select @numAdminID=numAdminID from domain where numDomainID=@numDomainID  

if @numAdminID=@numUserDetailId  
 SELECT top 1 UM.numUserId, UM.vcUserName,
-- @vcFileName as vcFileName,        
   @numModuleID as numModuleID, @numPageID as numPageID, 3 As intExportAllowed,         
   3 As intPrintAllowed, 3 As intViewAllowed,         
  3 As intAddAllowed, 3 As intUpdateAllowed,         
   3 As intDeleteAllowed        
   FROM UserMaster UM     
   WHERE UM.numUserId= @numUserID      
     
else  
       
        
 SELECT top 1 UM.numUserId, UM.vcUserName, PM.vcFileName,        
   GA.numModuleID, GA.numPageID, MAX(GA.intExportAllowed) As intExportAllowed,         
   MAX(GA.intPrintAllowed) As intPrintAllowed, MAX(GA.intViewAllowed) As intViewAllowed,         
   MAX(GA.intAddAllowed) As intAddAllowed, MAX(GA.intUpdateAllowed) As intUpdateAllowed,         
   MAX(GA.intDeleteAllowed) As intDeleteAllowed        
   FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID=UM.numGroupID LEFT JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID
   WHERE         
	GA.numModuleID=@numModuleID          
    AND GA.numPageID=@numPageID      
	AND UM.numUserId= @numUserID      
   GROUP BY UM.numUserId, UM.vcUserName,        
    PM.vcFileName, GA.numModuleID, GA.numPageID
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPrimaryListItemID')
DROP PROCEDURE USP_GetPrimaryListItemID 
GO
--exec USP_GetPrimaryListItemID 1,1,1593,5,7074 
CREATE PROCEDURE USP_GetPrimaryListItemID 
@numDomainID NUMERIC,
@tintMode TINYINT=0,
@numFormFieldId NUMERIC,
@numPrimaryListID NUMERIC,
@numRecordID NUMERIC,
@bitCustomField bit
AS 
BEGIN
	DECLARE @fld_id NUMERIC 
	DECLARE @numFormID NUMERIC
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @numCompanyID NUMERIC
	DECLARE @strSQL VARCHAR(4000)

	IF @tintMode=1 OR @tintMode=5 -- 1 for organization, 5 for contact
	BEGIN
	
		-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				DECLARE @strTemp VARCHAR(100)
				IF @tintMode =1 SET @strTemp ='CFW_FLD_Values'
				ELSE IF @tintMode =5 SET @strTemp ='CFW_FLD_Values_Cont'
			
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.'+ @strTemp + ' WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE
			BEGIN
				IF @numPrimaryListID = 834 -- 834: Shipping Zone
				BEGIN
					SELECT numShippingZone FROM dbo.AddressDetails AD2 INNER JOIN [State] ON Ad2.numState=[State].numStateID WHERE AD2.numDomainID=@numDomainID AND AD2.numRecordID= @numRecordID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
				END
				ELSE
				BEGIN
					SELECT @numFormID =numFormID FROM View_DynamicDefaultColumns WHERE numFieldId=@numFormFieldId AND numDomainID= @numDomainID
				
					SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
					numFormID=@numFormID AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
				
					IF @vcLookBackTableName = 'CompanyInfo'
					BEGIN
						SELECT @numCompanyID= numCompanyId FROM dbo.DivisionMaster WHERE numDivisionID=@numRecordID AND numDomainID = @numDomainID
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.CompanyInfo WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numCompanyId = ' + CONVERT(VARCHAR(15),@numCompanyID)
					END
					ELSE IF @vcLookBackTableName = 'DivisionMaster'
					BEGIN
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.DivisionMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numDivisionID = ' + CONVERT(VARCHAR(15),@numRecordID)
					END
					ELSE IF @vcLookBackTableName = 'AdditionalContactsInformation'
					BEGIN
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.AdditionalContactsInformation WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numContactId = ' + CONVERT(VARCHAR(15),@numRecordID)
					END
					ELSE
						SELECT 0
				END
			END 
		
		EXEC(@strSQL);
	END		
	IF @tintMode=2 -- Project
	BEGIN
			-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				
				
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_FLD_Values_Pro WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=13 AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.ProjectsMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numProId = ' + CONVERT(VARCHAR(15),@numRecordID)
				
			END
			
			EXEC(@strSQL);
	END
	IF @tintMode=3 -- Opportunity
	BEGIN
		-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_Fld_Values_Opp WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT @numFormID =numFormID FROM View_DynamicDefaultColumns WHERE numFieldId=@numFormFieldId AND numDomainID= @numDomainID
		
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=@numFormID AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.OpportunityMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numOppID = ' + CONVERT(VARCHAR(15),@numRecordID)
				
			END
			EXEC(@strSQL);
		
	END
	IF @tintMode=4 -- Cases
	BEGIN
			-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_FLD_Values_Case WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=12 AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.Cases WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numCaseId = ' + CONVERT(VARCHAR(15),@numRecordID)
			END
			EXEC(@strSQL);
	END

		
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportListMaster')
DROP PROCEDURE USP_GetReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportListMaster]      
@numDomainId as numeric(9),   
@numReportModuleID as numeric(9),   
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr AS VARCHAR(50),
@tintReportType INT,
@tintPerspective INT,
@vcTimeline VARCHAR(100)
AS
BEGIN       
    SET NOCOUNT ON
     
	Create table #tempTable
	(
		ID INT IDENTITY PRIMARY KEY,                                                         
		numReportID NUMERIC(18,0)                                                       
	)     
	DECLARE @strSql AS VARCHAR(8000)                                                  
    
	SET  @strSql='Select numReportID from ReportListMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
	IF @numReportModuleID <> 0 
	BEGIN
		SET @strSql=@strSql + ' And numReportModuleID = '+ convert(varchar(15),@numReportModuleID) +''   
	END

	IF @SearchStr <> '' 
	BEGIN
		SET @strSql=@strSql + ' AND LOWER(vcReportName) like ''%'+ LOWER(@SearchStr) +'%'''
	END

	IF @tintReportType <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintReportType = ',@tintReportType)
	END

	IF @tintPerspective <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintRecordFilter = ',@tintPerspective)
	END

	IF LEN(ISNULL(@vcTimeline,'')) > 0 AND ISNULL(@vcTimeline,'') <> '-1'
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND vcDateFieldValue = ''',@vcTimeline,'''')
	END
    
	SET  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
	INSERT INTO #tempTable(numReportID) EXEC(@strSql)    
    
	DECLARE @firstRec as integer                                                        
	DECLARE @lastRec as integer                                                        
	SET @firstRec= (@CurrentPage-1) * @PageSize                                                        
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                         

	SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,RMM.vcModuleName
		,RMGM.vcGroupName
		,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
		,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
		,(CASE 
			WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
			THEN 
				CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
				(CASE RLM.vcDateFieldValue
					WHEN 'AllTime' THEN 'All Time'
					WHEN 'Custom' THEN 'Custom'
					WHEN 'CurYear' THEN 'Current CY'
					WHEN 'PreYear' THEN 'Previous CY'
					WHEN 'Pre2Year' THEN 'Previous 2 CY'
					WHEN 'Ago2Year' THEN '2 CY Ago'
					WHEN 'NextYear' THEN 'Next CY'
					WHEN 'CurPreYear' THEN 'Current and Previous CY'
					WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
					WHEN 'CurNextYear' THEN 'Current and Next CY'
					WHEN 'CuQur' THEN 'Current CQ'
					WHEN 'CurNextQur' THEN 'Current and Next CQ'
					WHEN 'CurPreQur' THEN 'Current and Previous CQ'
					WHEN 'NextQur' THEN 'Next CQ'
					WHEN 'PreQur' THEN 'Previous CQ'
					WHEN 'LastMonth' THEN 'Last Month'
					WHEN 'ThisMonth' THEN 'This Month'
					WHEN 'NextMonth' THEN 'Next Month'
					WHEN 'CurPreMonth' THEN 'Current and Previous Month'
					WHEN 'CurNextMonth' THEN 'Current and Next Month'
					WHEN 'LastWeek' THEN 'Last Week'
					WHEN 'ThisWeek' THEN 'This Week'
					WHEN 'NextWeek' THEN 'Next Week'
					WHEN 'Yesterday' THEN 'Yesterday'
					WHEN 'Today' THEN 'Today'
					WHEN 'Tomorrow' THEN 'Tomorrow'
					WHEN 'Last7Day' THEN 'Last 7 Days'
					WHEN 'Last30Day' THEN 'Last 30 Days'
					WHEN 'Last60Day' THEN 'Last 60 Days'
					WHEN 'Last90Day' THEN 'Last 90 Days'
					WHEN 'Last120Day' THEN 'Last 120 Days'
					WHEN 'Next7Day' THEN 'Next 7 Days'
					WHEN 'Next30Day' THEN 'Next 30 Days'
					WHEN 'Next60Day' THEN 'Next 60 Days'
					WHEN 'Next90Day' THEN 'Next 90 Days'
					WHEN 'Next120Day' THEN 'Next 120 Days'
					ELSE '-'
				END),')')
			ELSE 
				'' 
		END) AS vcTimeline,
		(CASE WHEN (SELECT COUNT(*) FROM ReportFilterList WHERE numReportID=RLM.numReportID) > 0 THEN STUFF((SELECT DISTINCT ', ' + vcFilterValue FROM ReportFilterList WHERE numReportID=RLM.numReportID AND numFieldID=199 FOR XML PATH('')),1,2,'') ELSE '' END) AS vcLocationSource
	FROM 
		ReportListMaster RLM 
	JOIN 
		#tempTable T 
	ON 
		T.numReportID=RLM.numReportID 
	JOIN 
		ReportModuleMaster RMM 
	ON 
		RLM.numReportModuleID=RMM.numReportModuleID
	JOIN 
		ReportModuleGroupMaster RMGM 
	ON 
		RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	WHERE 
		ID > @firstRec 
		AND ID < @lastRec 
	ORDER BY
		ID
   
	DROP TABLE #tempTable
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @SortCol VARCHAR(50),
      @SortDirection VARCHAR(4),
      @CurrentPage INT,
      @PageSize INT,
      @TotRecs INT OUTPUT,
      @numUserCntID NUMERIC,
      @vcBizDocStatus VARCHAR(500),
      @vcBizDocType varchar(500),
      @numDivisionID as numeric(9)=0,
      @ClientTimeZoneOffset INT=0,
      @bitIncludeHistory bit=0,
      @vcOrderStatus VARCHAR(500),
      @vcOrderSource VARCHAR(1000),
	  @bitFulfilled BIT,
	  @numOppID AS NUMERIC(18,0) = 0,
	  @vcShippingService AS VARCHAR(MAX) = '',
	  @numShippingZone NUMERIC(18,0)=0
     )
AS 
    BEGIN

        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	  IF LEN(ISNULL(@SortCol,''))=0
		SET  @SortCol = 'dtCreatedDate'
		
	  IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'desc'
      
        DECLARE @strSql NVARCHAR(MAX)
        DECLARE @SELECT NVARCHAR(MAX)
        DECLARE @FROM NVARCHAR(MAX)
        DECLARE @WHERE NVARCHAR(MAX)
		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
	CREATE TABLE #temp (row INT,vcpoppname VARCHAR(300),numoppid NUMERIC,vcOrderStatus VARCHAR(100), vcInventoryStatus VARCHAR(800),
		numoppbizdocsid NUMERIC,numBizDocId NUMERIC,vcbizdocid VARCHAR(100),vcBizDocType VARCHAR(100),
		numbizdocstatus NUMERIC,vcbizdocstatus VARCHAR(100), bitFulfilled bit, dtFromDate VARCHAR(30),numDivisionID NUMERIC,
		vcCompanyName VARCHAR(300),tintCRMType TINYINT,vcFulfillmentStatus VARCHAR(MAX), bitPendingExecution BIT, numShipVia INT, vcShippingBox VARCHAR(MAX))
		
        SET @SELECT = 'WITH bizdocs AS 
						(SELECT 
							Row_number() OVER(ORDER BY obd.[numoppbizdocsid] DESC) AS row,
							om.vcpoppname,
							om.numoppid
							,dbo.fn_GetListItemName(om.[numstatus]) vcOrderStatus
							,(CASE WHEN CHARINDEX(''Back Order'',dbo.CheckOrderInventoryStatus(om.numOppID,om.numDomainID)) > 0 THEN REPLACE(dbo.CheckOrderInventoryStatus(om.numOppID,om.numDomainID),''Back Order '','''') ELSE '''' END) As vcInventoryStatus
							,obd.[numoppbizdocsid]
							,OBD.numBizDocId
							,obd.[vcbizdocid]
							,dbo.fn_GetListItemName(OBD.numBizDocId) AS vcBizDocType
							,obd.[numbizdocstatus]
							,dbo.fn_GetListItemName(obd.[numbizdocstatus]) vcbizdocstatus
							,ISNULL(OBD.bitFulfilled,0) bitFulfilled
							,CONCAT((CASE WHEN (SELECT COUNT(*) FROM OpportunityItemsReleaseDates WHERE numOppID=om.numOppID)>0 THEN ''* '' ELSE '''' END),[dbo].[FormatedDateFromDate](om.dtReleaseDate,'+ CONVERT(VARCHAR(15), @numDomainId) + ')) dtFromDate
							,om.numDivisionID
							,vcCompanyName
							,DM.tintCRMType
							,ISNULL(STUFF((SELECT CONCAT(''<br />'',vcMessage) FROM SalesFulfillmentLog WHERE numDomainID = OM.numDomainId AND numOppID=OM.numOppId AND ISNULL(bitSuccess,0) = 1 FOR XML PATH('''')) ,1,12,''''),'''') AS vcFulfillmentStatus
							, (CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = OM.numDomainId AND numOppId=OM.numOppId AND ISNULL(bitExecuted,0)=0) > 0 THEN 1 ELSE 0 END) as bitPendingExecution
							,ISNULL(om.numShipmentMethod,ISNULL(DM.intShippingCompany,90)) AS numShipVia
							,STUFF((SELECT CONCAT('', '',OpportunityBizDocs.numOppID,''~'',numOppBizDocsId,''~'',ISNULL(numShippingReportId,0),''~'',(CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0) > 0 THEN 1 ELSE 0 END)) vcText FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=om.numDomainID) AND OpportunityBizDocs.numOppId=om.numOppID AND OpportunityBizDocs.bitShippingGenerated=1 FOR XML PATH(''''), TYPE).value(''.'',''NVARCHAR(MAX)''),1,2,'' '') AS vcShippingBox'
			
		SET @FROM =' FROM opportunitymaster om INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = obd.[numoppid]
                     INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
					 Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID'
					
        SET @WHERE = ' WHERE (om.numOppID = ' +  CAST(@numOppID AS VARCHAR) + ' OR ' +  CAST(ISNULL(@numOppID,0) AS VARCHAR) + ' = 0) AND om.tintopptype = 1 and om.tintOppStatus=1 AND (ISNULL(OBD.bitFulfilled,0)=' + CAST(@bitFulfilled AS VARCHAR) + ' OR ' + CAST(@bitFulfilled AS VARCHAR) + ' = ''1'') AND OBD.numBizDocId=296 AND om.numDomainID = '+ CONVERT(VARCHAR(15), @numDomainId)
		
		IF  LEN(ISNULL(@vcBizDocStatus,''))>0
		BEGIN
			IF @bitIncludeHistory=1
				SET @WHERE = @WHERE + ' and (select count(*) from OppFulfillmentBizDocsStatusHistory WHERE numOppId=om.numOppId and numOppBizDocsId=obd.numOppBizDocsId and isnull(numbizdocstatus,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','','')))>0' 			
			ELSE	
				SET @WHERE = @WHERE + ' and obd.numbizdocstatus in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','',''))' 			
		END
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(om.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(om.intUsedShippingCompany,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(om.numOppId,om.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(om.numOppId,om.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		IF  LEN(ISNULL(@vcBizDocType,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(OBD.numBizDocId,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocType + ''','',''))' 			
		END
		
		SET @WHERE = @WHERE + ' AND (om.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = @SELECT + @FROM +@WHERE + ') insert into #temp SELECT * FROM [bizdocs] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)
        
        PRINT @strSql ;
        EXEC ( @strSql ) ;
       
       SELECT OM.*,ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
	FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainId WHERE numOppId=om.numOppId and numOppBizDocsId=OM.numOppBizDocsId FOR XML PATH('')),4,2000)),'') as vcBizDocsStatusList,
	CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID,ISNULL(vcItemDesc,'' ) AS vcItemDesc,ISNULL(monTotAmount,0) AS monTotAmount,
	vcFulfillmentStatus,
	bitPendingExecution,numShipVia,vcShippingBox
	FROM #temp OM 
		LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '') vcItemDesc,monTotAmount,
		Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
		FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode =@numShippingServiceItemID) 
		OI ON OI.row=1 AND OM.numOppId = OI.numOppId
	ORDER BY
		OM.row
       
       
       SELECT opp.numOppId,OBD.numOppBizDocID, OM.bitFulfilled, Opp.vcitemname AS vcItemName, dbo.GetOppItemReleaseDates(@numDomainId,Opp.numOppId,Opp.numoppitemtCode,0) AS vcItemReleaseDate,
				ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType, CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,  
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) AS vcAttributes,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,WL.vcLocation,OBD.numUnitHour AS numUnitHourOrig,
                        Opp.bitDropShip AS DropShip,SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN '('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,Opp.numWarehouseItmsID,opp.numoppitemtCode,vcFulfillmentStatus,bitPendingExecution
              FROM      OpportunityItems opp 
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        join #temp OM on opp.numoppid=OM.numoppid and OM.numOppBizDocsId=OBD.numOppBizDocID
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        INNER JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainId
                        INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID= WI.numWLocationID
			
              
        
        DROP TABLE #temp 
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
    END
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight]+[fltDimensionalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				ISNULL(vcTrackingNumber,'') AS vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(
				(CASE WHEN SUM(fltDimensionalWeight) > fltTotalRegularWeight
				      THEN SUM(fltDimensionalWeight) 
				      ELSE fltTotalRegularWeight+fltDimensionalWeight
				END), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				SUM(fltDimensionalWeight) AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,fltTotalRegularWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId       
                
                
    END
/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                SR.tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ( SELECT   SUM(fltTotalWeight)
                               FROM     dbo.ShippingReportItems
                               WHERE    numShippingReportId = SR.numShippingReportId
                             ) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(( SELECT SUM(fltDimensionalWeight)
                             FROM   dbo.ShippingBox
                             WHERE  numShippingReportId = SR.numShippingReportId
                           ), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(IsLargePackage,0) [IsLargePackage],
				ISNULL(vcCODType,0) [vcCODType],
				SR.numOppID,
				ISNULL(numCODAmount,0) [numCODAmount],
				ISNULL([SR].[numTotalInsuredValue],0) AS [numTotalInsuredValue],
				ISNULL([SR].[numTotalCustomsValue],0) AS [numTotalCustomsValue],
				ISNULL(tintSignatureType,0) AS tintSignatureType,
				OpportunityMaster.numShipFromWarehouse,
				ISNULL(vcDeliveryConfirmation,0) AS vcDeliveryConfirmation,
				ISNULL(SR.vcDescription,'') AS vcDescription
        FROM    ShippingReport AS SR
				INNER JOIN OpportunityMaster ON SR.numOppID = OpportunityMaster.numOppId
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
SET ANSI_NULLS ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowFormFieldMaster')
DROP PROCEDURE USP_GetWorkFlowFormFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowFormFieldMaster]     
    @numDomainID numeric(18, 0),
	@numFormID numeric(18, 0)
as                 
BEGIN
CREATE TABLE #tempField(numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT,vcGroup varchar(100),intWFCompare int)

--Regular Fields
INSERT INTO #tempField
	SELECT numFieldID,vcFieldName,
		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
		   vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit,ISNULL(vcGroup,'Custom Fields')as vcGroup,ISNULL(intWFCompare,0) as intWFCompare
		FROM View_DynamicDefaultColumns
		where numFormId=@numFormId and numDomainID=@numDomainID 
		AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0 AND 1 = (CASE 
																				WHEN @numFormId = 68
																				THEN (CASE WHEN vcGroup='Contact Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 70
																				THEN (CASE WHEN vcGroup='BizDoc Fields' OR vcGroup='Contact Fields' OR vcGroup='Milestone/Stage Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 94
																				THEN (CASE WHEN vcGroup='Project Fields' THEN 0 ELSE 1 END)
																				ELSE 1
																			END)


DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

IF @numFormID = 49
BEGIN
	IF LEN(ISNULL(@vcLocationID,'')) > 0
	BEGIN
		Select @vcLocationID = @vcLocationID + ',' + isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
	ELSE
	BEGIN
		Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
END
ELSE IF @numFormID = 94
BEGIN
	-- DO NOT SHOW CUSTOM FIELDS
	SET @vcLocationID = ''
END

IF @numFormID IN (69,70,49,72,73,124)
BEGIN
	IF LEN(ISNULL(@vcLocationID ,'')) > 0
	BEGIN
		SET @vcLocationID = CONCAT(@vcLocationID,',1,12,13,14')
	END
	ELSE
	BEGIN
		SET @vcLocationID = '1,12,13,14'
	END
END

--Custom Fields			
INSERT INTO #tempField
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,1 AS bitAllowEdit,(CASE WHEN Grp_id IN (1,12,13,14) THEN 'Organization Custom Fields' WHEN Grp_id=4 THEN 'Contact Custom Fields' WHEN Grp_id IN (2,6) THEN 'Opportunity & Order Custom Fields' WHEN Grp_id = 3 THEN 'Case Custom Fields' WHEN Grp_id=11 THEN 'Project Custom Fields' ELSE 'Custom Fields' END)  as vcGroup,0 as intWFCompare
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
					JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
            WHERE   CFM.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
 

SELECT *,/*vcOrigDbColumnName + '~' +*/ CAST(numFieldID AS VARCHAR(18)) + '_' + CAST(CASE WHEN bitCustom=1 THEN 'True' ELSE 'False' END AS VARCHAR(10)) AS ID  FROM #tempField
ORDER BY vcGroup,vcFieldName,bitCustom

DROP TABLE #tempField
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterDetail')
DROP PROCEDURE USP_GetWorkFlowMasterDetail
GO
Create PROCEDURE [dbo].[USP_GetWorkFlowMasterDetail]     
    @numDomainID numeric(18, 0),
	@numWFID numeric(18, 0)
as                 

SELECT * FROM WorkFlowMaster WF WHERE WF.numDomainID=@numDomainID AND WF.numWFID=@numWFID
--please

CREATE TABLE #tempField(numFieldID NUMERIC,  
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),  
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT)  
DECLARE @numFormId AS NUMERIC(18,0)
SELECT @numFormId=numFormID from dbo.WorkFlowMaster WHERE numDomainID=@numDomainID  AND numWFID=@numWFID

IF (@numFormId=71)
	BEGIN

	SET @numFormId=49
	
	END
--Regular Fields  
INSERT INTO #tempField  
 SELECT numFieldID,vcFieldName,  
     vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,  
     vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit  
  FROM View_DynamicDefaultColumns  
  where numFormId=@numFormId and numDomainID=@numDomainID   
  AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0  
  
  
DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'  
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId  
  
IF @numFormID IN (69,70,49,72,73,124)
BEGIN
	IF LEN(ISNULL(@vcLocationID ,'')) > 0
	BEGIN
		SET @vcLocationID = CONCAT(@vcLocationID,',1,12,13,14')
	END
	ELSE
	BEGIN
		SET @vcLocationID = '1,12,13,14'
	END
END


--Custom Fields     
INSERT INTO #tempField  
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,  
    'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,  
    CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,  
    CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,0 AS bitAllowEdit  
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id  
     JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id  
            WHERE   CFM.numDomainID = @numDomainId  
     AND GRP_ID IN( SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))  



--Fire Trigger Events on Fields
SELECT numFieldID,bitCustom,numWFID,numWFTriggerFieldID,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=
WorkFlowTriggerFieldList.numFieldID)AS FieldName FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID

SELECT 
	*
	, CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),WorkFlowConditionList.numFieldId) +'_' + CASE WHEN WorkFlowConditionList.bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID
	,t1.vcFieldName AS FieldName
	,t1.vcLookBackTableName
	,CASE WHEN  vcFilterOperator='eq' THEN 'equals' WHEN vcFilterOperator='ne' THEN 'not equal to' WHEN vcFilterOperator='gt' THEN 'greater than' WHEN vcFilterOperator='ge' THEN 'greater or equal' WHEN vcFilterOperator='lt' THEN 'less than' WHEN vcFilterOperator='le' THEN 'less or equal' ELSE 'NA' END AS FilterOperator  
FROM 
	WorkFlowConditionList 
LEFT JOIN
	#tempField t1
ON
	 t1.numFieldID=WorkFlowConditionList.numFieldID
WHERE 
	numWFID=@numWFID



--Declare @vcApprovalName varchar(Max)
--Set @vcApprovalName=''

--IF(@numFormId=49)
--BEGIN
--	Declare @test varchar(100)
--	SELECT @test= vcApproval from WorkFlowActionList where numWFID=@numWFID

--	SELECT @vcApprovalName=@vcApprovalName+Coalesce (isnull(vcFirstname,'')+' '+isnull(vcLastName,'')+ ', ','' )from AdditionalContactsInformation  where numContactID in (SELECT items FROM dbo.Split(@test,','))	
--END






SELECT *,CASE WHEN tintActionType=1 THEN 
				   (SELECT  vcDocName              
					FROM    GenericDocuments  
					WHERE   numDocCategory = 369  
					AND tintDocumentType =1 --1 =generic,2=specific  
					AND ISNULL(vcDocumentSection,'') <> 'M'  
					AND numDomainId = @numDomainID  AND ISNULL(VcFileName,'') not LIKE '#SYS#%' AND numGenericDocID=numTemplateID)                
             WHEN	tintActionType=2 then (Select TemplateName From tblActionItemData  where numdomainId=@numDomainID AND RowID=numTemplateID) 
              ELSE '0'  END EmailTemplate ,
         CASE WHEN vcEmailToType='1' THEN 'Owner of trigger record' WHEN vcEmailToType='2' THEN 'Assignee of trigger record'  WHEN vcEmailToType='1,2' THEN 'Owner of trigger record,Assignee of trigger record'  WHEN   vcEmailToType='3' THEN 'Primary contact of trigger record' WHEN   vcEmailToType='1,3' THEN 'Owner of trigger record,Primary contact of trigger record' WHEN vcEmailToType='2,3' THEN 'Primary contact of trigger record,Assignee of trigger record'  WHEN vcEmailToType='1,2,3' THEN  'Owner of trigger record,Primary contact of trigger record,Assignee of trigger record' ELSE NULL END AS EmailToType,
         CASE WHEN tintTicklerActionAssignedTo=1 THEN 'Owner of trigger record' ELSE 'Assignee of trigger record' END AS TicklerAssignedTo,
		 (select vcTemplateName from BizDocTemplate where numBizDocTempID=numBizDocTemplateID ) as  vcBizDocTemplate, 
		 (SELECT isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
						left join listorder LO 
						on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
						WHERE Ld.numListID=27 AND (constFlag=1 or Ld.numDomainID=@numDomainID )    AND  Ld.numListItemID=numBizDocTypeID) as vcBizDoc ,							
		 (SELECT Case When numOpptype=2 then 'Purchase' else 'Sales' end As BizDocType from BizDocTemplate WHERE numBizDocTempID=numBizDocTemplateID) as vcBizDocType ,
		 (SELECT numOpptype from BizDocTemplate where numBizDocTempID=numBizDocTemplateID) as numOppType,
		 ISNULL(vcSMSText,'') as vcSMSText    ,
		 ISNULL(vcEmailSendTo,'') as vcEmailSendTo ,
		 ISNULL(vcMailBody,'') as vcMailBody ,
		 ISNULL(vcMailSubject,'') as vcMailSubject,
		 ISNULL( vcApproval,'') as vcApprovalName
FROM dbo.WorkFlowActionList WHERE numWFID=@numWFID

 
SELECT *,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowActionUpdateFields.numFieldID)AS FieldName  FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)
DROP TABLE #tempField
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT 
				@bitApprovalforOpportunity=bitApprovalforOpportunity
				,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
			FROM 
				Domain 
			WHERE 
				numDomainId=@numDomainID
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@InlineEditValue='1' OR @InlineEditValue='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@InlineEditValue
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@InlineEditValue
							WHERE numOppId=@numOppId
						END
					END
				END
				
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRule]
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		UPDATE  
				CommissionRules
            SET     
				[vcCommissionName] = @vcCommissionName,
                [tintComBasedOn] = @tintComBasedOn,
                [tinComDuration] = @tinComDuration,
                [tintComType] = @tintComType,
				[tintComAppliesTo] = @tintComAppliesTo,
				[tintComOrgType] = @tintComOrgType,
				[tintAssignTo] = @tintAssignTo
            WHERE   
				[numComRuleID] = @numComRuleID 
				AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            INSERT  INTO [CommissionRuleDtl]
                    (
                        numComRuleID,
                        [intFrom],
                        [intTo],
                        [decCommission]
                    )
                    SELECT  @numComRuleID,
                            X.[intFrom],
                            X.[intTo],
                            X.[decCommission]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
                                        WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
                            ) X  
            
			INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
			
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1 OR @tintComAppliesTo = 2
			BEGIN
				-- ITEM ID OR ITEM CLASSIFICATION
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageDashboardAllowedReports')
DROP PROCEDURE USP_ManageDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[USP_ManageDashboardAllowedReports]
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@strReportIds as varchar(2000),
@strKPIGroupReportIds as varchar(2000),
@strPredefinedReportIds AS VARCHAR(MAX)
as
BEGIN
	DELETE ReportDashboardAllowedReports where numDomainId=@numDomainId AND numGrpId=@numGroupId

	IF @strReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,0 from (select * from dbo.split(@strReportIds,','))x
	END

	IF @strKPIGroupReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,1 from (select * from dbo.split(@strKPIGroupReportIds,','))x
	END

	IF @strPredefinedReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,2 from (select * from dbo.split(@strPredefinedReportIds,','))x

		DELETE FROM 
			ReportDashboard 
		WHERE 
			numUserCntID IN (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainId AND numGroupID=@numGroupId)	 
			AND numReportID IN (SELECT 
									numReportID 
								FROM 
									ReportListMaster 
								WHERE 
									ISNULL(numDomainID,0)=0 
									AND ISNULL(bitDefault,0)=1 
									AND intDefaultReportID NOT IN (SELECT 
																		numReportID 
																	FROM 
																		ReportDashboardAllowedReports 
																	WHERE 
																		numDomainID=@numDomainId 
																		AND numGrpID=@numGroupId 
																		AND tintReportCategory=2)
								)
	END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC 
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				IF ISNULL((SELECT bitInventoryImpacted FROM WorkOrder WHERE numOppID=@numOppID AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID),0) = 0
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
				ELSE
				BEGIN
					-- WE ARE NOT ALLOWING USER TO EDIT WORK ORDER ITEM INSIDE SALES ORDER AFTER ORDER IS CREATED
					SET @description='DO NOT ADD WAREHOUSE TRACKING'
				END
				
			END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits
                        - @onHand                                    
                    SET @onHand = 0                                    
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END  
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
			END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits 
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					PRINT 'in @onOrder < @numUnits '
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numUnits - @onOrder
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
			END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			DECLARE @numDeletedReceievedQty AS NUMERIC(18,0)
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE numoppitemtcode=@numoppitemtCode
			UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtcode=@numoppitemtCode

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numOIRLID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT
			DECLARE @numTempDeletedReceievedQty FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numOIRLID = numOIRLID,
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
					@numTempDeletedReceievedQty = numDeletedReceievedQty
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description='PO Re-Open (Qty:' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - @numTempDeletedReceievedQty),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID=@numOIRLID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR) + ')'
             
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand - (@numUnits-@numDeletedReceievedQty)
                ,numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID <> -1                   

			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID = -1
        END
    END
            
            
            
    IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(9),
	@numItemCode as numeric(9),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS FLOAT,
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty FLOAT 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)
DECLARE @bitInventoryImpacted BIT

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0),
		@bitInventoryImpacted=ISNULL(bitInventoryImpacted,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty FLOAT,
			numOrgQtyRequired INT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO 
			@TEMPITEM 
		SELECT 
			numChildItemID,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numWareHouseItemId,
			Item.bitAssembly 
		FROM 
			WorkOrderDetails 
		INNER JOIN 
			Item 
		ON 
			WorkOrderDetails.numChildItemID=Item.numItemCode 
		WHERE 
			numWOId = @numWOID 
			AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS FLOAT
			DECLARE @numChildOrgQtyRequired AS INT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS FLOAT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS FLOAT
			DECLARE @numOnOrder AS FLOAT
			DECLARE @numOnAllocation AS FLOAT
			DECLARE @numOnBackOrder AS FLOAT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 AND ISNULL(@bitInventoryImpacted,0) = 0 --WO INSERT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF @tintMode = 4 --WO DELETE
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						--SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1 AND ISNULL(@bitInventoryImpacted,0) = 0 --INVENTORY SHOULD NOT BE AFFECTED ON EDIT BECAUSE WE AE NOT ALLOWING USER TO EDIT WORK ORDER ITEM AFTER SALES ORDER IS CREATED
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS FLOAT
					DECLARE @numQtyShipped AS FLOAT

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END

		UPDATE WorkOrder SET bitInventoryImpacted=1 WHERE numWOId=@numWOID
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
/****** Object:  StoredProcedure [dbo].[USP_ManageItemList]    Script Date: 07/26/2008 16:19:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--Author:Anoop Jayaraj
--Modified By:Sachin Sadhu
--History: Added One Field numListType to save  bizdoc type wise BizDoc status     (added on 12thJun)    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemlist')
DROP PROCEDURE usp_manageitemlist
GO
CREATE PROCEDURE [dbo].[USP_ManageItemList]                
@numListItemID as numeric(9) OUTPUT,                  
@numDomainID as numeric(9)=0,                     
@vcData as varchar(100)='',             
@numListID as numeric(9)=0,                      
@numUserCntID as numeric(9),
@numListType numeric(9,0),
@tintOppOrOrder TINYINT              
as                    
if @numListItemID =0                    
                    
begin         
declare @sintOrder as smallint     
Declare @numListItemIDPK as numeric(9)   
update ListDetails set  sintOrder=0 where sintOrder is null      
select @sintOrder=(select max(sintOrder)+1 from ListDetails where numListID=@numListID)      
      
                 
insert into ListDetails(numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder)                    
values(@numListID,@vcData,@numUserCntID,getutcdate(),@numUserCntID,getutcdate(),0,@numDomainID,0,@sintOrder,@numListType,@tintOppOrOrder)     
  
Set @numListItemIDPK = SCOPE_IDENTITY()
SET @numListItemID = SCOPE_IDENTITY()

Declare @CurrentDate as datetime      
    
Set @CurrentDate=getutcdate()     
--If @numListID=64    
--Begin    
--Exec usp_InsertNewChartAccountDetails @numParntAcntId=10,@numAcntType=818,@vcCatgyName=@vcData,@vcCatgyDescription=@vcData,@numOriginalOpeningBal=null,    
--     @numOpeningBal=null,@dtOpeningDate=@CurrentDate,@bitActive=1,@numAccountId=0,@bitFixed=0,@numDomainId=@numDomainID,@numListItemID=@numListItemIDPK    
--End    
end                    
else                    
begin                    
update ListDetails set vcData=@vcData,                   
numModifiedBy=@numUserCntID,                  
bintModifiedDate=getutcdate(),
numListType=@numListType,
tintOppOrOrder=@tintOppOrOrder            
where numListItemID=@numListItemID     
--To update Category Description in Chart_of_accounts table  
--update Chart_Of_Accounts Set vcCatgyName=@vcData,vcCatgyDescription=@vcData Where numListItemID=@numListItemID        
                       
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingReport]
--SELECT * FROM [ShippingReportItems]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingReport')
DROP PROCEDURE USP_ManageShippingReport
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingReport]
  @numDomainId NUMERIC(9),
    @numShippingReportId NUMERIC(9) = 0 OUTPUT,
    @numOppBizDocId NUMERIC(9),
    @numShippingCompany NUMERIC(9),
    @Value1 VARCHAR(100) = NULL,
    @Value2 VARCHAR(100) = NULL,
    @Value3 VARCHAR(100) = NULL,
    @Value4 VARCHAR(100) = NULL,
    @vcFromState VARCHAR(50) = '',
    @vcFromZip VARCHAR(50) = '',
    @vcFromCountry VARCHAR(50) = '',
    @vcToState VARCHAR(50) = '',
    @vcToZip VARCHAR(50) = '',
    @vcToCountry VARCHAR(50) = '',
    @numUserCntId NUMERIC(9),
    @strItems TEXT = NULL,
    @tintMode TINYINT=0,
	@tintPayorType TINYINT=0,
	@vcPayorAccountNo VARCHAR(20)='',
	@numPayorCountry NUMERIC(9),
	@vcPayorZip VARCHAR(50)='',
	@vcFromCity VARCHAR(50)='',
    @vcFromAddressLine1 VARCHAR(50)='',
    @vcFromAddressLine2 VARCHAR(50)='',
    @vcToCity VARCHAR(50)='',
    @vcToAddressLine1 VARCHAR(50)='',
    @vcToAddressLine2 VARCHAR(50)='',
    @vcFromName			VARCHAR(1000)='',
    @vcFromCompany		VARCHAR(1000)='',
    @vcFromPhone		VARCHAR(100)='',
    @bitFromResidential	BIT=0,
    @vcToName			VARCHAR(1000)='',
    @vcToCompany		VARCHAR(1000)='',
    @vcToPhone			VARCHAR(100)='',
    @bitToResidential	BIT=0,
    @IsCOD					BIT = 0,
	@IsDryIce				BIT = 0,
	@IsHoldSaturday			BIT = 0,
	@IsHomeDelivery			BIT = 0,
	@IsInsideDelevery		BIT = 0,
	@IsInsidePickup			BIT = 0,
	@IsReturnShipment		BIT = 0,
	@IsSaturdayDelivery		BIT = 0,
	@IsSaturdayPickup		BIT = 0,
	@numCODAmount			NUMERIC(18,0) = 0,
	@vcCODType				VARCHAR(50) = 'Any',
	@numTotalInsuredValue	NUMERIC(18,2) = 0,
	@IsAdditionalHandling	BIT,
	@IsLargePackage			BIT,
	@vcDeliveryConfirmation	VARCHAR(1000),
	@vcDescription			VARCHAR(MAX),
	@numOppId				NUMERIC(18,0),
	@numTotalCustomsValue	NUMERIC(18,2),
	@tintSignatureType NUMERIC(18,0)
AS 
    BEGIN
		
  
        IF @numShippingReportId = 0 
        BEGIN
				SELECT
					@IsCOD = IsCOD
					,@IsHomeDelivery = IsHomeDelivery
					,@IsInsideDelevery = IsInsideDelevery
					,@IsInsidePickup = IsInsidePickup
					,@IsSaturdayDelivery = IsSaturdayDelivery
					,@IsSaturdayPickup = IsSaturdayPickup
					,@IsAdditionalHandling = IsAdditionalHandling
					,@IsLargePackage=IsLargePackage
					,@vcCODType = vcCODType
					,@vcDeliveryConfirmation = vcDeliveryConfirmation
					,@vcDescription = vcDescription
					,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
				FROM
					OpportunityMaster
				INNER JOIN
					DivisionMaster
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					DivisionMasterShippingConfiguration
				ON
					DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
				WHERE
					OpportunityMaster.numDomainId = @numDomainId
					AND DivisionMaster.numDomainID=@numDomainId
					AND DivisionMasterShippingConfiguration.numDomainID=@numDomainId
					AND OpportunityMaster.numOppId = @numOppID

                INSERT  INTO [ShippingReport]
                        (
                          [numOppBizDocId],
                          [numShippingCompany],
                          vcValue1,
                          vcValue2,
                          vcValue3,
                          vcValue4,
                          vcFromState,
                          vcFromZip,
                          vcFromCountry,
                          vcToState,
                          vcToZip,
                          vcToCountry,
                          [dtCreateDate],
                          [numCreatedBy],
                          numDomainId,
						  tintPayorType,
						  vcPayorAccountNo,
						  numPayorCountry,
						  vcPayorZip,
						  vcFromCity ,
						  vcFromAddressLine1 ,
						  vcFromAddressLine2 ,
						  vcToCity ,
						  vcToAddressLine1 ,
						  vcToAddressLine2,
						  vcFromName,
						  vcFromCompany,
						  vcFromPhone,
						  bitFromResidential,
						  vcToName,
						  vcToCompany,
						  vcToPhone,
						  bitToResidential,
						  IsCOD,					
						  IsDryIce,				
						  IsHoldSaturday,			
						  IsHomeDelivery,			
						  IsInsideDelevery,		
						  IsInsidePickup,			
						  IsReturnShipment,		
						  IsSaturdayDelivery,		
						  IsSaturdayPickup,		
						  numCODAmount,			
						  vcCODType,				
						  numTotalInsuredValue,
						  IsAdditionalHandling,
						  IsLargePackage,
						  vcDeliveryConfirmation,
						  vcDescription,
						  numOppId,numTotalCustomsValue,tintSignatureType
                        )
                VALUES  (
                          @numOppBizDocId,
                          @numShippingCompany,
                          @value1,
                          @value2,
                          @value3,
                          @value4,
                          @vcFromState,
                          @vcFromZip,
                          @vcFromCountry,
                          @vcToState,
                          @vcToZip,
                          @vcToCountry,
                          GETUTCDATE(),
                          @numUserCntId,
                          @numDomainId,
						  @tintPayorType,
						  @vcPayorAccountNo,
						  @numPayorCountry,
						  @vcPayorZip,
						  @vcFromCity ,
						  @vcFromAddressLine1 ,
						  @vcFromAddressLine2 ,
						  @vcToCity ,
						  @vcToAddressLine1 ,
						  @vcToAddressLine2,
						  @vcFromName,
						  @vcFromCompany,
						  @vcFromPhone,
						  @bitFromResidential,
						  @vcToName,
						  @vcToCompany,
						  @vcToPhone,
						  @bitToResidential,
						  @IsCOD,					
						  @IsDryIce,				
						  @IsHoldSaturday,			
						  @IsHomeDelivery,			
						  @IsInsideDelevery,		
						  @IsInsidePickup,			
						  @IsReturnShipment,		
						  @IsSaturdayDelivery,		
						  @IsSaturdayPickup,		
						  @numCODAmount,			
						  @vcCODType,				
						  @numTotalInsuredValue,
						  @IsAdditionalHandling,
						  @IsLargePackage,
						  @vcDeliveryConfirmation,
						  @vcDescription,
						  @numOppId,@numTotalCustomsValue,@tintSignatureType						  
                        )
                        
				SET @numShippingReportId = SCOPE_IDENTITY()
				
                DECLARE @numBoxID NUMERIC 
--				IF ISNULL(@value2,0) >0 -- Service type i.e Priority overnight,Ground
--				BEGIN
					-- create shipping box and Put all items into single box
						INSERT INTO [ShippingBox] (
							[vcBoxName],
							[numShippingReportId],
							[dtCreateDate],
							[numCreatedBy]
						) VALUES ( 
							/* vcBoxName - varchar(20) */ 'Box1',
							/* numShippingReportId - numeric(18, 0) */ @numShippingReportId,
							/* dtCreateDate - datetime */ GETUTCDATE(),
							/* numCreatedBy - numeric(18, 0) */ @numUserCntId ) 
		                
						SET @numBoxID = SCOPE_IDENTITY()
--				END
                
                
                IF (@strItems IS NOT NULL)
				BEGIN
				PRINT 'XML'
					DECLARE @hDocItem INT
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
	  
					INSERT  INTO [ShippingReportItems]
							(
							  [numShippingReportId],
							  [numItemCode],
							  [tintServiceType],
							  [dtDeliveryDate],
							  [monShippingRate],
							  [fltTotalWeight],
							  [intNoOfBox],
							  [fltHeight],
							  [fltWidth],
							  [fltLength],
							  [dtCreateDate],
							  [numCreatedBy],
							  numBoxID,
							  [numOppBizDocItemID]
							)
							SELECT  @numShippingReportId,--X.[numShippingReportId],
									X.[numItemCode],
									X.[tintServiceType],
									X.[dtDeliveryDate],
									X.[monShippingRate],
									X.[fltTotalWeight],
									X.[intNoOfBox],
									X.[fltHeight],
									X.[fltWidth],
									X.[fltLength],
									GETUTCDATE(),--X.[dtCreateDate],
									@numUserCntId,
									@numBoxID,
									CASE @tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE (SELECT TOP 1 numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode) END AS numOppBizDocItemID
							FROM    ( SELECT    *
									  FROM      OPENXML (@hDocItem, '/NewDataSet/Items', 2)
												WITH ( --numShippingReportId  NUMERIC(18,0),
												numItemCode NUMERIC(18, 0), tintServiceType TINYINT, dtDeliveryDate DATETIME, monShippingRate MONEY, 
												fltTotalWeight FLOAT, intNoOfBox INT, fltHeight FLOAT, fltWidth FLOAT, fltLength FLOAT,numOppBizDocItemID NUMERIC)
									) X
					
					EXEC sp_xml_removedocument @hDocItem	
				END                                
                

            END
        ELSE 
            IF @numShippingReportId > 0 
                BEGIN
                    UPDATE  [ShippingReport]
                    SET     [numShippingCompany] = @numShippingCompany,
                            vcValue1 = @Value1,
                            vcValue2 = @Value2,
                            vcValue3 = @Value3,
                            vcValue4 = @Value4,
                            vcFromState = @vcFromState,
                            vcFromZip = @vcFromZip,
                            vcFromCountry = @vcFromCountry,
                            vcToState = @vcToState,
                            vcToZip = @vcToZip,
                            vcToCountry = @vcToCountry,
							tintPayorType = @tintPayorType,
						    vcPayorAccountNo=@vcPayorAccountNo,
						    numPayorCountry=@numPayorCountry,
						    vcPayorZip=@vcPayorZip,
							vcFromCity=@vcFromCity,
							vcFromAddressLine1=@vcFromAddressLine1,
							vcFromAddressLine2=@vcFromAddressLine2,
							vcToCity=@vcToCity,
							vcToAddressLine1=@vcToAddressLine1,
							vcToAddressLine2=@vcToAddressLine2,
							vcFromName = @vcFromName,
							vcFromCompany = @vcFromCompany,
							vcFromPhone = @vcFromPhone,
							bitFromResidential = @bitFromResidential,
							vcToName = @vcToName,
							vcToCompany = @vcToCompany,
							vcToPhone = @vcToPhone,
							bitToResidential = @bitToResidential,
						    IsCOD = @IsCOD ,					
						    IsDryIce = @IsDryIce,				
						    IsHoldSaturday  = @IsHoldSaturday,			
						    IsHomeDelivery = @IsHomeDelivery,			
						    IsInsideDelevery = @IsInsideDelevery,		
						    IsInsidePickup = @IsInsidePickup,			
						    IsReturnShipment = @IsReturnShipment,		
						    IsSaturdayDelivery = @IsSaturdayDelivery,		
						    IsSaturdayPickup = @IsSaturdayPickup,		
						    numCODAmount = @numCODAmount,			
						    vcCODType = @vcCODType,				
						    numTotalInsuredValue = @numTotalInsuredValue,
						    IsAdditionalHandling = @IsAdditionalHandling,
							IsLargePackage = @IsLargePackage,
							vcDeliveryConfirmation = @vcDeliveryConfirmation,
							vcDescription = @vcDescription,							
						    numOppId = @numOppId,
						    numTotalCustomsValue = @numTotalCustomsValue,
							tintSignatureType=@tintSignatureType
                    WHERE   [numShippingReportId] = @numShippingReportId
                    
                    UPDATE [ShippingReportItems] 
                    SET [tintServiceType] = @Value2 
                    WHERE [numShippingReportId]=@numShippingReportId
                  
                END
    
       
    END

/****** Object:  StoredProcedure [dbo].[USP_ManageWarehouse]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managewarehouse')
DROP PROCEDURE usp_managewarehouse
GO
CREATE PROCEDURE [dbo].[USP_ManageWarehouse]      
@byteMode as tinyint=0,      
@numWareHouseID as numeric(9)=0,      
@vcWarehouse as varchar(50),          
@numDomainID as numeric(9)=0,
@numAddressID As numeric(18,0) = 0,
@vcPrintNodeAPIKey AS VARCHAR(100) = '',
@vcPrintNodePrinterID AS VARCHAR(100) = ''
AS      
BEGIN      
	IF @byteMode=0      
	BEGIN      
		IF @numWareHouseID=0      
		BEGIN
			INSERT INTO Warehouses 
			(
				vcWarehouse
				,numDomainID
				,numAddressID
				,vcPrintNodeAPIKey
				,vcPrintNodePrinterID	
			)      
			VALUES 
			(
				@vcWarehouse
				,@numDomainID
				,@numAddressID
				,@vcPrintNodeAPIKey
				,@vcPrintNodePrinterID
			)      
        
			DECLARE @numNewWarehouseID NUMERIC(18,0)
			SET @numNewWarehouseID = SCOPE_IDENTITY()


			INSERT INTO WareHouseItems 
			(
				numItemID, 
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified
			) 
			SELECT
				numItemCode
				,@numNewWarehouseID
				,NULL
				,0
				,0
				,monListPrice
				,''
				,vcSKU
				,numBarCodeId
				,@numDomainID
				,GETDATE()
			FROM
				Item
			WHERE
				numDomainID=@numDomainID
				AND charItemType='P'
				AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)

			INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  
				WHI.numWareHouseItemID
				,ISNULL(WHI.numOnHand,0)
				,ISNULL(WHI.numOnOrder,0)
				,ISNULL(WHI.numReorder,0)
				,ISNULL(WHI.numAllocation,0)
				,ISNULL(WHI.numBackOrder,0)
				,WHI.numDomainID
				,'INSERT WareHouse'
				,1
				,I.numItemCode
				,GETUTCDATE()
				,0
				,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)
				,(SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode)
				,GETUTCDATE()
            FROM 
				Item I 
			INNER JOIN 
				WareHouseItems WHI 
			ON 
				I.numItemCode=WHI.numItemID
			WHERE 
				WHI.numWareHouseID = @numNewWarehouseID
				AND [WHI].[numDomainID] = @numDomainID


			SELECT @numNewWarehouseID     
		END      
		ELSE IF @numWareHouseID>0      
		BEGIN
			UPDATE 
				Warehouses 
			SET    
				vcWarehouse= @vcWarehouse
				,numAddressID=@numAddressID
				,vcPrintNodeAPIKey=@vcPrintNodeAPIKey
				,vcPrintNodePrinterID=@vcPrintNodePrinterID
			WHERE 
				numWareHouseID=@numWareHouseID      
		
			SELECT @numWareHouseID      
		END      
	END
	ELSE IF @byteMode=1      
	BEGIN     
		IF (SELECT COUNT(*) FROM [WareHouseItems] WHERE numWarehouseID = @numWareHouseID) = 0
		BEGIN
			DELETE FROM Warehouses WHERE numWareHouseID=@numWareHouseID	
		END
		ELSE
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	IF EXISTS (SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND bitActive=1)
	BEGIN
		DECLARE @numRule2OrderStatus NUMERIC(18,0)
		DECLARE @numRule3OrderStatus NUMERIC(18,0)
		DECLARE @numRule4OrderStatus NUMERIC(18,0)

		SELECT
			@numRule2OrderStatus= (CASE WHEN bitRule2IsActive = 1 THEN numRule2OrderStatus ELSE 0 END)
			,@numRule3OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule3OrderStatus ELSE 0 END)
			,@numRule4OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule4OrderStatus ELSE 0 END)
		FROM
			SalesFulfillmentConfiguration
		WHERE 
			numDomainID=@numDomainID 
			AND bitActive=1

		IF EXISTS (SELECT * FROM WorkFlowConditionList WHERE numFieldID=101 AND vcFilterValue IN (@numRule2OrderStatus,@numRule3OrderStatus,@numRule4OrderStatus))
		BEGIN
			RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
			RETURN -1
		END
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWorkFlowQueue' ) 
    DROP PROCEDURE USP_ManageWorkFlowQueue
GO
CREATE PROCEDURE USP_ManageWorkFlowQueue
    @numWFQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0,
    @numFormID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @tintWFTriggerOn TINYINT,
    @tintMode TINYINT,
    @vcColumnsUpdated VARCHAR(1000)='',
    @numWFID NUMERIC(18,0)=0,
    @bitSuccess BIT=0,
    @vcDescription VARCHAR(1000)=''
    
AS 
BEGIN
	IF @tintMode=1
	BEGIN
		IF @numWFQueueID>0
		   BEGIN
	  		 UPDATE [dbo].[WorkFlowQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numWFQueueID] = @numWFQueueID AND [numDomainID] = @numDomainID
		   END
		ELSE
		   BEGIN
	 		 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --Added By:Sachin Sadhu||Date:10thFeb2014
			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
			 --case of Field(s) Update
			 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=4)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --End of Script:Sachin
		  END
	 END
	  ELSE IF @tintMode = 2
        BEGIN
			INSERT INTO [dbo].[WorkFlowQueueExecution] ([numWFQueueID], [numWFID], [dtExecutionDate], [bitSuccess], [vcDescription])
			SELECT @numWFQueueID, @numWFID, GETUTCDATE(), @bitSuccess, @vcDescription
        END    
END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE WHEN OBD.numUnitHour > ISNULL(WI.numAllocation,0) THEN (OBD.numUnitHour - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0))  ELSE 0 END) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS MONEY) AS numCost
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty NUMERIC(18,2),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,ISNULL(WI.numBackOrder,0) AS numBackOrder,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN ISNULL(WL.numWLocationID,0) > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CT')
DROP PROCEDURE USP_OpportunityMaster_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityMaster_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.OpportunityMaster, 0) AS CT
WHERE numOppId=@numRecordID

IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

End
Else--Update
Begin

SET @last_synchronization_version=@ChangeVersion-1
        

        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

		
		
		--Fields Update:
			Declare @UFFields as table 
			(
					tintActive varchar(70),
					monPAmount varchar(70),
					numAssignedBy varchar(70),
					numAssignedTo varchar(70),
					numCampainID varchar(70),
					bintAccountClosingDate varchar(70),
					txtComments varchar(70),
					lngPConclAnalysis varchar(70),
					bintCreatedDate varchar(70),
					vcOppRefOrderNo varchar(70),
					tintOppStatus varchar(70),
					intpEstimatedCloseDate varchar(70),
					--monDealAmount varchar(70),
					vcMarketplaceOrderID varchar(70),
					tintOppType varchar(70),
					numStatus varchar(70),
					monDealAmount varchar(70),
					numRecOwner varchar(70),
					vcPoppName varchar(70),
					numSalesOrPurType varchar(70),
					tintSource varchar(70),
					bitRecurred varchar(70),
					numPartner varchar(70),
					numPartenerContact varchar(70),
					dtReleaseDate varchar(70),
					numReleaseStatus varchar(70),
					numShipmentMethod varchar(70),
					numPercentageComplete varchar(70)
			)

		INSERT into @UFFields
		(
			tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
			,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,
			numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,numShipmentMethod,numPercentageComplete
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintActive, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monPAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monPAmount, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numCampainID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampainID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintAccountClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintAccountClosingDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'lngPConclAnalysis', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS lngPConclAnalysis, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcOppRefOrderNo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcOppRefOrderNo, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intpEstimatedCloseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intpEstimatedCloseDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcMarketplaceOrderID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcMarketplaceOrderID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPoppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPoppName, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numSalesOrPurType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numSalesOrPurType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintSource', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintSource ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartner,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartenerContact', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartenerContact,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'dtReleaseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtReleaseDate,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numReleaseStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numReleaseStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numShipmentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numShipmentMethod,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPercentageComplete', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPercentageComplete
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppId=@numRecordID

		--exec USP_GetWorkFlowFormFieldMaster 1,70
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT 
					tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
					,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred
					,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,numShipmentMethod,numPercentageComplete
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,numShipmentMethod,numPercentageComplete)
		) AS upv
		where FieldValue<>0
 

End

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
IF(	@Columns_Updated='monDealAmount')
BEGIN

set @tintWFTriggerOn=1
END

--select  @Columns_Updated
--select @tintWFTriggerOn
--select * from @UFFields
--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 70, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END

/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Organization_CT')
DROP PROCEDURE USP_Organization_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_Organization_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	-- Insert statements for procedure here
	DECLARE @tblFields TABLE 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	DECLARE @Type char(1)
	DECLARE @ChangeVersion bigint
	DECLARE @CreationVersion bigint
	DECLARE @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version
		,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, 0) AS CT
	WHERE 
		numDivisionID=@numRecordID


	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
			SELECT 
				@Type = CT.SYS_CHANGE_OPERATION
			FROM 
				CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
			WHERE  
				CT.numDivisionID=@numRecordID
	END
	ELSE --Update
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
			CT.numDivisionID=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    bitActiveInActive varchar(70),
				numAssignedBy varchar(70),
				numAssignedTo varchar(70),
				numCampaignID varchar(70),
				numCompanyDiff varchar(70),
				vcCompanyDiff varchar(70), 
				numCurrencyID varchar(70),
				numFollowUpStatus1 varchar(70),
				numFollowUpStatus varchar(70),
				numGrpID varchar(70),
				bintCreatedDate varchar(70),
				vcComFax varchar(70),
				bitPublicFlag varchar(70),
				tintCRMType varchar(70),
				bitNoTax varchar(70),
				numTerID varchar(70),
				numStatusID varchar(70),
				vcComPhone varchar(70),
				numAccountClassID varchar(70),
				numBillingDays VARCHAR(70),
				tintPriceLevel VARCHAR(70),
				numDefaultPaymentMethod VARCHAR(70),
				intShippingCompany VARCHAR(70),
				numDefaultShippingServiceID VARCHAR(70)
			)

		INSERT into @UFFields
		(
			bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
			,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany,
			numDefaultShippingServiceID
		)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitActiveInActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitActiveInActive ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCampaignID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampaignID, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCompanyDiff	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  vcCompanyDiff ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCurrencyID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCurrencyID, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus1', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus1,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numGrpID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numGrpID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComFax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComFax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitPublicFlag', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPublicFlag,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintCRMType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintCRMType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitNoTax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitNoTax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numTerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numStatusID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatusID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComPhone,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAccountClassID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAccountClassID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numBillingDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBillingDays,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintPriceLevel', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPriceLevel,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultPaymentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultPaymentMethod,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'intShippingCompany', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intShippingCompany,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultShippingServiceID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultShippingServiceID
		FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numDivisionID=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,68
		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName,
			FieldValue
		FROM
			(
				SELECT 
					bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
					,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany
					,numDefaultShippingServiceID
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus
			,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel
			,numDefaultPaymentMethod,intShippingCompany,numDefaultShippingServiceID)
		) AS upv
		WHERE 
			FieldValue<>0
	END

	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

	Declare @numAssignedTo numeric(18,0)
	Declare @numAssignedBy numeric(18,0)
	Declare @numWebLead numeric(18,0)
	Declare @bitLeadBoxFlg bit

	Select @numAssignedTo=numAssignedTo,@numAssignedBy=numAssignedBy,@numWebLead=numGrpId,@bitLeadBoxFlg=bitLeadBoxFlg from DivisionMaster where numDivisionID=@numRecordID

	--For Fields Update Exection Point
	DECLARE @Columns_Updated VARCHAR(1000)
	SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
	SET @Columns_Updated=ISNULL(@Columns_Updated,'')

	select @Columns_Updated,@numAssignedTo,@numAssignedBy,@numWebLead
	IF (@Columns_Updated='numAssignedBy,numAssignedTo')
	BEGIN	
		IF (@numWebLead=1) --web Leads
		BEGIN
			IF	(@bitLeadBoxFlg=1) --(through System)
			BEGIN
				IF(@numAssignedTo=0 AND @numAssignedBy=0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
			ELSE		--(through Web)			
			BEGIN
				IF(@numAssignedTo>0 AND @numAssignedBy>0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
		END

		ELSE    --Normal Leads
		BEGIN
			IF	(@numAssignedTo=0 AND @numAssignedBy=0)
			BEGIN
				SET @tintWFTriggerOn=1
			END
			ELSE
			BEGIN
				SET @tintWFTriggerOn=2
			END
		END
	END

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 68, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END



GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId          AS NUMERIC(9)  = 0,
    @numDepartmentId      AS NUMERIC(9)  = 0,
    @dtStartDate          AS DATETIME,
    @dtEndDate            AS DATETIME,
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TempCommissionUnPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionUnPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionUnPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate MONEY,
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense MONEY,
		monReimburse MONEY,
		monCommPaidInvoice MONEY,
		monCommPaidInvoiceDeposited MONEY,
		monCommUNPaidInvoice MONEY,
		monCommPaidCreditMemoOrRefund MONEY,
		monCommUnPaidCreditMemoOrRefund MONEY,
		monTotalAmountDue MONEY,
		monAmountPaid MONEY, 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommPaidInvoiceDepositedToBank MONEY,@monCommUNPaidInvoice MONEY,@monCommPaidCreditMemoOrRefund MONEY,@monCommUnPaidCreditMemoOrRefund MONEY

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0,@monCommUnPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
					AND 1 = (CASE WHEN EXISTS 
								(SELECT 
									DM.numDepositId 
								FROM 
									DepositeDetails DD 
								INNER JOIN 
									DepositMaster DM 
								ON 
									DD.numDepositID=DM.numDepositId 
								WHERE 
									numDomainId=@numDomainId 
									AND DD.numOppID=Opp.numOppId 
									AND DD.numOppBizDocsID=oppBiz.numOppBizDocsId 
									AND DM.tintDepositePage=2 
									AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
								)
							 THEN 1 ELSE 0 END)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)		
		-- CALCULATE COMMISSION FOR UN-PAID CREDIT MEMO/REFUND
		SET @monCommUnPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionUnPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommPaidInvoiceDeposited=@monCommPaidInvoiceDepositedToBank,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monCommPaidCreditMemoOrRefund=@monCommPaidCreditMemoOrRefund,
			monCommUnPaidCreditMemoOrRefund=@monCommUnPaidCreditMemoOrRefund,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS MONEY 
    DECLARE @monAvgCost AS MONEY   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
				IF @tintMode = 0
				BEGIN
					-- WE ARE NOT ALLOWING USER TO EDIT WORK ORDER ITEM INSIDE SALES ORDER AFTER ORDER IS CREATED
					SET @description='DO NOT ADD WAREHOUSE TRACKING'
				END
				ELSE IF @tintMode=1 OR @tintMode=2 -- DELETE OR DEAL LOST
				BEGIN
					DECLARE @numWOID AS NUMERIC(18,0)
					DECLARE @numWOStatus AS NUMERIC(18,0)
					SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

					IF  @tintMode=2
					BEGIN
						SET @description='SO-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
					END

					--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
					IF @numWOStatus <> 23184
					BEGIN
						IF @onOrder >= @numUnits
							SET @onOrder = @onOrder - @numUnits
						ELSE
							SET @onOrder = 0
					END

					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numUnits < @onBackOrder 
					BEGIN                  
						SET @onBackOrder = @onBackOrder - @numUnits
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@onAllocation - @numUnits) >= 0
							SET @onAllocation = @onAllocation - @numUnits
						
						--ADD QTY TO ONHAND
						SET @onHand = @onHand + @numUnits
					END

					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand ,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID   

					--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
					IF @numWOStatus <> 23184
					BEGIN
						EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID
					END
				END
			END	
			ELSE
			BEGIN	
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF @tintMode = 2
			BEGIN
				SET @description='PO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END

			IF @numWLocationID = -1 AND ISNULL(@bitStockTransfer,0)=0
			BEGIN
				IF @tintmode = 1 OR @tintMode=2
				BEGIN
					DECLARE @TEMPReceievedItems TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numDeletedReceievedQty FLOAT
					)

					DELETE FROM @TEMPReceievedItems

					INSERT INTO
						@TEMPReceievedItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY ID),
						ID,
						numWarehouseItemID,
						numUnitReceieved,
						ISNULL(numDeletedReceievedQty,0)
					FROM
						OpportunityItemsReceievedLocation 
					WHERE
						numDomainID=@numDomain
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode

					DECLARE @i AS INT = 1
					DECLARE @COUNT AS INT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempOnHand FLOAT
					DECLARE @numTempWarehouseItemID NUMERIC(18,0)
					DECLARE @numTempUnitReceieved FLOAT
					DECLARE @numTempDeletedReceievedQty FLOAT

					SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

					WHILE @i <= @COUNT
					BEGIN
						SELECT 
							@numTempOIRLID=TRI.numOIRLID,
							@numTempOnHand= ISNULL(numOnHand,0),
							@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
							@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
							@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
						FROM 
							@TEMPReceievedItems TRI
						INNER JOIN
							WareHouseItems WI
						ON
							TRI.numWarehouseItemID=WI.numWareHouseItemID
						WHERE 
							ID=@i

						IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
								dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID
						END
						ELSE
						BEGIN
							RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
							RETURN
						END

						IF @tintMode = 2
						BEGIN
							SET @description='PO DEMOTED TO OPPORTUNITY (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END
						ELSE
						BEGIN
							SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 4,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain

						DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

						SET @i = @i + 1
					END
				END

				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					UPDATE
						WareHouseItems
					SET 
						numOnOrder = ISNULL(numOnOrder,0) - @numUnits,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONORDER_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				--Partial Fulfillment
				IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
				BEGIN
					SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
				END
						
				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					--Causing Negative Inventory Bug ID:494
					SET @onOrder = @onOrder - @numUnits	
				END
				ELSE IF (@onHand + @onOrder) - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - (@numUnits-@onOrder)
					SET @onOrder = 0
				END
				ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					Declare @numDiff numeric
	
					SET @numDiff = @numUnits - @onOrder
					SET @onOrder = 0

					SET @numDiff = @numDiff - @onHand
					SET @onHand = 0

					SET @onAllocation = @onAllocation - @numDiff
					SET @onBackOrder = @onBackOrder + @numDiff
				END
					    
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
                        
						

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
			END                                        
        END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
    END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentConfiguration_Save')
DROP PROCEDURE USP_SalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentConfiguration_Save]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@bitActive BIT,
	@bitRule1IsActive BIT,
	@numRule1BizDoc NUMERIC(18,0),
	@tintRule1Type TINYINT,
	@numRule1OrderStatus NUMERIC(18,0),
	@bitRule2IsActive BIT,
	@numRule2OrderStatus NUMERIC(18,0),
	@numRule2SuccessOrderStatus NUMERIC(18,0),
	@numRule2FailOrderStatus NUMERIC(18,0),
	@bitRule3IsActive BIT,
	@numRule3OrderStatus NUMERIC(18,0),
	@numRule3FailOrderStatus NUMERIC(18,0),
	@bitRule4IsActive BIT,
	@numRule4OrderStatus NUMERIC(18,0),
	@numRule4FailOrderStatus NUMERIC(18,0),
	@bitRule5IsActive BIT,
	@numRule5OrderStatus NUMERIC(18,0),
	@bitRule6IsActive BIT,
	@numRule6OrderStatus NUMERIC(18,0)
AS  
BEGIN 
	IF EXISTS(SELECT numSFCID FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			SalesFulfillmentConfiguration
		SET
			numDomainID=@numDomainID
			,bitActive=@bitActive
			,bitRule1IsActive=@bitRule1IsActive
			,numRule1BizDoc=@numRule1BizDoc
			,tintRule1Type=@tintRule1Type
			,numRule1OrderStatus=@numRule1OrderStatus
			,bitRule2IsActive=@bitRule2IsActive
			,numRule2OrderStatus=@numRule2OrderStatus
			,numRule2SuccessOrderStatus=@numRule2SuccessOrderStatus
			,numRule2FailOrderStatus=@numRule2FailOrderStatus
			,bitRule3IsActive=@bitRule3IsActive
			,numRule3OrderStatus=@numRule3OrderStatus
			,numRule3FailOrderStatus=@numRule3FailOrderStatus
			,bitRule4IsActive=@bitRule4IsActive
			,numRule4OrderStatus=@numRule4OrderStatus
			,numRule4FailOrderStatus=@numRule4FailOrderStatus
			,bitRule5IsActive=@bitRule5IsActive
			,numRule5OrderStatus=@numRule5OrderStatus
			,bitRule6IsActive=@bitRule6IsActive
			,numRule6OrderStatus=@numRule6OrderStatus
			,dtModified=GETUTCDATE()
			,numModifiedBy=@numUserCntID
		WHERE
			numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		INSERT INTO SalesFulfillmentConfiguration
		(
			numDomainID
			,bitActive
			,bitRule1IsActive
			,numRule1BizDoc
			,tintRule1Type
			,numRule1OrderStatus
			,bitRule2IsActive
			,numRule2OrderStatus
			,numRule2SuccessOrderStatus
			,numRule2FailOrderStatus
			,bitRule3IsActive
			,numRule3OrderStatus
			,numRule3FailOrderStatus
			,bitRule4IsActive
			,numRule4OrderStatus
			,numRule4FailOrderStatus
			,bitRule5IsActive
			,numRule5OrderStatus
			,bitRule6IsActive
			,numRule6OrderStatus
			,dtCreated
			,numCreatedBy
		)
		VALUES
		(
			@numDomainID
			,@bitActive
			,@bitRule1IsActive
			,@numRule1BizDoc
			,@tintRule1Type
			,@numRule1OrderStatus
			,@bitRule2IsActive
			,@numRule2OrderStatus
			,@numRule2SuccessOrderStatus
			,@numRule2FailOrderStatus
			,@bitRule3IsActive
			,@numRule3OrderStatus
			,@numRule3FailOrderStatus
			,@bitRule4IsActive
			,@numRule4OrderStatus
			,@numRule4FailOrderStatus
			,@bitRule5IsActive
			,@numRule5OrderStatus
			,@bitRule6IsActive
			,@numRule6OrderStatus
			,GETUTCDATE()
			,@numUserCntID
		)
	END

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@numTotalInsuredValue NUMERIC(18,2),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX),@numTotalCustomsValue NUMERIC(18,2)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	SELECT
		@IsCOD = IsCOD
		,@IsHomeDelivery = IsHomeDelivery
		,@IsInsideDelevery = IsInsideDelevery
		,@IsInsidePickup = IsInsidePickup
		,@IsSaturdayDelivery = IsSaturdayDelivery
		,@IsSaturdayPickup = IsSaturdayPickup
		,@IsAdditionalHandling = IsAdditionalHandling
		,@IsLargePackage=IsLargePackage
		,@vcCODType = vcCODType
		,@vcDeliveryConfirmation = vcDeliveryConfirmation
		,@vcDescription = vcDescription
		,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		,@numOrderShippingCompany = ISNULL(numShipmentMethod,0)
		,@numOrderShippingService = ISNULL(intUsedShippingCompany,0)
		,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
		,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		DivisionMasterShippingConfiguration
	ON
		DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
	WHERE
		OpportunityMaster.numDomainId = @numDomainId
		AND DivisionMaster.numDomainID=@numDomainId
		AND DivisionMasterShippingConfiguration.numDomainID=@numDomainId
		AND OpportunityMaster.numOppId = @numOppID



	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
	IF ISNULL(@numOrderShippingCompany,0) > 0
	BEGIN
		If @numOrderShippingCompany >= 10 And @numOrderShippingCompany <= 28
		BEGIN
             SET @numOrderShippingCompany = 91
		END
        ELSE IF (@numOrderShippingCompany = 40 Or
                @numOrderShippingCompany = 42 Or
                @numOrderShippingCompany = 43 Or
                @numOrderShippingCompany = 48 Or
                @numOrderShippingCompany = 49 Or
                @numOrderShippingCompany = 50 Or
                @numOrderShippingCompany = 51 Or
                @numOrderShippingCompany = 55) 
		BEGIN
            SET @numOrderShippingCompany = 88
		END
        Else If @numOrderShippingCompany >= 70 And @numOrderShippingCompany <= 76
		BEGIN
            SET @numOrderShippingCompany = 90
		END


		IF ISNULL(@numOrderShippingService,0) = 0
		BEGIN
			SET @numOrderShippingService = CASE @numOrderShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numOrderShippingCompany
		SET @numShippingService = @numOrderShippingService
	END
	ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	BEGIN
		If @numDivisionShippingCompany >= 10 And @numDivisionShippingCompany <= 28
		BEGIN
             SET @numDivisionShippingCompany = 91
		END
        ELSE IF (@numDivisionShippingCompany = 40 Or
                @numDivisionShippingCompany = 42 Or
                @numDivisionShippingCompany = 43 Or
                @numDivisionShippingCompany = 48 Or
                @numDivisionShippingCompany = 49 Or
                @numDivisionShippingCompany = 50 Or
                @numDivisionShippingCompany = 51 Or
                @numDivisionShippingCompany = 55) 
		BEGIN
            SET @numDivisionShippingCompany = 88
		END
        Else If @numDivisionShippingCompany >= 70 And @numDivisionShippingCompany <= 76
		BEGIN
            SET @numDivisionShippingCompany = 90
		END


		IF ISNULL(@numDivisionShippingService,0) = 0
		BEGIN
			SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numDivisionShippingCompany
		SET @numShippingService = @numDivisionShippingService
	END
	ELSE -- USE DOMAIN DEFAULT
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @numShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END



	DECLARE @bitUseBizdocAmount BIT

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END
	ELSE
	BEGIN
		SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T2.numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,SUM(OBI.numUnitHour) numTotalContainer
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,ISNULL(fltWeight,0)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,31
										,@numShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > 0 THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/166) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty
								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)=''                                                
As                                                                         
  
DECLARE @tintPerformanceFilter AS TINYINT
DECLARE @tintActionItemsViewRights TINYINT  = 3

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),vcLastSalesOrderDate VARCHAR(100),monDealAmount MONEY, vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500) )                                                         
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                                                            
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0                                                
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] VARCHAR(800)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) '
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END

        
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
AND Comm.bitclosedflag=0                                                
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
) 
 Order by endtime'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
dbo.fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql2 =@strSql2 +' 
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            




DECLARE @strSql3 AS VARCHAR(MAX)

SET @strSql3=   ' select  * from #tempRecords '

IF CHARINDEX(@columnName,@strSql) > 0 OR CHARINDEX(@columnName,@strSql1) > 0 OR CHARINDEX(@columnName,@strSql2) > 0
BEGIN
	SET @strSql3=@strSql3+' order by ' + @columnName +' ' + @columnSortOrder
END


 PRINT @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOppItemLabel')
DROP PROCEDURE USP_UpdateOppItemLabel
GO
CREATE PROCEDURE  USP_UpdateOppItemLabel
    @numOppID NUMERIC,
	@numoppitemtCode numeric(9),
    @vcItemName varchar(300),
    @vcModelID varchar(200),
    @vcItemDesc varchar(1000),
	@vcManufacturer varchar(250),
	@numProjectID numeric(9),
	@numClassID numeric(9),
	@vcNotes varchar(1000),
	@numCost decimal(30, 16)
AS 
    BEGIN

 UPDATE  OpportunityItems
                SET     [vcItemName] = @vcItemName,
                        [vcModelID] = @vcModelID,
                        [vcItemDesc] = @vcItemDesc,
                        vcManufacturer = @vcManufacturer,
                        numProjectID = @numProjectID,
                        numClassID = @numClassID,
						vcNotes=@vcNotes,
						numCost=@numCost,
						numProjectStageID=(Case when numProjectID<>@numProjectID then 0 else numProjectStageID end)
                WHERE   [numOppId] = @numOppID and numoppitemtCode = @numoppitemtCode
    END
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppItems]    Script Date: 07/26/2008 16:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppitems')
DROP PROCEDURE usp_updateoppitems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppItems]
@numOppID as numeric(9)=0,
@numItemCode as numeric(9)=0,
@numUnitHour as FLOAT=0,
@monPrice as money,
@vcItemName VARCHAR(300),
@vcModelId VARCHAR(200),
@vcPathForTImage VARCHAR(300),
@vcItemDesc as varchar(1000),
@numWarehouseItmsID as numeric(9),
@ItemType as Varchar(50),
@DropShip as bit=0,
@numUOM as numeric(9)=0,
@numClassID as numeric(9)=0,
@numProjectID as numeric(9)=0,
@vcNotes varchar(1000)=null

as                                                   
	SET @vcNotes = (CASE WHEN @vcNotes='&nbsp;' THEN '' ELSE @vcNotes END)
	
   if @numWarehouseItmsID=0 set @numWarehouseItmsID=null 
	
	DECLARE @vcManufacturer VARCHAR(250)
	SELECT @vcManufacturer = vcManufacturer FROM item WHERE [numItemCode]=@numItemCode
    insert into OpportunityItems                                                    
	(numOppId,numItemCode,vcNotes,numUnitHour,monPrice,numCost,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcPathForTImage],[vcManufacturer],numUOMId,numClassID,numProjectID,numSortOrder)
	values
	(@numOppID,@numItemCode,@vcNotes,@numUnitHour,@monPrice,@monPrice,@numUnitHour*@monPrice,@vcItemDesc,@numWarehouseItmsID,@ItemType,@DropShip,0,0,@numUnitHour*@monPrice,@vcItemName,@vcModelId,@vcPathForTImage,@vcManufacturer,@numUOM,@numClassID,@numProjectID,ISNULL((SELECT MAX(numSortOrder) FROM OpportunityItems WHERE numOppID=@numOppID),0) + 1)
	
	select SCOPE_IDENTITY()
	                                 
	update OpportunityMaster set  monPamount=(select sum(monTotAmount)from OpportunityItems where numOppId=@numOppID)                                                     
	where numOppId=@numOppID
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
@byteMode as tinyint=0,  
@numWareHouseItemID as numeric(9)=0,  
@Units as FLOAT,
@numWOId AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0,
@numOppId AS NUMERIC(9)=0,
@numAssembledItemID AS NUMERIC(18,0) = 0,
@fltQtyRequiredForSingleBuild AS FLOAT = 0   
as      
BEGIN TRY
BEGIN TRANSACTION
  DECLARE @Description AS VARCHAR(200)
  DECLARE @numItemCode NUMERIC(18)
  DECLARE @numDomain AS NUMERIC(18,0)
  DECLARE @ParentWOID AS NUMERIC(18,0)

  SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
  SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID
  
if @byteMode=0  -- Aseeembly Item
begin  
	DECLARE @CurrentAverageCost MONEY
	DECLARE @TotalCurrentOnHand FLOAT
	DECLARE @newAverageCost MONEY
	
	SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
	SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
	PRINT @CurrentAverageCost
	PRINT @TotalCurrentOnHand

	IF @numWOId > 0
	BEGIN
		SELECT 
			@newAverageCost = SUM((numQtyItemsReq_Orig * dbo.fn_UOMConversion(WOD.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
		FROM 
			WorkOrderDetails WOD
		LEFT JOIN 
			dbo.Item I 
		ON 
			I.numItemCode = WOD.numChildItemID
		WHERE 
			WOD.numWOId = @numWOId
			AND I.charItemType = 'P'

		UPDATE WorkOrder SET monAverageCost=@newAverageCost WHERE numWOId=@numWOId
	END
	ELSE
	BEGIN
		SELECT 
			@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
		FROM 
			dbo.ItemDetails ID
		LEFT JOIN 
			dbo.Item I 
		ON 
			I.numItemCode = ID.numChildItemID
		WHERE 
			numItemKitID = @numItemCode
			AND I.charItemType = 'P'

		UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID
	END

	SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
	UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

	IF ISNULL(@numWOId,0) > 0
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,numOnOrder= CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0)-@Units END,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
	ELSE
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
end  
else if @byteMode=1  --Aseembly Child item
begin  
	declare @onHand as FLOAT                                    
	declare @onOrder as FLOAT                                    
	declare @onBackOrder as FLOAT                                   
	declare @onAllocation as FLOAT    
	declare @monAverageCost AS MONEY


	SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

	IF @numWOId > 0
	BEGIN
		UPDATE
			WorkOrderDetails
		SET
			monAverageCost=@monAverageCost
		WHERE
			numWOId=@numWOId
			AND numChildItemID=@numItemCode
			AND numWareHouseItemId=@numWareHouseItemID
	END
	ELSE
	BEGIN
		-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
		-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
		INSERT INTO [dbo].[AssembledItemChilds]
		(
			numAssembledItemID,
			numItemCode,
			numQuantity,
			numWarehouseItemID,
			monAverageCost,
			fltQtyRequiredForSingleBuild
		)
		VALUES
		(
			@numAssembledItemID,
			@numItemCode,
			@Units,
			@numWareHouseItemID,
			@monAverageCost,
			@fltQtyRequiredForSingleBuild
		)
	END

	select                                     
		@onHand=isnull(numOnHand,0),                                    
		@onAllocation=isnull(numAllocation,0),                                    
		@onOrder=isnull(numOnOrder,0),                                    
		@onBackOrder=isnull(numBackOrder,0)                                     
	from 
		WareHouseItems 
	where 
		numWareHouseItemID=@numWareHouseItemID  


	IF ISNULL(@numWOId,0) > 0
	BEGIN
		--RELEASE QTY FROM ALLOCATION
		SET @onAllocation=@onAllocation-@Units
	END
	ELSE
	BEGIN
		--DECREASE QTY FROM ONHAND
		SET @onHand=@onHand-@Units
	END

	 --UPDATE INVENTORY
	UPDATE 
		WareHouseItems 
	SET      
		numOnHand=@onHand,
		numOnOrder=@onOrder,                         
		numAllocation=@onAllocation,
		numBackOrder=@onBackOrder,
		dtModified = GETDATE()                                    
	WHERE 
		numWareHouseItemID=@numWareHouseItemID   
END

IF @numWOId>0
BEGIN
    
	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintRefType NUMERIC(18,0)

	IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			IF ISNULL(@ParentWOID,0) = 0
			BEGIN
				SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END

		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

		SET @numReferenceID = @numWOId
		SET @tintRefType=2
	END

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=0
BEGIN
DECLARE @desc AS VARCHAR(100)
SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

  EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @desc, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=1
BEGIN
SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowMaster_History')
DROP PROCEDURE USP_WorkFlowMaster_History
GO
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
 -- Add the parameters for the stored procedure here      
@numDomainID numeric(18,0),      
@numFormID INT  ,    
@CurrentPage int,                                                              
@PageSize int,                                                              
@TotRecs int output,           
@SortChar char(1)='0' ,                                                             
@columnName as Varchar(50),                                                              
@columnSortOrder as Varchar(50)  ,      
@SearchStr  as Varchar(50)         
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
        numWFID NUMERIC(18,0) ,    
        numFormID NUMERIC(18,0),    
        vcWFName VARCHAR(50),      
        vcPOppName VARCHAR(500),      
        vcWFDescription VARCHAR(max),      
        numRecordID NUMERIC(18,0),      
        tintProcessStatus int,      
        bitSuccess int,      
        vcDescription VARCHAR(max),      
        vcFormName VARCHAR(50),      
        dtExecutionDate DATETIME,    
        numOppId NUMERIC(18,0),      
        STATUS VARCHAR(50),    
        TriggeredOn VARCHAR(50) ,  
        vcUserName VARCHAR(200)   ,
		dtCreatedDate     DATETIME                                                   
 )           
declare @strSql as varchar(8000)                                                        
          
set  @strSql='  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   ISNULL(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  ISNULL( e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,     
   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' WHEN q.tintProcessStatus=6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 dbo.fn_GetContactName(ISNULL(m.numCreatedBy,0)) AS vcUserName   ,
 q.dtCreatedDate   
  FROM dbo.WorkFlowMaster as m       
  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
  LEFT JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3 AND q.tintProcessStatus NOT IN (6,5)  WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
insert into #tempTable( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName,dtCreatedDate) exec(@strSql)          
          
 declare @firstRec as integer                                                              
 declare @lastRec as integer                                                   
 set @firstRec= (@CurrentPage-1) * @PageSize                                                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
 SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
  SELECT       
   ID AS RowNo,*    
   From #tempTable T        
   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   DROP TABLE #tempTable      
           
END 
