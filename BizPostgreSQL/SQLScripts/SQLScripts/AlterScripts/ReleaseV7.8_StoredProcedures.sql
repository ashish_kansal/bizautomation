/******************************************************************
Project: Release 7.8 Date: 27.JULY.2017
Comments: STORE PROCEDURES
*******************************************************************/



--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalBizDocTaxAmt')
DROP FUNCTION fn_CalBizDocTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalBizDocTaxAmt]
(
	@numDomainID as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppID AS NUMERIC(9),
	@numOppItemID as numeric(9),
	@tintMode AS TINYINT,
	@monAmount AS MONEY,
	@numUnitHour FLOAT
) 
RETURNS MONEY
AS
BEGIN
	DECLARE @monTaxAmount AS MONEY = 0

	DECLARE @tintTaxOperator AS TINYINT;
	SET @tintTaxOperator=0
   
	IF @tintMode=1 --Order
	BEGIN
		SELECT @tintTaxOperator=ISNULL([tintTaxOperator],0) FROM [OpportunityMaster] WHERE numOppID=@numOppID and numDomainID=@numDomainID

		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityMasterTaxItems OMTI
			WHERE
				OMTI.numOppId=@numOppID
				AND OMTI.numTaxItemID=0
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @monTaxAmount = 0
		END
		ELSE IF (SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numOppID=@numOppID AND numOppItemId=@numOppItemID AND numTaxItemID=@numTaxItemID)>0
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityItemsTaxItems OITI
			INNER JOIn
				OpportunityMasterTaxItems OMTI
			ON
				OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
			WHERE
				OMTI.numOppId=@numOppID
				AND OITI.numOppItemID=@numOppItemID
				AND OITI.numTaxItemID=@numTaxItemID
		END 
	END

	IF @tintMode=2 --Return RMA
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numReturnItemID=@numOppItemID and numTaxItemID=@numTaxItemID)>0
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityItemsTaxItems OITI
			INNER JOIn
				OpportunityMasterTaxItems OMTI
			ON
				OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
			WHERE
				OITI.numReturnHeaderID=@numOppID 
				AND OITI.numTaxItemID=@numTaxItemID 
				AND OITI.numReturnItemID=@numOppItemID 
		END 
	END

	RETURN ISNULL(@monTaxAmount,0)
END


--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_calitemtaxamt')
DROP FUNCTION fn_calitemtaxamt
GO
CREATE FUNCTION [dbo].[fn_CalItemTaxAmt]
(
	@numDomainID as numeric(9),
	@numDivisionID as numeric(9),
	@numItemCode as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppId as numeric(9),
	@bitAlwaysTaxApply AS BIT,
	@numReturnHeaderID AS NUMERIC(9)
) 
RETURNS @TEMP TABLE
(
	decTaxValue FLOAT, 
	tintTaxType TINYINT,
	numTaxID NUMERIC(18,0)
)
AS
BEGIN

	SET @numOppId=NULLIF(@numOppId,0)
	SET @numReturnHeaderID=NULLIF(@numReturnHeaderID,0)

	DECLARE @numTaxID AS NUMERIC(18,0)
	declare @tintBaseTaxCalcOn as tinyint;
	declare @tintBaseTaxOnArea as tinyint;

	SELECT  @tintBaseTaxCalcOn = [tintBaseTax],@tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

	declare @TaxPercentage as float
	DECLARE @tintTaxType AS TINYINT = 1 -- 1 = PERCENTAGE, 2 = FLAT AMOUNT
	declare @bitTaxApplicable as bit;set @bitTaxApplicable=0 ---Check Tax is applied for the Item

	IF @numTaxItemID = 0 AND @numItemCode <> 0
	BEGIN
		SELECT 
			@bitTaxApplicable=bitTaxable 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode
	END
	ELSE IF @numItemCode <> 0
	BEGIN
		SELECT 
			@bitTaxApplicable=bitApplicable,
			@numTaxID = numTaxID
		FROM 
			ItemTax 
		WHERE 
			numItemCode=@numItemCode 
			AND numTaxItemID=@numTaxItemID
	END
	ELSE IF @numItemCode=0 
	BEGIN
		SET @bitTaxApplicable=1
	END


	---Check Tax is applied for Company
	if @bitTaxApplicable=1 AND @bitAlwaysTaxApply=0
	BEGIN
		set @bitTaxApplicable=0

		if @numTaxItemID=0 
			select @bitTaxApplicable=(case when bitNoTax=1 then 0 else 1 end) from DivisionMaster where numDivisionID=@numDivisionID
		else
			select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=@numDivisionID and numTaxItemID=@numTaxItemID
	END


	if @bitTaxApplicable=1
	BEGIN

		declare @numCountry as numeric(9),@numState as numeric(9)
		declare @vcCity as varchar(100),@vcZipPostal as varchar(20)

		IF @numOppId>0
		BEGIN
		  DECLARE  @tintOppType  AS TINYINT,@tintBillType  AS TINYINT,@tintShipType  AS TINYINT
	 
			SELECT  @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
			  FROM   OpportunityMaster WHERE  numOppId = @numOppId
	 
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				IF @tintBillType IS NULL or (@tintBillType = 1 AND @tintOppType = 1) or (@tintBillType = 1 AND @tintOppType = 2)
				BEGIN
					Select @numState = ISNULL(AD.numState, 0),
					@numCountry = ISNULL(AD.numCountry, 0),
					@vcCity=isnull(AD.VcCity,''),@vcZipPostal=isnull(AD.vcPostalCode,'')
					from AddressDetails AD where AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
				END
				ELSE IF @tintBillType = 0
				BEGIN
					Select @numState = ISNULL(AD1.numState, 0),
						@numCountry = ISNULL(AD1.numCountry, 0),
						@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')
							FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								JOIN Domain D1  ON D1.numDivisionID = div1.numDivisionID
								JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
								WHERE  D1.numDomainID = @numDomainID
				END
				 ELSE IF @tintBillType = 2 or @tintBillType = 3
				BEGIN
					Select @numState = ISNULL(numBillState, 0),
					@numCountry = ISNULL(numBillCountry, 0),
					@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
					FROM   OpportunityAddress WHERE  numOppID = @numOppId
				END
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
				 IF @tintShipType IS NULL or (@tintShipType = 1 AND @tintOppType = 1) or (@tintShipType = 1 AND @tintOppType = 2) 
				BEGIN
						Select @numState = ISNULL(AD2.numState, 0),
						@numCountry = ISNULL(AD2.numCountry, 0),
						@vcCity=isnull(AD2.VcCity,''),@vcZipPostal=isnull(AD2.vcPostalCode,'')				
						from AddressDetails AD2 where AD2.numDomainID=@numDomainID AND AD2.numRecordID=@numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1
				END
				ELSE IF @tintShipType = 0 
				BEGIN
					Select @numState = ISNULL(AD1.numState, 0),
					@numCountry = ISNULL(AD1.numCountry, 0),
					@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')	
					FROM companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
							JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
							JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
							WHERE  D1.numDomainID = @numDomainID
				END
				ELSE IF @tintShipType = 2 or @tintShipType = 3  
				BEGIN
					Select @numState = ISNULL(numShipState, 0),
						@numCountry = ISNULL(numShipCountry, 0),
						@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
						FROM  OpportunityAddress WHERE  numOppID = @numOppId
				END
			END
		END
		ELSE IF @numReturnHeaderID>0
		BEGIN
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				Select @numState = ISNULL(numBillState, 0),
				@numCountry = ISNULL(numBillCountry, 0),
				@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
				FROM   OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			END
			ELSE --Shipping Address(Default)
			BEGIN
				Select @numState = ISNULL(numShipState, 0),
					@numCountry = ISNULL(numShipCountry, 0),
					@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
					FROM  OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			END
		END
		ELSE
		BEGIN
		   IF @tintBaseTaxCalcOn = 2 --Shipping Address(Default)
				   SELECT   @numState = ISNULL(numState, 0),
							@numCountry = ISNULL(numCountry, 0),
							@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
				   FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 2
							AND bitIsPrimary=1
			ELSE --Billing Address
					SELECT   @numState = ISNULL(numState, 0),
							@numCountry = ISNULL(numCountry, 0),
							@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
				   FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 1
							AND bitIsPrimary=1
		END 



		If EXISTS (SELECT tintBaseTaxOnArea FROM TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numCountry)
		BEGIN
			--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
			SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID AND numCountry=@numCountry 
		END
             
		SET @TaxPercentage=0 
             
		IF @numCountry >0 and @numState>0
		BEGIN
			IF @numState>0        
			BEGIN  
				IF EXISTS (SELECT 
								* 
							FROM 
								TaxDetails 
							WHERE 
								numCountryID=@numCountry 
								AND numStateID=@numState
								AND numDomainID= @numDomainID 
								AND numTaxItemID=@numTaxItemID
								AND 1=(Case 
										when @tintBaseTaxOnArea=0 then 1 --State
										when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
										when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
										else 0 end))            
				BEGIN
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT
							decTaxPercentage
							,tintTaxType 
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
							AND 1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
							AND 1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)
					END        
				END
				ELSE IF EXISTS(SELECT * FROM TaxDetails where numCountryID=@numCountry and numStateID=@numState AND numDomainID= @numDomainID and numTaxItemID=@numTaxItemID AND isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
				BEGIN
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')=''      
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')=''      
					END
				END
				ELSE         
				BEGIN    
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT 
							decTaxPercentage
							,tintTaxType
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=0 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')='' 
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=0 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')='' 
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID                   
					END
				END
			END                  
		END
		ELSE IF @numCountry > 0
		BEGIN
			IF @numTaxItemID = 1
			BEGIN
				INSERT INTO @TEMP 
				(
					decTaxValue,
					tintTaxType,
					numTaxID
				)
				SELECT 
					decTaxPercentage
					,tintTaxType 
					,numTaxID
				FROM 
					TaxDetails 
				WHERE 
					numCountryID=@numCountry 
					AND numStateID=0 
					AND ISNULL(vcCity,'')='' 
					AND ISNULL(vcZipPostal,'')='' 
					AND numDomainID= @numDomainID 
					AND numTaxItemID=@numTaxItemID
			END
			ELSE
			BEGIN 
				INSERT INTO @TEMP 
				(
					decTaxValue,
					tintTaxType,
					numTaxID
				)
				SELECT TOP 1 
					decTaxPercentage
					,tintTaxType 
					,numTaxID
				FROM 
					TaxDetails 
				WHERE 
					numCountryID=@numCountry 
					AND numStateID=0 
					AND ISNULL(vcCity,'')='' 
					AND ISNULL(vcZipPostal,'')='' 
					AND numDomainID= @numDomainID 
					AND numTaxItemID=@numTaxItemID
			END
		END
	END

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalOppItemTotalTaxAmt')
DROP FUNCTION fn_CalOppItemTotalTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalOppItemTotalTaxAmt]
(
@numDomainID AS NUMERIC(9),    
@numTaxItemID AS NUMERIC(9),    
@numOppID AS NUMERIC(9), 
@numOppBizDocID AS NUMERIC(9)
) 
RETURNS MONEY
AS
BEGIN
	DECLARE @numOppItemID AS NUMERIC(9) 
	DECLARE @numOppBizDocItemID AS NUMERIC(9) 
	DECLARE @numItemCode AS NUMERIC(9)  
	DECLARE @numUnitHour AS FLOAT
	DECLARE @ItemAmount AS MONEY    
	DECLARE @TotalTaxAmt AS MONEY = 0 
	DECLARE @tintTaxOperator AS TINYINT = 0
	
	SELECT 
		@tintTaxOperator= ISNULL([tintTaxOperator],0) 
	FROM 
		[OpportunityMaster] 
	WHERE 
		numOppID=@numOppID

	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)
	SELECT @numShippingServiceItemID = numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainID

	IF @numOppBizDocID=0
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityItems OI
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=OI.numOppId
				AND OMTI.numTaxItemID = 0
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				OI.numOppID=@numOppID  
				AND ISNULL(OI.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @TotalTaxAmt = 0
		END
		ELSE
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(monTotAmount,0) / 100 END) 
			FROM 
				OpportunityItems OI
			INNER JOIN
				OpportunityItemsTaxItems OITI
			ON
				OI.numoppitemtCode=OITI.numOppItemID
				AND OITI.numOppId=@numOppID
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=@numOppID
				AND OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				OI.numOppID=@numOppID  
				AND ISNULL(OI.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
				AND OITI.numTaxItemID=@numTaxItemID
		END
	END
	ELSE --IF BIZDoc
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(OpportunityBizDocItems.numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(OpportunityBizDocItems.monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityBizDocItems
			JOIN
				OpportunityItems OI
			ON
				OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=OI.numOppId
				AND OMTI.numTaxItemID = 0
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID=@numOppBizDocID  
				AND ISNULL(OpportunityBizDocItems.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
				AND OI.numOppID=@numOppID  
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @TotalTaxAmt = 0
		END
		ELSE
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(OpportunityBizDocItems.numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(OpportunityBizDocItems.monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityBizDocItems
			JOIN
				OpportunityItems OI
			ON
				OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				OpportunityItemsTaxItems OITI
			ON
				OI.numoppitemtCode=OITI.numOppItemID
				AND OITI.numOppId=@numOppID
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=@numOppID
				AND OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID=@numOppBizDocID  
				AND ISNULL(OpportunityBizDocItems.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
				AND OI.numOppID=@numOppID  
				AND OITI.numTaxItemID=@numTaxItemID
		END
	END

	RETURN ISNULL(@TotalTaxAmt,0)
end

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetDealTaxAmount')
DROP FUNCTION GetDealTaxAmount
GO
CREATE FUNCTION [dbo].[GetDealTaxAmount]
(
	@numOppID NUMERIC(18,0),
	@numOppBizDocsId NUMERIC(18,0) = 0,
	@numDomainId NUMERIC(18,0)
)    
RETURNS MONEY
AS    
BEGIN   
	DECLARE @TotalTaxAmt AS MONEY = 0		
	DECLARE @numTaxItemID AS NUMERIC(9)
		
	SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId ORDER BY numTaxItemID 
 
	WHILE @numTaxItemID>-1    
	BEGIN
		SELECT @TotalTaxAmt = @TotalTaxAmt + ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocsId),0)
    
		SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID>@numTaxItemID ORDER BY numTaxItemID
    
		IF @@rowcount=0 
			SET @numTaxItemID=-1    
	END   
	    
	RETURN CAST(ISNULL(@TotalTaxAmt,0) AS MONEY) -- Set Accuracy of Two precision
END
GO

--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist1')
DROP PROCEDURE usp_accountlist1
GO
CREATE PROCEDURE [dbo].[USP_AccountList1]                                                              
		@CRMType numeric,                                                              
		@numUserCntID numeric,                                                              
		@tintUserRightType tinyint,                                                              
		@tintSortOrder numeric=4,                                                                                  
		@SortChar char(1)='0',  
		@numDomainID as numeric(9)=0,                                                       
		@CurrentPage int,                                                            
		@PageSize int,        
		 @TotRecs int output,                                                           
		@columnName as Varchar(MAX),                                                            
		@columnSortOrder as Varchar(10)    ,                    
		@numProfile as numeric   ,                  
		@bitPartner as BIT,       
		@ClientTimeZoneOffset as int,
		@bitActiveInActive as bit,
		@vcRegularSearchCriteria varchar(MAX)='',
		@vcCustomSearchCriteria varchar(MAX)=''   ,
		@SearchText VARCHAR(100) = ''
AS                                                            
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
           
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END

	

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)   

	DECLARE @Nocolumns AS TINYINT = 0                

	SELECT 
		@Nocolumns =ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
	) TotalRows

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 36 AND numRelCntType=2 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		if @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				36,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,2,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=36 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC   
		END
          
		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END


	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	set @strColumns=' isnull(ADC.numContactId,0)numContactId,isnull(DM.numDivisionID,0)numDivisionID,isnull(DM.numTerID,0)numTerID,isnull(ADC.numRecOwner,0)numRecOwner,isnull(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName'   

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(20)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
    SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC            
                             
	WHILE @tintOrder>0                                                  
	BEGIN
		IF @bitCustom = 0                
		BEGIN
			DECLARE @Prefix AS VARCHAR(5)
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation' 
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster' 
				SET @Prefix = 'DM.'
	
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END
			ELSE IF @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                  
			BEGIN
				IF @vcDbColumnName = 'numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
				END
				ELSE IF @vcListItemType='LI'                                                         
				BEGIN
					IF @numListID=40 
					BEGIN
						SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'  
						                                                 
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
						END                              
						IF @vcDbColumnName='numShipCountry'
						BEGIN
							SET @WhereCondition = @WhereCondition
												+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
												+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
						END
						ELSE
						BEGIN
							SET @WhereCondition = @WhereCondition
											+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
											+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
						END
					 END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
					 END
				END                                                        
				ELSE IF @vcListItemType='S'                                                         
				BEGIN 
					
					                                                           
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END
					      						
					IF @vcDbColumnName='numShipState'
					BEGIN
						SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
							+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
					END
					ELSE IF @vcDbColumnName='numBillState'
					BEGIN
						SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
							+ ' left Join State BillState on BillState.numStateID=AD4.numState '
					END
					ELSE  
					BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
					END		                                           
				END                                                        
				ELSE IF @vcListItemType='T'                                                         
				BEGIN
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END                                                           
				  
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
				END  
				ELSE IF @vcListItemType='C'
				BEGIN
					SET @strColumns=@strColumns+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']' 
					 
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
					END  
					                                                  
					SET @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
				END                       
				ELSE IF @vcListItemType='U'                                                     
				BEGIN
					SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
					END                                                          
				END  
				ELSE IF @vcListItemType='SYS'                                                         
				BEGIN
					SET @strColumns=@strColumns+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                        

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end LIKE ''%' + @SearchText + '%'' '    
					END         
				END                     
			END                                                        
			ELSE IF @vcAssociatedControlType='DateField'                                                    
			BEGIN
				 SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
				 SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                  
					
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
				END
			END      
			ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
			BEGIN
				SET @strColumns = @strColumns + ',AD5.vcCity' + ' [' + @vcColumnName + ']'   

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD5.vcCity LIKE ''%' + @SearchText + '%'' '
				END
				
				SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

			END
			ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
			BEGIN
				SET @strColumns=@strColumns + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD6.vcCity LIKE ''%' + @SearchText + '%'' '
				END  
				
				SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
			END
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

				set @strColumns=@strColumns+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ CONCAT(' OUTER APPLY(SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ', @tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ')

				set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                   
			BEGIN
				SET @strColumns=@strColumns+','
								+ case  
									when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' 
									when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
									else @vcDbColumnName 
								end +' ['+ @vcColumnName+']'                
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
					case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when      
				  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end
					+' LIKE ''%' + @SearchText + '%'' '
				END  
			END   
		 	ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				set @strColumns=@strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END  
			ELSE                                                 
			BEGIN
				 set @strColumns=@strColumns+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
			END             
		END                
		ELSE IF @bitCustom = 1                
		BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
                
			SELECT 
				@vcFieldName=FLd_label,
				@vcAssociatedControlType=fld_type,
				@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
			FROM 
				CFW_Fld_Master
			WHERE 
				CFW_Fld_Master.Fld_Id = @numFieldId
			
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
			BEGIN
				SET @strColumns= @strColumns+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
				SET @WhereCondition= @WhereCondition 
									+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
									+ ' on CFW' + convert(varchar(3),@tintOrder) 
									+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
									+ 'and CFW' + convert(varchar(3),@tintOrder) 
									+ '.RecId=DM.numDivisionId   '                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'       
			BEGIN
				set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
												+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
		   END                
			ELSE IF @vcAssociatedControlType = 'DateField'            
			BEGIN
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                         
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'             
			BEGIN
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
				set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
			BEGIN
				SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=DM.numDivisionId '
			END              
		END          
                
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END     

	  
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
   
	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql=' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VIE.numContactId = ADC.numContactId ' + ISNULL(@WhereCondition,'')

	IF @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID ' 
	
	 -------Change Row Color-------
	Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
	SET @vcCSOrigDbCOlumnName=''
	SET @vcCSLookBackTableName=''

	Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

	insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	where DFCS.numDomainID=@numDomainID and DFFM.numFormID=36 AND DFCS.numFormID=36 and isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
	   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END   
	----------------------------                   
 
	 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
	set @strColumns=@strColumns+',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			set @Prefix = 'ADC.'                  
		 if @vcCSLookBackTableName = 'DivisionMaster'                  
			set @Prefix = 'DM.'
		 if @vcCSLookBackTableName = 'CompanyInfo'                  
			set @Prefix = 'CMP.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END

	IF @columnName LIKE 'CFW.Cust%' 
	BEGIN                
		SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                         
		SET @columnName = 'CFW.Fld_Value'                
	END                                         
	ELSE IF @columnName LIKE 'DCust%' 
    BEGIN                
        SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                   
        SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
        SET @columnName = 'LstCF.vcData'                  
    END
	ELSE IF @columnName = 'Opp.vcLastSalesOrderDate'   
	BEGIN
		SET @columnName = 'TempLastOrder.bintCreatedDate'
	END
	
	IF @columnName = 'CMP.vcPerformance'   
	BEGIN
		SET @columnName = 'TempPerformance.monDealAmount'
	END

	SET @strSql = @strSql + ' WHERE  (ISNULL(ADC.bitPrimaryContact,0)=1   OR ADC.numContactID IS NULL)
							AND CMP.numDomainID=DM.numDomainID     
							AND CMP.numCompanyType=46                                                        
							AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                                            
							AND DM.numDomainID= ' + convert(varchar(15),@numDomainID)

	SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive)
	
	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	

	 IF @bitPartner = 1 
	BEGIN
        SET @strSql = @strSql + 'and (DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numCreatedBy=' + CONVERT(VARCHAR(15), @numUserCntID) + ') '                                                                 
    END
	                                                         
    IF @SortChar <> '0' 
	BEGIN
        SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'''                                                               
	END

    IF @tintUserRightType = 1 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
	END
    ELSE IF @tintUserRightType = 2 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
    END
	               
    IF @numProfile <> 0 
	BEGIN
		SET @strSQl = @strSql + ' and cmp.vcProfile = ' + CONVERT(VARCHAR(15), @numProfile)                                                    
    END
	                                                      
    IF @tintSortOrder = 1 
        SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
    ELSE IF @tintSortOrder = 2 
        SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
    ELSE IF @tintSortOrder = 3 
        SET @strSql = @strSql + ' AND (DM.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                  
    ELSE IF @tintSortOrder = 5 
        SET @strSql = @strSql + ' order by numCompanyRating desc '                                                                     
    ELSE IF @tintSortOrder = 6 
        SET @strSql = @strSql + ' AND DM.bintCreatedDate > ''' + CONVERT(VARCHAR(20), DATEADD(day, -7,GETUTCDATE())) + ''''                                                                    
    ELSE IF @tintSortOrder = 7 
        SET @strSql = @strSql + ' and DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                     
    ELSE IF @tintSortOrder = 8 
        SET @strSql = @strSql + ' and DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID)                  
                                                   
        
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
		BEGIN
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
			set @strSql=@strSql
		END
		ELSE
			set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END


	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcCustomSearchCriteria 
    END  

	DECLARE @firstRec AS INTEGER                                                            
	DECLARE @lastRec AS INTEGER                                                            
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)=''
as                              
BEGIN
	DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(20)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
DECLARE @vcOrigDbColumnName as varchar(50)
DECLARE @bitAllowSorting AS CHAR(1)
DECLARE @bitAllowEdit AS CHAR(1)
DECLARE @bitCustomField AS BIT;
DECLARE @ListRelID AS NUMERIC(9)
DECLARE @intColumnWidth INT
DECLARE @bitAllowFiltering AS BIT;
DECLARE @vcfieldatatype CHAR(1)

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN
select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   PRINT @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
 select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType='LI'                               
    begin                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    end                              
    else if @vcListItemType='S'                               
    begin           
       IF @ColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
    end                           
   end       
           
                      
   if (@SortColumnName<>'')                        
  begin                          
 select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType1='LI'                   
    begin     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    end                              
    else if @vcListItemType1='S'           
    begin                
        IF @SortColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType1='T'                  
    begin                              
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    end                           
  end                          
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
--IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
--IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='T'                               
		begin                              
		  set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
    else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			A.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin                              
                                              
			  if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END                       
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
                   
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
	    @vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
		@intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
            vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			A.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',			
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                
                                   
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by C.vcCompanyName, T.ID' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' ,OM.bintCreatedDate AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID      
   JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                                                              
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			numFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			Fld_type,
			'',
			numListID,
			'',
			numFormFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			''
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchItems')
DROP PROCEDURE USP_AdvancedSearchItems
GO
CREATE PROCEDURE USP_AdvancedSearchItems
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(50)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(50)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(10)='' ,        
@strMassUpdate as varchar(2000)=''    
AS 
BEGIN
 
 
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable
  (
  id           INT   IDENTITY   PRIMARY KEY,
  numItemID VARCHAR(15))
  
------------Declaration---------------------  
DECLARE  @strSql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @from         NVARCHAR(1000),
         @Where         NVARCHAR(4000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelectFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
         
 DECLARE @tintOrder AS TINYINT,
		 @vcFormFieldName AS VARCHAR(50),
		 @vcListItemType AS VARCHAR(3),
		 @vcListItemType1 AS VARCHAR(3),
		 @vcAssociatedControlType VARCHAR(20),
		 @numListID AS NUMERIC(9),
		 @vcDbColumnName VARCHAR(30),
		 @vcLookBackTableName VARCHAR(50),
		 @WCondition VARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Where = ''
SET @from = ' FROM item I '
SET @GroupBy = ''
SET @SelectFields = ''  
SET @WCondition = ''


	
--	SELECT I.numItemCode,* FROM item I 
--	LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID
--	LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode --AND I.numVendorID = V.numVendorID
	
	
SET @Where = @Where + ' WHERE I.charItemType <>''A'' and I.numDomainID=' + convert(varchar(15),@numDomainID)
if (@ColumnSearch<>'') SET @Where = @Where + ' and I.vcItemName like ''' + @ColumnSearch + '%'''

CREATE TABLE #temp(ItemID INT PRIMARY KEY)
IF CHARINDEX('SplitString',@WhereCondition) > 0
	BEGIN
		DECLARE @strTemp AS NVARCHAR(MAX)
		SET @WhereCondition	= REPLACE(@WhereCondition,' AND I.numItemCode IN ','')
		SET @WhereCondition = RIGHT(@WhereCondition,LEN(@WhereCondition) - 1)
		SET @WhereCondition = LEFT(@WhereCondition,LEN(@WhereCondition) - 1)
		
		SET @strTemp = 'INSERT INTO #temp ' + @WhereCondition	
		PRINT @strTemp
		EXEC SP_EXECUTESQL @strTemp
		
		set @InneJoinOn= @InneJoinOn + ' JOIN #temp SP ON SP.ItemID = I.numItemCode '
	END
ELSE
	BEGIN
		SET @Where = @Where + @WhereCondition	
	END

	
	
	
set @tintOrder=0
set @WhereCondition =''
set @strSql='SELECT  I.numItemCode '

DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 29                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND A.numFormID = 29 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 29
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 29
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc
while @tintOrder>0
begin

	IF @bitCustom = 0
	BEGIN
		if @vcAssociatedControlType='SelectBox'                              
        begin                              
		  if @vcListItemType='IG' --Item group
		  begin                              
			   set @SelectFields=@SelectFields+',IG.vcItemGroup ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   set @InneJoinOn= @InneJoinOn +' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   SET @GroupBy = @GroupBy + 'IG.vcItemGroup,'
		  end
		  else if @vcListItemType='PP'--Item Type
		  begin
			   set @SelectFields=@SelectFields+', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN  ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   SET @GroupBy = @GroupBy + 'I.charItemType,'
		  END
		  else if @vcListItemType='UOM'--Unit of Measure
		  BEGIN
		  		SET @SelectFields = @SelectFields + ' ,dbo.fn_GetUOMName(I.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
		  else if @vcListItemType='LI'--Any List Item
		  BEGIN
			IF @vcDbColumnName='vcUnitofMeasure'
			BEGIN 		
				SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END 
			ELSE 
				BEGIN
					set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
					set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
					SET @GroupBy = @GroupBy + 'L' + convert(varchar(3),@tintOrder)+'.vcData,'
					IF @SortColumnName ='numItemClassification'
						SET @SortColumnName = 'L'+ convert(varchar(3),@tintOrder)+'.vcData'
					
				END
			END                              
			
		END
		ELSE if @vcAssociatedControlType='ListBox'
		BEGIN
	 		if @vcListItemType='V'--Vendor
			BEGIN
				set @SelectFields=@SelectFields+', dbo.fn_GetComapnyName(I.numVendorID)  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		END
		ELSE
		BEGIN
			IF @vcLookBackTableName ='WareHouseItems'
			BEGIN
				IF @vcDbColumnName = 'vcBarCode'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcBarCode FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE IF @vcDbColumnName = 'vcWHSKU'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcWHSKU FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE
				BEGIN
		 			SET @SelectFields = @SelectFields + ' ,SUM(WI.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
		 			IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 				SET @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
		 		END
			END
			else IF @vcDbColumnName='vcSerialNo'
			BEGIN
--			
				SET @SelectFields = @SelectFields + ' ,0 ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 			set @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
			END
			ELSE IF @vcDbColumnName='vcPartNo'
			BEGIN
				SET @SelectFields = @SelectFields + ' ,(SELECT top 1 ISNULL(vcPartNo,'''') FROM dbo.Vendor V1 WHERE V1.numVendorID =I.numVendorID) as  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE 
			BEGIN
		  		SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+ @vcFormFieldName + '~' + @vcDbColumnName+']'
		  		SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		   
		END

		IF CHARINDEX('WareHouseItmsDTL',@Where)>0
		BEGIN
	  		IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 	set @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
		END
	END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @SelectFields= @SelectFields+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @SelectFields= @SelectFields+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @SelectFields= @SelectFields+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
			set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @SelectFields= @SelectFields+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '
		END

	END
               
                                      
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 29                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND A.numFormID = 29
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 29
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 29
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc     
                            
 if @@rowcount=0 set @tintOrder=0                              
end                              

/*--------------Sorting logic-----------*/
	
	IF @SortColumnName=''
		SET @SortColumnName = 'vcItemName'
	ELSE IF @SortColumnName ='numItemGroup'
		SET @SortColumnName = 'IG.vcItemGroup'
	ELSE IF @SortColumnName ='charItemType'
		SET @SortColumnName = 'I.charItemType'
	ELSE IF @SortColumnName = 'vcPartNo'
		SET @SortColumnName = 'vcItemName'
		
	 IF exists(SELECT * FROM dbo.AdvSerViewConf WHERE numDomainID=@numDomainID AND numFormId=29 AND numGroupID=@numGroupID AND tintViewID=@ViewID)
		SET @OrderBy = ' ORDER BY ' + @SortColumnName + ' '+ @columnSortOrder	
	
/*--------------Sorting logic ends-----------*/
	
	

	
	SET @strSql =  @strSql /*+ @SelectFields */+ @from + @InneJoinOn + @Where + @OrderBy
	PRINT @strSql
	INSERT INTO #temptable (
		numItemID
	) EXEC(@strSql)

/*-----------------------------Mass Update-----------------------------*/
if @bitMassUpdate=1
BEGIN
	IF CHARINDEX('USP_ItemCategory_MassUpdate',@strMassUpdate) >0
	BEGIN
		declare @vcItemCodes VARCHAR(MAX)
		SET @vcItemCodes = ''
		select @vcItemCodes = @vcItemCodes + numItemID + ', ' from #tempTable

		SET @strMassUpdate = REPLACE(@strMassUpdate,'##ItemCodes##',SUBSTRING(@vcItemCodes, 0, LEN(@vcItemCodes)))

		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
	ELSE
	BEGIN
		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
END

	
/*-----------------------------Paging Logic-----------------------------*/
	declare @firstRec as integer
	declare @lastRec as integer
	set @firstRec = ( @CurrentPage - 1 ) * @PageSize
	set @lastRec = ( @CurrentPage * @PageSize + 1 )
	set @TotRecs=(select count(*) from #tempTable)
	
	SET @InneJoinOn =@InneJoinOn+ ' join #temptable T on T.numItemID = I.numItemCode '
	SET @strSql = 'SELECT distinct I.numItemCode  [numItemCode],T.ID [ID~ID],isnull(I.bitSerialized,0) as [bitSerialized],isnull(I.bitLotNo,0) as [bitLotNo] ' +  @SelectFields + @from + @InneJoinOn + @Where 
	IF @GetAll=0
	BEGIN
		SET @strSql = @strSql +' and T.ID > '+convert(varchar(10),@firstRec)+ ' and T.ID <'+ convert(varchar(10),@lastRec) 
	END
	
	-- enable or disble group by based on search result view
	IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		SET @strSql = @strSql + ' order by ID; '
	ELSE 
		SET @strSql = @strSql + ' GROUP BY I.numItemCode,T.ID,I.bitSerialized,I.bitLotNo,I.numVendorID,' + SUBSTRING(@GroupBy, 1, LEN(@GroupBy)-1);
	
	
	EXEC (@strSql)
	PRINT @strSql
	
	DROP TABLE #temptable;
-------------------------------------------------	


	SELECT
		vcDbColumnName,
		vcFormFieldName,
		tintOrder AS tintOrder,
		bitAllowSorting
	FROM
	(
		SELECT
			D.vcDbColumnName,
			D.vcFieldName AS vcFormFieldName,
			A.tintOrder AS tintOrder,
			isnull(D.bitAllowSorting,0) AS bitAllowSorting
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 29                          
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND A.numFormID = 29 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			A.tintOrder AS tintOrder,
			1 AS bitAllowSorting
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 29
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 29
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1      
	UNION 
	SELECT 'ID','numItemCode',0 tintOrder,0 
	order by T1.tintOrder ASC




DROP TABLE #temp
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@ViewID as tinyint,
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@numGroupID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)=''
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(20)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT




IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
 IF CHARINDEX('vcNotes', @WhereCondition) > 0
	BEGIN
			SET 	@bitIsSearchBizDoc = 1
	END
--End of script

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
	  numContactID NUMERIC(18,0),
	  numDivisionID NUMERIC(18,0),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId, OppMas.numDivisionID, ADC.numContactId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount money ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId, OppMas.numDivisionID, ADC.numContactId '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END


DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 15                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND A.numFormID = 15 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 15
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 15
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                                                             

while @tintOrder>0                                  
begin                                  
	IF @bitCustom = 0
	BEGIN
	 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
		if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
			IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcColumnName+']'
				
            END                                            
			ELSE if @vcListItemType='LI'                                   
			begin   
				IF @numListID=40--Country
				BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'++' ['+ @vcColumnName+']'                              
				IF @vcDbColumnName ='numBillCountry'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
				END
				ELSE IF @vcDbColumnName ='numShipCountry'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
				END
			  END                        
			  ELSE
			  BEGIN                               
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
				
				if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
				else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
				else
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  END                                  
        END                      
		END 
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'
				IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end  
		END  
		else if @vcAssociatedControlType = 'CheckBox'           
		   begin            
               
			set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end  ['+ @vcColumnName+']'              
 
		   end                                  
		 else 
				BEGIN
					IF @bitIsSearchBizDoc = 0
					BEGIN
						IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
						  BEGIN
	  							SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
						  END	
					END
	  
					set @strSql=@strSql+','+ 
						case  
							when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
							when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
							when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
							when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
							when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
									'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
							WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							else @vcDbColumnName end+ ' ['+ @vcColumnName+']'
				END
    END
	ELSE IF @bitCustom = 1
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '
	END
	END            
                                          
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 15                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND A.numFormID = 15
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 15
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 15
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                   

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END
	--End of Script

SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID 
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 

 SELECT 
 		tintOrder,
		numFormFieldID,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		intColumnWidth,
		bitCustom as bitCustomField,
		numGroupID,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			numFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			0 AS numGroupID
			,vcOrigDbColumnName
			,bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 15                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND A.numFormID = 15
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			Grp_id
			,'',
			null,
			null,
			null,
			null,
			''
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 15
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 15
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc  

--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	FROM 
		OpportunityItems OI 
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	WHERE 
		OI.numOppId=@numNewOppID AND 
		OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numNewOppID)
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9) 
DECLARE @bitIsPrimary BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1
END


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN  
	INSERT INTO dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId 
		AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivisionID	
	UNION 
	SELECT
		@numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numNewOppID,1,NULL)	
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist1')
DROP PROCEDURE usp_companylist1
GO
CREATE PROCEDURE [dbo].[USP_CompanyList1]                                                      
@numRelationType as numeric(9),                                                   
@numUserCntID numeric(9),                                                      
@tintUserRightType tinyint,                                                          
@SortChar char(1)='0',                                                     
@FirstName varChar(100)= '',                                                      
@LastName varChar(100)= '',                                                      
@CustName varChar(100)= '',                                                    
@CurrentPage int,                                                    
@PageSize int,                                                    
@columnName as Varchar(50),                                                    
@columnSortOrder as Varchar(10),                                            
@tintSortOrder numeric=0,                                             
@numProfile as numeric(9)=0,                                    
@numDomainID as numeric(9)=0,                            
@tintCRMType as tinyint,                            
@bitPartner as BIT,
@numFormID AS NUMERIC(9),
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                     
as       
	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END

    declare @firstRec as integer                                                                
  declare @lastRec as integer                                                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                          
  declare @column as varchar(50)                  
set @column = @columnName                  
declare @lookbckTable as varchar(50)                  
set @lookbckTable = ''                                                               
                                                    
  declare @strSql as varchar(MAX)                                                  
set @strSql='with tblCompany as (SELECT  '          
if @tintSortOrder=7 or @tintSortOrder=8  set @strSql=@strSql + ' '             
if @tintSortOrder=5  set @strSql=@strSql + 'ROW_NUMBER() OVER ( order by numCompanyRating desc) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '           
else           
 set @strSql=@strSql +' ROW_NUMBER() OVER (ORDER BY '+@column+' '+ @columnSortOrder+') AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '                                                       
set @strSql=@strSql +'  DM.numDivisionID                                          
    FROM  CompanyInfo CMP                                 
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID 
	left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
	left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId '                                            
                               
if @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                            
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0                           
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                          
set @strSql=@strSql + ' ##JOIN##                                                     
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus 
left Join AddressDetails AD on AD.numRecordId= DM.numDivisionID and AD.tintAddressOf=2 and AD.tintAddressType=1 AND AD.bitIsPrimary=1 and AD.numDomainID= DM.numDomainID                                                   
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL)                                                    
    and CMP.numDomainID=  DM.numDomainID                                                    
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                                    
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                     
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                                    
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                               
if @numRelationType<>'0' set    @strSql=@strSql+' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''                                                    
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                                                     
if @tintUserRightType=1 set @strSql=@strSql + ' AND ((DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or ( CA.bitShareportal=1 and CA.bitDeleted=0))' else ')' end                                                 
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ') '                     
if @tintSortOrder=1  set @strSql=@strSql + ' AND DM.numStatusID=2 '                                                                
else if @tintSortOrder=2  set @strSql=@strSql + ' AND DM.numStatusID=3 '                                                                          
else if @tintSortOrder=3  set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                       
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                                                                       
else if @tintSortOrder=6  set @strSql=@strSql + ' AND DM.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                                                      
else if @tintSortOrder=7  set @strSql=@strSql + ' and DM.numCreatedby='+convert(varchar(15),@numUserCntID)                    
--+ ' ORDER BY DM.bintCreateddate desc '                                                              
else if @tintSortOrder=8  set @strSql=@strSql + ' and DM.numModifiedby='+convert(varchar(15),@numUserCntID)          
           
--if (@tintSortOrder=7 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X   '                                                     
--else if (@tintSortOrder=8 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X  '                                                               
--else 
if @tintSortOrder=9  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''              
                                       
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                                            
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                                              
                                                  
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria
                                                  
                                                    
  set @strSql=@strSql + ')'                      
   
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)           
declare @numListID AS numeric(9)                                        
declare @vcAssociatedControlType varchar(20)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(MAX)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)             
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)            
declare @vcColumnName AS VARCHAR(500)                          
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                 
declare @DefaultNocolumns as tinyint                      
set @Nocolumns=0  

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType) TotalRows

                    
  set @DefaultNocolumns=  @Nocolumns  
                       
while @DefaultNocolumns>0                      
begin                     
 set @DefaultNocolumns=@DefaultNocolumns-1                    
end              
   

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    


set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'                       

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelationType AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN      

	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select @numFormID,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=@numFormID and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc   
	END          
    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
 END                                                  
                        
--    set @DefaultNocolumns=  @Nocolumns  
Declare @ListRelID as numeric(9)     
      select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                                                    
while @tintOrder>0                                                        
begin                                             
     print @vcFieldName                                               
    if @bitCustom = 0            
 begin                      
       
      
 declare @Prefix as varchar(5)                            
      if @vcLookBackTableName = 'AdditionalContactsInformation'                            
		set @Prefix = 'ADC.'                            
      else if @vcLookBackTableName = 'DivisionMaster'                            
		set @Prefix = 'DM.'   
		
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
	
 if @vcAssociatedControlType='SelectBox'                                                              
        begin                                                              
                                                                  
     if @vcListItemType='LI'                                                               
     BEGIN
     	IF @numListID=40 
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'          
			
			IF @vcDbColumnName='numShipCountry'
			BEGIN
				SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
			END
			ELSE
			BEGIN
				SET @WhereCondition = @WhereCondition
								+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
								+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
			END
		 END
		 ELSE
		 BEGIN    
		  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                             
		  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
		 END
     end    
       else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end                                                            
     else if @vcListItemType='S'                                                               
     begin       
		IF @vcDbColumnName='numShipState'
		BEGIN
			SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
				+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
		END
		ELSE IF @vcDbColumnName='numBillState'
		BEGIN
			SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
				+ ' left Join State BillState on BillState.numStateID=AD4.numState '
		END
                                                       
--      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                              
--      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                              
     end                                                              
     else if @vcListItemType='T'                                                               
     begin                                                              
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                              
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
     end                             
     else if   @vcListItemType='U'                                                           
    begin                             
                                
                                  
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                              
    end
	ELSE IF @vcListItemType='SYS'                                                         
	BEGIN
		SET @strSql=@strSql+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                              
	END                                  
    end           
          
else if @vcAssociatedControlType='DateField'                                                        
 begin                  
             
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '            
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                      
   end          
ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
BEGIN
	SET @strSql = @strSql + ',AD5.vcCity' + ' [' + @vcColumnName + ']'  
				
	SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

END
ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
BEGIN
	SET @strSql=@strSql + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
						
	SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
END                
else if @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                       
begin          
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when          
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                    
             
 end          
else                                                  
begin      
 set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end    
            
 end            
else if @bitCustom = 1            
begin            
               
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                           
    from CFW_Fld_Master                                            
   where  CFW_Fld_Master.Fld_Id = @numFieldId             
            
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'    
          
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin              
                 
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
    else if @vcAssociatedControlType = 'CheckBox'              
   begin              
                 
-- OLD    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+']'               
set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'               
  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                       
   end              
   else if @vcAssociatedControlType = 'DateField'          
   begin    
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'                
   begin              
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                      
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END                    
end                
   
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                  
                       
                                      
                                                       
end                             


set @strSql=@strSql+' ,TotalRowCount,
	ISNULL(VIE.Total,0) as TotalEmail,
	ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
  From CompanyInfo CMP                                                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                                      
    left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
    left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId
   '+@WhereCondition+       
' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                                          
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL) and 
  CMP.numDomainID=  DM.numDomainID and DM.numDomainID = ' + convert(varchar(18),@numDomainID) + '
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)     
  
  

SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)              
-- new code added by sojan 05 Mar 2010

--declare @vcTestSql varchar(8000);
--declare @FieldCount numeric(8);
--
--set @vcTestSql = substring(@strSql, charindex('select',@strSql),len(@strSql))
--select @FieldCount= [dbo].[uf_charCount] (@vcTestSql,',')
--
--set @vcTestSql=' union select count(*),' + replicate('Null,',@FieldCount-2)
--
--set @vcTestSql=substring(@vcTestSql,1,len(@vcTestSql)-1)
--set @vcTestSql = @vcTestSql +' from tblCompany order by RowNumber'   
--
--set @strSql=REPLACE(@strSql,'|',',') + @vcTestSql

-- old code
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblCompany order by RowNumber'                                                 
      
SET @strSql =@strSql + ' ORDER BY '+@column+' '+ @columnSortOrder+' '                                
print @strSql                              
                      
exec (@strSql) 


SELECT * FROM #tempForm

DROP TABLE #tempForm
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0 
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;



	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId >0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			
			IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
			BEGIN
				INSERT INTO OpportunityBizDocs
					   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
						[bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID],bitShippingGenerated,numSourceBizDocId)
				SELECT @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
						@bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId,@bitShippingGenerated,@numSourceBizDocId
				FROM OpportunityBizDocs OBD WHERE numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId     
			END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID],bitShippingGenerated,numSourceBizDocId)
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId,@bitShippingGenerated,@numSourceBizDocId)
        END
        
		SET @numOppBizDocsId = SCOPE_IDENTITY()
		
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
		BEGIN
			IF DATALENGTH(@strBizDocItems)>2
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity FLOAT,
					Notes varchar(500),
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END
		END
		ELSE
		BEGIN
			IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
			BEGIN
				DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

				SELECT TOP 1
					@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
				FROM 
					OpportunityBizDocs 
				WHERE 
					numOppId=@numOppId 
					AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
					AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
				ORDER BY
					numOppBizDocsId

				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount,
					vcNotes
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
					OI.vcNotes
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				INNER JOIN
					(
						SELECT 
							OpportunityBizDocItems.numOppItemID,
							OpportunityBizDocItems.numUnitHour
						FROM 
							OpportunityBizDocs 
						JOIN 
							OpportunityBizDocItems 
						ON 
							OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
						WHERE 
							numOppId=@numOppId 
							AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
					) TempBizDoc
				ON
					TempBizDoc.numOppItemID = OI.numoppitemtCode
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


				UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
			END
			ELSE
			BEGIN
		   		INSERT INTO OpportunityBizDocItems                                                                          
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount,
					vcNotes
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc 
						WHEN 1 
						THEN 1 
						ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
					END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
					OI.vcNotes
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				OUTER APPLY
					(
						SELECT 
							SUM(OBDI.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs OBD 
						JOIN 
							dbo.OpportunityBizDocItems OBDI 
						ON 
							OBDI.numOppBizDocId  = OBD.numOppBizDocsId
							AND OBDI.numOppItemID = OI.numoppitemtCode
						WHERE 
							numOppId=@numOppId 
							AND 1 = (CASE 
										WHEN @bitAuthBizdoc = 1 
										THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
										WHEN @numBizDocId = 304 --Deferred Income
										THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
									END)
					) TempBizDoc
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
			END
		END

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY,
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost money,
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAdvancedSearchFieldList' ) 
    DROP PROCEDURE USP_GetAdvancedSearchFieldList
GO
CREATE PROCEDURE [dbo].[USP_GetAdvancedSearchFieldList]
    @numDomainId NUMERIC(9),
    @numFormId INT,
    @numAuthGroupId NUMERIC(9)
AS  
	--added by chintan to select Custom field form multiple location
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	--DECLARE @vcSectionName VARCHAR(500);SET @vcSectionName=''

	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

	--select ItemNumber as intSectionID,Item as vcSectionName from dbo.DelimitedSplit8K(@vcSectionName,',')
	select intSectionID,vcSectionName,Loc_id from DycFormSectionDetail where @numFormId=numFormId

    CREATE TABLE #tempFieldsList(ID int IDENTITY(1,1) NOT NULL,RowNo int,intSectionID int,numModuleID numeric(9),numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),vcPropertyName NVARCHAR(50),vcToolTip NVARCHAR(1000),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),GRP_ID int,intFieldMaxLength int)

DECLARE @Nocolumns AS TINYINT;SET @Nocolumns = 0                

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID ) TotalRows

if @Nocolumns>0 
BEGIN

  INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID, intFieldMaxLength
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
       
       UNION
    
    SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType AS vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    CONVERT(VARCHAR(15), numFieldId) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                   tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID  AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))

END
ELSE
BEGIN 
   INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
            SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID,intFieldMaxLength
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID = @numDomainId and intSectionID!=0
					 AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
			UNION
            
			SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    L.vcFieldType AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CONVERT(VARCHAR(15), Fld_id) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
   
END
Update #tempFieldsList set RowNO=RowNO-1
Update #tempFieldsList set  intRowNum=(RowNo/3)+1 ,intColumnNum= (RowNo%3)+1 

SELECT * FROM #tempFieldsList ORDER BY intRowNum,intColumnNum
        
DROP TABLE #tempFieldsList
        
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCaseList1]    Script Date: 07/26/2008 16:16:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaselist1')
DROP PROCEDURE usp_getcaselist1
GO
CREATE PROCEDURE [dbo].[USP_GetCaseList1]                                                               
	 @numUserCntId numeric(9)=0,                                            
	 @tintSortOrder numeric=0,                                                                                
	 @numDomainID numeric(9)=0,                                                            
	 @tintUserRightType tinyint,                                                            
	 @SortChar char(1)='0' ,                                                                                                                   
	 @CurrentPage int,                                                            
	 @PageSize int,                                                            
	 @TotRecs int output,                                                            
	 @columnName as Varchar(50),                                                            
	 @columnSortOrder as Varchar(10),                                                          
	 @numDivisionID as numeric(9)=0,                              
	 @bitPartner as bit=0,
	 @vcRegularSearchCriteria varchar(1000)='',
	 @vcCustomSearchCriteria varchar(1000)='' ,
	 @ClientTimeZoneOffset as INT,                                                          
	 @numStatus AS BIGINT = 0,
	 @SearchText VARCHAR(MAX) = ''
AS        
	
	CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))


	DECLARE @Nocolumns AS TINYINT                
	SET @Nocolumns=0                
 
	SELECT @Nocolumns=isnull(sum(TotalRow),0)  FROM(            
	SELECT COUNT(*) TotalRow from View_DynamicColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 12 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			12,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=12 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			12,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=12 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=12 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=12 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
	if @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
		select 12,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=12 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
		order by tintOrder asc      
	END

	INSERT INTO #tempForm
	select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
	 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	 FROM View_DynamicColumns 
	 where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
		   UNION
    
		   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
	 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	 from View_DynamicCustomColumns
	where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	 AND ISNULL(bitCustom,0)=1
 
	  order by tintOrder asc  
  
END     

	DECLARE  @strColumns AS VARCHAR(MAX)

	SET @strColumns=' ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,ISNULL(cs.numAssignedTo,0) AS numAssignedTo,DM.tintCRMType,cs.numCaseID,cs.vcCaseNumber'                

	DECLARE @tintOrder as tinyint                                                  
	DECLARE @vcFieldName as varchar(50)                                                  
	DECLARE @vcListItemType as varchar(3)                                             
	DECLARE @vcListItemType1 as varchar(1)                                                 
	DECLARE @vcAssociatedControlType varchar(20)                                                  
	DECLARE @numListID AS numeric(9)                                                  
	DECLARE @vcDbColumnName varchar(20)                      
	DECLARE @WhereCondition varchar(2000)                       
	DECLARE @vcLookBackTableName varchar(2000)                
	DECLARE @bitCustom as bit          
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
    DECLARE @SearchQuery AS VARCHAR(MAX) = ''            
	SET @tintOrder=0                                                  
	SET @WhereCondition =''    
	--set @DefaultNocolumns=  @Nocolumns              
	Declare @ListRelID as numeric(9) 

	  SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
			@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                               
			@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	  FROM  
			#tempForm 
	  ORDER BY 
			tintOrder 
	  ASC            
                                        
WHILE @tintOrder>0                                
BEGIN                                                  
 if @bitCustom = 0        
 begin        
    declare @Prefix as varchar(5)                
      if @vcLookBackTableName = 'AdditionalContactsInformation'                
    set @Prefix = 'ADC.'                
      if @vcLookBackTableName = 'DivisionMaster'                
    set @Prefix = 'DM.'                
      if @vcLookBackTableName = 'Cases'                
    set @PreFix ='Cs.'      
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
    
     if @vcAssociatedControlType='SelectBox'                                                  
     begin                                                  
                                     
     if @vcListItemType='LI'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
		  END                                                     
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='S'                                                   
     begin                                                  
      set @strColumns=@strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'     
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
		  END                                                
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='T'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
	   IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '
		  END                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                 
    ELSE IF   @vcListItemType='U'                                               
		BEGIN        
			SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'
			  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '
		  END                                                     
		END                
    END           
    ELSE IF @vcAssociatedControlType='DateField'                                                  
   begin            
		IF @vcDbColumnName='intTargetResolveDate'
		BEGIN
			 set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
			 set @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               
			IF LEN(@SearchText) > 0
			BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
			END
		END
	ELSE
	BEGIN
     set @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
	 IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END
	END
   -- end      
--  else      
--     set @strSql=@strSql+',dbo.FormatedDateFromDate('+@vcDbColumnName+','++convert(varchar(10),@numDomainId)+') ['+ @vcFieldName+'~'+ @vcDbColumnName+']'            
   END          
    ELSE IF @vcAssociatedControlType='TextBox'                                                  
	   BEGIN           
		 SET @strColumns=@strColumns+','+ case                
		WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
		WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
		WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
		WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
		WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
		ELSE @vcDbColumnName end+' ['+ @vcColumnName+']'     
		IF LEN(@SearchText) > 0
		 BEGIN
			SET @SearchQuery = @SearchQuery +  (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (case                
			WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
			WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
			WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
			WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
			WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
			ELSE @vcDbColumnName end) + ' LIKE ''%' + @SearchText + '%'' '
		 END       
	   END  
  ELSE IF  @vcAssociatedControlType='TextArea'                                              
  begin  
       set @strColumns=@strColumns+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'            
   end 
    	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Cs.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=Cs.numCaseID
				AND UM.numDomainID=Cs.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end     
 end                                            
 Else                                                    
 Begin        
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
        
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
   begin         
           
    set @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
           
   else if @vcAssociatedControlType = 'CheckBox'          
   begin          
             
    set @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'           
   begin        
           
    set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
   else if @vcAssociatedControlType = 'SelectBox'            
   begin          
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)        
    set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'        
   end           
 End        
                                                
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
           
end    

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=7 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                              
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	                                                        
	 set @StrSql=@StrSql+'  FROM AdditionalContactsInformation ADC                                                             
	 JOIN Cases  Cs                                                              
	   ON ADC.numContactId =Cs.numContactId                                                             
	 JOIN DivisionMaster DM                                                              
	  ON ADC.numDivisionId = DM.numDivisionID                                                             
	  JOIN CompanyInfo CMP                                                             
	 ON DM.numCompanyID = CMP.numCompanyId                                                             
	 left join listdetails lst                                                            
	 on lst.numListItemID= cs.numPriority
	 left join listdetails lst1                                                            
	 on lst1.numListItemID= cs.numStatus                                                           
	 left join AdditionalContactsInformation ADC1                          
	 on Cs.numAssignedTo=ADC1.numContactId                                 
	  ' + ISNULL(@WhereCondition,'')  
	 
	IF @bitPartner=1 
		SET @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                              

	if @columnName like 'CFW.Cust%'         
	begin        
		set @columnName='CFW.Fld_Value'        
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
	end                                 
	if @columnName like 'DCust%'        
	begin        
		set @columnName='LstCF.vcData'                                                  
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                           
		set @StrSql= @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'        
	end 

	       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=12 AND DFCS.numFormID=12 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strColumns=@strColumns+',tCS.vcColorScheme'
END
       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'Cases'                  
		set @Prefix = 'Cs.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

	SET @strSql = @StrSql + ' Where cs.numDomainID='+ convert(varchar(15),@numDomainID ) + ''
 
 
	IF @numStatus <> 0
	BEGIN
		SET @strSql= @strSql + ' AND cs.numStatus = ' + CONVERT(VARCHAR(10),@numStatus)                                      	
	END
	ELSE IF @tintSortOrder <> 5
	BEGIN
 		SET @strSql= @strSql + ' AND cs.numStatus <> 136 ' 
	END
                                                 
	if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                                      
	if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                                              
	if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                                             

	IF @tintSortOrder!=6
	BEGIN
		if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) + 
		+ case when @tintSortOrder=1 THEN ' OR (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)' ELSE '' end
		+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +')'                
		else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                   
	END
	ELSE
	BEGIN
		SET @strSql=@strSql + ' AND (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)'
	END                 
                                                   
	if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+ ' or ' + @strShareRedordWith +')'                                                                                                     
	ELSE if @tintSortOrder=2  set @strSql=@strSql + 'And cs.numStatus<>136  ' + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                 
	else if @tintSortOrder=3  set @strSql=@strSql+'And cs.numStatus<>136  ' + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)       
	else if @tintSortOrder=4  set @strSql=@strSql+ 'And cs.numStatus<>136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
	else if @tintSortOrder=5  set @strSql=@strSql+ 'And cs.numStatus=136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
 
	IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' + @vcCustomSearchCriteria + ')'


	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	
	
	declare @firstRec as integer                                                            
	declare @lastRec as integer                                                            
	set @firstRec= (@CurrentPage-1) * @PageSize                                                            
	set @lastRec= (@CurrentPage*@PageSize+1)  

	-- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT

	

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist1')
DROP PROCEDURE usp_getcontactlist1
GO
CREATE PROCEDURE [dbo].[USP_GetContactList1]                                                                                   
@numUserCntID numeric(9)=0,                                                                                    
@numDomainID numeric(9)=0,                                                                                    
@tintUserRightType tinyint=0,                                                                                    
@tintSortOrder tinyint=4,                                                                                    
@SortChar char(1)='0',                                                                                   
@FirstName varChar(100)= '',                                                                                  
@LastName varchar(100)='',                                                                                  
@CustName varchar(100)='',                                                                                  
@CurrentPage int,                                                                                  
@PageSize int,                                                                                  
@columnName as Varchar(50),                                                                                  
@columnSortOrder as Varchar(10),                                                                                  
@numDivisionID as numeric(9)=0,                                              
@bitPartner as bit ,                                                   
@inttype as numeric(9)=0,
@ClientTimeZoneOffset as int,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                                             
--                                                                                    
as                  
                       
declare @join as varchar(400)                  
set @join = ''                 
                        
 if @columnName like 'CFW.Cust%'                 
begin                
                
                 
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                         
 set @columnName='CFW.Fld_Value'                
                  
end                                         
if @columnName like 'DCust%'                
begin                
                                                       
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                                   
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
 set @columnName='LstCF.vcData'                  
                
end                                                                              
                                                                                  
declare @firstRec as integer                                                                                  
declare @lastRec as integer                                                                                  
set @firstRec= (@CurrentPage-1) * @PageSize                              
set @lastRec= (@CurrentPage*@PageSize+1)                                                                                   
                                                                                  
                                                                                  
declare @strSql as varchar(8000)                                                                            
 set  @strSql='with tblcontacts as (Select '                                                                          
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                                
                                     
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE when @columnName='ADC.numAge' then 'ADC.bintDOB' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   ADC.numContactId                                   
  FROM AdditionalContactsInformation ADC                                
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId '  +@join+  '                                                                  
  left join ListDetails LD on LD.numListItemID=cmp.numCompanyRating                                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus 
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1 '                                           
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                                                   
                                                                    
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                                                     
                                                       
  set  @strSql= @strSql +'                                                                
        where 
 --DM.tintCRMType<>0     AND 
 cmp.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                             
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+''                             
 if @inttype<>0 and @inttype<>101  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)                            
 if @inttype=101  set @strSql=@strSql+' and ADC.bitPrimaryContact  =1 '                            
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                             
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                              
    if @CustName<>'' set @strSql=@strSql+' and cmp.vcCompanyName like '''+@CustName+'%'''                                                                                  
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                                                           
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
   
else if @tintUserRightType=2 set @strSql=@strSql + ' AND DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) '                                                           
                
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)                      
                                                                                         
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                                                      
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                    
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)                      
                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)                      
                                                     
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                                             
                    
 if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                        
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                                
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' '                      
    
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria

set @strSql=@strSql + ')'                      
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype) TotalRows

                   
--declare @strColumns as varchar(2000)                      
--set @strColumns=''                      
--while @DefaultNocolumns>0                      
--begin                      
--                  
-- set @strColumns=@strColumns+',null'                      
-- set @DefaultNocolumns=@DefaultNocolumns-1                      
--end                   
                     
   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 10 AND numRelCntType=@inttype AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		10,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		10,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@inttype,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN                 
     
	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select 10,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=10 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc 
	END
                                   
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
 FROM View_DynamicColumns 
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  

END
   
set @strSql=@strSql+' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
       ,ISNULL(ADC.numECampaignID,0) as numECampaignID
	   ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
	   ,ISNULL(VIE.Total,0) as TotalEmail
	   ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
       ,ISNULL(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType ,RowNumber'                      
                                                    
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                          
 if @vcDbColumnName = 'vcPassword'
 BEGIN
	set @strSql=@strSql+',ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=ADC.numContactID),'''') ['+ @vcColumnName+']'  
 END
 ELSE if @vcAssociatedControlType='SelectBox'                                                        
begin                                                                                                      
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                 
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end
   ELSE
	BEGIN
		SET @strSql = @strSql + CONCAT(',dbo.GetCustFldValueContact(',@numFieldId,',ADC.numContactID)') + ' [' + @vcColumnName + ']'
	END                
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END                         
                                  
set @strSql=@strSql+' ,TotalRowCount FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                      
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+                      
' join tblcontacts T on T.numContactId=ADC.numContactId'                     
  
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
--		set @strSql=@strSql+' left join #tempColorScheme tCS on ' + @Prefix + @vcCSOrigDbCOlumnName + '> DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,1,CHARINDEX(''~'',tCS.vcFieldValue)-1) AS INT),GETDATE())
--			 and ' + @Prefix + @vcCSOrigDbCOlumnName + '< DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,CHARINDEX(''~'',tCS.vcFieldValue)+1,len(tCS.vcFieldValue) - PATINDEX(''~'',tCS.vcFieldValue)) AS INT),GETDATE())'
		
		
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
    

set @strSql=@strSql+' WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID   
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'

                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

drop table #tempColorScheme
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0'  
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'


	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition

	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	END
	ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	

	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
/****** Object:  StoredProcedure [dbo].[USP_GetDepositDetails]    Script Date: 07/26/2008 16:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDepositDetailsWithClass' ) 
    DROP PROCEDURE USP_GetDepositDetailsWithClass
GO
CREATE PROCEDURE [dbo].[USP_GetDepositDetailsWithClass]
    @numDepositId AS NUMERIC(9) = 0 ,
    @numDomainId AS NUMERIC(9) = 0 ,
    @tintMode AS TINYINT = 0, -- 1= Get Undeposited Payments 
    @numCurrencyID numeric(9) =0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    BEGIN  
      				
	    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
      
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
			  
        IF @tintMode = 0 
            BEGIN 
    	
    
                SELECT  numDepositId ,
                        dbo.fn_GetComapnyName(numDivisionID) vcCompanyName ,
                        numDivisionID ,
                        numChartAcntId ,
                        dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
                        dtDepositDate AS [dtOrigDepositDate],
                        monDepositAmount ,
                        numPaymentMethod ,
                        vcReference ,
                        vcMemo ,
                        tintDepositeToType,tintDepositePage,bitDepositedToAcnt
                        ,DM.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount
						,ISNULL(DM.numAccountClass,0) AS numAccountClass
                FROM    dbo.DepositMaster DM
						LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
                WHERE   numDepositId = @numDepositId
                        AND DM.numDomainId = @numDomainId
        
                SELECT  DD.numDepositeDetailID ,
                        DD.numDepositID ,
                        DD.numOppBizDocsID ,
                        DD.numOppID ,
                        DD.monAmountPaid ,
                        DD.numChildDepositID ,
                        DD.numPaymentMethod ,
                        DD.vcMemo ,
                        DD.vcReference ,
                        DD.numClassID ,
                        DD.numProjectID ,
                        DD.numReceivedFrom ,
                        DD.numAccountID,
                        GJD.numTransactionId AS numTransactionID,
                        GJD1.numTransactionId AS numTransactionIDHeader
                        ,ISNULL(DM.numCurrencyID,0) numCurrencyID
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						
                FROM    dbo.DepositeDetails DD
                LEFT JOIN dbo.DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
                LEFT JOIN dbo.General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7 
                LEFT JOIN dbo.General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6 
                WHERE   DD.numDepositID = @numDepositId
            END        
        ELSE 
            IF @tintMode = 1 
                BEGIN
                
                --Get Undeposited payments
                SELECT * INTO #tempDeposits FROM 
				(
				--Get Saved deposit entries for edit mode
				SELECT  Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							DD.numDepositeDetailID ,
							DM.numDepositId ,
							DM.numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid AS monDepositAmount,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numReceivedFrom AS numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							 (SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
							LEFT JOIN dbo.TransactionHistory TH ON TH.numTransHistoryID= DM.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DD.numDepositID = @numDepositId AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR ISNULL(@numAccountClass,0)=0)
					UNION
					SELECT Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							0 numDepositeDetailID ,
							numDepositId ,
							numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							monDepositAmount ,
							numPaymentMethod ,
							vcReference ,
							vcMemo ,
							DM.numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							(SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositMaster DM
							INNER JOIN dbo.Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
							LEFT JOIN dbo.TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DM.numDomainId = @numDomainID
							AND tintDepositeToType = 2
							AND ISNULL(bitDepositedToAcnt,0) = 0 
							AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
							AND (ISNULL(DM.numAccountClass,0)=@numAccountClass  OR ISNULL(@numAccountClass,0)=0)
			  )  [Deposits]
			
			SELECT  @TotRecs = COUNT(*) FROM #tempDeposits
						
			SELECT  * 
			FROM    #tempDeposits
			WHERE   row > @firstRec
					AND row < @lastRec ;
            
            DROP TABLE #tempDeposits        
					
			-- get new deposite Entries
					SELECT  DD.numDepositeDetailID ,
							DM.numDepositId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid ,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numAccountID,
--							 COA.vcAccountCode numAcntType,
							DD.numClassID,
							DD.numProjectID,
							DD.numReceivedFrom,
							dbo.fn_GetComapnyName(DD.numReceivedFrom) ReceivedFrom,
							bitDepositedToAcnt
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numDepositID = DM.numDepositId
--							LEFT JOIN dbo.Chart_Of_Accounts COA ON DD.numAccountID = COA.numAccountId
					WHERE   DD.numDepositID = @numDepositId AND ISNULL(numChildDepositID,0) = 0
					AND (ISNULL(DM.numAccountClass,0)=@numAccountClass  OR ISNULL(@numAccountClass,0)=0)
					
						
                END
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetGatewatDTls]    Script Date: 07/26/2008 16:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getgatewatdtls')
DROP PROCEDURE usp_getgatewatdtls
GO
CREATE PROCEDURE [dbo].[USP_GetGatewatDTls]    
@numDomainID as numeric(9),
@numSiteId  as  numeric(9) 
AS    
BEGIN    
	IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
	BEGIN
		SELECT intPaymentGateWay FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID
	END
	ELSE
	BEGIN
		SELECT intPaymentGateWay FROM Domain WHERE numDomainId=@numDomainID
	END

	SELECT 
		PG.intPaymentGateWay
		,vcGateWayName
		,vcFirstFldName
		,vcSecndFldName
		,vcThirdFldName
		,vcFirstFldValue
		,vcSecndFldValue
		,vcThirdFldValue
		,isnull(bitTest,0) bitTest
		,isnull(vcToolTip,'') vcToolTip  
	FROM 
		PaymentGateway PG    
	LEFT JOIN 
		PaymentGatewayDTLID PGDTLID    
	ON 
		PG.intPaymentGateWay=PGDTLID.intPaymentGateWay 
		AND numDomainID=@numDomainID 
		AND PGDTLID.numSiteId=@numSiteId
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadList1]    Script Date: 09/08/2010 14:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Created By Anoop Jayaraj                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist1')
DROP PROCEDURE usp_getleadlist1
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList1]                                                                          
 @numGrpID numeric,                                                                              
 @numUserCntID numeric,
 @tintUserRightType tinyint,
 @CustName varchar(100)='',                                                                              
 @FirstName varChar(100)= '',                                                                              
 @LastName varchar(100)='',                                                                            
 @SortChar char(1)='0' ,                                                                            
 @numFollowUpStatus as numeric(9)=0 ,                                                                            
 @CurrentPage int,                                                                            
 @PageSize int,                                                                            
 @columnName as Varchar(50)='',                                                                            
 @columnSortOrder as Varchar(10)='',                                                    
 @ListType as tinyint=0,                                                    
 @TeamType as tinyint=0,                                                     
 @TeamMemID as numeric(9)=0,                                                
 @numDomainID as numeric(9)=0    ,                  
 @ClientTimeZoneOffset as int  ,              
 @bitPartner as bit,
 @bitActiveInActive as bit,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                                                  
AS       

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
END
ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
END
                                                                             
declare @strSql as varchar(MAX)                                                                             


declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                           
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                            
 
  DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                                                                                           
                                                              
 set @strSql= '
DECLARE @numDomain NUMERIC
SET @numDomain='+ convert(varchar(15),@numDomainID)+';

 with tblLeads as ( SELECT     '         
 set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@columnName)>0 THEN ' CONVERT(VARCHAR(Max),' + @columnName + ')'  WHEN @columnName='numRecOwner' THEN 'DM.numRecOwner' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID    
   left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId ##JOIN##                                           
   
 WHERE                                                                               
   DM.tintCRMType=0  and  (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL)
  AND CMP.numDomainID=DM.numDomainID                                                          
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                                 
 AND DM.numDomainID=@numDomain'               
      
SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive) 
        
if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+               
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '              
                                                                                    
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                            
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                            
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                                           
                                                                                   
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                            
if @tintUserRightType=1 AND @numGrpID<>2 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')' 


if @ListType=0 -- when MySelf radiobutton is checked
begin                                                    
IF @tintUserRightType=1 AND @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                
if @tintUserRightType=1 AND @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                     
end                                          
                                      
else if @ListType=1 -- when Team Selected radiobutton is checked
begin                                 
if @TeamMemID=0 -- When All team members is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+'))' + ' or ' + @strShareRedordWith +')'                                                                       
                                                   
 end                                                  
else-- When All team member name is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) + ' or ' + @strShareRedordWith +')'                                                         
                                                   
 end                                                   
end

IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria

  set @strSql=@strSql+')'
--set @strSql=@strSql+'), AddUnreadMailCount AS 
--             ( SELECT ISNULL(VIE.Total,0) AS TotalEmail,t.* FROM tblLeads t left JOIN View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = @numDomain AND  VIE.numContactId = t.numContactId
--                            )
--             , AddActionCount AS
--             ( SELECT    ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem,U.* FROM AddUnreadMailCount U LEFT JOIN  VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = U.numDivisionID 
--                            '                                  
                                                                           
declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)                  
set @tintOrder=0                                                
set @WhereCondition =''               
                 
declare @Nocolumns as tinyint              
--declare @DefaultNocolumns as tinyint         
set @Nocolumns=0              
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1) TotalRows
 

--Set @DefaultNocolumns=  @Nocolumns
  

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      


--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 34 AND numRelCntType=1 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		34,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,1,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		34,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,1,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
	BEGIN
					INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 34,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=34 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

	END

	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END        
Declare @ListRelID as numeric(9)  
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
 


while @tintOrder>0                                                
begin                                                
     
     if @bitCustomField = 0            
 begin             
     declare @Prefix as varchar(5)                  
     if @vcLookBackTableName = 'AdditionalContactsInformation'                  
   set @Prefix = 'ADC.'                  
     if @vcLookBackTableName = 'DivisionMaster'                  
   set @Prefix = 'DM.'       
   
          SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                                         
 IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strSql=@strSql+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END
			ELSE if @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                                                    
        begin                                                    
     
	IF @vcDbColumnName = 'numPartenerSource'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
	END
	IF @vcDbColumnName = 'numPartenerContact'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerContact) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=DM.numPartenerContact) AS vcPartenerContact' 
	END
      
     if @vcListItemType='LI'                                                     
     begin                                                    
		 IF @numListID=40 
		 BEGIN
			
				SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
				IF @vcDbColumnName='numShipCountry'
				BEGIN
					SET @WhereCondition = @WhereCondition
										+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
										+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
				END
				ELSE
				BEGIN
					SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
				END


		 END
		 ELSE
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                   
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName		 	
		 END
     end                                                    
     else if @vcListItemType='S'                                                     
     begin        
			IF @vcDbColumnName='numShipState'
			BEGIN
				SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
					+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
			END
			ELSE IF @vcDbColumnName='numBillState'
			BEGIN
				SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
					+ ' left Join State BillState on BillState.numStateID=AD4.numState '
			END
			ELSE  
			BEGIN
				SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
			END		                                            
     end                                                    
     else if @vcListItemType='T'                                                     
     begin                                                    
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end  
      else if @vcListItemType='AG'                                                     
     begin                                                    
      set @strSql=@strSql+',AG'+ convert(varchar(3),@tintOrder)+'.vcGrpName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join Groups AG'+ convert(varchar(3),@tintOrder)+ ' on AG'+convert(varchar(3),@tintOrder)+ '.numGrpId=DM.'+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='DC'                                                     
     begin                                                    
      set @strSql=@strSql+',DC'+ convert(varchar(3),@tintOrder)+'.vcECampName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ECampaign DC'+ convert(varchar(3),@tintOrder)+ ' on DC'+convert(varchar(3),@tintOrder)+ '.numECampaignID=ADC.'+@vcDbColumnName                                                    
     end                   
     else if   @vcListItemType='U'                                                 
    begin                   
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
    end                  
    end                                                    
      else if @vcAssociatedControlType='DateField'                                                  
 begin            
       
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
   end    
          
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                 
begin    
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when    
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'              
       
 end 
 else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end       
 else                                               
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
 end          
                
end            
else if @bitCustomField = 1            
begin            
               
     

   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
            
   print @vcAssociatedControlType   
   print @numFieldId           
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin            
               
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end  
   else if @vcAssociatedControlType = 'CheckBox'         
   begin            
               
    --set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
    set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'DateField'        
   begin            
               
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'           
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                     
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end   
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END        
   BEGIN
		SET @strSql = @strSql + CONCAT(',dbo.GetCustFldValue(',@numFieldId,',1,DM.numDivisionID)') + ' [' + @vcColumnName + ']'
	END                 
end      
            
    
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
        
    
          
end                     
 
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=34 AND DFCS.numFormID=34 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  set @strSql=@strSql+' ,TotalRowCount
 ,ISNULL(ADC.numECampaignID,0) as numECampaignID
 ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
 ,ISNULL(VIE.Total,0) as TotalEmail,
 ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
   left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId                                               
   join tblLeads T on T.numDivisionID=DM.numDivisionID   '+@WhereCondition


SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
		--set @strSql=@strSql+' left join #tempColorScheme tCS on convert(varchar(11),DateAdd(day,tCS.vcFieldValue,GETUTCDATE()))=' + @Prefix + 'convert(varchar(11),DateAdd(day,tCS.vcFieldValue,' + @vcCSOrigDbCOlumnName + '))'
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111) 
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

 set @strSql=@strSql+' WHERE                                               
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1 AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'
                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0'                                                                 
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartenerSource' 
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

            
	IF @columnName like 'CFW.Cust%'             
	BEGIN
		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'   
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOpportunityMasterTaxItems' ) 
    DROP PROCEDURE USP_GetOpportunityMasterTaxItems
GO

CREATE PROCEDURE [dbo].[USP_GetOpportunityMasterTaxItems]
    @numOppId NUMERIC(18, 0) =0,
    @numOppItemID NUMERIC(18, 0) =0,
    @tintMode AS TINYINT 
AS 
BEGIN			
	IF @tintMode=1
	BEGIN
		SELECT * FROM OpportunityMasterTaxItems WHERE ISNULL(numOppId,0)=@numOppId 
	END
	ELSE IF @tintMode=2
	BEGIN
		SELECT 
			OITI.* 
			,OMTI.fltPercentage
			,OMTI.tintTaxType
		FROM 
			OpportunityItemsTaxItems OITI
		INNER JOIN
			OpportunityMasterTaxItems OMTI
		ON
			OMTI.numOppID=@numOppId
			AND OITI.numTaxItemID=OMTI.numTaxItemID
			AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
		WHERE 
			ISNULL(OITI.numOppId,0)=@numOppId 
			AND ISNULL(numOppItemID,0)=@numOppItemID
	END
END
   
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                              
--- Modified By Gangadhar 03/05/2008    
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getprospectslist1' ) 
    DROP PROCEDURE usp_getprospectslist1
GO
CREATE PROCEDURE [dbo].[USP_GetProspectsList1]
    @CRMType NUMERIC,
    @numUserCntID NUMERIC,
    @tintUserRightType TINYINT,
    @tintSortOrder NUMERIC = 4,
    @numDomainID AS NUMERIC(9) = 0,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numProfile AS NUMERIC,
    @bitPartner AS BIT,
    @ClientTimeZoneOffset AS INT,
	@bitActiveInActive as bit,
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='' ,
	@SearchText VARCHAR(100) = '',
	 @TotRecs int output 
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)
    
	DECLARE @Nocolumns AS TINYINT = 0
    
	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3
	) TotalRows


	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 35 AND numRelCntType=3 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			35,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,3,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			35,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,3,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
			select 35,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,3,1,0,intColumnWidth
			 FROM View_DynamicDefaultColumns
			 where numFormId=35 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
			order by tintOrder asc   
		END    
       
		INSERT INTO 
			#tempForm
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
        WHERE 
			numFormId = 35
			AND numUserCntID = @numUserCntID
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND ISNULL(bitSettingField, 0) = 1
			AND ISNULL(bitCustom, 0) = 0
        UNION
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormId = 35
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND numRelCntType = 3
            AND ISNULL(bitCustom, 0) = 1
        ORDER BY 
			tintOrder ASC  
	END  

		

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=''

	 SET @strColumns =' ISNULL(ADC.numContactId,0) numContactId, ISNULL(DM.numDivisionID,0) numDivisionID, ISNULL(DM.numTerID,0)numTerID, ISNULL(ADC.numRecOwner,0) numRecOwner, ISNULL(DM.tintCRMType,0) AS tintCRMType, CMP.vcCompanyName '              

	DECLARE @tintOrder AS TINYINT                                                      
    DECLARE @vcFieldName AS VARCHAR(50)                                                      
    DECLARE @vcListItemType AS VARCHAR(3)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
    DECLARE @numListID AS NUMERIC(9)                                                      
    DECLARE @vcDbColumnName VARCHAR(20)                          
    DECLARE @WhereCondition VARCHAR(MAX)                           
    DECLARE @vcLookBackTableName VARCHAR(2000)                    
    DECLARE @bitCustom AS BIT
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)              
    DECLARE @vcColumnName AS VARCHAR(500)  
    SET @tintOrder = 0                                                      
    SET @WhereCondition = ''
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
    FROM    #tempForm
    ORDER BY tintOrder ASC            
 
                                               
    WHILE @tintOrder > 0  
    BEGIN
            IF @bitCustom = 0 
                BEGIN            
                    DECLARE @Prefix AS VARCHAR(5)                        
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'                        
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'  

					SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
					BEGIN
						SET @WhereCondition = @WhereCondition
														+ CONCAT(' OUTER APPLY(SELECT
																			SUM(monDealAmount) AS monDealAmount
																			,AVG(ProfitPer) AS fltProfitPer
																		FROM
																		(
																			SELECT
																				OM.numDivisionId
																				,monDealAmount
																				,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																			FROM
																				OpportunityMaster OM
																			INNER JOIN
																				Domain D
																			ON
																				OM.numDomainID = D.numDomainID
																			INNER JOIN
																				OpportunityItems OI
																			ON
																				OM.numOppId = OI.numOppId
																			INNER JOIN
																				Item I
																			ON
																				OI.numItemCode = I.numItemCode
																			LEFT JOIN
																				Vendor V
																			ON
																				V.numVendorID = I.numVendorID
																				AND V.numItemCode = I.numItemCode
																			WHERE
																				OM.numDomainId = ',@numDomainID,'
																				AND OM.numDivisionId = DM.numDivisionID
																				AND ISNULL(I.bitContainer,0) = 0
																				AND tintOppType = 1
																				AND tintOppStatus=1
																				AND 1 = (CASE ', @tintPerformanceFilter,' 
																						WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						ELSE 1
																						END)
																			GROUP BY
																				OM.numDivisionId
																				,OM.numOppId
																				,monDealAmount
																		) T1
																		GROUP BY
																			T1.numDivisionId ) AS TempPerformance ')

						set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
					END
					ELSE IF @vcDbColumnName='bitEcommerceAccess'
					BEGIN
						SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
					END
                    ELSE IF @vcAssociatedControlType = 'SelectBox' or @vcAssociatedControlType='ListBox'     
                        BEGIN                                                          
                            IF @vcDbColumnName = 'numPartenerSource'
							BEGIN
								SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
								SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
							END
							ELSE IF @vcListItemType = 'LI' 
                                BEGIN    
                                    IF @numListID = 40 
                                        BEGIN
                                            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipCountry'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
											END
											ELSE
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
											END
                                        END
                                    ELSE 
                                        BEGIN                                                      
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											                                              
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                END                                                          
                            ELSE 
                                IF @vcListItemType = 'S' 
                                    BEGIN     
											
											                                                           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State BillState on BillState.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END
                                                 
                                                                                          
                                    END                                                          
                                ELSE 
                                    IF @vcListItemType = 'T' 
                                        BEGIN                                                          
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'     
												
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END  
											                                                     
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'C' 
                                            BEGIN                                                    
                                                SET @strColumns = @strColumns + ',C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcCampaignName' + ' ['
                                                    + @vcColumnName + ']'    
												
												IF LEN(@SearchText) > 0
												BEGIN 
												SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
												END  
												                                                
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left Join CampaignMaster C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numCampaignId=DM.'
                                                    + @vcDbColumnName                                                    
                                            END                       
                                        ELSE 
                                            IF @vcListItemType = 'U' 
                                                BEGIN                         
                            
                              
                                                    SET @strColumns = @strColumns
                                                        + ',dbo.fn_GetContactName('
                                                        + @Prefix
                                                        + @vcDbColumnName
                                                        + ') ['
                                                        + @vcColumnName + ']'  
														
													IF LEN(@SearchText) > 0
													  BEGIN 
														SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
													  END       
													                                                        
                                                END                        
                        END       
      
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN              
                                SET @strColumns = @strColumns
                                    + ',case when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
                                SET @strColumns = @strColumns
                                    + 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName + '),'
                                    + CONVERT(VARCHAR(10), @numDomainId)
                                    + ') end  [' + @vcColumnName + ']'    
									
								
								IF LEN(@SearchText) > 0
								BEGIN
									SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
								END
								
								              
                            END      
            
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox'
                                OR @vcAssociatedControlType = 'Label' 
                                BEGIN      
                                    SET @strColumns = @strColumns + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'    
										  
										  
									IF LEN(@SearchText) > 0
									BEGIN
										SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
										CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END
										+' LIKE ''%' + @SearchText + '%'' '
									END  		  
										              
         
                                END 
                          else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end         
                            ELSE 
                                BEGIN    
                                    SET @strColumns = @strColumns + ', '
                                        + @vcDbColumnName + ' ['
                                        + @vcColumnName + ']'              
       
                                END                                    
                   
                END                  
            ELSE 
                IF @bitCustom = 1 
                    BEGIN                  
      
						SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
 
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                                 
                        IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'   
                            BEGIN                              
                                SET @strColumns = @strColumns + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcColumnName + ']'                     
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=DM.numDivisionId   '                                                           
                            END    
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox'  
                                BEGIN              
set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'        
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=DM.numDivisionId   '                                                     
                                END       
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField'  
                                    BEGIN                  
                     
                                        SET @strColumns = @strColumns
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcColumnName + ']'                     
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=DM.numDivisionId   '                                                           
                                    END                  
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox'
                                        BEGIN                   
                                            SET @vcDbColumnName = 'DCust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                  
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'                                                            
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                   
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=DM.numDivisionId    '                                                           
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                  
                                        END      
								ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
								BEGIN
									SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

								SET @WhereCondition= @WhereCondition 
													+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
													+ ' on CFW' + convert(varchar(3),@tintOrder) 
													+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
													+ 'and CFW' + convert(varchar(3),@tintOrder) 
													+ '.RecId=DM.numDivisionId '
								END                       
                    END    
                    

		  select top 1 
		   @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
		  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc
            IF @@rowcount = 0 
                SET @tintOrder = 0 
    END  

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   

		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql='	FROM  CompanyInfo CMP                                       
						join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
						left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
						left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID =  '+ convert(varchar(15),@numDomainID )+' AND  VOA.numDivisionId = DM.numDivisionID 
						left join View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = '+ convert(varchar(15),@numDomainID )+' AND  VIE.numContactId = ADC.numContactId '
		IF @tintSortOrder = 9
		BEGIN
			SET @StrSql=@StrSql+ ' join Favorites F on F.numContactid=DM.numDivisionID '
		END 

		SET @StrSql=@StrSql + ISNULL(@WhereCondition,'')

		 -------Change Row Color-------
		Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
		SET @vcCSOrigDbCOlumnName=''
		SET @vcCSLookBackTableName=''

		Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

		insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
		from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
		join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
		where DFCS.numDomainID=@numDomainID and DFFM.numFormID=35 AND DFCS.numFormID=35 and isnull(DFFM.bitAllowGridColor,0)=1

		IF(SELECT COUNT(*) FROM #tempColorScheme)>0
		BEGIN
		   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
		END   
		----------------------------                        
 
		 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
		set @strColumns=@strColumns+' ,tCS.vcColorScheme '
		END

		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
				set @Prefix = 'ADC.'                  
			 if @vcCSLookBackTableName = 'DivisionMaster'                  
				set @Prefix = 'DM.'
			 if @vcCSLookBackTableName = 'CompanyInfo'                  
				set @Prefix = 'CMP.'   
 
			IF @vcCSAssociatedControlType='DateField'
			BEGIN
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
			END
			ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
			BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
			END
		END

		IF @columnName LIKE 'CFW.Cust%' 
        BEGIN                   
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                           
            SET @columnName = 'CFW.Fld_Value'                  
        END                                           
		ELSE IF @columnName LIKE 'DCust%' 
        BEGIN                                                      
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @columnName = 'LstCF.vcData' 
        END        

		IF @columnName = 'CMP.vcPerformance'   
		BEGIN
			SET @columnName = 'TempPerformance.monDealAmount'
		END

		SET @StrSql = @StrSql + ' WHERE (numCompanyType = 0 OR numCompanyType=46) 
									AND (ISNULL(ADC.bitPrimaryContact,0) = 1 OR ADC.numContactID IS NULL)
									AND CMP.numDomainID=DM.numDomainID   
									AND DM.tintCRMType= ' + CONVERT(VARCHAR(2), @CRMType) + ' AND DM.numDomainID = ' + CONVERT(VARCHAR(20),@numDomainID)


		IF @tintUserRightType = 1 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
		END
		ELSE IF @tintUserRightType = 2 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
		END	

		IF @SortChar <> '0' 
		BEGIN
			SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'' '                                                               
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                     
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
					SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END
		ELSE
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                   
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
        SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
			BEGIN
				-- WE ARE MANAGING CONDITION IN OUTER APPLY
				set @strSql=@strSql
			END
			ELSE
				set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
		END


		IF @vcCustomSearchCriteria <> '' 
			SET @strSql=@strSql + ' AND ' + @vcCustomSearchCriteria
                                            


		
		DECLARE @firstRec AS INTEGER                                                              
		DECLARE @lastRec AS INTEGER                                                              
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )     
		
		DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
		exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT                                                                   
       
		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #tempColorScheme 
		 
END	
--Created By Anoop Jayaraj    
    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxAmountOppForTaxItem')
DROP PROCEDURE USP_GetTaxAmountOppForTaxItem
GO
CREATE PROCEDURE [dbo].[USP_GetTaxAmountOppForTaxItem]    
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9),    
@numDivisionID as numeric(9),
@numOppBizDocID as numeric(9),
@tintMode AS TINYINT=0    
AS    
IF @tintMode=0 --Order
BEGIN
	SELECT ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocID),0)
END
ELSE IF @tintMode=5 OR @tintMode=7 OR @tintMode=8 OR @tintMode=9 --RMA Return
BEGIN
	DECLARE @fltPercentage FLOAT = 0
	DECLARE @TotalTaxAmt AS MONEY = 0    
	DECLARE @numUnitHour AS NUMERIC(18,0) = 0
	DECLARE @ItemAmount AS MONEY

	SELECT 
		@TotalTaxAmt = SUM(ISNULL(CASE 
					WHEN OMTI.tintTaxType = 2 
					THEN 
						ISNULL(OMTI.fltPercentage,0) * ISNULL((CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(RI.numUOMId, 0)),0)
					ELSE 
						(ISNULL(OMTI.fltPercentage,0) * (CASE WHEN @tintMode=5 OR @tintMode=6 THEN ISNULL(numUnitHour,0) ELSE ISNULL(numUnitHourReceived,0) END) * (CASE
			WHEN ISNULL(RI.fltDiscount,0) > 0 THEN 
				CASE WHEN RI.bitDiscountType = 1 THEN monPrice - (RI.fltDiscount / RI.numUnitHour) ELSE	monPrice - (monPrice * (RI.fltDiscount / 100)) END
			ELSE 
				monPrice
		END)) / 100 END,0))
	FROM 
		ReturnItems RI
	JOIN
		Item I
	ON
		RI.numItemCode = I.numItemCode
	JOIN
		OpportunityItemsTaxItems OITI
	ON
		RI.numReturnItemID = OITI.numReturnItemID
		AND RI.numReturnHeaderID = OITI.numReturnHeaderID
	JOIN
		OpportunityMasterTaxItems OMTI
	ON
		OITI.numReturnHeaderID = OMTI.numReturnHeaderID
		AND OITI.numTaxItemID = OMTI.numTaxItemID
		AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
	WHERE 
		RI.numReturnHeaderID=@numOppID   
		AND OITI.numTaxItemID=@numTaxItemID
		 
	SELECT ISNULL(@TotalTaxAmt,0) AS TotalTaxAmt				 
END  
    

GO
/****** Object:  StoredProcedure [dbo].[USP_GetUSAEpayDtls]    Script Date: 05/07/2009 21:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusaepaydtls')
DROP PROCEDURE usp_getusaepaydtls
GO
CREATE PROCEDURE [dbo].[USP_GetUSAEpayDtls]
@numDomainID as numeric(9),
@intPaymentGateway AS INT=0,
@numSiteId as int
as  
BEGIN 
	IF @intPaymentGateway =0 
	BEGIN
		IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
		BEGIN
			SELECT 
				S.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Sites S
			INNER JOIN
				Domain D
			ON
				S.numDomainID = D.numDomainId
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = S.intPaymentGateWay
			WHERE 
				S.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=0
		END
		ELSE
		BEGIN
			SELECT 
				D.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Domain D
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = D.intPaymentGateWay
			WHERE 
				D.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=0
		END
	
	END
	ELSE IF @intPaymentGateway >0 
	BEGIN
	SELECT intPaymentGateWay,
		isnull(vcFirstFldValue,'') AS vcFirstFldValue,
		isnull(vcSecndFldValue,'') AS vcSecndFldValue,
		isnull(vcThirdFldValue,'') AS vcThirdFldValue,
		isnull(bitTest,0) AS bitTest
	FROM   PaymentGatewayDTLID PD
	WHERE  
		PD.numDomainID = @numDomainID
		AND PD.intPaymentGateWay = @intPaymentGateway
		AND PD.numSiteId=@numSiteId
	END
END
GO	   

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice MONEY
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost MONEY
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,@numIncomeChartAcntId
			,@numAssetChartAcntId
			,@numCOGsChartAcntId
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,ISNULL((SELECT numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0)
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc) VALUES (@numItemCode,@txtDesc)

		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=ISNULL((SELECT numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0)
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc) VALUES (@numItemCode,@txtDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0
as                    
declare @Identity as numeric(9)                
declare @FromName as varchar(1000)                
declare @FromEmailAdd as varchar(1000)                
declare @startPos as integer                
declare @EndPos as integer                
declare @EndPos1 as integer                
                
  set @startPos=CHARINDEX( '<', @vcMessageFrom)                
  set @EndPos=CHARINDEX( '>', @vcMessageFrom)                
  if @startPos>0                 
  begin                
   set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
   set @startPos=CHARINDEX( '"', @vcMessageFrom)                
   set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
   set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                
                   
                   
  end                 
  else                 
  begin                
   set @FromEmailAdd=@vcMessageFrom                
   set @FromName=@vcMessageFromName                
  end                
declare @updateinsert as bit     
    
if @vcItemId = ''    
begin    
 set @updateinsert  = 1    
end    
else    
begin    
 if (select count(*) from emailhistory  where vcItemId =@vcItemId) > 0    
  begin    
   set @updateinsert  = 0    
  end    
 else    
  begin    
   set @updateinsert  = 1    
  end    
end    
                  
if @updateinsert =0    
begin              
update EmailHistory            
set            
vcSubject=@vcSubject,            
vcBody=@vcBody,      
vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
bintCreatedOn=getutcdate(),            
bitHasAttachments=@bitHasAttachment,            
vcItemId=@vcItemId,            
vcChangeKey=@vcChangeKey,            
bitIsRead=@bitIsRead,            
vcSize=convert(varchar(200),@vcSize),            
chrSource=@chrSource,            
tinttype=@tinttype,            
vcCategory =@vcCategory ,        
dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
where vcItemId = @vcItemId   and numDomainid= @numDomainId      
              
end                
else             
begin            
declare @node as NUMERIC(18,0)
set @node = case when @vcCategory = 'B' then ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4),0) else 0 end 

SET @vcMessageTo = @vcMessageTo + '#^#'
SET @vcMessageFrom = @vcMessageFrom + '#^#'
SET @vcCC = @vcCC + '#^#'
SET @vcBCC = @vcBCC + '#^#'

EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

--IF @vcCategory = 'B'
--	SELECT @vcSize =DATALENGTH(@vcBody)
            
Insert into EmailHistory(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
values                          
(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@node,dbo.fn_GetNewvcEmailString(@vcMessageFrom,@numDomainId),dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId),dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId),dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId),@IsReplied,@bitInvisible)              
set @Identity=SCOPE_IDENTITY()                 

update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$'))
				where [numEmailHstrID]=@Identity                

update EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID
                
--exec USP_InsertEmailAddCCAndBCC  @vcMessageTo,1,@Identity , @vcMessageToName               
--exec USP_InsertEmailAddCCAndBCC  @vcBCC,2,@Identity ,''               
--exec USP_InsertEmailAddCCAndBCC  @vcCC,3,@Identity  ,@vcCCName          
--exec USP_InsertEmailAddCCAndBCC  @vcMessageFrom,4,@Identity,  @vcMessageFromName      
  
  --No longer being used .. We are calling same function from front end to add attachment to email.. -- by chintan       
--if @bitHasAttachment  = 1          
--begin          
--exec USP_InsertAttachment   @vcFileName,@vcAttachmentItemId,@vcAttachmentType,@Identity           
--end       

SELECT ISNULL(@Identity,0)
end
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemCategory_MassUpdate')
DROP PROCEDURE USP_ItemCategory_MassUpdate
GO
CREATE PROCEDURE [dbo].[USP_ItemCategory_MassUpdate]     
	@numDomainID NUMERIC(9)
	,@tintType TINYINT
	,@numCategoryProfileID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX)
	,@vcCategories VARCHAR(MAX)
AS
BEGIN
	IF ISNULL(@vcCategories,'') <> '' AND ISNULL(@vcItemCodes,'') <> ''
	BEGIN
		IF @tintType = 1 -- ADD
		BEGIN
			DECLARE @TEMP TABLE
			(
				numItemCode NUMERIC(18,0)
				,numCategoryID NUMERIC(18,0)
			)
			INSERT INTO
				@TEMP
			SELECT
				t1.ID
				,t2.ID
			FROM
				(SELECT ISNULL(ID,0) AS ID FROM dbo.SplitIDs(@vcItemCodes,',')) t1
			CROSS APPLY
				(SELECT ISNULL(ID,0) AS ID FROM dbo.SplitIDs(@vcCategories,',')) t2

			INSERT INTO ItemCategory
			(
				numCategoryID
				,numItemID
			)
			SELECT
				t1.numCategoryID
				,t1.numItemCode
			FROM	
				@TEMP t1
			LEFT JOIN
				ItemCategory
			ON
				t1.numCategoryID = ItemCategory.numCategoryID
				AND t1.numItemCode = ItemCategory.numItemID
			WHERE
				t1.numCategoryID IN (SELECT ISNULL(numCategoryID,0) FROM Category WHERE numDomainID=@numDomainID AND numCategoryProfileID=@numCategoryProfileID)
				AND ItemCategory.numCategoryID IS NULL
		END
		ELSE IF @tintType = 2 -- REMOVE
		BEGIN
			DELETE 
				IC
			FROM 
				dbo.ItemCategory IC
			INNER JOIN 
				Category 
			ON 
				IC.numCategoryID = Category.numCategoryID
			WHERE 
				numItemID IN (SELECT ISNULL(ID,0) from dbo.SplitIDs(@vcItemCodes,','))
				AND IC.numCategoryID IN (SELECT ISNULL(ID,0) from dbo.SplitIDs(@vcCategories,','))		
				AND numCategoryProfileID = @numCategoryProfileID
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)=''
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'OnHand' OR @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = ''
AS   


	IF ISNULL(@numContactType,0) = 0
	BEGIN
		SET @numContactType = 70
	END

	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID                
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                                    
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_ManageGatewayDTLs]    Script Date: 07/26/2008 16:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managegatewaydtls')
DROP PROCEDURE usp_managegatewaydtls
GO
CREATE PROCEDURE [dbo].[USP_ManageGatewayDTLs]  
@numDomainID as numeric(9), 
@str as text,
@numSiteId as int,
@numPaymentGatewayID NUMERIC(18,0)
as  

	IF ISNULL(@numSiteId,0) > 0
	BEGIN
		UPDATE Sites SET intPaymentGateWay=@numPaymentGatewayID WHERE numSiteID=@numSiteId AND numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		UPDATE Domain SET intPaymentGateWay=@numPaymentGatewayID WHERE numDomainID=@numDomainID
	END
  
delete from PaymentGatewayDTLID where ISNULL(numSiteId,0)=ISNULL(@numSiteId,0) AND numDomainID=@numDomainID
 DECLARE @hDocItem int  
  
 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str                     
   insert into                     
   PaymentGatewayDTLID                                                                        
   (intPaymentGateWay,vcFirstFldValue,vcSecndFldValue,vcThirdFldValue,numDomainID,bitTest,numSiteId)                    
   select X.intPaymentGateWay,X.vcFirstFldValue,x.vcSecndFldValue,x.vcThirdFldValue,@numDomainID,X.bitTest,@numSiteId from(                                                                        
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                                        
   WITH                     
   (                                                                        
   intPaymentGateWay numeric(9),                                   
   vcFirstFldValue varchar(1000),                                                                        
   vcSecndFldValue varchar(1000),
   vcThirdFldValue varchar(1000),
   bitTest bit          
   ))X  
  
  
  EXEC sp_xml_removedocument @hDocItem
GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
BEGIN 
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END)
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId

            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1
        END
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,NULL,1,@numReturnHeaderID)	
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,
				bitDiscountType,fltDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,
				bitDiscountType,
				fltDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0
					 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1
			END 
		END
    END            
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0	) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as      

Declare @ParentBizDoc AS NUMERIC(9)=0
DECLARE @lngJournalId AS NUMERIC(18,0)

SET @ParentBizDoc=(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numSourceBizDocId= @numOppBizDocsId)     
IF @ParentBizDoc>0
BEGIN	
	IF @byteMode = 1
	BEGIN
		SET @lngJournalId=(Select TOP 1 GJH.numJournal_Id as numJournalId from General_Journal_Header GJH Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@ParentBizDoc)
		IF(@lngJournalId>0)
		BEGIN
			exec USP_DeleteJournalDetails @numDomainID=@numDomainID,@numJournalID=@lngJournalId,@numBillPaymentID=0,@numDepositID=0,@numCheckHeaderID=0,@numBillID=0,@numCategoryHDRID=0,@numReturnID=0,@numUserCntID=0
		END
		exec DeleteComissionDetails @numDomainId=@numDomainID,@numOppBizDocID=@ParentBizDoc       
	END
	exec USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
END      
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint
DECLARE @tintShipped AS BIT
DECLARE @bitAuthoritativeBizDoc AS BIT

SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
BEGIN
	RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
	RETURN
END

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X         
                        
END


GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetOppAddressDetails')
DROP PROCEDURE USP_OPPGetOppAddressDetails
GO
CREATE PROCEDURE [dbo].[USP_OPPGetOppAddressDetails]
(
    @numOppId        AS NUMERIC(9)  = NULL,
    @numDomainID     AS NUMERIC(9)  = 0
   )
AS

  DECLARE  @tintOppType  AS TINYINT, @tintBillType  AS TINYINT, @tintShipType  AS TINYINT
 
  DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC,@numContactID AS NUMERIC, @numBillToAddressID NUMERIC(18,0), @numShipToAddressID NUMERIC(18,0)
      
  SELECT  @tintOppType = tintOppType,
		  @tintBillType = tintBillToType,
		  @tintShipType = tintShipToType,
          @numDivisionID = numDivisionID,
          @numContactID = numContactID,
		  @numBillToAddressID = ISNULL(numBillToAddressID,0),
		  @numShipToAddressID = ISNULL(numShipToAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId


	 IF @tintOppType=2
		BEGIN
			--************Vendor Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcBillStreet,'') + '</pre>' + isnull(AD.vcBillCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numBillState),'') + ' ' + isnull(AD.vcBillPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcFullAddress,
					isnull(AD.vcBillStreet,'') AS vcStreet,
					isnull(AD.vcBillCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numBillState),'') AS vcState,
					isnull(AD.vcBillPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcBillCompanyName) > 0 THEN AD.vcBillCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numBillCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			END

			--************Vendor Shipping Address************
			IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcShipStreet,'') + '</pre>' + isnull(AD.vcShipCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numShipState),'') + ' ' + isnull(AD.vcShipPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcFullAddress,
					isnull(AD.vcShipStreet,'') AS vcStreet,
					isnull(AD.vcShipCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numShipState),'') AS vcState,
					isnull(AD.vcShipPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcShipCompanyName) > 0 THEN AD.vcShipCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numShipCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			
			 --************Employer Billing Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' +isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			 
			 
			 --************Employer Shipping Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
	  END

	IF @tintOppType=1 
		BEGIN
			--************Customer Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			END
		
			--************Customer Shipping Address************
			IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
	
			--************Employer Billing Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	  END
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(MAX)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               

SET @strSQL = ''

DECLARE @avgCost int
SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS MONEY) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour FLOAT,
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@tintOppStatus TINYINT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                                     

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			-- GET ACCOUNT CLASS IF ENABLED
			DECLARE @numAccountClass AS NUMERIC(18) = 0

			DECLARE @tintDefaultClassType AS INT = 0
			SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

			IF @tintDefaultClassType = 1 --USER
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numDefaultClass,0) 
				FROM 
					dbo.UserMaster UM 
				JOIN 
					dbo.Domain D 
				ON 
					UM.numDomainID=D.numDomainId
				WHERE 
					D.numDomainId=@numDomainId 
					AND UM.numUserDetailId=@numUserCntID
			END
			ELSE IF @tintDefaultClassType = 2 --COMPANY
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numAccountClassID,0) 
				FROM 
					dbo.DivisionMaster DM 
				WHERE 
					DM.numDomainId=@numDomainId 
					AND DM.numDivisionID=@numDivisionID
			END
			ELSE
			BEGIN
				SET @numAccountClass = 0
			END
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,@tintOppStatus,
				@numOrderStatus,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=Item.numItemCode),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					0,
					NULL,'', (SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
				) AS TEMPTax
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numOppId
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)	
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			DECLARE @DealStatus AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND @tintOppStatus = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND @tintOppStatus = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND @tintOppStatus = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID,0
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0,0
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
				dbo.OpportunityItems OI 
			INNER JOIN
				ItemTax IT
			ON
				IT.numItemCode = OI.numItemCode
			INNER JOIN
				TaxDetails TD
			ON
				TD.numTaxID = IT.numTaxID
				AND TD.numDomainId = @numDomainId
			WHERE
				OI.numOppId = @numOppID
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSODropshipItems')
DROP PROCEDURE USP_OpportunityMaster_GetSODropshipItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSODropshipItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @numDivisionID AS NUMERIC(18,0)
	DECLARE @tintDefaultCost AS NUMERIC(18,0)

	SELECT @tintDefaultCost = ISNULL(numCost,0) FROM Domain WHERE numDomainId = @numDomainID
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID

	SELECT
		OI.numItemCode
		,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
		,OI.vcNotes
		,I.charItemType
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
		,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
		,OI.numUnitHour
		,ISNULL(OI.numUOMId,0) AS numUOMID
		,ISNULL(U.vcUnitName, '-') vcUOMName
		,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
		,ISNULL(I.numVendorID, 0) AS numVendorID
		,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) WHEN ISNULL(@tintDefaultCost,0) = 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END AS monCost
		,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		,ISNULL(V.intMinQty, 0) AS intMinQty
		,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
	FROM    
		dbo.OpportunityItems OI
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
		INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
		LEFT JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
		LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
		LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
	WHERE   
		OI.numOppId = @numOppID
		AND OM.numDomainId = @numDomainId
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(OI.bitDropShip, 0) = 1
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


	EXEC usp_getoppaddress @numOppID,1,0

	SELECT dbo.fn_getOPPAddress(@numOppID,@numDomainId,2) AS ShippingAdderss
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectList1')
DROP PROCEDURE USP_ProjectList1
GO
CREATE PROCEDURE [dbo].[USP_ProjectList1]                                                                
			 @numUserCntID numeric(9)=0,                                              
			 @numDomainID numeric(9)=0,                                              
			 @tintUserRightType tinyint=0,                                              
			 @tintSortOrder tinyint=4,                                                    
			 @SortChar char(1)='0',                                                                                           
			 @CurrentPage int,                                              
			 @PageSize int,                                              
			 @TotRecs int output,                                              
			 @columnName as Varchar(50),                                              
			 @columnSortOrder as Varchar(10),                                              
			 @numDivisionID as numeric(9) ,                                             
			 @bitPartner as bit=0,          
			 @ClientTimeZoneOffset as int,
			 @numProjectStatus as numeric(9),
			 @vcRegularSearchCriteria varchar(MAX)='',
			 @vcCustomSearchCriteria varchar(MAX)='' ,
			 @SearchText VARCHAR(100) = ''                                        
as                                              
     BEGIN TRY       
	CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
	vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
	numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
	bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
                
	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0		             
	SET @Nocolumns=0                  
	SELECT 
		@Nocolumns=isnull(sum(TotalRow),0)  
	FROM
	(            
		SELECT 
			count(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=13 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=13 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		) TotalRows
		
	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 13 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=13 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=13 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 13,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
  where numFormId=13 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID                
  order by tintOrder asc 

END
             
	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=13 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
      select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
 from View_DynamicCustomColumns
 where numFormId=13 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END 	

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=' addCon.numContactId,Pro.vcProjectName,Div.numDivisionID,isnull(Div.numTerID,0) as numTerID,pro.numCustPrjMgr,Div.tintCRMType,pro.numProId,Pro.numRecOwner'
	
	declare @tintOrder as tinyint                                                    
	declare @vcFieldName as varchar(50)                                                    
	declare @vcListItemType as varchar(3)                                               
	declare @vcListItemType1 as varchar(1)                                                   
	declare @vcAssociatedControlType varchar(20)                                                    
	declare @numListID AS numeric(9)                                                    
	declare @vcDbColumnName varchar(20)                        
                      
	declare @vcLookBackTableName varchar(2000)                  
	Declare @bitCustom as bit  
	DECLARE @bitAllowEdit AS CHAR(1)                 
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1) 
	declare @vcColumnName AS VARCHAR(500)    
                  
	set @tintOrder=0                         
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
		@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder asc  
	Declare @WhereCondition varchar(MAX)  =''
	while @tintOrder>0                                                    
	BEGIN         
     PRINT @vcDbColumnName
	 PRINT @vcListItemType   
	 PRINT @vcAssociatedControlType                                       
	IF @bitCustom = 0          
	BEGIN          
--print @vcAssociatedControlType        
--print @vcFieldName  --print @vcDbColumnName        
  declare @Prefix as varchar(5)                  
      if @vcLookBackTableName = 'AdditionalContactsInformation'                  
    set @Prefix = 'ADC.'                  
      if @vcLookBackTableName = 'DivisionMaster'                  
    set @Prefix = 'DM.'                  
      if @vcLookBackTableName = 'ProjectsMaster'                  
    set @PreFix ='Pro.'        
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

    IF @vcAssociatedControlType='SelectBox'                                                    
    BEGIN      
		                                                           
		IF @vcListItemType='LI'                                                     
		BEGIN                                                    
		  SET @strColumns= @strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
		  
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
		  END   
		                                          
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
			PRINT @WhereCondition   
		END                                                    
		ELSE IF @vcListItemType='S'                                                     
		BEGIN                                                    
		  SET @strColumns=@strColumns + ',S' + convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'     
		    
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
		  END                
		                                  
		  SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                    
		END   
		ELSE IF @vcListItemType='PP'
		BEGIN                                                    
		  SET @strColumns=@strColumns + ',ISNULL(PP.intTotalProgress,0)'+' ['+ @vcColumnName+']'   
		  
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'' '
		  END
		END   
		ELSE IF @vcListItemType='T'                                                     
		BEGIN        
		  SET @strColumns=@strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'  
		                
		  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '
		  END                                       
		                                           
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
		END                   
		ELSE IF   @vcListItemType='U'                                                 
		BEGIN                     
			SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'  
		  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '
		  END                                                  
		END                  
	END             
    
	ELSE IF @vcAssociatedControlType='DateField'                                                    
	BEGIN             
	IF @vcDbColumnName='intDueDate'
	BEGIN	
		set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+' )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
		set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
		set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
		set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(' +@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
		
		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END
	END
	ELSE
	BEGIN
        SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''    
		SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''    
		SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '    
		SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'             
		
		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END

	END  
      
	END            
    ELSE IF @vcAssociatedControlType='TextBox'                                                    
	   BEGIN             
		 
		 SET @strColumns=@strColumns+','+ case                  
		 WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                 
		 WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'                 
		 WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)'                 
		 ELSE @PreFix+@vcDbColumnName end+' ['+@vcColumnName+']'
		 
		 IF LEN(@SearchText) > 0
		 BEGIN
			 SET @SearchQuery = @SearchQuery +  (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (case                  
			 WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                 
			 WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'                 
			 WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)'                 
			 ELSE @PreFix+@vcDbColumnName end) + ' LIKE ''%' + @SearchText + '%'' '
		 END
		               
	   END  
 ELSE IF  @vcAssociatedControlType='TextArea'                                              
 BEGIN  
  SET @strColumns=@strColumns+', '''' ' +' ['+ @vcColumnName+']'            
 END 
 ELSE  IF @vcAssociatedControlType='CheckBox'                                                
 BEGIN      
	SET @strColumns=@strColumns+', isnull('+ @vcDbColumnName +',0) ['+ @vcColumnName+']'         
	IF LEN(@SearchText) > 0
	BEGIN
		SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''            
	END
 END                                               
 ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
 BEGIN      
	 SET @strColumns=@strColumns+',(SELECT SUBSTRING(
					(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
					FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
					JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
					AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'		           
 END       
 END                                              
 ELSE IF @bitCustom = 1        
 BEGIN          
           
--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                           
--   where CFW_Fld_Master.Fld_Id = @numFieldId                                      
      
      SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
    
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
   begin          
             
    set @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
             
   else if @vcAssociatedControlType = 'Checkbox'         
   begin          
             
    set @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'         
   begin           
             
    set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
   else if @vcAssociatedControlType = 'SelectBox'       
   begin        
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)          
    set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                    
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'          
   end
   ELSE
	BEGIN
		SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValuePro(',@numFieldId,',pro.numProID)') + ' [' + @vcColumnName + ']'
	END             
 End          
                                                  
	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
		@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                             
		@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	WHERE 
		tintOrder > @tintOrder-1 
	ORDER BY 
		tintOrder asc            
 
	IF @@rowcount=0 
		SET @tintOrder=0 

	
	         
	END
	

	DECLARE @strShareRedordWith AS VARCHAR(MAX);
	SET @strShareRedordWith=' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=5 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
 
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	
	SET @StrSql = @StrSql + ' FROM ProjectsMaster pro                                                               
                                 LEFT JOIN ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ '                                                              
                                 JOIN additionalContactsinformation addCon on addCon.numContactId=pro.numCustPrjMgr
								 JOIN DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID    
								 JOIN CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID    
								  left join ProjectProgress PP on PP.numProID=pro.numProID ' + ISNULL(@WhereCondition,'')
								

	if @columnName like 'CFW.Cust%'           
	begin                    
		set  @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
		set @columnName='CFW.Fld_Value'                                                            
	end                                   
	if @columnName like 'DCust%'          
	begin                                  
		set  @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                             
		set  @StrSql = @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'
		set @columnName='LstCF.vcData'
	end

	  if @bitPartner=1 
		set @strSql=@strSql+' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''   


 -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=13 AND DFCS.numFormID=13 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------             
    
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strColumns=@strColumns+',tCS.vcColorScheme'
END
                                                                
 --set @strSql=@strSql+' ,TotalRowCount                                                                  
 --  FROM ProjectsMaster pro                          
 -- join additionalContactsinformation ADC                                              
 --  on ADC.numContactId=pro.numCustPrjMgr                                              
 -- join DivisionMaster DM                                               
 --  on DM.numDivisionID=ADC.numDivisionID                                              
 -- join CompanyInfo cmp                                             
 --  on cmp.numCompanyID=DM.numCompanyID                                            
 --    '+@WhereCondition+  '                                
 --join tblProjects T on T.numProID=Pro.numProID'                                                              
  
--' union select count(*),null,null,null,null,null,null,null '+@strColumns+' from tblProjects order by RowNumber'                 
                                                           
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'ProjectsMaster'                  
		set @Prefix = 'pro.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @StrSql=@StrSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @StrSql=@StrSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END


 set @strSql=@strSql+ CONCAT(' where tintProStatus=0 and Div.numDomainID= ',@numDomainID)
                                
                                             
 
set @strSql=@strSql+ 'and (isnull(pro.numProjectStatus,0) = '+ convert(varchar(10),@numProjectStatus) + ' OR (' + convert(varchar(10),@numProjectStatus) +'=0 and isnull(pro.numProjectStatus,0) <> 27492))'
                                       
if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                               
if @SortChar<>'0' set @strSql=@strSql + ' And Pro.vcProjectName like '''+@SortChar+'%'''                                               
if @tintUserRightType=1 set @strSql=@strSql + ' AND (Pro.numRecOwner = '+convert(varchar(15),@numUserCntID)+' or Pro.numAssignedTo = '+convert(varchar(15),@numUserCntID) +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails         
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + case when @bitPartner=1 then ' or ( ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +')'                                                
else if @tintUserRightType=2 set @strSql=@strSql + ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or Pro.numAssignedTo= '+ convert(varchar(15),@numUserCntID
)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                             
                  
if @tintSortOrder=1  set @strSql=@strSql + ' AND (Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)  +' or Pro.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                   
else if @tintSortOrder=2  set @strSql=@strSql  + ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+'))' + ' or ' + @strShareRedordWith +')'                                           
else if @tintSortOrder=4  set @strSql=@strSql + ' AND Pro.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-8,getutcdate()))+''''                                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and Pro.numModifiedBy= '+ convert(varchar(15),@numUserCntID)        
                                          
                                                
else if @tintSortOrder=4  set @strSql=@strSql + ' and Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)                                            
else if @tintSortOrder=5  set @strSql=@strSql + ' and Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)                                             
 
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria


IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	
	




DECLARE  @firstRec  AS INTEGER=0
DECLARE  @lastRec  AS INTEGER=0
SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)
 --set @StrSql=@StrSql+' WHERE RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec) + ' order by RowNumber'

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords=COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
	END TRY
  BEGIN CATCH
  IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);

  END CATCH
GO 
/****** Object:  StoredProcedure [dbo].[USP_SaveGridColumnWidth]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveGridColumnWidth')
DROP PROCEDURE USP_SaveGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveGridColumnWidth]  
@numDomainID as numeric(9)=0,  
@numUserCntID as numeric(9)=0,
@str as text
as  


declare @hDoc as int     
EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

Create table #tempTable(numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)

  insert into #tempTable (numFormId,numFieldId,bitCustom,intColumnWidth)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
  WITH (numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)                                           
     
UPDATE DFCD set intColumnWidth=temp.intColumnWidth
from  DycFormConfigurationDetails  DFCD  join #tempTable temp
on   DFCD.numFormId=temp.numFormId and DFCD.numFieldId=temp.numFieldId and isnull(DFCD.bitCustom,0)=temp.bitCustom
where DFCD.numDomainID=@numDomainID and DFCD.numUserCntID=@numUserCntID and DFCD.tintPagetype=1                                                         

  drop table #tempTable                                        
 EXEC sp_xml_removedocument @hDoc  
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0
as                            
BEGIN TRY
BEGIN TRANSACTION          
declare @CRMType as integer               
declare @numRecOwner as integer       
DECLARE @numPartenerContact NUMERIC(18,0)=0
	
SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
		
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1        

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
			                       
                            
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0
	-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShipmentMethod,dtReleaseDate
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE()
  
		  )        
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
	C.vcCoupon=P.txtCouponCode AND 
	(C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
		AND numUserCntId =@numContactId


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME
AS                 
	SELECT 
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax 
	FROM 
	(
		SELECT 
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount
		FROM 
			OpportunityMaster OM 
		JOIN 
			OpportunityBizDocs OBD 
		ON 
			OM.numOppId=OBD.numOppId
		JOIN 
			OpportunityMasterTaxItems OMTI 
		ON 
			OMTI.numOppId=OBD.numOppId
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)=''                                                
As                                                                         
  
DECLARE @tintPerformanceFilter AS TINYINT

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500),numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),vcLastSalesOrderDate VARCHAR(100),monDealAmount MONEY, vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500) )                                                         
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                                                            
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0                                                
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] VARCHAR(800)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,                                                                                                            
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) 
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
AND Comm.bitclosedflag=0                                                
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner,                   
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)
 Order by endtime'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql2 =@strSql2 +' 
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            




DECLARE @strSql3 AS VARCHAR(MAX)

SET @strSql3=   ' select  * from #tempRecords '


SET @strSql3=@strSql3+' order by ' + @columnName +' ' + @columnSortOrder


 PRINT @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs,
 numAuthorizePercentage=@numAuthorizePercentage
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAddress]    Script Date: 05/31/2009 15:44:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppaddress')
DROP PROCEDURE usp_updateoppaddress
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppAddress]      
@numOppID as numeric(9)=0,      
@byteMode as tinyint,      
@vcStreet as varchar(100),      
@vcCity as varchar(50),      
@vcPostalCode as varchar(15),      
@numState as numeric(9),      
@numCountry as numeric(9),  
@vcCompanyName as varchar(100), 
@numCompanyId as numeric(9), -- This in Parameter is added by Ajit Singh on 01/10/2008,
@vcAddressName as varchar(50)='',
@numReturnHeaderID as numeric(9)=0,
@bitCalledFromProcedure AS BIT = 0 -- Avoid Tax calculation when this procedure is called from another procedure because all calling procedure have tax calulation uptill now
as      
	declare @numDivisionID as numeric(9)   -- This local variable is added by Ajit Singh on 01/10/2008
	
	SET @numReturnHeaderID = NULLIF(@numReturnHeaderID,0)
	SET @numOppID = NULLIF(@numOppID,0)
	
	DECLARE @numDomainID AS NUMERIC(9)
	SET @numDomainID = 0
	
if @byteMode=0      
	begin      
		IF ISNULL(@numOppID,0) >0
		BEGIN
			 update OpportunityMaster set tintBillToType=2 where numOppID=@numOppID    
	 
			if not exists(select * from OpportunityAddress where numOppID=@numOppID)    
				insert into OpportunityAddress (numOppID,vcBillCompanyName,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcAddressName,numReturnHeaderID,numBillCompanyID)    
				values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numReturnHeaderID,@numCompanyID)    
			else    
				 update OpportunityAddress set  
				  vcBillCompanyName=@vcCompanyName,       
				  vcBillStreet=@vcStreet,      
				  vcBillCity=@vcCity,      
				  numBillState=@numState,      
				  vcBillPostCode=@vcPostalCode,      
				  numBillCountry=@numCountry,vcAddressName=@vcAddressName,
				  numBillCompanyID=@numCompanyId      
				 where numOppID=@numOppID 
		END	 
	   ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			if not exists(select * from OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
				insert into OpportunityAddress (numOppID,vcBillCompanyName,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcAddressName,numReturnHeaderID,numBillCompanyID)    
				values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numReturnHeaderID,@numCompanyID)    
			else
 			 update OpportunityAddress set  
			  vcBillCompanyName=@vcCompanyName,       
			  vcBillStreet=@vcStreet,      
			  vcBillCity=@vcCity,      
			  numBillState=@numState,      
			  vcBillPostCode=@vcPostalCode,      
			  numBillCountry=@numCountry,vcAddressName=@vcAddressName,
			  numBillCompanyID=@numCompanyId      
			 where numReturnHeaderID = @numReturnHeaderID 
		END
	end      
	
else if @byteMode=1      
	begin     
		IF ISNULL(@numOppID,0) >0
		BEGIN
		IF @numCompanyId >0 
		BEGIN
			update OpportunityMaster set tintShipToType=2 where numOppID=@numOppID     
		END 
		
		if not exists(select * from OpportunityAddress where numOppID=@numOppID)    
			insert into OpportunityAddress (numOppID,vcShipCompanyName,vcShipStreet,vcShipCity,numShipState,vcShipPostCode,numShipCountry,vcAddressName,numShipCompanyID)    
			values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numCompanyID)    
		else 
				update OpportunityAddress set  
			  vcShipCompanyName= @vcCompanyName,      
			  vcShipStreet=@vcStreet,      
			  vcShipCity=@vcCity,      
			  numShipState=@numState,      
			  vcShipPostCode=@vcPostalCode,      
			  numShipCountry=@numCountry,vcAddressName=@vcAddressName,
			  numShipCompanyID=@numCompanyId         
			 where numOppID=@numOppID   
		END
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
		if not exists(select * from OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
			insert into OpportunityAddress (numOppID,vcShipCompanyName,vcShipStreet,vcShipCity,numShipState,vcShipPostCode,numShipCountry,vcAddressName,numShipCompanyID)    
			values(@numOppID,@vcCompanyName,@vcStreet,@vcCity,@numState,@vcPostalCode,@numCountry,@vcAddressName,@numCompanyID)    
		else
				update OpportunityAddress set  
			  vcShipCompanyName= @vcCompanyName,      
			  vcShipStreet=@vcStreet,      
			  vcShipCity=@vcCity,      
			  numShipState=@numState,      
			  vcShipPostCode=@vcPostalCode,      
			  numShipCountry=@numCountry,vcAddressName=@vcAddressName,
			  numShipCompanyID=@numCompanyId         
			 where numReturnHeaderID = @numReturnHeaderID 
		END
	END
	
else if @byteMode=2
	BEGIN
		update OpportunityMaster set tintShipToType=3 where numOppID=@numOppID   
	END

ELSE IF @byteMode= 3     
	BEGIN
		--SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 
		IF NOT EXISTS(SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID AND tintAddressOf = 4)    
			BEGIN
				PRINT 'ADD Mode'
				INSERT INTO dbo.AddressDetails 
				(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
				SELECT @vcAddressName,@vcStreet,@vcCity,@vcPostalCode,@numState,@numCountry,1,4,2,@numOppID,numDomainID	
				FROM dbo.ProjectsMaster WHERE numProId = @numOppID
				
				UPDATE dbo.ProjectsMaster SET numAddressID = SCOPE_IDENTITY() WHERE numProId = @numOppID
			END
			
		ELSE
			BEGIN
				PRINT 'Update Mode'	 
				UPDATE dbo.AddressDetails SET 
							vcStreet = @vcStreet,
							vcCity = @vcCity,
							numState = @numState,
							vcPostalCode = @vcPostalCode,
							numCountry = @numCountry,
							vcAddressName = @vcAddressName  
				WHERE   numRecordID = @numOppID   	
			END
			
        --SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 			
	END

IF @numOppID>0 AND ISNULL(@bitCalledFromProcedure,0)=0
BEGIN
	IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND bitAuthoritativeBizDocs=1) = 0
	BEGIN
		DECLARE @tintOppType TINYINT
		SELECT 
			@numDomainID=numDomainID
			,@numDivisionID=numDivisionID
			,@tintOppType=tintOppType
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppId

		DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID	
			UNION 
			SELECT
				@numOppId
				,1
				,decTaxValue
				,tintTaxType
				,numTaxID
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
		END	

		UPDATE OBD SET monDealAmount= [dbo].[GetDealAmount](@numOppId ,getutcdate(),OBD.numOppBizDocsId ) 
		FROM dbo.OpportunityBizDocs OBD WHERE numOppId=@numOppID
	END                
END 


