/******************************************************************
Project: Release 5.0 Date: 09.October.2015
Comments: STORED PROCEDURES
*******************************************************************/



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAllItemQuantityAddedToBizDoc')
DROP FUNCTION CheckAllItemQuantityAddedToBizDoc
GO
CREATE FUNCTION [dbo].[CheckAllItemQuantityAddedToBizDoc] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppBizDocID AS NUMERIC(18,0)
)
RETURNS BIT
AS 
BEGIN	
	DECLARE @isAllItemWithQuantityIncluded BIT = 0

	
	-- check if all quantity of for each item is added to bizdoc
	IF (SELECT
			SUM(QtyDifference) AS QtyDifference
		FROM
			(
				SELECT 
					OpportunityItems.numOppId,
					(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(TEMP1.numUnitHour,0)) AS QtyDifference
				FROM
					OpportunityItems
				LEFT JOIN
					(
						SELECT
							TEMP2.numOppItemID,
							SUM(TEMP2.numUnitHour) AS numUnitHour
						FROM
						(
							SELECT
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
							WHERE
								OpportunityBizDocs.numOppId = @numOppID AND bitAuthoritativeBizDocs = 1) AS TEMP2
						GROUP BY
							TEMP2.numOppItemID
					) AS TEMP1
				ON
					OpportunityItems.numoppitemtCode = TEMP1.numOppItemID
				WHERE
					OpportunityItems.numOppId = @numOppID AND
					 numoppitemtCode In (SELECT numOppItemID FROM OpportunityBizDocItems WHERE OpportunityBizDocItems.numOppBizDocID = @numOppBizDocID)
			) AS TEMP	
		GROUP BY
			TEMP.numOppId) > 0
	BEGIN
		SET @isAllItemWithQuantityIncluded = 0
	END
	ELSE
	BEGIN
		SET @isAllItemWithQuantityIncluded = 1
	END
	
	RETURN @isAllItemWithQuantityIncluded
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderItemInventoryStatus')
DROP FUNCTION CheckOrderItemInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderItemInventoryStatus] 
(
	@numItemCode NUMERIC(18,0) = NULL,
	@numQuantity AS INT = 0,
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT 
)
RETURNS INT
AS
BEGIN

    DECLARE @bitBackOrder INT = 0
	DECLARE @numAllocation INT
	DECLARE @numQtyShipped INT = 0

	SELECT @numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq INT,
			numQtyItemReq_Orig INT,
			numAllocation INT,
			numPrentItemBackOrder INT
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		UPDATE 
			@TEMPTABLE 
		SET 
			numPrentItemBackOrder = (CASE 
								WHEN numAllocation > numQtyItemsReq
								THEN 0 
								ELSE
									(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numAllocation/numQtyItemReq_Orig AS INT) ELSE 0 END)
							END)
		WHERE
			bitKitParent = 0

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numPrentItemBackOrder = (CASE WHEN t2.numQtyItemReq_Orig > 0 THEN CAST((SELECT MAX(numPrentItemBackOrder) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig AS INT) ELSE 0 END)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SELECT @bitBackOrder = MAX(numPrentItemBackOrder) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = numAllocation
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF (@numQuantity - @numQtyShipped) > @numAllocation
            SET @bitBackOrder = (@numQuantity - @numQtyShipped) - @numAllocation
		ELSE
			SET @bitBackOrder = 0
	END

	RETURN @bitBackOrder

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAttributes]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getattributes')
DROP FUNCTION fn_getattributes
GO
CREATE FUNCTION [dbo].[fn_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+'  - '+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
        END
		
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
		IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+'  - -,' 
        END
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getpricebasedonpricebook')
DROP FUNCTION getpricebasedonpricebook
GO
CREATE FUNCTION GetPriceBasedOnPriceBook
--Below function applies Price Rule on given Price an Qty and returns calculated value
    (
      @numRuleID NUMERIC,
      @monPrice MONEY,
      @units INT,
      @ItemCode numeric
    )
RETURNS MONEY
AS BEGIN
    DECLARE @PriceBookPrice MONEY
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount FLOAT
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt FLOAT
--    DECLARE @intOperator INT
    
    SET @PriceBookPrice = 0
    SELECT  @tintRuleType = PB.[tintRuleType],
			@tintPricingMethod = PB.[tintPricingMethod],
			@tintDiscountType = PB.[tintDiscountType],
			@decDiscount = PB.[decDiscount],
			@intQntyItems = PB.[intQntyItems],
			@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    [PriceBookRules] PB
			LEFT OUTER JOIN [PriceBookRuleDTL] PD ON PB.[numPricRuleID] = PD.[numRuleID]
	WHERE   [numPricRuleID] = @numRuleID
	
	
	
    
    IF ( @tintPricingMethod = 1 ) -- Price table
        BEGIN
				SET @intQntyItems = 1;
			   SELECT TOP 1  @tintRuleType = [tintRuleType],
						@tintDiscountType = [tintDiscountType],
						@decDiscount = [decDiscount]
			   FROM     [PricingTable]
			   WHERE    [numPriceRuleID] = @numRuleID
						AND @units BETWEEN [intFromQty] AND [intToQty]

IF (@@ROWCOUNT=0)						
	RETURN @monPrice;


IF @tintRuleType = 2
	BEGIN
		SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
	END
	
				-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
				 IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					
					SET @decDiscount = @monPrice * ( @decDiscount /100);

					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
               -- SELECT dbo.GetPriceBasedOnPriceBook(366,500,21)
				IF ( @tintDiscountType = 2 ) -- Flat discount 
					BEGIN
							
						IF ( @tintRuleType = 1 )
								SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
						IF ( @tintRuleType = 2 )
								SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
					END
           
        END
        
    IF ( @tintPricingMethod = 2 ) -- Pricing Formula 
        BEGIN

		IF @tintRuleType = 2
		BEGIN
			SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
		END
		  
    -- SELECT dbo.GetPriceBasedOnPriceBook(368,500,10)

            IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
					SET @decDiscount = @decDiscount * @monPrice /100;
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice = @monPrice - @decDiscount  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
            IF ( @tintDiscountType = 2 ) -- Flat discount 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
                END
			
        END


    RETURN @PriceBookPrice ;

   END
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as numeric(18,3)=0,
@numReorder as numeric(18,3)=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
as  

DECLARE @numDomain AS NUMERIC(18,0)
SET @numDomain = @numDomainID
Declare @bitLotNo as bit;SET @bitLotNo=0  
Declare @bitSerialized as bit;SET @bitSerialized=0  

select @bitLotNo=isnull(Item.bitLotNo,0),@bitSerialized=isnull(Item.bitSerialized,0)
from item where numItemCode=@numItemCode and numDomainID=@numDomainID

DECLARE @vcDescription AS VARCHAR(100)
IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
BEGIN
--Insert/Update WareHouseItems
IF @byteMode=0 or @byteMode=1
BEGIN
IF @numWareHouseItemID>0
  BEGIN
		UPDATE WareHouseItems  SET numWareHouseID=@numWareHouseID,                                                              
				--numOnHand=(Case When @bitLotNo=1 or @bitSerialized=1 then numOnHand else @numOnHand end),
				numReorder=@numReorder,monWListPrice=@monWListPrice,vcLocation=@vcLocation,numWLocationID = @numWLocationID
				,vcWHSKU=@vcWHSKU,vcBarCode=@vcBarCode ,dtModified=GETDATE()   
		WHERE numItemID=@numItemCode and numDomainID=@numDomainID and numWareHouseItemID=@numWareHouseItemID
		
		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
			BEGIN
				IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0) --AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
					BEGIN
						UPDATE WareHouseItems SET numOnHand = @numOnHand WHERE numItemID = @numItemCode AND numDomainID = @numDomainID AND numWareHouseItemID = @numWareHouseItemID		
					END
			END
		
		SET @vcDescription='UPDATE WareHouse'
  END
ELSE
  BEGIN
	 insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,numReorder,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,dtModified,numWLocationID)  
			values(@numItemCode,@numWareHouseID,(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),@numReorder,@monWListPrice,@vcLocation,@vcWHSKU,@vcBarCode,@numDomainID,GETDATE(),@numWLocationID)  

	 --PRINT @numWareHouseItemID
	 SET @numWareHouseItemID = @@identity
	 --PRINT @numWareHouseItemID
	 
		SET @vcDescription='INSERT WareHouse'
  END
END

--Insert/Update WareHouseItmsDTL
--DEclare @numWareHouseItmsDTLID as numeric(18);SET @numWareHouseItmsDTLID=0
DECLARE @OldQty INT;SET @OldQty=0

IF @byteMode=0 or @byteMode=2 or @byteMode=5
BEGIN

IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	IF @bitSerialized=1
		SET @numQty=1
	
    IF @byteMode=0
	BEGIN
		select top 1 @numWareHouseItmsDTLID=numWareHouseItmsDTLID,@OldQty=numQty from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
			vcSerialNo=@vcSerialNo and numQty > 0
	END
	ELSE IF @numWareHouseItmsDTLID>0
	BEGIN
		select top 1 @OldQty=numQty from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END

	IF @numWareHouseItmsDTLID>0
		BEGIN
				UPDATE WareHouseItmsDTL SET vcComments = @vcComments,numQty=@numQty,vcSerialNo=@vcSerialNo
                where numWareHouseItmsDTLID=@numWareHouseItmsDTLID and numWareHouseItemID=@numWareHouseItemID
                
                SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
		END
	ELSE
		BEGIN
			   INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
			   Values (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
			   set @numWareHouseItmsDTLID=@@identity

               SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
		END

 	   update WareHouseItems SET numOnHand=numOnHand + (@numQty - @OldQty) ,dtModified=GETDATE() 
	   where numWareHouseItemID = @numWareHouseItemID 
	   AND [numDomainID] = @numDomainID
	   AND (numOnHand + (@numQty - @OldQty))>=0
END
END

IF DATALENGTH(@strFieldList)>2
BEGIN
	--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
	declare @hDoc as int     
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

	declare  @rows as integer                                        
	                                         
	SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
					WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

	if @rows>0                                        
	begin  
	  Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
	  insert into #tempTable (Fld_ID,Fld_Value)                                        
	  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
	  WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
	  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
	  inner join #tempTable T on C.Fld_ID=T.Fld_ID 
		where C.RecId=(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end) and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end)                                       
	      
	  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
	  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end),(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) from #tempTable 

	  drop table #tempTable                                        
	 end  

	 EXEC sp_xml_removedocument @hDoc 
END	  
 
 EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numItemCode, --  numeric(9, 0)
	@tintRefType = 1, --  tinyint
	@vcDescription = @vcDescription, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@ClientTimeZoneOffset = 0,
	@dtRecordDate = @dtAdjustmentDate,
	@numDomainID = @numDomain 
END

ELSE IF @byteMode=3
BEGIN

	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
	BEGIN
		raiserror('OpportunityItems_Depend',16,1);
		RETURN ;
	END

     IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) >0
 	 BEGIN
	  	RAISERROR ('OpportunityKitItems_Depend',16,1);
        RETURN
	  END
	
	IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) >0
 	 BEGIN
	  	RAISERROR ('KitItems_Depend',16,1);
        RETURN
	  END	
	    
IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END
ELSE
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END

DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 

DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

END

ELSE IF @byteMode=4
BEGIN
IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END

update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
AND WHI.numDomainID = @numDomainID
AND (WHI.numOnHand - WHID.numQty)>=0

SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
AND WHI.numDomainID = @numDomainID


EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numItemCode, --  numeric(9, 0)
	@tintRefType = 1, --  tinyint
	@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@ClientTimeZoneOffset = 0,
	@dtRecordDate = NULL,
	@numDomainID = @numDomain

DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID


END

GO
/* 
UnComment this  Only for dubug purpose
UPDATE item SET numAssetChartAcntId=NULL,numCOGsChartAcntId=NULL,numIncomeChartAcntId=NULL WHERE numDomainID=@numDomainId
DELETE FROM dbo.AccountingCharges WHERE numDomainID=@numDomainId                                
DELETE FROM dbo.COAShippingMapping WHERE numDomainID=@numDomainId
DELETE FROM dbo.COARelationships WHERE numDomainID=@numDomainId
DELETE FROM dbo.ChartAccountOpening WHERE numDomainId=@numDomainId
DELETE FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainId
DELETE FROM dbo.AccountTypeDetail WHERE numDomainId=@numDomainId
*/    
--exec USP_ChartAcntDefaultValues @numDomainId=204,@numUserCntId=116784
--SELECT * FROM chart_of_Accounts where numDomainID = 183
--select * from AccountTypeDetail where numDomainID=110
     
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartAcntDefaultValues' ) 
    DROP PROCEDURE USP_ChartAcntDefaultValues
GO
CREATE PROCEDURE [dbo].[USP_ChartAcntDefaultValues]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0
AS 
    BEGIN        
 
        DECLARE @intCount AS NUMERIC(9)   
        DECLARE @dtTodayDate AS DATETIME
        SET @dtTodayDate = GETDATE()                                                 
        SET @intCount = 0                                  
                                           
        SELECT  @intCount = COUNT(*)
        FROM    Chart_Of_Accounts
        WHERE   numDomainId = @numDomainId                                                    
        
        
        IF @intCount = 0 
            BEGIN      
----INSERTING DEFAULT ACCOUNT TYPES
                EXEC USP_AccountTypeDefaultValue @numDomainId = @numDomainId ;


 --Check for Current Fiancial Year
        IF ( SELECT COUNT([numFinYearId])
             FROM   [FinancialYear]
             WHERE  [numDomainId] = @numDomainId
                    AND [bitCurrentYear] = 1
           ) = 0 
            BEGIN
            
				DECLARE @dtPeriodFrom AS DATETIME,@dtPeriodTo AS DATETIME
				
				SELECT	@dtPeriodFrom=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0),
						@dtPeriodTo=DATEADD(YEAR, DATEDIFF(YEAR, -1, GETDATE()), -1)
	
				EXEC dbo.USP_ManageFinancialYear
					@numDomainId = @numDomainId, --  numeric(9, 0)
					@dtPeriodFrom = @dtPeriodFrom, --  datetime
					@dtPeriodTo = @dtPeriodTo, --  datetime
					@vcFinYearDesc = '', --  varchar(50)
					@bitCloseStatus = 0, --  bit
					@bitAuditStatus = 0, --  bit
					@bitCurrentYear = 1 --  bit
            END
            
                


DECLARE @numAssetAccountTypeID AS NUMERIC(9,0)
DECLARE @numLiabilityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numIncomeAccountTypeID AS NUMERIC(9,0) 
DECLARE @numExpenseAccountTypeID  AS NUMERIC(9,0)
DECLARE @numEquityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numCOGSAccountTypeID  AS NUMERIC(9,0)

SELECT @numAssetAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = @numDomainID 
SELECT @numLiabilityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @numDomainID 
SELECT @numIncomeAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0103' AND numDomainID = @numDomainID 
SELECT @numExpenseAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = @numDomainID 
SELECT @numEquityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = @numDomainID  

DECLARE @numLoanAccountTypeID AS NUMERIC(9)
SELECT @numLoanAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020201'  AND numDomainID = @numDomainID  
PRINT @numLoanAccountTypeID

DECLARE @numParentForCOGSAccID AS NUMERIC(9)
SELECT @numParentForCOGSAccID = numParentID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID AND vcAccountCode = '0101'
PRINT @numParentForCOGSAccID


SELECT @numCOGSAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = @numDomainID  
DECLARE @numRSccountTypeID AS NUMERIC(9)
SELECT @numRSccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = @numDomainID  
PRINT @numRSccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Profit & Loss',
    @vcAccountDescription = 'Profit & Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 1, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
   
PRINT 18
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Retained Earnings',
    @vcAccountDescription = 'Retained Earnings', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Opening Balance Equity',
    @vcAccountDescription = 'Opening Balance Equity', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

   
------------------------------------ ASSETS ENTRY --------------------------------------------
    
DECLARE @numBankAccountTypeID AS NUMERIC(9)
SELECT @numBankAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010101'  AND numDomainID = @numDomainID  
PRINT @numBankAccountTypeID

--select numAccountTypeID,'UnDepositedFunds','UnDepositedFunds','0101010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01010101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numBankAccountTypeID , @vcAccountName = 'UnDeposited Funds',
    @vcAccountDescription = 'UnDepositedFunds',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
-----------------
 
DECLARE @numPropAccountTypeID AS NUMERIC(9)
SELECT @numPropAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010201'  AND numDomainID = @numDomainID  
PRINT @numPropAccountTypeID   
    
PRINT 1
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Office Equipment',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 2
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID,
    @vcAccountName = 'Less Accumulated Depreciation on Office Equipment',
    @vcAccountDescription = 'The total amount of office equipment cost that has been consumed by the entity (based on the useful life)',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 3
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Computer Equipment',
    @vcAccountDescription = 'Computer equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 4
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
--    @numParntAcntTypeID = @numPropAccountTypeID,
--    @vcAccountName = 'Less Accumulated Depreciation on Computer Equipment',
--    @vcAccountDescription = 'The total amount of computer equipment cost that has been consumed by the business (based on the useful life)',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-------------------------

DECLARE @numARAccountTypeID AS NUMERIC(9)
SELECT @numARAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @numDomainID  
PRINT @numARAccountTypeID  

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Account Receivable',
    @vcAccountDescription = 'Account Receivable.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 5
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Prepayments',
    @vcAccountDescription = 'An expenditure that has been paid for in advance.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Sales Clearing',
    @vcAccountDescription = 'Sales Clearing.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


DECLARE @numStockAccountTypeID AS NUMERIC(9)
SELECT @numStockAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010104'  AND numDomainID = @numDomainID  
PRINT @numStockAccountTypeID  

PRINT 6
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Inventory',
    @vcAccountDescription = 'Items available for sale including all costs of production.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Work In Progress',
    @vcAccountDescription = 'Work In Progress',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0    

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Finished Goods',
    @vcAccountDescription = 'Finished Goods',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0     
---------------------------------------------------------------------------------------------------------

------------------------------------ LIABILITIES ENTRY --------------------------------------------
DECLARE @numDutyTaxAccountTypeID AS NUMERIC(9)
SELECT @numDutyTaxAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101'  AND numDomainID = @numDomainID  
PRINT @numDutyTaxAccountTypeID

--select numAccountTypeID,'Sales Tax Payable','Sales Tax Payable','0102010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01020101'   
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numDutyTaxAccountTypeID , @vcAccountName = 'Sales Tax Payable',
    @vcAccountDescription = 'Sales Tax Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numAPAccountTypeID AS NUMERIC(9)
SELECT @numAPAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numAPAccountTypeID


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Deduction from Employee Payroll',
    @vcAccountDescription = 'Deduction from Employee Payroll',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Accounts Payable',
    @vcAccountDescription = 'Accounts Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 7
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Employee Tax Payable',
    @vcAccountDescription = 'The amount of tax that has been deducted from wages or salaries paid to employes and is due to be paid',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 8
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Income Tax Payable',
    @vcAccountDescription = 'The amount of income tax that is due to be paid, also resident withholding tax paid on interest received.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 9
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = 1490,
--    @numParntAcntTypeID = 1491, @vcAccountName = 'Suspense',
--    @vcAccountDescription = 'An entry that allows an unknown transaction to be entered, so the accounts can still be worked on in balance and the entry can be dealt with later.',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102' AND numDomainID = @numDomainID  

DECLARE @numCLAccountTypeID AS NUMERIC(9)
SELECT @numCLAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numCLAccountTypeID

PRINT 10
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Unpaid Expense Claims',
    @vcAccountDescription = 'Automatically updates this account for payroll entries created using Payroll and will store the payroll amount to be paid to the employee for the pay run. This account enables you to maintain separate accounts for employee Wages Payable amounts and Accounts Payable amounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 11
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Wages Payable',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 14
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Tracking Transfers',
    @vcAccountDescription = 'Transfers between tracking categories',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numWPAccountTypeID AS NUMERIC(9)
SELECT @numWPAccountTypeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID  AND vcAccountName = 'Wages Payable'
PRINT @numWPAccountTypeID
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Wages & Salaries Payable',
    @vcAccountDescription = 'Wages & Salaries Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWPAccountTypeID
    
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE numAccountTypeID = 1490 AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND bitActive = 0 AND vcAccountName IN ('Sales Tax Payable', 'Loan','Tracking Transfers')


PRINT 15
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numLoanAccountTypeID , @vcAccountName = 'Loan',
    @vcAccountDescription = 'Money that has been borrowed from a creditor',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount =0, @numParentAccId = 0

DECLARE @numCurrentLiabilitiesAccountTypeID AS NUMERIC(9)
SELECT @numCurrentLiabilitiesAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201'  AND numDomainID = @numDomainID  
PRINT @numCurrentLiabilitiesAccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCurrentLiabilitiesAccountTypeID, @vcAccountName = 'Deferred Income',
    @vcAccountDescription = 'Deferred Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

---------------------------------------------------------------------------------------------------

------------------------------------ EQUITY ENTRY --------------------------------------------

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105' AND numDomainID = @numDomainID  
DECLARE @numEquityID AS NUMERIC(9)
SELECT @numEquityID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105'  AND numDomainID = @numDomainID  
PRINT @numEquityID

---------------------------------------------------------------------------------------------------

------------------------------------ EXPENSE ENTRY --------------------------------------------

----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010601' AND numDomainID = @numDomainID  
--DECLARE @numCOGSID AS NUMERIC(9)
--SELECT @numCOGSID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
--PRINT @numCOGSID
----select numAccountTypeID,'COGS Control Account','COGS','0104010401',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numCOGSID, @vcAccountName = 'COGS',
--    @vcAccountDescription = 'COGS Control Account',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040105' AND numDomainID = @numDomainID  
DECLARE @numOtherDirectExpenseID AS NUMERIC(9)
SELECT @numOtherDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
PRINT @numOtherDirectExpenseID
--select numAccountTypeID,'Billable Time & Expenses','Billable Time & Expenses','0104010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Billable Time & Expenses',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Purchase Account','Purchase','0104010402',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Purchase',
    @vcAccountDescription = 'Purchase Account',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Reconciliation Discrepancies',
    @vcAccountDescription = 'Reconciliation Discrepancies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Foreign Exchange Gain/Loss',
    @vcAccountDescription = 'Foreign Exchange Gain/Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--01040104
--DECLARE @numPriceExpenseID AS NUMERIC(9)
--SELECT @numPriceExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040104' 
--PRINT @numPriceExpenseID 

--PRINT 20
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Price Variance',
--    @vcAccountDescription = 'Price Variance',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Inventory Adjustment',
    @vcAccountDescription = 'Inventory Adjustment',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
DECLARE @numCOGSExpenseID AS NUMERIC(9)
SELECT @numCOGSExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
PRINT @numCOGSExpenseID 
       
PRINT 20
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Cost of Goods Sold',
    @vcAccountDescription = 'Costs of goods made by the business include material, labor, and other modification costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Purchase Price Variance',
    @vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


    
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
DECLARE @numDirectExpenseID AS NUMERIC(9)
SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
PRINT @numDirectExpenseID 
--       
----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
--DECLARE @numDirectExpenseID AS NUMERIC(9)
--SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
--PRINT @numDirectExpenseID 

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040103' AND numDomainID = @numDomainID  
DECLARE @numOpeExpenseID AS NUMERIC(9)
SELECT @numOpeExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040103' 
PRINT @numOpeExpenseID 

--PRINT 28
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Office Equipment',
--    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
  
PRINT 31
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Utilities',
    @vcAccountDescription = 'Expenses incurred for lighting, powering or heating the premises',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
     
PRINT 32 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Automobile Expenses',
    @vcAccountDescription = 'Expenses incurred on the running of company automobiles.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 35 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Rent',
    @vcAccountDescription = 'The payment to lease a building or area.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 42
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Bad Debts',
    @vcAccountDescription = 'Noncollectable accounts receivable which have been written off.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010402' AND numDomainID = @numDomainID  
DECLARE @numIndirectExpenseID AS NUMERIC(9)
SELECT @numIndirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010402' 
PRINT @numIndirectExpenseID 
        
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040201' AND numDomainID = @numDomainID  
DECLARE @numAdminExpenseID AS NUMERIC(9)
SELECT @numAdminExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040201' 
PRINT @numAdminExpenseID 

       
PRINT 26
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Postage & Delivery',
    @vcAccountDescription = 'Expenses incurred on postage & delivery costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 23
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Janitorial Expenses',
    @vcAccountDescription = 'Expenses incurred for cleaning business property.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 24
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Consulting & Accounting',
    @vcAccountDescription = 'Expenses related to paying consultants',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 27
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'General Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 33
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Office Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business office.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 36
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Repairs and Maintenance',
    @vcAccountDescription = 'Expenses incurred on a damaged or run down asset that will bring the asset back to its original condition.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
 
PRINT 39 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Dues & Subscriptions',
    @vcAccountDescription = 'E.g. Magazines, professional bodies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202' AND numDomainID = @numDomainID  
DECLARE @numStaffExpenseID AS NUMERIC(9)
SELECT @numStaffExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
PRINT @numStaffExpenseID 

PRINT 37 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Wages and Salaries',
    @vcAccountDescription = 'Payment to employees in exchange for their resources',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 41 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Travel',
    @vcAccountDescription = 'Expenses incurred from travel which has a business purpose',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    

SELECT * FROM dbo.Chart_Of_Accounts WHERE numDomainID = 1 AND vcAccountName = 'Wages and Salaries'
DECLARE @numWSID AS NUMERIC(9)
SELECT @numWSID = numAccountID FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID   AND vcAccountName = 'Wages and Salaries'
PRINT @numWSID 

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID , @vcAccountName = 'Employee payroll expense',
    @vcAccountDescription = 'Employee payroll expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Contract Employee Payroll Expense',
    @vcAccountDescription = 'Contract Employee Payroll Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID
  
---------------------------------------------------------------------------------------------------

------------------------------------ INCOME ENTRY --------------------------------------------
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301' AND numDomainID = @numDomainID  
DECLARE @numDirectIncomeAccountTypeID AS NUMERIC(9)
SELECT @numDirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301'  AND numDomainID = @numDomainID  
PRINT @numDirectIncomeAccountTypeID

--select numAccountTypeID,'Sales Discounts','Sales Discounts','01030101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010301'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales Discounts',
    @vcAccountDescription = 'Sales Discounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Shipping Income','Shipping Income','01030201',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Shipping Income',
    @vcAccountDescription = 'Shipping Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Late Charges','Late Charges','01030202',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Late Charges',
    @vcAccountDescription = 'Late Charges',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 46 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numDirectIncomeAccountTypeID, @vcAccountName = 'Other Revenue',
    @vcAccountDescription = 'Any other income that does not relate to normal business activities and is not recurring',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales',
    @vcAccountDescription = 'Sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
---------------------------------------------------------------------------------------------------

--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201')
--DELETE FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'

----------------

-- Create Credit Card as a new account under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Credit Card',
    @vcAccountDescription = 'Credit Card',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--- Adding new account "Purchase Clearing" under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Purchase Clearing',
    @vcAccountDescription = 'Purchase Clearing',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-- From Accounts payable remove �Historical adjustments� and �rounding� unless it is required as default for some specific purpose. 
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID   
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  
--
----SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010211')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  

PRINT 12
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Historical Adjustment',
--    @vcAccountDescription = 'For accountant adjustments',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 13
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Rounding',
--    @vcAccountDescription = 'An adjustment entry to allow for rounding',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Remove Suspense A/c from Current Liability
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020104' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  

--From Current liability change the name of �Loans (Liability)� account type to Short Term borrowings.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01020103'
--UPDATE dbo.AccountTypeDetail SET vcAccountType = 'Short Term Borrowings' WHERE vcAccountCode = '01020103' AND numDomainID = @numDomainID  


--Interest income should be under indirect income instead of direct income.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010302' 
DECLARE @numIndirectIncomeAccountTypeID AS NUMERIC(9,0)
SELECT @numIndirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010302'  AND numDomainID = @numDomainID  
PRINT @numIndirectIncomeAccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Interest Income',
    @vcAccountDescription = 'Interest income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Account with the name of �Dividend income� should be there under indirect income.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Dividend Income',
    @vcAccountDescription = 'Dividend income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--------------------------------------------------------------------------------------------------------------------------------------
-- DONE 																															--																																		    
--Under expense side COGS accounts should not be under any other category except COGS so remove the same from other direct expense. --
--Remove �office equipment� from operating expense type.																			--
--------------------------------------------------------------------------------------------------------------------------------------

--Transfer following accounts to Administrative Expense from Operating Expense.
--Printing & Stationary,   telephone & Internet, Income Tax expense, Entertainment, Insurance.
PRINT 34 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Printing & Stationery',
    @vcAccountDescription = 'Expenses incurred by the entity as a result of printing and stationery',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 40
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Telephone & Internet',
    @vcAccountDescription = 'Expenditure incurred from any business-related phone calls, phone lines, or internet connections',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 44 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Income Tax Expense',
    @vcAccountDescription = 'A percentage of total earnings paid to the government',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0 , @numParentAccId = 0

PRINT 25
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Entertainment',
    @vcAccountDescription = 'Expenses paid by company for the business but are not deductable for income tax purposes.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 29
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Insurance',
    @vcAccountDescription = 'Expenses incurred for insuring the business'' assets',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
           
--Advertising expense should be moved to Selling & Distribution from operating expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040102' 
DECLARE @numSDccountTypeID AS NUMERIC(9,0)
SELECT @numSDccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040102'  AND numDomainID = @numDomainID  
PRINT @numSDccountTypeID
   
PRINT 21
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSDccountTypeID, @vcAccountName = 'Advertising',
    @vcAccountDescription = 'Expenses incurred for advertising while trying to increase sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--�Interest Expense� should be under Finance Cost rather than Operating Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040203' 
DECLARE @numFCccountTypeID AS NUMERIC(9,0)
SELECT @numFCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040203'  AND numDomainID = @numDomainID  
PRINT @numFCccountTypeID

PRINT 45 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Interest Expense',
    @vcAccountDescription = 'Any interest expenses paid to your tax authority, business bank accounts or credit card accounts.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move bank Service charges under finance Costs.
PRINT 22
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Bank Service Charges',
    @vcAccountDescription = 'Fees charged by your bank for transactions regarding your bank account(s).',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Payroll tax expense� to Staff expense from operating expense.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
DECLARE @numSEccountTypeID AS NUMERIC(9,0)
SELECT @numSEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202'  AND numDomainID = @numDomainID  
PRINT @numSEccountTypeID
    
PRINT 38 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSEccountTypeID, @vcAccountName = 'Payroll Tax Expense',
    @vcAccountDescription = '', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Depreciation� from administrative expense to Non-cash Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010404' 
DECLARE @numNCEccountTypeID AS NUMERIC(9,0)
SELECT @numNCEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040205'  AND numDomainID = @numDomainID  
PRINT @numNCEccountTypeID

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Depreciation',
    @vcAccountDescription = 'The amount of the asset''s cost (based on the useful life) that was consumed during the period',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Create new account with the name �Amortization� under non-cash expense.   

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Amortization',
    @vcAccountDescription = 'Amortization',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Legal Expense should be under Administrative Expenses.
PRINT 30
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Legal Expenses',
    @vcAccountDescription = 'Expenses incurred on any legal matters',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--Owner�s contribution, owner�s draw, and common stock should be under Shareholder�s Fund.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010502' 
DECLARE @numOCccountTypeID AS NUMERIC(9,0)
SELECT @numOCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010502'  AND numDomainID = @numDomainID  
PRINT @numOCccountTypeID
        
PRINT 16
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Contribution',
    @vcAccountDescription = 'Funds contributed by the owner',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 17
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Draw',
    @vcAccountDescription = 'Withdrawals by the owners',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050102' 
DECLARE @numSFccountTypeID AS NUMERIC(9,0)
SELECT @numSFccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050102'  AND numDomainID = @numDomainID  
PRINT @numSFccountTypeID

PRINT 19
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numSFccountTypeID, @vcAccountName = 'Common Stock',
    @vcAccountDescription = 'The value of shares purchased by the shareholders',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
--Change the name of �Capital & Reserves� to �Reserves & Surplus�.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 
--UPDATE AccountTypeDetail SET vcAccountType = 'Reserves & Surplus' WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 

--�Profit & Loss� and �retained Earning� should be under Reserves & Surplus.
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101' AND numDomainID = @numDomainID  


--add 2 new COA under : Indirect expense->UnCategorized Expense
--					  : Indirect Income->UnCategorized Income
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numIndirectIncomeAccountTypeID , @vcAccountName = 'UnCategorized Income',
    @vcAccountDescription = 'UnCategorized Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

------- Add new Account type UnCategorized Expense and move account "UnCategorized Expense" under it 

DECLARE @numUnCategorizedExpenseID AS NUMERIC(9,0)
SELECT @numUnCategorizedExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040206'  AND numDomainID = @numDomainID  
PRINT 'numUnCategorizedExpenseID : ' + CONVERT(VARCHAR(10),@numUnCategorizedExpenseID)

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numUnCategorizedExpenseID , @vcAccountName = 'UnCategorized Expense',
    @vcAccountDescription = 'UnCategorized Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

----------------

--SELECT * FROM dbo.AccountingCharges WHERE numDomainID = 1
DELETE FROM dbo.AccountingCharges WHERE numDomainID = @numDomainID

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @ST AS VARCHAR(10)
SELECT @ST = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Tax Payable'
PRINT 'Sales Tax Payable : ' + @ST

DECLARE @AR AS VARCHAR(10)
SELECT @AR = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Account Receivable'
PRINT 'Account Receivable : ' + @AR

DECLARE @SC AS VARCHAR(10)
SELECT @SC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Clearing'
PRINT 'Sales Clearing : ' + @SC

DECLARE @DG AS VARCHAR(10)
SELECT @DG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Discounts'
PRINT 'Sales Discounts : ' + @DG

DECLARE @SI AS VARCHAR(10)
SELECT @SI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Shipping Income'
PRINT 'Shipping Income: ' + @SI

DECLARE @LC AS VARCHAR(10)
SELECT @LC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Late Charges'
PRINT 'Late Charges :' + @LC

DECLARE @AP AS VARCHAR(10)
SELECT @AP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Accounts Payable'
PRINT 'Account Payable : ' + @AP

DECLARE @UF AS VARCHAR(10)
SELECT @UF = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnDeposited Funds'
PRINT 'UnDeposited Funds : ' + @UF

DECLARE @BE AS VARCHAR(10)
SELECT @BE = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Billable Time & Expenses'
PRINT 'Billable Time & Expenses : ' + @BE

DECLARE @CG AS VARCHAR(10)
SELECT @CG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Cost of Goods Sold'
PRINT 'Cost of Goods Sold : ' + @CG

DECLARE @EP AS VARCHAR(10)
SELECT @EP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Employee Payroll Expense'
PRINT 'Employee Payroll Expense : ' + @EP

DECLARE @CP AS VARCHAR(10)
SELECT @CP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Contract Employee Payroll Expense'
PRINT 'Contract Employee Payroll Expense : ' + @CP

DECLARE @PL AS VARCHAR(10)
SELECT @PL = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Wages & Salaries Payable'
--SELECT ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = 1 AND vcAccountName = 'Payroll Liability'
--SET @WSP = '1010'
PRINT 'Wages & Salaries Payable : ' + @PL

DECLARE @WP AS VARCHAR(10)
SELECT @WP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Work in Progress'
--SET @WP = '1010'
PRINT 'Wok in Progress : ' + @WP

DECLARE @DP AS VARCHAR(10)       
SELECT @DP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deduction from Employee Payroll'
--SET @DP = '1010'
PRINT 'Deductions form Employee Payroll : ' + @DP

DECLARE @UI AS VARCHAR(10)       
SELECT @UI = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnCategorized Income'
--SET @DP = '1010'
PRINT 'UnCategorized Income : ' + @UI

DECLARE @UE AS VARCHAR(10)       
SELECT @UE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND ISNULL(vcAccountName,'') = 'UnCategorized Expense'
--SET @DP = '1010'
PRINT 'UnCategorized Expense : ' + CONVERT(VARCHAR(10),@UE)

DECLARE @RC AS VARCHAR(10)       
SELECT @RC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Reconciliation Discrepancies'
--SET @DP = '1010'
PRINT 'Reconciliation Discrepancies : ' + @RC

DECLARE @FE AS VARCHAR(10)       
SELECT @FE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Foreign Exchange Gain/Loss'
PRINT 'Foreign Exchange Gain/Loss : ' + ISNULL(@FE,'')

DECLARE @IA AS VARCHAR(10)       
SELECT @IA = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Inventory Adjustment'
PRINT 'Inventory Adjustment : ' + ISNULL(@IA,'')

DECLARE @OE AS VARCHAR(10)       
SELECT @OE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Opening Balance Equity'
PRINT 'Opening Balance Equity : ' + ISNULL(@OE,'')

DECLARE @PC AS VARCHAR(10)       
SELECT @PC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Clearing'

PRINT 'Purchase Clearing : ' + ISNULL(@PC,'')

DECLARE @PV AS VARCHAR(10)       
SELECT @PV = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Price Variance'

PRINT 'Purchase Price Variance : ' + ISNULL(@PV,'')

DECLARE @DI AS VARCHAR(10)
SELECT @DI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deferred Income'
PRINT 'Deferred Income : ' + @DI


--SET @strSQL = ''
SET @strSQL = ' exec USP_ManageDefaultAccountsForDomain @numDomainID = ' + CONVERT(VARCHAR(10),@numDomainId) + ',@str=''<NewDataSet>
																			  <Table1>
																				<chChargeCode>EP</chChargeCode>
																				<numAccountID>' + ISNULL(@EP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CP</chChargeCode>
																				<numAccountID>' + ISNULL(@CP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>ST</chChargeCode>
																				<numAccountID>' + ISNULL(@ST,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AR</chChargeCode>
																				<numAccountID>' + ISNULL(@AR,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DG</chChargeCode>
																				<numAccountID>' + ISNULL(@DG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SI</chChargeCode>
																				<numAccountID>' + ISNULL(@SI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>LC</chChargeCode>
																				<numAccountID>' + ISNULL(@LC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AP</chChargeCode>
																				<numAccountID>' + ISNULL(@AP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UF</chChargeCode>
																				<numAccountID>' + ISNULL(@UF,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>BE</chChargeCode>
																				<numAccountID>' + ISNULL(@BE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CG</chChargeCode>
																				<numAccountID>' + ISNULL(@CG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PL</chChargeCode>
																				<numAccountID>' + ISNULL(@PL,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>WP</chChargeCode>
																				<numAccountID>' + ISNULL(@WP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DP</chChargeCode>
																				<numAccountID>' + ISNULL(@DP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UI</chChargeCode>
																				<numAccountID>' + ISNULL(@UI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UE</chChargeCode>
																				<numAccountID>' + ISNULL(@UE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>RC</chChargeCode>
																				<numAccountID>' + ISNULL(@RC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>FE</chChargeCode>
																				<numAccountID>' + ISNULL(@FE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>IA</chChargeCode>
																				<numAccountID>' + ISNULL(@IA,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>OE</chChargeCode>
																				<numAccountID>' + ISNULL(@OE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PC</chChargeCode>
																				<numAccountID>' + ISNULL(@PC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PV</chChargeCode>
																				<numAccountID>' + ISNULL(@PV,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SC</chChargeCode>
																				<numAccountID>' + ISNULL(@SC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DI</chChargeCode>
																				<numAccountID>' + ISNULL(@DI,'0') + '</numAccountID>
																			  </Table1>
																			</NewDataSet>'''

PRINT CAST(@strSQL AS VARCHAR(MAX))
EXEC SP_EXECUTESQL @strSQL

------- Setting up Default Accounts for Income, Asset & COGS of domain. ---------------------------------------
DECLARE @AssetAccountID AS VARCHAR(10)
SELECT @AssetAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Inventory' AND numDomainID = @numDomainID
PRINT 'AssetAccountID : ' + @AssetAccountID

DECLARE @IncomeAccountID AS VARCHAR(10)
SELECT @IncomeAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Sales' AND numDomainID = @numDomainID
PRINT 'IncomeAccountID : ' + @IncomeAccountID

DECLARE @COGSAccountID AS VARCHAR(10)
SELECT @COGSAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Cost of Goods Sold' AND numDomainID = @numDomainID
PRINT 'COGSAccountID : ' + @COGSAccountID

UPDATE dbo.Domain SET numAssetAccID = @AssetAccountID, numIncomeAccID = @IncomeAccountID, numCOGSAccID = @COGSAccountID WHERE numDomainId = @numDomainID
------------------------------------------------------------

DECLARE @numModuleID AS INT

------- UPDATE COARelationships FOR DEFAULT ACCOUNT SETTING FOR Accounts Receivable & Accounts Payable --------
DECLARE @numCustRelationshipID AS NUMERIC(10)
DECLARE @numEmpRelationshipID AS NUMERIC(10)
DECLARE @numVendorRelationShipID AS NUMERIC(10)

DECLARE @numListID AS NUMERIC(10)
SELECT @numListID = numListID FROM dbo.ListMaster WHERE vcListName = 'Relationship'  AND bitFlag = 1
PRINT @numListID

SELECT @numCustRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Customer'
PRINT @numCustRelationshipID

SELECT @numEmpRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Employer'
PRINT @numEmpRelationshipID

SELECT @numVendorRelationShipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Vendor'
PRINT @numVendorRelationShipID


DELETE FROM dbo.COARelationships WHERE numDomainID = @numDomainID

INSERT INTO dbo.COARelationships (numRelationshipID,numARParentAcntTypeID,numAPParentAcntTypeID,numARAccountId,numAPAccountId,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy) 
SELECT @numCustRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numEmpRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numVendorRelationShipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
---------------------------------------------------------------------------------------------------------------------                   
 
------- UPDATE COAShippingMapping FOR DEFAULT SHIPPING METHOD MAPPING SETTING ---------------------------------
--SELECT * FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
DELETE FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID AND bitActive = 0
DECLARE @numShipIncomeID AS NUMERIC(10)
SELECT @numShipIncomeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID --AND bitActive = 0
PRINT @numShipIncomeID

INSERT INTO dbo.COAShippingMapping (numShippingMethodID,numIncomeAccountID,numDomainID) 
SELECT 88,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 91,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 90,@numShipIncomeID,@numDomainID

--Script to Correct  Account type for COA -added by chintan
                SELECT  *
                INTO    #TempAccounts
                FROM    ( SELECT    numAccountId,
                                    vcAccountName,
                                    COA.vcAccountCode,
                                    COA.numParntAcntTypeId,
                                    COA.numAcntTypeId OLDnumAcntTypeId,
                                    ( SELECT    ATD.numAccountTypeID
                                      FROM      dbo.AccountTypeDetail ATD
                                      WHERE     ATD.numDomainID = COA.numDomainID
                                                AND ATD.vcAccountCode = SUBSTRING(COA.vcAccountCode, 1, 4)
                                    ) NEWnumAcntTypeId
                          FROM      dbo.Chart_Of_Accounts COA
                          WHERE     COA.numDomainId = @numDomainId
                        ) X
                WHERE   X.OLDnumAcntTypeId <> X.NEWnumAcntTypeId

--                SELECT  *
--                FROM    #TempAccounts

                UPDATE  dbo.Chart_Of_Accounts
                SET     numAcntTypeId = X.NEWnumAcntTypeId
                FROM    #TempAccounts X
                WHERE   X.numAccountID = dbo.Chart_Of_Accounts.numAccountId

                DROP TABLE #TempAccounts
--
--
----- MAPPING CUSTOMER AND VENDOR RELATION WITH ITS DEFAULT COA.
--
--

                DECLARE @numCustCount INT ;

                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Customer' ;


                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Customer'
                                        ),
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        NULL,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0101010501'
                    END
                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Vendor' ;
                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Vendor'
                                        ),
                                        NULL,
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0102010201'
                    END
            END                                                    
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0),
		(4,'Invoice is not generated against diferred income bizDocs.',0),
		(5,'Qty is not available to add in deferred bizdoc.',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF ALL ITEMS WHITHIN SALES ORDER ARE FULFILLED(SHIPPED) OR NOT
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*)
			FROM  
			(
				SELECT 
					OBDI.numOppItemID,
					(ISNULL(SUM(OBDI.numUnitHour),0) - ISNULL(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				CROSS APPLY
				(
					SELECT 
						SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						numOppId=@numOppID
						AND ISNULL(numDeferredBizDocID,0) > 0
						AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID

				) AS TEMPInvoiceAgainstDeferred
				WHERE
					numOppId=@numOppID 
					AND numBizDocId=304
				GROUP BY
					OBDI.numOppItemID
			) AS TEMP
			WHERE
				intQtyRemaining > 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 4
		END

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS intQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
						AND (ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocID = 304)
				) AS TempQtyLeftForDifferedIncome
				WHERE
					OI.numOppID = @numOppID
			) X
			WHERE
				X.OrderedQty <> X.intQty) = 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 5
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP
END

--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              

DECLARE @listIds VARCHAR(MAX)
SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcEmailID=@UserName and vcPassword=@vcPassword

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId >0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			
			IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
			BEGIN
				INSERT INTO OpportunityBizDocs
					   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
						[bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID])
				SELECT @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
						@bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId
				FROM OpportunityBizDocs OBD WHERE numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId     
			END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID])
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue					@numOppQueueID = 0, -- numeric(18, 0)									@numDomainID = @numDomainID, -- numeric(18, 0)									@numOppId = @numOppID, -- numeric(18, 0)									@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)									@numOrderStatus = 0, -- numeric(18, 0)							@numUserCntID = @numUserCntID, -- numeric(18, 0)							@tintProcessStatus = 1, -- tinyint								@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
		BEGIN
			IF DATALENGTH(@strBizDocItems)>2
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END
		END
		ELSE
		BEGIN
			IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
			BEGIN
				DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

				SELECT TOP 1
					@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
				FROM 
					OpportunityBizDocs 
				WHERE 
					numOppId=@numOppId 
					AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
					AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
				ORDER BY
					numOppBizDocsId

				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				INNER JOIN
					(
						SELECT 
							OpportunityBizDocItems.numOppItemID,
							OpportunityBizDocItems.numUnitHour
						FROM 
							OpportunityBizDocs 
						JOIN 
							OpportunityBizDocItems 
						ON 
							OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
						WHERE 
							numOppId=@numOppId 
							AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
					) TempBizDoc
				ON
					TempBizDoc.numOppItemID = OI.numoppitemtCode
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


				UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
			END
			ELSE
			BEGIN
		   		INSERT INTO OpportunityBizDocItems                                                                          
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc 
						WHEN 1 
						THEN 1 
						ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
					END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				OUTER APPLY
					(
						SELECT 
							SUM(OBDI.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs OBD 
						JOIN 
							dbo.OpportunityBizDocItems OBDI 
						ON 
							OBDI.numOppBizDocId  = OBD.numOppBizDocsId
							AND OBDI.numOppItemID = OI.numoppitemtCode
						WHERE 
							numOppId=@numOppId 
							AND 1 = (CASE 
										WHEN @bitAuthBizdoc = 1 
										THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
										WHEN @numBizDocId = 304 --Deferred Income
										THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
									END)
					) TempBizDoc
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
			END
		END


               
--		insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--		select @numOppBizDocsId,numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,numTaxItemID,@numOppId,0) from TaxItems
--        where numDomainID= @numDomainID
--        union 
--        select @numOppBizDocsId,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,0)

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = Getdate(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014		--Purpose :MAke entry in WFA queue 		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue						@numOppQueueID = 0, -- numeric(18, 0)										@numDomainID = @numDomainID, -- numeric(18, 0)										@numOppId = @numOppID, -- numeric(18, 0)										@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)										@numOrderStatus = 0, -- numeric(18, 0)								@numUserCntID = @numUserCntID, -- numeric(18, 0)								@tintProcessStatus = 1, -- tinyint									@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity numeric(18,10),
			Notes varchar(500),
			monPrice MONEY,
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity numeric(18,10),
			Notes varchar(500),
			monPrice MONEY
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost money,
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Delete')
DROP PROCEDURE dbo.USP_CustomQueryReport_Delete
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Delete]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	DELETE FROM CustomQueryReport WHERE numReportID = @numReportID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_EmailReport')
DROP PROCEDURE dbo.USP_CustomQueryReport_EmailReport
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_EmailReport]

AS 
BEGIN
	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1),
		numReportID NUMERIC(18,0),
		numReportDomainID NUMERIC(18,0),
		vcReportName VARCHAR(300),
		vcEmailTo VARCHAR(1000),
		tintEmailFrequency TINYINT,
		vcCSS VARCHAR(MAX),
		dtCreatedDate DATETIME
	)	

	DECLARE @TempFinal TABLE
	(
		numReportID NUMERIC(18,0),
		numReportDomainID NUMERIC(18,0),
		vcReportName VARCHAR(300),
		vcEmailTo VARCHAR(1000),
		tintEmailFrequency TINYINT,
		vcCSS VARCHAR(MAX),
		dtDateToSend DATE
	)	

	INSERT INTO @Temp
	(
		numReportID,
		numReportDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate
	)
	SELECT
		numReportID,
		numDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate
	FROM	
		CustomQueryReport
	WHERE
		LEN(ISNULL(vcEmailTo,'')) > 0
		AND ISNULL(tintEmailFrequency,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	DECLARE @bitAddForEmail AS BIT = 0
	DECLARE @numReportID AS NUMERIC(18,0)
	DECLARE @numReportDomainID AS NUMERIC(18,0)
	DECLARE @dtCreatedDate AS DATE
	DECLARE @dtFiscalStartDate AS DATE
	DECLARE @tintEmailFrequency AS TINYINT
	DECLARE @dtDateToSend AS DATE
	

	SELECT @COUNT=COUNT(*) FROM @Temp

	WHILE @i <= @COUNT
	BEGIN
		SET @bitAddForEmail = 0

		SELECT 
			@numReportID=numReportID, 
			@numReportDomainID=numReportDomainID, 
			@tintEmailFrequency=tintEmailFrequency,
			@dtCreatedDate=CAST(dtCreatedDate AS DATE)
		FROM 
			@Temp 
		WHERE 
			ID=@i

		SELECT @dtFiscalStartDate = CAST(dbo.GetFiscalStartDate(YEAR(GETDATE()),@numReportDomainID) AS DATE)

		IF @tintEmailFrequency = 1 --DAILY
		BEGIN
			SELECT @dtDateToSend = CAST(GETDATE() AS DATE)

			IF (SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = (CAST(GETDATE() AS DATE))) = 0
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 2 --WEEKLY
		BEGIN
			SELECT @dtDateToSend = CAST(dbo.get_week_start(GETDATE()) AS DATE)
			
			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 3 --MONTHLY
		BEGIN
			SELECT @dtDateToSend = CAST(dbo.get_month_start(GETDATE()) AS DATE)

			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 4 --YEARLY
		BEGIN
			SELECT @dtDateToSend = @dtFiscalStartDate

			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END

		IF @bitAddForEmail = 1
		BEGIN
			INSERT INTO 
				@TempFinal
			SELECT
				numReportID,
				numReportDomainID,
				vcReportName,
				vcEmailTo,
				tintEmailFrequency,
				ISNULL(vcCSS,''),
				@dtDateToSend
			FROM
				@Temp
			WHERE
				ID = @i
		END

		SET @i = @i + 1
	END

	SELECT * FROM @TempFinal
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Execute')
DROP PROCEDURE dbo.USP_CustomQueryReport_Execute
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Execute]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @vcQuery AS VARCHAR(MAX)

	SELECT @vcQuery = CAST(vcQuery AS VARCHAR(MAX)) FROM CustomQueryReport WHERE numReportID = @numReportID

	IF LEN(@vcQuery) > 0
	BEGIN
		EXEC (@vcQuery)
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetAll')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetAll
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetAll]

AS 
BEGIN
	SELECT 
		CustomQueryReport.numReportID,
		CustomQueryReport.numDomainID,
		Domain.vcDomainName,
		CustomQueryReport.vcReportName,
		CustomQueryReport.vcReportDescription,
		CustomQueryReport.vcEmailTo,
		(CASE CustomQueryReport.tintEmailFrequency WHEN 1 THEN 'Daily' WHEN 2 THEN 'Weekly' WHEN 3 THEN 'Monthly' WHEN 4 THEN 'Yearly' END) As vcEmailFrequency
	FROM 
		CustomQueryReport
	INNER JOIN
		Domain
	ON
		CustomQueryReport.numDomainID = Domain.numDomainId
	ORDER BY
		Domain.vcDomainName ASC
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetByDomainID')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetByDomainID
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetByDomainID]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		tintEmailFrequency
	FROM 
		CustomQueryReport
	WHERE
		numDomainID=@numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetByID')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetByID
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetByID]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		vcEmailTo,
		tintEmailFrequency,
		vcQuery,
		vcCSS
	FROM 
		CustomQueryReport
	WHERE
		numReportID=@numReportID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_LogEmailNotification')
DROP PROCEDURE dbo.USP_CustomQueryReport_LogEmailNotification
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_LogEmailNotification]
	@numReportID NUMERIC(18,0),
	@tintEmailFrequency TINYINT,
	@dtDateToSend DATE
AS 
BEGIN
	INSERT INTO CustomQueryReportEmail
	(
		numReportID,
		tintEmailFrequency,
		dtDateToSend,
		dtSentDate
	)
	VALUES
	(
		@numReportID,
		@tintEmailFrequency,
		@dtDateToSend,
		CAST(GETDATE() AS DATE)
	)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Save')
DROP PROCEDURE dbo.USP_CustomQueryReport_Save
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Save]
	@numReportID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@vcReportName VARCHAR(300),
	@vcReportDescription VARCHAR(300),
	@vcEmailTo VARCHAR(1000),
	@tintEmailFrequency TINYINT,
	@vcQuery TEXT,
	@vcCSS VARCHAR(MAX)
AS 
BEGIN
	IF ISNULL(@numReportID,0) = 0 --INSERT
	BEGIN
		INSERT INTO CustomQueryReport
		(
			numDomainID,
			vcReportName,
			vcReportDescription,
			vcEmailTo,
			tintEmailFrequency,
			vcQuery,
			vcCSS,
			dtCreatedDate
		)
		VALUES
		(
			@numDomainID,
			@vcReportName,
			@vcReportDescription,
			@vcEmailTo,
			@tintEmailFrequency,
			@vcQuery,
			@vcCSS,
			GETDATE()
		)
	END
	ELSE --UPDATE
	BEGIN
		UPDATE
			CustomQueryReport
		SET
			numDomainID=@numDomainID,
			vcReportName=@vcReportName,
			vcReportDescription=@vcReportDescription,
			vcEmailTo=@vcEmailTo,
			tintEmailFrequency=@tintEmailFrequency,
			vcQuery=@vcQuery,
			vcCSS=@vcCSS
		WHERE	
			numReportID=@numReportID
	END		
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_SaveEmailChanges')
DROP PROCEDURE dbo.USP_CustomQueryReport_SaveEmailChanges
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_SaveEmailChanges]
	@numReportID NUMERIC(18,0),
	@vcReportName VARCHAR(300),
	@vcReportDescription VARCHAR(300),
	@vcEmailTo VARCHAR(1000)
AS 
BEGIN
	UPDATE
		CustomQueryReport
	SET
		vcReportName=@vcReportName,
		vcReportDescription=@vcReportDescription,
		vcEmailTo=@vcEmailTo
	WHERE	
		numReportID=@numReportID	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWarehouseItem')
DROP PROCEDURE USP_DeleteWarehouseItem
GO
CREATE PROCEDURE USP_DeleteWarehouseItem
  @numWareHouseItmsDTLID NUMERIC(9),
  @numWareHouseItemID NUMERIC(9)
AS 
    BEGIN
--        DELETE  FROM WareHouseItmsDTL
--        WHERE   numWareHouseItmsDTLID = @numWareHouseItmsDTLID

 DECLARE @Total INT

 SELECT @Total=numQty - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON w.numOppId=opp.numOppId where ISNULL(opp.tintOppType,0) <> 2 AND 1 = (CASE WHEN ISNULL(opp.bitStockTransfer,0) = 1 THEN CASE WHEN ISNULL(w.bitTransferComplete,0) = 0 THEN 1 ELSE 0 END ELSE 1 END) AND w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) FROM WareHouseItmsDTL 
        WHERE  numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID
     
    
                IF (SELECT numOnHand=numOnHand - @Total FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID) >=0
                    Begin
                     update WareHouseItems SET numOnHand=numOnHand - @Total,dtModified = GETDATE()  where numWareHouseItemID = @numWareHouseItemID

                     DELETE FROM WareHouseItmsDTL WHERE  numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID
                     
                     SELECT 1
                    END
                ELSE 
                     SELECT 0 	
     END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetIDName')
DROP PROCEDURE dbo.USP_Domain_GetIDName
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetIDName]
	
AS 
BEGIN
	SELECT 
		numDomainID,
		vcDomainName 
	FROM 
		Domain
	WHERE
		ISNULL(numDomainID,0) > 0
	ORDER BY
		vcDomainName ASC
END
/****** Object:  StoredProcedure [dbo].[usp_GenDocDetails]    Script Date: 07/26/2008 16:16:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendocdetails')
DROP PROCEDURE usp_gendocdetails
GO
CREATE PROCEDURE [dbo].[usp_GenDocDetails]                          
(                          
@byteMode as tinyint=null,                          
@GenDocId as numeric(9)=null,                
@numUserCntID as numeric(9)=0 ,    
@numDomainID numeric(9),
@ClientTimeZoneOffset Int                          
)                          
as                          
if @byteMode=0                          
begin                          
select                           
 numGenericDocId,                          
 VcFileName ,                          
 vcDocName ,                          
 numDocCategory,                          
 vcfiletype,                          
 numDocStatus,                          
 vcDocdesc,                        
 vcSubject,                         
 ISNULL(cUrlType,'') cUrlType,
numCreatedBy,       
case when ISNULL(vcDocumentSection,'') ='M' then 'true' else 'false' end as isForMarketingDept,
 dbo.fn_GetContactName(numCreatedBy) as RecordOwner,                          
 dbo.fn_GetContactName(numCreatedBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as Created ,                          
 dbo.fn_GetContactName(numModifiedBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as Modified,
 (select count(*) from DocumentWorkflow where numDocID=@GenDocId and numContactID=@numUserCntID and cDocType='D' and tintApprove=0) as AppReq ,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=1) as Approved,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=2) as Declined,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=0) as Pending,
 ISNULL(tintCheckOutStatus,0) tintCheckOutStatus,
 ISNULL(numLastCheckedOutBy,0) numLastCheckedOutBy,
 dbo.fn_GetContactName(numLastCheckedOutBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,dtCheckOutDate )) as CheckedOut,
 ISNULL(numModuleId,0) numModuleID,ISNULL(vcContactPosition,'') vcContactPosition
from GenericDocuments where numGenericDocId=@GenDocId  and    numDomainID = @numDomainID
              
end                          

if @byteMode=1                          
begin                          
                          
delete from DocumentWorkflow where numDocID=@GenDocId and cDocType='D'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@GenDocId and btDocType=2) 
delete from BizActionDetails where numOppBizDocsId =@GenDocId and btDocType=2


delete from GenericDocuments where numGenericDocId=@GenDocId  and numDomainID = @numDomainID                        
end          
                        
if @byteMode=2                          
begin                          
                          
select  numGenericDocId,                          
 VcFileName  from GenericDocuments where numGenericDocId=@GenDocId and    numDomainID = @numDomainID                      
end
GO
/****** Object:  StoredProcedure [dbo].[usp_GenDocManage]    Script Date: 07/26/2008 16:16:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendocmanage')
DROP PROCEDURE usp_gendocmanage
GO
CREATE PROCEDURE [dbo].[usp_GenDocManage]          
(          
 @numDomainID as numeric(9)=null,          
 @vcDocDesc as text='',          
 @VcFileName as varchar(100)='',          
 @vcDocName as varchar(100)='',          
 @numDocCategory as numeric(9)=null,          
 @vcfiletype as varchar(10)='',          
 @numDocStatus as numeric(9)=null,          
 @numUserCntID as numeric(9)=null,          
 @numGenDocId as numeric(9)=0 output,          
 @cUrlType as varchar(1)='',        
 @vcSubject as varchar(500)='' ,  
 @vcDocumentSection VARCHAR(10),
 @numRecID as numeric(9)=0,
 @tintDocumentType TINYINT=1,-- 1 = generic, 2= specific document
 @numModuleID NUMERIC =0,
 @vcContactPosition VARCHAR(200)=''
)          
as          

          
if @numGenDocId=0           
begin          

IF @numModuleID=0 SET @numModuleID=null

insert into GenericDocuments          
          
 (          
 numDomainID,          
 vcDocDesc,          
 VcFileName,          
 vcDocName,          
 numDocCategory,          
 vcfiletype,          
 numDocStatus,          
 numCreatedBy,          
 bintCreatedDate,          
 numModifiedBy,          
 bintModifiedDate,          
 cUrlType,        
 vcSubject,
 vcDocumentSection,
 numRecID,
 tintDocumentType,
 numModuleID,vcContactPosition
 )          
values          
 (          
 @numDomainID,          
 @vcDocDesc,          
 @VcFileName,          
 @vcDocName,          
 @numDocCategory,          
 @vcfiletype,          
 @numDocStatus,          
 @numUserCntID,           
 getutcdate(),          
 @numUserCntID,          
 getutcdate(),          
 @cUrlType,        
 @vcSubject,
 @vcDocumentSection,
 @numRecID,
 @tintDocumentType,
 @numModuleID,@vcContactPosition
 )          
          
set @numGenDocId=@@identity           
          
end          
          
else          
          
begin          
          
 IF @numModuleID = 0  SET @numModuleID = NULL;
 
 update  GenericDocuments          
           
 set           
  vcDocDesc=@vcDocDesc,          
  VcFileName=@VcFileName,          
  vcDocName=@vcDocName,          
  numDocCategory=@numDocCategory,          
  vcfiletype=@vcfiletype,
  numDocStatus=@numDocStatus,          
  numModifiedBy=@numUserCntID,          
  bintModifiedDate=getutcdate(),          
  cUrlType=@cUrlType,        
  vcSubject=@vcSubject,
  vcDocumentSection = @vcDocumentSection,--Bug ID 1149
  numModuleID= @numModuleID,vcContactPosition=@vcContactPosition
           
 where numGenericDocID = @numGenDocId          
          
end
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccessesReport]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallselectedsystemmodulespagesandaccessesreport')
DROP PROCEDURE usp_getallselectedsystemmodulespagesandaccessesreport
GO
CREATE PROCEDURE [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccessesReport]             
 @numModuleID NUMERIC(9) = 0,              
 @numGroupID NUMERIC(9) = 0                 
--              
AS              
  select * from ReportList where rptGroup=0 AND [bitEnable]=1 ORDER BY rptSequence
GO
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssociations]    Script Date: 03/05/2010 20:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Modified By: Debasish              
--Modified Date: 30th Dec 2005              
--Purpose for Modification: To get the associations for the company              
-- EXEC usp_GetAssociations 91300 ,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getassociations')
DROP PROCEDURE usp_getassociations
GO
CREATE PROCEDURE [dbo].[usp_GetAssociations]
    @numDivID AS NUMERIC(9),
    @bitGetAssociatedTo BIT = 1,
	@numAssociationId AS NUMERIC(9)=0,
	@numDominId AS NUMERIC(9)=0
AS 
    BEGIN
		IF @numAssociationID > 0
		BEGIN
		     SELECT CA.*, CMP.vcCompanyName FROM [dbo].[CompanyAssociations] AS CA 
			 LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
			 WHERE [CA].[numAssociationID]=@numAssociationID AND [CA].[numDomainID]=@numDominId
		END			                
        ELSE IF @bitGetAssociatedTo = 1 
            BEGIN                    
				
                SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster,
                                    CompanyInfo
                          WHERE     DivisionMaster.numDivisionID = CA.numDivisionID
                                    AND DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                        ) AS TargetCompany,
                        ACI.numContactID,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        LD.vcData,
                        LD1.vcData AS Relationship,
                        LD2.vcData AS OrgProfile,
                        DIV.tintCRMType,
                        CASE WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             --WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						CAST(1 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numAssociateFromDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numAssociateFromDivisionID = @numDivID
                        AND CA.bitDeleted = 0

              
                
				union
				
						SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numAssociateFromDivisionID as numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster,
                                    CompanyInfo
                          WHERE     DivisionMaster.numDivisionID = CA.numAssociateFromDivisionID
                                    AND DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                        ) AS TargetCompany,
                        ACI.numContactID,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        LD.vcData,
                        LD1.vcData AS OrgProfile,
                        LD2.vcData AS Relationship ,
                        DIV.tintCRMType,
                        CASE --WHEN CA.bitParentOrg = 1 THEN 'Child'
                             WHEN CA.bitChildOrg = 1 THEN '(Parent)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						--CAST((CASE WHEN CA.bitChildOrg = 1 THEN 0 ELSE 1 END)AS BIT) 
						CAST(0 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numAssociateFromDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numDivisionID = @numDivID
                        AND CA.bitDeleted = 0 --AND isnull(CA.numAssoTypeLabel,0)<>0
                ORDER BY TargetCompany


                SELECT  CompanyInfo.vcCompanyName,
						DIV.[numDivisionID],
                        LD.vcData AS OrgProfile,
                        LD1.vcData AS Relationship,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        ACI.[numContactId],isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo
                FROM    DivisionMaster AS DIV
                        LEFT OUTER JOIN CompanyInfo ON DIV.numCompanyID = CompanyInfo.numCompanyId
                        LEFT OUTER JOIN ListDetails AS LD ON [CompanyInfo].[vcProfile] = LD.[numListItemID]
                        LEFT OUTER JOIN ListDetails AS LD1 ON CompanyInfo.numCompanyType = LD1.numListItemID
                        LEFT OUTER JOIN AdditionalContactsInformation AS ACI ON DIV.numDivisionID = ACI.[numDivisionId]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= DIV.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   DIV.[numDivisionID] = @numDivID
                        AND ACI.bitPrimaryContact = 1
           
            END       
        ELSE 
            BEGIN      
                SELECT  numAssociationID,
                        CMP.vcCompanyName AS Company,
                        LD.vcData
                FROM    CompanyAssociations CA,
                        CompanyInfo CMP,
                        DivisionMaster DIV,
                        ListDetails LD
                WHERE   CA.bitDeleted = 0
                        AND CA.numDivisionID = @numDivID
                        AND DIV.numDivisionID = CA.numAssociateFromDivisionID
                        AND CMP.numCompanyID = DIV.numCompanyID
                        AND LD.numListItemID = CA.numReferralType
                GROUP BY numAssociationID,
                        CMP.vcCompanyName,
                        DIV.vcDivisionName,
                        LD.vcData
                ORDER BY vcCompanyName,
                        vcDivisionName       
            END           
    END

/****** Object:  StoredProcedure [dbo].[USP_GetAuthoritativeOpportunityCount]    Script Date: 07/26/2008 16:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthoritativeopportunitycount')
DROP PROCEDURE usp_getauthoritativeopportunitycount
GO
CREATE PROCEDURE [dbo].[USP_GetAuthoritativeOpportunityCount]      
(
	@numDomainId as numeric(9)=0,      
	@numOppId as numeric(9)=0        
)
AS   
BEGIN
	DECLARE @tinyOppType AS TINYINT
	DECLARE @numAuthoritativeBizDocId AS NUMERIC(9)
	DECLARE @numBizDocsId AS NUMERIC(9)      
  
	SELECT @tinyOppType=ISNULL(tintOppType,0) FROM OpportunityMaster WHERE numOppId=@numOppId       
  
	IF @tinyOppType=1      
		SELECT @numAuthoritativeBizDocId=ISNULL(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainId      
	ELSE IF @tinyOppType=2
		SELECT @numAuthoritativeBizDocId=ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainId      
 
	SELECT 
		COUNT(*) 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppId=@numOppId 
		AND ((numBizDocId=@numAuthoritativeBizDocId AND ISNULL(bitAuthoritativeBizDocs,0)=1) OR numBizDocId=304) -- 304 - IS DEFERRED INCOME BIZDOC 
End
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBizDocTemplate' ) 
    DROP PROCEDURE USP_GetBizDocTemplate
GO
-- USP_GetBizDocTemplate 72,0,0
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplate]
    @numDomainID NUMERIC ,
    @tintTemplateType TINYINT ,
    @numBizDocTempID NUMERIC
AS 
    SET NOCOUNT ON    
    
    SELECT  [numDomainID] ,
            [numBizDocID] ,
            [txtBizDocTemplate] ,
            [txtCSS] ,
            ISNULL(bitEnabled, 0) AS bitEnabled ,
            [numOppType] ,
            ISNULL(vcTemplateName, '') AS vcTemplateName ,
            ISNULL(bitDefault, 0) AS bitDefault ,
            vcBizDocImagePath ,
            vcBizDocFooter ,
            vcPurBizDocFooter,
			numOrientation,
			bitKeepFooterBottom,
			ISNULL(numRelationship,0) AS numRelationship,
			ISNULL(numProfile,0) AS numProfile,
			ISNULL(bitDisplayKitChild,0) AS bitDisplayKitChild
    FROM    BizDocTemplate
    WHERE   [numDomainID] = @numDomainID
            AND tintTemplateType = @tintTemplateType
            AND ( numBizDocTempID = @numBizDocTempID
                  OR ( @numBizDocTempID = 0
                       AND @tintTemplateType != 0
                     )
                ) 
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
@numKitId as numeric(9)                            
as                            


WITH CTE(ID,numItemDetailID,numParentID,numItemKitID,numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,numWarehouseItmsID,txtItemDesc,numQtyItemsReq,numOppChildItemID,
charItemType,ItemType,StageLevel,monListPrice,numBaseUnit,numCalculatedQty,numIDUOMId,sintOrder,numRowNumber,RStageLevel)
AS
(
select CAST(CONCAT('#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000)),Dtl.numItemDetailID,CAST(0 AS NUMERIC(18,0)),convert(NUMERIC(18,0),0),numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,1,(CASE WHEN charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= Dtl.numWareHouseItemId) ELSE Item.monListPrice END) monListPrice,ISNULL(numBaseUnit,0),CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId,ISNULL(sintOrder,0) sintOrder,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber,0
from item                                
INNER join ItemDetails Dtl on numChildItemID=numItemCode
where  numItemKitID=@numKitId 

UNION ALL

select CAST(CONCAT(c.ID,'-#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000)),Dtl.numItemDetailID,C.numItemDetailID,Dtl.numItemKitID,i.numItemCode,i.vcItemName,i.monAverageCost,i.numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when i.charItemType='P' then 'Inventory Item' when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory Item' end as charItemType,i.charItemType as ItemType                            
,c.StageLevel + 1,(CASE WHEN i.charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= dtl.numWareHouseItemId) ELSE i.monListPrice END) monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId,ISNULL(Dtl.sintOrder,0) sintOrder,
ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber,0
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
where Dtl.numChildItemID!=@numKitId
)

SELECT * INTO #temp FROM CTE

;WITH Final AS (SELECT *,1 AS RStageLevel1 FROM #temp WHERE numitemcode NOT IN (SELECT numItemKitID FROM #temp)

UNION ALL

SELECT t.*,c.RStageLevel1 + 1 AS RStageLevel1 FROM  #temp t JOIN Final c ON t.numitemcode=c.numItemKitID
)

--SELECT DISTINCT * FROM Total ORDER BY StageLevel

UPDATE t set t.RStageLevel=f.RStageLevel from 
#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
WHERE t.numitemcode=f.numitemcode AND t.numItemKitID=f.numItemKitID 

--SELECT * FROM #temp

SELECT DISTINCT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,UOM.vcUnitName,ISNULL(UOM.numUOMId,0) numUOMId,
WI.numItemID,vcWareHouse,IDUOM.vcUnitName AS vcIDUnitName,
dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor,
c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty,
(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired,
c.sintOrder,
ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID,ISNULL(numOnHand,0) numOnHand,isnull(numOnOrder,0) numOnOrder,isnull(numReorder,0) numReorder,isnull(numAllocation,0) numAllocation,isnull(numBackOrder,0) numBackOrder
FROM #temp c  
LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=c.numIDUOMId
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID ORDER BY c.StageLevel

--SELECT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
--convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,
--WI.numItemID,vcWareHouse
-- FROM CTE c  
--LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
--LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
--LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID



GO
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10
        OR @FormId = 44
        SET @PageId = 4          
    ELSE
        IF @FormId = 11
            OR @FormId = 34
            OR @FormId = 35
            OR @FormId = 36
            OR @FormId = 96
            OR @FormId = 97
            SET @PageId = 1          
        ELSE
            IF @FormId = 12
                SET @PageId = 3          
            ELSE
                IF @FormId = 13
                    SET @PageId = 11       
                ELSE
                    IF ( @FormId = 14
                         AND ( @numtype = 1
                               OR @numtype = 3
                             )
                       )
                        OR @FormId = 33
                        OR @FormId = 38
                        OR @FormId = 39
                        SET @PageId = 2          
                    ELSE
                        IF @FormId = 14
                            AND ( @numtype = 2
                                  OR @numtype = 4
                                )
                            OR @FormId = 40
                            OR @FormId = 41
                            SET @PageId = 6  
                        ELSE
                            IF @FormId = 21
                                OR @FormId = 26
                                OR @FormId = 32
								OR @FormId = 126
                                SET @PageId = 5   
                            ELSE
                                IF @FormId = 22
                                    OR @FormId = 30
                                    SET @PageId = 5    
                                ELSE
                                    IF @FormId = 76
                                        SET @PageId = 10 
                                    ELSE
                                        IF @FormId = 84
                                            OR @FormId = 85
                                            SET @PageId = 5
                     
    IF @PageId = 1
        OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE IF @PageId = 10
    BEGIN          
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
	                                     
            END 
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END

        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
             
            ELSE
                BEGIN          
      
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1
        OR @PageId = 4
        BEGIN
 
            IF ( @FormId = 96
                 OR @FormId = 97
               )
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE
        IF @PageId = 10
            BEGIN
	
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
   --and grp_id=@PageId    
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
            END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN

                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 

                        END



    

                END
            ELSE
                BEGIN  
 
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = @PageId
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetEmailTemplateContactPostionEmail')
DROP PROCEDURE usp_GetEmailTemplateContactPostionEmail
GO
CREATE PROCEDURE [dbo].[usp_GetEmailTemplateContactPostionEmail]                          
(                          
@GenDocId as numeric(9)=null,                
@numDomainID as numeric(9),
@numDivID AS numeric(9)                      
)                          
as       

DECLARE @vcContactPosition AS VARCHAR(200)
SELECT @vcContactPosition = ISNULL(vcContactPosition ,'')
FROM GenericDocuments where numGenericDocId=@GenDocId and numDomainID = @numDomainID
	
SELECT [A].[vcEmail],[A].[numContactId],[A].[vcFirstName],[A].[vcLastName] FROM [dbo].[DivisionMaster] AS DM JOIN AdditionalContactsInformation A ON A.numDivisionId = DM.numDivisionID  
WHERE DM.[numDomainID]=@numDomainID AND dm.[numDivisionID]=@numDivID AND A.[vcPosition] IN(SELECT * FROM dbo.Split(@vcContactPosition ,','))
AND isnull([A].[vcEmail],'')!='' AND isnull([A].[vcFirstName],'')!='' AND isnull([A].[vcLastName],'')!=''

GO



/****** Object:  StoredProcedure [dbo].[USP_GetItemGroups]    Script Date: 07/26/2008 16:17:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemgroups')
DROP PROCEDURE usp_getitemgroups
GO
CREATE PROCEDURE [dbo].[USP_GetItemGroups]    
@numDomainID as numeric(9)=0,    
@numItemGroupID as numeric(9)=0,
@numItemCode as numeric(9)=0    
 
as    
    
if @numItemGroupID=0    
begin    
	IF @numItemCode>0
		BEGIN
			Select @numItemGroupID=numItemGroup FROM Item WHERE numItemCode=@numItemCode
			
			  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
on numOppAccAttrID=numItemCode  
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit
                  
where  Dtl.numItemGroupID=@numItemGroupID and tintType=1 
			
		END
	ELSE
	BEGIN
		select numItemGroupID, vcItemGroup, numDomainID from ItemGroups     
		where numDomainID=@numDomainID    		
	END
end    
else if @numItemGroupID>0     
begin    
 select numItemGroupID, vcItemGroup, numDomainID from ItemGroups     
 where numDomainID=@numDomainID and numItemGroupID=@numItemGroupID    
  
--select numItemCode,vcItemName,txtItemDesc,monListPrice from Item     
--  join ItemGroupsDTL    
--  on numOppAccAttrID=numItemCode    
--  where numItemGroupID=@numItemGroupID and tintType=1    
  
  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
on numOppAccAttrID=numItemCode     
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit

where  numItemGroupID=@numItemGroupID and tintType=1 

select Fld_id,Fld_label from CFW_Fld_Master     
  join ItemGroupsDTL    
  on numOppAccAttrID=Fld_id    
  where numItemGroupID=@numItemGroupID and tintType=2  
   
  
    
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	select numItemCode,vcItemName from Item where numDomainID =@numDomainID  and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF (SELECT ISNULL(bitAssembly,0) FROM Item  WHERE numItemCode = @numItemCode) = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     ((SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0 OR ISNULL(bitAssembly,0) = 1)) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
	END
	ELSE
	BEGIN
		--ONLY NON INVENTORY, SERVICE, INVENTORY ITEMS AND KIT ITEMS WITHOUT HAVING ANOTHER KIT AS SUB ITEM WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY

		SELECT 
			numItemCode,
			vcItemName,
			bitKitParent,
			bitAssembly 
		FROM 
			Item 
		WHERE 
			numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
			AND numItemCode <> @numItemCode
			AND 1= (
					CASE 
						WHEN charitemtype='P' THEN
						CASE 
							WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode)>0 
							THEN 1 
							ELSE 0 
						END 
						ELSE 1 
					END)
			AND 1 = (CASE 
						WHEN Item.bitKitParent = 1 
						THEN 
							(
								CASE 
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item I ON ItemDetails.numChildItemID = I.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(I.bitKitParent,0) = 1) > 0
									THEN 0
									ELSE 1
								END
							)
						ELSE 1 
					END)
	END
END				
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocItems' ) 
    DROP PROCEDURE USP_GetMirrorBizDocItems
GO
--- EXEC USP_GetMirrorBizDocItems 53,7,1
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocItems]
    (
      @numReferenceID NUMERIC(9) = NULL,
      @numReferenceType TINYINT = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 

  IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
    			EXEC USP_MirrorOPPBizDocItems @numReferenceID,@numDomainID
END
ELSE
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    DECLARE @tintReturnType AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    
    SELECT  @DivisionID = numDivisionID,@tintReturnType=tintReturnType
    FROM    dbo.ReturnHeader
    WHERE   numReturnHeaderID = @numReferenceID                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
    SET @numBizDocTempID = 0
        
    IF @numReferenceType = 5 OR @numReferenceType = 6 
        BEGIN
            SELECT  @numBizDocTempID = ISNULL(numRMATempID, 0)
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReferenceID                       
        END 
    ELSE IF @numReferenceType = 7 OR @numReferenceType = 8  OR @numReferenceType = 9  OR @numReferenceType = 10 
            BEGIN
                SELECT  @numBizDocTempID = ISNULL(numBizdocTempID, 0)
                FROM    dbo.ReturnHeader
                WHERE   numReturnHeaderID = @numReferenceID
            END  
	
    PRINT 'numBizdocTempID : ' + CONVERT(VARCHAR(10), @numBizdocTempID)
    SELECT  @numBizDocId = numBizDocId
    FROM    dbo.BizDocTemplate
    WHERE   numBizDocTempID = @numBizDocTempID
            AND numDomainID = @numDomainID
      
     IF @numReferenceType = 5 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 6 
	 BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		
		SET @tintType = 8   
	 END
	 ELSE IF @numReferenceType = 7 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 8
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		
		SET @tintType = 8 
	 END
	 ELSE IF @numReferenceType = 9
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 10
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
														  

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT    numReturnItemID,I.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        RI.vcItemDesc AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
--                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
--                                             i.numItemCode, @numDomainID,
--                                             ISNULL(RI.numUOMId, 0))
--                        * RI.numUnitHour AS numUnitHour,
						CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour,
                       RI.monPrice,
					   (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END)  As monUnitSalePrice,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS  Amount,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                         * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS monTotAmount/*Fo calculating sum*/,
                        RH.monTotalTax AS [Tax],
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        RI.numItemCode AS [numoppitemtCode],
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(i.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(RI.numWarehouseItemID,
                                              bitSerialized) AS vcAttributes,
                        '' AS vcPartNo,
                        ( ISNULL(RH.monTotalDiscount, RI.monTotAmount)
                          - RI.monTotAmount ) AS DiscAmt,
                        '' AS vcNotes,
                        '' AS vcTrackingNo,
                        '' AS vcShippingMethod,
                        0 AS monShipCost,
                        [dbo].[FormatedDateFromDate](RH.dtCreatedDate,
                                                     @numDomainID) dtDeliveryDate,
                        RI.numWarehouseItemID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(RI.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        '' AS vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        0 AS DropShip,
                        ISNULL(RI.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(RI.numUOMId, RI.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        i.numShipClass,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalReturnDate,
                       		SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
              FROM      dbo.ReturnHeader RH
                        JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                        LEFT JOIN dbo.Item i ON RI.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = RI.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
                        LEFT JOIN dbo.WareHouseItems WI ON RI.numWarehouseItemID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     RH.numReturnHeaderID = @numReferenceID
            ) X


	--SELECT * FROM #Temp1
	
    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000)
    DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SET @strSQLUpdate = ''
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID

    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintReturnType = 1
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2)*Amount/100'
    ELSE 
        IF @tintReturnType = 1
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
        ELSE 
            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'

    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numReferenceID) + ',numReturnItemID,2)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END
	
    PRINT 'QUERY  :' + @strSQLUpdate
	
    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN  
            PRINT @vcDbColumnName  
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END 

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
    
    END
GO
--- EXEC USP_GetMirrorBizDocItems 30,5,1
--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
BEGIN

	DECLARE @numSalesAuthoritativeBizDocID AS INT
	SELECT @numSalesAuthoritativeBizDocID = numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID


    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                '' AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                GETDATE() AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                SELECT  OBDI.numOppItemID
                                FROM    OpportunityBizDocs OBD
                                        JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                WHERE   OBD.numOppId = @numOppID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OM.bintCreatedDate,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
        ELSE 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                '' AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
                                LEFT JOIN 
									OpportunityBizDocItems OBI 
								ON 
									OBI.numOppItemID = OI.numoppitemtCode 
                                    AND OBI.numOppBizDocId IN ( SELECT  
																	numOppBizDocsId
                                                                FROM    
																	OpportunityBizDocs OB
                                                                WHERE   
																	OB.numOppId = OI.numOppId
                                                                    AND 1 = (CASE 
																				WHEN @numBizDocId = @numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
																				THEN 
																					(CASE WHEN numBizDocId = 304 OR (ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 AND ISNULL(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
																				WHEN  @numBizDocId = 304
																				THEN (CASE WHEN ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
																				ELSE 
																					(CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
																				END) 
																)
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
    ELSE 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    ISNULL(vcNotes, '') AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip
        END
        ELSE 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) - ISNULL(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    ISNULL(vcNotes, '') AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
					(
						SELECT
							SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs
						INNER JOIN
							OpportunityBizDocItems 
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						WHERE
							numOppId = @numOppID
							AND numOppBizDocID <> @numOppBizDocID
							AND	1 = (CASE 
									WHEN @numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
									THEN (CASE WHEN (numBizDocId = @numSalesAuthoritativeBizDocID OR numBizDocId=304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
									ELSE (CASE WHEN numBizDocId = @numBizDocID THEN 1 ELSE 0 END)
									END) 
							AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					) TEMPDeferredIncomeItem
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,
					TEMPDeferredIncomeItem.numUnitHour
            UNION
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                '' AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                1 AS monShipCost,
                                NULL dtDeliveryDate,
                                CONVERT(BIT, 0) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OBI.numUnitHour,
                                OI.[numQtyShipped],
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(300),bitDropShip BIT,numUnitHour INT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					0,OI.numUnitHour,ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END

            
					--AND 1 = (CASE 
					--			WHEN @tintOppType = 1 
					--			THEN (CASE WHEN ISNULL(I.bitKitParent,0) = 0 THEN 1 ELSE 0 END)
					--			ELSE 1
					--		END)
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT,
	  @SortCreatedDate TINYINT
    )
AS 
    BEGIN
		
		DECLARE @TEMP TABLE
		(
			Items INT
		)

		INSERT INTO @TEMP SELECT Items FROM dbo.Split(@vcTransactionType,',')

		DECLARE @TempTransaction TABLE(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate DATETIME,numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount MONEY,monPaidAmount MONEY,dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=1)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 1,'Sales Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=2)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 2,'SO Invoice',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=3)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 3,'Sales Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=4)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 4,'Purchase Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=5)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 5,'PO Bill',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=6)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 6,'Purchase Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=7)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 7,'Sales Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=8)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 8,'Purchase Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=9)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 9,'Credit Memo',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=10)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 10,'Refund Receipt',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=11)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 11,'Bills',BH.dtCreatedDate,BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=16)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 16,'Landed Cost',BH.dtCreatedDate,BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=12)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',BPH.dtCreateDate,BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=13)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 13,'Checks',CH.dtCreatedDate,CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=14)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 14,'Deposits',DM.dtCreationDate,DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=15)
		 BEGIN
			INSERT INTO @TempTransaction 
		 	SELECT 15,'Unapplied Payments',DM.dtCreationDate,DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   @TempTransaction)
         
		 IF @SortCreatedDate = 1 -- Ascending 
		 BEGIN
			SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate ASC) AS RowNumber FROM @TempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType, CAST(dtCreatedDate AS DATE) ASC
		 END
		 ELSE --By Default Descending
		 BEGIN
			 SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate DESC) AS RowNumber FROM @TempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType, CAST(dtCreatedDate AS DATE) DESC 
         END
    END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
@numDomainID as numeric(9),
@numUserCntID numeric(9)=0,                                                                                                               
@SortChar char(1)='0',                                                            
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),
 @Filter AS VARCHAR(30),
 @FilterBy AS int
as


--Create a Temporary table to hold data                                                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numoppitemtCode numeric(9),
    numOppID  numeric(9),
     vcPOppName varchar(200),
    vcItemName varchar(500),
    vcModelID varchar(200),
    numUnitHour numeric(9),
    numUnitHourReceived numeric(9),
    numOnHand numeric(9),
	numOnOrder  numeric(9),
	numAllocation  numeric(9),
	numBackOrder  numeric(9),
	vcWarehouse varchar(200),
	dtDueDate VARCHAR(25),bitSerialized BIT,numWarehouseItmsID NUMERIC(9),bitLotNo BIT,SerialLotNo VARCHAR(1000),
	fltExchangeRate float,numCurrencyID numeric(9),numDivisionID numeric(9),
	itemIncomeAccount numeric(9),itemInventoryAsset numeric(9),itemCoGs numeric(9),
	DropShip BIT,charItemType CHAR(1),ItemType VARCHAR(50),
	numProjectID numeric(9),numClassID numeric(9),numItemCode numeric(9),monPrice MONEY,bitPPVariance bit,Vendor varchar(max))
	
declare @strSql as varchar(8000)                                                              
set @strSql='Select numoppitemtCode,Opp.numOppID,vcPOppName,OI.vcItemName + '' - ''+ isnull(dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),'''') as vcItemName ,OI.vcModelID,numUnitHour,isnull(numUnitHourReceived,0) as numUnitHourReceived,
isnull(numOnHand,0) as numOnHand,isnull(numOnOrder,0) as numOnOrder,isnull(numAllocation,0) as numAllocation,isnull(numBackOrder,0) as numBackOrder,vcWarehouse + '': '' + isnull(WL.vcLocation,'''') as  vcWarehouse,
[dbo].[FormatedDateFromDate](intPEstimatedCloseDate,'+convert(varchar(20),@numDomainID)+') AS dtDueDate,isnull(I.bitSerialized,0) as bitSerialized,OI.numWarehouseItmsID,isnull(I.bitLotNo,0) as bitLotNo
,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo,
ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID,ISNULL(Opp.numDivisionID,0) AS numDivisionID,
isnull(I.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(I.numAssetChartAcntId,0) as itemInventoryAsset,isnull(I.numCOGsChartAcntId,0) as itemCoGs,
isnull(OI.bitDropShip,0) as DropShip,I.charItemType,OI.vcType ItemType,ISNULL(OI.numProjectID, 0) numProjectID,ISNULL(OI.numClassID, 0) numClassID,
OI.numItemCode,OI.monPrice,isnull(Opp.bitPPVariance,0) as bitPPVariance,
ISNULL(com.vcCompanyName,''-'') as Vendor

from OpportunityMaster Opp
Join OpportunityItems OI On OI.numOppId=Opp.numOppId
Join Item I on I.numItemCode=OI.numItemCode
Left Join WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
Left Join Warehouses W on W.numWarehouseID=WI.numWarehouseID
LEFT join divisionmaster div on Opp.numDivisionId=div.numDivisionId 
LEFT join companyInfo com  on com.numCompanyid=div.numcompanyID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
where tintOppType=2 and tintOppstatus=1 and ISNULL(bitStockTransfer,0)=0 and tintShipped=0 and numUnitHour-isnull(numUnitHourReceived,0)>0  
AND I.[charItemType] IN (''P'',''N'',''S'') and (OI.bitDropShip=0 or OI.bitDropShip is null) and Opp.numDomainID='+convert(varchar(20),@numDomainID) 

--OI.[numWarehouseItmsID] IS NOT NULL
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      

IF @FilterBy=1 --Item
	set	@strSql=@strSql + ' and OI.vcItemName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=2 --Warehouse
	set	@strSql=@strSql + ' and W.vcWareHouse like ''%' + @Filter + '%'''
ELSE IF @FilterBy=3 --Purchase Order
	set	@strSql=@strSql + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=4 --Vendor
	set	@strSql=@strSql + ' and com.vcCompanyName LIKE ''%' + @Filter + '%'''		
ELSE IF @FilterBy=5 --BizDoc
	set	@strSql=@strSql + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'

If @columnName = 'vcModelID' 
	set @columnName='OI.vcModelID'
								
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder

PRINT @strSql

insert into #tempTable(numoppitemtCode,
    numOppID ,
    vcPOppName,
    vcItemName,
    vcModelID,
    numUnitHour,
    numUnitHourReceived,
    numOnHand,
	numOnOrder,
	numAllocation,
	numBackOrder,
	vcWarehouse,
	dtDueDate,bitSerialized,numWarehouseItmsID,bitLotNo,SerialLotNo,fltExchangeRate,numCurrencyID,numDivisionID,
	itemIncomeAccount,itemInventoryAsset,itemCoGs,DropShip,charItemType,ItemType,numProjectID,numClassID,numItemCode,monPrice,bitPPVariance,Vendor)                                                              
exec (@strSql) 


declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable) 


select *  from #tempTable where ID>@firstRec and ID < @lastRec

drop table #tempTable
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReceivePayment' ) 
    DROP PROCEDURE USP_GetReceivePayment
GO
CREATE PROCEDURE [dbo].USP_GetReceivePayment
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDepositID NUMERIC = 0,
    @numDivisionID NUMERIC(9) = 0,
    @numCurrencyID numeric(9),
    @numAccountClass NUMERIC(18,0)=0
   
AS 
    BEGIN
		
		DECLARE @numDiscountItemID AS NUMERIC(18,0)
		DECLARE @numShippingServiceItemID AS NUMERIC(18,0)

		SELECT @numDiscountItemID = ISNULL(numDiscountServiceItemID,0),@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainID = @numDomainID
		PRINT @numDiscountItemID
			
		DECLARE @BaseCurrencySymbol 
		nVARCHAR(3);SET @BaseCurrencySymbol='$'
		SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId)
    
		SELECT [T].[numDivisionID],T.bitChild,dbo.[fn_GetComapnyName](T.[numDivisionID]) AS vcCompanyName,AD2.[vcCity]
		,isnull(dbo.fn_GetState(AD2.numState),'') AS vcState
		INTO #tempOrg
		FROM (SELECT @numDivisionID AS numDivisionID,cast(0 AS BIT) AS bitChild
		UNION
		SELECT [CA].[numDivisionID],CAST(1 AS BIT) AS bitChild
		FROM [dbo].[CompanyAssociations] AS CA 
		WHERE [CA].[numDomainID]=@numDomainID AND  ca.[numAssociateFromDivisionID]=@numDivisionID AND CA.[bitChildOrg]=1) AS T
		LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=@numDomainID 
		AND AD2.numRecordID= T.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

		
		SELECT  om.vcPOppName ,
                om.numoppid ,
                OBD.[numoppbizdocsid] ,
                OBD.[vcbizdocid] ,
                OBD.[numbizdocstatus] ,
                OBD.[vccomments] ,
                OBD.monAmountPaid AS monAmountPaid ,
                OBD.monDealAmount ,
                OBD.monCreditAmount monCreditAmount ,
                ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
                [dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
                dtFromDate AS [dtOrigFromDate],
                CASE ISNULL(om.bitBillingTerms, 0)
                  WHEN 1
                  --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)), dtFromDate), 1)
                  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)), dtFromDate), @numDomainID)
                  WHEN 0
                  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
                END AS dtDueDate ,
                om.numDivisionID ,
                
                (OBD.monDealAmount - ISNULL(OBD.monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) + ISNULL(DD.monAmountPaid,0)  ) AS baldue ,
                T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
                DD.numDepositID ,
                DD.monAmountPaid monAmountPaidInDeposite ,
                ISNULL(DD.numDepositeDetailID,0) numDepositeDetailID,
                obd.dtCreatedDate,
                OM.numContactId,obd.vcRefOrderNo
                ,OM.numCurrencyID
               ,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
                ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
                ,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
                CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numDiscount],
				CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numDiscountPaidInDays],
				CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numNetDueInDays],
				CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						   JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						   WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						   AND OBDI.numItemCode = @numDiscountItemID
						   AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
				(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
				--(SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)	
        FROM    opportunitymaster om
                INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
                --INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
                --INNER JOIN CompanyInfo cmp ON cmp.numCompanyID = DM.numCompanyID
				INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
                LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
                LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
                JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
        WHERE   
        OBD.[bitauthoritativebizdocs] = 1
                AND om.tintOppType = 1
                AND om.numDomainID = @numDomainID
                --AND ( om.numDivisionID = @numDivisionID
                --      OR @numDivisionID = 0
                --    )
				AND DD.numDepositID = @numDepositID
				AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	UNION
        SELECT  om.vcPOppName ,
                om.numoppid ,
                OBD.[numoppbizdocsid] ,
                OBD.[vcbizdocid] ,
                OBD.[numbizdocstatus] ,
                OBD.[vccomments] ,
                OBD.monAmountPaid AS monAmountPaid ,
                OBD.monDealAmount ,
                OBD.monCreditAmount monCreditAmount ,
                ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
                [dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
                dtFromDate AS [dtOrigFromDate],
                CASE ISNULL(om.bitBillingTerms, 0)
                  WHEN 1
                  --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
                  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
                  WHEN 0
                  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
                END AS dtDueDate ,
                om.numDivisionID ,
                (OBD.monDealAmount - ISNULL(monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) ) AS baldue ,
                T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
                0 numDepositID ,
                0 monAmountPaidInDeposite ,
                0 numDepositeDetailID,
                dtCreatedDate,
                OM.numContactId,obd.vcRefOrderNo
                ,OM.numCurrencyID
                ,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
                ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
                ,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
			    CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numDiscount],
				CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numDiscountPaidInDays],
				CASE ISNULL(om.bitBillingTerms, 0)
					WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
					ELSE 0
				END AS [numNetDueInDays],
				CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						   JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						   WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						   AND OBDI.numItemCode = @numDiscountItemID
						   AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
				(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
        FROM    opportunitymaster om
                INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
				INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
                --INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
                --INNER JOIN CompanyInfo cmp ON cmp.numCompanyID = DM.numCompanyID
                LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
                LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
                
--                LEFT JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
        WHERE   OBD.[bitauthoritativebizdocs] = 1
                AND om.tintOppType = 1
                AND om.numDomainID = @numDomainID
				AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
                --AND ( om.numDivisionID = @numDivisionID
                --      OR @numDivisionID = 0
                --    )
                AND OBD.monDealAmount - OBD.monAmountPaid > 0
                AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositID)
                AND OBD.numOppBizDocsId NOT IN ( SELECT numDocID FROM dbo.DocumentWorkflow D WHERE D.cDocType='B' AND ISNULL(tintApprove,0) = 0 /*discard invoice which are Pending to approve */ )
                AND (OM.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
                AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
--         AND obd.numOppBizDocsId=42040
				ORDER BY T.bitChild,dtCreatedDate DESC
             
             
	
	DROP TABLE #tempOrg
	
    END


/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @SortCol VARCHAR(50),
      @SortDirection VARCHAR(4),
      @CurrentPage INT,
      @PageSize INT,
      @TotRecs INT OUTPUT,
      @numUserCntID NUMERIC,
      @vcBizDocStatus VARCHAR(500),
      @vcBizDocType varchar(500),
      @numDivisionID as numeric(9)=0,
      @ClientTimeZoneOffset INT=0,
      @bitIncludeHistory bit=0,
      @vcOrderStatus VARCHAR(500),
      @vcOrderSource VARCHAR(1000),
	  @bitFulfilled BIT,
	  @numOppID AS NUMERIC(18,0) = 0
     )
AS 
    BEGIN

        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	  IF LEN(ISNULL(@SortCol,''))=0
		SET  @SortCol = 'dtCreatedDate'
		
	  IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'desc'
      
        DECLARE @strSql NVARCHAR(4000)
        DECLARE @SELECT NVARCHAR(4000)
        DECLARE @FROM NVARCHAR(4000)
        DECLARE @WHERE NVARCHAR(4000)
		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
	CREATE TABLE #temp (row INT,vcpoppname VARCHAR(100),numoppid NUMERIC,vcOrderStatus VARCHAR(100),
		numoppbizdocsid NUMERIC,numBizDocId NUMERIC,vcbizdocid VARCHAR(100),vcBizDocType VARCHAR(100),
		numbizdocstatus NUMERIC,vcbizdocstatus VARCHAR(100),dtFromDate VARCHAR(30),numDivisionID NUMERIC,
		vcCompanyName VARCHAR(100),tintCRMType TINYINT)
		
        SET @SELECT = 'WITH bizdocs
         AS (SELECT Row_number() OVER(ORDER BY ' + @SortCol + ' '
            + @SortDirection
            + ') AS row,
			om.vcpoppname,
			om.numoppid,dbo.fn_GetListItemName(om.[numstatus]) vcOrderStatus,
			obd.[numoppbizdocsid],OBD.numBizDocId,
			obd.[vcbizdocid],dbo.fn_GetListItemName(OBD.numBizDocId) AS vcBizDocType,
			obd.[numbizdocstatus],
			dbo.fn_GetListItemName(obd.[numbizdocstatus]) vcbizdocstatus,
			[dbo].[FormatedDateFromDate](dtFromDate,'+ CONVERT(VARCHAR(15), @numDomainId) + ') dtFromDate,
			om.numDivisionID,
			vcCompanyName,DM.tintCRMType'
			
		SET @FROM =' FROM opportunitymaster om INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = obd.[numoppid]
                     INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
					 Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID'
					
        SET @WHERE = ' WHERE (om.numOppID = ' +  CAST(@numOppID AS VARCHAR) + ' OR ' +  CAST(ISNULL(@numOppID,0) AS VARCHAR) + ' = 0) AND om.tintopptype = 1 and om.tintOppStatus=1 AND ISNULL(OBD.bitFulfilled,0)=' + CAST(@bitFulfilled AS VARCHAR) + ' AND OBD.numBizDocId=296 AND om.numDomainID = '+ CONVERT(VARCHAR(15), @numDomainId)
		
		IF  LEN(ISNULL(@vcBizDocStatus,''))>0
		BEGIN
			IF @bitIncludeHistory=1
				SET @WHERE = @WHERE + ' and (select count(*) from OppFulfillmentBizDocsStatusHistory WHERE numOppId=om.numOppId and numOppBizDocsId=obd.numOppBizDocsId and isnull(numbizdocstatus,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','','')))>0' 			
			ELSE	
				SET @WHERE = @WHERE + ' and obd.numbizdocstatus in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','',''))' 			
		END
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and isnull(om.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		IF  LEN(ISNULL(@vcBizDocType,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(OBD.numBizDocId,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocType + ''','',''))' 			
		END
		
		SET @WHERE = @WHERE + ' AND (om.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = @SELECT + @FROM +@WHERE + ') insert into #temp SELECT * FROM [bizdocs] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)
        
        PRINT @strSql ;
        EXEC ( @strSql ) ;
       
       SELECT OM.*,ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
	FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainId WHERE numOppId=om.numOppId and numOppBizDocsId=OM.numOppBizDocsId FOR XML PATH('')),4,2000)),'') as vcBizDocsStatusList,
	isnull((select max(numShippingReportId) from ShippingReport SR where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID),0) AS numShippingReportId,
	ISNULL((SELECT max(SB.numShippingReportId) FROM [ShippingBox] SB JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId,
	CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID,ISNULL(vcItemDesc,'' ) AS vcItemDesc,ISNULL(monTotAmount,0) AS monTotAmount
	FROM #temp OM 
		LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '') vcItemDesc,monTotAmount,
		Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
		FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode =@numShippingServiceItemID) 
		OI ON OI.row=1 AND OM.numOppId = OI.numOppId
       
       
       SELECT opp.numOppId,OBD.numOppBizDocID,  Opp.vcitemname AS vcItemName,
				ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType, CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,  
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) AS vcAttributes,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,WL.vcLocation,OBD.numUnitHour AS numUnitHourOrig,
                        Opp.bitDropShip AS DropShip,SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN '('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,Opp.numWarehouseItmsID,opp.numoppitemtCode
              FROM      OpportunityItems opp 
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        join #temp OM on opp.numoppid=OM.numoppid and OM.numOppBizDocsId=OBD.numOppBizDocID
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainId
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID= WI.numWLocationID
              
        
        DROP TABLE #temp 
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
    END
  
/****** Object:  StoredProcedure [dbo].[USP_GetTaxPercentage]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxpercentage')
DROP PROCEDURE usp_gettaxpercentage
GO
CREATE PROCEDURE [dbo].[USP_GetTaxPercentage]
@DivisionID as numeric(9)=0,
@numBillCountry as numeric(9)=0,
@numBillState  as numeric(9)=0,
@numDomainID as numeric(9),
@numTaxItemID as numeric(9),
@tintBaseTaxCalcOn TINYINT=2, -- Base tax calculation on Shipping Address(2) or Billing Address(1)
@tintBaseTaxOnArea TINYINT=0,
@vcCity varchar(100),
@vcZipPostal varchar(20)
as

  
declare @TaxPercentage as float 
declare @bitTaxApplicable as bit 
set @TaxPercentage=0 
 
if @DivisionID>0 --Existing Customer
BEGIN
	IF @tintBaseTaxCalcOn = 1 --Billing Address
			select @numBillState=isnull(numState,0), @numBillCountry=isnull(numCountry,0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 where numDivisionID=@DivisionID
	ELSE --Shipping Address
			select @numBillState=isnull([numState],0), @numBillCountry=isnull([numCountry],0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
			AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 where numDivisionID=@DivisionID
             
	select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=  @DivisionID and numTaxItemID=@numTaxItemID

end 
else --New Customer
begin
	SET @bitTaxApplicable=1
END

--Take from TaxCountryConfi If not then use default from domain
select @tintBaseTaxOnArea=tintBaseTaxOnArea from TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numBillCountry 

if  @bitTaxApplicable=1 --If Tax Applicable
	begin
		if @numBillCountry >0 and @numBillState>0            
		begin            
			if @numBillState>0            
			begin            
				if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
						(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity + '%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
								(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState 
							and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''         
						END
				 else             
					select top 1 @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID          
		   end                
		end            
		else if @numBillCountry >0            
			 select top 1 @TaxPercentage=decTaxPercentage from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end

select @TaxPercentage
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertImapNewMails]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapnewmails')
DROP PROCEDURE usp_insertimapnewmails
GO
Create PROCEDURE [dbo].[USP_InsertImapNewMails]            
@numUserCntId as numeric(9),            
@numDomainID as numeric(9),            
@numUid NUMERIC(9),
@vcSubject VARCHAR(500),
@vcBody TEXT,
@vcBodyText text,
@dtReceivedOn datetime,
@vcSize varchar(50),
@bitIsRead bit,
@bitHasAttachments bit,
@vcFromName VARCHAR(5000),
@vcToName VARCHAR(5000),
@vcCCName VARCHAR(5000),
@vcBCCName VARCHAR(5000),
@vcAttachmentName VARCHAR(500),
@vcAttachmenttype VARCHAR(500),
@vcNewAttachmentName VARCHAR(500),
@vcAttachmentSize VARCHAR(500)
as             
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON

                                        
DECLARE @Identity AS NUMERIC(9)              
            
if (select count(*) from emailhistory where numUserCntId = @numUserCntId             
   and numDomainid=@numDomainID  and numUid=@numUid)=0            
  begin      
             --Inser into Email Master
             SET @vcToName = @vcToName + '#^#'
             SET @vcFromName = @vcFromName + '#^#'
             SET @vcCCName = @vcCCName + '#^#'
             SET @vcBCCName = @vcBCCName + '#^#'

            -- PRINT 'ToName' + @ToName 
             EXECUTE USP_InsertEmailMaster @vcToName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcFromName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcCCName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcBCCName ,@numDomainID 

             -- Insert Into Email History
                SET ARITHABORT ON
                INSERT  INTO EmailHistory
                        (
                          vcSubject,
                          vcBody,
                          vcBodyText,
                          bintCreatedOn,
                          bitHasAttachments,
                          vcItemId,
                          vcChangeKey,
                          bitIsRead,
                          vcSize,
                          chrSource,
                          tinttype,
                          vcCategory,
                          dtReceivedOn,
                          numDomainid,
                          numUserCntId,
                          numUid,IsReplied,
						  numNodeId
                        )
                VALUES  (
                          @vcSubject,
                          @vcBody,
                          dbo.fn_StripHTML(@vcBodyText),
                          GETUTCDATE(),
                          @bitHasAttachments,
                          '',
                          '',
                          @bitIsRead,
                          CONVERT(VARCHAR(200), @vcSize),
                          'I',
                          1,
                          '',
                          @dtReceivedOn,
                          @numDomainid,
                          @numUserCntId,
                          @numUid,0,
						  ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntId AND numFixID=1 AND ISNULL(bitSystem,0) = 1),0)
                        )                        
                SET @Identity = @@identity        
                

--				To increase performance of Select
                UPDATE  [EmailHistory]
                SET     vcFrom = dbo.fn_GetNewvcEmailString(@vcFromName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 4),
                        [vcTo] = dbo.fn_GetNewvcEmailString(@vcToName,@numDomainID),    --dbo.GetEmaillName(numEmailHstrID, 1),
                        [vcCC] = dbo.fn_GetNewvcEmailString(@vcCCName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 3)
                        [vcBCC]= dbo.fn_GetNewvcEmailString(@vcBCCName,@numDomainID)
                WHERE   [numEmailHstrID] = @Identity

				IF(SELECT vcFrom FROM EmailHistory WHERE [numEmailHstrID]=@Identity) IS NOT NULL 
				BEGIN
					update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) 
					where [numEmailHstrID]=@Identity	
				END

                IF @bitHasAttachments = 1 
                    BEGIN                    
                        EXEC USP_InsertAttachment @vcAttachmentName, '',
                            @vcAttachmentType, @Identity, @vcNewAttachmentName,@vcAttachmentSize                       
                    END 
    END            
GO
    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and Fld_Value like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild'
									  + @strSQL
									  + ' from Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
										  (CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 
												THEN 
													(
														CASE
															WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
															THEN 1
															ELSE 0
														END
													)
												ELSE 0 
											END) bitHasKitAsChild'
										  + @strSQL
										  + '  from item I 
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
   
 @numDomainID NUMERIC(18,0),  
 @numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	SELECT
		Item.numItemCode,
		ItemDetails.numWarehouseItemID,
		Item.vcItemName
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
   WHERE
		ItemDetails.numItemKitID = @numItemCode
END  


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetKitChildItems' ) 
    DROP PROCEDURE USP_ItemDetails_GetKitChildItems
GO
CREATE PROCEDURE USP_ItemDetails_GetKitChildItems  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  

	DECLARE @finalSQL AS VARCHAR(8000)
	DECLARE @strSQL VARCHAR(8000) 
	 
	 SELECT    
	   @strSQL = COALESCE(@strSQL + ' ', '') + ', dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) + ', Item.numItemCode) as [' + vcFieldName + ']'
    FROM
		View_DynamicCustomColumns
    WHERE     
		Grp_id = 5
        AND numFormId = 127
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 0

  
	

    SET @finalSQL = CONCAT('
	SELECT 
		Item.numItemCode,
		ItemDetails.numWarehouseItemID,
		ISNULL(Item.vcItemName,'''') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND bitDefault=1),'''') AS vcImage,
		ISNULL(Item.vcModelID,'''') AS vcModelID,
		ISNULL(Item.txtItemDesc,'''') AS txtItemDesc,
		CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN ISNULL(WareHouseItems.vcWHSKU,'''') ELSE ISNULL(Item.vcSKU,'''') END vcSKU,
		CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN ISNULL(WareHouseItems.vcBarCode,'''') ELSE ISNULL(Item.numBarCodeId,'''') END numBarCodeId,
		CASE WHEN Item.charItemType = ''P'' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(monListPrice,0) END AS monListPrice,
		ISNULL(CompanyInfo.vcCompanyName,'''') AS vcCompanyName,
		ISNULL(Vendor.vcPartNo,'''') AS vcPartNo,
		CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN ISNULL(dbo.fn_GetAttributes(WarehouseItems.numWareHouseItemID,Item.bitSerialized),'''') ELSE '''' END AS vcAttributes ', @strSQL , 
	'FROM 
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
	LEFT JOIN
		WarehouseItems
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Vendor
	ON
		Vendor.numVendorID = Item.numVendorID
		AND Vendor.numItemCode = Item.numItemCode
	LEFT JOIN
		DivisionMaster
	ON
		Vendor.numVendorID = DivisionMaster.numDivisionID
	LEFT JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		ItemDetails.numItemKitID = ',@numItemCode,' ORDER BY Item.numItemCode, ItemDetails.numWarehouseItemID')
	
	EXEC (@finalSQL)
   
END  


/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units integer,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0
as                 
BEGIN TRY

--      SELECT 1/0 ; --Divide by zero error encountered.
      
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  
declare @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems bit
DECLARE @numDefaultSalesPricing TINYINT

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

          
            
if @tintOppType=1            
begin            

SELECT @bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) FROM item WHERE numItemCode = @numItemCode
      


/*Get List Price for item*/      
if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and  charItemType='P'))      
begin      
	select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
    if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
end      
else      
begin      
	 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
end      
print @monListPrice   
/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
IF @bitCalAmtBasedonDepItems = 1 
BEGIN
	
	SELECT  
			@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
			THEN CASE WHEN I.bitSerialized = 1 THEN I.monListPrice ELSE WI.[monWListPrice] END 
			ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
	FROM    [ItemDetails] ID
			INNER JOIN [Item] I ON ID.[numChildItemID] = I.[numItemCode]
			left join  WareHouseItems WI
			on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.[numWareHouseItemId]
	WHERE   [numItemKitID] = @numItemCode
	
END


SELECT @numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) FROM Domain WHERE numDomainID = @numDomainID

IF @numDefaultSalesPricing = 1 -- Use Price Level
BEGIN
	DECLARE @newPrice MONEY
	DECLARE @finalUnitPrice FLOAT = 0
	DECLARE @tintRuleType INT
	DECLARE @tintDiscountType INT
	DECLARE @decDiscount FLOAT
	DECLARE @ItemPrice FLOAT

	SET @tintRuleType = 0
	SET @tintDiscountType = 0
	SET @decDiscount  = 0
	SET @ItemPrice = 0

	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		(@units BETWEEN intFromQty AND intToQty)

	IF @tintRuleType > 0 AND @tintDiscountType > 0
	BEGIN	
		IF @tintRuleType = 1 -- Deduct from List price
		BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
		ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
		BEGIN
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

			If @ItemPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
			END
		END
		ELSE IF @tintRuleType = 3 -- Named Price
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
			SET @tintDiscountType = 2

			If @monListPrice > 0 AND @monListPrice >= @finalUnitPrice
			BEGIN
				SET @decDiscount = @monListPrice - @finalUnitPrice
			END
		END
	END

	Print @finalUnitPrice

	IF @finalUnitPrice = 0
		SET @newPrice = @monListPrice
	ELSE
		SET @newPrice = @finalUnitPrice

	If @tintRuleType = 2
	BEGIN
		IF @finalUnitPrice > 0
		BEGIN
			IF @tintDiscountType = 1 -- Percentage
			BEGIN
				IF @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					If @monListPrice > 0
						SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
					ELSE
						SET @decDiscount = 0
			END
			ELSE IF @tintDiscountType = 2 -- Flat Amount
			BEGIN
				If @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					SET @decDiscount = @monListPrice - @newPrice
			END
		END
		ELSE
		BEGIN
			SET @decDiscount = 0
			SET @tintDiscountType = 0
		END
		
	END

	SELECT 
		1 as numUintHour, 
		@newPrice AS ListPrice,
		'' AS vcPOppName,
		'' AS bintCreatedDate,
		CASE 
			WHEN @tintRuleType = 0 THEN 'Price - List price'
            ELSE 
               CASE @tintRuleType
               WHEN 1 
					THEN 
						'Deduct from List price ' +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
               END
        END AS OppStatus,
		CAST(0 AS NUMERIC) AS numPricRuleID,
		CAST(1 AS TINYINT) AS tintPricingMethod,
		CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
		CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
		CAST(@tintRuleType AS TINYINT) AS tintRuleType
END
ELSE -- Use Price Rule
BEGIN
/* Checks Pricebook if exist any.. */      
SELECT TOP 1 1 AS numUnitHour,
		dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
		* (CASE WHEN P.tintPricingMethod =2 AND P.tintRuleType=2 THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
        vcRuleName AS vcPOppName,
        CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
        CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
             ELSE 
              CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE PBT.tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
        END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
FROM    Item I
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC
END

IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
	RETURN 
-- select 1 as numUnitHour,convert(money,(case when tintRuleType is null then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units when tintRuleType=0 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-
--(case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*(case when bitVolDisc=1 then           
--(case when (@units/intQntyItems)*fltDedPercentage >decMaxDedPerAmt then decMaxDedPerAmt else (@units/intQntyItems)*fltDedPercentage end)           
-- else fltDedPercentage end )*@units/100                  
--  when tintRuleType=1 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-          
--(case when bitVolDisc=1 then (case when (@units/intQntyItems)*decflatAmount>decMaxDedPerAmt then decMaxDedPerAmt else           
--(@units/intQntyItems)*decflatAmount end) else decflatAmount end )  end)/@units)           
--as ListPrice,vcRuleName as vcPOppName,convert(varchar(20),null) as bintCreatedDate,case when numPricRuleID is null then 'Price - List price' else 'Price Book Rule' end as OppStatus  from Item I      
-- left join  PriceBookRules P      
-- on I.numDomainID=P.numDomainID                   
-- left join PriceBookRuleDTL  PDTL                  
-- on P.numPricRuleID=PDTL.numRuleID                   
--where numItemCode=@numItemCode and    (((tintRuleAppType=1 and numValue=@numItemCode) or                  
-- (tintRuleAppType=2 and numValue=numItemGroup) or                  
-- (tintRuleAppType=4 and numValue=@numDivisionID) or                  
-- (tintRuleAppType=5 and numValue=@numRelationship) or                  
-- (tintRuleAppType=6 and numValue=@numRelationship and numProfile=@numProfile) or                  
-- (tintRuleAppType=7) or (tintRuleAppType= case when numItemGroup>0 then 3 else 10 end)) and (case when bitTillDate=1 then dtTillDate  else getutcdate() end >=getutcdate()) or (tintRuleAppType is null) )                               
                                   
/*Checks Old Order for price*/
 select 1 as numUnitHour,convert(money,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end))) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
 when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
 from OpportunityItems itm                                        
 join OpportunityMaster mst                                        
 on mst.numOppId=itm.numOppId              
 where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
  /* */
 select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,@CalPrice) ELSE convert(money,@monListPrice) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
  
            
end             
else            
begin                                               

If @numDivisionID=0
BEGIN
 SELECT @numDivisionID=V.numVendorID FROM [Vendor] V INNER JOIN Item I 
		on V.numVendorID=I.numVendorID and V.numItemCode=I.numItemCode 
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
END

IF EXISTS(SELECT ISNULL([monCost],0) ListPrice FROM [Vendor] V INNER JOIN dbo.Item I 
		ON V.numItemCode = I.numItemCode
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID AND V.numVendorID=@numDivisionID)       
begin      
       SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] V INNER JOIN dbo.Item I 
				ON V.numItemCode = I.numItemCode
				WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
				AND V.numVendorID=@numDivisionID
end      
else      
begin      
	 SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] WHERE [numItemCode]=@numItemCode AND [numDomainID]=@numDomainID
end      

print @monListPrice   

SELECT TOP 1 1 AS numUnitHour,
			dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) AS ListPrice,
			vcRuleName AS vcPOppName,
			CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
			CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
				 ELSE  CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
			END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,@numDivisionID as numDivisionID
		FROM    Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

--	IF @bitMode = 1 /* from Popup for Grid*/
--	BEGIN
		 select 1 as numUnitHour,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus DESC
--	END
--	ELSE
--	BEGIN
		/* Checks Pricebook if exist any.. */      
		
--	END
 
--	 IF @@ROWCOUNT=0
--	 BEGIN
 		select 1 as numUnitHour,convert(money,@monListPrice) as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod                         
--	 END
 
	
end

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


/****** Object:  StoredProcedure [dbo].[usp_ManageAssociations]    Script Date: 07/26/2008 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By: Debasish                    
--Modified Date: 30th Dec 2005                    
--Purpose for Modification: To insert new associations/ modifying existing ones for the Organizations                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageassociations')
DROP PROCEDURE usp_manageassociations
GO
CREATE PROCEDURE [dbo].[usp_ManageAssociations]                          
 @numAssociationID numeric=0,                          
 @numAssociatedFromDivID numeric,                          
 @numCompID numeric,                          
 @numDivID numeric,                          
 @bitParentOrg bit,                           
 @bitChildOrg bit,                          
 @numTypeID numeric,                          
 @bitDeleted bit = 0,                          
 @numUserCntID numeric,                           
 @numDomainID numeric,  
 @bitShareportal BIT,
 @bitTypeLabel BIT,
 @numAssoTypeLabel NUMERIC(9)=0,
 @numContactID AS NUMERIC(9)
AS                      
  DECLARE @Count As Integer         
  DECLARE @vcParentOrgName As NVarchar(100)          

  SELECT @numCompID = [numCompanyID] FROM [DivisionMaster] WHERE [numDivisionID]=@numDivID
  
  IF EXISTS(SELECT 1 FROM CompanyAssociations                          
  WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID != @numAssociatedFromDivID AND numDivisionID=@numDivID AND ISNULL(bitChildOrg,0)=1 AND @bitChildOrg = 1)
  BEGIN
  	SELECT 'DuplicateChild'
	RETURN 
  END
  ELSE IF EXISTS(SELECT 1 FROM CompanyAssociations                          
  WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID = @numAssociatedFromDivID AND numDivisionID=@numDivID)
  BEGIN
 -- 	SELECT 'Duplicate'
		UPDATE CompanyAssociations SET 
	   numReferralType = @numTypeID,            
	   bitDeleted = 0,           
	   bitParentOrg = @bitParentOrg,             
	   bitChildOrg = @bitChildOrg,             
	   numModifiedBy = @numUserCntID,               
	   numModifiedOn = getutcdate(),  
	   bitShareportal  =@bitShareportal           
	   WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID = @numAssociatedFromDivID AND numDivisionID=@numDivID  
	RETURN 
  END
  ELSE
  BEGIN         
--      IF  @bitParentOrg = 1        
--          SELECT @vcParentOrgName = dbo.fn_GetParentOrgName(@numAssociatedFromDivID)        
--      ELSE IF  @bitChildOrg = 1        
--          SELECT @vcParentOrgName = dbo.fn_GetParentOrgName(@numDivID)        
--        
--      IF @vcParentOrgName IS NULL             
--      BEGIN       
		IF @bitTypeLabel =1 
        BEGIN
			UPDATE [CompanyAssociations] SET [bitTypeLabel]=0 WHERE [numAssociateFromDivisionID]=@numAssociatedFromDivID
		END	
        
            INSERT INTO CompanyAssociations (numAssociateFromDivisionID, numCompanyID, numDivisionID, bitParentOrg,                    
            bitChildOrg, numReferralType, bitDeleted, numCreateBy, numCreatedOn,                     
            numModifiedBy, numModifiedOn, numDomainID,bitShareportal,[bitTypeLabel],[numAssoTypeLabel],numContactID)                           
            VALUES (@numAssociatedFromDivID, @numCompID, @numDivID, @bitParentOrg,                          
            @bitChildOrg, @numTypeID, @bitDeleted, @numUserCntID, getutcdate(),                           
            @numUserCntID, getutcdate(), @numDomainID,@bitShareportal,@bitTypeLabel,@numAssoTypeLabel,@numContactID)
        
        
        
            SELECT '0'           
--       END        
--       ELSE             
--       BEGIN          
--          DECLARE @numDivisionId Numeric        
--          IF  @bitParentOrg = 1          
--            SET @numDivisionId = @numAssociatedFromDivID        
--          ELSE IF  @bitChildOrg = 1        
--            SET @numDivisionId = @numDivId        
--        
--          SELECT vcCompanyName FROM        
--          (        
--             SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci, DivisionMaster dm        
--             WHERE ca.numAssociateFromDivisionId = @numDivisionId AND              
--             ca.numAssociateFromDivisionId = DM.numDivisionId AND        
--             dm.numCompanyId = ci.numCompanyId AND        
--             ca.bitParentOrg=1          
--             UNION        
--             SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci        
--             WHERE ca.numDivisionId = @numDivisionId AND        
--             ca.numCompanyId = ci.numCompanyId AND        
--             ca.bitChildOrg = 1         
--          ) As tmpTable        
--       END         
  END             
--  ELSE              
--  BEGIN              
--   UPDATE CompanyAssociations SET              
--   bitDeleted = 0,           
--   bitParentOrg = @bitParentOrg,             
--   bitChildOrg = @bitChildOrg,             
--   numModifiedBy = @numUserCntID,               
--   numModifiedOn = getutcdate(),  
--   bitShareportal  =@bitShareportal           
--   WHERE numAssociateFromDivisionID = @numAssociatedFromDivID AND                          
--   numCompanyID = @numCompID AND                          
--   numDivisionID = @numDivID AND              
--   numReferralType = @numTypeID    
--              
--   SELECT '0'            
--  END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBizDocTemplate')
DROP PROCEDURE USP_ManageBizDocTemplate
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocTemplate]
    @numDomainID NUMERIC,
    @numBizDocID NUMERIC,
    @numOppType NUMERIC, 
    @txtBizDocTemplate TEXT,
    @txtCSS TEXT,
    @bitEnabled BIT,
    @tintTemplateType TINYINT,
	@numBizDocTempID NUMERIC,
	@vcTemplateName varchar(50)='',
	@bitDefault bit=0,
	@numOrientation int=1,
	@bitKeepFooterBottom bit = 0,
	@numRelationship NUMERIC(18,0) = 0,
	@numProfile NUMERIC(18,0) = 0,
	@bitDisplayKitChild AS BIT = 0
AS 
BEGIN TRY
	IF (ISNULL(@numRelationship,0) <> 0 OR ISNULL(@numProfile,0) <>0)
	BEGIN
		IF (SELECT 
				COUNT(numBizDocTempID) 
			FROM 
				BizDocTemplate 
			WHERE 
				[numDomainID] = @numDomainID 
				AND [numBizDocID] = @numBizDocID 
				AND [numOppType]=@numOppType 
				AND tintTemplateType=@tintTemplateType 
				AND (ISNULL(numProfile,0) > 0 OR ISNULL(numRelationship,0) > 0) 
				AND numProfile=@numProfile 
				AND numBizDocTempID <> @numBizDocTempID
				AND numRelationship=@numRelationship) > 0
		BEGIN
			RAISERROR('BIZDOC_RELATIONSHIP_PROFILE_EXISTS',16,1)
		END
	END


	IF @tintTemplateType=0
	BEGIN
		IF @bitDefault=1
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1)>0
			BEGIN
				Update BizDocTemplate SET bitDefault=0 where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1
			END
		END
		ELSE
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType)=0
			BEGIN
				SET @bitDefault=1
			END
		END

		IF @numBizDocTempID>0
			BEGIN
				UPDATE  BizDocTemplate SET [txtBizDocTemplate] = @txtBizDocTemplate,[txtCSS] = @txtCSS,
					bitEnabled =@bitEnabled,vcTemplateName=@vcTemplateName,bitDefault=@bitDefault,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					[numBizDocID] = @numBizDocID,
					[numRelationship] = @numRelationship,
					[numProfile] = @numProfile,
					bitDisplayKitChild = @bitDisplayKitChild
					WHERE numBizDocTempID = @numBizDocTempID 
			END
		ELSE
			BEGIN
				INSERT  INTO BizDocTemplate
                    ([numDomainID],[numBizDocID],[txtBizDocTemplate],[txtCSS],bitEnabled,[numOppType],tintTemplateType,vcTemplateName,bitDefault,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild)
				VALUES  (@numDomainID,@numBizDocID,@txtBizDocTemplate,@txtCSS,@bitEnabled,@numOppType,@tintTemplateType,@vcTemplateName,@bitDefault,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile,@bitDisplayKitChild)
            
				SELECT  SCOPE_IDENTITY() AS InsertedID
			END
	END
	ELSE
		BEGIN
		IF NOT EXISTS ( SELECT  * FROM    [BizDocTemplate] WHERE   numDomainID = @numDomainID AND numBizDocID = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType) 
        BEGIN
            INSERT  INTO BizDocTemplate
                    (
                      [numDomainID],
                      [numBizDocID],
                      [txtBizDocTemplate],
                      [txtCSS],
					  bitEnabled,
					  [numOppType],tintTemplateType,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild
	              )
            VALUES  (
                      @numDomainID,
                      @numBizDocID,
                      @txtBizDocTemplate,
                      @txtCSS,
					  @bitEnabled,
					  @numOppType,@tintTemplateType,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile,@bitDisplayKitChild
	              )
            SELECT  SCOPE_IDENTITY() AS InsertedID
        END
    ELSE 
        BEGIN
            UPDATE  BizDocTemplate
            SET     
                    [txtBizDocTemplate] = @txtBizDocTemplate,
                    [txtCSS] = @txtCSS,
                    bitEnabled =@bitEnabled,
                    numOppType = @numOppType,tintTemplateType=@tintTemplateType,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					numRelationship = @numRelationship, numProfile = @numProfile, bitDisplayKitChild=@bitDisplayKitChild
            WHERE   [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType
        END
END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)       
SET @ParentSKU = @vcSKU                                 
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END
                                                         
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice= @monListPrice,
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END                                      
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int, numItemDetailID NUMERIC(18,0)))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID and numItemDetailID = X.ItemDetailID               
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                      
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              
                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemSerialNo')
DROP PROCEDURE USP_ManageItemSerialNo
GO
CREATE PROCEDURE USP_ManageItemSerialNo
    @numDomainID NUMERIC(9),
    @strFieldList TEXT,
    @numUserCntID AS NUMERIC(9)=0
AS 
BEGIN
    DECLARE @hDoc AS INT                                            
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList
                                                     
     
    SELECT 
		X.*,
		ROW_NUMBER() OVER( order by X.NewQty) AS ROWNUMBER
	INTO 
		#TempTable
	FROM 
		( 
			SELECT  
				numWareHouseItmsDTLID,
				numWareHouseItemID,
				vcSerialNo,
				NewQty,
				OldQty, 
				(CASE WHEN CAST(dExpirationDate AS DATE) = cast('1753-1-1' as date) THEN NULL ELSE dExpirationDate END) AS dExpirationDate
			FROM      
				OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
            WITH 
				( 
					numWareHouseItmsDTLID NUMERIC(9), 
					numWareHouseItemID NUMERIC(9), 
					vcSerialNo VARCHAR(100), 
					NewQty NUMERIC(9),
					OldQty NUMERIC(9),
					dExpirationDate DATETIME
				)
        ) X

	DECLARE @minROWNUMBER INT
	DECLARE @maxROWNUMBER INT
	DECLARE @Diff INT
	DECLARE @numNewQty AS INT
	DECLARE @dtExpirationDate AS DATE
	DECLARE @Diff1 INT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numWarehouseItmDTLID NUMERIC(18,0)
	DECLARE @vcSerialNo AS VARCHAR(100)

	SELECT  @minROWNUMBER=MIN(ROWNUMBER), @maxROWNUMBER=MAX(ROWNUMBER) FROM #TempTable

	DECLARE @vcDescription VARCHAR(100),@numItemCode NUMERIC(18)

	WHILE  @minROWNUMBER <= @maxROWNUMBER
    BEGIN

   	    SELECT 
			@numWarehouseItmDTLID = numWarehouseItmsDTLID,
			@numWareHouseItemID=numWareHouseItemID,
			@numNewQty = NewQty,
			@vcSerialNo = vcSerialNo,
			@dtExpirationDate = dExpirationDate,
			@Diff = NewQty - OldQty
		FROM 
			#TempTable 
		WHERE 
			ROWNUMBER=@minROWNUMBER

		SET @numItemCode = 0

		SELECT 
			@numItemCode=numItemID 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID 
			AND [numDomainID] = @numDomainID

		IF ISNULL((SELECT bitSerialized FROM Item WHERE numItemCode = @numItemCode),0) = 1
		BEGIN
			IF EXISTS (SELECT 
							numWareHouseItmsDTLID 
						FROM 
							WareHouseItmsDTL 
						WHERE 
							numWareHouseItemID IN (SELECT ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = @numItemCode)
							AND WareHouseItmsDTL.numWareHouseItmsDTLID <> @numWarehouseItmDTLID
							AND WareHouseItmsDTL.vcSerialNo = @vcSerialNo)
			BEGIN
				RAISERROR('DUPLICATE_SERIAL_NUMBER',16,1)
				RETURN
			END
		END

		UPDATE 
			WareHouseItmsDTL 
		SET  
			numQty=@numNewQty,
			vcSerialNo=@vcSerialNo,
			dExpirationDate=@dtExpirationDate 
		WHERE
			numWareHouseItmsDTLID = @numWarehouseItmDTLID
			
                
        SET @vcDescription='UPDATE Lot/Serial#'
		
   	    UPDATE
			WareHouseItems 
		SET 
			numOnHand=ISNULL(numOnHand,0) + @Diff,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID = @numWareHouseItemID 
			AND (numOnHand + @Diff) >= 0

		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = @numDomainID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomain 

        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
    END	           
     
	DROP TABLE #TempTable
    EXEC sp_xml_removedocument @hDoc
END
                      
/****** Object:  StoredProcedure [dbo].[USP_ManagekitParentsForChildItems]    Script Date: 07/26/2008 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managekitparentsforchilditems')
DROP PROCEDURE usp_managekitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_ManagekitParentsForChildItems]  
@numItemKitID as numeric(9)=0,  
@numChildItemID as numeric(9)=0,  
@numQtyItemsReq as numeric(9)=0,  
@byteMode as tinyint,
@numWarehouseItemID AS NUMERIC(18,0) = 0
AS  
IF @byteMode=0  
BEGIN
	SELECT TOP 1 @numWarehouseItemID=ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = @numChildItemID

	INSERT INTO ItemDetails 
	(numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId)  
	VALUES
	(@numItemKitID,@numChildItemID,@numQtyItemsReq,@numWarehouseItemID)  
END  
  
IF @byteMode=1  
BEGIN  
	DELETE FROM ItemDetails WHERE numItemKitID=@numItemKitID and numChildItemID=@numChildItemID AND numWareHouseItemId = @numWarehouseItemID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
              --                              AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														--ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]						
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),OBD.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
OBD.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
OBD.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(OBD.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) as DiscAmt,OBD.vcNotes,OBD.vcTrackingNo,OBD.vcShippingMethod,OBD.monShipCost,[dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on OBD.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   OBD.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE /*IF @tintOppType = 1*/ 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    --ELSE 
    --            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            /*IF @tintOppType = 1 */
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            --ELSE 
            --    SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc,                                    
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
            OBD.vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                    ELSE i.vcSKU
            END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                    ELSE i.numBarCodeId
            END vcBarcode,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
       

    ) X

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numUnitHourOrig * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,                                    
            t2.numQty AS numUnitHour,
            (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),(t2.numQty * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            (t2.numQty * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode ELSE I.numBarCodeId END vcBarcode,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC @strSQLUpdate
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY 
		tintRow
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as                        
                                          
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint

SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
--delete from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  JOIN divisionmaster div ON div.numDivisionId=oppmst.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
			  ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile,
		0 AS intInvoiceForDiferredBizDoc	 
from                         
 ReturnHeader RH                        
  JOIN divisionmaster div ON div.numDivisionId=RH.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = RH.numContactId
where RH.numOppId=@numOppId
)X         
                        
END




/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_OPPCheckCanBeShipped 5853,1  
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcheckcanbeshipped')
DROP PROCEDURE usp_oppcheckcanbeshipped
GO
CREATE PROCEDURE [dbo].[USP_OPPCheckCanBeShipped]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
AS              
BEGIN         
	CREATE TABLE #tempItem(vcItemName VARCHAR(300))   

	---REVERTING BACK TO PREVIOUS STATE IF DEAL IS BEING EDITED AFTER IT IS CLOSED
	DECLARE @OppType AS VARCHAR(2)
	DECLARE @vcItemName AS VARCHAR(300)
	DECLARE @vcKitItemName AS VARCHAR(300)
	DECLARE @Sel AS TINYINT   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC                                 
	DECLARE @numUnits AS NUMERIC                                            
	DECLARE @numQtyShipped AS NUMERIC
	DECLARE @numAllocation AS NUMERIC                                                                                                                          
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @numQtyReceived AS NUMERIC(9)
	DECLARE @bitAsset as BIT

	SET @Sel=0          
	SELECT @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
                                            
	SELECT TOP 1 
		@numoppitemtCode=numoppitemtCode,
		@itemcode=OI.numItemCode,
		@numUnits=ISNULL(numUnitHour,0),
		@numQtyShipped=ISNULL(numQtyShipped,0),
		@numQtyReceived=ISNULL(numUnitHourReceived,0),
		@numWareHouseItemID=numWarehouseItmsID,
		@bitAsset=ISNULL(I.bitAsset,0),
		@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
		@vcItemName=OI.vcItemName 
	FROM 
		OpportunityItems OI                                            
	JOIN 
		Item I                                            
	ON 
		OI.numItemCode=I.numItemCode 
		AND numOppId=@intOpportunityId 
		AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
	WHERE 
		charitemtype='P'  
	ORDER BY 
		OI.numoppitemtCode        
		                                      
	WHILE @numoppitemtCode>0                               
	BEGIN  
		IF @OppType=1 AND @Kit = 1
		BEGIN
			DECLARE @TEMPTABLE TABLE
			(
				numItemCode NUMERIC(18,0),
				numParentItemID NUMERIC(18,0),
				numOppChildItemID NUMERIC(18,0),
				vcItemName VARCHAR(300),
				bitKitParent BIT,
				numWarehouseItmsID NUMERIC(18,0),
				numQtyItemsReq INT,
				numAllocation INT
			)

			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OKI.numOppID = @intOpportunityId
				AND OKI.numOppItemID = @numoppitemtCode
				AND OI.numoppitemtCode = @numoppitemtCode  

			INSERT INTO
				@TEMPTABLE
			SELECT
				OKCI.numItemID,
				t1.numItemCode,
				t1.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKCI.numWareHouseItemId,
				OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				WareHouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				@TEMPTABLE t1
			ON
				OKCI.numOppChildItemID = t1.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			WHERE
				OKCI.numOppID = @intOpportunityId
				AND OKCI.numOppItemID = @numoppitemtCode
	
			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE ISNULL(bitKitParent,0) = 0 AND ISNULL(numQtyItemsReq,0) > ISNULL(numAllocation,0)) > 0
			BEGIN
				INSERT INTO #tempItem values (@vcItemName)
			END    
        END
		ELSE
		BEGIN
			SELECT                                          
				@numAllocation=isnull(numAllocation,0)                                           
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
				                                            
			IF @OppType=1                                              
			BEGIN          
				/*below is added to be validated from Sales fulfillment*/
				IF @bitCheck =1
				BEGIN
					IF @numUnits-@numQtyShipped>0
					BEGIN  
						IF (@bitAsset=0 )
						BEGIN
							INSERT INTO #tempItem values (@vcItemName)     
						END              
					 END            	
				END
				 
				SET @numUnits = @numUnits - @numQtyShipped
				
				IF @numUnits > @numAllocation   
				BEGIN  
					If (@bitAsset=0 )
					BEGIN
						INSERT INTO #tempItem VALUES (@vcItemName)  
					END                                                             
				END            
			END    
			ELSE IF @OppType=2
			BEGIN
				IF @bitCheck =1
				BEGIN
   					IF @numUnits-@numQtyReceived>0
					BEGIN  
						INSERT INTO #tempItem VALUES (@vcItemName)                              
					END  
			   END
			END
		END
                           
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),@vcItemName=OI.vcItemName from OpportunityItems OI                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
		WHERE 
			charitemtype='P' 
			AND OI.numoppitemtCode>@numoppitemtCode 
			AND (bitDropShip=0 or bitDropShip is null) 
		ORDER BY 
			OI.numoppitemtCode                                            
    
		IF @@rowcount=0 SET @numoppitemtCode=0    
	END   
  
	SELECT * FROM #tempItem
	DROP TABLE #tempItem
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
--			 case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0))=1 
--				  then ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0) 
--				  else 0 
--			 end AS numBillingDaysName,
--			 CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))) = 1
-- 				   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))
--				   ELSE 0
--			  END AS numBillingDaysName,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,0) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint               
	DECLARE @tintDefaultClassType NUMERIC
	DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
	--Get Default Item Class for particular user based on Domain settings  
	SELECT 
		@tintDefaultClassType=ISNULL(tintDefaultClassType,0) 
	FROM 
		dbo.Domain 
	WHERE 
		numDomainID = @numDomainID 

	IF @tintDefaultClassType=0
	BEGIN
		SELECT 
			@numDefaultClassID=NULLIF(numDefaultClass,0) 
		FROM 
			dbo.UserMaster 
		WHERE 
			numUserDetailId = @numUserCntID AND numDomainID = @numDomainID 
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		--Set Default Class If enable User Level Class Accountng 
		DECLARE @numAccountClass AS NUMERIC(18) = 0
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numUserCntID 
			AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,@Comments,@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany
		)                                                                                                                      
		
		SET @numOppID=SCOPE_IDENTITY()                                                
  
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				CASE WHEN @tintDefaultClassType=0 THEN @numDefaultClassID ELSE (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
				X.numToWarehouseItemID,X.Attributes,(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired 
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour NUMERIC(9,2), monPrice MONEY, monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=(CASE WHEN @tintDefaultClassType=0 THEN @numDefaultClassID ELSE (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour NUMERIC(9,2),monPrice MONEY,monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		SET @TotalAmount=0                                                           
		SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
		UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) 
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
			FROM 
				dbo.DivisionMaster 
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = @Comments,bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,tintOppStatus = @DealStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
				(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired 
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour numeric(9,2),monPrice money,monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount float,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), 
					bitItemPriceApprovalRequired BIT
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder  
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour NUMERIC(9,2),monPrice MONEY,monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(200),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              

declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @numXmlQty AS INT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS INT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped NUMERIC(18,0),
				numQty NUMERIC(18,0),
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* GET ALL ITEMS ADDED TO BIZDOC WITH ASSIGNED SERIAL/LOT# FOR SERIAL/LOT ITEM  */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO
				@TempItemXML
			SELECT
				OBI.numOppItemID,
				OBI.numUnitHour,
				OBI.numWarehouseItmsID,
				STUFF((SELECT 
							',' + WIDL.vcSerialNo + (CASE WHEN ISNULL(I.bitLotNo,0) = 1 THEN '(' + CAST(OWSI.numQty AS VARCHAR) + ')'  ELSE '' END)
						FROM 
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WIDL
						ON
							OWSI.numWarehouseItmsDTLID = WIDL.numWareHouseItmsDTLID
						INNER JOIN
							WareHouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						INNER JOIN
							Item I
						ON
							WI.numItemID = I.numItemCode
						WHERE 
							OWSI.numOppID = @numOppId
							AND OWSI.numOppItemID = OBI.numOppItemID
							AND ISNULL(OWSI.numOppBizDocsId,0) = 0 --THIS CONDITION IS REQUIRED BECAUSE IT IS AUTOFULFILLMENT
					for xml path('')
				),1,1,'') AS vcSerialLot
			FROM 
				OpportunityBizDocItems OBI
			WHERE
				OBI.numOppBizDocID = @numOppBizDocID
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty INT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS INT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS INT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS INT
		DECLARE @numChildQtyShipped AS INT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty INT,
			numChildQtyShipped INT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS INT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS INT
		DECLARE @numKitChildQtyShipped AS INT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty INT,
			numKitChildQtyShipped INT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						(ISNULL(numQtyItemsReq_Orig,0) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									(ISNULL(OKCI.numQtyItemsReq_Orig,0) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty

							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@dtShippedDate DATETIME
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @numXmlQty AS INT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS INT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped NUMERIC(18,0),
				numQty NUMERIC(18,0),
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty INT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS INT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS INT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS INT
		DECLARE @numChildQtyShipped AS INT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty INT,
			numChildQtyShipped INT
		)


		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS INT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS INT
		DECLARE @numKitChildQtyShipped AS INT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty INT,
			numKitChildQtyShipped INT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						(ISNULL(numQtyItemsReq_Orig,0) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									(ISNULL(OKCI.numQtyItemsReq_Orig,0) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE 
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty
							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=@dtShippedDate WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @vcDescription AS VARCHAR(300)
	DECLARE @numAllocation AS INT


	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS INT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS INT
	DECLARE @numChildQtyShipped AS INT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty INT,
		numChildQtyShipped INT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS INT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS INT
	DECLARE @numKitChildQtyShipped AS INT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty INT,
		numKitChildQtyShipped INT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped NUMERIC(18,0),
		numQty NUMERIC(18,0),
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped Return (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						(ISNULL(OKI.numQtyItemsReq_Orig,0) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									(ISNULL(OKCI.numQtyItemsReq_Orig,0) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--PLACE QTY BACK ON ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation + @numKitChildQty
						
									-- UPDATE WAREHOUSE INVENTORY
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numKitChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numKitChildWarehouseItemID      	
						
									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped Return (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							
							--PLACE QTY BACK ON ALLOCATION
							SET @numChildAllocation = @numChildAllocation + @numChildQty
						
							-- UPDATE WAREHOUSE INVENTORY
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numChildAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numChildWarehouseItemID      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--PLACE QTY BACK ON ALLOCATION
					SET @numAllocation = @numAllocation + @numQty	

					-- UPDATE WAREHOUSE INVENTORY
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numAllocation
						,dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID      	

					IF @bitSerial = 1
					BEGIN 
						--INCREASE SERIAL NUMBER
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numQty=1 
						WHERE 
							numWareHouseItmsDTLID IN (
								SELECT 
									ISNULL(OWSI.numWareHouseItmsDTLID ,0)
								FROM 
									OppWarehouseSerializedItem OWSI
								WHERE 
									OWSI.numOppID = @numOppID
									AND OWSI.numOppItemID =  @numOppItemID
									AND OWSI.numWarehouseItmsID = @numWarehouseItemID
									AND OWSI.numOppBizDocsId = @numOppBizDocID
							)

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID		
					END

					IF @bitLot = 1
					BEGIN
						-- INCREASE LOT NOT QTY
						UPDATE WHDL
							SET WHDL.numQty = ISNULL(WHDL.numQty,0) + ISNULL(OWSI.numQty,0)
						FROM
							WareHouseItmsDTL  WHDL
						INNER JOIN
							OppWarehouseSerializedItem OWSI
						ON
							WHDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
							AND OWSI.numOppID = @numOppID
							AND OWSI.numOppItemID =  @numOppItemID
							AND OWSI.numWarehouseItmsID = @numWarehouseItemID
							AND OWSI.numOppBizDocsId = @numOppBizDocID

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null                                                              
)                                                                                                                                                
                                                                                                                                            
as                           
BEGIN
     --OpportunityKitItems
    SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * OKI.numQtyItemsReq_Orig) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		ISNULL(i.monAverageCost,'0') AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityItems opp 
	JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	JOIN 
		OpportunityKitItems OKI 
	ON 
		OKI.numOppItemID=opp.numoppitemtCode 
		AND Opp.numOppId=OKI.numOppId
	LEFT JOIN 
		Item i 
	ON 
		OKI.numChildItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
	UNION ALL
	SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.numQtyItemsReq_Orig,0)) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		OKI.numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		ISNULL(i.monAverageCost,'0') AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityKitChildItems OKCI 
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems opp 
	ON
		OKCI.numOppItemID = opp.numoppitemtCode
		AND OKCI.numOppID = @numOppId 
	INNER JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       	
	LEFT JOIN 
		Item i 
	ON 
		OKCI.numItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		OKCI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END
/****** Object:  StoredProcedure [dbo].[USP_SaveContactColumnConfiguration1]    Script Date: 07/26/2008 16:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecontactcolumnconfiguration1')
DROP PROCEDURE usp_savecontactcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_SaveContactColumnConfiguration1]    
 @numDomainID Numeric(9),        
 @numUserCntId Numeric(9),      
 @FormId tinyint ,    
 @numtype numeric(9),    
 @strXml   as text ='',
@numViewID numeric(9)=0         
AS     
    
    
delete from DycFormConfigurationDetails where numDomainID=@numDomainID and [numFormId]= @FormId      
and [numUserCntId]= @numUserCntId AND tintPageType=1 AND isnull(numRelCntType,0)=@numtype and isnull(numViewID,0)=@numViewID   
    
    
DECLARE @hDoc1 int                                      
 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                      

 insert into DycFormConfigurationDetails(numFormId,numFieldId,intRowNum,intColumnNum,
    numUserCntId,numDomainId,bitCustom,tintPageType,numRelCntType,numViewID)    
 select @FormId,X.numFieldID,X.tintOrder,1,@numUserCntId,@numDomainID,X.bitCustom,1,@numtype,@numViewID
from (SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                      
 WITH (numFieldID numeric(9),      
       tintOrder tinyint,
		bitCustom bit ))X                                     
                                      
                                      
 EXEC sp_xml_removedocument @hDoc1
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID as numeric(9),
	@Units as integer,
	@Mode as TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint BIT = 0
	DECLARE @tintOppType AS TINYINT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID
	
	SELECT 
		@bitReOrderPoint=ISNULL(bitReOrderPoint,0),
		@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomain

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand INT
	DECLARE @onAllocation INT
	DECLARE @onOrder INT
	DECLARE @onBackOrder INT
	DECLARE @onReOrder INT
	DECLARE @numUnits INT
	DECLARE @QtyShipped INT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
				SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=2
				SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
				SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType,
				@bitReOrderPoint = @bitReOrderPoint,
				@numReOrderPointOrderStatus = @numReOrderPointOrderStatus
		END
		ELSE
		BEGIN
    		SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
					
    		IF @Mode=0 --insert/edit
    		BEGIN
    			SET @numUnits = @numUnits - @QtyShipped
    	
				IF @onHand>=@numUnits                                    
				BEGIN                                    
					SET @onHand=@onHand-@numUnits                            
					SET @onAllocation=@onAllocation+@numUnits                                    
				END                                    
				ELSE IF @onHand<@numUnits                                    
				BEGIN                                    
					SET @onAllocation=@onAllocation+@onHand                                    
					SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
					SET @onHand=0                                    
				END  
			
				-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
				If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @onReOrder > 0 AND @bitReOrderPoint = 1
				BEGIN
					BEGIN TRY
						DECLARE @numNewOppID AS NUMERIC(18,0) = 0
						EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomain,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
					END TRY
					BEGIN CATCH
						--WE ARE NOT TROWING ERROR
					END CATCH
				END                       
    		END
			ELSE IF @Mode=1 --Revert
			BEGIN
				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 
								                    
				IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                            
					IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits
					SET @onHand = @onHand + @numUnits                                            
				END                                            
				ELSE IF @numUnits < @onBackOrder 
				BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
				END 
			END
			ELSE IF @Mode=2 --Close
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped  
				SET @onAllocation = @onAllocation - @numUnits            
			END
			ELSE IF @Mode=3 --Re-Open
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation + @numUnits            
			END

			UPDATE 
				WareHouseItems 
			SET 
				numOnHand=@onHand,
				numAllocation=@onAllocation,                                    
				numBackOrder=@onBackOrder,
				dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
	
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
		END
		
	
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_CheckForDuplicateAttr')
DROP PROCEDURE USP_WareHouseItems_CheckForDuplicateAttr
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_CheckForDuplicateAttr]  
	@numDomainID AS NUMERIC(18,0),
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@strFieldList AS TEXT
AS
BEGIN
	DECLARE @bitDuplicate AS BIT = 0
	
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))        

		DECLARE @strAttribute VARCHAR(300) = ''
		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numFldID AS NUMERIC(18,0)
		DECLARE @numFldValue AS VARCHAR(300) 
		SELECT @COUNT = COUNT(*) FROM @TempTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

			IF ISNUMERIC(@numFldValue)=1
			BEGIN
				SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
				IF LEN(@strAttribute) > 0
				BEGIN
					SELECT @strAttribute=@strAttribute+'  - '+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
				END
			END
			ELSE
			BEGIN
				SELECT @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numFldID AND Grp_id = 9
				IF LEN(@strAttribute) > 0
				BEGIN
					SELECT @strAttribute=@strAttribute+'  - -,' 
				END
			END

			SET @i = @i + 1
		END

		IF                                                                 
			(SELECT
				COUNT(*)
			FROM
				WareHouseItems WI                               
			WHERE
				WI.numWarehouseID = @numWareHouseID
				AND ISNULL(WI.numWLocationID,0) = ISNULL(@numWLocationID,0)
				AND WI.numItemID = @numItemCode
				AND WI.numDomainID = @numDomainID
				AND dbo.fn_GetAttributes(WI.numWarehouseItemID,0) = @strAttribute
			) > 0
		BEGIN
			SET @bitDuplicate = 1
		END

		EXEC sp_xml_removedocument @hDoc 
	END	  

	SELECT @bitDuplicate
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ManageInventoryKitWithinKit')
DROP PROCEDURE USP_WareHouseItems_ManageInventoryKitWithinKit
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ManageInventoryKitWithinKit]    
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@Mode as TINYINT,
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@bitReOrderPoint AS BIT,
	@numReOrderPointOrderStatus AS NUMERIC(18,0)
AS
BEGIN	
	DECLARE @TableKitChildItem TABLE
	(
		RowNo INT,
		numItemCode NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numUnits INT,
		numQtyShipped INT
	)

	INSERT INTO @TableKitChildItem
	(
		RowNo,
		numItemCode,
		numWarehouseItemID,
		numUnits,
		numQtyShipped
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped
	FROM
		OpportunityKitChildItems
	JOIN 
		Item I 
	ON 
		OpportunityKitChildItems.numItemID=I.numItemCode    
	WHERE 
		charitemtype = 'P' 
		AND ISNULL(numWareHouseItemId,0) > 0 
		AND numOppID = @numOppID
		AND numOppItemID = @numOppItemID
		AND numOppChildItemID = @numOppChildItemID
  
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS INT
	DECLARE @onAllocation AS INT
	DECLARE @onOrder AS INT
	DECLARE @onBackOrder AS INT
	DECLARE @onReOrder AS INT
	DECLARE @numUnits as INT
	DECLARE @QtyShipped AS INT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	SELECT @COUNT = COUNT(*) FROM @TableKitChildItem

	WHILE @i <= @COUNT
	BEGIN
		SELECT 
			@numItemCode=numItemCode,
			@numWareHouseItemID=numWarehouseItemID,
			@numUnits=ISNULL(numUnits,0),
			@QtyShipped=ISNULL(numQtyShipped,0)
		FROM 
			@TableKitChildItem 
		WHERE 
			RowNo=@i
		
		DECLARE @description AS VARCHAR(100)

		IF @Mode=0
			SET @description='SO CHILD KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
			SET @description='SO CHILD KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=2
			SET @description='SO CHILD KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
			SET @description='SO CHILD KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			
    	SELECT 
			@onHand=ISNULL(numOnHand,0),
			@onAllocation=ISNULL(numAllocation,0),                                    
			@onOrder=ISNULL(numOnOrder,0),
			@onBackOrder=ISNULL(numBackOrder,0),
			@onReOrder = ISNULL(numReorder,0)                                     
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
					
    	IF @Mode=0 --insert/edit
    	BEGIN
    		SET @numUnits = @numUnits - @QtyShipped
    	
		    IF @onHand >= @numUnits                                    
			BEGIN                                    
				SET @onHand=@onHand-@numUnits                            
				SET @onAllocation=@onAllocation+@numUnits                                    
			END                                    
			ELSE IF @onHand < @numUnits                                    
			BEGIN                                    
				SET @onAllocation=@onAllocation+@onHand                                    
				SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
				SET @onHand=0                                    
			END  
			
			-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
			If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @onReOrder > 0 AND @bitReOrderPoint = 1
			BEGIN
				BEGIN TRY
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
				END TRY
				BEGIN CATCH
					--WE ARE NOT TROWING ERROR
				END CATCH
			END                       
    	END
        ELSE IF @Mode=1 --Revert
		BEGIN
			IF @QtyShipped>0
			BEGIN
				IF @tintMode=0
					SET @numUnits = @numUnits - @QtyShipped
				ELSE IF @tintmode=1
					SET @onAllocation = @onAllocation + @QtyShipped 
			END 
								                    
            IF @numUnits >= @onBackOrder 
            BEGIN
                SET @numUnits = @numUnits - @onBackOrder
                SET @onBackOrder = 0
                            
                IF (@onAllocation - @numUnits >= 0)
					SET @onAllocation = @onAllocation - @numUnits

                SET @onHand = @onHand + @numUnits                                            
            END                                            
            ELSE IF @numUnits < @onBackOrder 
            BEGIN                  
				IF (@onBackOrder - @numUnits >0)
					SET @onBackOrder = @onBackOrder - @numUnits					
            END 
		END       
		ELSE IF @Mode=2 --Close
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
            SET @onAllocation = @onAllocation - @numUnits            
		END
		ELSE IF @Mode=3 --Re-Open
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
            SET @onAllocation = @onAllocation + @numUnits            
		END

		UPDATE 
			WareHouseItems 
		SET 
			numOnHand=@onHand,
			numAllocation=@onAllocation,                                    
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
	
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = 3, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID


		SET @i = @i + 1
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ValidateLocation')
DROP PROCEDURE USP_WareHouseItems_ValidateLocation
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ValidateLocation]  
	@numDomainID AS NUMERIC(18,0),
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @bitDuplicate AS BIT = 0
	
	IF                                                                 
		(SELECT
			COUNT(*)
		FROM
			WareHouseItems WI                               
		WHERE
			WI.numWarehouseID = @numWareHouseID
			AND ISNULL(WI.numWLocationID,0) = ISNULL(@numWLocationID,0)
			AND WI.numItemID = @numItemCode
			AND WI.numDomainID = @numDomainID
		) > 0
	BEGIN
		SET @bitDuplicate = 1
	END

	SELECT @bitDuplicate
END

