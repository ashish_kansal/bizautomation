/******************************************************************
Project: Release 6.1 Date: 22.SEP.2016
Comments: ALTER SCRIPTS
*******************************************************************/

BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 INSERT INTO DycFieldMaster
 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
 VALUES
 (1,'Last Sales Order Date','','','','','V','R','Label',47,1,1,1,0,0,0,1,0,0,0,0,0)

 SELECT @numFieldID = SCOPE_IDENTITY()

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (1,'Last Order Date','R','Label','',0,0,'V','',0,'',0,47,1,'',47,1,1,0,0,0)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (1,@numFieldID,1,0,0,'Last Sales Order Date','Label','',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

==================================


USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesOrderQueue]    Script Date: 22-Sep-16 11:09:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesOrderQueue](
	[numMSOQID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numContactId] [numeric](18, 0) NULL,
	[numDivisionId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[bitExecuted] [bit] NULL,
 CONSTRAINT [PK_TblBulkOrderTemp] PRIMARY KEY CLUSTERED 
(
	[numMSOQID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

============================================================


USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesOrderLog]    Script Date: 22-Sep-16 11:09:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesOrderLog](
	[numMSOLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numMSOQID] [numeric](18, 0) NOT NULL,
	[bitSuccess] [bit] NOT NULL,
	[numCreatedOrderID] [numeric](18, 0) NULL,
	[vcError] [text] NULL,
 CONSTRAINT [PK_MassSalesOrderLog] PRIMARY KEY CLUSTERED 
(
	[numMSOLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


