/******************************************************************
Project: Release 5.5 Date: 24.APRIL.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRASANT ****************************/



INSERT INTO RecordHistoryModuleMaster VALUES(7,'Transaction')


CREATE TABLE [dbo].[TrackNotification](
	[numTrackId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserId] [numeric](18, 0) NULL,
	[numModuleId] [numeric](18, 0) NULL,
	[numRecordId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TrackNotification] PRIMARY KEY CLUSTERED 
(
	[numTrackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER DATABASE [Production.2014] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE


INSERT INTO [dbo].[ModuleMaster](vcModuleName, tintGroupType)VALUES('Global Alert Panel',1)
INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(118,42,'','Organization',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(119,42,'','Case',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(120,42,'','Opportunity',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(121,42,'','Order',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(122,42,'','Projects',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(123,42,'','Email',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(124,42,'','Tickler',1,0,0,0,0,'',0)

INSERT INTO [dbo].[PageMaster]
	(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted)
	VALUES
	(125,42,'','Process',1,0,0,0,0,'',0)


CREATE TABLE [dbo].[tbl_LastExecution](
	[dtm_LastExecution] [datetime] NULL
) ON [PRIMARY]


INSERT INTO [dbo].[tbl_LastExecution]
           ([dtm_LastExecution])
     VALUES
           ('2016-02-04 00:00:00.000')

CREATE VIEW [dbo].[View_CompanyAlert]
AS
SELECT        numCompanyId, vcCompanyName, numCompanyType, numCompanyRating, numCompanyIndustry, numCompanyCredit, txtComments, vcWebSite, vcWebLabel1, vcWebLink1, vcWebLabel2, vcWebLink2, 
                         vcWebLabel3, vcWeblabel4, vcWebLink3, vcWebLink4, numAnnualRevID, numNoOfEmployeesId, vcHow, vcProfile, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitPublicFlag, 
                         numDomainID
FROM            dbo.CompanyInfo
WHERE        (bintCreatedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_CaseAlert]
AS
SELECT        numCaseId, numDivisionID, numContactId, vcCaseNumber, intTargetResolveDate, textSubject, numStatus, numPriority, textDesc, textInternalComments, numReason, numOrigin, numType, numRecOwner, 
                         numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainID, tintSupportKeyType, numAssignedBy, numAssignedTo, numContractId
FROM            dbo.Cases
WHERE        (bintModifiedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_OpportunityAlert]
AS
SELECT        numOppId, tintOppType, numContactId, numDivisionId, bintAccountClosingDate, txtComments, bitPublicFlag, tintSource, vcPOppName, intPEstimatedCloseDate, numPClosingPercent, numCampainID, 
                         monPAmount, lngPConclAnalysis, tintActive, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainId, numRecOwner, tintOppStatus, tintshipped, bitOrder, numSalesOrPurType, 
                         numAssignedTo, numAssignedBy, numUserClosed, tintBillToType, tintShipToType, numCurrencyID, fltExchangeRate, monDealAmount, numStatus, numBusinessProcessID, fltDiscountTotal, bitDiscountTypeTotal, 
                         bitStockTransfer, bintOppToOrder, tintSourceType, tintTaxOperator, bitBillingTerms, intBillingDays, bitInterestType, fltInterest, fltDiscount, bitDiscountType, numDiscountAcntType, numOppBizDocTempID, 
                         monShipCost, vcWebApiOrderNo, vcOppRefOrderNo, vcCouponCode, dtItemReceivedDate, bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass, 
                         bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, bitUseShippersAccountNo, vcLanedCost, monLandedCostTotal, bintClosedDate, monDealAmountWith4Decimals, 
                         vcRecurrenceType, bitRecurred, bitFromWorkOrder, bitStopMerge, numBillToAddressID, numShipToAddressID, numShipmentMethod
FROM            dbo.OpportunityMaster
WHERE        (bintModifiedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_ProjectsAlert]
AS
SELECT        numProId, vcProjectID, numIntPrjMgr, numOppId, intDueDate, numCustPrjMgr, numDivisionId, txtComments, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainId, numRecOwner, 
                         tintProStatus, bintProClosingDate, numAssignedby, numAssignedTo, numContractId, vcProjectName, numAccountID, numProjectType, numProjectStatus, numAddressID, dtCompletionDate, monTotalExpense, 
                         monTotalIncome, monTotalGrossProfit
FROM            dbo.ProjectsMaster
WHERE        (bintModifiedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_EmailAlert]
AS
SELECT        numEmailHstrID, vcSubject, vcBody, bintCreatedOn, numNoofTimes, vcItemId, vcChangeKey, bitIsRead, vcSize, bitHasAttachments, dtReceivedOn, tintType, chrSource, vcCategory, vcBodyText, numDomainID, 
                         numUserCntId, numUid, numNodeId, vcFrom, vcTo, vcCC, vcBCC, numListItemId, numEmailId, IsReplied, bitArchived, numOldNodeID, bitInvisible
FROM            dbo.EmailHistory
WHERE        (bintCreatedOn >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_TicklerAlert]
AS
SELECT        numCommId, bitTask, numContactId, numDivisionId, textDetails, intSnoozeMins, intRemainderMins, numStatus, numActivity, numAssign, tintSnoozeStatus, tintRemStatus, numOppId, numCreatedBy, 
                         dtCreatedDate, numModifiedBy, dtModifiedDate, numDomainID, bitClosedFlag, vcCalendarName, bitOutlook, dtStartTime, dtEndTime, numAssignedBy, bitSendEmailTemp, numEmailTemplate, tintHours, bitAlert, 
                         CaseId, CaseTimeId, CaseExpId, numActivityId, bitEmailSent, bitFollowUpAnyTime, dtEventClosedDate, LastSnoozDateTimeUtc
FROM            dbo.Communication
WHERE        (dtModifiedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

CREATE VIEW [dbo].[View_ProcessAlert]
AS
SELECT        numStageDetailsId, numStagePercentageId, tintConfiguration, vcStageName, numDomainId, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, slp_id, numAssignTo, vcMileStoneName, 
                         tintPercentage, bitClose, tinProgressPercentage, dtStartDate, dtEndDate, numParentStageID, intDueDays, numProjectID, numOppID, vcDescription, bitFromTemplate, bitExpenseBudget, monExpenseBudget, 
                         bitTimeBudget, monTimeBudget
FROM            dbo.StagePercentageDetails
WHERE        (bintModifiedDate >=
                             (SELECT        TOP (1) dtm_LastExecution
                               FROM            dbo.tbl_LastExecution))

ALTER TABLE Domain ADD bitApprovalforTImeExpense bit

/*************************** Susant ****************************/


UPDATE Domain SET monAmountPastDue=1 WHERE monAmountPastDue=NULL OR monAmountPastDue=0

ALTER TABLE UserMaster
ADD ProfilePic varchar(100)


Alter table PaymentGatewayDTLID add numSiteId int


UPDATE PaymentGatewayDTLID SET numSiteId=0 WHERE numSiteId IS NULL

BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(26,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,26,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(7,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,7,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK


BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(8,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,8,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

ALTER TABLE domain
ADD numCost numeric

ALTER TABLE OpportunityItems
ADD numCost numeric

ALTER TABLE domain ADD DEFAULT '1' FOR numCost

BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Cost','numCost','numCost','Cost','OpportunityItems','N','R','TextBox',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(26,'Cost','R','TextBox','numCost',0,0,'V','N',0,'OpportunityItems',0,7,1,'Cost',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,26,1,1,'Cost','TextBox','Cost',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK


/*************************** Sandeep ****************************/
ALTER TABLE Item ADD 
bitVirtualInventory BIT

ALTER TABLE BizDocTemplate ADD
numAccountClass NUMERIC(18,0)

ALTER TABLE TabMaster ADD
bitAddIsPopUp BIT,
vcAddURL VARCHAR(500)

======================================================================


UPDATE TabMaster SET numTabName='Email',Remarks='Email',vcAddURL='contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=&pqwRT=' WHERE numTabId=44
UPDATE TabMaster SET vcAddURL='cases/frmAddCases.aspx' WHERE numTabId=4
UPDATE TabMaster SET vcAddURL='Documents/frmGenDocPopUp.aspx'  WHERE numTabId=8
UPDATE TabMaster SET vcAddURL='Projects/frmProjectAdd.aspx'  WHERE numTabId=5


=========================================================================

UPDATE TabMaster SET bitAddIsPopUp=1 WHERE LEN(vcAddURL) > 0


UPDATE TabMaster SET numTabName='Activities',Remarks='Activities',vcAddURL='Admin/actionitemdetails.aspx',bitAddIsPopUp=0 WHERE numTabId=36
UPDATE TabMaster SET vcAddURL='ContractManagement/frmcontract.aspx',bitAddIsPopUp=0  WHERE numTabId=46

=============================================================================
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
	)
	VALUES
	(
		(@numMAXPageNavID + 1),7,0,'Support','',1,4,1
	),
	(
		(@numMAXPageNavID + 2),7,(@numMAXPageNavID + 1),'Cases','../cases/frmCaseList.aspx',1,4,1
	),
	(
		(@numMAXPageNavID + 3),7,(@numMAXPageNavID + 1),'Knowledge Base','../cases/frmKnowledgeBaseList.aspx',1,4,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		4,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		4,
		(@numMAXPageNavID + 2),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		4,
		(@numMAXPageNavID + 3),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
=========================================================================
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
	)
	VALUES
	(
		(@numMAXPageNavID + 1),8,22,'Old Custom Reports','../reports/frmCustomRptList.aspx',1,6,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		6,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
=================================================================================
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
	)
	VALUES
	(
		(@numMAXPageNavID + 1),8,22,'Sales Forecasts','../ForeCasting/frmForeCasting.aspx',1,6,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		6,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

================================================================================================
-- TODO: CHECK IF numTabID 2 = Forecasting, numTabID 68 = Dashboard

DELETE FROM TreeNavigationAuthorization WHERE numTabID IN (2,68)
DELETE FROM PageNavigationDTL WHERE numTabID IN (2,68)
DELETE FROM TabMaster WHERE numTabId IN (2,68)

================================================================================================

ALTER TABLE PageNavigationDTL ADD
bitAddIsPopUp BIT,
vcAddURL VARCHAR(1000)

================================================================================================

-- Sales Opportunity
UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmNewOrder.aspx?OppStatus=0&OppType=1' WHERE numPageNavID=123
-- Purchase Opportunity
UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmAddOpportunity.aspx?OppType=2' WHERE numPageNavID=124
-- Sales Order
UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1' WHERE numPageNavID=125
-- Purchase Order
UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewPurchaseOrder.aspx' WHERE numPageNavID=126
-- Sales Return
UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=1' WHERE numPageNavID=149
-- Purchase Return
UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=2' WHERE numPageNavID=207


-- Campaigns
UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmNewCampaign.aspx' WHERE numPageNavID=16
-- Drip Campaigns
UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmECampaignDtls.aspx' WHERE numPageNavID=168
-- Surveys/Questionaires
UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmCampaignSurveyEditor.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurId=0' WHERE numPageNavID=18

-- TODO: CHECK numPageNavID AS NEW LINK Drip Cases
UPDATE PageNavigationDTL SET vcAddURL='~/cases/frmAddCases.aspx' WHERE numPageNavID=247
-- TODO: CHECK numPageNavID AS NEW LINK Knowledge Base
UPDATE PageNavigationDTL SET vcAddURL='~/cases/frmAddSolution.aspx' WHERE numPageNavID=248


-- My Leads
UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=5&RelID=1&FormID=34' WHERE numPageNavID=8
-- Web Leads
UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=1&RelID=1&FormID=34' WHERE numPageNavID=9
-- Public Leads
UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=2&RelID=1&FormID=34' WHERE numPageNavID=10
-- Prospects
UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?RelID=3&FormID=35' WHERE numPageNavID=11
-- Accounts
UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?RelID=2&FormID=36' WHERE numPageNavID=12
-- Contacts
UPDATE PageNavigationDTL SET vcAddURL='~/contact/newcontact.aspx' WHERE numPageNavID=13

-- Credit Memo
UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=3' WHERE numPageNavID=211
-- Refund
UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=4' WHERE numPageNavID=212


-- Services
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=87&ItemType=Services' WHERE numPageNavID=1
-- Kits
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86&ItemType=Kits' WHERE numPageNavID=2
-- Assemblies
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86&ItemType=Assembly' WHERE numPageNavID=3
-- Inventory item
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86' WHERE numPageNavID=4
-- Non Inventory item
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=87' WHERE numPageNavID=71
-- Serialized/LOT #s Items
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=88' WHERE numPageNavID=72
-- Asset Items
UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86' WHERE numPageNavID=171


================================================================================================================

UPDATE PageNavigationDTL SET bitAddIsPopUp=1 WHERE vcAddURL IS NOT NULL

===============================================================================================================

UPDATE PageNavigationDTL SET bitAddIsPopUp=0 WHERE numPageNavID = 18

=========================================================================
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder,vcAddURL,bitAddIsPopUp
	)
	VALUES
	(
		(@numMAXPageNavID + 1),35,54,'Add Bill','',1,45,1,'~/Accounting/frmAddBill.aspx',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		45,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
=================================================================================

UPDATE PageNavigationDTL SET vcImageURL='' WHERE numTabID=1 AND vcImageURL='../images/tf_note.gif'

==================================================================================

UPDATE PageNavigationDTL SET bitVisible=0 WHERE numTabID=-1 AND numParentID=157

==================================================================================

UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-User.png' WHERE numPageNavID=57
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-List.png',vcPageNavName='Add & Edit Drop-Down Lists' WHERE numPageNavID=61
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-BizForm.png' WHERE numPageNavID=64
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Workflow.png' WHERE numPageNavID=66
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-ECommerce.png' WHERE numPageNavID=73
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Price-Control.png' WHERE numPageNavID=78
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Global.png' WHERE numPageNavID=79
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Field-Management.png' WHERE numPageNavID=109
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Import-Export.png' WHERE numPageNavID=205
UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Commission.png' WHERE numPageNavID=181

==================================================================================

UPDATE TabMaster SET vcURL='Opportunity/frmDealList.aspx?type=1' WHERE numTabId=1 --opportunity/frmOppNavigation.aspx
UPDATE TabMaster SET vcURL='' WHERE numTabId=3 --Marketing/frmMarNavigation.aspx
UPDATE TabMaster SET vcURL='reports/reportslinks.aspx' WHERE numTabId=6 --reports/frmrepNavigation.aspx
UPDATE TabMaster SET vcURL='account/frmAccountList.aspx' WHERE numTabId=7 --prospects/frmRelNavigation.aspx
UPDATE TabMaster SET vcURL='' WHERE numTabId=8 --Documents/frmDocNavigation.aspx
UPDATE TabMaster SET vcURL='Items/frmItemList.aspx?Page=All Items&ItemGroup=0' WHERE numTabId=80 --Items/ItemNavigation.aspx
UPDATE TabMaster SET vcURL='Accounting/frmChartofAccounts.aspx' WHERE numTabId=45 --Accounting/frmActNavigation.aspx
UPDATE TabMaster SET vcURL='Outlook/frmInboxItems.aspx' WHERE numTabID=44 --Outlook/frmBizOWA.aspx

===============================================================================================================

UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Item.png' WHERE numPageNavID=177 --ITEM
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/organization.png' WHERE numPageNavID=135 --Companies & Contacts
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Sales Order.png' WHERE numPageNavID=136 --Orders/Opps
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Case.png' WHERE numPageNavID=138 --Cases
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/project.png' WHERE numPageNavID=139 --Projects
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/campaign.png' WHERE numPageNavID=140 --Surveys
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Search.png' WHERE numPageNavID=178 --My Saved Search
UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Accounting.png' WHERE numPageNavID=204 --Transaction Search

====================================================================================================================

UPDATE PageNavigationDTl SET vcAddURL='~/opportunity/frmAmtPaid.aspx?frm=topTab',bitAddIsPopUp=1 WHERE numPageNavID=55

UPDATE PageNavigationDTl SET vcNavURL='' WHERE numPageNavID=55

========================================================================================================================

UPDATE PageNavigationDTL SET numTabID=45 WHERE numPageNavID IN (211,212)
UPDATE TreeNavigationAuthorization SET numTabID=45 WHERE numPageNavID IN (211,212)


=========================================================================================================================

UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=L&type=1&PType=2&FormId=34' WHERE numPageNavID=188
UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=c&type=0&PType=2&FormId=10' WHERE numPageNavID=191
UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=S&type=0&PType=2&FormId=12' WHERE numPageNavID=192
UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=I&type=1&PType=2&FormId=86' WHERE numPageNavID=231
=====================================

UPDATE PageNavigationDTL SET bitVisible=0 WHERE numPageNavID IN (38,42,39,40,41,43,44)