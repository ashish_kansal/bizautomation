/******************************************************************
Project: Release 8.7 Date: 29.DECEMBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************   NEELAM  *****************************/

ALTER TABLE OpportunityBizDocs ADD bitIsEmailSent bit

INSERT INTO DycFieldMaster
(numModuleID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType,
vcAssociatedControlType, numListID, [order], bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, 
bitWorkFlowField, bitAllowFiltering)
VALUES
(3,'Invoice(s)','tintInvoicing','tintInvoicing','Invoicing','OpportunityMaster','N','R',
'SelectBox',0,50,1,0,0,0,1,0,0,0,0,1)

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Invoice(s)' AND numModuleID = 3 AND vcOrigDbColumnName='tintInvoicing' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID, numFieldID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, bitInResults, bitDeleted, bitDefault, bitSettingField,  
	bitAddField, bitDetailField, bitAllowSorting, bitAllowFiltering
)
VALUES
(
	3, @numFieldID, 39, 0, 0, 'Invoice(s)', 'SelectBox', 1, 0, 0, 1, 0, 0,0,1
)