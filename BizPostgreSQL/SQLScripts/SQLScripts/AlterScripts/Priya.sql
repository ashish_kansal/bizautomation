-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-----------------Activities module addition in Custom reports---------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO [ReportModuleMaster]
--	  (
--		vcModuleName
--		,bitActive
--	  )
--	VALUES
--	  (
--		'Activities'
--		,1
--	  )

--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO [ReportModuleGroupMaster]
--  (
--	numReportModuleID
--	,vcGroupName
--	,bitActive
--  )
--  VALUES
--  (
--	@numFieldID
--	,'Activities'
--	,1
--  )


--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)
	
	--INSERT INTO DycFieldMaster
	--	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
	--	bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	--VALUES
	--	(1,'Follow-up Any Time','bitFollowUpAnyTime','bitFollowUpAnyTime','bitFollowUpAnyTime','Communication','Y','R','CheckBox',
	--		1,1,0,0,1,1,1,1,1,0)

--	SELECT @numFieldID = SCOPE_IDENTITY()


--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
----------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	--DECLARE @numFieldID NUMERIC(18,0)
	
--	INSERT INTO DycFieldMaster
--		(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
--		bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,vcGroup)
--	VALUES
--		(1,'Comments','textDetails','textDetails','textDetails','Communication','V','R','TextBox',
--			1,1,0,0,1,1,1,1,1,0,'Action Items Fields')


--	--SELECT @numFieldID = SCOPE_IDENTITY()


--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

------------------------------------------------------------------------
--INSERT INTO ReportFieldGroupMaster
--(	
--	vcFieldGroupName
--	,bitActive
--)
--	VALUES
--(
--	'Activities'
--	,1
--)
--------------------------------------------------------------------------

--DECLARE @numFieldGroupMasterID NUMERIC(18,0)

--SET @numFieldGroupMasterID = (SELECT MAX(numReportFieldGroupID) + 1 FROM ReportFieldGroupMaster)

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--)
--	VALUES
--(
--	@numFieldGroupMasterID
--	,540
--	,'Assigned To'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,542
--	,'Activity'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,543
--	,'Priority'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,544
--	,'Type'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,549
--	,'Due Date'
--	,'DateField'
--	,'V'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,936
--	,'Follow-up Any Time'
--	,'CheckBox'
--	,'Y'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,937
--	,'Comments'
--	,'TextBox'
--	,'V'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,3
--	,'Organization Name'
--	,'TextBox'
--	,'V'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,65
--	,'Team'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--),
--(
--	@numFieldGroupMasterID
--	,18
--	,'Follow-up Status'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--)



------------------------------------------------------------------

--DECLARE @numReportModuleGroupID NUMERIC(18,0)

--SET @numReportModuleGroupID = (SELECT numReportModuleGroupID FROM [ReportModuleGroupMaster] where vcGroupName = 'Activities')

--INSERT INTO ReportModuleGroupFieldMappingMaster
--(	
--	numReportModuleGroupID
--	,numReportFieldGroupID
--)
--	VALUES
--(
--	@numReportModuleGroupID
--	,@numFieldGroupMasterID
--)
--------------------





-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain
--ADD bitEnableShippingExceptions BIT

--update domain
--set bitEnableStaticShippingRule = 1


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--------------------------------------Order Based Promotion---------------

--ALTER TABLE OpportunityMaster
--ADD numDiscountID NUMERIC(18)
-------
--USE [Production.2014]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DiscountCodes](
--	[numDiscountId] [NUMERIC](18, 0) IDENTITY(1,1) NOT NULL,
--	[numPromotionID] [NUMERIC](18, 0) NOT NULL,
--	[vcDiscountCode] [VARCHAR](20) NULL,
--	[CodeUsageLimit] [INT] NULL	

-- CONSTRAINT [PK_DiscountCodes] PRIMARY KEY CLUSTERED 
--(
--	[numDiscountId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

----------------
--USE [Production.2014]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DiscountCodeUsage](
--	[numDiscountCodeUsageId] [NUMERIC](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDiscountId] [NUMERIC](18, 0) NOT NULL,
--	[numDivisionId] [NUMERIC](18, 0) NULL,
--	[intCodeUsed] [INT] NULL

-- CONSTRAINT [PK_DiscountCodeUsage] PRIMARY KEY CLUSTERED 
--(
--	[numDiscountCodeUsageId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO
--------------------
--USE [Production.2014]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[PromotionOfferOrder](
--	[numProOfferOrderID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numPromotionID] [numeric](18, 0) NOT NULL,
--	[numOrderAmount] [float] NULL,
--	[fltDiscountValue] [float] NULL,
--	[numDomainId] [numeric](18, 0) NULL

-- CONSTRAINT [PK_PromotionOfferOrder] PRIMARY KEY CLUSTERED 
--(
--	[numProOfferOrderID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

-------------------------Shipping Promotions ---------------

--ALTER TABLE ShippingRules
--ADD bitFreeShipping BIT,
--	FreeShippingOrderAmt INT,
--	numRelationship NUMERIC(18,0),
--	numProfile NUMERIC(18,0),
--	numWareHouseID VARCHAR(1000)

--ALTER TABLE ShippingRuleStateList
--ADD vcZipPostal Varchar(10)

--UPDATE PageNavigationDTL
--SET vcPageNavName = 'Real-Time Shipping Rates & Ship Vias'
--WHERE vcPageNavName = 'Shipping Carrier Setup'

--UPDATE PageNavigationDTL
--SET vcPageNavName = 'Static Shipping Rate Tables & Promotions'
--WHERE vcPageNavName = 'Shipping Rules'

----UPDATE PageNavigationDTL
----SET bitVisible = 0
----WHERE vcPageNavName = 'Shipping Promotions'

--delete from PageNavigationDTL WHERE vcPageNavName = 'Shipping Promotions'

--ALTER TABLE Domain
--ADD minShippingCost FLOAT NULL,
--	bitEnableStaticShippingRule BIT

--	--------------Web Store-----------

	
--INSERT INTO PageElementMaster
--(
--	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
--)
--VALUES
--(
--	59,'ShippingPromotion','~/UserControls/ShippingPromotion.ascx','{#ShippingPromotion#}',1,0,0,0
--)

--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType,bitEditor
--)
--VALUES
--(
--	59,'Html Customize','HtmlEditor',1
--)

--------------------------
--USE [Production.2014]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ShippingExceptions](
--	[numShippingExceptionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,	
--	[numDomainID] [numeric](18, 0) NULL,
--	[numClassificationID] [numeric](18, 0) NULL,
--	[PercentAbove] [float] NULL,
--	[FlatAmt] [float] NULL
-- CONSTRAINT [PK_ShippingExceptions] PRIMARY KEY CLUSTERED 
--(
--	[numShippingExceptionID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--USE [Production.2014]
--GO

--DROP TABLE [dbo].[AuthenticationGroupBackOrder]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[AuthenticationGroupBackOrder](
--	[numBackOrderGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL CONSTRAINT [DF_AuthenticationGroupBackOrder_numDomainID]  DEFAULT (1),
--	[numGroupID] [numeric](18, 0) NOT NULL
	
-- CONSTRAINT [PK_AuthenticationGroupBackOrder] PRIMARY KEY CLUSTERED 
--(
--	[numBackOrderGroupID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

---------- Avaliable , On-Hand label change
---------------  
--select * from DycFieldMaster Where vcFieldName like '%on hand%'

--UPDATE DycFieldMaster
--SET vcFieldName = 'Available'
--WHERE numFieldId = 195
--------------------

----FormId = 7, 26, 27, 129
----select * from DycFormField_Mapping WHERE vcFieldName = 'on-hand' AND  numFieldId = 195

--Update DycFormField_Mapping
--SET vcFieldName = 'Available'
--WHERE vcFieldName = 'on-hand' AND  numFieldId = 195
-------------------------------

----FormId = 15, 29, 33, 67, 125
----select * from DycFormField_Mapping WHERE vcFieldName = 'onhand' AND  numFieldId = 195

--Update DycFormField_Mapping
--SET vcFieldName = 'Available'
--WHERE vcFieldName = 'onhand' AND  numFieldId = 195
--------------------------------------

---- FormId = 21, 25, 48, 74, 135

----select * from DycFormField_Mapping WHERE vcFieldName = 'on hand' AND  numFieldId = 195

--Update DycFormField_Mapping
--SET vcFieldName = 'Available'
--WHERE vcFieldName = 'on hand' AND  numFieldId = 195
-------------------------------------------


--UPDATE DycFieldMaster
--SET vcFieldName = 'On Hand'
--WHERE numFieldId = 900 --OR numFieldId = 80833 

----------------------------------------------------
---- FormId = 139

----select * from DycFormField_Mapping WHERE vcFieldName = 'Available' AND  numFieldId = 900

--UPDATE DycFormField_Mapping
--SET vcFieldName = 'On Hand'
--WHERE numFieldId = 900 
--------------------------------------------------------
---- Formid = 7
 
--select * from DycFormField_Mapping WHERE vcFieldName = 'Available' AND  numFieldId = 80833

--UPDATE DycFormField_Mapping
--SET vcFieldName = 'On Hand'
--WHERE numFieldId = 80833 

----------------Next Tasks For Priya.PPT , 2nd Slide--------------

--ALTER TABLE OpportunityMaster
--ADD bitDropShipAddress BIT
-----------------------Task List Item Category in Custom reports--------------

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--)
--	VALUES
--(
--	7
--	,311
--	,'Item Category'
--	,'SelectBox'
--	,'N'
--	,1
--	,1
--	,0
--	,1
--)

-------------------------------------Task list SO comments editable -----------
--UPDATE DycFormField_Mapping
--SET bitInlineEdit = 1
--WHERE numFieldID = 646 AND numFormID = 23

----------------------------------Exhibit Markup Changes----------------------------
--ALTER TABLE Domain
--ADD tintMarkupDiscountOption tinyint,
-- tintMarkupDiscountValue tinyint
 
--ALTER TABLE OpportunityItems
--ADD bitMarkupDiscount bit NULL

--ALTER table ReturnItems
--ADD bitMarkupDiscount BIT NULL

--------------------------------Exhibit Markup Changes----------------------------

------------------------------Date Field WorkFlow changes --------------------
--USE [Production.2014]
--GO

--DROP TABLE [dbo].[AdditionalContactsInformation_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[AdditionalContactsInformation_TempDateFields](
--	[numContactId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[bintDOB] [datetime] NULL
-- CONSTRAINT [PK_AdditionalContactsInformation_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numContactId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------------------------------------

--DROP TABLE [dbo].[Cases_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Cases_TempDateFields](
--	[numCaseId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[intTargetResolveDate] [datetime] NULL
-- CONSTRAINT [PK_Cases_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numCaseId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-----------------------------------------------------------------------------------------------------
--DROP TABLE [dbo].[Communication_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Communication_TempDateFields](
--	[numCommId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[dtStartTime] [datetime] NULL
-- CONSTRAINT [PK_Communication_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numCommId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------------------------------------------------------

--DROP TABLE [dbo].[DivisionMaster_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DivisionMaster_TempDateFields](
--	[numDivisionID] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL
-- CONSTRAINT [PK_DivisionMaster_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numDivisionID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------------


--DROP TABLE [dbo].[OpportunityMaster_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityMaster_TempDateFields](
--	[numOppId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[bintClosedDate] [datetime] NULL,
--	[intPEstimatedCloseDate] [datetime] NULL,
--	[dtReleaseDate] [datetime] NULL
-- CONSTRAINT [PK_OpportunityMaster_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numOppId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------------------------------

--DROP TABLE [dbo].[ProjectsMaster_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ProjectsMaster_TempDateFields](
--	[numProId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[bintProClosingDate] [datetime] NULL,
--	[intDueDate] [datetime] NULL
-- CONSTRAINT [PK_ProjectsMaster_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numProId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------------------------------

--DROP TABLE [dbo].[StagePercentageDetails_TempDateFields]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[StagePercentageDetails_TempDateFields](
--	[numStageDetailsId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[dtStartDate] [datetime] NULL,
--	[dtEndDate] [datetime] NULL
-- CONSTRAINT [PK_StagePercentageDetails_TempDateFields] PRIMARY KEY CLUSTERED 
--(
--	[numStageDetailsId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO
-------------------------------------------------------------

--DROP TABLE [dbo].[WorkFlowARAgingExecutionHistory]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[WorkFlowARAgingExecutionHistory](
--	[numWFDateFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numRecordID] [numeric](18, 0) NOT NULL,
--	[numFormID] [numeric](18, 0) NOT NULL,
--	[numWFID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_WorkFlowARAgingExecutionHistory] PRIMARY KEY CLUSTERED 
--(
--	[numWFDateFieldID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO
------------------------------------------------------------------
--GO

--DROP TABLE [dbo].[WorkFlowDateFieldExecutionHistory]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[WorkFlowDateFieldExecutionHistory](
--	[numWFDateFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numRecordID] [numeric](18, 0) NOT NULL,
--	[numFormID] [numeric](18, 0) NOT NULL,
--	[numWFID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_WorkFlowDateFieldExecutionHistory] PRIMARY KEY CLUSTERED 
--(
--	[numWFDateFieldID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO
-----------------------------------------------------------------------
--INSERT INTO DynamicFormMaster
--(
--	numFormId
--	,vcFormName
--	,cCustomFieldsAssociated
--	,cAOIAssociated
--	,bitDeleted
--	,tintFlag
--	,bitWorkFlow
--	,vcLocationID
--	,bitAllowGridColor
--	,numBizFormModuleID
--)
--VALUES
--(
--	138
--	,'A/R (Aging)'
--	,'Y'
--	,'N'
--	,0
--	,3
--	,0
--	,''
--	,0
--	,null
--)
--------------------------------------------------------------------------------------------
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,138,1,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=68 AND numFieldID = 678 AND vcFieldName = 'On Credit Hold'

------------------------------------------------------------------------------------------------------
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,70,1,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=39 AND numFieldID = 40730 AND vcFieldName = 'Release Date'

-----------------------------------------

--Update DycFormField_Mapping
--SET bitDetailField = 1
--WHERE (numFormID = 35 OR numFormID = 36) AND (numFieldID = 51 OR numFieldID = 52 OR numFieldID = 53 OR numFieldID = 59 OR numFieldID = 60)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
    
------------------------------------------------
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
----Created By Priya Sharma 
--GO
--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteshippingserviceabbr')
--DROP PROCEDURE usp_deleteshippingserviceabbr
--GO
--CREATE PROCEDURE  [dbo].[USP_DeleteShippingServiceAbbr]  
--@numShippingServiceAbbrID AS NUMERIC(18,0)=0,  
--@numDomainID AS NUMERIC(9)=0  

--AS  

--DELETE FROM ShippingServiceAbbreviations WHERE ID=@numShippingServiceAbbrID and numDomainID=@numDomainID

--GO

         
--------------------------------------------
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
----created by Priya Sharma
--GO
--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdateshippingserviceabbr')
--DROP PROCEDURE usp_insertupdateshippingserviceabbr
--GO
--CREATE PROCEDURE [dbo].[USP_InsertUpdateShippingServiceAbbr]   
--@Id AS NUMERIC(18,0)=0,   
--@numDomainID AS NUMERIC(9)=0,   
--@numShippingServiceID AS NUMERIC(9),
--@vcAbbreviation AS varchar(300)

--AS
--	IF @Id = 0     
--	BEGIN      
--		INSERT INTO ShippingServiceAbbreviations
--		(
--			numDomainID,numShippingServiceID,vcAbbreviation
--		)
--		VALUES
--		(
--			@numDomainID,@numShippingServiceID,@vcAbbreviation
--		)      
--	END      
--	ELSE      
--	BEGIN      
--		UPDATE 
--			ShippingServiceAbbreviations 
--		SET 
--			numDomainID=@numDomainID
--			,numShippingServiceID=@numShippingServiceID
--			,vcAbbreviation=@vcAbbreviation   
--		WHERE 
--			ID=@Id 
--			AND numDomainID=@numDomainID  

--	end
--GO


-----------------------------------------
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- created By Priya Sharma
--GO
--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingserviceabbr')
--DROP PROCEDURE usp_getshippingserviceabbr
--GO
--CREATE PROCEDURE [dbo].[USP_GetShippingServiceAbbr]       
--@numDomainID as numeric(9)=0,    
--@numShipViaID as numeric(9)=0,
--@vcAbbreviations AS VARCHAR(300)=''      
--as     
--IF(LEN(@vcAbbreviations)>0)
--BEGIN
--	SELECT SSA.ID
--		,SS.numShipViaID
--		,SSA.numShippingServiceID
--		,SS.vcShipmentService
--		,vcAbbreviation 
--	FROM ShippingServiceAbbreviations SSA
--	LEFT JOIN ShippingService SS ON SSA.numShippingServiceID = SS.numShipmentServiceID     
--	WHERE SSA.numDomainID=@numDomainID AND SS.numShipViaID=@numShipViaID and @vcAbbreviations IN (SSA.vcAbbreviation,vcShipmentService)
--	order by  vcShipmentService
--END
--ELSE
--BEGIN   
--	SELECT 
--		SSA.ID
--		,SS.numShipViaID
--		,SSA.numShippingServiceID
--		,SS.vcShipmentService
--		,vcAbbreviation
--	FROM 
--		ShippingServiceAbbreviations  SSA
--	LEFT JOIN 
--		ShippingService SS 
--	ON 
--		SSA.numShippingServiceID = SS.numShipmentServiceID     

--	WHERE 
--		SSA.numDomainID = @numDomainID 
--		AND SS.numShipViaID=@numShipViaID
--order by  vcShipmentService
--END
--GO

    
-------------------------------------------
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- created By Priya Sharma
--GO
--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingservicevalues')
--DROP PROCEDURE usp_getshippingservicevalues
--GO

--CREATE PROCEDURE [dbo].[USP_GetShippingServiceValues] 
--@numDomainID AS numeric(9)=0  ,
--@numShipViaId AS numeric(18,0)

--AS     

--BEGIN    

-- SELECT vcShipmentService 
--		,numShipmentServiceID
--FROM ShippingService     
-- where numDomainId = @numDomainID  and numShipViaID=@numShipViaId  

--END    


--GO



--------------------------------------------------------------------------
--INSERT INTO ShippingService
--(
--	numShipViaID,
--	numDomainId,
--	vcShipmentService,
--	numShipmentServiceID,
--	bitResidential
--)
--VALUES  
----- 88 UPS
--(
--	88,
--	0,
--	'UPS Next Day Air',
--	40,
--	0
--),
--(
--	88,
--	0,
--	'UPS 2nd Day Air',
--	42,
--	0
--),
--(
--	88,
--	0,
--	'UPS Ground',
--	43,
--	0
--),
--(
--	88,
--	0,
--	'UPS 3Day Select',
--	48,
--	0
--),
--(
--	88,
--	0,
--	'UPS Next Day Air Saver',
--	49,
--	0
--),
--(
--	88,
--	0,
--	'UPS Saver',
--	50,
--	0
--),
--(
--	88,
--	0,
--	'UPS Next Day Air Early A.M.',
--	51,
--	0
--),
--(
--	88,
--	0,
--	'UPS 2nd Day Air AM',
--	55,
--	0
--),

----- 90 USPS
--(
--	90,
--	0,
--	'USPS Express',
--	70,
--	0
--),
--(
--	90,
--	0,
--	'USPS First Class',
--	71,
--	0
--),
--(
--	90,
--	0,
--	'USPS Priority',
--	72,
--	0
--),
--(
--	90,
--	0,
--	'USPS Parcel Post',
--	73,
--	0
--),
--(
--	90,
--	0,
--	'USPS Bound Printed Matter',
--	74,
--	0
--),
--(
--	90,
--	0,
--	'USPS Media',
--	75,
--	0
--),
--(
--	90,
--	0,
--	'USPS Library',
--	76,
--	0
--),
----- 91 FeDEX
--(
--	91,
--	0,
--	'FedEx Priority Overnight',
--	10,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Standard Overnight',
--	11,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Overnight',
--	12,
--	0
--),
--(
--	91,
--	0,
--	'FedEx 2nd Day',
--	13,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Express Saver',
--	14,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Ground',
--	15,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Ground Home Delivery',
--	16,
--	0
--),
--(
--	91,
--	0,
--	'FedEx 1 Day Freight',
--	17,
--	0
--),
--(
--	91,
--	0,
--	'FedEx 2 Day Freight',
--	18,
--	0
--),
--(
--	91,
--	0,
--	'FedEx 3 Day Freight',
--	19,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Priority',
--	20,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Priority Distribution',
--	21,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Economy',
--	22,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Economy Distribution',
--	23,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International First',
--	24,
--	0
--),

--(
--	91,
--	0,
--	'FedEx International Priority Freight',
--	25,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Economy Freight',
--	26,
--	0
--),
--(
--	91,
--	0,
--	'FedEx International Distribution Freight',
--	27,
--	0
--),
--(
--	91,
--	0,
--	'FedEx Europe International Priority',
--	28,
--	0
--)
--------------------------------------------------------
--GO

--/****** Object:  Table [dbo].[ShippingService]    Script Date: 10-08-2018 17:43:55 ******/
--DROP TABLE [dbo].[ShippingService]
--GO

--/****** Object:  Table [dbo].[ShippingService]    Script Date: 10-08-2018 17:43:55 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ShippingService](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numShipViaID] [numeric](18, 0) NULL,
--	[vcShipmentService] [varchar](300) NULL,
--	[numShipmentServiceID] [numeric](18, 0) NOT NULL,
--	[bitResidential] [bit] NULL,
-- CONSTRAINT [PK_ShippingService] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY],
--UNIQUE NONCLUSTERED 
--(
--	[numShipmentServiceID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO



-------------------------------------------------
--GO

--ALTER TABLE [dbo].[ShippingServiceAbbreviations] DROP CONSTRAINT [FK_ShippingServiceAbbreviation]
--GO

--/****** Object:  Table [dbo].[ShippingServiceAbbreviations]    Script Date: 10-08-2018 17:42:59 ******/
--DROP TABLE [dbo].[ShippingServiceAbbreviations]
--GO

--/****** Object:  Table [dbo].[ShippingServiceAbbreviations]    Script Date: 10-08-2018 17:42:59 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ShippingServiceAbbreviations](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numShippingServiceID] [numeric](18, 0) NULL,
--	[vcAbbreviation] [varchar](300) NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[ShippingServiceAbbreviations]  WITH CHECK ADD  CONSTRAINT [FK_ShippingServiceAbbreviation] FOREIGN KEY([numShippingServiceID])
--REFERENCES [dbo].[ShippingService] ([numShipmentServiceID])
--GO

--ALTER TABLE [dbo].[ShippingServiceAbbreviations] CHECK CONSTRAINT [FK_ShippingServiceAbbreviation]
--GO




-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--UPDATE ShippingFieldValues
--set vcShipFieldValue = 'https://wwwapps.ups.com/WebTracking/processInputRequest?tracknum='
--where numListItemID = 88 AND intShipFieldID=14 


--Insert into EmailMergeFields
--(
--	vcMergeField, vcMergeFieldValue, numModuleID,tintModuleType
--)
--Values
--( 
--	'Opp/Order Account', '##OppOrderAccountName##', 2,0
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--SET @numFieldID = (select numFieldID from DycFormField_Mapping WHERE numFormID = 68 AND vcFieldName = 'Signature Type')

--UPDATE DycFieldMaster 
--SET vcGroup = 'Organization Detail Fields' 
--WHERE numFieldId = @numFieldID

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,68,1,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	1,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=36 AND vcFieldName = 'Signature Type'

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--ALTER table TempARRecord1
--ADD numCurrentDays decimal(20,5) NULL,
-- intCurrentDaysCount int null,
-- numCurrentDaysPaid decimal(20,5) NULL

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--UPDATE ShippingFieldValues
--set vcShipFieldValue = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='
--where numListItemID = 91 AND intShipFieldID=10


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Insert into EmailMergeFields
--(
--	vcMergeField,	vcMergeFieldValue, numModuleID,tintModuleType
--)
--Values
--( 
--	'Opp/Order Shipping Service', '##OppOrderShippingService##', 2,0
--),
--( 
--	'Opp/Order Last 4 digits of primary credit card', '##PrimaryCreditCardNo##', 2,0
--)

-------------------------------------------------------------------------

--UPDATE EmailMergeFields
--SET vcMergeFieldValue = '##PrimaryCreditCardNo##'
--WHERE numModuleID = 45 AND vcMergeFieldValue = '##OrgLast4digitsofprimarycreditcard##'

-----------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--		(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--		bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	VALUES
--		(3,'EDI Fields','EDIFields','EDIFields','EDIFields','EDIFields','V','R','HyperLink',46,1,1,1,1,0,0,1,1,1,1,1,0)

--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--		(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--		bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering)
--	VALUES
--		(3,@numFieldID,39,1,1,'EDI Fields','EDIFields','EDIFields',7,1,7,1,0,0,1,1,1,0,1)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE ITEM 
--ADD vcASIN VARCHAR(50) NULL

--ALTER TABLE DivisionMaster
--ADD tintInbound850PickItem INT NULL

-----------------------------------------------------------------------

--SET IDENTITY_INSERT [dbo].[CFW_Loc_Master] ON

--INSERT INTO CFW_Loc_Master
--(
--	Loc_id,Loc_name,vcFieldType,vcCustomLookBackTableName
--)
--VALUES
--(
--	19,'EDI Fields for Orders','O','CFW_Fld_Values_Opp_EDI'
--),
--(
--	20,'EDI Fields for Items','P','CFW_FLD_Values_Item_EDI'
--),
--(
--	21,'EDI Fields for OppItems','O','CFW_Fld_Values_OppItems_EDI'
--)


--SET IDENTITY_INSERT [dbo].[CFW_Loc_Master] OFF
--GO

------------------------------------------------------------------------------------

--DELETE FROM ListDetails WHERE numListItemID = 15446 AND numListID = 176 AND vcData = 'Shipment Request (940) Failed'

--DELETE FROM ListDetails WHERE numListItemID = 15448 AND numListID = 176 AND vcData = 'Send 856 Failed'

--UPDATE ListDetails
--SET vcData = 'Send 856 & Invoice (810)' WHERE numListItemID = 15447 AND numListID = 176 AND vcData = 'Send 856'

-------------------------------------------------------------------------------

--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_OppItems_EDI] DROP CONSTRAINT [FK_CFW_Fld_Values_OppItems_EDI_CFW_Fld_Master]
--GO

--DROP TABLE [dbo].[CFW_Fld_Values_OppItems_EDI]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CFW_Fld_Values_OppItems_EDI](
--	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[Fld_ID] [numeric](18, 0) NULL,
--	[Fld_Value] [varchar](max) NULL,
--	[RecId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_CFW_Fld_Values_OppItems_EDI] PRIMARY KEY CLUSTERED 
--(
--	[FldDTLID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_OppItems_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_OppItems_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
--REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_OppItems_EDI] CHECK CONSTRAINT [FK_CFW_Fld_Values_OppItems_EDI_CFW_Fld_Master]
--GO

---------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--		(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--		bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	VALUES
--		(3,'EDI Fields','EDIFields','EDIFields','EDIFields','EDIFields','V','R','HyperLink',46,1,1,1,1,0,0,1,1,1,1,1,0)

--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--		(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--		bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering)
--	VALUES
--		(3,@numFieldID,26,1,1,'EDI Fields','EDIFields','EDIFields',7,1,7,1,0,0,1,1,1,0,1)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------------------------------
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_Opp_EDI] DROP CONSTRAINT [FK_CFW_Fld_Values_Opp_EDI_CFW_Fld_Master]
--GO


--DROP TABLE [dbo].[CFW_Fld_Values_Opp_EDI]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CFW_Fld_Values_Opp_EDI](
--	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[Fld_ID] [numeric](18, 0) NOT NULL,
--	[Fld_Value] [varchar](max) NULL,
--	[RecId] [numeric](18, 0) NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[bintModifiedDate] [datetime] NULL,
-- CONSTRAINT [PK_CFW_Fld_Values_Opp_EDI] PRIMARY KEY CLUSTERED 
--(
--	[FldDTLID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_Opp_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_Opp_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
--REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_Opp_EDI] CHECK CONSTRAINT [FK_CFW_Fld_Values_Opp_EDI_CFW_Fld_Master]
--GO

-----------------------------------------------------------------------
--GO

--ALTER TABLE [dbo].[CFW_FLD_Values_Item_EDI] DROP CONSTRAINT [FK_CFW_FLD_Values_Item_EDI_CFW_Fld_Master]
--GO

--DROP TABLE [dbo].[CFW_FLD_Values_Item_EDI]
--GO


--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CFW_FLD_Values_Item_EDI](
--	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[Fld_ID] [numeric](18, 0) NULL,
--	[Fld_Value] [varchar](max) NULL,
--	[RecId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_CFW_FLD_Values_Item_EDI] PRIMARY KEY CLUSTERED 
--(
--	[FldDTLID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[CFW_FLD_Values_Item_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_FLD_Values_Item_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
--REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
--GO

--ALTER TABLE [dbo].[CFW_FLD_Values_Item_EDI] CHECK CONSTRAINT [FK_CFW_FLD_Values_Item_EDI_CFW_Fld_Master]
--GO

---------------------------------------------------------
--GO
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO
--CREATE TABLE [dbo].[TrueCommerceLog](
--	[numTCQueueID] [int] IDENTITY(1,1) NOT NULL,
--	[FileName] [varchar](50) NULL,
--	[PurchaseOrder] [varchar](300) NULL,
--	[vcMessage] [varchar](max) NULL,
--	[dtDate] [datetime] NULL,
--	[numDomainID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_TrueCommerceLog] PRIMARY KEY CLUSTERED 
--(
--	[numTCQueueID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO



----------------------------------------------------

--ALTER TABLE DomainSFTPDetail ADD vcImportPath VARCHAR(MAX)
--ALTER TABLE DomainSFTPDetail ADD vcExportPath VARCHAR(MAX)

--TODO: change domianid to allure domain id
--INSERT INTO DomainSFTPDetail
--(
--	numDomainID,vcUsername,vcPassword,tintType,vcImportPath,vcExportPath
--)
--VALUES
--(
--	72,'124085054465','HUGGLE37146',2,'/124085054465/IntegrationFTP/Import/Huggle Hounds/Transaction/','/124085054465/IntegrationFTP/Export/Huggle Hounds/Transaction/'
--)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Alter Table OpportunityItems ADD ItemReleaseDate  [datetime] null

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	SET @numFieldID = (SELECT TOP 1 numFieldID FROM DycFieldMaster WHERE vcDbColumnName ='numDefaultShippingServiceID' and vcFieldName = 'Shipping Service' and vcLookBackTableName = 'OpportunityMaster')
--	print @numFieldID

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,41,0,0,'Shipping Service','SelectBox','ShippingService',1,1,1,0,0,1,0,1,0,1
--	)

--	DECLARE @numFieldIDShipVia NUMERIC(18,0)

--	SET @numFieldIDShipVia = (SELECT TOP 1 numFieldID FROM DycFieldMaster WHERE vcDbColumnName ='numShipmentMethod' and vcFieldName = 'Ship Via' and vcLookBackTableName = 'OpportunityMaster')
--	print @numFieldIDShipVia

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldIDShipVia,41,0,0,'Ship Via','SelectBox','ShipmentMethod',1,1,1,0,0,1,0,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE SalesOrderConfiguration ADD bitDisplayComments BIT

--ALter TABLE SalesOrderConfiguration
--ADD bitDisplayReleaseDate BIT

--ALTER TABLE salesorderconfiguration
--ADD bitDisplayCustomerPart#Entry BIT

--ALTER TABLE salesorderconfiguration
--ADD bitDisplayItemGridOrderAZ BIT,
-- bitDisplayItemGridItemID BIT,
-- bitDisplayItemGridSKU BIT,
-- bitDisplayItemGridUnitListPrice BIT,
-- bitDisplayItemGridUnitSalePrice BIT,
-- bitDisplayItemGridDiscount BIT,
-- bitDisplayItemGridItemRelaseDate BIT,
-- bitDisplayItemGridLocation BIT,
-- bitDisplayItemGridShipTo BIT,
-- bitDisplayItemGridOnHandAllocation BIT,
-- bitDisplayItemGridDescription BIT,
-- bitDisplayItemGridNotes BIT,
-- bitDisplayItemGridAttributes BIT,
-- bitDisplayItemGridInclusionDetail BIT,
-- bitDisplayItemGridItemClassification BIT


-- BEGIN TRY
--BEGIN TRANSACTION

--Alter Table OpportunityItems ADD numShipToAddressID  NUMERIC(18,0)

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(4,'Ship To','numShipToAddressID','OppItemShipToAddress','ShipToAddress','OpportunityItems','V','R','Label',1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()


--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering)
--VALUES
--(4,@numFieldID,26,0,0,'Ship To','Label','ShipToAddress',1,1,0,0,1,0,1)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Insert into EmailMergeFields
--(
--	vcMergeField,	vcMergeFieldValue, numModuleID,tintModuleType
--)
--Values
--( 
--	'Opp/Order TrackingNo', '##OppOrderTrackingNo##', 2,0
--)
---------------------------------------------
--DECLARE @numShippingServiceFieldID NUMERIC(18,0)
--SELECT @numShippingServiceFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numDefaultShippingServiceID'  AND vcLookBackTableName='OpportunityMaster'

--print @numShippingServiceFieldID

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,@numShippingServiceFieldID,1,0,0,'Shipping Service','SelectBox','ShippingService',1,0,0,1,0,1,
--	1,1,1
--)
-----------------------------------------------------
--DECLARE @numShipViaFieldID NUMERIC(18,0)
--SELECT @numShipViaFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intShippingCompany'  AND vcLookBackTableName='DivisionMaster'

--print @numShipViaFieldID

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,@numShipViaFieldID,36,0,0,'Preferred Ship Via','SelectBox','ShipmentMethod',1,0,0,1,0,1,
--	1,1,1
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	2,40828,36,0,0,'Shipping Service','SelectBox','ShippingService',1,0,0,1,0,1,
--	0,1,1
--),
--(
--	2,70840,36,0,0,'Signature Type','SelectBox','SignatureType',1,0,0,1,0,1,
--	0,1,1
--)



--UPDATE DycFormField_Mapping
--SET bitAllowFiltering = 1
--WHERE numFieldId = 269 AND  numFormID = 23

----------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--ALTER TABLE GenericDocuments
--  ADD BizDocOppType VARCHAR(10),
--	BizDocType VARCHAR(10),
--	BizDocTemplate VARCHAR(10);

-----------------------------------------------------

--DECLARE @numParentId NUMERIC(18,0)
--DECLARE @numTabId NUMERIC(18,0)
--DECLARE @numModuleId NUMERIC(18,0)


--SET @numParentId = (SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')

--Print @numParentId

--SET @numTabId = (SELECT numTabID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')

--Print @numTabId

--SET @numModuleId = (SELECT numModuleID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')

--Print @numModuleId

--UPDATE PageNavigationDTL 
--SET numParentID = @numParentId, numTabID = @numTabId, numModuleID = @numModuleId ---numParentID = 17
--WHERE vcPageNavName = 'e-Mail Templates';

-------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)

-- INSERT INTO DycFieldMaster
-- (
--  numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup,bitInResults
-- )
-- VALUES
-- (
--  1,'e-commerce Access','bitEcommerceAccess','bitEcommerceAccess','IsEcommerceAccess','ExtarnetAccounts','Y','R','CheckBox',0,1,0,1,0,1,1,1,1,1,'Org.Details Fields',1
-- )

-- SELECT @numFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (
--  numModuleID, numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
-- )
-- VALUES
-- (
--  1,@numFieldID,34,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
-- ),
-- (
--  1,@numFieldID,35,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
-- ),
-- (
--  1,@numFieldID,36,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
-- )


-- INSERT INTO DycFieldMaster
-- (
--  numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup,bitInResults
-- )
-- VALUES
-- (
--  2,'e-commerce password','vcPassword','vcPassword','vcPassword','ExtranetAccountsDtl','V','R','TextBox',0,1,0,1,0,1,1,1,1,1,'Contact Fields',1
-- )

-- SELECT @numFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (
--  numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
-- )
-- VALUES
-- (
--  2,@numFieldID,10,1,1,'e-commerce password','TextBox','vcPassword',1,0,0,1,1,1,0
-- )

-----------------------------------------------------------------


----Declare @numModuleId As numeric

--SET @numModuleId = (SELECT MAX(numModuleID) + 1 FROM EmailMergeModule)

--Print @numModuleId

--INSERT INTO EmailMergeModule
--(
--  numModuleID,vcModuleName,tintModuleType
--)
--VALUES
--(
--	@numModuleId, 'Organization(Leads/Prospects/Accounts) And Contacts' , 0
--)
-----------------------------------------------------
--Update EmailMergeModule
--SET tintModuleType = 2
--WHERE vcModuleName IN('Items','Projects','Cases','Tickler','Documents','Leads/Prospects/Accounts')
----------------------------------------------------------------------------------------------------

----Declare @numModuleId numeric

--SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Leads/Prospects/Accounts And Contacts' AND tintModuleType = 0)

--Print @numModuleId
--INSERT INTO EmailMergeFields
--(
--	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
--)
--VALUES
----Org. Name
--(
--	'Org. Profile', '##OrgProfile##', @numModuleId , 0 
--),
--(
--	'Org. Assign to', '##OrgAssignto##' , @numModuleId, 0
--),
--(
--	'Org. Billing Name', '##OrgBillingName##' , @numModuleId, 0
--),
--(
--	'Org. Billing Street', '##OrgBillingStreet##' , @numModuleId, 0
--),
--(
--	'Org. Billing City', '##OrgBillingCity##' , @numModuleId, 0
--),
--(
--	'Org. Billing State', '##OrgBillingState##' , @numModuleId, 0
--),
--(
--	'Org. Billing Postal', '##OrgBillingPostal##' , @numModuleId, 0
--),
--(
--	'Org. Billing Country', '##OrgBillingCountry##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Name', '##OrgShippingName##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Street', '##OrgShippingStreet##' , @numModuleId, 0
--),
--(
--	'Org. Shipping City', '##OrgShippingCity##' , @numModuleId, 0
--),
--(
--	'Org. Shipping State', '##OrgShippingState##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Postal', '##OrgShippingPostal##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Country', '##OrgShippingCountry##' , @numModuleId, 0
--),
--(
--	'Org. Credit Limit', '##OrgCreditLimit##' , @numModuleId, 0
--),
--(
--	'Org. Net Terms', '##OrgNetTerms##' , @numModuleId, 0
--),
--(
--	'Org. Payment Method', '##OrgPaymentMethod##' , @numModuleId, 0
--),
--(
--	'Org. Preferred Ship Via', '##OrgPreferredShipVia##' , @numModuleId, 0
--),
--(
--	'Org. Preferred Parcel Shipping Service', '##OrgPreferredParcelShippingService##' , @numModuleId, 0
--),
--(
--	'Org. Last 4 digits of primary credit card', '##OrgLast4digitsofprimarycreditcard##' , @numModuleId, 0
--),
---- My Signature
--(
--	'Contact First Name', '##ContactFirstName##', @numModuleId , 0 
--),
--(
--	'Contact Last Name', '##ContactLastName##', @numModuleId , 0 
--),
--(
--	'Contact Email', '##ContactEmail##', @numModuleId , 0 
--),
--(
--	'Contact Phone', '##ContactPhone##', @numModuleId , 0 
--),
--(
--	'Contact Phone Extension', '##ContactPhoneExt##', @numModuleId , 0 
--),
--(
--	'Contact Cell Phone', '##ContactCellPhone##', @numModuleId , 0 
--),
--(
--	'Contact Ecommerce password', '##ContactEcommercepassword##' , @numModuleId, 0
--),
--(
--	'My Signature', '##Signature##' , @numModuleId, 0
--)

--UPDATE EmailMergeFields
--SET tintModuleType = 0 , numModuleID = 45
--WHERE vcMergeField = 'Org Name' ANd numModuleID = 1 and  isnull(tintModuleType,0) = 0

--UPDATE EmailMergeFields SET vcMergeField = 'Org. Name' WHERE vcMergeField = 'Org Name' AND numModuleID = 1 AND tintModuleType = 0
-----------------------------------------------------------------------------


----Declare @numModuleId numeric

--SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Opportunity/Order' AND tintModuleType = 0)

--Print @numModuleId
--INSERT INTO EmailMergeFields
--(
--	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
--)
--VALUES

--(
--	'Opp/Order Assigned to', '##OpportunityAssigneeName##', @numModuleId , 0 
--),
--(
--	'Opp/Order Sub-Total', '##OppOrderSubTotal##', @numModuleId , 0 
--),
--(
--	'Opp/Order Name/ID', '##OpportunityName##', @numModuleId , 0 
--),
--(
--	'Opp/Order Contact', '##OppOrderContact##' , @numModuleId, 0
--),
--(
--	'Opp/Order Customer PO#', '##OppOrderCustomerPO##' , @numModuleId, 0
--),
--(
--	'Opp/Order Invoice Grand-total', '##OppOrderInvoiceGrandtotal##' , @numModuleId, 0
--),
--(
--	'Opp/Order Total Amount Paid', '##OppOrderTotalAmountPaid##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing Address Name', '##OppOrderBillingAddressName##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing Street', '##OppOrderBillingStreet##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing City', '##OppOrderBillingCity##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing State', '##OppOrderBillingState##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing Postal', '##OppOrderBillingPostal##' , @numModuleId, 0
--),
--(
--	'Opp/Order Billing Country', '##OppOrderBillingCountry##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping Address Name', '##OppOrderShippingAddressName##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping Street', '##OppOrderShippingStreet##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping City', '##OppOrderShippingCity##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping State', '##OppOrderShippingState##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping Postal', '##OppOrderShippingPostal##' , @numModuleId, 0
--),
--(
--	'Opp/Order Shipping Country', '##OppOrderShippingCountry##' , @numModuleId, 0
--),
--(
--	'Opp/Order Ship Via', '##OppOrderShipVia##' , @numModuleId, 0
--),
--(
--	'Opp/Order Comments', '##OppOrderComments##' , @numModuleId, 0
--),
--(
--	'Opp/Order Release Date', '##OppOrderReleaseDate##' , @numModuleId, 0
--),
--(
--	'Opp/Order Order Close Date', '##OppOrderEstimatedCloseDate##' , @numModuleId, 0
--),
--(
--	'Opp/Order Source', '##OppOrderSource##' , @numModuleId, 0
--)

---- My Signature

--UPDATE EmailMergeFields SET vcMergeField = 'My Signature' WHERE vcMergeField = 'Signature' AND numModuleID = 2 AND tintModuleType = 0

--UPDATE EmailMergeFields 
--SET tintModuleType = 2 WHERE vcMergeField = 'Tracking Numbers' AND numModuleID = 2 AND tintModuleType = 0
--------------------------------------------------------------------------------------------------

----Declare @numModuleId numeric

--SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Leads/Prospects/Accounts' AND tintModuleType = 0)
--INSERT INTO EmailMergeFields
--(
--	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
--)
--VALUES
----Org. Name
--(
--	'Org. Profile', '##OrgProfile##', @numModuleId , 0 
--),
--(
--	'Org. Assign to', '##OrgAssignto##' , @numModuleId, 0
--),
--(
--	'Org. Billing Name', '##OrgBillingName##' , @numModuleId, 0
--),
--(
--	'Org. Billing Street', '##OrgBillingStreet##' , @numModuleId, 0
--),
--(
--	'Org. Billing City', '##OrgBillingCity##' , @numModuleId, 0
--),
--(
--	'Org. Billing State', '##OrgBillingState##' , @numModuleId, 0
--),
--(
--	'Org. Billing Postal', '##OrgBillingPostal##' , @numModuleId, 0
--),
--(
--	'Org. Billing Country', '##OrgBillingCountry##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Name', '##OrgShippingName##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Street', '##OrgShippingStreet##' , @numModuleId, 0
--),
--(
--	'Org. Shipping City', '##OrgShippingCity##' , @numModuleId, 0
--),
--(
--	'Org. Shipping State', '##OrgShippingState##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Postal', '##OrgShippingPostal##' , @numModuleId, 0
--),
--(
--	'Org. Shipping Country', '##OrgShippingCountry##' , @numModuleId, 0
--),
--(
--	'Org. Credit Limit', '##OrgCreditLimit##' , @numModuleId, 0
--),
--(
--	'Org. Net Terms', '##OrgNetTerms##' , @numModuleId, 0
--),
--(
--	'Org. Payment Method', '##OrgPaymentMethod##' , @numModuleId, 0
--),
--(
--	'Org. Preferred Ship Via', '##OrgPreferredShipVia##' , @numModuleId, 0
--),
--(
--	'Org. Preferred Parcel Shipping Service', '##OrgPreferredParcelShippingService##' , @numModuleId, 0
--),
--(
--	'Org. Last 4 digits of primary credit card', '##OrgLast4digitsofprimarycreditcard##' , @numModuleId, 0
--)
---- My Signature

--UPDATE EmailMergeFields
--SET tintModuleType = 2
--WHERE vcMergeField = 'Contact Phone Ext' ANd numModuleID = 1 and tintModuleType = 0

--UPDATE EmailMergeFields
--SET tintModuleType = 0
--WHERE vcMergeField = 'Org Name' ANd numModuleID = 1 and  isnull(tintModuleType,0) = 0

--UPDATE EmailMergeFields SET vcMergeField = 'Org. Name' WHERE vcMergeField = 'Org Name' AND numModuleID = 1 AND tintModuleType = 0

--UPDATE EmailMergeFields SET vcMergeField = 'My Signature' WHERE vcMergeField = 'Signature' AND numModuleID = 1 AND tintModuleType = 0

---------------------------------------------------------------------------------------------------------------------------------------

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


------ Insert record for Email Menu 

--Insert into PageNavigationDTL
--(
--numPageNavID,
--numModuleID,
--numParentID,
--vcPageNavName,
--vcNavURL,
--vcImageURL,
--bitVisible,
--numTabID,
--intSortOrder,
--vcAddURL,
--bitAddIsPopUp
--)

--values
--(
--262,
--33,
--0,
--'Email',
--NULL,
--NULL,
--1,
--44,
--NULL,
--NULL,
--NULL
--)

----- Update Drip Campaigns menu as Parent Email menu
--UPDATE PageNavigationDTL 
--SET numParentID = 262, numTabID = 44, numModuleID = 33 -- numParentID = 16, numTabID = 3   1 row
--WHERE vcPageNavName = 'Drip Campaigns';

-------- Update TabId for Drip Campaigns sub menu with Email TabId

--UPDATE TreeNavigationAuthorization
--set numTabID = 44 -- numTabID = 3, 506 rows
--where numPageNavID = 168



----------------------------------

----- Shift Drip Campaign from Email Menu to Marketing Menu and rename it, remove Surveys/Questionaires sub menu from Marketing tab

--UPDATE PageNavigationDTL 
--SET numParentID = 15, numTabID = 3, numModuleID = 6, vcPageNavName = 'Automated Follow-ups' --numParentID = 262, numTabID = 44, numModuleID = 33 --------  -- numParentID = 16, numTabID = 3   1 row
--WHERE vcPageNavName = 'Drip Campaigns';


--UPDATE TreeNavigationAuthorization
--set numTabID = 3 -- numTabID = 44  506 rows----------- numTabID = 3, 506 rows
--where numPageNavID = 168


--UPDATE PageNavigationDTL 
--SET bitVisible = 0--bitVisible = 1 
--WHERE vcPageNavName = 'Surveys/Questionaires';

---------Delete drip campaign history...

--delete from DycFieldMaster
-- where numFieldId = 80
-- delete from DycFormField_Mapping
-- where numFieldID = 80  -- 4 rows

-- delete from DycFormConfigurationDetails
-- where numFieldId = 80  -- 45 rows
-- -----------------------------------------------
-- BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,
--	bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,bitInlineEdit,vcGroup
--)
--VALUES
--(
--	2,'Last Follow-up','vcLastFollowup','vcLastFollowup','LastFollowup','AdditionalContactsInformation','V','R','Label','',0,1,0,0,0,1,
--	0,1,1,0,0,0,'Contact Fields'
--),
--(
--	2,'Next Follow-up','vcNextFollowup','vcNextFollowup','NextFollowup','AdditionalContactsInformation','V','R','Label','',0,1,0,0,0,1,
--	0,1,1,0,0,0,'Contact Fields'
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	2,@numFieldID,10,0,0,'Last Follow-up','Label','LastFollowup',1,0,0,1,0,1,
--	1,1,0
--),
--(
--	2,@numFieldID,10,0,0,'Next Follow-up','Label','NextFollowup',1,0,0,1,0,1,
--	1,1,0

--)


------------ Leads--------
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	2,40830,34,0,0,'Last Follow-up','Label','LastFollowup',1,0,0,1,0,1,
--	1,1,1
--),
--(
--	2,40831,34,0,0,'Next Follow-up','Label','NextFollowup',1,0,0,1,0,1,
--	1,1,1

--)
-----------------Prospects----------------
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	2,40830,35,0,0,'Last Follow-up','Label','LastFollowup',1,0,0,1,0,1,
--	1,1,1
--),
--(
--	2,40831,35,0,0,'Next Follow-up','Label','NextFollowup',1,0,0,1,0,1,
--	1,1,1

--)

-----------------Accounts----------------
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
--	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	2,40830,36,0,0,'Last Follow-up','Label','LastFollowup',1,0,0,1,0,1,
--	1,1,1
--),
--(
--	2,40831,36,0,0,'Next Follow-up','Label','NextFollowup',1,0,0,1,0,1,
--	1,1,1

--)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



---------------------Change Display name of Drip Campaign-----------------------

--Update DycFieldMaster SET vcFieldName = 'Follow-up campaign' where numFieldId = '74' and vcFieldName = 'Drip Campaign'
--Update DycFormField_Mapping SET vcFieldName = 'Follow-up campaign' where numFieldID = '74' and vcFieldName = 'Drip Campaign'

-- update PageLayout SET vcFieldName = 'Follow-up campaign' WHERE vcFieldName = 'Drip Campaign' -- 2 rows

--  update DynamicFormField_Validation SET vcNewFormFieldName = 'Follow-up campaign' WHERE vcNewFormFieldName = 'Drip Campaign' and numDomainId = 72  ---2 rows

--  UPDATE PageNavigationDTL 
--SET bitVisible = 0--bitVisible = 1 
--WHERE vcPageNavName = 'Drip Campaign Tracker';


---For Total Amount Paid Filter DropDown added in Sales Fulfillment Screen
