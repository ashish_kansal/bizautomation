/******************************************************************
Project: Release 9.6 Date: 19.APR.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

--COMMENT TRIGGER IN OpportunityMaster table
ALTER TABLE OpportunityMaster ADD numShippingService NUMERIC(18,0)
UPDATE OpportunityMaster SET numShippingService=intUsedShippingCompany
UPDATE OpportunityMaster SET intUsedShippingCompany=numShipmentMethod
UPDATE DycFieldMaster SET vcDbColumnName='intUsedShippingCompany',vcOrigDbColumnName='intUsedShippingCompany',vcPropertyName='ShippingCompany' WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='numShipmentMethod' AND vcFieldName = 'Ship Via'

BEGIN TRY
BEGIN TRANSACTION

	 --add new values for slide 1
	 DECLARE @numFieldID AS NUMERIC(18,0)
	 DECLARE @numFormFieldID AS NUMERIC(18,0)

	 INSERT INTO DycFieldMaster 
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,bitInlineEdit
	)
	VALUES
	(
		3,'Shipping Service','numShippingService','numShippingService','ShippingService','OpportunityMaster','N','R','SelectBox','',0,1,0,1,0,1,1,1,0,1,1
	)


	 SELECT @numFieldID = SCOPE_IDENTITY()



	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering)
	 VALUES
	 (3,@numFieldID,41,1,1,'Shipping Service','SelectBox','ShippingService',48,1,1,1,0,0,1,1,1,1,1)

	 UPDATE DycFormField_Mapping SET numFieldID=@numFieldID,bitAllowEdit=1,bitInlineEdit=1 WHERE numFormID=39 AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='numDefaultShippingServiceID' AND vcFieldName='Shipping Service')
	 UPDATE DycFormField_Mapping SET vcPropertyName=NULL WHERE numFormID IN (39,41) AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='intUsedShippingCompany' AND vcFieldName='Ship Via')
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


UPDATE DycFormField_Mapping SET numFieldID=198,vcFieldName='Warehouse BO' WHERE vcFieldName='Back Order' AND vcPropertyName='BackOrder' AND numFormID=26
UPDATE DycFormConfigurationDetails SET numFieldId=198 WHERE numFormId=26 AND numFieldId=40720

-----------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcBackordered' AND vcLookBackTableName='OpportunityItems'


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,26,0,0,'Order BO','Label',1,0,0,1,0
)

----------------------------

ALTER TABLE WorkOrder ADD numOppItemID NUMERIC(18,0)
ALTER TABLE OpportunityItems ADD vcInstruction VARCHAR(1000)
ALTER TABLE OpportunityItems ADD bintCompliationDate DATETIME
ALTER TABLE OpportunityItems ADD numWOAssignedTo NUMERIC(18,0)

/*************************** PRIYA *******************************/

Insert into EmailMergeFields
(
	vcMergeField,	vcMergeFieldValue, numModuleID,tintModuleType
)
Values
( 
	'Opp/Order TrackingNo', '##OppOrderTrackingNo##', 2,0
)
-------------------------------------------
DECLARE @numShippingServiceFieldID NUMERIC(18,0)
SELECT @numShippingServiceFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numDefaultShippingServiceID'  AND vcLookBackTableName='OpportunityMaster'

print @numShippingServiceFieldID

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,@numShippingServiceFieldID,1,0,0,'Shipping Service','SelectBox','ShippingService',1,0,0,1,0,1,
	1,1,1
)
---------------------------------------------------
DECLARE @numShipViaFieldID NUMERIC(18,0)
SELECT @numShipViaFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intShippingCompany'  AND vcLookBackTableName='DivisionMaster'

print @numShipViaFieldID

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,
	bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,@numShipViaFieldID,36,0,0,'Preferred Ship Via','SelectBox','ShipmentMethod',1,0,0,1,0,1,
	1,1,1
)
