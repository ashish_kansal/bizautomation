/******************************************************************
Project: Release 6.9 Date: 13.FEB.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowFiltering,bitAllowSorting
)
VALUES
(
	1,5,43,0,0,'Organization Profile','SelectBox','',1,0,1,1,0,0,1,1
)

UPDATE DycFormField_Mapping SET vcFieldName='Active' WHERE numFormID=1 AND numFieldID=41


UPDATE BizFormWizardMasterConfiguration SET tintPageType=1 WHERE numFormID IN (34,35,36,10,13,12,43,38,39,40,41) AND bitGridConfiguration=1 AND tintPageType=0


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,273,21,1,1,'Item Classification','SelectBox',1,0,1,1,1,1
)


DELETE 
	W 
FROM 
	DycFormConfigurationDetails w
INNER JOIN
	Domain 
ON
	Domain.numDomainId = w.numDomainId
WHERE
	w.numFormId = 96
	AND w.numUserCntID <> Domain.numAdminID

DELETE 
	W 
FROM 
	DycFormConfigurationDetails w
INNER JOIN
	Domain 
ON
	Domain.numDomainId = w.numDomainId
WHERE
	w.numFormId = 97
	AND w.numUserCntID <> Domain.numAdminID


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	3,262,'Discount','TextBox','M',1,1,1,1
),
(
	4,262,'Discount','TextBox','M',1,1,1,1
)

UPDATE DycFormField_Mapping SET vcFieldName='List Price' WHERE numFormID=21 AND numFieldID=190

UPDATE PageMaster SET bitIsUpdateApplicable=1 WHERE numPageID=87 AND numModuleID=35

UPDATE GroupAuthorization SET intUpdateAllowed=3 WHERE numPageID=87 AND numModuleID=35


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID INT

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
	)
	VALUES
	(
		1,'Email to Case','bitEmailToCase','bitEmailToCase','DivisionMaster','bitEmailToCase','Y','R','CheckBox','',0,1,0,1,1,1,1,1,1
	) 


	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
	)
	VALUES
	(
		1,@numFieldID,36,1,1,'Email to Case','bitEmailToCase','CheckBox',16,1,0,0,1,0,1,1,1,0
	)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


ALTER TABLE SalesOrderConfiguration ADD bitDisplayShipVia BIT

UPDATE ListDetails SET numListID=82,vcData='Will-call',bintCreatedDate='2007-12-27 12:10:48.000',numModifiedBy=1,bintModifiedDate='2007-12-27 12:10:48.000',bitDelete=0,constFlag=1 WHERE numListItemID=92


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID INT

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitInlineEdit
	)
	VALUES
	(
		3,'Ship Via','numShipmentMethod','numShipmentMethod','ShipmentMethod','OpportunityMaster','N','R','SelectBox','LI',82,1,0,1,0,1,1,1,1,1
	)


	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
	)
	VALUES
	(
		3,@numFieldID,39,1,1,'Ship Via','ShipmentMethod','SelectBox',16,1,0,0,1,0,1,1,1,0
	)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID INT

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,PopupFunctionName,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
	)
	VALUES
	(
		1,'Credit Cards','vcCreditCards','vcCreditCards','CustomerCreditCardInfo','vcCreditCards','V','R','Popup','OpenManageCC','',0,1,0,1,0,0,1,1,0
	) 


	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
	)
	VALUES
	(
		1,@numFieldID,36,1,1,'Credit Cards','vcCreditCards','Popup',16,1,0,0,0,0,1,0,0,0
	)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


/************************** PRASANT ************************/


INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		46,
		'Gallery',
		'~/UserControls/Gallery.ascx',
		'{#Gallery#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		46,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

--------------------------------------------------------------

INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		45,
		'Sitemap',
		'~/UserControls/Sitemap.ascx',
		'{#Sitemap#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		45,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)