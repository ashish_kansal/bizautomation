/******************************************************************
Project: Release 10.8 Date: 10.JAN.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

INSERT INTO PageElementMaster
(
	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
)
VALUES
(
	60,'OrderPromotion','~/UserControls/OrderPromotion.ascx','{#OrderPromotion#}',1,0,0,0
)

INSERT INTO PageElementAttributes
(
	numElementID,vcAttributeName,vcControlType,bitEditor
)
VALUES
(
	60,'Html Customize','HtmlEditor',1
)

UPDATE PromotionOffer SET dtValidFrom=NULL WHERE dtValidFrom='0001-01-01'
UPDATE PromotionOffer SET dtValidTo=NULL WHERE dtValidTo='0001-01-01'
ALTER TABLE PromotionOffer ALTER COLUMN [dtValidFrom] DATETIME
ALTER TABLE PromotionOffer ALTER COLUMN [dtValidTo] DATETIME


/******************************************** PRASANT *********************************************/

ALTER TABLE FieldRelationship ADD bitTaskRelation BIT DEFAULT 0
CREATE TABLE [dbo].[ProjectProcessStageDetails](
	[numProjectStageDetailsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numProjectId] [numeric](18, 0) NULL,
	[numStageDetailsId] [numeric](18, 0) NULL,
	[dtmStartDate] [datetime] NULL,
 CONSTRAINT [PK_OrderStageDetails] PRIMARY KEY CLUSTERED 
(
	[numProjectStageDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[StagePercentageDetailsTask](
	[numTaskId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numStageDetailsId] [numeric](18, 0) NULL,
	[vcTaskName] [varchar](500) NULL,
	[numHours] [numeric](18, 0) NULL,
	[numMinutes] [numeric](18, 0) NULL,
	[numAssignTo] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
 CONSTRAINT [PK_StagePercentageDetailsTask] PRIMARY KEY CLUSTERED 
(
	[numTaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[StagePercentageDetailsTask] ADD  CONSTRAINT [DF_StagePercentageDetailsTask_dtmCreatedOn]  DEFAULT (getdate()) FOR [dtmCreatedOn]
GO

ALTER TABLE StagePercentageDetailsTask ADD numOppId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD numProjectId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD numParentTaskId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD bitDefaultTask BIT DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD bitTaskClosed BIT DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD bitSavedTask BIT DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD dtmUpdatedOn DATETIME


ALTER TABLE StagePercentageDetails ADD bitIsDueDaysUsed BIT DEFAULT 0
ALTER TABLE StagePercentageDetails ADD numTeamId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetails ADD bitRunningDynamicMode BIT DEFAULT 0

ALTER TABLE Sales_process_List_Master ADD bitAssigntoTeams BIT DEFAULT 0
ALTER TABLE Sales_process_List_Master ADD bitAutomaticStartTimer BIT DEFAULT 0

UPDATE StagePercentageMaster SET numStagePercentage=66 WHERE numStagePercentageId=16

INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(14,50,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(15,33,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(16,33,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(17,34,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(18,25,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(19,25,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(20,25,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(22,20,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(21,20,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(23,20,1)
INSERT INTO StagePercentageMaster(numStagePercentageId,numStagePercentage,numCreatedBy)VALUES(24,20,1)


/******************************************** PRIYA *********************************************/

ALTER TABLE Domain
ADD bitEnableShippingExceptions BIT

update domain
set bitEnableStaticShippingRule = 1