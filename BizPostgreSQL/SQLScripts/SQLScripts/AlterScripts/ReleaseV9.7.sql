/******************************************************************
Project: Release 9.7 Date: 06.MAY.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRIYA *******************************/

ALTER TABLE SalesOrderConfiguration ADD bitDisplayComments BIT

ALter TABLE SalesOrderConfiguration
ADD bitDisplayReleaseDate BIT

ALTER TABLE salesorderconfiguration
ADD bitDisplayCustomerPart#Entry BIT

ALTER TABLE salesorderconfiguration
ADD bitDisplayItemGridOrderAZ BIT,
 bitDisplayItemGridItemID BIT,
 bitDisplayItemGridSKU BIT,
 bitDisplayItemGridUnitListPrice BIT,
 bitDisplayItemGridUnitSalePrice BIT,
 bitDisplayItemGridDiscount BIT,
 bitDisplayItemGridItemRelaseDate BIT,
 bitDisplayItemGridLocation BIT,
 bitDisplayItemGridShipTo BIT,
 bitDisplayItemGridOnHandAllocation BIT,
 bitDisplayItemGridDescription BIT,
 bitDisplayItemGridNotes BIT,
 bitDisplayItemGridAttributes BIT,
 bitDisplayItemGridInclusionDetail BIT,
 bitDisplayItemGridItemClassification BIT


 BEGIN TRY
BEGIN TRANSACTION

Alter Table OpportunityItems ADD numShipToAddressID  NUMERIC(18,0)

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(4,'Ship To','numShipToAddressID','OppItemShipToAddress','ShipToAddress','OpportunityItems','V','R','Label',1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()


INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering)
VALUES
(4,@numFieldID,26,0,0,'Ship To','Label','ShipToAddress',1,1,0,0,1,0,1)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH